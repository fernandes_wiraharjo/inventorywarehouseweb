<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Vendor</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<form name="RawMaterial" action = "${pageContext.request.contextPath}/RawMaterial" method="post">
	<div class="wrapper">

		<%@ include file="/mainform/pages/master_header.jsp"%>

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Vendor <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">

							<table id="tb_itemlib" class="table table-bordered table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>VendorID</th>
										<th>VendorName</th>
										<th>VendorAddress</th>
										<th>Phone</th>
										<th>PIC</th>
										<th></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listvendor}" var="vendor">
										<tr>
											<td><a href="/mainform/pages/RawMaterial?param_id=<c:out value="${raw.rm_ID}" />" data-toggle="modal" data-target="#exampleModal" /><c:out value="${raw.rm_ID}" /></a></td>
											<td><c:out value="${raw.rm_Name}" /></td>
											<td><c:out value="${raw.rm_Type}" /></td>
											<td><a href="/mainform/pages/RawMaterial?exec=delete&param_id=<c:out value="${raw.rm_ID}" />">Delete</a></td>
										</tr>

									</c:forEach>
									<!--modal update -->
									<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel">Raw Material</h4>
      											</div>
      								<div class="modal-body">
        								<form>
          								<div class="form-group">
            								<label for="recipient-name" class="control-label">Raw Material ID</label>	
            								<input type="text" class="form-control" id="Rm_ID" name="Rm_ID">
          								</div>
          								<div class="form-group">
            								<label for="message-text" class="control-label">Raw Material Name</label>
            								<input type="text" class="form-control" id="Rm_Name" name="Rm_Name">
          								</div>
          								<div class="form-group">
            								<label for="message-text" class="control-label">Raw Material Type</label>
            								<input type="text" class="form-control" id="Rm_Type" name="Rm_Type">
          								</div>
        								</form>
      								</div>
      								<div class="modal-footer">
        								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								<button type="button" class="btn btn-primary">Update</button>
      								</div>
    										</div>
  										</div>
								</div>
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> </section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<script src="mainform/plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="mainform/dist/js/pages/dashboard2.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
		$(function() {
			$('#tb_itemlib').DataTable({
				"paging" : true,
				"lengthChange" : false,
				"searching" : false,
				"ordering" : true,
				"info" : true,
				"autoWidth" : false
			});
		});
	</script>
</body>
</html>