<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_storagebintype" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
			<th>Storage Bin Type</th>
			<th>Storage Bin Type Name</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listStorageBinType}" var ="storbintype">
	  <tr>
		<td><c:out value="${storbintype.storageBinTypeID}" /></td>
		<td><c:out value="${storbintype.storageBinTypeName}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassStringStorageBinType('<c:out value="${storbintype.storageBinTypeID}"/>','<c:out value="${storbintype.storageBinTypeName}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_storagebintype").DataTable();
  	});
</script>