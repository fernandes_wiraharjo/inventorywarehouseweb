<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Order Type</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="bom_ordertype" name="bom_ordertype" action = "${pageContext.request.contextPath}/bomordertype" method="post">
<input type="hidden" name="temp_string" value="" />

	<div class="wrapper">
	
		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Order Type <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertOrderType'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan tipe order produksi.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateOrderType'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui tipe order produksi.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessDeleteOrderType'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menghapus tipe order produksi.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedDeleteOrderType'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal menghapus tipe order produksi. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>
							
							<!--modal update & Insert -->
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvOrderTypeID" class="form-group">
	            								<label for="lblOrderTypeID" class="control-label">Order Type ID</label><label id="mrkOrderTypeID" for="formrkOrderTypeID" class="control-label"><small>*</small></label>
	            								<input type="text" class="form-control" id="txtOrderTypeID" name="txtOrderTypeID">
	          								</div>
	          								<div id="dvNumberFrom" class="form-group">
	            								<label for="lblNumberFrom" class="control-label">Number From</label><label id="mrkNumberFrom" for="formrkNumberFrom" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtNumberFrom" name="txtNumberFrom">
	          								</div>
	          								<div id="dvNumberTo" class="form-group">
	            								<label for="lblNumberTo" class="control-label">Number To</label><label id="mrkNumberTo" for="formrkNumberTo" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtNumberTo" name="txtNumberTo">
	          								</div>
	          								<div class="form-group">
	            								<label for="lblOrderTypeDesc" class="control-label">Order Type Desc</label>	
	            								<textarea class="form-control" id="txtOrderTypeDesc" name="txtOrderTypeDesc"></textarea>
	          								</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									<!-- end of update insert modal -->
									
									<!--modal Delete -->
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Order Type</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtOrderTypeID" name="temp_txtOrderTypeID"  />
               	 									<p>Are you sure to delete this order type ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_ordertype" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Order Type ID</th>
										<th>Range From</th>
										<th>Range To</th>
										<th>Order Type Desc</th>
										<th style="width:60px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listBomOrderType}" var="ordertype">
										<tr>
											<td><c:out value="${ordertype.orderTypeID}" /></td>
											<td><c:out value="${ordertype.from}" /></td>
											<td><c:out value="${ordertype.to}" /></td>
											<td><c:out value="${ordertype.orderTypeDesc}" /></td>
											<td><button <c:out value="${buttonstatus}"/> 
														id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert" 
														data-lordertypeid='<c:out value="${ordertype.orderTypeID}" />'
														data-lfrom='<c:out value="${ordertype.from}" />'
														data-lto='<c:out value="${ordertype.to}" />'
														data-lordertypedesc='<c:out value="${ordertype.orderTypeDesc}" />'>
														<i class="fa fa-edit"></i></button> 
												<button <c:out value="${buttonstatus}"/>
														id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" onclick="FuncButtonDelete()" 
														data-toggle="modal" 
														data-target="#ModalDelete"
														data-lordertypeid='<c:out value="${ordertype.orderTypeID}" />'>
												<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
																
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_ordertype").DataTable();
  		$('#M006').addClass('active');
  		$('#M034').addClass('active');
  		
  		$("#dvErrorAlert").hide();
  		
  	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
  	});
	</script>
	
	<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lOrderTypeID = button.data('lordertypeid');
		$("#temp_txtOrderTypeID").val(lOrderTypeID);
	})
	</script>
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		
 		var lOrderTypeID = button.data('lordertypeid');
 		var lNumberFrom = button.data('lfrom');
 		var lNumberTo = button.data('lto');
 		var lOrderTypeDesc = button.data('lordertypedesc');
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtOrderTypeID").val(lOrderTypeID);
 		modal.find(".modal-body #txtNumberFrom").val(lNumberFrom);
 		modal.find(".modal-body #txtNumberTo").val(lNumberTo);
 		modal.find(".modal-body #txtOrderTypeDesc").val(lOrderTypeDesc);
 		
 		if(lOrderTypeID == null || lOrderTypeID == '')
 			{
 				$("#txtOrderTypeID").focus();
 			}
 		else
 			$("#txtNumberFrom").focus();
	})
</script>



<script>

function FuncClear(){
	$('#mrkOrderTypeID').hide();
	$('#mrkNumberFrom').hide();
	$('#mrkNumberTo').hide();
	
	$('#dvOrderTypeID').removeClass('has-error');
	$('#dvNumberFrom').removeClass('has-error');
	$('#dvNumberTo').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Order Type";

	FuncClear();
	$('#txtOrderTypeID').prop('disabled', false);
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById("lblTitleModal").innerHTML = 'Edit Order Type';

	FuncClear();
	$('#txtOrderTypeID').prop('disabled', true);
}

function FuncValEmptyInput(lParambtn) {
	var txtOrderTypeID = document.getElementById('txtOrderTypeID').value;
	var txtNumberFrom = document.getElementById('txtNumberFrom').value;
	var txtNumberTo = document.getElementById('txtNumberTo').value;
	var txtOrderTypeDesc = document.getElementById('txtOrderTypeDesc').value;

	var dvOrderTypeID = document.getElementsByClassName('dvPlantID');
	var dvNumberFrom = document.getElementsByClassName('dvNumberFrom');
	var dvNumberTo = document.getElementsByClassName('dvNumberTo');
	
    if(!txtOrderTypeID.match(/\S/)) {
    	$("#txtOrderTypeID").focus();
    	$('#dvOrderTypeID').addClass('has-error');
    	$('#mrkOrderTypeID').show();
        return false;
    } 
    
    if(!txtNumberFrom.match(/\S/)) {    	
    	$('#txtNumberFrom').focus();
    	$('#dvNumberFrom').addClass('has-error');
    	$('#mrkNumberFrom').show();
        return false;
    } 
    
    if(!txtNumberTo.match(/\S/)) {
    	$('#txtNumberTo').focus();
    	$('#dvNumberTo').addClass('has-error');
    	$('#mrkNumberTo').show();
        return false;
    } 
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/bomordertype',	
        type:'POST',
        data:{"key":lParambtn,"txtOrderTypeID":txtOrderTypeID,"txtNumberFrom":txtNumberFrom,"txtNumberTo":txtNumberTo,"txtOrderTypeDesc":txtOrderTypeDesc},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertOrderType')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan tipe order produksi";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtOrderTypeID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateOrderType')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui tipe order produksi";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtNumberFrom").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/bomordertype';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}

</script>


</body>
</html>