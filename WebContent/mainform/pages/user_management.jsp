<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>User Management</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Select2 -->
 <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="user" name="user" action = "${pageContext.request.contextPath}/user" method="post">
	
	<div class="wrapper">
	
		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				User Management
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertUser'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan pengguna.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateUser'}">
	    					  <div id="alrUpdate" class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui pengguna.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'SuccessDeleteUser'}">
	    					  <div id="alrUpdate" class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menghapus pengguna.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedDeleteUser'}">
									<div class="alert alert-danger alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-ban"></i> Failed
										</h4>
										Gagal menghapus pengguna.
										<c:out value="${errorDescription}" />
										.
									</div>
								</c:if>
	      					
	      					<!--modal update & Insert -->
	      					<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalUpdateInsert" name="lblTitleModalUpdateInsert"></label></h4>	
        												
        											
      											</div>
	      							<div class="modal-body">
	        								
	          						<div id="dvUserID" class="form-group col-xs-12">
	         							<label for="recipient-name" class="control-label">User Id</label><label id="mrkUserID" for="recipient-name" class="control-label"><small>*</small></label>
         								<input type="text" class="form-control" id="txtUserID" name="txtUserID">
       								</div>
       								<div id="dvPassword" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">Password</label><label id="mrkPassword" for="recipient-name" class="control-label"><small>*</small></label>	
         								<input type="password" class="form-control" id="txtPassword" name="txtPassword">
       								</div>
       								<div id="dvRePassword" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">Re-Password</label><label id="mrkRePassword" for="recipient-name" class="control-label"><small>*</small></label>	
         								<input type="password" class="form-control" id="txtRePassword" name="txtRePassword">
       								</div>
       								<div id="dvArea" class="form-group col-xs-12">
         								<label for="message-text" class="control-label">Plant</label><label id="mrkArea" for="recipient-name" class="control-label"> <small>*</small></label>	       								
										<select id="slArea" name="slArea"
											class="form-control select2" multiple="multiple"
											data-placeholder="Select Plant" style="width: 100%;">
											<c:forEach items="${listplant}" var="plant">
												<option id="optarea" name="optarea"
													value="<c:out value="${plant.plantID}" />"><c:out
														value="${plant.plantID}" /> - <c:out
														value="${plant.plantNm}" /></option>
											</c:forEach>
										</select>					        	
       								</div>

       								<div id="dvRole" class="form-group col-xs-12">
         								<label >Role Id</label><label id="mrkRole" for="recipient-name" class="control-label"> <small>*</small></label>
         								<select id="slRole" name="slRole" class="form-control select2" data-placeholder="Select Role" style="width: 100%;">
											<c:forEach items="${listrole}" var="role">
												<option value="<c:out value="${role.roleID}" />"><c:out value="${role.roleID}" /> - <c:out value="${role.roleName}" /></option>
											</c:forEach>
							        	</select>
       								</div>		
	        							
	        							<div class="row"></div>	
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									<!-- modal -->
									
									<!--modal Delete -->
									<div class="example-modal">
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete User</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtUserId" name="temp_txtUserId" >
               	 									<p>Are you sure to delete this user ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button <c:out value="${buttonstatus}"/> type="submit" id="btnDelete" name="btnDelete" class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								      </div>
								    <!--modal Delete -->
									
									
	      					<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_user" class="table table-bordered table-striped table-hover">
								
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>User Id</th>
										<th>Password</th>
										<th>Plant Id</th>
										<th>Role Id</th>
										<th></th>
									</tr>
								</thead>	

								<tbody>

									<c:forEach items="${listuser}" var="user">
										<tr>
											<td>
											<a href="" style="color:#144471;font-weight: bold;" 
											   onclick="FuncButtonUpdate()"
											   data-toggle="modal" 
											   data-target="#ModalUpdateInsert" 
											   data-luserid='<c:out value="${user.userId}" />'
											   data-lpassword='<c:out value="${user.password}" />'
											   data-lrole='<c:out value="${user.role}" />'
											   data-larea='<c:out value="${user.plant}" />'>
											<c:out value="${user.userId}" />
											</a>	
											</td>
											<td><c:out value="${user.password}" /></td>
											<td><c:out value="${user.plant}" /></td>
											<td><c:out value="${user.role}" /></td>
											<td>
											<c:if test="${buttonstatus != 'disabled'}">
												<a href="" 
												   data-toggle="modal" 
												   data-target="#ModalDelete" 
												   data-luserid='<c:out value="${user.userId}" />'
												>Delete</a>
											</c:if>
											</td>
										</tr>

									</c:forEach>

								</tbody>
								
							</table>
							
						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 		$(function () {
 			//Initialize Select2 Elements
 			$(".select2").select2({
 				tags: true,
 			    dropdownParent: $("#ModalUpdateInsert")
 			});
 	 		
 	 		$("#tb_user").DataTable({"scrollX": true});
 	 		$('#M008').addClass('active');
 	  		$('#M040').addClass('active');
 	  		//shortcut for button 'new'
 	  	    Mousetrap.bind('n', function() {
 	  	    	FuncButtonNew(),
 	  	    	$('#ModalUpdateInsert').modal('show')
 	  	    	});
 	  	  $("#dvErrorAlert").hide();
  		});
	</script>
	
	<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
 		
 		var button = $(event.relatedTarget);
 		var luserid = button.data('luserid');
 		var lpassword = button.data('lpassword');
 		var lrole = button.data('lrole');
 		var modal = $(this);
 		
		modal.find(".modal-body #txtUserID").val(luserid);
		
		//modal.find(".modal-body #slRole").val(lrole);
 		
 		$('#slArea').val(null).trigger("change");
		//$("#AreaPanel :input").prop("checked", false);
 		
 		
 		if(luserid == null || luserid == '')
 			$('#txtUserID').focus();
 		else
 		{
 			$('#txtPassword').focus();
			//  			$.each(lbrandlist,function(i){			
			//  			modal.find(".modal-body #"+lbrandlist[i].toString()).prop('checked', true);
			 		
			//  			});
			
 			modal.find(".modal-body #txtPassword").val(atob(unescape(encodeURIComponent(lpassword))));
 			modal.find(".modal-body #txtRePassword").val(atob(unescape(encodeURIComponent(lpassword))));
 			
 			$('#slRole').val(lrole).trigger('change.select2');
 			
			//  			$.each(larealist,function(i){
			//  				modal.find(".modal-body #"+larealist[i].toString()).prop('checked', true);	
			//  			});

 			var larealist = button.data('larea').toString().split(",");
 			$.each(larealist,function(i){			
 				larealist.push(larealist[i].toString());
 	 		});
 			$('#slArea').val(larealist).trigger('change.select2');
 			
 		}	
	})
	</script>

	<script>
	function FuncClear(){
 		$('#mrkUserID').hide();
 		$('#mrkPassword').hide();
 		$('#mrkRePassword').hide();
 		$('#mrkArea').hide();
 		$('#mrkRole').hide();
 	
 		
 		$('#dvUserID').removeClass('has-error');
 		$('#dvPassword').removeClass('has-error');
 		$('#dvRePassword').removeClass('has-error');
 		$('#dvArea').removeClass('has-error');
 		$('#dvRole').removeClass('has-error');
 	
 		$("#dvErrorAlert").hide();
 	}

	function FuncButtonNew() {
 		$('#txtUserID').val('');
 		$('#txtPassword').val('');
 		$('#txtRePassword').val('');
 		$('#slRole').val('').trigger("change");
		//$('#slRole').val($('#slRole option:first-child').val()).trigger("change");
 		$('#slArea').val(null).trigger("change");
 		
		//$('#txtUserID').prop('disabled', false); nandes
 		
 		$('#btnSave').show();
 		$('#btnUpdate').hide();
 		document.getElementById("lblTitleModalUpdateInsert").innerHTML = "USER - ADD NEW";

 		FuncClear();
 	}
	
	function FuncButtonUpdate() {
 		$('#btnSave').hide();
 		$('#btnUpdate').show();
 		document.getElementById("lblTitleModalUpdateInsert").innerHTML = 'USER - EDIT';

 		FuncClear();
 		
 		$('#txtUserID').prop('disabled', true);
 	}
	
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var luserid = button.data('luserid');
		$("#temp_txtUserId").val(luserid);
	})
	
	function FuncValEmptyInput(lParambtn) {
		txtUserID = document.getElementById('txtUserID').value;
		var txtPassword = document.getElementById('txtPassword').value;
		var txtRePassword = document.getElementById('txtRePassword').value;
		var slRole = document.getElementById('slRole').value;
		var slAreaId = $("#slArea").val();
 			
		if(!txtUserID.match(/\S/)) {
    		$("#txtUserID").focus();
   		 	$('#dvUserID').addClass('has-error');
    		$('#mrkUserID').show();
     	   return false;
    	}
    	
    	if(!txtPassword.match(/\S/)) {
    		$("#txtPassword").focus();
   		 	$('#dvPassword').addClass('has-error');
    		$('#mrkPassword').show();
     	   return false;
    	}
    	
    	if(!txtRePassword.match(/\S/)) {
    		$("#txtRePassword").focus();
   		 	$('#dvRePassword').addClass('has-error');
    		$('#mrkRePassword').show();
     	   return false;
    	}
    	
    	if(txtPassword != txtRePassword){
    		$('#txtPassword').focus();
    		$('#dvPassword').addClass('has-error');
    		$('#mrkPassword').show();
    		$('#dvRePassword').addClass('has-error');
    		$('#mrkRePassword').show();
			//$('#txtPassword').val('');
			//$('#txtRePassword').val('');
    		
    		alert("password not match");
    		return false;
    	}
    	
    	if (slAreaId == null) {
			$("#slArea").focus();
			$('#dvArea').addClass('has-error');
			$('#mrkArea').show();
			return false;
		}
    	
    	if(!slRole.match(/\S/)) {
    		$("#slRole").focus();
   		 	$('#dvRole').addClass('has-error');
    		$('#mrkRole').show();
     	   return false;
    	}
    
    	
 	   jQuery.ajax({
 	      url:'${pageContext.request.contextPath}/user',	
  	      type:'POST',
  	      data:{"key":lParambtn,"txtUserID":txtUserID,"txtPassword":txtPassword,"slRole":slRole,"arealist":slAreaId},
          dataType : 'text',
    	  success:function(data, textStatus, jqXHR){
    		  if(data.split("--")[0] == 'FailedInsertUser')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan pengguna";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtUserID").focus();
		        	$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateUser')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui pengguna";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtPassword").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
	        		var url = '${pageContext.request.contextPath}/user';  
	             	$(location).attr('href', url);
	        	}
          },
          error:function(data, textStatus, jqXHR){
          console.log('Service call failed!');
        }
        });
 	   
 	  FuncClear();
 	  
      return true;	
	}
	
	</script>
	
</body>
</html>