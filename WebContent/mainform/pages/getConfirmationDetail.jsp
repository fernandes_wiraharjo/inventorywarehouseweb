<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_confirmation_detail" class="table table-bordered table-hover">
    <thead style="background-color: #d2d6de;">
    <tr>
    <th>Component Line</th>
	<th>Component ID</th>
	<th>Batch No</th>
	<th>Qty Confirmed</th>
	<th>UOM</th>
	<th>Qty Base Confirmed</th>
	<th>Base UOM</th>
   </tr>
   </thead>
     
     <tbody>
     
     <c:forEach items="${listConfirmationDetail}" var ="confirmationdetail">
      <tr>
	<td><c:out value="${confirmationdetail.componentLine}" /></td>
	<td><c:out value="${confirmationdetail.componentID}" /></td>
	<td><c:out value="${confirmationdetail.batchNo}" /></td>
	<td><c:out value="${confirmationdetail.qty}" /></td>
	<td><c:out value="${confirmationdetail.UOM}" /></td>
	<td><c:out value="${confirmationdetail.qtyBaseUOM}" /></td>
	<td><c:out value="${confirmationdetail.baseUOM}" /></td>
     </tr>
     		
     </c:forEach>
     
     </tbody>
     </table>

<script>
 		$(function () {
 			$("#tb_confirmation_detail").DataTable(
 	  				{
 	  					 "aaSorting": []
 	  				});
  		});
</script>