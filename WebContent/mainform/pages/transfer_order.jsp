<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Transfer Order</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll };
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="formTransferOrder" name="formTransferOrder" action = "${pageContext.request.contextPath}/TransferOrder" method="post">

	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Transfer Order : Item List <br><br>
			</h1>
							<c:if test="${condition == 'SuccessConfirmTOLine'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses konfirmasi peletakan.
              				</div>
	      					</c:if>
	      					<c:if test="${condition == 'FailedConfirmTOLine'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal kofirmasi peletakan. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>
	     		<h1>	
				<div class="row">
					<div class="col-md-1">
						<small style="color: black; font-weight: bold;">Plant</small>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" readonly="readonly" value="<c:out value="${lPlantID}"/>">
					</div>
					<div class="col-md-1">
						<small style="color: black; font-weight: bold;">Warehouse</small>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" readonly="readonly" value="<c:out value="${lWarehouseID}"/>">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-1">
						<small style="color: black; font-weight: bold;">TO Number</small>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="txtTransferOrderID" name="txtTransferOrderID" readonly="readonly" value="<c:out value="${lTransferOrderID}"/>">
					</div>
					<div class="col-md-1">
						<small style="color: black; font-weight: bold;">Date</small>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="txtCreationDate" name="txtCreationDate" readonly="readonly" value="<c:out value="${lCreationDate}"/>">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-1">
						<small style="color: black; font-weight: bold;">Src Stor Type</small>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="txtSrcStorType" name="txtSrcStorType" readonly="readonly" value="<c:out value="${lSrcStorageType}"/>">
					</div>
					<div class="col-md-1">
						<small style="color: black; font-weight: bold;">Src Stor Bin</small>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="txtSrcStorBin" name="txtSrcStorBin" readonly="readonly" value="<c:out value="${lSrcStorageBin}"/>">
					</div>
				</div>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
	     					
	     								<!--modal Confirm -->
       									<div class="modal modal-primary" id="ModalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Confirmation Transfer Order</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"  />
              									<input type="hidden" id="temp_txtWarehouseID" name="temp_txtWarehouseID"  />
              									<input type="hidden" id="temp_txtTransferOrderID" name="temp_txtTransferOrderID"  />
              									<input type="hidden" id="temp_txtProductID" name="temp_txtProductID"  />
              									<input type="hidden" id="temp_txtBatchNo" name="temp_txtBatchNo"  />
              									<input type="hidden" id="temp_txtStorageTypeID" name="temp_txtStorageTypeID"  />
              									<input type="hidden" id="temp_txtStorageSectionID" name="temp_txtStorageSectionID"  />
              									<input type="hidden" id="temp_txtStorageBinID" name="temp_txtStorageBinID"  />
              									<input type="hidden" id="temp_txtDestStorageUnit" name="temp_txtDestStorageUnit"  />
              									<input type="hidden" id="temp_txtDestQty" name="temp_txtDestQty"  />
              									<input type="hidden" id="temp_txtDestQtyUOM" name="temp_txtDestQtyUOM"  />
              									<input type="hidden" id="temp_txtCapacityUsed" name="temp_txtCapacityUsed"  />
              									<input type="hidden" id="temp_txtStorageUnitType" name="temp_txtStorageUnitType"  />
              									<input type="hidden" id="temp_txtWMStatus" name="temp_txtWMStatus"  />
              									<input type="hidden" id="temp_txtUpdateWMFirst" name="temp_txtUpdateWMFirst"  />
              									
               	 									<p>Are you sure to confirm this transfer order ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnConfirm" name="btnConfirm"  class="btn btn-outline" >Confirm</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->  
									
							<table id="tb_transfer_order" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>No</th>
										<th>Product ID</th>
										<th>Status</th>
										<th>Dest Stor Type</th>
										<th>Dest Stor Bin</th>
										<th>Dest Qty</th>
										<th>UOM</th>
										<th>Dest Stor Unit</th>
										<th>Stor Unit Type</th>
										<th style="width:20px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listTO}" var="to">
										<tr>
											<td><c:out value="${to.no}" /></td>
											<td><c:out value="${to.productID}" /></td>
											<td><c:out value="${to.status}" /></td>
											<td><c:out value="${to.storageTypeID}" /></td>
											<td><c:out value="${to.destinationStorageBin}" /></td>
											<td><c:out value="${to.destinationQty}" /></td>
											<td><c:out value="${to.UOM}" /></td>
											<td><c:out value="${to.destinationStorageUnit}" /></td>
											<td><c:out value="${to.storageUnitType}" /></td>
											<td><button id="btnModalConfirm" name="btnModalConfirm" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonConfirm()"
														data-target="#ModalConfirm" 
														data-lplantid='<c:out value="${to.plantID}" />'
														data-lwarehouseid='<c:out value="${to.warehouseID}" />'
														data-ltransferorderid='<c:out value="${to.transferOrderID}" />'
														data-lproductid='<c:out value="${to.productID}" />'
														data-lbatchno='<c:out value="${to.batchNo}" />'
														data-lstoragetypeid='<c:out value="${to.storageTypeID}" />'
														data-lstoragesectionid='<c:out value="${to.storageSectionID}" />'
														data-lstoragebinid='<c:out value="${to.destinationStorageBin}" />'
														data-ldeststorageunit='<c:out value="${to.destinationStorageUnit}" />'
														data-ldestqty='<c:out value="${to.destinationQty}" />'
														data-ldestqtyuom='<c:out value="${to.UOM}" />'
														data-lcapacityused='<c:out value="${to.capacityUsed}" />'
														data-lstorageunittype='<c:out value="${to.storageUnitType}" />'
														data-lwmstatus='<c:out value="${to.WMS_Status}" />'
														data-lupdatewmfirst='<c:out value="${to.updateWMFirst}" />'
														<c:out value="${to.buttonStatus}"/>
														>
														CONFIRM</button>
											</td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_transfer_order").DataTable();
  		$('#M004').addClass('active');
  		$('#M026').addClass('active');
  	});
  	
  	$('#ModalConfirm').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lPlantID = button.data('lplantid');
		var lWarehouseID = button.data('lwarehouseid');
		var lTransferOrderID = button.data('ltransferorderid');
		var lProductID = button.data('lproductid');
		var lBatchNo = button.data('lbatchno');
		var lStorageTypeID = button.data('lstoragetypeid');
		var lStorageSectionID = button.data('lstoragesectionid');
		var lStorageBinID = button.data('lstoragebinid');
		var lDestStorageUnit = button.data('ldeststorageunit');
		var lDestQty = button.data('ldestqty');
		var lDestQtyUOM = button.data('ldestqtyuom');
		var lCapacityUsed = button.data('lcapacityused');
		var lStorageUnitType = button.data('lstorageunittype');
		var lWMStatus = button.data('lwmstatus');
		var lUpdateWMFirst = button.data('lupdatewmfirst');
		
		$("#temp_txtPlantID").val(lPlantID);
		$("#temp_txtWarehouseID").val(lWarehouseID);
		$("#temp_txtTransferOrderID").val(lTransferOrderID);
		$("#temp_txtProductID").val(lProductID);
		$("#temp_txtBatchNo").val(lBatchNo);
		$("#temp_txtStorageTypeID").val(lStorageTypeID);
		$("#temp_txtStorageSectionID").val(lStorageSectionID);
		$("#temp_txtStorageBinID").val(lStorageBinID);
		$("#temp_txtDestStorageUnit").val(lDestStorageUnit);
		$("#temp_txtDestQty").val(lDestQty);
		$("#temp_txtDestQtyUOM").val(lDestQtyUOM);
		$("#temp_txtCapacityUsed").val(lCapacityUsed);
		$("#temp_txtStorageUnitType").val(lStorageUnitType);
		$("#temp_txtWMStatus").val(lWMStatus);
		$("#temp_txtUpdateWMFirst").val(lUpdateWMFirst);
	})
	</script>

</body>
</html>