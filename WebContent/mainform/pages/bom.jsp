<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master Bill Of Materials</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<!-- <style type="text/css">	 -->
<!-- #ModalUpdateInsert { overflow-y:scroll } -->
<!-- </style> -->
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="MasterBom" name="MasterBom" action = "${pageContext.request.contextPath}/MasterBom" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Bill Of Materials <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
<%-- 							<c:if test="${condition == 'SuccessInsert'}"> --%>
<!-- 	    					  <div class="alert alert-success alert-dismissible"> -->
<!--           				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--              				  	<h4><i class="icon fa fa-check"></i> Success</h4> -->
<!--                 			  	Sukses menambahkan bom. -->
<!--               				</div> -->
<%-- 	      					</c:if> --%>
	      					
<%-- 	      					<c:if test="${condition == 'FailedInsert'}"> --%>
<!-- 	      						<div class="alert alert-danger alert-dismissible"> -->
<!--                 				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--                 				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--                 				Gagal menambahkan bom. <c:out value="${conditionDescription}"/>. --%>
<!--               				</div> -->
<%-- 	     					</c:if> --%>
	     					
<!-- 	     					<div class="box-body"> -->
<%-- 							<c:if test="${condition == 'SuccessUpdate'}"> --%>
<!-- 	    					  <div class="alert alert-success alert-dismissible"> -->
<!--           				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--              				  	<h4><i class="icon fa fa-check"></i> Success</h4> -->
<!--                 			  	Sukses memperbaharui bom. -->
<!--               				</div> -->
<%-- 	      					</c:if> --%>
	      					
<%-- 	      					<c:if test="${condition == 'FailedUpdate'}"> --%>
<!-- 	      						<div class="alert alert-danger alert-dismissible"> -->
<!--                 				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--                 				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--                 				Gagal memperbaharui bom. <c:out value="${conditionDescription}"/>. --%>
<!--               				</div> -->
<%-- 	     					</c:if> --%>
	      					
	      					<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvProductID" class="form-group">
	            								<label for="recipient-name" class="control-label">Product ID</label><label id="mrkProductID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblProductName" name="lblProductName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtProductID" name="txtProductID" data-toggle="modal" data-target="#ModalGetBomProductID">
	          								</div>
	          								<div id="dvDate" class="form-group">
	            								<label for="message-text" class="control-label">Valid Date</label><label id="mrkDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDate" name="txtDate" data-date-format="dd M yyyy">
	          								</div>
	          								<div id="dvPlantID" class="form-group">
	            								<label for="message-text" class="control-label">Plant ID</label><label id="mrkPlantID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" data-target="#ModalGetPlantID">
	          								</div>
	          								<div id="dvQty" class="form-group">
	            								<label for="message-text" class="control-label">Quantity</label><label id="mrkQty" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtQty" name="txtQty">
	          								</div>
	          								<div id="dvUOM" class="form-group">
	            								<label for="message-text" class="control-label">UOM</label><label id="mrkUOM" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<select id="slMasterUOM" name="slMasterUOM" class="form-control" onfocus="FuncValShowUOM()">
							                    </select>
	          								</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
										
										<!--modal show plant data -->
										<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelPlantID"> Plant Data</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_plant" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Plant ID</th>
										                <th>Plant Name</th>
										                <th>Description</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
 										        <c:forEach items="${listPlant}" var ="plant">
										        <tr>
												        <td><c:out value="${plant.plantID}"/></td>											        
												        <td><c:out value="${plant.plantNm}"/></td>
											        <td><c:out value="${plant.desc}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>','','')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->
										
										<!--modal show bom product data -->
										<div class="modal fade" id="ModalGetBomProductID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelBOMProduct">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelBOMProduct">BOM Product Data</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_bom_product" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Product ID</th>
														<th>Product Name</th>
														<th>Desc</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listBOMProduct}" var ="bomproduct">
												        <tr>
												        <td><c:out value="${bomproduct.id}" /></td>
														<td><c:out value="${bomproduct.title_en}" /></td>
														<td><c:out value="${bomproduct.short_description_en}" /></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('','','<c:out value="${bomproduct.id}"/>','<c:out value="${bomproduct.title_en}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show bom product data -->
										
										<!-- modal show bom detail data -->
										<div class="modal fade bs-example-modal-lg" id="ModalBOMDetail" tabindex="-1" role="dialog" aria-labelledby="ModalLabelBOMDetail">
												<div class="modal-dialog modal-lg" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelBOMDetail">Bill Of Materials Detail Data</h4>
			      											<br><br>
			      											<label id="bomheaderdesc"></label>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load bom detail by bom header will be load here -->                          
           										<div id="dynamic-content">
           										</div>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show bom detail data -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_bom" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Product ID</th>
										<th style="display:none;">Name</th>
										<th>Valid Date</th>
										<th>Plant ID</th>
										<th style="display:none;">Plant Name</th>
										<th>Qty</th>
										<th>UOM</th>
										<th>Qty Base UOM</th>
										<th>Base UOM</th>
										<th style="width:62px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listBOM}" var="bom">
										<tr>
											<td><c:out value="${bom.productID}" /></td>
											<td style="display:none;"><c:out value="${bom.productName}" /></td>
											<td><c:out value="${bom.validDate}" /></td>
											<td><c:out value="${bom.plantID}" /></td>
											<td style="display:none;"><c:out value="${bom.plantName}" /></td>
											<td><c:out value="${bom.qty}" /></td>
											<td><c:out value="${bom.UOM}" /></td>
											<td><c:out value="${bom.qtyBaseUOM}" /></td>
											<td><c:out value="${bom.baseUOM}" /></td>
											<td>
											<button <c:out value="${buttonstatus}"/> 
												id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info"
												onclick="FuncButtonUpdate('<c:out value="${bom.productID}" />')"
												data-toggle="modal"
												data-target="#ModalUpdateInsert" 
												data-lproductid='<c:out value="${bom.productID}" />'
												data-lproductnm='<c:out value="${bom.productName}" />'
												data-ldate='<c:out value="${bom.validDate}" />'
												data-lplantid='<c:out value="${bom.plantID}" />'
												data-lplantnm='<c:out value="${bom.plantName}" />'
												data-lqty='<c:out value="${bom.qty}" />'
												data-luom='<c:out value="${bom.UOM}" />'
												>
												<i class="fa fa-edit"></i>
												</button>

<!-- 											<button type="button" id="btnBOMDetail" name="btnBOMDetail"  -->
<!-- 												class="btn btn-default" data-toggle="modal" data-target="#ModalBOMDetail" -->
<%-- 												data-lproductid='<c:out value="${bom.productID}" />' --%>
<%-- 												data-ldate='<c:out value="${bom.validDate}" />' --%>
<%-- 												data-lplantid='<c:out value="${bom.plantID}" />'> --%>
<!-- 												<i class="fa fa-list-ul"></i> -->
<!-- 												</button> -->
												<c:if test="${buttonstatus == 'disabled'}">
													<button <c:out value="${buttonstatus}"/> type="button" id="btnBOMDetail" name="btnBOMDetail"  class="btn btn-default"><i class="fa fa-list-ul"></i></button>
												</c:if>
												<c:if test="${buttonstatus != 'disabled'}">
													<a href="${pageContext.request.contextPath}/bomproductdetail?id=<c:out value="${bom.productID}" />&date=<c:out value="${bom.validDate}" />&plant=<c:out value="${bom.plantID}" />&qty=<c:out value="${bom.qtyBaseUOM}" />"><button type="button" id="btnBOMDetail" name="btnBOMDetail"  class="btn btn-default"><i class="fa fa-list-ul"></i></button></a>
												</c:if>
											</td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_bom_product").DataTable();
  		$("#tb_master_plant").DataTable();
  		$("#tb_bom").DataTable();
//   		$("#tb_bom_detail").DataTable();
  		$('#M006').addClass('active');
  		$('#M033').addClass('active');
  		
  		$("#dvErrorAlert").hide();
  	});
 	
 	$('#txtDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
 		$('#txtDate').datepicker('setDate', new Date());
 	
 	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
	</script>
    
<script>
$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
	$("#dvErrorAlert").hide();
	
  var button = $(event.relatedTarget) // Button that triggered the modal
  
  var lproductid = button.data('lproductid') // Extract info from data-* attributes
  var lproductnm = button.data('lproductnm') // Extract info from data-* attributes
  var ldate = button.data('ldate') // Extract info from data-* attributes
  var lplantid = button.data('lplantid') // Extract info from data-* attributes
  var lplantnm = button.data('lplantnm') // Extract info from data-* attributes
  var lqty = button.data('lqty') // Extract info from data-* attributes
  var luom = button.data('luom') // Extract info from data-* attributes
  
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  
  modal.find('.modal-body #txtProductID').val(lproductid)
 
  if(lproductnm == undefined)
	  document.getElementById("lblProductName").innerHTML = null;
  else
  	  document.getElementById("lblProductName").innerHTML = '(' + lproductnm + ')'
  
  if(ldate != undefined)
  	modal.find('.modal-body #txtDate').val(ldate)
  
  modal.find('.modal-body #txtPlantID').val(lplantid)
  
  if(lplantnm == undefined)
	  document.getElementById("lblPlantName").innerHTML = null;
  else
	  document.getElementById("lblPlantName").innerHTML = '(' + lplantnm + ')'
  
  modal.find('.modal-body #txtQty').val(lqty)
  modal.find('.modal-body #slMasterUOM').val(luom)
  
  if(lproductid == null || lproductid == '')
 			{
 				$("#txtProductID").focus(); 
 				$("#txtProductID").click();
 			}
 		else
 			$("#txtQty").focus();
})
</script>

<script>
function FuncPassString(lParamPlantID,lParamPlantName,lParamProductID,lParamProductName){
	if(lParamPlantID)
		{
			$("#txtPlantID").val(lParamPlantID);
			document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
		}
	
	if(lParamProductID)
		{
			$("#txtProductID").val(lParamProductID);
			document.getElementById("lblProductName").innerHTML = '(' + lParamProductName + ')';
			
			FuncShowUOM();
		}
}
</script>

<script>
function FuncClear(){
	$('#mrkProductID').hide();
	$('#mrkDate').hide();
	$('#mrkPlantID').hide();
	$('#mrkQty').hide();
	$('#mrkUOM').hide();
	
	$('#dvProductID').removeClass('has-error');
	$('#dvDate').removeClass('has-error');
	$('#dvPlantID').removeClass('has-error');
	$('#dvQty').removeClass('has-error');
	$('#dvUOM').removeClass('has-error');
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#txtProductID').val('');
	//$('#txtDate').val('');
	$('#txtPlantID').val('');
	$('#txtQty').val('');
	$('#slMasterUOM').val('');
		
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Bill Of Materials";	
	document.getElementById("lblProductName").innerHTML = null;
	document.getElementById("lblPlantName").innerHTML = null;

	FuncClear();
	$('#txtProductID').prop('disabled', false);
	$('#txtDate').prop('disabled', false);
	$('#txtPlantID').prop('disabled', false);
}

function FuncButtonUpdate(lProductID) {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById("lblTitleModal").innerHTML = 'Edit Bill Of Materials';

	FuncClear();
	$('#txtProductID').prop('disabled', true);
	$('#txtDate').prop('disabled', true);
	$('#txtPlantID').prop('disabled', true);
	
	$('#txtProductID').val(lProductID);
	FuncShowUOM();
}

function FuncValShowUOM(){	
	var txtProductID = document.getElementById('txtProductID').value;

	FuncClear();

	if(!txtProductID.match(/\S/)) {    	
	    	$('#txtProductID').focus();
	    	$('#dvProductID').addClass('has-error');
	    	$('#mrkProductID').show();
	    	
	    	alert("Fill Product ID First ...!!!");
	        return false;
	    } 
	    return true;
	}

function FuncValEmptyInput(lParambtn) {
	var txtProductID = document.getElementById('txtProductID').value;
	var txtDate = document.getElementById('txtDate').value;
	var txtPlantID = document.getElementById('txtPlantID').value;
	var txtQty = document.getElementById('txtQty').value;
	var slMasterUOM = document.getElementById('slMasterUOM').value;
	
	var dvProductID = document.getElementsByClassName('dvProductID');
	var dvDate = document.getElementsByClassName('dvDate');
	var dvPlantID = document.getElementsByClassName('dvPlantID');
	var dvQty = document.getElementsByClassName('dvQty');
	var dvUOM = document.getElementsByClassName('dvUOM');

    if(!txtProductID.match(/\S/)) {
    	$("#txtProductID").focus();
    	$('#dvProductID').addClass('has-error');
    	$('#mrkProductID').show();
        return false;
    } 
    
    if(!txtDate.match(/\S/)) {    	
    	$('#txtDate').focus();
    	$('#dvDate').addClass('has-error');
    	$('#mrkDate').show();
        return false;
    }
    
    if(!txtPlantID.match(/\S/)) {
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
        return false;
    } 
    
    if(!txtQty.match(/\S/)) {
    	$('#txtQty').focus();
    	$('#dvQty').addClass('has-error');
    	$('#mrkQty').show();
        return false;
    }
    
    if(!slMasterUOM.match(/\S/)) {
    	$('#slMasterUOM').focus();
    	$('#dvUOM').addClass('has-error');
    	$('#mrkUOM').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/MasterBom',	
        type:'POST',
        data:{"key":lParambtn,"txtProductID":txtProductID,"txtDate":txtDate,"txtPlantID":txtPlantID,"txtQty":txtQty,"slMasterUOM":slMasterUOM},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertBOM')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan produk bom";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtProductID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateBOM')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui produk bom";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtQty").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedConversion')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				//$("#txtQty").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/MasterBom';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<!-- get uom from product id -->
 	<script>
 	function FuncShowUOM(){	
 		$('#slMasterUOM').find('option').remove();
 		
		var paramproductid = document.getElementById('txtProductID').value;
 
		         $.ajax({
		              url: '${pageContext.request.contextPath}/getuom',
		              type: 'POST',
		              data: {productid : paramproductid},
		              dataType: 'json'
		         })
		         .done(function(data){
		              console.log(data);
		
		             //for json data type
		             for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slMasterUOM').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
		        
		    }
		             
		         })
		         .fail(function(){
		        	 console.log('Service call failed!');
		         });
 	}
 </script>

<!-- get bom detail from bom header key -->
<!-- <script> -->
<!-- // $(document).ready(function(){ -->

<!-- //     $(document).on('click', '#btnBOMDetail', function(e){ -->
    	
<!-- //      e.preventDefault(); -->
  
<!-- //      var productid = $(this).data('lproductid'); // get product id of clicked row -->
<!-- //      var date = $(this).data('ldate'); // get date data of clicked row -->
<!-- //      var plantid = $(this).data('lplantid'); // get plant id of clicked row -->
     
<!-- //      document.getElementById("bomheaderdesc").innerHTML = "Product ID : " + productid + "<br/>" + "Valid Date : " + date + "<br/>" + "Plant ID : " + plantid; -->
  
<!-- //      $('#dynamic-content').html(''); // leave this div blank -->
<!-- // //      $('#modal-loader').show();      // load ajax loader on button click -->
 
<!-- //      $.ajax({ -->
<%-- //           url: '${pageContext.request.contextPath}/getbomdetail', --%>
<!-- //           type: 'POST', -->
<!-- //           data: {"productid":productid,"date":date,"plantid":plantid}, -->
<!-- //           dataType: 'html' -->
<!-- //      }) -->
<!-- //      .done(function(data){ -->
<!-- //           console.log(data);  -->
<!-- //           $('#dynamic-content').html(''); // blank before load. -->
<!-- //           $('#dynamic-content').html(data); // load here -->
<!-- // //           $('#modal-loader').hide(); // hide loader   -->
<!-- //      }) -->
<!-- //      .fail(function(){ -->
<!-- //           $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...'); -->
<!-- // //           $('#modal-loader').hide(); -->
<!-- //      }); -->

<!-- //     }); -->
<!-- // }); -->
<!-- </script> -->

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtPlantID:focus').length) {
    	$('#txtPlantID').click();
    }
});
</script>

</body>

</html>