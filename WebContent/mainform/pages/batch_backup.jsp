<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Batch</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="Batch" name="Batch" action = "${pageContext.request.contextPath}/Batch" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Batch <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
<%-- 							<c:if test="${condition == 'SuccessInsert'}"> --%>
<!-- 	    					  <div class="alert alert-success alert-dismissible"> -->
<!--           				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--              				  	<h4><i class="icon fa fa-check"></i> Success</h4> -->
<!--                 			  	Insert new batch succed. -->
<!--               				</div> -->
<%-- 	      					</c:if> --%>
	      					
<%-- 	      					<c:if test="${condition == 'FailedInsert'}"> --%>
<!-- 	      						<div class="alert alert-danger alert-dismissible"> -->
<!--                 				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--                 				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<!--                 				Insert new batch fail. Please contact admin for help. -->
<!--               				</div> -->
<%-- 	     					</c:if> --%>
	     					
<%-- 	     					<c:if test="${condition == 'SuccessUpdate'}"> --%>
<!-- 	    					  <div id="alrUpdate" class="alert alert-success alert-dismissible"> -->
<!--           				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--              				  	<h4><i class="icon fa fa-check"></i> Success</h4> -->
<!--                 			  	Update batch succed. -->
<!--               				</div> -->
<%-- 	      					</c:if> --%>
	      					
<%-- 	      					<c:if test="${condition == 'FailedUpdate'}"> --%>
<!-- 	      						<div class="alert alert-danger alert-dismissible"> -->
<!--                 				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--                 				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<!--                 				Update batch fail. Please contact admin for help. -->
<!--               				</div> -->
<%-- 	     					</c:if>  --%>
	     					
<%-- 	     					<c:if test="${condition == 'SuccessDelete'}"> --%>
<!-- 	    					  <div class="alert alert-success alert-dismissible"> -->
<!--           				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--              				  	<h4><i class="icon fa fa-check"></i> Success</h4> -->
<!--                 			  	Delete batch succed. -->
<!--               				</div> -->
<%-- 	      					</c:if> --%>
	      					
<%-- 	      					<c:if test="${condition == 'empty_condition'}"> --%>
<!-- 	    					  <script>$('#alrUpdate').hide();</script> -->
<%-- 	      					</c:if> --%>

<%-- 	      					<c:if test="${condition == 'FailedDelete'}"> --%>
<!-- 	      						<div class="alert alert-danger alert-dismissible"> -->
<!--                 				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--                 				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<!--                 				Delete batch fail. Please contact admin for help. -->
<!--               				</div> -->
<%-- 	     					</c:if>    --%>
	      				
<!-- 							<button id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br> -->
							<table id="tb_itemlib" class="table table-bordered table-striped">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Batch No</th>
										<th>Product ID</th>
										<th>Packing No</th>
										<th>GRDate</th>
										<th>Expired Date</th>
										<th>Vendor Batch No</th>
										<th>Vendor ID</th>
<!-- 										<th style="width:20px;"></th> -->
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listbatch}" var="batch">
										<tr>
											<td><c:out value="${batch.batch_No}" /></td>
											<td><c:out value="${batch.productID}" /></td>
											<td><c:out value="${batch.packing_No}" /></td>
											<td><c:out value="${batch.GRDate}" /></td>
											<td><c:out value="${batch.expired_Date}" /></td>
											<td><c:out value="${batch.vendor_Batch_No}" /></td>
											<td><c:out value="${batch.vendorID}" /></td>
<!-- 											<td><button id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal"  -->
<!-- 														onclick="FuncButtonUpdate()" -->
<!-- 														data-target="#ModalUpdateInsert"  -->
<%-- 														data-lproductid='<c:out value="${batch.productID}" />' --%>
<%-- 														data-lbatchno='<c:out value="${batch.batch_No}" />' --%>
<%-- 														data-lpackingno='<c:out value="${batch.packing_No}" />' --%>
<%-- 														data-lgrdate='<c:out value="${batch.gRDate}" />' --%>
<%-- 														data-lexpireddate='<c:out value="${batch.expired_Date}" />' --%>
<%-- 														data-lverdorbatchno='<c:out value="${batch.vendor_Batch_No}" />' --%>
<%-- 														data-lvendorid='<c:out value="${batch.vendorID}" />'> --%>
<!-- 														<i class="fa fa-edit"></i></button>  -->
<!-- <!-- 												<button id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" onclick="FuncButtonDelete()" data-toggle="modal" data-target="#ModalDelete"  -->
<%-- <%-- 												data-lproductid='<c:out value="${batch.productID}" />' --%>
<%-- <%-- 												data-lbatchno='<c:out value="${batch.batch_No}" />'> --%>
												
<!-- <!-- 												<i class="fa fa-trash"></i> -->
<!-- <!-- 												</button> -->
<!-- 											</td> -->
										</tr>

									</c:forEach>
									<!--modal update & Insert -->
									
<!-- 									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"> -->
<!--  										<div class="modal-dialog" role="document"> -->
<!--     										<div class="modal-content"> -->
<!--       											<div class="modal-header"> -->
<!--         											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
<!--         											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	 -->
        												
        											
<!--       											</div> -->
<!-- 	      								<div class="modal-body"> -->
	        								
<!-- 	          								<div id="dvProductID" class="form-group"> -->
<!-- 	            								<label for="recipient-name" class="control-label">Product ID</label><label id="mrkProductID" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtProductID" name="txtProductID"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvBatchNo" class="form-group"> -->
<!-- 	            								<label for="message-text" class="control-label">Batch No</label><label id="mrkBatchNo" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtBatchNo" name="txtBatchNo"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvPackingNo" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">Packing No</label><label id="mrkPackingNo" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtPackingNo" name="txtPackingNo"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvGRDate" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">GRDate</label><label id="mrkGRDate" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtGRDate" name="txtGRDate"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvExpiredDate" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">Expired Date</label><label id="mrkExpiredDate" for="recipient-name" class="control-label"> <small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtExpiredDate" name=""txtExpiredDate""> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvVendorBatchNo" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">Vendor Batch No</label><label id="mrkVendorBatchNo" for="recipient-name" class="control-label"> <small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtVendorBatchNo" name="txtVendorBatchNo"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvVendorId" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">Vendor ID</label><label id="mrkVendorId" for="recipient-name" class="control-label"> <small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtVendorId" name="txtVendorId"> -->
<!-- 	          								</div> -->
	        								
<!--       								</div> -->
      								
<!--       								<div class="modal-footer"> -->
<!--         									<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button> -->
<!--         									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button> -->
<!--         									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        								
<!--       								</div> -->
<!--     										</div> -->
<!--   										</div> -->
<!-- 									</div> -->
									
									<!--modal Delete -->
<!--        									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> -->
<!--           									<div class="modal-dialog" role="document"> -->
<!--            										<div class="modal-content"> -->
<!--               										<div class="modal-header">            											 -->
<!--                 										<button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
<!--                   										<span aria-hidden="true">&times;</span></button> -->
<!--                 										<h4 class="modal-title">Alert Delete Batch</h4> -->
<!--               										</div> -->
<!--               									<div class="modal-body"> -->
<!--               									<input type="hidden" id="temp_txtProductID" name="temp_txtProductID"  /> -->
<!--               									<input type="hidden" id="temp_txtBatchNo" name="temp_txtBatchNo"  /> -->
<!--                	 									<p>Are You Sure Delete This Batch ?</p> -->
<!--               									</div> -->
<!-- 								              <div class="modal-footer"> -->
<!-- 								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button> -->
<!-- 								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button> -->
<!-- 								              </div> -->
<!-- 								            </div> -->
<!-- 								            /.modal-content -->
<!-- 								          </div> -->
<!-- 								          /.modal-dialog -->
<!-- 								        </div> -->
								        <!-- /.modal -->							
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 		$(function () {
  			$("#tb_itemlib").DataTable();
  			$('#mmaster').addClass('active');
  	  		$('#mbatch').addClass('active');
  		});
	</script>
	
	<script>
// 	$('#ModalDelete').on('show.bs.modal', function (event) {
// 		var button = $(event.relatedTarget);
// 		var lProductId = button.data('lproductid');
// 		var lBatchno = button.data('lbatchno');
// 		$("#temp_txtProductID").val(lProductId);
// 		$("#temp_txtBatchNo").val(lBatchno);
// 	})
	</script>
	
<script>
// 	$('#ModalUpdateInsert').on('show.bs.modal', function (event) {
//  		var button = $(event.relatedTarget);
//  		var lProductId = button.data('lproductid');
//  		var lBatchNo = button.data('lbatchno');
//  		var lPackingNo = button.data('lpackingno');
//  		var lGRDate = button.data('lgrdate');
//  		var lVendorid = button.data('lvendorid');
//  		var lVerdorbatchno = button.data('lverdorbatchno');
//  		var lExpireddate = button.data('lexpireddate');
//  		var modal = $(this);
 		
//  		modal.find(".modal-body #txtProductID").val(lProductId);
//  		modal.find(".modal-body #txtBatchNo").val(lBatchNo);
//  		modal.find(".modal-body #txtPackingNo").val(lPackingNo)
//  		modal.find(".modal-body #txtGRDate").val(lGRDate);
//  		modal.find(".modal-body #txtExpiredDate").val(lExpireddate);
//  		modal.find(".modal-body #txtVendorBatchNo").val(lVerdorbatchno);
//  		modal.find(".modal-body #txtVendorId").val(lExpireddate);
// 	})
</script>

<script>
// var mrkVendorID = document.getElementById('mrkVendorID').value;
// var mrkVendorName = document.getElementById('mrkVendorName').value;
// var mrkVendorAddress = document.getElementById('mrkVendorAddress').value;
// var mrkPhone = document.getElementById('mrkPhone').value;
// var mrkPIC = document.getElementById('mrkPIC').value;
// var btnSave = document.getElementById('btnSave').value;
// var btnUpdate = document.getElementById('btnUpdate').value;

// function FuncClear(){
// 	$('#mrkProductID').hide();
// 	$('#mrkBatchNo').hide();
// 	$('#mrkPackingNo').hide();
// 	$('#mrkGRDate').hide();
// 	$('#mrkExpiredDate').hide();
// 	$('#mrkVendorBatchNo').hide();
// 	$('#mrkVendorId').hide();
	
// 	$('#dvProductID').removeClass('has-error');
// 	$('#dvBatchNo').removeClass('has-error');
// 	$('#dvPackingNo').removeClass('has-error');
// 	$('#dvGRDate').removeClass('has-error');
// 	$('#dvExpiredDate').removeClass('has-error');
// 	$('#dvVendorBatchNo').removeClass('has-error');
// 	$('#dvVendorId').removeClass('has-error');
// }

// function FuncButtonNew() {
// 	$('#btnSave').show();
// 	$('#btnUpdate').hide();
// 	document.getElementById('lblTitleModal').innerHTML = "Add Batch";	

// 	FuncClear();
// 	$('#txtVendorID').prop('disabled', false);
// }

// function FuncButtonUpdate() {
// 	$('#btnSave').hide();
// 	$('#btnUpdate').show();
// 	document.getElementById('lblTitleModal').innerHTML = 'Edit Batch';

// 	FuncClear();
// 	$('#txtVendorID').prop('disabled', true);
// }

// function FuncValEmptyInput(lParambtn) {
// 	var txtProductID = document.getElementById('txtProductID').value;
// 	var txtBatchNo = document.getElementById('txtBatchNo').value;
// 	var txtPackingNo = document.getElementById('txtPackingNo').value;
// 	var txtGRDate = document.getElementById('txtGRDate').value;
// 	var txtExpiredDate = document.getElementById('txtExpiredDate').value;
// 	var txtVendorBatchNo = document.getElementById('txtVendorBatchNo').value;
// 	var txtVendorId = document.getElementById('txtVendorId').value;
	

// 	var dvProductID = document.getElementsByClassName('dvProductID');
// 	var dvBatchNo = document.getElementsByClassName('dvBatchNo');
// 	var dvPackingNo = document.getElementsByClassName('dvPackingNo');
// 	var dvGRDate = document.getElementsByClassName('dvGRDate');
// 	var dvExpiredDate = document.getElementsByClassName('dvExpiredDate');
// 	var dvVendorBatchNo = document.getElementsByClassName('dvVendorBatchNo');
// 	var dvVendorId = document.getElementsByClassName('dvVendorId');
	
// 	FuncClear();
	
// 	if(lParambtn == 'save'){
// 		$('#txtProductID').prop('disabled', false);
// 		$('#txtBatchNo').prop('disabled', false);
// 	}
// 	else{
// 		$('#txtProductID').prop('disabled', true);
// 		$('#txtBatchNo').prop('disabled', true);
// 	}
	
//     if(!txtProductID.match(/\S/)) {
//     	$("#txtProductID").focus();
//     	$('#dvProductID').addClass('has-error');
//     	$('#mrkProductID').show();
//         return false;
//     } 
    
//     if(!txtBatchNo.match(/\S/)) {    	
//     	$('#txtBatchNo').focus();
//     	$('#dvBatchNo').addClass('has-error');
//     	$('#mrkBatchNo').show();
//         return false;
//     } 
    
//     if(!txtPackingNo.match(/\S/)) {
//     	$('#txtPackingNo').focus();
//     	$('#dvPackingNo').addClass('has-error');
//     	$('#mrkPackingNo').show();
//         return false;
//     } 
	
//     if(!txtGRDate.match(/\S/)) {
//     	$('#txtGRDate').focus();
//     	$('#dvGRDate').addClass('has-error');
//     	$('#mrkGRDate').show();
//         return false;
//     } 
    
//     if(!txtExpiredDate.match(/\S/)) {
//     	$('#txtExpiredDate').focus();
//     	$('#dvExpiredDate').addClass('has-error');
//     	$('#mrkExpiredDate').show();
//         return false;
//     } 
    
//     if(!txtVendorBatchNo.match(/\S/)) {
//     	$('#txtVendorBatchNo').focus();
//     	$('#dvVendorBatchNo').addClass('has-error');
//     	$('#mrkVendorBatchNo').show();
//         return false;
//     }
    
//     if(!txtVendorId.match(/\S/)) {
//     	$('#txtVendorId').focus();
//     	$('#dvVendorId').addClass('has-error');
//     	$('#mrkVendorId').show();
//         return false;
//     } 
    
//     jQuery.ajax({
//         url:'${pageContext.request.contextPath}/Batch',	
//         type:'POST',
//         data:{"key":lParambtn,"txtProductID":txtProductID,"txtBatchNo":txtBatchNo,"txtPackingNo":txtPackingNo,"txtGRDate":txtGRDate,"txtExpiredDate":txtExpiredDate,"txtVendorBatchNo":txtVendorBatchNo,"txtVendorId":txtVendorId},
//         dataType : 'text',
//         success:function(data, textStatus, jqXHR){
//         	var url = '${pageContext.request.contextPath}/Batch';  
//         	$(location).attr('href', url);
//         },
//         error:function(data, textStatus, jqXHR){
//             console.log('Service call failed!');
//         }
//     });
//     return true;
    
// }
</script>


</body>
</html>