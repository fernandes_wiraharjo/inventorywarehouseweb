<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form id="getCancelTransactionDetail" name="getCancelTransactionDetail" action = "${pageContext.request.contextPath}/getcanceltransactiondetail" method="post">

<table id="tb_cancel_transaction_detail" class="table table-bordered table-hover">
    <thead style="background-color: #d2d6de;">
    <tr>
    <th>Doc No</th>
	<th>Line</th>
	<th>Product ID</th>
	<th>Qty UOM</th>
	<th>UOM</th>
	<th>Qty Base UOM</th>
	<th>Base UOM</th>
	<th>Batch No</th>
	<th>Packing No</th>
   </tr>
   </thead>
     
     <tbody>
     
     <c:forEach items="${listCancelTransactionDetail}" var ="canceltransactiondetail">
       <tr>
       <td><c:out value="${canceltransactiondetail.docNumber}" /></td>
	<td><c:out value="${canceltransactiondetail.docLine}" /></td>
	<td><c:out value="${canceltransactiondetail.productID}" /></td>
	<td><c:out value="${canceltransactiondetail.qtyUOM}" /></td>
	<td><c:out value="${canceltransactiondetail.UOM}" /></td>
	<td><c:out value="${canceltransactiondetail.qtyBaseUOM}" /></td>
	<td><c:out value="${canceltransactiondetail.baseUOM}" /></td>
	<td><c:out value="${canceltransactiondetail.batch_No}" /></td>
	<td><c:out value="${canceltransactiondetail.packing_No}" /></td>
     </tr>
     		
     </c:forEach>
     
     </tbody>
     </table>

</form>

<script>
 		$(function () {
  	  		$("#tb_cancel_transaction_detail").DataTable();
  		});
	</script>