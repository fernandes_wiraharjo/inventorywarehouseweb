<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Storage Bin</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="formstoragebin" name="formstoragebin" action = "${pageContext.request.contextPath}/storagebin" method="post">
		<input type="hidden" name="temp_string" value="" />
	
		<div class="wrapper">	
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					Storage Bin
				</h1>
				</section>
	
				<!-- Main content -->
				<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<c:if test="${condition == 'SuccessInsertStorageBin'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menambahkan storage bin.
	           						</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessUpdateStorageBin'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses memperbaharui storage bin.
	              					</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessDeleteStorageBin'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menghapus storage bin.
	              					</div>
		      					</c:if>
		      					
		      					<c:if test="${condition == 'FailedDeleteStorageBin'}">
		      						<div class="alert alert-danger alert-dismissible">
		                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
		                				Gagal menghapus storage bin. <c:out value="${conditionDescription}"/>.
	              					</div>
		     					</c:if>
								
								<!--modal update & Insert -->
								<div class="modal fade bs-example-modal-lg" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
										
	  										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				     						</div>
				     					
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
											</div>
		     								<div class="modal-body">
		         								<div id="dvPlantID" class="form-group col-xs-3">
		           								<label for="lblPlantID" class="control-label">Plant ID</label><label id="mrkPlantID" for="formrkPlantID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" 
		           								data-target="#ModalGetPlantID">
		         								</div>
		         								<div id="dvWarehouseID" class="form-group col-xs-3">
		           								<label for="lblWarehouseID" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="formrkWarehouseID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" 
		           								data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
		         								</div>
		         								<div id="dvStorageTypeID" class="form-group col-xs-3">
		           								<label class="control-label">Storage Type</label><label id="mrkStorageTypeID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblStorageTypeName" name="lblStorageTypeName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtStorageTypeID" name="txtStorageTypeID" data-toggle="modal" 
		           								data-target="#ModalGetStorageTypeID" onfocus="FuncValPlantWarehouse()">
		         								</div>
		         								<div id="dvStorageBinID" class="form-group col-xs-3">
		           								<label class="control-label">Storage Bin ID</label><label id="mrkStorageBinID" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageBinID" name="txtStorageBinID">
		         								</div>
		         								<div class="form-group col-xs-12">
		         								<button type="button" class="btn btn-default" id="btnRefresh" name="btnRefresh"><i class="fa fa-refresh"> Refresh</i></button>
												<!-- onclick="FuncRefresh()" -->
		         								</div>
		         								
		         								<div class="form-group col-xs-12">
			         								<!-- Storage bin detail panel -->
			          								<label for="recipient-name" class="control-label">Storage Bin Detail</label>
					   								<div class="panel panel-primary" id="StorageBinDetailPanel">
					   								<div class="panel-body fixed-panel">
			         								
			         								<div id="dvStorageSectionID" class="form-group col-xs-6">
			           								<label class="control-label">Storage Section ID</label><label id="mrkStorageSectionID" class="control-label"><small>*</small></label>	
			           								<small><label id="lblStorageSectionName" name="lblStorageSectionName" class="control-label"></label></small>
			           								<input type="text" class="form-control" id="txtStorageSectionID" name="txtStorageSectionID" data-toggle="modal" 
			           								data-target="#ModalGetStorageSectionID" onfocus="FuncValPlantWarehouseStorType()">
			         								</div>
			         								<div id="dvStorageBinTypeID" class="form-group col-xs-6">
			           								<label class="control-label">Storage Bin Type ID</label><label id="mrkStorageBinTypeID" class="control-label"><small>*</small></label>	
			           								<small><label id="lblStorageBinTypeName" name="lblStorageBinTypeName" class="control-label"></label></small>
			           								<input type="text" class="form-control" id="txtStorageBinTypeID" name="txtStorageBinTypeID" data-toggle="modal" 
			           								data-target="#ModalGetStorageBinTypeID" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div id="dvTotalCapacity" class="form-group col-xs-4">
			           								<label class="control-label">Total Capacity</label><label id="mrkTotalCapacity" class="control-label"><small>*</small></label>	
			           								<input type="number" class="form-control" id="txtTotalCapacity" name="txtTotalCapacity">
			         								</div>
			         								<div id="dvCapacityUsed" class="form-group col-xs-4">
			           								<label class="control-label">Capacity Used</label><label id="mrkCapacityUsed" class="control-label"><small>*</small></label>	
			           								<input type="number" class="form-control" id="txtCapacityUsed" name="txtCapacityUsed">
			         								</div>
			         								<div id="dvUtilization" class="form-group col-xs-4">
			           								<label class="control-label">Utilization (%)</label><label id="mrkUtilization" class="control-label"><small>*</small></label>	
			           								<small><label id="lblUtilization" name="lblUtilization" class="control-label"></label></small>
			           								<input type="number" class="form-control" id="txtUtilization" name="txtUtilization">
			         								</div>
			         								<div id="dvNoOfQuants" class="form-group col-xs-6">
			           								<label class="control-label">No Of Quants</label><label id="mrkNoOfQuants" class="control-label"><small>*</small></label>	
			           								<input type="number" class="form-control" id="txtNoOfQuants" name="txtNoOfQuants">
			         								</div>
			         								<div id="dvNoStorUnits" class="form-group col-xs-6">
			           								<label class="control-label">No Stor Units</label><label id="mrkNoStorUnits" class="control-label"><small>*</small></label>	
			           								<input type="number" class="form-control" id="txtNoStorUnits" name="txtNoStorUnits">
			         								</div>
			         								
			         								</div>
													</div>
													<!-- /.Storage bin detail panel -->
												</div>
												
												<div class="form-group col-xs-12">
													<!-- Status -->
			          								<label for="recipient-name" class="control-label">Status</label>
					   								<div class="panel panel-primary" id="StatusPanel">
					   								<div class="panel-body fixed-panel">
					   								
					   								<div class="form-group col-xs-4">
			            								<label><input type="checkbox" id="cbPutawayBlock" name="cbPutawayBlock">Putaway Block</label>
													</div>
													<div class="form-group col-xs-4">
														<label><input type="checkbox" id="cbStockRemovalBlock" name="cbStockRemovalBlock">Stock Removal Block</label>
													</div>
													<div class="form-group col-xs-4">
														<label class="control-label">Block Reason</label>	
				           								<input type="text" class="form-control" id="txtBlockReasonID" name="txtBlockReasonID" data-toggle="modal" 
				           								data-target="#ModalGetBlockReasonID">
		          									</div>
					   								
					   								</div>
													</div>
													<!-- /.Status -->
												</div>
	          									
	          									<div class="row"></div>
	          									
	          									<!-- mysql data load stor bin detail will be load here -->                          
           										<div id="dynamic-storbin-detail">
           										</div>
		    								</div>
		    								<div class="modal-footer">
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
		      									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    								</div>
										</div>
									</div>
								</div>
								<!-- end of update insert modal -->
								
								<!--modal Delete -->
								<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
	 										<div class="modal-header">            											
	   										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	     										<span aria-hidden="true">&times;</span></button>
	   										<h4 class="modal-title">Alert Delete Storage Bin</h4>
	 										</div>
		 									<div class="modal-body">
			 									<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"  />
			 									<input type="hidden" id="temp_txtWarehouseID" name="temp_txtWarehouseID"  />
			 									<input type="hidden" id="temp_txtStorageBinID" name="temp_txtStorageBinID"  />
			 									<input type="hidden" id="temp_txtStorageTypeID" name="temp_txtStorageTypeID"  />
			  	 									<p>Are you sure to delete this storage bin ?</p>
		 									</div>
		 									<div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline">Delete</button>
							              	</div>
							            </div>
							            <!-- /.modal-content -->
									</div>
							          <!-- /.modal-dialog -->
						        </div>
						        <!-- /.modal -->   
							        
						        <!--modal show plant data -->
								<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
									<div class="modal-dialog" role="document">
	 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelPlantID">Data Plant</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<table id="tb_master_plant" class="table table-bordered table-hover">
											        <thead style="background-color: #d2d6de;">
											                <tr>
											                  <th>ID</th>
											                  <th>Name</th>
											                  <th>Description</th>
											                  <th style="width: 20px"></th>
											                </tr>
											        </thead>
											        <tbody>
												        <c:forEach items="${listPlant}" var ="plant">
													        <tr>
														        <td><c:out value="${plant.plantID}"/></td>
														        <td><c:out value="${plant.plantNm}"/></td>
														        <td><c:out value="${plant.desc}"/></td>
														        <td><button type="button" class="btn btn-primary"
														        			data-toggle="modal"
														        			onclick="FuncPassStringPlant('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
														        			data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
														        </td>
											        		</tr>
												        </c:forEach>
											        </tbody>
									        	</table>
		    								</div>
	   										<div class="modal-footer"></div>
	 										</div>
									</div>
								</div>
								<!-- /. end of modal show plant data -->
								
								<!--modal show warehouse data -->
								<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelWarehouseID">Data Warehouse</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content-warehouse">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show warehouse data -->
								
								<!--modal show storage type data -->
								<div class="modal fade" id="ModalGetStorageTypeID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageTypeID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelStorageTypeID">Data Storage Type</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load storage type by plant and warehouse id will be load here -->                          
           										<div id="dynamic-content-stortype">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show storage type data -->
								
								<!--modal show storage section data -->
								<div class="modal fade" id="ModalGetStorageSectionID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageSectionID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelStorageSectionID">Data Storage Section</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load storage section by plant,warehouse and storage type id will be load here -->                          
           										<div id="dynamic-content-storsection">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show storage section data -->
								
								<!--modal show storage bin type data -->
								<div class="modal fade" id="ModalGetStorageBinTypeID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageBinTypeID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelStorageBinTypeID">Data Storage Bin Type</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load storage section by plant,warehouse and storage type id will be load here -->                          
           										<div id="dynamic-content-storbintype">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show storage bin type data -->
								
								<!--modal show block reason data -->
								<div class="modal fade" id="ModalGetBlockReasonID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelBlockReasonID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelBlockReasonID">Data Block Reason</h4>	
	   										</div>
		     								<div class="modal-body">
<!-- 		         								<table id="tb_block_reason" class="table table-bordered table-hover"> -->
<!-- 											        <thead style="background-color: #d2d6de;"> -->
<!-- 											                <tr> -->
<!-- 											                  <th>ID</th> -->
<!-- 											                  <th>Name</th> -->
<!-- 											                  <th style="width: 20px"></th> -->
<!-- 											                </tr> -->
<!-- 											        </thead> -->
<!-- 											        <tbody> -->
<%-- 												        <c:forEach items="${listBlockReason}" var ="reason"> --%>
<!-- 													        <tr> -->
<%-- 														        <td><c:out value="${reas.plantID}"/></td> --%>
<%-- 														        <td><c:out value="${plant.plantNm}"/></td> --%>
<%-- 														        <td><c:out value="${plant.desc}"/></td> --%>
<!-- 														        <td><button type="button" class="btn btn-primary" -->
<!-- 														        			data-toggle="modal" -->
<%-- 														        			onclick="FuncPassStringPlant('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')" --%>
<!-- 														        			data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button> -->
<!-- 														        </td> -->
<!-- 											        		</tr> -->
<%-- 												        </c:forEach> --%>
<!-- 											        </tbody> -->
<!-- 									        	</table> -->
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show block reason data -->
								
								<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
								<table id="tb_storagebin" class="table table-bordered table-striped table-hover">
									<thead style="background-color: #d2d6de;">
										<tr>
											<th>Plant ID</th>
											<th>Warehouse ID</th>
											<th>Storage Type ID</th>
											<th>Storage Bin ID</th>
											<th style="width:60px;"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listStorageBin}" var="storagebin">
											<tr>
												<td><c:out value="${storagebin.plantID}" /></td>
												<td><c:out value="${storagebin.warehouseID}" /></td>
												<td><c:out value="${storagebin.storageTypeID}" /></td>
												<td><c:out value="${storagebin.storageBinID}" /></td>
												<td><button <c:out value="${buttonstatus}"/>
															id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
															onclick="FuncButtonUpdate()"
															data-target="#ModalUpdateInsert" 
															data-lplantid='<c:out value="${storagebin.plantID}" />'
															data-lplantname='<c:out value="${storagebin.plantName}" />'
															data-lwarehouseid='<c:out value="${storagebin.warehouseID}" />'
															data-lwarehousename='<c:out value="${storagebin.warehouseName}" />'
															data-lstoragetypeid='<c:out value="${storagebin.storageTypeID}" />'
															data-lstoragetypename='<c:out value="${storagebin.storageTypeName}" />'
															data-lstoragebin='<c:out value="${storagebin.storageBinID}" />'
															data-lstoragesectionid='<c:out value="${storagebin.storageSectionID}" />'
															data-lstoragesectionname='<c:out value="${storagebin.storageSectionName}" />'
															data-lstoragebintypeid='<c:out value="${storagebin.storageBinTypeID}" />'
															data-lstoragebintypename='<c:out value="${storagebin.storageBinTypeName}" />'
															data-ltotalcapacity='<c:out value="${storagebin.totalCapacity}" />'
															data-lcapacityused='<c:out value="${storagebin.capacityUsed}" />'
															data-lutilization='<c:out value="${storagebin.utilization}" />'
															data-lnoofquants='<c:out value="${storagebin.noOfQuantity}" />'
															data-lnostorunits='<c:out value="${storagebin.noStorageUnits}" />'
															data-lputawayblock='<c:out value="${storagebin.putawayBlock}" />'
															data-lstockremovalblock='<c:out value="${storagebin.stockRemovalBlock}" />'
															data-lblockreasonid='<c:out value="${storagebin.blockReasonID}" />'
															>
															<i class="fa fa-edit"></i></button> 
													<button <c:out value="${buttonstatus}"/>
															id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" 
															data-toggle="modal" 
															data-target="#ModalDelete"
															data-lplantid='<c:out value="${storagebin.plantID}" />' 
															data-lwarehouseid='<c:out value="${storagebin.warehouseID}" />'
															data-lstoragebin='<c:out value="${storagebin.storageBinID}" />'
															data-lstoragetypeid='<c:out value="${storagebin.storageTypeID}" />'
															>
													<i class="fa fa-trash"></i>
													</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row --> 
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
	
			<%@ include file="/mainform/pages/master_footer.jsp"%>
	
		</div>
		<!-- ./wrapper -->
	</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
	 	$(function () {
	 		$("#tb_storagebin").DataTable();
	  		$("#tb_master_plant").DataTable();
	  		$('#M002').addClass('active');
	  		$('#M052').addClass('active');
	  		
	  	//shortcut for button 'new'
		    Mousetrap.bind('n', function() {
		    	FuncButtonNew(),
		    	$('#ModalUpdateInsert').modal('show')
		    	});
  		});

		$('#ModalDelete').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var lPlantID = button.data('lplantid');
			var lWarehouseID = button.data('lwarehouseid');
			var lStorageBinID = button.data('lstoragebin');
			var lStorageTypeID = button.data('lstoragetypeid');
			$("#temp_txtPlantID").val(lPlantID);
			$("#temp_txtWarehouseID").val(lWarehouseID);
			$("#temp_txtStorageBinID").val(lStorageBinID);
			$("#temp_txtStorageTypeID").val(lStorageTypeID);
		})

		$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
			FuncClear();
			
	 		var button = $(event.relatedTarget);
	 		
	 		var lPlantID = button.data('lplantid');
	 		var lPlantName = button.data('lplantname');
	 		var lWarehouseID = button.data('lwarehouseid');
	 		var lWarehouseName = button.data('lwarehousename');
	 		var lStorageTypeID = button.data('lstoragetypeid');
	 		var lStorageTypeName = button.data('lstoragetypename');
	 		var lStorageBinID = button.data('lstoragebin');
	 		var lStorageSectionID = button.data('lstoragesectionid');
	 		var lStorageSectionName = button.data('lstoragesectionname');
	 		var lStorageBinTypeID = button.data('lstoragebintypeid');
	 		var lStorageBinTypeName = button.data('lstoragebintypename');
	 		var lTotalCapacity = button.data('ltotalcapacity');
	 		var lCapacityUsed = button.data('lcapacityused');
	 		var lUtilization = button.data('lutilization');
	 		var lNoOfQuants = button.data('lnoofquants');
	 		var lNoStorUnits = button.data('lnostorunits');
	 		var lPutawayBlock = button.data('lputawayblock');
	 		var lStockRemovalBlock = button.data('lstockremovalblock');
	 		var lBlockReasonID = button.data('lblockreasonid');
	 		
	 		var modal = $(this);
	 		
	 		modal.find(".modal-body #txtPlantID").val(lPlantID);
	 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
	 		modal.find(".modal-body #txtStorageTypeID").val(lStorageTypeID);
	 		modal.find(".modal-body #txtStorageBinID").val(lStorageBinID);
	 		modal.find(".modal-body #txtStorageSectionID").val(lStorageSectionID);
	 		modal.find(".modal-body #txtStorageBinTypeID").val(lStorageBinTypeID);
	 		modal.find(".modal-body #txtTotalCapacity").val(lTotalCapacity);
	 		modal.find(".modal-body #txtCapacityUsed").val(lCapacityUsed);
	 		modal.find(".modal-body #txtUtilization").val(lUtilization);
	 		modal.find(".modal-body #txtNoOfQuants").val(lNoOfQuants);
	 		modal.find(".modal-body #txtNoStorUnits").val(lNoStorUnits);
	 		modal.find(".modal-body #txtBlockReasonID").val(lBlockReasonID);
 			
	 		if(lPlantName != null)
	 			document.getElementById("lblPlantName").innerHTML = '(' + lPlantName + ')';
	 		if(lWarehouseName != null)
	 			document.getElementById("lblWarehouseName").innerHTML = '(' + lWarehouseName + ')';
	 		if(lStorageTypeName != null)
	 			document.getElementById("lblStorageTypeName").innerHTML = '(' + lStorageTypeName + ')';
	 		if(lStorageSectionName != null)
	 			document.getElementById("lblStorageSectionName").innerHTML = '(' + lStorageSectionName + ')';
	 		if(lStorageBinTypeName != null)
	 			document.getElementById("lblStorageBinTypeName").innerHTML = '(' + lStorageBinTypeName + ')';
	 			
	 		if(lPutawayBlock==true)
 				modal.find(".modal-body #cbPutawayBlock").prop("checked", true);
 			if(lStockRemovalBlock==true)
 				modal.find(".modal-body #cbStockRemovalBlock").prop("checked", true);
	 			
	 		
	 		if(lPlantID == null || lPlantID == '')
	 			{
	 				$("#txtPlantID").focus(); 
	 				$("#txtPlantID").click();
	 			}
	 		else
	 		{
	 			$("#txtStorageTypeID").focus();
	 		
	 			//get dynamic stor bin detail
			     $('#dynamic-storbin-detail').html(''); // leave this div blank
			     $.ajax({
			          url: '${pageContext.request.contextPath}/getStorBinDetail',
			          type: 'POST',
			          data: {"plantid":lPlantID,"warehouseid":lWarehouseID,"storagetypeid":lStorageTypeID,
			          		"storagebinid":lStorageBinID,"storagesectionid":lStorageSectionID},
			          dataType: 'html'
			     })
			     .done(function(data){
			          console.log(data); 
			          $('#dynamic-storbin-detail').html(''); // blank before load.
			          $('#dynamic-storbin-detail').html(data); // load here 
			     })
			     .fail(function(){
			          $('#dynamic-storbin-detail').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			     });
		 		}
		});
		
		function FuncClear(){
			$('#mrkPlantID').hide();
			$('#mrkWarehouseID').hide();
			$('#mrkStorageTypeID').hide();
			$('#mrkStorageBinID').hide();
			$('#mrkStorageSectionID').hide();
			$('#mrkStorageBinTypeID').hide();
			$('#mrkTotalCapacity').hide();
			$('#mrkCapacityUsed').hide();
			$('#mrkUtilization').hide();
			$('#mrkNoOfQuants').hide();
			$('#mrkNoStorUnits').hide();
			
			$('#dvPlantID').removeClass('has-error');
			$('#dvWarehouseID').removeClass('has-error');
			$('#dvStorageTypeID').removeClass('has-error');
			$('#dvStorageBinID').removeClass('has-error');
			$('#dvStorageSectionID').removeClass('has-error');
			$('#dvStorageBinTypeID').removeClass('has-error');
			$('#dvTotalCapacity').removeClass('has-error');
			$('#dvCapacityUsed').removeClass('has-error');
			$('#dvUtilization').removeClass('has-error');
			$('#dvNoOfQuants').removeClass('has-error');
			$('#dvNoStorUnits').removeClass('has-error');
			
			document.getElementById("lblPlantName").innerHTML = null;
			document.getElementById("lblWarehouseName").innerHTML = null;
			document.getElementById("lblStorageTypeName").innerHTML = null;
			document.getElementById("lblStorageSectionName").innerHTML = null;
			document.getElementById("lblStorageBinTypeName").innerHTML = null;
			
			$("#cbPutawayBlock").prop("checked", false);
			$("#cbStockRemovalBlock").prop("checked", false);
			
			$("#dvErrorAlert").hide();
		}
	
		function FuncButtonNew() {
			$('#btnSave').show();
			$('#btnUpdate').hide();
			document.getElementById("lblTitleModal").innerHTML = "Add Storage Bin";
		
			$('#txtPlantID').prop('disabled', false);
			$('#txtWarehouseID').prop('disabled', false);
			$('#txtStorageTypeID').prop('disabled', false);
			$('#txtStorageBinID').prop('disabled', false);
			 $('#dynamic-storbin-detail').html('');
		}
		
		function FuncButtonUpdate() {
			$('#btnSave').hide();
			$('#btnUpdate').show();
			document.getElementById("lblTitleModal").innerHTML = 'Edit Storage Bin';
		
			$('#txtPlantID').prop('disabled', true);
			$('#txtWarehouseID').prop('disabled', true);
			$('#txtStorageTypeID').prop('disabled', true);
			$('#txtStorageBinID').prop('disabled', true);
		}
		
		function FuncPassStringPlant(lParamPlantID,lParamPlantName){
			$("#txtPlantID").val(lParamPlantID);
			document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
			$('#mrkPlantID').hide();
			$('#dvPlantID').removeClass('has-error');
		}
		
		function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
			$("#txtWarehouseID").val(lParamWarehouseID);
			document.getElementById("lblWarehouseName").innerHTML = '(' + lParamWarehouseName + ')';
			$('#mrkWarehouseID').hide();
			$('#dvWarehouseID').removeClass('has-error');
		}
		
		function FuncPassDynamicStorageTypeData(lParamID,lType,lParamName){
			$("#txtStorageTypeID").val(lParamID);
			document.getElementById("lblStorageTypeName").innerHTML = '(' + lParamName + ')';
			$('#mrkStorageTypeID').hide();
			$('#dvStorageTypeID').removeClass('has-error');
		}
		
		function FuncPassStringStorageSection(lParamStorageSectionID,lParamStorageSectionName){
			$("#txtStorageSectionID").val(lParamStorageSectionID);
			document.getElementById("lblStorageSectionName").innerHTML = '(' + lParamStorageSectionName + ')';
			$('#mrkStorageSectionID').hide();
			$('#dvStorageSectionID').removeClass('has-error');
		}
		
		function FuncPassStringStorageBinType(lParamStorageBinTypeID,lParamStorageBinTypeName){
			$("#txtStorageBinTypeID").val(lParamStorageBinTypeID);
			document.getElementById("lblStorageBinTypeName").innerHTML = '(' + lParamStorageBinTypeName + ')';
			$('#mrkStorageBinTypeID').hide();
			$('#dvStorageBinTypeID').removeClass('has-error');
		}
		
		function FuncValPlant(){	
			var txtPlantID = document.getElementById('txtPlantID').value;
			
			if(!txtPlantID.match(/\S/)) {    	
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    } 
			
		    return true;	
		}
		
		function FuncValPlantWarehouse(){
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			
			if(!txtPlantID.match(/\S/)){
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    }else if(!txtWarehouseID.match(/\S/)){
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		    	
		    	alert("Fill Warehouse ID First ...!!!");
		        return false;
		    }
		    return true;
		}
		
		function FuncValPlantWarehouseStorType(){
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			var txtStorageTypeID = document.getElementById('txtStorageTypeID').value;
			
			if(!txtPlantID.match(/\S/)){
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    }else if(!txtWarehouseID.match(/\S/)){
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		    	
		    	alert("Fill Warehouse ID First ...!!!");
		        return false;
		    }
		    else if(!txtStorageTypeID.match(/\S/)){
		    	$('#txtStorageTypeID').focus();
		    	$('#dvStorageTypeID').addClass('has-error');
		    	$('#mrkStorageTypeID').show();
		    	
		    	alert("Fill Storage Type ID First ...!!!");
		        return false;
		    }
		    return true;
		}
		
		function FuncValEmptyInput(lParambtn) {
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			var txtStorageTypeID = document.getElementById('txtStorageTypeID').value;
			var txtStorageBinID = document.getElementById('txtStorageBinID').value;
			var txtStorageSectionID = document.getElementById('txtStorageSectionID').value;
			var txtStorageBinTypeID = document.getElementById('txtStorageBinTypeID').value;
			var txtTotalCapacity = document.getElementById('txtTotalCapacity').value;
			var txtUtilization = document.getElementById('txtUtilization').value;
			var txtCapacityUsed = document.getElementById('txtCapacityUsed').value;
			var txtNoOfQuants = document.getElementById('txtNoOfQuants').value;
			var txtNoStorUnits = document.getElementById('txtNoStorUnits').value;
			var txtBlockReasonID = document.getElementById('txtBlockReasonID').value;
			
			var cbPutawayBlock = false;
			var cbStockRemovalBlock = false;
			
		    if(!txtPlantID.match(/\S/)) {
		    	$("#txtPlantID").focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		        return false;
		    } 
		    
		    if(!txtWarehouseID.match(/\S/)) {    	
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		        return false;
		    }
		    
		    if(!txtStorageTypeID.match(/\S/)) {
		    	$('#txtStorageTypeID').focus();
		    	$('#dvStorageTypeID').addClass('has-error');
		    	$('#mrkStorageTypeID').show();
		        return false;
		    }
		    
		    if(!txtStorageBinID.match(/\S/)) {
		    	$('#txtStorageBinID').focus();
		    	$('#dvStorageBinID').addClass('has-error');
		    	$('#mrkStorageBinID').show();
		        return false;
		    }
		    
		    if(!txtStorageSectionID.match(/\S/)) {
		    	$('#txtStorageSectionID').focus();
		    	$('#dvStorageSectionID').addClass('has-error');
		    	$('#mrkStorageSectionID').show();
		        return false;
		    }
		    
		    if(!txtStorageBinTypeID.match(/\S/)) {
		    	$('#txtStorageBinTypeID').focus();
		    	$('#dvStorageBinTypeID').addClass('has-error');
		    	$('#mrkStorageBinTypeID').show();
		        return false;
		    }
		    
		    if(!txtTotalCapacity.match(/\S/)) {
		    	$('#txtTotalCapacity').focus();
		    	$('#dvTotalCapacity').addClass('has-error');
		    	$('#mrkTotalCapacity').show();
		        return false;
		    }
		    
		    if(!txtNoOfQuants.match(/\S/)) {
		    	$('#txtNoOfQuants').focus();
		    	$('#dvNoOfQuants').addClass('has-error');
		    	$('#mrkNoOfQuants').show();
		        return false;
		    }
		    
		    if(!txtNoStorUnits.match(/\S/)) {
		    	$('#txtNoStorUnits').focus();
		    	$('#dvNoStorUnits').addClass('has-error');
		    	$('#mrkNoStorUnits').show();
		        return false;
		    }
		    
		    if($("#cbPutawayBlock").prop('checked') == true)
		    	cbPutawayBlock = true;
		    if($("#cbStockRemovalBlock").prop('checked') == true)
		    	cbStockRemovalBlock = true;
		    
		    
		    jQuery.ajax({
		        url:'${pageContext.request.contextPath}/storagebin',	
		        type:'POST',
		        data:{"key":lParambtn,"txtPlantID":txtPlantID,"txtWarehouseID":txtWarehouseID,
		        	  "txtStorageTypeID":txtStorageTypeID,"txtStorageBinID":txtStorageBinID,"txtStorageSectionID":txtStorageSectionID,
		        	  "txtStorageBinTypeID":txtStorageBinTypeID,"txtTotalCapacity":txtTotalCapacity,"txtCapacityUsed":txtCapacityUsed,
		        	  "txtUtilization":txtUtilization,"txtNoOfQuants":txtNoOfQuants,"txtNoStorUnits":txtNoStorUnits,"cbPutawayBlock":cbPutawayBlock,
		        	  "cbStockRemovalBlock":cbStockRemovalBlock,"txtBlockReasonID":txtBlockReasonID},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	if(data.split("--")[0] == 'FailedInsertStorageBin')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan storage bin";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtPlantID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else if(data.split("--")[0] == 'FailedUpdateStorageBin')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui storage bin";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtStorageTypeID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else
		        	{
			        	var url = '${pageContext.request.contextPath}/storagebin';  
			        	$(location).attr('href', url);
		        	}
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    
		    return true;
		}
		
		//action when txtStorageBinID changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtStorageBinID', function(e){
		  
		     $('#mrkStorageBinID').hide();
			 $('#dvStorageBinID').removeClass('has-error');
		
		    });
		});
		
		//action when txtTotalCapacity changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtTotalCapacity', function(e){
		  
		     $('#mrkTotalCapacity').hide();
			 $('#dvTotalCapacity').removeClass('has-error');
		
		    });
		});
		
		//action when txtNoOfQuants changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtNoOfQuants', function(e){
		  
		     $('#mrkNoOfQuants').hide();
			 $('#dvNoOfQuants').removeClass('has-error');
		
		    });
		});
		
		//action when txtNoStorUnits changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtNoStorUnits', function(e){
		  
		     $('#mrkNoStorUnits').hide();
			 $('#dvNoStorUnits').removeClass('has-error');
		
		    });
		});
		
		//get warehouse from plant id
		$(document).ready(function(){
	
		    $(document).on('click', '#txtWarehouseID', function(e){
		  
		     e.preventDefault();
		  
			 var plantid = document.getElementById('txtPlantID').value;
		  
		     $('#dynamic-content-warehouse').html(''); // leave this div blank
			//$('#modal-loader').show();      // load ajax loader on button click
		 
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getwarehouse',
		          type: 'POST',
		          data: 'plantid='+plantid,
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-warehouse').html(''); // blank before load.
		          $('#dynamic-content-warehouse').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-warehouse').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage type from plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStorageTypeID', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-content-stortype').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragetype',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"storagetypeid":'',"type":''},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-stortype').html(''); // blank before load.
		          $('#dynamic-content-stortype').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-stortype').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage section from plant, warehouse, and storage type id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStorageSectionID', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
				var storagetypeid = document.getElementById('txtStorageTypeID').value;
		     $('#dynamic-content-storsection').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragesection',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"storagetypeid":storagetypeid},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storsection').html(''); // blank before load.
		          $('#dynamic-content-storsection').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storsection').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage bin type from plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStorageBinTypeID', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-content-storbintype').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragebintype',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storbintype').html(''); // blank before load.
		          $('#dynamic-content-storbintype').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storbintype').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtWarehouseID:focus').length) {
		    	$('#txtWarehouseID').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageTypeID:focus').length) {
		    	$('#txtStorageTypeID').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageSectionID:focus').length) {
		    	$('#txtStorageSectionID').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageBinTypeID:focus').length) {
		    	$('#txtStorageBinTypeID').click();
		    }
		});
	</script>
</body>
</html>