<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Movement Type</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="formmovementtype" name="formmovementtype" action = "${pageContext.request.contextPath}/movementtype" method="post">
		<input type="hidden" name="temp_string" value="" />
	
		<div class="wrapper">	
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					Movement Type
				</h1>
				</section>
	
				<!-- Main content -->
				<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<c:if test="${condition == 'SuccessInsertMovementType'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menambahkan movement type.
	           						</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessUpdateMovementType'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses memperbaharui movement type.
	              					</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessDeleteMovementType'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menghapus movement type.
	              					</div>
		      					</c:if>
		      					
		      					<c:if test="${condition == 'FailedDeleteMovementType'}">
		      						<div class="alert alert-danger alert-dismissible">
		                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
		                				Gagal menghapus movement type. <c:out value="${conditionDescription}"/>.
	              					</div>
		     					</c:if>
								
								<!--modal update & Insert -->
								<div class="modal fade bs-example-modal-lg" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
										
	  										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				     						</div>
				     					
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
											</div>
		     								<div class="modal-body">
		         								<div id="dvPlantID" class="form-group col-xs-4">
		           								<label for="lblPlantID" class="control-label">Plant ID</label><label id="mrkPlantID" for="formrkPlantID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" 
		           								data-target="#ModalGetPlantID">
		         								</div>
		         								<div id="dvWarehouseID" class="form-group col-xs-4">
		           								<label for="lblWarehouseID" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="formrkWarehouseID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" 
		           								data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
		         								</div>
		         								<div id="dvMovementID" class="form-group col-xs-4">
		           								<label for="lblMovementID" class="control-label">Movement Type</label><label id="mrkMovementID" for="formMovementID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblMovementName" name="lblMovementName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtMovementID" name="txtMovementID" data-toggle="modal" 
		           								data-target="#ModalGetMovementID">
		         								</div>
		         								
	         								<div class="form-group col-xs-12">
		         								<!-- Storage Src panel -->
		          								<label for="recipient-name" class="control-label">Storage Src</label>
				   								<div class="panel panel-primary" id="StorageSrcPanel">
				   								<div class="panel-body fixed-panel">
				   								
		         								<div id="dvStorageTypeIDSrc" class="form-group col-xs-3">
		           								<label class="control-label">Storage Type Src</label><label id="mrkStorageTypeIDSrc" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageTypeIDSrc" name="txtStorageTypeIDSrc" data-toggle="modal" 
		           								data-target="#ModalGetStorageTypeID" onfocus="FuncValPlantWarehouse()">
		         								</div>
		         								<div id="dvStorageBinIDSrc" class="form-group col-xs-3">
		           								<label class="control-label">Storage Bin Src</label><label id="mrkStorageBinIDSrc" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageBinIDSrc" name="txtStorageBinIDSrc" data-toggle="modal" 
		           								data-target="#ModalGetStorageBinID" onfocus="FuncValPlantWarehouseStorType('src')">
		         								</div>
		         								<div class="form-group col-xs-3">
		            							<label><input type="checkbox" id="cbFxdBnSrc" name="cbFxdBnSrc">FxcBn Src</label>
												</div>
												<div class="form-group col-xs-3">
		            							<label><input type="checkbox" id="cbScr_Src" name="cbScr_Src">Scr. Src</label>
												</div>
												
												</div>
												</div>
												<!-- /.Storage Src panel -->
											</div>
		         								
		         							<div class="form-group col-xs-12">
		         								<!-- Storage Src panel -->
		          								<label for="recipient-name" class="control-label">Storage Dest</label>
				   								<div class="panel panel-primary" id="StorageDestPanel">
				   								<div class="panel-body fixed-panel">
				   								
		         								<div id="dvStorageTypeIDDest" class="form-group col-xs-3">
		           								<label class="control-label">Storage Type Dest</label><label id="mrkStorageTypeIDDest" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageTypeIDDest" name="txtStorageTypeIDDest" data-toggle="modal" 
		           								data-target="#ModalGetStorageTypeID" onfocus="FuncValPlantWarehouse()">
		         								</div>
		         								<div id="dvStorageBinIDDest" class="form-group col-xs-3">
		           								<label class="control-label">Storage Bin Dest</label><label id="mrkStorageBinIDDest" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageBinIDDest" name="txtStorageBinIDDest" data-toggle="modal" 
		           								data-target="#ModalGetStorageBinID" onfocus="FuncValPlantWarehouseStorType('dest')">
		         								</div>
		         								<div class="form-group col-xs-3">
		            							<label><input type="checkbox" id="cbFxdBnDest" name="cbFxdBnDest">FxcBn Dest</label>
												</div>
												<div class="form-group col-xs-3">
		            							<label><input type="checkbox" id="cbScr_Dest" name="cbScr_Dest">Scr. Dest</label>
												</div>
												
												</div>
												</div>
												<!-- /.Storage Dest panel -->
											</div>
		         								
		         							<div class="form-group col-xs-12">
		         								<!-- Storage Ret panel -->
		          								<label for="recipient-name" class="control-label">Storage Ret</label>
				   								<div class="panel panel-primary" id="StorageRetPanel">
				   								<div class="panel-body fixed-panel">
				   								
		         								<div id="dvStorageTypeIDRet" class="form-group col-xs-3">
		           								<label class="control-label">Storage Type Ret</label><label id="mrkStorageTypeIDRet" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageTypeIDRet" name="txtStorageTypeIDDest" data-toggle="modal" 
		           								data-target="#ModalGetStorageTypeID" onfocus="FuncValPlantWarehouse()">
		         								</div>
		         								<div id="dvStorageBinIDRet" class="form-group col-xs-3">
		           								<label class="control-label">Storage Bin Ret</label><label id="mrkStorageBinIDRet" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageBinIDRet" name="txtStorageBinIDRet" data-toggle="modal" 
		           								data-target="#ModalGetStorageBinID" onfocus="FuncValPlantWarehouseStorType('ret')">
		         								</div>
		         								
		         								</div>
												</div>
												<!-- /.Storage Ret panel -->
											</div>
	          									
	          									<div class="row"></div>
		    								</div>
		    								<div class="modal-footer">
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
		      									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    								</div>
										</div>
									</div>
								</div>
								<!-- end of update insert modal -->
								
								<!--modal Delete -->
								<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
	 										<div class="modal-header">            											
	   										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	     										<span aria-hidden="true">&times;</span></button>
	   										<h4 class="modal-title">Alert Delete Movement Type</h4>
	 										</div>
		 									<div class="modal-body">
			 									<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"  />
			 									<input type="hidden" id="temp_txtWarehouseID" name="temp_txtWarehouseID"  />
			 									<input type="hidden" id="temp_txtMovementID" name="temp_txtMovementID"  />
			  	 									<p>Are you sure to delete this movement type ?</p>
		 									</div>
		 									<div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline">Delete</button>
							              	</div>
							            </div>
							            <!-- /.modal-content -->
									</div>
							          <!-- /.modal-dialog -->
						        </div>
						        <!-- /.modal -->   
							        
						        <!--modal show plant data -->
								<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
									<div class="modal-dialog" role="document">
	 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelPlantID">Data Plant</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<table id="tb_master_plant" class="table table-bordered table-hover">
											        <thead style="background-color: #d2d6de;">
											                <tr>
											                  <th>ID</th>
											                  <th>Name</th>
											                  <th>Description</th>
											                  <th style="width: 20px"></th>
											                </tr>
											        </thead>
											        <tbody>
												        <c:forEach items="${listPlant}" var ="plant">
													        <tr>
														        <td><c:out value="${plant.plantID}"/></td>
														        <td><c:out value="${plant.plantNm}"/></td>
														        <td><c:out value="${plant.desc}"/></td>
														        <td><button type="button" class="btn btn-primary"
														        			data-toggle="modal"
														        			onclick="FuncPassStringPlant('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
														        			data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
														        </td>
											        		</tr>
												        </c:forEach>
											        </tbody>
									        	</table>
		    								</div>
	   										<div class="modal-footer"></div>
	 										</div>
									</div>
								</div>
								<!-- /. end of modal show plant data -->
								
								<!--modal show warehouse data -->
								<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelWarehouseID">Data Warehouse</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content-warehouse">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show warehouse data -->
								
								<!--modal show reference movement type data -->
								<div class="modal fade" id="ModalGetMovementID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelMovementID">
									<div class="modal-dialog" role="document">
	 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelPlantID">Data Reference Movement Type</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<table id="tb_reference_movementtype" class="table table-bordered table-hover">
											        <thead style="background-color: #d2d6de;">
											                <tr>
											                  <th>ID</th>
											                  <th>Type</th>
											                  <th>Description</th>
											                  <th style="width: 20px"></th>
											                </tr>
											        </thead>
											        <tbody>
												        <c:forEach items="${listRefMovementType}" var ="refmovementtype">
													        <tr>
														        <td><c:out value="${refmovementtype.movementID}"/></td>
														        <td><c:out value="${refmovementtype.movementType}"/></td>
														        <td><c:out value="${refmovementtype.movementDescription}"/></td>
														        <td><button type="button" class="btn btn-primary"
														        			data-toggle="modal"
														        			onclick="FuncPassMovementID('<c:out value="${refmovementtype.movementID}"/>','<c:out value="${refmovementtype.movementDescription}"/>')"
														        			data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
														        </td>
											        		</tr>
												        </c:forEach>
											        </tbody>
									        	</table>
		    								</div>
	   										<div class="modal-footer"></div>
	 										</div>
									</div>
								</div>
								<!-- /. end of modal show reference movement type data -->
								
								<!--modal show storage type data -->
								<div class="modal fade" id="ModalGetStorageTypeID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageTypeID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelStorageTypeID">Data Storage Type</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load storage type by plant and warehouse id will be load here -->                          
           										<div id="dynamic-content-stortype">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show storage type data -->
								
								<!--modal show storage bin data -->
								<div class="modal fade" id="ModalGetStorageBinID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageBinID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelStorageTypeID">Data Storage Bin</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load storage bin by plant,warehouse and storage type id will be load here -->                          
           										<div id="dynamic-content-storbin">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show storage bin data -->
								
								<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
								<table id="tb_movementtype" class="table table-bordered table-striped table-hover">
									<thead style="background-color: #d2d6de;">
										<tr>
											<th>Plant ID</th>
											<th>Warehouse ID</th>
											<th>Movement ID</th>
											<th style="width:60px;"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listMovementType}" var="movementtype">
											<tr>
												<td><c:out value="${movementtype.plantID}" /></td>
												<td><c:out value="${movementtype.warehouseID}" /></td>
												<td><c:out value="${movementtype.movementID}" /></td>
												<td><button <c:out value="${buttonstatus}"/>
															id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
															onclick="FuncButtonUpdate()"
															data-target="#ModalUpdateInsert" 
															data-lplantid='<c:out value="${movementtype.plantID}" />'
															data-lplantname='<c:out value="${movementtype.plantName}" />'
															data-lwarehouseid='<c:out value="${movementtype.warehouseID}" />'
															data-lwarehousename='<c:out value="${movementtype.warehouseName}" />'
															data-lmovementid='<c:out value="${movementtype.movementID}" />'
															data-lmovementname='<c:out value="${movementtype.movementName}" />'
															data-lstoragetypeidsrc='<c:out value="${movementtype.storageTypeSource}" />'
															data-lstoragetypeiddest='<c:out value="${movementtype.storageTypeDestination}" />'
															data-lstoragetypeidret='<c:out value="${movementtype.storageTypeReturn}" />'
															data-lstoragebinidsrc='<c:out value="${movementtype.storageBinSource}" />'
															data-lstoragebiniddest='<c:out value="${movementtype.storageBinDestination}" />'
															data-lstoragebinidret='<c:out value="${movementtype.storageBinReturn}" />'
															data-lfxdbnsrc='<c:out value="${movementtype.fxdBinSource}" />'
															data-lfxdbndest='<c:out value="${movementtype.fxdBinDestination}" />'
															data-lscrsrc='<c:out value="${movementtype.scrSource}" />'
															data-lscrdest='<c:out value="${movementtype.scrDestination}" />'
															>
															<i class="fa fa-edit"></i></button> 
													<button <c:out value="${buttonstatus}"/>
															id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" 
															data-toggle="modal" 
															data-target="#ModalDelete"
															data-lplantid='<c:out value="${movementtype.plantID}" />' 
															data-lwarehouseid='<c:out value="${movementtype.warehouseID}" />'
															data-lmovementid='<c:out value="${movementtype.movementID}" />'
															>
													<i class="fa fa-trash"></i>
													</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row --> 
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
	
			<%@ include file="/mainform/pages/master_footer.jsp"%>
	
		</div>
		<!-- ./wrapper -->
	</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
	 	$(function () {
	 		$("#tb_reference_movementtype").DataTable();
	  		$("#tb_master_plant").DataTable();
	  		$("#tb_movementtype").DataTable();
	  		$('#M057').addClass('active');
	  		$('#M061').addClass('active');
	  		
	  	//shortcut for button 'new'
		    Mousetrap.bind('n', function() {
		    	FuncButtonNew(),
		    	$('#ModalUpdateInsert').modal('show')
		    	});
  		});

		$('#ModalDelete').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var lPlantID = button.data('lplantid');
			var lWarehouseID = button.data('lwarehouseid');
			var lMovementID = button.data('lmovementid');
			$("#temp_txtPlantID").val(lPlantID);
			$("#temp_txtWarehouseID").val(lWarehouseID);
			$("#temp_txtMovementID").val(lMovementID);
		})

		$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
			FuncClear();
			
	 		var button = $(event.relatedTarget);
	 		
	 		var lPlantID = button.data('lplantid');
	 		var lPlantName = button.data('lplantname');
	 		var lWarehouseID = button.data('lwarehouseid');
	 		var lWarehouseName = button.data('lwarehousename');
	 		var lMovementID = button.data('lmovementid');
	 		var lMovementName = button.data('lmovementname');
	 		var lStorageTypeIDSrc = button.data('lstoragetypeidsrc');
	 		var lStorageTypeIDDest = button.data('lstoragetypeiddest');
	 		var lStorageTypeIDRet = button.data('lstoragetypeidret');
	 		var lStorageBinIDSrc = button.data('lstoragebinidsrc');
	 		var lStorageBinIDDest = button.data('lstoragebiniddest');
	 		var lStorageBinIDRet = button.data('lstoragebinidret');
	 		var lFxdBnSrc = button.data('lfxdbnsrc');
	 		var lFxdBnDest = button.data('lfxdbndest');
	 		var lScr_Src = button.data('lscrsrc');
	 		var lScr_Dest = button.data('lscrdest');
	 		
	 		var modal = $(this);
	 		
	 		modal.find(".modal-body #txtPlantID").val(lPlantID);
	 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
	 		modal.find(".modal-body #txtMovementID").val(lMovementID);
	 		modal.find(".modal-body #txtStorageTypeIDSrc").val(lStorageTypeIDSrc);
	 		modal.find(".modal-body #txtStorageTypeIDDest").val(lStorageTypeIDDest);
	 		modal.find(".modal-body #txtStorageTypeIDRet").val(lStorageTypeIDRet);
	 		modal.find(".modal-body #txtStorageBinIDSrc").val(lStorageBinIDSrc);
	 		modal.find(".modal-body #txtStorageBinIDDest").val(lStorageBinIDDest);
	 		modal.find(".modal-body #txtStorageBinIDRet").val(lStorageBinIDRet);
	 		modal.find(".modal-body #cbFxdBnSrc").val(lFxdBnSrc);
	 		modal.find(".modal-body #cbFxdBnDest").val(lFxdBnDest);
	 		modal.find(".modal-body #cbScr_Src").val(lScr_Src);
	 		modal.find(".modal-body #cbScr_Dest").val(lScr_Dest);
 			
	 		if(lPlantName != null)
	 			document.getElementById("lblPlantName").innerHTML = '(' + lPlantName + ')';
	 		if(lWarehouseName != null)
	 			document.getElementById("lblWarehouseName").innerHTML = '(' + lWarehouseName + ')';
	 		if(lMovementName != null)
	 			document.getElementById("lblMovementName").innerHTML = '(' + lMovementName + ')';
	 		
	 		if(lFxdBnSrc==true)
 				modal.find(".modal-body #cbFxdBnSrc").prop("checked", true);
 			if(lFxdBnDest==true)
 				modal.find(".modal-body #cbFxdBnDest").prop("checked", true);
	 		if(lScr_Src==true)
 				modal.find(".modal-body #cbScr_Src").prop("checked", true);
 			if(lScr_Dest==true)
 				modal.find(".modal-body #cbScr_Dest").prop("checked", true);
	 			
	 		
	 		if(lPlantID == null || lPlantID == '')
	 			{
	 				$("#txtPlantID").focus(); 
	 				$("#txtPlantID").click();
	 			}
	 		else
	 			$("#txtStorageTypeIDSrc").focus();
		});
		
		function FuncClear(){
			$('#mrkPlantID').hide();
			$('#mrkWarehouseID').hide();
			$('#mrkMovementID').hide();
			$('#mrkStorageTypeIDSrc').hide();
			$('#mrkStorageTypeIDDest').hide();
			$('#mrkStorageTypeIDRet').hide();
			$('#mrkStorageBinIDSrc').hide();
			$('#mrkStorageBinIDDest').hide();
			$('#mrkStorageBinIDRet').hide();
			
			$('#dvPlantID').removeClass('has-error');
			$('#dvWarehouseID').removeClass('has-error');
			$('#dvMovementID').removeClass('has-error');
			$('#dvStorageTypeIDSrc').removeClass('has-error');
			$('#dvStorageTypeIDDest').removeClass('has-error');
			$('#dvStorageTypeIDRet').removeClass('has-error');
			$('#dvStorageBinIDSrc').removeClass('has-error');
			$('#dvStorageBinIDDest').removeClass('has-error');
			$('#dvStorageBinIDRet').removeClass('has-error');
			
			document.getElementById("lblPlantName").innerHTML = null;
			document.getElementById("lblWarehouseName").innerHTML = null;
			document.getElementById("lblMovementName").innerHTML = null;
			
			$("#cbFxdBnSrc").prop("checked", false);
			$("#cbFxdBnDest").prop("checked", false);
			$("#cbScr_Src").prop("checked", false);
			$("#cbScr_Dest").prop("checked", false);
			
			$("#dvErrorAlert").hide();
		}
	
		function FuncButtonNew() {
			$('#btnSave').show();
			$('#btnUpdate').hide();
			document.getElementById("lblTitleModal").innerHTML = "Add Movement Type";
		
			$('#txtPlantID').prop('disabled', false);
			$('#txtWarehouseID').prop('disabled', false);
			$('#txtMovementID').prop('disabled', false);
		}
		
		function FuncButtonUpdate() {
			$('#btnSave').hide();
			$('#btnUpdate').show();
			document.getElementById("lblTitleModal").innerHTML = 'Edit Movement Type';
		
			$('#txtPlantID').prop('disabled', true);
			$('#txtWarehouseID').prop('disabled', true);
			$('#txtMovementID').prop('disabled', true);
		}
		
		function FuncPassStringPlant(lParamPlantID,lParamPlantName){
			$("#txtPlantID").val(lParamPlantID);
			document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
			$('#mrkPlantID').hide();
			$('#dvPlantID').removeClass('has-error');
		}
		
		function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
			$("#txtWarehouseID").val(lParamWarehouseID);
			document.getElementById("lblWarehouseName").innerHTML = '(' + lParamWarehouseName + ')';
			$('#mrkWarehouseID').hide();
			$('#dvWarehouseID').removeClass('has-error');
		}
		
		function FuncPassMovementID(lParamMovementID,lParamMovementName){
			$("#txtMovementID").val(lParamMovementID);
			document.getElementById("lblMovementName").innerHTML = '(' + lParamMovementName + ')';
			$('#mrkMovementID').hide();
			$('#dvMovementID').removeClass('has-error');
		}
		
		function FuncPassDynamicStorageTypeData(lParamID,lType,lParamName){
			if(lType=='src'){
				$("#txtStorageTypeIDSrc").val(lParamID);
				$('#mrkStorageTypeIDSrc').hide();
				$('#dvStorageTypeIDSrc').removeClass('has-error');
			}
			else if(lType=='dest'){
				$("#txtStorageTypeIDDest").val(lParamID);
				$('#mrkStorageTypeIDDest').hide();
				$('#dvStorageTypeIDDest').removeClass('has-error');
			}
			else if(lType=='ret'){
				$("#txtStorageTypeIDRet").val(lParamID);
				$('#mrkStorageTypeIDRet').hide();
				$('#dvStorageTypeIDRet').removeClass('has-error');
			}
		}
		
		function FuncPassDynamicStorageBinData(lParamID,lType){
			if(lType=='src'){
				$("#txtStorageBinIDSrc").val(lParamID);
				$('#mrkStorageBinIDSrc').hide();
				$('#dvStorageBinIDSrc').removeClass('has-error');
			}
			else if(lType=='dest'){
				$("#txtStorageBinIDDest").val(lParamID);
				$('#mrkStorageBinIDDest').hide();
				$('#dvStorageBinIDDest').removeClass('has-error');
			}
			else if(lType=='ret'){
				$("#txtStorageBinIDRet").val(lParamID);
				$('#mrkStorageBinIDRet').hide();
				$('#dvStorageBinIDRet').removeClass('has-error');
			}
		}
		
		function FuncValPlant(){	
			var txtPlantID = document.getElementById('txtPlantID').value;
			
			if(!txtPlantID.match(/\S/)) {    	
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    } 
			
		    return true;	
		}
		
		function FuncValPlantWarehouse(){
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			
			if(!txtPlantID.match(/\S/)){
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    }else if(!txtWarehouseID.match(/\S/)){
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		    	
		    	alert("Fill Warehouse ID First ...!!!");
		        return false;
		    }
		    return true;
		}
		
		function FuncValPlantWarehouseStorType(lParam){
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			if(!txtPlantID.match(/\S/)){
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    }else if(!txtWarehouseID.match(/\S/)){
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		    	
		    	alert("Fill Warehouse ID First ...!!!");
		        return false;
		    }
			
			if(lParam=='src')
			{
				var txtStorageTypeIDSrc = document.getElementById('txtStorageTypeIDSrc').value;
				if(!txtStorageTypeIDSrc.match(/\S/)){
		    	$('#txtStorageTypeIDSrc').focus();
		    	$('#dvStorageTypeIDSrc').addClass('has-error');
		    	$('#mrkStorageTypeIDSrc').show();
		    	
		    	alert("Fill Storage Type ID Src First ...!!!");
		        return false;
		    	}
			}
			else if(lParam=='dest')
			{
				var txtStorageTypeIDDest = document.getElementById('txtStorageTypeIDDest').value;
				if(!txtStorageTypeIDDest.match(/\S/)){
		    	$('#txtStorageTypeIDDest').focus();
		    	$('#dvStorageTypeIDDest').addClass('has-error');
		    	$('#mrkStorageTypeIDDest').show();
		    	
		    	alert("Fill Storage Type ID Dest First ...!!!");
		        return false;
		    	}
			}
			else if(lParam=='ret')
			{
				var txtStorageTypeIDRet = document.getElementById('txtStorageTypeIDRet').value;
				if(!txtStorageTypeIDRet.match(/\S/)){
		    	$('#txtStorageTypeIDRet').focus();
		    	$('#dvStorageTypeIDRet').addClass('has-error');
		    	$('#mrkStorageTypeIDRet').show();
		    	
		    	alert("Fill Storage Type ID Ret First ...!!!");
		        return false;
		    	}
			}
			
		    return true;
		}
		
		function FuncValEmptyInput(lParambtn) {
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			var txtMovementID = document.getElementById('txtMovementID').value;
			var txtStorageTypeIDSrc = document.getElementById('txtStorageTypeIDSrc').value;
			var txtStorageTypeIDDest = document.getElementById('txtStorageTypeIDDest').value;
			var txtStorageTypeIDRet = document.getElementById('txtStorageTypeIDRet').value;
			var txtStorageBinIDSrc = document.getElementById('txtStorageBinIDSrc').value;
			var txtStorageBinIDDest = document.getElementById('txtStorageBinIDDest').value;
			var txtStorageBinIDRet = document.getElementById('txtStorageBinIDRet').value;
			
			var cbFxdBnSrc = false;
			var cbFxdBnDest = false;
			var cbScr_Src = false;
			var cbScr_Dest = false;
			
		    if(!txtPlantID.match(/\S/)) {
		    	$("#txtPlantID").focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		        return false;
		    } 
		    
		    if(!txtWarehouseID.match(/\S/)) {    	
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		        return false;
		    }
		    
		     if(!txtMovementID.match(/\S/)) {    	
		    	$('#txtMovementID').focus();
		    	$('#dvMovementID').addClass('has-error');
		    	$('#mrkMovementID').show();
		        return false;
		    }
		    
// 		    if(!txtStorageTypeIDSrc.match(/\S/)) {
// 		    	$('#txtStorageTypeIDSrc').focus();
// 		    	$('#dvStorageTypeIDSrc').addClass('has-error');
// 		    	$('#mrkStorageTypeIDSrc').show();
// 		        return false;
// 		    }
		    
// 		    if(!txtStorageTypeIDDest.match(/\S/)) {
// 		    	$('#txtStorageTypeIDDest').focus();
// 		    	$('#dvStorageTypeIDDest').addClass('has-error');
// 		    	$('#mrkStorageTypeIDDest').show();
// 		        return false;
// 		    }
		    
// 		    if(!txtStorageTypeIDRet.match(/\S/)) {
// 		    	$('#txtStorageTypeIDRet').focus();
// 		    	$('#dvStorageTypeIDRet').addClass('has-error');
// 		    	$('#mrkStorageTypeIDRet').show();
// 		        return false;
// 		    }
		    
// 		    if(!txtStorageBinIDSrc.match(/\S/)) {
// 		    	$('#txtStorageBinIDSrc').focus();
// 		    	$('#dvStorageBinIDSrc').addClass('has-error');
// 		    	$('#mrkStorageBinIDSrc').show();
// 		        return false;
// 		    }
		    
// 		     if(!txtStorageBinIDDest.match(/\S/)) {
// 		    	$('#txtStorageBinIDDest').focus();
// 		    	$('#dvStorageBinIDDest').addClass('has-error');
// 		    	$('#mrkStorageBinIDDest').show();
// 		        return false;
// 		    }
		    
// 		     if(!txtStorageBinIDRet.match(/\S/)) {
// 		    	$('#txtStorageBinIDRet').focus();
// 		    	$('#dvStorageBinIDRet').addClass('has-error');
// 		    	$('#mrkStorageBinIDRet').show();
// 		        return false;
// 		    }
		    
		    if($("#cbFxdBnSrc").prop('checked') == true)
		    	cbFxdBnSrc = true;
		    if($("#cbFxdBnDest").prop('checked') == true)
		    	cbFxdBnDest = true;
		    if($("#cbScr_Src").prop('checked') == true)
		    	cbScr_Src = true;
		    if($("#cbScr_Dest").prop('checked') == true)
		    	cbScr_Dest = true;
		    
		    
		    jQuery.ajax({
		        url:'${pageContext.request.contextPath}/movementtype',	
		        type:'POST',
		        data:{"key":lParambtn,"txtPlantID":txtPlantID,"txtWarehouseID":txtWarehouseID,"txtMovementID":txtMovementID,
		        	  "txtStorageTypeIDSrc":txtStorageTypeIDSrc,"txtStorageTypeIDDest":txtStorageTypeIDDest,"txtStorageTypeIDRet":txtStorageTypeIDRet,
		        	  "txtStorageBinIDSrc":txtStorageBinIDSrc,"txtStorageBinIDDest":txtStorageBinIDDest,"txtStorageBinIDRet":txtStorageBinIDRet,
		        	  "cbFxdBnSrc":cbFxdBnSrc,"cbFxdBnDest":cbFxdBnDest,"cbScr_Src":cbScr_Src,"cbScr_Dest":cbScr_Dest},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	if(data.split("--")[0] == 'FailedInsertMovementType')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan movement type";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtPlantID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else if(data.split("--")[0] == 'FailedUpdateMovementType')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui movement type";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtStorageTypeID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else
		        	{
			        	var url = '${pageContext.request.contextPath}/movementtype';  
			        	$(location).attr('href', url);
		        	}
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    
		    return true;
		}
		
		//get warehouse from plant id
		$(document).ready(function(){
	
		    $(document).on('click', '#txtWarehouseID', function(e){
		  
		     e.preventDefault();
		  
			 var plantid = document.getElementById('txtPlantID').value;
		  
		     $('#dynamic-content-warehouse').html(''); // leave this div blank
			//$('#modal-loader').show();      // load ajax loader on button click
		 
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getwarehouse',
		          type: 'POST',
		          data: 'plantid='+plantid,
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-warehouse').html(''); // blank before load.
		          $('#dynamic-content-warehouse').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-warehouse').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		function FuncGetStorageType(plantid,warehouseid,type){
			var listSeq = new Array();
			
			if(document.getElementById('txtStorageTypeIDSrc').value != "")
					listSeq.push("'" + document.getElementById('txtStorageTypeIDSrc').value + "'");
			if(document.getElementById('txtStorageTypeIDDest').value != "")
					listSeq.push("'" + document.getElementById('txtStorageTypeIDDest').value + "'");
			if(document.getElementById('txtStorageTypeIDRet').value != "")
					listSeq.push("'" + document.getElementById('txtStorageTypeIDRet').value + "'");
			
			 $('#dynamic-content-stortype').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragetype',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"storagetypeid":'',"type":type,"listSeq":listSeq},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-stortype').html(''); // blank before load.
		          $('#dynamic-content-stortype').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-stortype').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		}
		
		//get storage type from plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStorageTypeIDSrc', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var type = 'src';
				FuncGetStorageType(plantid,warehouseid,type);
		    });
		    
		    
		    $(document).on('click', '#txtStorageTypeIDDest', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var type = 'dest';
				FuncGetStorageType(plantid,warehouseid,type);
		    });
		    
		    
		    $(document).on('click', '#txtStorageTypeIDRet', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var type = 'ret';
				FuncGetStorageType(plantid,warehouseid,type);
		    });
		    
		});
		
		//get storage bin from plant, warehouse, and storage type id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStorageBinIDSrc', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
				var storagetypeid = document.getElementById('txtStorageTypeIDSrc').value;
		     $('#dynamic-content-storbin').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragebin',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"storagetypeid":storagetypeid,"type":'src'},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storbin').html(''); // blank before load.
		          $('#dynamic-content-storbin').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storbin').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		    });
		    
		    
		    $(document).on('click', '#txtStorageBinIDDest', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
				var storagetypeid = document.getElementById('txtStorageTypeIDDest').value;
		     $('#dynamic-content-storbin').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragebin',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"storagetypeid":storagetypeid,"type":'dest'},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storbin').html(''); // blank before load.
		          $('#dynamic-content-storbin').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storbin').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		    });
		    
		    
		    $(document).on('click', '#txtStorageBinIDRet', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
				var storagetypeid = document.getElementById('txtStorageTypeIDRet').value;
		     $('#dynamic-content-storbin').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragebin',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"storagetypeid":storagetypeid,"type":'ret'},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storbin').html(''); // blank before load.
		          $('#dynamic-content-storbin').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storbin').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		    });
		    
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtWarehouseID:focus').length) {
		    	$('#txtWarehouseID').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtMovementID:focus').length) {
		    	$('#txtMovementID').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageTypeIDSrc:focus').length) {
		    	$('#txtStorageTypeIDSrc').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageTypeIDDest:focus').length) {
		    	$('#txtStorageTypeIDDest').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageTypeIDRet:focus').length) {
		    	$('#txtStorageTypeIDRet').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageBinIDSrc:focus').length) {
		    	$('#txtStorageBinIDSrc').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageBinIDDest:focus').length) {
		    	$('#txtStorageBinIDDest').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageBinIDRet:focus').length) {
		    	$('#txtStorageBinIDRet').click();
		    }
		});
	</script>
</body>
</html>