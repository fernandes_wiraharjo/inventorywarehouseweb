<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_door" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>Plant ID</th>
			<th>Warehouse ID</th>
			<th>Door ID</th>
			<th>Door Name</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listDoor}" var ="door">
	  <tr>
	  <td><c:out value="${door.plantID}" /></td>
		<td><c:out value="${door.warehouseID}" /></td>
		<td><c:out value="${door.doorID}" /></td>
		<td><c:out value="${door.doorName}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassStringDoor('<c:out value="${door.doorID}"/>','<c:out value="${door.doorName}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_door").DataTable();
  	});
</script>