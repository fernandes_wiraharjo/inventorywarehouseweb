<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Storage Type Search</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="formstortypesearch" name="formstortypesearch" action = "${pageContext.request.contextPath}/stortypesearch" method="post">
		<input type="hidden" name="temp_string" value="" />
	
		<div class="wrapper">	
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					Storage Type Search
				</h1>
				</section>
	
				<!-- Main content -->
				<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<c:if test="${condition == 'SuccessInsertStorageTypeSearch'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menambahkan storage type search.
	           						</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessUpdateStorageTypeSearch'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses memperbaharui storage type search.
	              					</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessDeleteStorageTypeSearch'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menghapus storage type search.
	              					</div>
		      					</c:if>
		      					
		      					<c:if test="${condition == 'FailedDeleteStorageTypeSearch'}">
		      						<div class="alert alert-danger alert-dismissible">
		                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
		                				Gagal menghapus storage type search. <c:out value="${conditionDescription}"/>.
	              					</div>
		     					</c:if>
								
								<!--modal update & Insert -->
								<div class="modal fade bs-example-modal-lg" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
										
	  										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				     						</div>
				     					
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
											</div>
		     								<div class="modal-body">
		         								<div id="dvPlantID" class="form-group col-xs-3">
			           								<label for="lblPlantID" class="control-label">Plant ID</label><label id="mrkPlantID" for="formrkPlantID" class="control-label"><small>*</small></label>	
			           								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
			           								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" 
			           								data-target="#ModalGetPlantID">
		         								</div>
		         								<div id="dvWarehouseID" class="form-group col-xs-3">
			           								<label for="lblWarehouseID" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="formrkWarehouseID" class="control-label"><small>*</small></label>	
			           								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
			           								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" 
			           								data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
		         								</div>
		         								<div id="dvOperation" class="form-group col-xs-3">
			           								<label class="control-label">Operation</label><label id="mrkOperation" class="control-label"><small>*</small></label>	
			           								<select id="txtOperation" name="txtOperation" class="form-control">
				           								<option value="A">A - Stock Removal</option>
								                    	<option value="E">E - Putaway</option>
							                    	</select>
		         								</div>
		         								<div id="dvStorageTypeIndicatorID" class="form-group col-xs-3">
			           								<label for="lblStorageTypeIndicatorID" class="control-label">Storage Type Indicator</label><label id="mrkStorageTypeIndicatorID" for="formrkStorageTypeIndicatorID" class="control-label"><small>*</small></label>			           								
			           								<input type="text" class="form-control" id="txtStorageTypeIndicatorID" name="txtStorageTypeIndicatorID" data-toggle="modal" 
			           								data-target="#ModalGetStorageTypeIndicator" onfocus="FuncValPlantWarehouse()">
		         								</div>
		         								
		         								<div class="form-group col-xs-12">
			         								<!-- Sequence Panel -->
			          								<label for="recipient-name" class="control-label">Storage Type Sequence</label>
					   								<div class="panel panel-primary" id="StorageTypeSequencePanel">
					   								<div class="panel-body fixed-panel">
			         								
			         								<div id="dvSeq1" class="form-group col-xs-2">
			           								<label class="control-label">Seq 1</label><label id="mrkSeq1" class="control-label"><small>*</small></label>			           								
			           								<input type="text" class="form-control" id="txtSeq1" name="txtSeq1" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 2</label>		           								
			           								<input type="text" class="form-control" id="txtSeq2" name="txtSeq2" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 3</label>			           								
			           								<input type="text" class="form-control" id="txtSeq3" name="txtSeq1" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 4</label>			           								
			           								<input type="text" class="form-control" id="txtSeq4" name="txtSeq4" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 5</label>			           								
			           								<input type="text" class="form-control" id="txtSeq5" name="txtSeq5" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 6</label>			           								
			           								<input type="text" class="form-control" id="txtSeq6" name="txtSeq6" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 7</label>		           								
			           								<input type="text" class="form-control" id="txtSeq7" name="txtSeq7" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 8</label>			           								
			           								<input type="text" class="form-control" id="txtSeq8" name="txtSeq8" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 9</label>			           								
			           								<input type="text" class="form-control" id="txtSeq9" name="txtSeq9" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 10</label>			           								
			           								<input type="text" class="form-control" id="txtSeq10" name="txtSeq10" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 11</label>			           								
			           								<input type="text" class="form-control" id="txtSeq11" name="txtSeq11" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 12</label>			           								
			           								<input type="text" class="form-control" id="txtSeq12" name="txtSeq12" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 13</label>		           								
			           								<input type="text" class="form-control" id="txtSeq13" name="txtSeq13" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 14</label>			           								
			           								<input type="text" class="form-control" id="txtSeq14" name="txtSeq14" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 15</label>			           								
			           								<input type="text" class="form-control" id="txtSeq15" name="txtSeq15" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 16</label>			           								
			           								<input type="text" class="form-control" id="txtSeq16" name="txtSeq16" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 17</label>			           								
			           								<input type="text" class="form-control" id="txtSeq17" name="txtSeq17" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 18</label>			           								
			           								<input type="text" class="form-control" id="txtSeq18" name="txtSeq18" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 19</label>		           								
			           								<input type="text" class="form-control" id="txtSeq19" name="txtSeq19" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 20</label>			           								
			           								<input type="text" class="form-control" id="txtSeq20" name="txtSeq20" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 21</label>			           								
			           								<input type="text" class="form-control" id="txtSeq21" name="txtSeq21" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 22</label>			           								
			           								<input type="text" class="form-control" id="txtSeq22" name="txtSeq22" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 23</label>			           								
			           								<input type="text" class="form-control" id="txtSeq23" name="txtSeq23" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 24</label>			           								
			           								<input type="text" class="form-control" id="txtSeq24" name="txtSeq24" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								<div class="form-group col-xs-2">
			           								<label class="control-label">Seq 25</label>			           								
			           								<input type="text" class="form-control" id="txtSeq25" name="txtSeq25" data-toggle="modal" 
			           								data-target="#ModalGetStorageType" onfocus="FuncValPlantWarehouse()">
			         								</div>
			         								
			         								</div>
													</div>
													<!-- /.Sequence Panel -->
												</div>
	          									
	          									<div class="row"></div>
		    								</div>
		    								<div class="modal-footer">
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
		      									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    								</div>
										</div>
									</div>
								</div>
								<!-- end of update insert modal -->
								
								<!--modal Delete -->
								<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
	 										<div class="modal-header">            											
	   										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	     										<span aria-hidden="true">&times;</span></button>
	   										<h4 class="modal-title">Alert Delete Storage Type Search</h4>
	 										</div>
		 									<div class="modal-body">
			 									<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"  />
			 									<input type="hidden" id="temp_txtWarehouseID" name="temp_txtWarehouseID"  />
			 									<input type="hidden" id="temp_txtOperation" name="temp_txtOperation"  />
			 									<input type="hidden" id="temp_txtStorageTypeIndicatorID" name="temp_txtStorageTypeIndicatorID"  />
			  	 									<p>Are you sure to delete this storage type search ?</p>
		 									</div>
		 									<div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline">Delete</button>
							              	</div>
							            </div>
							            <!-- /.modal-content -->
									</div>
							          <!-- /.modal-dialog -->
						        </div>
						        <!-- /.modal -->   
							        
						        <!--modal show plant data -->
								<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
									<div class="modal-dialog" role="document">
	 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelPlantID">Data Plant</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<table id="tb_master_plant" class="table table-bordered table-hover">
											        <thead style="background-color: #d2d6de;">
											                <tr>
											                  <th>ID</th>
											                  <th>Name</th>
											                  <th>Description</th>
											                  <th style="width: 20px"></th>
											                </tr>
											        </thead>
											        <tbody>
												        <c:forEach items="${listPlant}" var ="plant">
													        <tr>
														        <td><c:out value="${plant.plantID}"/></td>
														        <td><c:out value="${plant.plantNm}"/></td>
														        <td><c:out value="${plant.desc}"/></td>
														        <td><button type="button" class="btn btn-primary"
														        			data-toggle="modal"
														        			onclick="FuncPassStringPlant('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
														        			data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
														        </td>
											        		</tr>
												        </c:forEach>
											        </tbody>
									        	</table>
		    								</div>
	   										<div class="modal-footer"></div>
	 										</div>
									</div>
								</div>
								<!-- /. end of modal show plant data -->
								
								<!--modal show warehouse data -->
								<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelWarehouseID">Data Warehouse</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content-warehouse">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show warehouse data -->
								
								<!-- modal show storage type indicator data -->
								<div class="modal fade" id="ModalGetStorageTypeIndicator" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageTypeIndicator">
								<div class="modal-dialog" role="document">
								  	 <div class="modal-content">
								    	 <div class="modal-header">
								      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								      	 <h4 class="modal-title" id="ModalLabelStorageTypeIndicator">Data Storage Type Indicator</h4>	
								    	 </div>
								     	 <div class="modal-body">
								       		<!-- mysql data load Storage Type Indicator by plantid and warehouseid will be load here -->                          
       										<div id="dynamic-content-storage-type-indicator">
       										</div>
								    	 </div>
								    	
								    	 <div class="modal-footer">
								      	
								    	 </div>
								  	 </div>
								</div>
								</div>
								<!-- /. end of modal storage type indicator data -->
								
								<!-- modal show storage type data -->
								<div class="modal fade" id="ModalGetStorageType" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageType">
								<div class="modal-dialog" role="document">
								  	 <div class="modal-content">
								    	 <div class="modal-header">
								      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								      	 <h4 class="modal-title" id="ModalLabelStorageType">Data Storage Type</h4>	
								    	 </div>
								     	 <div class="modal-body">
								       		<!-- mysql data load Storage Type by plantid and warehouseid will be load here -->                          
       										<div id="dynamic-content-storage-type">
       										</div>
								    	 </div>
								    	
								    	 <div class="modal-footer">
								      	
								    	 </div>
								  	 </div>
								</div>
								</div>
								<!-- /. end of modal storage type indicator -->
								
								
								
								<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
								<table id="tb_storagetype_search" class="table table-bordered table-striped table-hover">
									<thead style="background-color: #d2d6de;">
										<tr>
											<th></th>
											<th>Plant ID</th>
											<th>Warehouse ID</th>
											<th>Operation</th>
											<th>Stor. Type Ind. ID</th>
											<th>Stor. Type Ind. Name</th>
											<th>1</th>
											<th>2</th>
											<th>3</th>
											<th>4</th>
											<th>5</th>
											<th>6</th>
											<th>7</th>
											<th>8</th>
											<th>9</th>
											<th>10</th>
											<th>11</th>
											<th>12</th>
											<th>13</th>
											<th>14</th>
											<th>15</th>
											<th>16</th>
											<th>17</th>
											<th>18</th>
											<th>19</th>
											<th>20</th>
											<th>21</th>
											<th>22</th>
											<th>23</th>
											<th>24</th>
											<th>25</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listStorageTypeSearch}" var="stortypesearch">
											<tr>
												<td>
													<table>
														<td>
														<button <c:out value="${buttonstatus}"/>
																	id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
																	onclick="FuncButtonUpdate()"
																	data-target="#ModalUpdateInsert" 
																	data-lplantid='<c:out value="${stortypesearch.plantID}" />'
																	data-lplantname='<c:out value="${stortypesearch.plantName}" />'
																	data-lwarehouseid='<c:out value="${stortypesearch.warehouseID}" />'
																	data-lwarehousename='<c:out value="${stortypesearch.warehouseName}" />'
																	data-loperation='<c:out value="${stortypesearch.operation}" />'
																	data-lstoragetypeindicatorid='<c:out value="${stortypesearch.storageTypeIndicatorID}" />'
																	data-lseq1='<c:out value="${stortypesearch.storageType1}" />'
																	data-lseq2='<c:out value="${stortypesearch.storageType2}" />'
																	data-lseq3='<c:out value="${stortypesearch.storageType3}" />'
																	data-lseq4='<c:out value="${stortypesearch.storageType4}" />'
																	data-lseq5='<c:out value="${stortypesearch.storageType5}" />'
																	data-lseq6='<c:out value="${stortypesearch.storageType6}" />'
																	data-lseq7='<c:out value="${stortypesearch.storageType7}" />'
																	data-lseq8='<c:out value="${stortypesearch.storageType8}" />'
																	data-lseq9='<c:out value="${stortypesearch.storageType9}" />'
																	data-lseq10='<c:out value="${stortypesearch.storageType10}" />'
																	data-lseq11='<c:out value="${stortypesearch.storageType11}" />'
																	data-lseq12='<c:out value="${stortypesearch.storageType12}" />'
																	data-lseq13='<c:out value="${stortypesearch.storageType13}" />'
																	data-lseq14='<c:out value="${stortypesearch.storageType14}" />'
																	data-lseq15='<c:out value="${stortypesearch.storageType15}" />'
																	data-lseq16='<c:out value="${stortypesearch.storageType16}" />'
																	data-lseq17='<c:out value="${stortypesearch.storageType17}" />'
																	data-lseq18='<c:out value="${stortypesearch.storageType18}" />'
																	data-lseq19='<c:out value="${stortypesearch.storageType19}" />'
																	data-lseq20='<c:out value="${stortypesearch.storageType20}" />'
																	data-lseq21='<c:out value="${stortypesearch.storageType21}" />'
																	data-lseq22='<c:out value="${stortypesearch.storageType22}" />'
																	data-lseq23='<c:out value="${stortypesearch.storageType23}" />'
																	data-lseq24='<c:out value="${stortypesearch.storageType24}" />'
																	data-lseq25='<c:out value="${stortypesearch.storageType25}" />'
																	>
																	<i class="fa fa-edit"></i></button> 
														</td>
														<td>&nbsp</td>
														<td>
														<button <c:out value="${buttonstatus}"/>
																	id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" 
																	data-toggle="modal" 
																	data-target="#ModalDelete"
																	data-lplantid='<c:out value="${stortypesearch.plantID}" />' 
																	data-lwarehouseid='<c:out value="${stortypesearch.warehouseID}" />'
																	data-loperation='<c:out value="${stortypesearch.operation}" />'
																	data-lstoragetypeindicatorid='<c:out value="${stortypesearch.storageTypeIndicatorID}" />'
																	>
															<i class="fa fa-trash"></i>
															</button>
															</td>
													</table>
												</td>
												<td><c:out value="${stortypesearch.plantID}" /></td>
												<td><c:out value="${stortypesearch.warehouseID}" /></td>
												<td><c:out value="${stortypesearch.operation}" /></td>
												<td><c:out value="${stortypesearch.storageTypeIndicatorID}" /></td>
												<td><c:out value="${stortypesearch.storageTypeIndicatorName}" /></td>
												<td><c:out value="${stortypesearch.storageType1}" /></td>
												<td><c:out value="${stortypesearch.storageType2}" /></td>
												<td><c:out value="${stortypesearch.storageType3}" /></td>
												<td><c:out value="${stortypesearch.storageType4}" /></td>
												<td><c:out value="${stortypesearch.storageType5}" /></td>
												<td><c:out value="${stortypesearch.storageType6}" /></td>
												<td><c:out value="${stortypesearch.storageType7}" /></td>
												<td><c:out value="${stortypesearch.storageType8}" /></td>
												<td><c:out value="${stortypesearch.storageType9}" /></td>
												<td><c:out value="${stortypesearch.storageType10}" /></td>
												<td><c:out value="${stortypesearch.storageType11}" /></td>
												<td><c:out value="${stortypesearch.storageType12}" /></td>
												<td><c:out value="${stortypesearch.storageType13}" /></td>
												<td><c:out value="${stortypesearch.storageType14}" /></td>
												<td><c:out value="${stortypesearch.storageType15}" /></td>
												<td><c:out value="${stortypesearch.storageType16}" /></td>
												<td><c:out value="${stortypesearch.storageType17}" /></td>
												<td><c:out value="${stortypesearch.storageType18}" /></td>
												<td><c:out value="${stortypesearch.storageType19}" /></td>
												<td><c:out value="${stortypesearch.storageType20}" /></td>
												<td><c:out value="${stortypesearch.storageType21}" /></td>
												<td><c:out value="${stortypesearch.storageType22}" /></td>
												<td><c:out value="${stortypesearch.storageType23}" /></td>
												<td><c:out value="${stortypesearch.storageType24}" /></td>
												<td><c:out value="${stortypesearch.storageType25}" /></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row --> 
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
	
			<%@ include file="/mainform/pages/master_footer.jsp"%>
	
		</div>
		<!-- ./wrapper -->
	</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
	 	$(function () {
	 		$("#tb_storagetype_search").DataTable({
  		        "scrollX": true
  		    });
	  		$("#tb_master_plant").DataTable();
	  		$('#M057').addClass('active');
	  		$('#M058').addClass('active');
	  		
	  	//shortcut for button 'new'
		    Mousetrap.bind('n', function() {
		    	FuncButtonNew(),
		    	$('#ModalUpdateInsert').modal('show')
		    	});
  		});

		$('#ModalDelete').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var lPlantID = button.data('lplantid');
			var lWarehouseID = button.data('lwarehouseid');
			var lOperation = button.data('loperation');
			var lStorageTypeIndicatorID = button.data('lstoragetypeindicatorid');
			$("#temp_txtPlantID").val(lPlantID);
			$("#temp_txtWarehouseID").val(lWarehouseID);
			$("#temp_txtOperation").val(lOperation);
			$("#temp_txtStorageTypeIndicatorID").val(lStorageTypeIndicatorID);
		})

		$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
			FuncClear();
			
	 		var button = $(event.relatedTarget);
	 		
	 		var lPlantID = button.data('lplantid');
	 		var lPlantName = button.data('lplantname');
	 		var lWarehouseID = button.data('lwarehouseid');
	 		var lWarehouseName = button.data('lwarehousename');
	 		var lOperation = button.data('loperation');
	 		var lStorageTypeIndicatorID = button.data('lstoragetypeindicatorid');
	 		var lSeq1 = button.data('lseq1');
	 		var lSeq2 = button.data('lseq2');
	 		var lSeq3 = button.data('lseq3');
	 		var lSeq4 = button.data('lseq4');
	 		var lSeq5 = button.data('lseq5');
	 		var lSeq6 = button.data('lseq6');
	 		var lSeq7 = button.data('lseq7');
	 		var lSeq8 = button.data('lseq8');
	 		var lSeq9 = button.data('lseq9');
	 		var lSeq10 = button.data('lseq10');
	 		var lSeq11 = button.data('lseq11');
	 		var lSeq12 = button.data('lseq12');
	 		var lSeq13 = button.data('lseq13');
	 		var lSeq14 = button.data('lseq14');
	 		var lSeq15 = button.data('lseq15');
	 		var lSeq16 = button.data('lseq16');
	 		var lSeq17 = button.data('lseq17');
	 		var lSeq18 = button.data('lseq18');
	 		var lSeq19 = button.data('lseq19');
	 		var lSeq20 = button.data('lseq20');
	 		var lSeq21 = button.data('lseq21');
	 		var lSeq22 = button.data('lseq22');
	 		var lSeq23 = button.data('lseq23');
	 		var lSeq24 = button.data('lseq24');
	 		var lSeq25 = button.data('lseq25');
	 		
	 		var modal = $(this);
	 		
	 		modal.find(".modal-body #txtPlantID").val(lPlantID);
	 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
	 		modal.find(".modal-body #txtOperation").val(lOperation);
	 		modal.find(".modal-body #txtStorageTypeIndicatorID").val(lStorageTypeIndicatorID);
	 		modal.find(".modal-body #txtSeq1").val(lSeq1);
	 		modal.find(".modal-body #txtSeq2").val(lSeq2);
	 		modal.find(".modal-body #txtSeq3").val(lSeq3);
	 		modal.find(".modal-body #txtSeq4").val(lSeq4);
	 		modal.find(".modal-body #txtSeq5").val(lSeq5);
	 		modal.find(".modal-body #txtSeq6").val(lSeq6);
	 		modal.find(".modal-body #txtSeq7").val(lSeq7);
	 		modal.find(".modal-body #txtSeq8").val(lSeq8);
	 		modal.find(".modal-body #txtSeq9").val(lSeq9);
	 		modal.find(".modal-body #txtSeq10").val(lSeq10);
	 		modal.find(".modal-body #txtSeq11").val(lSeq11);
	 		modal.find(".modal-body #txtSeq12").val(lSeq12);
	 		modal.find(".modal-body #txtSeq13").val(lSeq13);
	 		modal.find(".modal-body #txtSeq14").val(lSeq14);
	 		modal.find(".modal-body #txtSeq15").val(lSeq15);
	 		modal.find(".modal-body #txtSeq16").val(lSeq16);
	 		modal.find(".modal-body #txtSeq17").val(lSeq17);
	 		modal.find(".modal-body #txtSeq18").val(lSeq18);
	 		modal.find(".modal-body #txtSeq19").val(lSeq19);
	 		modal.find(".modal-body #txtSeq20").val(lSeq20);
	 		modal.find(".modal-body #txtSeq21").val(lSeq21);
	 		modal.find(".modal-body #txtSeq22").val(lSeq22);
	 		modal.find(".modal-body #txtSeq23").val(lSeq23);
	 		modal.find(".modal-body #txtSeq24").val(lSeq24);
	 		modal.find(".modal-body #txtSeq25").val(lSeq25);
 			
	 		if(lPlantName != null)
	 			document.getElementById("lblPlantName").innerHTML = '(' + lPlantName + ')';
	 		if(lWarehouseName != null)
	 			document.getElementById("lblWarehouseName").innerHTML = '(' + lWarehouseName + ')';
	 		
	 		if(lPlantID == null || lPlantID == '')
	 			{
	 				$("#txtPlantID").focus(); 
	 				$("#txtPlantID").click();
	 			}
	 		else
	 			$("#txtSeq1").focus();
		});
		
		function FuncClear(){
			$('#mrkPlantID').hide();
			$('#mrkWarehouseID').hide();
			$('#mrkOperation').hide();
			$('#mrkStorageTypeIndicatorID').hide();
			$('#mrkSeq1').hide();
			
			$('#dvPlantID').removeClass('has-error');
			$('#dvWarehouseID').removeClass('has-error');
			$('#dvOperation').removeClass('has-error');
			$('#dvStorageTypeIndicatorID').removeClass('has-error');
			$('#dvSeq1').removeClass('has-error');
			
			document.getElementById("lblPlantName").innerHTML = null;
			document.getElementById("lblWarehouseName").innerHTML = null;
			
			$("#dvErrorAlert").hide();
		}
	
		function FuncButtonNew() {
			$('#btnSave').show();
			$('#btnUpdate').hide();
			document.getElementById("lblTitleModal").innerHTML = "Add Storage Type Search";
		
			$('#txtPlantID').prop('disabled', false);
			$('#txtWarehouseID').prop('disabled', false);
			$('#txtOperation').prop('disabled', false);
			$('#txtStorageTypeIndicatorID').prop('disabled', false);
		}
		
		function FuncButtonUpdate() {
			$('#btnSave').hide();
			$('#btnUpdate').show();
			document.getElementById("lblTitleModal").innerHTML = 'Edit Storage Type Search';
		
			$('#txtPlantID').prop('disabled', true);
			$('#txtWarehouseID').prop('disabled', true);
			$('#txtOperation').prop('disabled', true);
			$('#txtStorageTypeIndicatorID').prop('disabled', true);
		}
		
		function FuncPassStringPlant(lParamPlantID,lParamPlantName){
			$("#txtPlantID").val(lParamPlantID);
			document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
			$('#mrkPlantID').hide();
			$('#dvPlantID').removeClass('has-error');
		}
		
		function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
			$("#txtWarehouseID").val(lParamWarehouseID);
			document.getElementById("lblWarehouseName").innerHTML = '(' + lParamWarehouseName + ')';
			$('#mrkWarehouseID').hide();
			$('#dvWarehouseID').removeClass('has-error');
		}
		
		function FuncPassStorageTypeIndicator(lParamID,lParamName,lParamType){
			$("#txtStorageTypeIndicatorID").val(lParamID);
			$('#mrkStorageTypeIndicatorID').hide();
			$('#dvStorageTypeIndicatorID').removeClass('has-error');
		}
		
		function FuncPassDynamicStorageTypeData(lParamID,lParamType,lParamName){
			if(lParamType=='seq1'){
				$("#txtSeq1").val(lParamID);
				$('#mrkSeq1').hide();
				$('#dvSeq1').removeClass('has-error');
			}
			else if(lParamType=='seq2'){
				$("#txtSeq2").val(lParamID);
			}
			else if(lParamType=='seq3'){
				$("#txtSeq3").val(lParamID);
			}
			else if(lParamType=='seq4'){
				$("#txtSeq4").val(lParamID);
			}
			else if(lParamType=='seq5'){
				$("#txtSeq5").val(lParamID);
			}
			else if(lParamType=='seq6'){
				$("#txtSeq6").val(lParamID);
			}
			else if(lParamType=='seq7'){
				$("#txtSeq7").val(lParamID);
			}
			else if(lParamType=='seq8'){
				$("#txtSeq8").val(lParamID);
			}
			else if(lParamType=='seq9'){
				$("#txtSeq9").val(lParamID);
			}
			else if(lParamType=='seq10'){
				$("#txtSeq10").val(lParamID);
			}
			else if(lParamType=='seq11'){
				$("#txtSeq11").val(lParamID);
			}
			else if(lParamType=='seq12'){
				$("#txtSeq12").val(lParamID);
			}
			else if(lParamType=='seq13'){
				$("#txtSeq13").val(lParamID);
			}
			else if(lParamType=='seq14'){
				$("#txtSeq14").val(lParamID);
			}
			else if(lParamType=='seq15'){
				$("#txtSeq15").val(lParamID);
			}
			else if(lParamType=='seq16'){
				$("#txtSeq16").val(lParamID);
			}
			else if(lParamType=='seq17'){
				$("#txtSeq17").val(lParamID);
			}
			else if(lParamType=='seq18'){
				$("#txtSeq18").val(lParamID);
			}
			else if(lParamType=='seq19'){
				$("#txtSeq19").val(lParamID);
			}
			else if(lParamType=='seq20'){
				$("#txtSeq20").val(lParamID);
			}
			else if(lParamType=='seq21'){
				$("#txtSeq21").val(lParamID);
			}
			else if(lParamType=='seq22'){
				$("#txtSeq22").val(lParamID);
			}
			else if(lParamType=='seq23'){
				$("#txtSeq23").val(lParamID);
			}
			else if(lParamType=='seq24'){
				$("#txtSeq24").val(lParamID);
			}
			else if(lParamType=='seq25'){
				$("#txtSeq25").val(lParamID);
			}
		}
		
		function FuncValPlant(){	
			var txtPlantID = document.getElementById('txtPlantID').value;
			
			if(!txtPlantID.match(/\S/)) {    	
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    } 
			
		    return true;	
		}
		
		function FuncValPlantWarehouse(){
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			
			if(!txtPlantID.match(/\S/)){
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    }else if(!txtWarehouseID.match(/\S/)){
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		    	
		    	alert("Fill Warehouse ID First ...!!!");
		        return false;
		    }
		    return true;
		}
		
		function FuncValEmptyInput(lParambtn) {
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			var txtOperation = document.getElementById('txtOperation').value;
			var txtStorageTypeIndicatorID = document.getElementById('txtStorageTypeIndicatorID').value;
			var txtSeq1 = document.getElementById('txtSeq1').value;
			var txtSeq2 = document.getElementById('txtSeq2').value;
			var txtSeq3 = document.getElementById('txtSeq3').value;
			var txtSeq4 = document.getElementById('txtSeq4').value;
			var txtSeq5 = document.getElementById('txtSeq5').value;
			var txtSeq6 = document.getElementById('txtSeq6').value;
			var txtSeq7 = document.getElementById('txtSeq7').value;
			var txtSeq8 = document.getElementById('txtSeq8').value;
			var txtSeq9 = document.getElementById('txtSeq9').value;
			var txtSeq10 = document.getElementById('txtSeq10').value;
			var txtSeq11 = document.getElementById('txtSeq11').value;
			var txtSeq12 = document.getElementById('txtSeq12').value;
			var txtSeq13 = document.getElementById('txtSeq13').value;
			var txtSeq14 = document.getElementById('txtSeq14').value;
			var txtSeq15 = document.getElementById('txtSeq15').value;
			var txtSeq16 = document.getElementById('txtSeq16').value;
			var txtSeq17 = document.getElementById('txtSeq17').value;
			var txtSeq18 = document.getElementById('txtSeq18').value;
			var txtSeq19 = document.getElementById('txtSeq19').value;
			var txtSeq20 = document.getElementById('txtSeq20').value;
			var txtSeq21 = document.getElementById('txtSeq21').value;
			var txtSeq22 = document.getElementById('txtSeq22').value;
			var txtSeq23 = document.getElementById('txtSeq23').value;
			var txtSeq24 = document.getElementById('txtSeq24').value;
			var txtSeq25 = document.getElementById('txtSeq25').value;
			
		    if(!txtPlantID.match(/\S/)) {
		    	$("#txtPlantID").focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		        return false;
		    } 
		    
		    if(!txtWarehouseID.match(/\S/)) {    	
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		        return false;
		    }
		    
		    if(!txtOperation.match(/\S/)) {
		    	$('#txtOperation').focus();
		    	$('#dvOperation').addClass('has-error');
		    	$('#mrkOperation').show();
		        return false;
		    }
		    
		    if(!txtStorageTypeIndicatorID.match(/\S/)) {
		    	$('#txtStorageTypeIndicatorID').focus();
		    	$('#dvStorageTypeIndicatorID').addClass('has-error');
		    	$('#mrkStorageTypeIndicatorID').show();
		        return false;
		    }
		    
		    if(!txtSeq1.match(/\S/)) {
		    	$('#txtSeq1').focus();
		    	$('#dvSeq1').addClass('has-error');
		    	$('#mrkSeq1').show();
		        return false;
		    }
		    
		    
		    jQuery.ajax({
		        url:'${pageContext.request.contextPath}/stortypesearch',	
		        type:'POST',
		        data:{"key":lParambtn,"txtPlantID":txtPlantID,"txtWarehouseID":txtWarehouseID,
		        	  "txtOperation":txtOperation,"txtStorageTypeIndicatorID":txtStorageTypeIndicatorID,"txtSeq1":txtSeq1,
		        	  "txtSeq2":txtSeq2,"txtSeq3":txtSeq3,"txtSeq4":txtSeq4,"txtSeq5":txtSeq5,"txtSeq6":txtSeq6,"txtSeq7":txtSeq7,
		        	  "txtSeq8":txtSeq8,"txtSeq9":txtSeq9,"txtSeq10":txtSeq10,"txtSeq11":txtSeq11,
		        	  "txtSeq12":txtSeq12,"txtSeq13":txtSeq13,"txtSeq14":txtSeq14,"txtSeq15":txtSeq15,"txtSeq16":txtSeq16,"txtSeq17":txtSeq17,
		        	  "txtSeq18":txtSeq18,"txtSeq19":txtSeq19,"txtSeq20":txtSeq20,"txtSeq21":txtSeq21,"txtSeq22":txtSeq22,
		        	  "txtSeq23":txtSeq23,"txtSeq24":txtSeq24,"txtSeq25":txtSeq25},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	if(data.split("--")[0] == 'FailedInsertStorageTypeSearch')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan storage type search";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtPlantID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else if(data.split("--")[0] == 'FailedUpdateStorageTypeSearch')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui storage type search";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtSeq1").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else
		        	{
			        	var url = '${pageContext.request.contextPath}/stortypesearch';  
			        	$(location).attr('href', url);
		        	}
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    
		    return true;
		}
		
		
		//get warehouse from plant id
		$(document).ready(function(){
	
		    $(document).on('click', '#txtWarehouseID', function(e){
		  
		     e.preventDefault();
		  
			 var plantid = document.getElementById('txtPlantID').value;
		  
		     $('#dynamic-content-warehouse').html(''); // leave this div blank
			//$('#modal-loader').show();      // load ajax loader on button click
		 
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getwarehouse',
		          type: 'POST',
		          data: 'plantid='+plantid,
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-warehouse').html(''); // blank before load.
		          $('#dynamic-content-warehouse').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-warehouse').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage type indicator by plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStorageTypeIndicatorID', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-content-storage-type-indicator').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragetypeindicator',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"type":''},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storage-type-indicator').html(''); // blank before load.
		          $('#dynamic-content-storage-type-indicator').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storage-type-indicator').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		function FuncGetStorageType(plantid,warehouseid,seq){
			var listSeq = new Array();
			
			for (x = 1; x < 26; x++) {
				if(document.getElementById('txtSeq'+x).value != "")
					listSeq.push("'" + document.getElementById('txtSeq'+x).value + "'");
			}
			
			 $('#dynamic-content-storage-type').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragetype',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"type":seq,"listSeq":listSeq},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storage-type').html(''); // blank before load.
		          $('#dynamic-content-storage-type').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storage-type').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		}
		
		//get storage type by plant and warehouse id
		$(document).ready(function(){
		
			//seq 1
		    $(document).on('click', '#txtSeq1', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
				var seq = 'seq1';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 2
		    $(document).on('click', '#txtSeq2', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq2';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 3
		    $(document).on('click', '#txtSeq3', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq3';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 4
		    $(document).on('click', '#txtSeq4', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq4';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 5
		    $(document).on('click', '#txtSeq5', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq5';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 6
		    $(document).on('click', '#txtSeq6', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq6';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 7
		    $(document).on('click', '#txtSeq7', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq7';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 8
		    $(document).on('click', '#txtSeq8', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq8';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 9
		    $(document).on('click', '#txtSeq9', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		    	 var seq = 'seq9';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 10
		    $(document).on('click', '#txtSeq10', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq10';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 11
		    $(document).on('click', '#txtSeq11', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq11';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 12
		    $(document).on('click', '#txtSeq12', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq12';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 13
		    $(document).on('click', '#txtSeq13', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq13';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 14
		    $(document).on('click', '#txtSeq14', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq14';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 15
		    $(document).on('click', '#txtSeq15', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		    	var seq = 'seq15';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 16
		    $(document).on('click', '#txtSeq16', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq16';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 17
		    $(document).on('click', '#txtSeq17', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq17';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 18
		    $(document).on('click', '#txtSeq18', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq18';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 19
		    $(document).on('click', '#txtSeq19', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq19';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 20
		    $(document).on('click', '#txtSeq20', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		   		var seq = 'seq20';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 21
		    $(document).on('click', '#txtSeq21', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		    	 var seq = 'seq21';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 22
		    $(document).on('click', '#txtSeq22', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		    	 var seq = 'seq22';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 23
		    $(document).on('click', '#txtSeq23', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		    	var seq = 'seq23';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 24
		    $(document).on('click', '#txtSeq24', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		    	var seq = 'seq24';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		    //seq 25
		    $(document).on('click', '#txtSeq25', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     	var seq = 'seq25';
				FuncGetStorageType(plantid,warehouseid,seq);
		    });
		    
		});
		
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtWarehouseID:focus').length) {
		    	$('#txtWarehouseID').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageTypeIndicatorID:focus').length) {
		    	$('#txtStorageTypeIndicatorID').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq1:focus').length) {
		    	$('#txtSeq1').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq2:focus').length) {
		    	$('#txtSeq2').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq3:focus').length) {
		    	$('#txtSeq3').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq4:focus').length) {
		    	$('#txtSeq4').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq5:focus').length) {
		    	$('#txtSeq5').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq6:focus').length) {
		    	$('#txtSeq6').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq7:focus').length) {
		    	$('#txtSeq7').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq8:focus').length) {
		    	$('#txtSeq8').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq9:focus').length) {
		    	$('#txtSeq9').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq10:focus').length) {
		    	$('#txtSeq10').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq11:focus').length) {
		    	$('#txtSeq11').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq12:focus').length) {
		    	$('#txtSeq12').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq13:focus').length) {
		    	$('#txtSeq13').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq14:focus').length) {
		    	$('#txtSeq14').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq15:focus').length) {
		    	$('#txtSeq15').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq16:focus').length) {
		    	$('#txtSeq16').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq17:focus').length) {
		    	$('#txtSeq17').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq18:focus').length) {
		    	$('#txtSeq18').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq19:focus').length) {
		    	$('#txtSeq19').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq20:focus').length) {
		    	$('#txtSeq20').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq21:focus').length) {
		    	$('#txtSeq21').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq22:focus').length) {
		    	$('#txtSeq22').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq23:focus').length) {
		    	$('#txtSeq23').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq24:focus').length) {
		    	$('#txtSeq24').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSeq25:focus').length) {
		    	$('#txtSeq25').click();
		    }
		});
	</script>
</body>
</html>