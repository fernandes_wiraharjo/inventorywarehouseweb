<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_staging_area" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>Plant ID</th>
			<th>Warehouse ID</th>
			<th>Door ID</th>
			<th>Staging Area ID</th>
			<th>Name</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listStagingArea}" var ="stagingarea">
	  <tr>
	  <td><c:out value="${stagingarea.plantID}" /></td>
		<td><c:out value="${stagingarea.warehouseID}" /></td>
		<td><c:out value="${stagingarea.doorID}" /></td>
		<td><c:out value="${stagingarea.materialStagingAreaID}" /></td>
		<td><c:out value="${stagingarea.materialStagingAreaName}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassStringStagingArea('<c:out value="${stagingarea.materialStagingAreaID}"/>','<c:out value="${stagingarea.materialStagingAreaName}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_staging_area").DataTable();
  	});
</script>