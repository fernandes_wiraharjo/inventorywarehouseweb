<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Product</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

<style>	
/*  css for loading bar */
 .loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 35px;
  height: 35px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
/* end of css loading bar */
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="ProductDetail" name="ProductDetail" action = "${pageContext.request.contextPath}/ProductDetail" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Product Detail <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsert'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Insert new Product Detail succed.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedInsert'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Insert new Product Detail fail. Please contact admin for help.
              				</div>
	     					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdate'}">
	    					  <div id="alrUpdate" class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Update Product Detail succed.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedUpdate'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Update Product Detail fail. Please contact admin for help.
              				</div>
	     					</c:if> 
	     					
	     					<c:if test="${condition == 'SuccessDelete'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="buttPon" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Delete Product Detail succed.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'empty_condition'}">
	    					  <script>$('#alrUpdate').hide();</script>
	      					</c:if>

	      					<c:if test="${condition == 'FailedDelete'}">	
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Delete Product Detail fail. Please contact admin for help.
              				</div>
	     					</c:if>   
	      					
	      					<div class="row">
							<div class="col-md-1 pull-right">
								<button <c:out value="${buttonstatus}"/> id="btnSync" name="btnSync" type="button" class="btn btn-primary pull-right" onclick="FuncSync()"><i class="fa fa-refresh"></i> Sync</button>
							</div>
							<div class="col-md-0 pull-right">
								<!-- loading bar -->
								<div id="dvloader" class="loader" style="display:none"></div>	
							</div>
							<br><br>
	      					</div>
	      					
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>ID</th>
										<th>Code</th>
										<th>Product ID</th>
										<th>Attribute ID</th>
										<th>Quantity</th>
										<th>SKU</th>
										<th>Status</th>
										<th>Image</th>
										<th>Price Basic</th>
										<th>Price Selling</th>
										<th>Price Carton</th>
										<th>Jumlah Per Carton</th>
										<th>Weight</th>
										<th>Width</th>
										<th>Height</th>
										<th>Volume</th>
										<th>Wide</th>
										<th>Wide Carton</th>
										<th>Weight Carton</th>
										<th>Width Carton</th>
										<th>Height Carton</th>
										<th>Volume Carton</th>
										<th>View</th>
										<th>Barcode</th>
										<th>Price Selling Tax</th>
										<th>Price Carton Tax</th>
									</tr>
								</thead>	

								<tbody>

									<c:forEach items="${listproductdetail}" var="productdetail">
										<tr>
											<td><c:out value="${productdetail.id}" /></td>
											<td><c:out value="${productdetail.code}" /></td>
											<td><c:out value="${productdetail.product_id}" /></td>
											<td><c:out value="${productdetail.attribute_id}" /></td>
											<td><c:out value="${productdetail.quantity}" /></td>
											<td><c:out value="${productdetail.sku}" /></td>
											<td><c:out value="${productdetail.status}" /></td>											
											<td><c:out value="${productdetail.image}" /></td>
											<td><c:out value="${productdetail.price_basic}" /></td>
											<td><c:out value="${productdetail.price_selling}" /></td>
											<td><c:out value="${productdetail.price_carton}" /></td>
											<td><c:out value="${productdetail.jumlah_per_carton}" /></td>
											<td><c:out value="${productdetail.weight}" /></td>
											<td><c:out value="${productdetail.width}" /></td>
											<td><c:out value="${productdetail.height}" /></td>											
											<td><c:out value="${productdetail.volume}" /></td>
											<td><c:out value="${productdetail.wide}" /></td>
											<td><c:out value="${productdetail.wide_carton}" /></td>
											<td><c:out value="${productdetail.weight_carton}" /></td>
											<td><c:out value="${productdetail.width_carton}" /></td>
											<td><c:out value="${productdetail.height_carton}" /></td>
											<td><c:out value="${productdetail.volume_carton}" /></td>
											<td><c:out value="${productdetail.view}" /></td>											
											<td><c:out value="${productdetail.barcode}" /></td>
											<td><c:out value="${productdetail.price_selling_tax}" /></td>
											<td><c:out value="${productdetail.price_carton_tax}" /></td>
										</tr>

									</c:forEach>

								</tbody>
							</table>
							
	
						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 		$(function () {
  			$("#tb_itemlib").DataTable( {
  		        "scrollX": true
  		    });
  			$('#M003').addClass('active');
  	  		$('#M024').addClass('active');
  			
  	  		$('#dvloader').hide();
  		});
 		
 		function FuncSync() {
 			$('#dvloader').show();
 			
 			jQuery.ajax({
 		        url:'${pageContext.request.contextPath}/ProductDetail',	
 		        type:'GET',
 		        data:{"key":"sync"},
 		        dataType : 'text',
 		        success:function(data, textStatus, jqXHR){
 		        	console.log('Service call succeed!');
 		        	
 		        	var url = '${pageContext.request.contextPath}/ProductDetail';  
 		        	$(location).attr('href', url);
 		        },
 		        error:function(data, textStatus, jqXHR){
 		            console.log('Service call failed!');
 		           $('#dvloader').hide();
 		        }
 		    });
 		    return true;
 		}
	</script>
</body>
</html>