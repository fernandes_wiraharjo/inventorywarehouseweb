<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Customer</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="Customer" name="Customer" action = "${pageContext.request.contextPath}/Customer" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Customer <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertCustomer'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambah pelanggan.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateCustomer'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui pelanggan.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessDeleteCustomer'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menghapus pelanggan.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedDeleteCustomer'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal menghapus pelanggan. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>   
	      					
	      					<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvCustomerID" class="form-group col-xs-6">
	            								<label for="recipient-name" class="control-label">Customer ID</label><label id="mrkCustomerID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtCustomerID" name="txtCustomerID">
	          								</div>
	          								<div id="dvCustomerName" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Customer Name</label><label id="mrkCustomerName" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtCustomerName" name="txtCustomerName">
	          								</div>
	          								<div id="dvCustomerAddress" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Customer Address</label><label id="mrkCustomerAddress" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtCustomerAddress" name="txtCustomerAddress">
	          								</div>
	          								<div id="dvCustomerTypeID" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Customer Type ID</label><label id="mrkCustomerTypeID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblCustomerTypeName" name="lblCustomerTypeName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtCustomerTypeID" name="txtCustomerTypeID" data-toggle="modal" data-target="#ModalGetCustTypeID">
	          								</div>
	          								<div id="dvPhone" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Phone</label><label id="mrkPhone" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtPhone" name="txtPhone">
	          								</div>
	          								<div id="dvEmail" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Email</label><label id="mrkEmail" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtEmail" name="txtEmail">
	          								</div>
	          								<div id="dvPIC" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">PIC</label><label id="mrkPIC" for="recipient-name" class="control-label"> <small>*</small></label>	
	            								<input type="text" class="form-control" id="txtPIC" name="txtPIC">
	          								</div>
	        								
	        								<div class="row"></div>	 
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
									<!--modal Delete -->
									<div class="example-modal">
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Customer</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtCustomerID" name="temp_txtCustomerID" >
               	 									<p>Are you sure to delete this customer ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								      </div>
								      
								      <!--modal show customer type data -->
										<div class="modal fade" id="ModalGetCustTypeID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelCustTypeID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelCustTypeID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_customertype" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Customer Type ID</th>
														<th>Customer Type Name</th>
														<th>Description</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listcusttype}" var ="custtype">
												        <tr>
												        <td><c:out value="${custtype.customerTypeID}" /></td>
														<td><c:out value="${custtype.customerTypeName}" /></td>
														<td><c:out value="${custtype.description}" /></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('<c:out value="${custtype.customerTypeID}"/>','<c:out value="${custtype.customerTypeName}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Customer ID</th>
										<th>Customer Name</th>
										<th>Customer Address</th>
										<th>Customer Type ID</th>
										<th>Phone</th>
										<th>Email</th>
										<th>PIC</th>
										<th style="width:60px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listcustomer}" var="customer">
										<tr>
											<td><c:out value="${customer.customerID}" /></td>
											<td><c:out value="${customer.customerName}" /></td>
											<td><c:out value="${customer.customerAddress}" /></td>
											<td><c:out value="${customer.customerTypeID}" /></td>
											<td><c:out value="${customer.phone}" /></td>
											<td><c:out value="${customer.email}" /></td>
											<td><c:out value="${customer.pic}" /></td>
											<td><button <c:out value="${buttonstatus}"/> 
														id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert" 
														data-lcustomerid='<c:out value="${customer.customerID}" />'
														data-lcustomername='<c:out value="${customer.customerName}" />'
														data-lcustomeraddress='<c:out value="${customer.customerAddress}" />'
														data-lphone='<c:out value="${customer.phone}" />'
														data-lemail='<c:out value="${customer.email}" />'
														data-lpic='<c:out value="${customer.pic}" />'
														data-lcustomertypeid='<c:out value="${customer.customerTypeID}" />'
														data-lcustomertypename='<c:out value="${customer.customerTypeName}" />'>
														<i class="fa fa-edit"></i></button> 
												<button <c:out value="${buttonstatus}"/>
														id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" data-toggle="modal" 
														data-target="#ModalDelete" data-lcustomerid='<c:out value="${customer.customerID}" />'>
												<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_itemlib").DataTable();
  		$("#tb_master_customertype").DataTable();
  		$('#M002').addClass('active');
  		$('#M013').addClass('active');
  		
  	//shortcut for button 'new'
  	    Mousetrap.bind('n', function() {
  	    	FuncButtonNew(),
  	    	$('#ModalUpdateInsert').modal('show')
  	    	});
  	
  	  $("#dvErrorAlert").hide();
  	});
	</script>
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lCustomerID = button.data('lcustomerid');
		$("#temp_txtCustomerID").val(lCustomerID);
	})
</script>
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		var lCustomerID = button.data('lcustomerid');
 		var lCustomerName = button.data('lcustomername');
 		var lCustomerAddress = button.data('lcustomeraddress');
 		var lEmail = button.data('lemail');
 		var lPhone = button.data('lphone');
 		var lPIC = button.data('lpic');
 		var lCustomerTypeID = button.data('lcustomertypeid');
 		var lCustomerTypeName = button.data('lcustomertypename');
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtCustomerID").val(lCustomerID);
//  		modal.find(".modal-body #txtCustomerID").prop('disabled', true);
 		modal.find(".modal-body #txtCustomerName").val(lCustomerName);
 		modal.find(".modal-body #txtCustomerAddress").val(lCustomerAddress);
 		modal.find(".modal-body #txtEmail").val(lEmail);
 		modal.find(".modal-body #txtPIC").val(lPIC);
 		modal.find(".modal-body #txtPhone").val(lPhone);
 		modal.find(".modal-body #txtCustomerTypeID").val(lCustomerTypeID);
 		
 		if(lCustomerTypeName != null)
 		document.getElementById('lblCustomerTypeName').innerHTML = "(" + lCustomerTypeName + ")";
 		
 		if(lCustomerID == null || lCustomerID == '')
 			$("#txtCustomerID").focus();
 		else
 			$("#txtCustomerName").focus();
	})
</script>

<script>
function FuncPassString(lParamCustTypeID,lParamCustTypeNm){
	$("#txtCustomerTypeID").val(lParamCustTypeID);
	document.getElementById('lblCustomerTypeName').innerHTML = "(" + lParamCustTypeNm + ")";
}
</script>

<script>
// var mrkCustomerID = document.getElementById('mrkCustomerID').value;
// var mrkCustomerName = document.getElementById('mrkCustomerName').value;
// var mrkCustomerAddress = document.getElementById('mrkCustomerAddress').value;
// var mrkEmail = document.getElementById('mrkEmail').value;
// var mrkPhone = document.getElementById('mrkPhone').value;
// var mrkPIC = document.getElementById('mrkPIC').value;
// var mrkCustomerTypeID = document.getElementById('mrkCustomerTypeID').value;
// var btnSave = document.getElementById('btnSave').value;
// var btnUpdate = document.getElementById('btnUpdate').value;

function FuncClear(){
	$('#mrkCustomerID').hide();
	$('#txtCustID').prop('disabled', false);
	$('#mrkCustomerName').hide();
	$('#mrkCustomerAddress').hide();
	$('#mrkEmail').hide();
	$('#mrkPhone').hide();
	$('#mrkPIC').hide();
	$('#mrkCustomerTypeID').hide();
	
	$('#dvCustomerID').removeClass('has-error');
	$('#dvCustomerName').removeClass('has-error');
	$('#dvCustomerAddress').removeClass('has-error');
	$('#dvEmail').removeClass('has-error');
	$('#dvPhone').removeClass('has-error');
	$('#dvPIC').removeClass('has-error');
	$('#dvCustomerTypeID').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Customer";
	document.getElementById('lblCustomerTypeName').innerHTML = null;

	FuncClear();
	$('#txtCustomerID').prop('disabled', false);
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById('lblTitleModal').innerHTML = 'Edit Customer';
	
	FuncClear();
	$('#txtCustomerID').prop('disabled', true);
}

function FuncValEmptyInput(lParambtn) {
	var txtCustomerID = document.getElementById('txtCustomerID').value;
	var txtCustomerName = document.getElementById('txtCustomerName').value;
	var txtCustomerAddress = document.getElementById('txtCustomerAddress').value;
	var txtEmail = document.getElementById('txtEmail').value;
	var txtPhone = document.getElementById('txtPhone').value;
	var txtPIC = document.getElementById('txtPIC').value;
	var txtCustomerTypeID = document.getElementById('txtCustomerTypeID').value;
	
	var dvCustomerID = document.getElementsByClassName('dvCustomerID');
	var dvCustomerName = document.getElementsByClassName('dvCustomerName');
	var dvCustomerAddress = document.getElementsByClassName('dvCustomerAddress');
	var dvEmail = document.getElementsByClassName('dvEmail');
	var dvPhone = document.getElementsByClassName('dvPhone');
	var dvPIC = document.getElementsByClassName('dvPIC');
	var dvCustomerTypeID = document.getElementsByClassName('dvCustomerTypeID');
	
	if(lParambtn == 'save'){
		$('#txtCustomerID').prop('disabled', false);
	}
	else{
		$('#txtCustomerID').prop('disabled', true);
	}

    if(!txtCustomerID.match(/\S/)) {
    	$("#txtCustomerID").focus();
    	$('#dvCustomerID').addClass('has-error');
    	$('#mrkCustomerID').show();
        return false;
    } 
    
    if(!txtCustomerName.match(/\S/)) {    	
    	$('#txtCustomerName').focus();
    	$('#dvCustomerName').addClass('has-error');
    	$('#mrkCustomerName').show();
        return false;
    } 
    
    if(!txtCustomerAddress.match(/\S/)) {
    	$('#txtCustomerAddress').focus();
    	$('#dvCustomerAddress').addClass('has-error');
    	$('#mrkCustomerAddress').show();
        return false;
    }
    
    if(!txtCustomerTypeID.match(/\S/)) {
    	$('#txtCustomerTypeID').focus();
    	$('#dvCustomerTypeID').addClass('has-error');
    	$('#mrkCustomerTypeID').show();
        return false;
    } 
    
    if(!txtPhone.match(/\S/)) {
    	$('#txtPhone').focus();
    	$('#dvPhone').addClass('has-error');
    	$('#mrkPhone').show();
        return false;
    } 
    
    if(!txtEmail.match(/\S/)) {
    	$('#txtEmail').focus();
    	$('#dvEmail').addClass('has-error');
    	$('#mrkEmail').show();
        return false;
    } 
    
    if(!txtPIC.match(/\S/)) {
    	$('#txtPIC').focus();
    	$('#dvPIC').addClass('has-error');
    	$('#mrkPIC').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/Customer',	
        type:'POST',
        data:{"key":lParambtn,"txtCustomerID":txtCustomerID,"txtCustomerName":txtCustomerName,"txtCustomerAddress":txtCustomerAddress, "txtEmail":txtEmail, "txtPhone":txtPhone,"txtPIC":txtPIC, "txtCustomerTypeID":txtCustomerTypeID},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertCustomer')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan pelanggan";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtCustomerID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateCustomer')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui pelanggan";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtCustomerName").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/Customer';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtCustomerTypeID:focus').length) {
    	$('#txtCustomerTypeID').click();
    }
});
</script>


</body>
</html>