<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="TableRptGoodReceipt" name="TableRptGoodReceipt" action = "${pageContext.request.contextPath}/TableRptGoodReceipt" method="post">

    		<!-- /.row -->
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
						<div class="row">
						<div class="col-md-2">
							Start Date
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-9">
							<c:out value = "${lStartDate}"/>
						</div>
						<div class="col-md-2">
							End Date
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-9">
							<c:out value = "${lEndDate}"/>
						</div>
						
						</div>
						<div class="row">
						<br>
						</div>
							<table id="tb_RptGoodIssue" class="table table-bordered table-striped table-hover">								
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Doc No</th>
										<th>Date</th>
										<th>Customer ID</th>
										<th>Plant ID</th>
										<th>Warehouse ID</th>
										<th>DocLine</th>
										<th>Product ID</th>
										<th>QtyUOM</th>
										<th>UOM</th>
										<th>Qty BaseUOM</th>
										<th>BaseUOM</th>
										<th>Batch No</th>
										<th>Packing No</th>
									</tr>
								</thead>	
								<tbody>
									<c:forEach items="${listgoodissue}" var="goodissue">
										<tr>
											<td><c:out value="${goodissue.docNo}" /></td>
											<td><c:out value="${goodissue.date}" /></td>
											<td><c:out value="${goodissue.customerID}" /></td>
											<td><c:out value="${goodissue.plantID}" /></td>
											<td><c:out value="${goodissue.warehouseID}" /></td>
											<td><c:out value="${goodissue.docLine}" /></td>
											<td><c:out value="${goodissue.productID}" /></td>
											<td><c:out value="${goodissue.qtyUOM}" /></td>
											<td><c:out value="${goodissue.UOM}" /></td>
											<td><c:out value="${goodissue.qtyBaseUOM}" /></td>
											<td><c:out value="${goodissue.baseUOM}" /></td>
											<td><c:out value="${goodissue.batchNo}" /></td>
											<td><c:out value="${goodissue.packingNo}" /></td>									
										</tr>

									</c:forEach>

								</tbody>
								
							</table>
							
						</div>
						
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
</form>

<script>
$(function () {
	$("#tb_RptGoodIssue").DataTable( {
		"scrollX": true,
		"order": [],
		"columnDefs": [ {
	        "targets": 'no-sort'
	 	 } ]
	});
});
</script>

		

			