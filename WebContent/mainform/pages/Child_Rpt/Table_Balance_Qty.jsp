<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="TableBalanceQty" name="TableBalanceQty" action = "${pageContext.request.contextPath}/TableBalanceQty" method="post">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
												<div class="row">
						<div class="col-md-2">
							Start Date
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-3">
							<c:out value = "${lStartDate}"/>
						</div>
						<div class="col-md-2">
							Total Inbound
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-3">
							<c:out value = "${lTotalInbound}"/>
						</div>
						
						<div class="col-md-2">
							End Date
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-3">
							<c:out value = "${lEndDate}"/>
						</div>
						<div class="col-md-2">
							Total Outbound
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-3">
							<c:out value = "${lTotalOutbound}"/>
						</div>
						</div>
						<div class="row">
						<br>
						</div>
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">								
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Period</th>
										<th>Plant ID</th>
										<th>Warehouse ID</th>
										<th>Product ID</th>
										<th>Batch No</th>
										<th>Qty</th>
										<th>UOM</th>
									</tr>
								</thead>	
								<tbody>
									<c:forEach items="${liststock}" var="stock">
										<tr>
											<td><c:out value="${stock.period}" /></td>
											<td><c:out value="${stock.plantID}" /></td>
											<td><c:out value="${stock.warehouseID}" /></td>
											<td><c:out value="${stock.productID}" /></td>
											<td><c:out value="${stock.batchNo}" /></td>
											<td><c:out value="${stock.qty}" /></td>
											<td><c:out value="${stock.UOM}" /></td>
										</tr>
									</c:forEach>
								</tbody>								
							</table>							
						</div>
					</div>
				</div>
			</div>
</form>
<script>
$(function () {
	$("#tb_itemlib").DataTable( {
		"scrollX": true,
		"order": [],
		"columnDefs": [ {
	        "targets": 'no-sort'
	 	 } ]
	});
});
</script>