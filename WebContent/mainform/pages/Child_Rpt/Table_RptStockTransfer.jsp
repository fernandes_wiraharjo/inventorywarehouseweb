<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="TableRptGoodReceipt" name="TableRptGoodReceipt" action = "${pageContext.request.contextPath}/TableRptGoodReceipt" method="post">
<!-- /.row -->
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
						<div class="row">
						<div class="col-md-2">
							Start Date
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-9">
							<c:out value = "${lStartDate}"/>
						</div>
						<div class="col-md-2">
							End Date
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-9">
							<c:out value = "${lEndDate}"/>
						</div>
						
						</div>
						<div class="row">
						<br>
						</div>
							<table id="tb_RptStockTransfer" class="table table-bordered table-striped table-hover">								
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Trans ID</th>
										<th>Date</th>
										<th>Plant ID</th>
										<th>From</th>
										<th>To</th>
										<th>DocLine</th>
										<th>Product ID</th>
										<th>QtyUOM</th>
										<th>UOM</th>
										<th>Qty BaseUOM</th>
										<th>BaseUOM</th>
										<th>Batch No</th>
										<th>Packing No</th>
									</tr>
								</thead>	
								<tbody>
									<c:forEach items="${liststocktransfer}" var="stock">
										<tr>
											<td><c:out value="${stock.transID}" /></td>
											<td><c:out value="${stock.date}" /></td>
											<td><c:out value="${stock.plantID}" /></td>
											<td><c:out value="${stock.from}" /></td>
											<td><c:out value="${stock.to}" /></td>
											<td><c:out value="${stock.docLine}" /></td>
											<td><c:out value="${stock.productID}" /></td>
											<td><c:out value="${stock.qtyUOM}" /></td>
											<td><c:out value="${stock.UOM}" /></td>
											<td><c:out value="${stock.qtyBaseUOM}" /></td>
											<td><c:out value="${stock.baseUOM}" /></td>
											<td><c:out value="${stock.batchNo}" /></td>
											<td><c:out value="${stock.packingNo}" /></td>									
										</tr>

									</c:forEach>

								</tbody>
								
							</table>							
						</div>				
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
</form>
<script>
$(function () {
	$("#tb_RptStockTransfer").DataTable( {
		"scrollX": true,
		"order": [],
		"columnDefs": [ {
	        "targets": 'no-sort'
	 	 } ]
	});
});
</script>

		

			