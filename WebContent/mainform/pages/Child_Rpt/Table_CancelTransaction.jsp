<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="TableCancTransServlet" name="TableCancTransServlet" action = "${pageContext.request.contextPath}/TableCancTransServlet" method="post">

			<div  class="row">
				<div class="col-xs-12">
					<div id="box1" name="box1" class="box">
						<div class="box-body">
						<div class="row">
						<div class="col-md-2">
							Transaction
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-9">
							<c:out value = "${lcbCancTrans}"/>
						</div>
						<div class="col-md-2">
							Start Date
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-9">
							<c:out value = "${lStartDate}"/>
						</div>
						<div class="col-md-2">
							End Date
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-9">
							<c:out value = "${lEndDate}"/>
						</div>
						
						</div>
						<div class="row">
						<br>
						</div>
							<table id="tb_CancRptGoodReceipt" class="table table-bordered table-striped table-hover">								
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Doc No</th>
										<th>Date</th>
										<th>Plant ID</th>
										<th>Warehouse ID</th>
										<th>RefDocNumber</th>
										<th>DocLine</th>
										<th>Product ID</th>
										<th>QtyUOM</th>
										<th>UOM</th>
										<th>Qty BaseUOM</th>
										<th>BaseUOM</th>
										<th>Batch No</th>
										<th>Packing No</th>
									</tr>
								</thead>	
								<tbody>
									<c:forEach items="${listcancelgoodreceipt}" var="cancelgoodreceipt">
										<tr>
											<td><c:out value="${cancelgoodreceipt.docNumber}" /></td>
											<td><c:out value="${cancelgoodreceipt.date}" /></td>
											<td><c:out value="${cancelgoodreceipt.plantID}" /></td>
											<td><c:out value="${cancelgoodreceipt.warehouseID}" /></td>
											<td><c:out value="${cancelgoodreceipt.refDocNumber}" /></td>
											<td><c:out value="${cancelgoodreceipt.docLine}" /></td>
											<td><c:out value="${cancelgoodreceipt.productID}" /></td>
											<td><c:out value="${cancelgoodreceipt.qtyUOM}" /></td>
											<td><c:out value="${cancelgoodreceipt.UOM}" /></td>
											<td><c:out value="${cancelgoodreceipt.qtyBaseUOM}" /></td>
											<td><c:out value="${cancelgoodreceipt.baseUOM}" /></td>
											<td><c:out value="${cancelgoodreceipt.batchNo}" /></td>
											<td><c:out value="${cancelgoodreceipt.packingNo}" /></td>									
										</tr>

									</c:forEach>

								</tbody>
								
							</table>
							
						</div>
						
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
</form>
<script>
$(function () {
	$("#tb_CancRptGoodReceipt").DataTable( {
		"scrollX": true,
		"order": [],
		"columnDefs": [ {
	        "targets": 'no-sort'
	 	 } ]
	});
});
</script>
			