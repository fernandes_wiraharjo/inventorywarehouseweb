<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="TableRptGoodReceipt" name="TableRptGoodReceipt" action = "${pageContext.request.contextPath}/TableRptGoodReceipt" method="post">

			<div  class="row">
				<div class="col-xs-12">
					<div id="box1" name="box1" class="box">
						<div class="box-body">
						<div class="row">
						<div class="col-md-2">
							Start Date
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-9">
							<c:out value = "${lStartDate}"/>
						</div>
						<div class="col-md-2">
							End Date
						</div>
						<div class="col-md-1">
							:
						</div>
						<div class="col-md-9">
							<c:out value = "${lEndDate}"/>
						</div>
						
						</div>
						<div class="row">
						<br>
						</div>
							<table id="tb_RptGoodReceipt" class="table table-bordered table-striped table-hover">								
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Doc No</th>
										<th>Date</th>
										<th>Vendor ID</th>
										<th>Plant ID</th>
										<th>Warehouse ID</th>
										<th>DocLine</th>
										<th>Product ID</th>
										<th>QtyUOM</th>
										<th>UOM</th>
										<th>Qty BaseUOM</th>
										<th>BaseUOM</th>
										<th>Batch No</th>
										<th>Packing No</th>
									</tr>
								</thead>	
								<tbody>
									<c:forEach items="${listgoodreceipt}" var="goodreceipt">
										<tr>
											<td><c:out value="${goodreceipt.docNo}" /></td>
											<td><c:out value="${goodreceipt.date}" /></td>
											<td><c:out value="${goodreceipt.vendorID}" /></td>
											<td><c:out value="${goodreceipt.plantID}" /></td>
											<td><c:out value="${goodreceipt.warehouseID}" /></td>
											<td><c:out value="${goodreceipt.docLine}" /></td>
											<td><c:out value="${goodreceipt.productID}" /></td>
											<td><c:out value="${goodreceipt.qtyUOM}" /></td>
											<td><c:out value="${goodreceipt.UOM}" /></td>
											<td><c:out value="${goodreceipt.qtyBaseUOM}" /></td>
											<td><c:out value="${goodreceipt.baseUOM}" /></td>
											<td><c:out value="${goodreceipt.batchNo}" /></td>
											<td><c:out value="${goodreceipt.packingNo}" /></td>									
										</tr>

									</c:forEach>

								</tbody>
								
							</table>
							
						</div>
						
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
</form>

<script>
$(function () {
	$("#tb_RptGoodReceipt").DataTable( {
		"scrollX": true,
		"order": [],
		"columnDefs": [ {
	        "targets": 'no-sort'
	 	 } ]
	});
});
</script>

		

			