<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_cancel_confirmation_detail" class="table table-bordered table-hover">
    <thead style="background-color: #d2d6de;">
    <tr>
	<th>Component Line</th>
	<th>Component ID</th>
	<th>Batch No</th>
	<th>Qty</th>
	<th>UOM</th>
	<th>Qty Base</th>
	<th>Base UOM</th>
   </tr>
   </thead>
     
     <tbody>
     
     <c:forEach items="${listCancelConfirmationDetail}" var ="cancelconfirmationdetail">
       <tr>
	<td><c:out value="${cancelconfirmationdetail.componentLine}" /></td>
	<td><c:out value="${cancelconfirmationdetail.componentID}" /></td>
	<td><c:out value="${cancelconfirmationdetail.batchNo}" /></td>
	<td><c:out value="${cancelconfirmationdetail.qty}" /></td>
	<td><c:out value="${cancelconfirmationdetail.UOM}" /></td>
	<td><c:out value="${cancelconfirmationdetail.qtyBaseUOM}" /></td>
	<td><c:out value="${cancelconfirmationdetail.baseUOM}" /></td>
     </tr>
     		
     </c:forEach>
     
     </tbody>
     </table>

<script>
 		$(function () {
  	  		$("#tb_cancel_confirmation_detail").DataTable(
  	  			{
  	  				"aaSorting": []
  	  			}		
  	  		);
  		});
	</script>