<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Confirmation Document</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="bom_confirmationDocument" name="bom_confirmationDocument" action = "${pageContext.request.contextPath}/BomConfirmationDocument" method="post">
<input type="hidden" name="temp_string" value="" />

	<div class="wrapper">
	
		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Confirmation Document <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertConfimationDoc'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan dokumen konfirmasi.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateConfimationDoc'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui dokumen konfirmasi.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessDeleteConfimationDoc'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menghapus dokumen konfirmasi.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedDeleteConfimationDoc'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal menghapus dokumen konfirmasi. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>
							
							<!--modal update & Insert -->
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvConfirmationID" class="form-group">
	            								<label for="lblConfirmationID" class="control-label">Confirmation ID</label><label id="mrkConfirmationID" for="formrkConfirmationID" class="control-label"><small>*</small></label>
	            								<input type="text" class="form-control" id="txtConfirmationID" name="txtConfirmationID">
	          								</div>
	          								<div id="dvNumberFrom" class="form-group">
	            								<label for="lblNumberFrom" class="control-label">Number From</label><label id="mrkNumberFrom" for="formrkNumberFrom" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtNumberFrom" name="txtNumberFrom">
	          								</div>
	          								<div id="dvNumberTo" class="form-group">
	            								<label for="lblNumberTo" class="control-label">Number To</label><label id="mrkNumberTo" for="formrkNumberTo" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtNumberTo" name="txtNumberTo">
	          								</div>
	          								<div class="form-group">
	            								<label for="lblConfirmationDesc" class="control-label">Confirmation Description</label>	
	            								<textarea class="form-control" id="txtConfirmationDesc" name="txtConfirmationDesc"></textarea>
	          								</div>
	          								<div id="dvConfirmationType" class="form-group">
	            								<label for="message-text" class="control-label">Confirmation Type</label><label id="mrkConfirmationType" for="recipient-name" class="control-label"> <small>*</small></label>	
	            								<select id="slConfirmationType" name="slConfirmationType" class="form-control">
							                    <option>Confirmation</option>
							                    <option>Cancel</option>
							                    </select>
	          								</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									<!-- end of update insert modal -->
									
									<!--modal Delete -->
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Confirmation Document</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtConfirmationID" name="temp_txtConfirmationID"  />
               	 									<p>Are you sure to delete this confirmation document ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_confirmation_document" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Confirmation ID</th>
										<th>Range From</th>
										<th>Range To</th>
										<th>Confirmation Desc</th>
										<th>Confirmation Type</th>
										<th style="width:60px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listBomConfirmationDocument}" var="confirmationdoc">
										<tr>
											<td><c:out value="${confirmationdoc.confirmationID}" /></td>
											<td><c:out value="${confirmationdoc.rangeFrom}" /></td>
											<td><c:out value="${confirmationdoc.rangeTo}" /></td>
											<td><c:out value="${confirmationdoc.confirmationDesc}" /></td>
											<td><c:out value="${confirmationdoc.confirmationType}" /></td>
											<td><button <c:out value="${buttonstatus}"/>
														id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert" 
														data-lconfirmationid='<c:out value="${confirmationdoc.confirmationID}" />'
														data-lfrom='<c:out value="${confirmationdoc.rangeFrom}" />'
														data-lto='<c:out value="${confirmationdoc.rangeTo}" />'
														data-lconfirmationdesc='<c:out value="${confirmationdoc.confirmationDesc}" />'
														data-lconfirmationtype='<c:out value="${confirmationdoc.confirmationType}" />'
														>
														<i class="fa fa-edit"></i></button> 
												<button <c:out value="${buttonstatus}"/>
														id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" onclick="FuncButtonDelete()" 
														data-toggle="modal" 
														data-target="#ModalDelete"
														data-lconfirmationid='<c:out value="${confirmationdoc.confirmationID}" />'>
												<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
																
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_confirmation_document").DataTable();
  		$('#M006').addClass('active');
  		$('#M035').addClass('active');
  		
  		$("#dvErrorAlert").hide();
  		
  	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
  	});
	</script>
	
	<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lConfirmationID = button.data('lconfirmationid');
		$("#temp_txtConfirmationID").val(lConfirmationID);
	})
	</script>
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		
 		var lConfirmationID = button.data('lconfirmationid');
 		var lNumberFrom = button.data('lfrom');
 		var lNumberTo = button.data('lto');
 		var lConfirmationDesc = button.data('lconfirmationdesc');
 		var lConfirmationType = button.data('lconfirmationtype');
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtConfirmationID").val(lConfirmationID);
 		modal.find(".modal-body #txtNumberFrom").val(lNumberFrom);
 		modal.find(".modal-body #txtNumberTo").val(lNumberTo);
 		modal.find(".modal-body #txtConfirmationDesc").val(lConfirmationDesc);
 		modal.find(".modal-body #slConfirmationType").val(lConfirmationType);
 		
 		if(lConfirmationID == null || lConfirmationID == '')
 			{
 				$("#txtConfirmationID").focus();
 			}
 		else
 			$("#txtNumberFrom").focus();
	})
</script>


<script>

function FuncClear(){
	$('#mrkConfirmationID').hide();
	$('#mrkNumberFrom').hide();
	$('#mrkNumberTo').hide();
	$('#mrkConfirmationType').hide();
	
	$('#dvConfirmationID').removeClass('has-error');
	$('#dvNumberFrom').removeClass('has-error');
	$('#dvNumberTo').removeClass('has-error');
	$('#dvConfirmationType').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Confirmation Document";

	FuncClear();
	$('#txtConfirmationID').prop('disabled', false);
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById("lblTitleModal").innerHTML = 'Edit Confirmation Document';

	FuncClear();
	$('#txtConfirmationID').prop('disabled', true);
}

function FuncValEmptyInput(lParambtn) {
	var txtConfirmationID = document.getElementById('txtConfirmationID').value;
	var txtNumberFrom = document.getElementById('txtNumberFrom').value;
	var txtNumberTo = document.getElementById('txtNumberTo').value;
	var txtConfirmationDesc = document.getElementById('txtConfirmationDesc').value;
	var txtConfirmationType = document.getElementById('slConfirmationType').value;

	var dvConfirmationID = document.getElementsByClassName('dvConfirmationID');
	var dvNumberFrom = document.getElementsByClassName('dvNumberFrom');
	var dvNumberTo = document.getElementsByClassName('dvNumberTo');
	var dvConfirmationType = document.getElementsByClassName('dvConfirmationType');
	
    if(!txtConfirmationID.match(/\S/)) {
    	$("#txtConfirmationID").focus();
    	$('#dvConfirmationID').addClass('has-error');
    	$('#mrkConfirmationID').show();
        return false;
    } 
    
    if(!txtNumberFrom.match(/\S/)) {    	
    	$('#txtNumberFrom').focus();
    	$('#dvNumberFrom').addClass('has-error');
    	$('#mrkNumberFrom').show();
        return false;
    } 
    
    if(!txtNumberTo.match(/\S/)) {
    	$('#txtNumberTo').focus();
    	$('#dvNumberTo').addClass('has-error');
    	$('#mrkNumberTo').show();
        return false;
    } 
    
    if(!txtConfirmationType.match(/\S/)) {
    	$('#slConfirmationType').focus();
    	$('#dvConfirmationType').addClass('has-error');
    	$('#mrkConfirmationType').show();
        return false;
    } 
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/BomConfirmationDocument',	
        type:'POST',
        data:{"key":lParambtn,"txtConfirmationID":txtConfirmationID,"txtNumberFrom":txtNumberFrom,"txtNumberTo":txtNumberTo,"txtConfirmationDesc":txtConfirmationDesc,"txtConfirmationType":txtConfirmationType},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertConfimationDoc')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan dokumen konfirmasi";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtConfirmationID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateBOM')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui dokumen konfirmasi";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtNumberFrom").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/BomConfirmationDocument';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}

</script>


</body>
</html>