<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Stock Transfer Detail</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<style type="text/css">	

/* give the modal scroll, Enable scroll for modal after showing another modal  */
 #ModalUpdateInsert { overflow-y:scroll }
 
/*  css for loading bar */
 .loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 60px;
  height: 60px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
/* end of css loading bar */

 </style>
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<%@ include file="/mainform/pages/master_header.jsp"%>
<form id="StockTransferDetail" name="StockTransferDetail" action = "${pageContext.request.contextPath}/StockTransferDetail" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">
		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Stock Transfer Detail <br> <br><small style="color: black; font-weight: bold;">Transfer ID : <c:out value="${transferid}"/></small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
						
						<!-- declare docNo from servlet -->
						<c:set var="transferid" value="${transferid}"></c:set>
						
							<c:if test="${condition == 'SuccessInsertStockTransfer'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan dokumen pindah gudang. Silahkan mengisi detil dokumen.
              				</div>
	      					</c:if>
	      					
							<c:if test="${condition == 'SuccessInsertStockTransferDetail'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan dokumen detil pindah gudang.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'empty_condition'}">
	    					  <script>$('#alrUpdate').hide();</script>
	      					</c:if>
	     					
	     					<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
			
	      								<div class="modal-body">
	        								
	          								<div id="dvTransferID" class="form-group col-xs-6">
	            								<label for="recipient-name" class="control-label">Transfer ID</label><label id="mrkTransferID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtTransferID" name="txtTransferID" readonly="readonly" value="<c:out value="${transferid}"/>">
	          								</div>
	          								<div id="dvTransferLine" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Transfer Line</label><label id="mrkTransferLine" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtTransferLine" name="txtTransferLine" readonly="readonly" value="<c:out value="${tempdocLine}"/>">
	          								</div>
	          								<div id="dvProductID" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Product ID</label><label id="mrkProductID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblProductName" name="lblProductName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtProductID" name="txtProductID" data-toggle="modal" 
	            								data-target="#ModalGetProductID">
	            								<input type="hidden" class="form-control" id="txtProductNm" name="txtProductNm">
	          								</div>
	          								<div id="dvQty_UOM" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Qty UOM</label><label id="mrkQty_UOM" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtQty_UOM" name="txtQty_UOM" >
	          								</div>
	          								<div id="dvUOM" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">UOM</label><label id="mrkUOM" for="recipient-name" class="control-label"><small>*</small></label>	
	            								
	            								<select id="slMasterUOM" name="slMasterUOM" class="form-control" onfocus="FuncValProduct()">
							                    </select>
           										
	          								</div>
	          								<div id="dvBatch_No" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Batch No</label><label id="mrkBatch_No" for="recipient-name" class="control-label"> <small>*</small></label>	
	            								<input type="text" class="form-control" id="txtBatch_No" name="txtBatch_No" data-toggle="modal" 
	            								data-target="#ModalGetBatchNo" onfocus="FuncValProduct()" readonly="readonly">
	          								</div>
	          								<div id="dvPacking_No" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Packing No</label><label id="mrkPacking_No" for="recipient-name" class="control-label"> <small>*</small></label>	
	            								<input type="text" class="form-control" id="txtPacking_No" name="txtPacking_No" >
	          								</div>
	        								
	        								<div class="row"></div>	 
	        								
      								</div>
      								
      								<div class="modal-footer">
      								
      										<!-- loading bar -->
											<div id="dvloader" class="loader" style></div>
        									<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
									<!--modal Delete -->
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Stock Transfer Detail</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtTransferID" name="temp_txtTransferID"  />
              									<input type="hidden" id="temp_txtTransferLine" name="temp_txtTransferLine"  />
               	 									<p>Are You Sure Delete This Stock Transfer Detail ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								        
								        <!--modal show Product data -->
										<div class="modal fade" id="ModalGetProductID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelProductID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelProductID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         							<table id="tb_master_product" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                  <th>ID</th>
										                  <th>Name</th>
										                  <th>Address</th>
										                  <th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listproduct}" var ="product">
												        <tr>
												        <td><c:out value="${product.id}"/></td>
												        <td><c:out value="${product.title_en}"/></td>
												        <td><c:out value="${product.short_description_en}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('<c:out value="${product.id}"/>','<c:out value="${product.title_en}"/>','')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show Product data -->
								        
								        <!--modal show Batch data -->
										<div class="modal fade bs-example-modal-lg" id="ModalGetBatchNo" tabindex="-1" role="dialog" aria-labelledby="ModalLabelBatchID">
												<div class="modal-dialog modal-lg" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelBatchID"></h4>	
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load batch by product will be load here -->                          
           										<div id="dynamic-content">
           										</div>
           										
<!-- 			         								<table id="tb_master_batch" class="table table-bordered table-hover"> -->
<!-- 										        <thead style="background-color: #d2d6de;"> -->
<!-- 										                <tr> -->
<!-- 										                  <th>Product ID</th> -->
<!-- 										                  <th>Product Name</th> -->
<!-- 										                  <th>Batch No</th> -->
<!-- 										                  <th>Packing No</th> -->
<!-- 										                  <th>Expired Date</th> -->
<!-- 										                  <th>Vendor Batch</th> -->
<!-- 										                  <th>Vendor ID</th> -->
<!-- 										                  <th style="width: 20px"></th> -->
<!-- 										                </tr> -->
<!-- 										        </thead> -->
										        
<!-- 										        <tbody> -->
										        
<%-- 										        <c:forEach items="${listbatch}" var ="batch"> --%>
<!-- 												        <tr> -->
<%-- 												        <td><c:out value="${batch.productID}"/></td> --%>
<%-- 												        <td><c:out value="${batch.productNm}"/></td> --%>
<%-- 												        <td><c:out value="${batch.batch_No}"/></td> --%>
<%-- 												        <td><c:out value="${batch.packing_No}"/></td> --%>
<%-- 												        <td><c:out value="${batch.expired_Date}"/></td> --%>
<%-- 												        <td><c:out value="${batch.vendor_Batch_No}"/></td> --%>
<%-- 												        <td><c:out value="${batch.vendorID}"/></td> --%>
<!-- 												        <td><button type="button" class="btn btn-primary" -->
<!-- 												        			data-toggle="modal" -->
<%-- 												        			onclick="FuncPassString('','','<c:out value="${batch.batch_No}"/>')" --%>
<!-- 												        			data-dismiss="modal" -->
<!-- 												        	><i class="fa fa-fw fa-check"></i></button> -->
<!-- 												        </td> -->
<!-- 										        		</tr> -->
										        		
<%-- 										        </c:forEach> --%>
										        
<!-- 										        </tbody> -->
<!-- 										        </table> -->
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show Batch data -->	
										

							<button id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>TransferID</th>
										<th>TransferLine</th>
										<th>Product ID</th>
										<th>Qty UOM</th>
										<th>UOM</th>
										<th>Qty BaseUOM</th>
										<th>Base UOM</th>
										<th>Batch No</th>
										<th>Packing No</th>
										
									</tr>
								</thead>

								<tbody>
									<c:forEach items="${liststocktransferdetail}" var="stocktransferdetail">
										<tr>
											<td><c:out value="${stocktransferdetail.transferID}" /></td>
											<td><c:out value="${stocktransferdetail.transferLine}" /></td>
											<td><c:out value="${stocktransferdetail.productID}" /></td>
											<td><c:out value="${stocktransferdetail.qty_UOM}" /></td>
											<td><c:out value="${stocktransferdetail.UOM}" /></td>
											<td><c:out value="${stocktransferdetail.qty_BaseUOM}" /></td>
											<td><c:out value="${stocktransferdetail.baseUOM}" /></td>
											<td><c:out value="${stocktransferdetail.batch_No}" /></td>
											<td><c:out value="${stocktransferdetail.packing_No}" /></td>
											
<!-- 											<td> -->
<!-- 													<button id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal"  -->
<!-- 														onclick="FuncButtonUpdate()" -->
<!-- 														data-target="#ModalUpdateInsert"  -->
<%-- 														data-ltransferid='<c:out value="${stocktransferdetail.transferid}" />' --%>
<%-- 														data-ltransferline='<c:out value="${stocktransferdetail.transferline}" />' --%>
<%-- 														data-lproductid='<c:out value="${stocktransferdetail.productID}" />' --%>
<%-- 														data-lqtyuom='<c:out value="${stocktransferdetail.qty_UOM}" />' --%>
<%-- 														data-lqtybaseuom='<c:out value="${stocktransferdetail.qty_BaseUOM}" />' --%>
<%-- 														data-lbatchno='<c:out value="${stocktransferdetail.batch_No}" />' --%>
<%-- 														data-lpackingno='<c:out value="${stocktransferdetail.packing_No}" />'> --%>
<!-- 														<i class="fa fa-edit"></i></button>  -->
<%-- <%-- 												<button id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" onclick="FuncButtonDelete()" data-toggle="modal" data-target="#ModalDelete" data-ldocnumber='<c:out value="${outbound.docNumber}" />'> --%>
<!-- <!-- 												<i class="fa fa-trash"></i> -->
<!-- <!-- 												</button> -->
<!-- 												<button type="button" class="btn btn-default" data-toggle="modal"  -->
<%-- 														data-target="#ModalInboundConfirmation" data-ldocnumber='<c:out value="${outbounddetail.docNumber}" />'> --%>
<!-- 														<i class="fa fa-list-ul"></i> -->
<!-- 												</button> -->
<!-- 											</td> -->
										</tr>

									</c:forEach>
															
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 		$(function () {
  			$("#tb_itemlib").DataTable();
  			$("#tb_master_product").DataTable();
  	  		$("#tb_master_batch").DataTable();
	  	  	$('#M004').addClass('active');
	  		$('#M028').addClass('active');
	  		
	  		$("#dvErrorAlert").hide();
  		});
 		
 		//shortcut for button 'new'
 	    Mousetrap.bind('n', function() {
 	    	FuncButtonNew(),
 	    	$('#ModalUpdateInsert').modal('show')
 	    	});
 		
 	   $('#dvloader').hide();
	</script>
	<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		
 		var modal = $(this);
 		
 		$('#txtProductID').focus();
 		$('#txtProductID').click();
	})
	
	$('#ModalGetProductID').on('shown.bs.modal', function (event) {
	$('#slMasterUOM').find('option').remove();
	});
	</script>
<script>


function FuncClear(){
	$('#mrkTransferID').hide();
	$('#mrkTransferLine').hide();
	$('#mrkProductID').hide();
	$('#mrkQty_UOM').hide();
	$('#mrkUOM').hide();
// 	$('#mrkQty_BaseUOM').hide();
// 	$('#mrkBaseUOM').hide();
	$('#mrkBatch_No').hide();
	$('#mrkPacking_No').hide();
	
	$('#dvTranferID').removeClass('has-error');
	$('#dvTransferLine').removeClass('has-error');
	$('#dvProductID').removeClass('has-error');
	$('#dvQty_UOM').removeClass('has-error');
	$('#dvUOM').removeClass('has-error');
// 	$('#dvQty_BaseUOM').removeClass('has-error');
// 	$('#dvBaseUOM').removeClass('has-error');
	$('#dvBatch_No').removeClass('has-error');
	$('#dvPacking_No').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	//$('#txtTransferID').val('');
// 	$('#txtTransferLine').val('');
	$('#txtProductID').val('');
	$('#txtQty_UOM').val('');
	$('#txtUOM').val('');
// 	$('#txtQty_BaseUOM').val('');
// 	$('#txtBaseUOM').val('');
	$('#txtBatch_No').val('');
	$('#txtPacking_No').val('');
	
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById('lblTitleModal').innerHTML = "Add Stock Transfer Detail";
	document.getElementById('lblProductName').innerHTML = null;

	FuncClear();
// 	$('#txtTransferLine').prop('disabled', true);
}

// function FuncButtonUpdate() {
// 	$('#btnSave').hide();
// 	$('#btnUpdate').show();
// 	document.getElementById('lblTitleModal').innerHTML = 'Edit Outbound Detail';

// 	FuncClear();
// 	$('#txtDocNumber').prop('disabled', true);
	
// 	$('#ModalUpdateInsert').on('show.bs.modal', function (event) {
//  		var button = $(event.relatedTarget);
//  		var lDocNumber = button.data('ldocnumber');
//  		var lDocLine = button.data('ldocline');
//  		var lProductID = button.data('lproductid');
//  		var lQty_UOM = button.data('lqtyuom');
//  		var lQty_BaseUOM = button.data('lqtybaseuom');
//  		var lBatch_No = button.data('lbatchno');
//  		var lPacking_No = button.data('lpackingno');
 		
//  		var modal = $(this);
 		
//  		if(lDocNumber == undefined)
//  		{
//  		}
//  		else
//  		{
//  			modal.find(".modal-body #txtDocNumber").val(lDocNumber);
//  	 		modal.find(".modal-body #txtDocLine").val(lDocLine);
//  	 		modal.find(".modal-body #txtProductID").val(lProductID);
//  	 		modal.find(".modal-body #txtQty_UOM").val(lQty_UOM);
//  	 		modal.find(".modal-body #txtQty_BaseUOM").val(lQty_BaseUOM);	 		
//  	 		modal.find(".modal-body #txtBatch_No").val(lBatch_No);
//  	 		modal.find(".modal-body #txtPacking_No").val(lPacking_No);
//  		}
// 	})
// }

function FuncPassString(lParamProductID,lParamProductNm,lParamBatchID){
	if (lParamProductID) {
		$("#txtProductID").val(lParamProductID);
		
		FuncShowUOM();
	}
	
	if (lParamProductNm)
// 		$("#txtProductNm").val(lParamProductNm);
		document.getElementById('lblProductName').innerHTML = '(' + lParamProductNm + ')';
	
	if (lParamBatchID)
		$("#txtBatch_No").val(lParamBatchID);
}

function FuncValProduct(){	
	var txtProductID = document.getElementById('txtProductID').value;
// 	var txtProductNm = document.getElementById('txtProductNm').value;
	
// 	var table = $("#tb_master_batch").DataTable();
// 	table.search( txtProductNm + " " ).draw();
	
	FuncClear();
	
	if(!txtProductID.match(/\S/)) {    	
    	$('#txtProductID').focus();
    	$('#dvProductID').addClass('has-error');
    	$('#mrkProductID').show();
    	
    	alert("Fill Product ID First ...!!!");
        return false;
    } 
	
    return true;
	
}

function FuncValEmptyInput(lParambtn) {
	var txtTransferID = document.getElementById('txtTransferID').value;
	var txtTransferLine = document.getElementById('txtTransferLine').value;
	var txtProductID = document.getElementById('txtProductID').value;
	var txtQty_UOM = document.getElementById('txtQty_UOM').value;
	var slMasterUOM = document.getElementById('slMasterUOM').value;
// 	var txtQty_BaseUOM = document.getElementById('txtQty_BaseUOM').value;
	var txtBatch_No = document.getElementById('txtBatch_No').value;
	var txtPacking_No = document.getElementById('txtPacking_No').value;

	var dvDocNumber = document.getElementsByClassName('dvDocNumber');
	var dvDocLine = document.getElementsByClassName('dvDocLine');
	var dvProductID = document.getElementsByClassName('dvProductID');
	var dvQty_UOM = document.getElementsByClassName('dvQty_UOM');
	var dvUOM = document.getElementsByClassName('dvUOM');
// 	var dvQty_BaseUOM = document.getElementsByClassName('dvQty_BaseUOM');
	var dvBatch_No = document.getElementsByClassName('dvBatch_No');
	var dvPacking_No = document.getElementsByClassName('dvPacking_No');
	
	if(lParambtn == 'save'){
		$('#txtTransferID').prop('disabled', false);
		$('#txtTransferLine').prop('disabled', false);
	}
	else{
		$('#txtTransferID').prop('disabled', true);
		$('#txtTransferLine').prop('disabled', true);
	}
	
    if(!txtTransferID.match(/\S/)) {
    	$("#txtTransferID").focus();
    	$('#dvTransferID').addClass('has-error');
    	$('#mrkTransferID').show();
        return false;
    } 
    
    if(!txtTransferLine.match(/\S/)) {    	
    	$('#txtTransferLine').focus();
    	$('#dvTransferLine').addClass('has-error');
    	$('#mrkTransferLine').show();
        return false;
    } 
    
    if(!txtProductID.match(/\S/)) {
    	$('#txtProductID').focus();
    	$('#dvProductID').addClass('has-error');
    	$('#mrkProductID').show();
        return false;
    } 
	
    if(!txtQty_UOM.match(/\S/)) {
    	$('#txtQty_UOM').focus();
    	$('#dvQty_UOM').addClass('has-error');
    	$('#mrkQty_UOM').show();
        return false;
    } 
    
    if(!slMasterUOM.match(/\S/)) {
    	$('#slMasterUOM').focus();
    	$('#dvUOM').addClass('has-error');
    	$('#mrkUOM').show();
        return false;
    }
    
//     if(!txtQty_BaseUOM.match(/\S/)) {
//     	$('#txtQty_BaseUOM').focus();
//     	$('#dvQty_BaseUOM').addClass('has-error');
//     	$('#mrkQty_BaseUOM').show();
//         return false;
//     } 
    
    if(!txtBatch_No.match(/\S/)) {
    	$('#txtBatch_No').focus();
    	$('#dvBatch_No').addClass('has-error');
    	$('#mrkBatch_No').show();
        return false;
    } 
    
    if(!txtPacking_No.match(/\S/)) {
    	$('#txtPacking_No').focus();
    	$('#dvPacking_No').addClass('has-error');
    	$('#mrkPacking_No').show();
        return false;
    }
    
    $('#dvloader').show();
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/StockTransferDetail',	
        type:'POST',
        data:{"key":lParambtn,"txtTransferID":txtTransferID,"txtTransferLine":txtTransferLine,"txtProductID":txtProductID,"txtQty_UOM":txtQty_UOM,"txtUOM":slMasterUOM,"txtBatch_No":txtBatch_No,"txtPacking_No":txtPacking_No},
//         "txtQty_BaseUOM":txtQty_BaseUOM
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertStockTransferDetail')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan dokumen detil pindah gudang";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtProductID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		$('#dvloader').hide();
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedConversion')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				//$("#txtQty").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		$('#dvloader').hide();
        		return false;
        	}
        	else if(data.split("--")[0] == 'SuccessInsertStockTransferDetail-1')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Sukses menambahkan dokumen detil pindah gudang";
        		document.getElementById("lblAlertDescription").innerHTML = " Tetapi terdapat kesalahan saat ingin memperbaharui/menambah data stok produk yang bersangkutan di server e-commerce";
				//$("#txtQty").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		$('#dvloader').hide();
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/StockTransferDetail';  
	        	$(location).attr('href', url);
        	}
        },
        
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
            $('#dvloader').hide();
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<!-- get batch from product id script -->
<script>
$(document).ready(function(){

    $(document).on('click', '#txtBatch_No', function(e){
  
     e.preventDefault();
  
//      var plantid = $(this).data('id'); // get id of clicked row
		var productid = document.getElementById('txtProductID').value;
  
     $('#dynamic-content').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getbatch',
          type: 'POST',
          data: 'productid='+productid,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content').html(''); // blank before load.
          $('#dynamic-content').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<!-- get uom from product id -->
 	<script>
 	function FuncShowUOM(){	
 		$('#slMasterUOM').find('option').remove();
 		
		var paramproductid = document.getElementById('txtProductID').value;
 
		         $.ajax({
		              url: '${pageContext.request.contextPath}/getuom',
		              type: 'POST',
		              data: {productid : paramproductid},
		              dataType: 'json'
		         })
		         .done(function(data){
		              console.log(data);
		
		             //for json data type
		             for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slMasterUOM').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
		        
		    }
		             
		         })
		         .fail(function(){
		        	 console.log('Service call failed!');
		         });
 	}
 </script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtBatch_No:focus').length) {
    	$('#txtBatch_No').click();
    }
});
</script>

</body>
</html>