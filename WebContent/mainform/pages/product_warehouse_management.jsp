<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Product Warehouse Management</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="formproductwm" name="formproductwm" action = "${pageContext.request.contextPath}/productWM" method="post">
		<input type="hidden" name="temp_string" value="" />
	
		<div class="wrapper">	
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					Product Warehouse Management
				</h1>
				</section>
	
				<!-- Main content -->
				<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<c:if test="${condition == 'SuccessInsertProductWM'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menambahkan produk.
	           						</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessUpdateProductWM'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses memperbaharui produk.
	              					</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessDeleteProductWM'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menghapus produk.
	              					</div>
		      					</c:if>
		      					
		      					<c:if test="${condition == 'FailedDeleteProductWM'}">
		      						<div class="alert alert-danger alert-dismissible">
		                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
		                				Gagal menghapus produk. <c:out value="${conditionDescription}"/>.
	              					</div>
		     					</c:if>
								
								<!--modal update & Insert -->
								<div class="modal fade bs-example-modal-lg" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
										
	  										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				     						</div>
				     					
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
											</div>
		     								<div class="modal-body">
		         								<div id="dvPlantID" class="form-group col-xs-4">
		           								<label for="lblPlantID" class="control-label">Plant ID</label><label id="mrkPlantID" for="formrkPlantID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" 
		           								data-target="#ModalGetPlantID">
		         								</div>
		         								<div id="dvWarehouseID" class="form-group col-xs-4">
		           								<label for="lblWarehouseID" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="formrkWarehouseID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" 
		           								data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
		         								</div>
		         								<div id="dvProductID" class="form-group col-xs-4">
		           								<label for="lblProductID" class="control-label">Product ID</label><label id="mrkProductID" for="formrkProductID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblProductName" name="lblProductName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtProductID" name="txtProductID" data-toggle="modal" 
		           								data-target="#ModalGetProductID">
		         								</div>
		         								
		         								<div class="form-group col-xs-12">
			         								<!-- General Data Panel -->
			          								<label for="recipient-name" class="control-label">General Data</label>
					   								<div class="panel panel-primary" id="GeneralDataPanel">
					   								<div class="panel-body fixed-panel">
			         								
			         								<div id="dvBaseUOM" class="form-group col-xs-3">
			           								<label class="control-label">Base UOM</label><label id="mrkBaseUOM" class="control-label"><small>*</small></label>			           								
			           								<input type="text" class="form-control" id="txtBaseUOM" name="txtBaseUOM" onfocus="FuncValProduct()" readonly="readonly">
			         								</div>
			         								<div id="dvCapacityUsage" class="form-group col-xs-3">
			           								<label class="control-label">Capacity Usage</label><label id="mrkCapacityUsage" class="control-label"><small>*</small></label>	
			           								<input type="number" class="form-control" id="txtCapacityUsage" name="txtCapacityUsage">
			         								</div>
			         								<div id="dvCapacityUsageUOM" class="form-group col-xs-3">
			           								<label class="control-label">Capacity Usage UOM</label><label id="mrkCapacityUsageUOM" class="control-label"><small>*</small></label>	
			           								<select id="slMasterUOM" name="slMasterUOM" class="form-control" onfocus="FuncValProduct()">
							                    	</select>
			         								</div>
			         								<div class="form-group col-xs-3">
			            							<label><input type="checkbox" id="cbApprBatchRecReq" name="cbApprBatchRecReq">Appr.batch rec. req.</label>
													</div>
			         								
			         								</div>
													</div>
													<!-- /.General Data Panel -->
												</div>
												
												<div class="form-group col-xs-12">
													<!-- Stock Strategies Panel -->
			          								<label for="recipient-name" class="control-label">Stock Strategies</label>
					   								<div class="panel panel-primary" id="StockStrategiesPanel">
					   								<div class="panel-body fixed-panel">
					   								
					   								<div id="dvStockRemoval" class="form-group col-xs-3">
			            								<label class="control-label">Stock Removal</label><label id="mrkStockRemoval" class="control-label"><small>*</small></label>	
			           									<input type="text" class="form-control" id="txtStockRemoval" name="txtStockRemoval" data-toggle="modal" 
		           										data-target="#ModalGetStorageTypeIndicator" onfocus="FuncValPlantWarehouse()">
													</div>
													<div id="dvStockPlacement" class="form-group col-xs-3">
														<label class="control-label">Stock Placement</label><label id="mrkStockPlacement" class="control-label"><small>*</small></label>	
			           									<input type="text" class="form-control" id="txtStockPlacement" name="txtStockPlacement" data-toggle="modal" 
		           										data-target="#ModalGetStorageTypeIndicator" onfocus="FuncValPlantWarehouse()">
													</div>
													<div id="dvStorageSectionIndicator" class="form-group col-xs-3">
														<label class="control-label">Storage Section Indicator</label><label id="mrkStorageSectionIndicator" class="control-label"><small>*</small></label>	
				           								<input type="text" class="form-control" id="txtStorageSectionIndicator" name="txtStorageSectionIndicator" data-toggle="modal" 
				           								data-target="#ModalGetStorageSectionIndicator" onfocus="FuncValPlantWarehouse()">
		          									</div>
		          									<div id="dvStorageBin" class="form-group col-xs-3">
														<label class="control-label">Storage Bin</label><label id="mrkStorageBin" class="control-label"><small>*</small></label>	
				           								<input type="text" class="form-control" id="txtStorageBin" name="txtStorageBin" data-toggle="modal" 
				           								data-target="#ModalGetStorageBin" onfocus="FuncValPlantWarehouse()">
		          									</div>
					   								
					   								</div>
													</div>
													<!-- /.Stock Strategies Panel -->
												</div>
												
												<div class="form-group col-xs-12">
													<!-- Palletization Data Panel -->
			          								<label for="recipient-name" class="control-label">Palletization Data</label>
					   								<div class="panel panel-primary" id="PalletizationDataPanel">
					   								<div class="panel-body fixed-panel">
					   								
					   								<div id="dvLEQty1" class="form-group col-xs-4">
			            								<label class="control-label">LE Quantity 1</label><label id="mrkLEQty1" class="control-label"><small>*</small></label>	
			           									<input type="number" class="form-control" id="txtLEQty1" name="txtLEQty1">
													</div>
													<div id="dvUOMLEQty1" class="form-group col-xs-4">
			            								<label class="control-label">UOM LE Quantity 1</label>	
			           									<select id="slMasterUOMLEQty1" name="slMasterUOMLEQty1" class="form-control" onfocus="FuncValProduct()">
							                    		</select>
													</div>
													<div id="dvSUTLEQty1" class="form-group col-xs-4">
			            								<label class="control-label">SUT LE Quantity 1</label><label id="mrkSUTLEQty1" class="control-label"><small>*</small></label>	
			           									<input type="text" class="form-control" id="txtSUTLEQty1" name="txtSUTLEQty1" data-toggle="modal" 
				           								data-target="#ModalGetStorageUnitType" onfocus="FuncValPlantWarehouse()">
													</div>
													
													<div id="dvLEQty2" class="form-group col-xs-4">
			            								<label class="control-label">LE Quantity 2</label><label id="mrkLEQty2" class="control-label"><small>*</small></label>	
			           									<input type="number" class="form-control" id="txtLEQty2" name="txtLEQty2">
													</div>
													<div id="dvUOMLEQty2" class="form-group col-xs-4">
			            								<label class="control-label">UOM LE Quantity 2</label><label id="mrkUOMLEQty2" class="control-label"><small>(tidak boleh sama)</small></label>	
			           									<select id="slMasterUOMLEQty2" name="slMasterUOMLEQty2" class="form-control" onfocus="FuncValProduct()">
							                    		</select>
													</div>
													<div id="dvSUTLEQty2" class="form-group col-xs-4">
			            								<label class="control-label">SUT LE Quantity 2</label><label id="mrkSUTLEQty2" class="control-label"><small>*</small></label>	
			           									<input type="text" class="form-control" id="txtSUTLEQty2" name="txtSUTLEQty2" data-toggle="modal" 
				           								data-target="#ModalGetStorageUnitType" onfocus="FuncValPlantWarehouse()">
													</div>
													
													<div id="dvLEQty3" class="form-group col-xs-4">
			            								<label class="control-label">LE Quantity 3</label><label id="mrkLEQty3" class="control-label"><small>*</small></label>	
			           									<input type="number" class="form-control" id="txtLEQty3" name="txtLEQty3">
													</div>
													<div id="dvUOMLEQty3" class="form-group col-xs-4">
			            								<label class="control-label">UOM LE Quantity 3</label><label id="mrkUOMLEQty3" class="control-label"><small>(tidak boleh sama)</small></label>	
			           									<select id="slMasterUOMLEQty3" name="slMasterUOMLEQty3" class="form-control" onfocus="FuncValProduct()">
							                    		</select>
													</div>
													<div class="form-group col-xs-4">
			            								<label class="control-label">SUT LE Quantity 3</label>	
			           									<input type="text" class="form-control" id="txtSUTLEQty3" name="txtSUTLEQty3" data-toggle="modal" 
				           								data-target="#ModalGetStorageUnitType" onfocus="FuncValPlantWarehouse()">
													</div>
					   								
					   								</div>
					   								</div>
					   								<!-- /.Palletization Data Panel -->
					   							</div>
	          									
	          									<div class="row"></div>
		    								</div>
		    								<div class="modal-footer">
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
		      									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    								</div>
										</div>
									</div>
								</div>
								<!-- end of update insert modal -->
								
								<!--modal Delete -->
								<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
	 										<div class="modal-header">            											
	   										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	     										<span aria-hidden="true">&times;</span></button>
	   										<h4 class="modal-title">Alert Delete Product</h4>
	 										</div>
		 									<div class="modal-body">
			 									<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"  />
			 									<input type="hidden" id="temp_txtWarehouseID" name="temp_txtWarehouseID"  />
			 									<input type="hidden" id="temp_txtProductID" name="temp_txtProductID"  />
			  	 									<p>Are you sure to delete this product ?</p>
		 									</div>
		 									<div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline">Delete</button>
							              	</div>
							            </div>
							            <!-- /.modal-content -->
									</div>
							          <!-- /.modal-dialog -->
						        </div>
						        <!-- /.modal -->   
							        
						        <!--modal show plant data -->
								<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
									<div class="modal-dialog" role="document">
	 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelPlantID">Data Plant</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<table id="tb_master_plant" class="table table-bordered table-hover">
											        <thead style="background-color: #d2d6de;">
											                <tr>
											                  <th>ID</th>
											                  <th>Name</th>
											                  <th>Description</th>
											                  <th style="width: 20px"></th>
											                </tr>
											        </thead>
											        <tbody>
												        <c:forEach items="${listPlant}" var ="plant">
													        <tr>
														        <td><c:out value="${plant.plantID}"/></td>
														        <td><c:out value="${plant.plantNm}"/></td>
														        <td><c:out value="${plant.desc}"/></td>
														        <td><button type="button" class="btn btn-primary"
														        			data-toggle="modal"
														        			onclick="FuncPassStringPlant('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
														        			data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
														        </td>
											        		</tr>
												        </c:forEach>
											        </tbody>
									        	</table>
		    								</div>
	   										<div class="modal-footer"></div>
	 										</div>
									</div>
								</div>
								<!-- /. end of modal show plant data -->
								
								<!--modal show warehouse data -->
								<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelWarehouseID">Data Warehouse</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content-warehouse">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show warehouse data -->
								
								<!--modal show product data -->
								<div class="modal fade" id="ModalGetProductID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelProductID">
										<div class="modal-dialog" role="document">
	  										<div class="modal-content">
	    											<div class="modal-header">
	      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      											<h4 class="modal-title" id="ModalLabelProductID">Data Product</h4>	
	      												
	      											
	    											</div>
	     								<div class="modal-body">
	       								
	         								<table id="tb_master_product" class="table table-bordered table-hover">
								        <thead style="background-color: #d2d6de;">
								                <tr>
								                <th>Product ID</th>
												<th>Product Name</th>
												<th>Description</th>
												<th style="width: 20px"></th>
								                </tr>
								        </thead>
								        
								        <tbody>
								        
								        <c:forEach items="${listProduct}" var ="product">
										        <tr>
										        <td><c:out value="${product.id}" /></td>
												<td><c:out value="${product.title_en}" /></td>
												<td><c:out value="${product.short_description_en}" /></td>
										        <td><button type="button" class="btn btn-primary"
										        			data-toggle="modal"
										        			onclick="FuncPassStringProduct('<c:out value="${product.id}"/>','<c:out value="${product.title_en}"/>','<c:out value="${product.baseUOM}"/>')"
										        			data-dismiss="modal"
										        	><i class="fa fa-fw fa-check"></i></button>
										        </td>
								        		</tr>
								        		
								        </c:forEach>
								        
								        </tbody>
								        </table>
	       								
	    								</div>
	    								
	    								<div class="modal-footer">
	      								
	    								</div>
	  										</div>
											</div>
								</div>
								<!-- /. end of modal show product data -->
								
								<!-- modal show storage type indicator data -->
								<div class="modal fade" id="ModalGetStorageTypeIndicator" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageTypeIndicator">
								<div class="modal-dialog" role="document">
								  	 <div class="modal-content">
								    	 <div class="modal-header">
								      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								      	 <h4 class="modal-title" id="ModalLabelStorageTypeIndicator">Data Storage Type Indicator</h4>	
								    	 </div>
								     	 <div class="modal-body">
								       		<!-- mysql data load Storage Type Indicator by plantid and warehouseid will be load here -->                          
       										<div id="dynamic-content-storage-type-indicator">
       										</div>
								    	 </div>
								    	
								    	 <div class="modal-footer">
								      	
								    	 </div>
								  	 </div>
								</div>
								</div>
								<!-- /. end of modal storage type indicator data -->
								
								<!-- modal show storage section indicator data -->
								<div class="modal fade" id="ModalGetStorageSectionIndicator" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageSectionIndicator">
								<div class="modal-dialog" role="document">
								  	 <div class="modal-content">
								    	 <div class="modal-header">
								      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								      	 <h4 class="modal-title" id="ModalLabelStorageSectionIndicator">Data Storage Section Indicator</h4>	
								    	 </div>
								     	 <div class="modal-body">
								       		<!-- mysql data load Storage Section Indicator by plantid and warehouseid will be load here -->                          
       										<div id="dynamic-content-storage-section-indicator">
       										</div>
								    	 </div>
								    	
								    	 <div class="modal-footer">
								      	
								    	 </div>
								  	 </div>
								</div>
								</div>
								<!-- /. end of modal storage section indicator data -->
								
								<!-- modal show storage bin data -->
								<div class="modal fade" id="ModalGetStorageBin" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageBin">
								<div class="modal-dialog" role="document">
								  	 <div class="modal-content">
								    	 <div class="modal-header">
								      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								      	 <h4 class="modal-title" id="ModalLabelStorageBin">Data Storage Bin</h4>	
								    	 </div>
								     	 <div class="modal-body">
								       		<!-- mysql data load Storage Bin by plantid and warehouseid will be load here -->                          
       										<div id="dynamic-storage-bin">
       										</div>
								    	 </div>
								    	
								    	 <div class="modal-footer">
								      	
								    	 </div>
								  	 </div>
								</div>
								</div>
								<!-- /. end of modal storage bin data -->
								
								<!-- modal show storage unit type data -->
								<div class="modal fade" id="ModalGetStorageUnitType" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageUnitType">
								<div class="modal-dialog" role="document">
								  	 <div class="modal-content">
								    	 <div class="modal-header">
								      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								      	 <h4 class="modal-title" id="ModalLabelStorageUnitType">Data Storage Unit Type</h4>	
								    	 </div>
								     	 <div class="modal-body">
								       		<!-- mysql data load Storage Unit Type by plantid and warehouseid will be load here -->                          
       										<div id="dynamic-content-storage-unit-type">
       										</div>
								    	 </div>
								    	
								    	 <div class="modal-footer">
								      	
								    	 </div>
								  	 </div>
								</div>
								</div>
								<!-- /. end of modal storage unit type data -->
								
								<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
								<table id="tb_product_wm" class="table table-bordered table-striped table-hover">
									<thead style="background-color: #d2d6de;">
										<tr>
											<th>Plant ID</th>
											<th>Warehouse ID</th>
											<th>Product ID</th>
											<th style="width:60px;"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listProductWM}" var="productwm">
											<tr>
												<td><c:out value="${productwm.plantID}" /></td>
												<td><c:out value="${productwm.warehouseID}" /></td>
												<td><c:out value="${productwm.productID}" /></td>
												<td><button <c:out value="${buttonstatus}"/>
															id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
															onclick="FuncButtonUpdate('<c:out value="${productwm.productID}" />')"
															data-target="#ModalUpdateInsert"
															data-lproductid='<c:out value="${productwm.productID}" />'
															data-lproductname='<c:out value="${productwm.productName}" />' 
															data-lplantid='<c:out value="${productwm.plantID}" />'
															data-lplantname='<c:out value="${productwm.plantName}" />'
															data-lwarehouseid='<c:out value="${productwm.warehouseID}" />'
															data-lwarehousename='<c:out value="${productwm.warehouseName}" />'
															data-lbaseuom='<c:out value="${productwm.baseUOM}" />'
															data-lcapacityusage='<c:out value="${productwm.capacityUsage}" />'
															data-lcapacityusageuom='<c:out value="${productwm.capacityUsageUOM}" />'
															data-lapprbatchrecreq='<c:out value="${productwm.apprBatchRecReq}" />'
															data-lstockremoval='<c:out value="${productwm.stockRemoval}" />'
															data-lstockplacement='<c:out value="${productwm.stockPlacement}" />'
															data-lstoragesectionindicator='<c:out value="${productwm.storageSectionIndicator}" />'
															data-lstoragebin='<c:out value="${productwm.storageBin}" />'
															data-lleqty1='<c:out value="${productwm.LEQty1}" />'
															data-lleqty2='<c:out value="${productwm.LEQty2}" />'
															data-lleqty3='<c:out value="${productwm.LEQty3}" />'
															data-luomleqty1='<c:out value="${productwm.UOMLEQty1}" />'
															data-luomleqty2='<c:out value="${productwm.UOMLEQty2}" />'
															data-luomleqty3='<c:out value="${productwm.UOMLEQty3}" />'
															data-lsutleqty1='<c:out value="${productwm.SUTLEQty1}" />'
															data-lsutleqty2='<c:out value="${productwm.SUTLEQty2}" />'
															data-lsutleqty3='<c:out value="${productwm.SUTLEQty3}" />'
															>
															<i class="fa fa-edit"></i></button> 
													<button <c:out value="${buttonstatus}"/>
															id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" 
															data-toggle="modal" 
															data-target="#ModalDelete"
															data-lplantid='<c:out value="${productwm.plantID}" />' 
															data-lwarehouseid='<c:out value="${productwm.warehouseID}" />'
															data-lproductid='<c:out value="${productwm.productID}" />'
															>
													<i class="fa fa-trash"></i>
													</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row --> 
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
	
			<%@ include file="/mainform/pages/master_footer.jsp"%>
	
		</div>
		<!-- ./wrapper -->
	</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
	 	$(function () {
	 		$("#tb_product_wm").DataTable();
	  		$("#tb_master_plant").DataTable();
	  		$("#tb_master_product").DataTable();
	  		$('#M003').addClass('active');
	  		$('#M056').addClass('active');
	  		
	  	//shortcut for button 'new'
		    Mousetrap.bind('n', function() {
		    	FuncButtonNew(),
		    	$('#ModalUpdateInsert').modal('show')
		    	});
  		});

		$('#ModalDelete').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var lPlantID = button.data('lplantid');
			var lWarehouseID = button.data('lwarehouseid');
			var lProductID = button.data('lproductid');
			$("#temp_txtPlantID").val(lPlantID);
			$("#temp_txtWarehouseID").val(lWarehouseID);
			$("#temp_txtProductID").val(lProductID);
		})

		$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
			FuncClear();
			
	 		var button = $(event.relatedTarget);
	 		
	 		var lPlantID = button.data('lplantid');
	 		var lPlantName = button.data('lplantname');
	 		var lWarehouseID = button.data('lwarehouseid');
	 		var lWarehouseName = button.data('lwarehousename');
	 		var lProductID = button.data('lproductid');
	 		var lProductName = button.data('lproductname');
	 		var lBaseUOM = button.data('lbaseuom');
	 		var lCapacityUsage = button.data('lcapacityusage');
	 		var lCapacityUsageUOM = button.data('lcapacityusageuom');
	 		var lApprBatchRecReq = button.data('lapprbatchrecreq');
	 		var lStockRemoval = button.data('lstockremoval');
	 		var lStockPlacement = button.data('lstockplacement');
	 		var lStorageSectionIndicator = button.data('lstoragesectionindicator');
	 		var lStorageBin = button.data('lstoragebin');
	 		var lLEQty1 = button.data('lleqty1');
	 		var lLEQty2 = button.data('lleqty2');
	 		var lLEQty3 = button.data('lleqty3');
	 		var lUOMLEQty1 = button.data('luomleqty1');
	 		var lUOMLEQty2 = button.data('luomleqty2');
	 		var lUOMLEQty3 = button.data('luomleqty3');
	 		var lSUTLEQty1 = button.data('lsutleqty1');
	 		var lSUTLEQty2 = button.data('lsutleqty2');
	 		var lSUTLEQty3 = button.data('lsutleqty3');
	 		
	 		var modal = $(this);
	 		
	 		modal.find(".modal-body #txtPlantID").val(lPlantID);
	 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
	 		modal.find(".modal-body #txtProductID").val(lProductID);
	 		modal.find(".modal-body #txtBaseUOM").val(lBaseUOM);
	 		modal.find(".modal-body #txtCapacityUsage").val(lCapacityUsage);
	 		modal.find(".modal-body #slMasterUOM").val(lCapacityUsageUOM);
	 		modal.find(".modal-body #txtStockRemoval").val(lStockRemoval);
	 		modal.find(".modal-body #txtStockPlacement").val(lStockPlacement);
	 		modal.find(".modal-body #txtStorageSectionIndicator").val(lStorageSectionIndicator);
	 		modal.find(".modal-body #txtStorageBin").val(lStorageBin);
	 		modal.find(".modal-body #txtLEQty1").val(lLEQty1);
	 		modal.find(".modal-body #txtLEQty2").val(lLEQty2);
	 		modal.find(".modal-body #txtLEQty3").val(lLEQty3);
			modal.find(".modal-body #slMasterUOMLEQty1").val(lUOMLEQty1);
	 		modal.find(".modal-body #slMasterUOMLEQty2").val(lUOMLEQty2);
	 		modal.find(".modal-body #slMasterUOMLEQty3").val(lUOMLEQty3);
	 		modal.find(".modal-body #txtSUTLEQty1").val(lSUTLEQty1);
	 		modal.find(".modal-body #txtSUTLEQty2").val(lSUTLEQty2);
	 		modal.find(".modal-body #txtSUTLEQty3").val(lSUTLEQty3);
 			
	 		if(lPlantName != null)
	 			document.getElementById("lblPlantName").innerHTML = '(' + lPlantName + ')';
	 		if(lWarehouseName != null)
	 			document.getElementById("lblWarehouseName").innerHTML = '(' + lWarehouseName + ')';
	 		if(lProductName != null)
	 			document.getElementById("lblProductName").innerHTML = '(' + lProductName + ')';
	 			
	 		if(lApprBatchRecReq==true)
 				modal.find(".modal-body #cbApprBatchRecReq").prop("checked", true);
	 			
	 		
	 		if(lPlantID == null || lPlantID == '')
	 			{
	 				$("#txtPlantID").focus(); 
	 				$("#txtPlantID").click();
	 			}
	 		else
	 			$("#txtCapacityUsage").focus();
		});
		
		function FuncClear(){
			$('#mrkPlantID').hide();
			$('#mrkWarehouseID').hide();
			$('#mrkProductID').hide();
			$('#mrkBaseUOM').hide();
			$('#mrkCapacityUsage').hide();
			$('#mrkCapacityUsageUOM').hide();
			$('#mrkStockRemoval').hide();
			$('#mrkStockPlacement').hide();
			$('#mrkStorageSectionIndicator').hide();
			$('#mrkStorageBin').hide();
			$('#mrkLEQty1').hide();
			$('#mrkLEQty2').hide();
			$('#mrkLEQty3').hide();
			$('#mrkUOMLEQty2').hide();
			$('#mrkUOMLEQty3').hide();
			$('#mrkSUTLEQty1').hide();
			$('#mrkSUTLEQty2').hide();
			
			$('#dvPlantID').removeClass('has-error');
			$('#dvWarehouseID').removeClass('has-error');
			$('#dvProductID').removeClass('has-error');
			$('#dvBaseUOM').removeClass('has-error');
			$('#dvCapacityUsage').removeClass('has-error');
			$('#dvCapacityUsageUOM').removeClass('has-error');
			$('#dvStockRemoval').removeClass('has-error');
			$('#dvStockPlacement').removeClass('has-error');
			$('#dvStorageSectionIndicator').removeClass('has-error');
			$('#dvStorageBin').removeClass('has-error');
			$('#dvLEQty1').removeClass('has-error');
			$('#dvLEQty2').removeClass('has-error');
			$('#dvLEQty3').removeClass('has-error');
			$('#dvUOMLEQty1').removeClass('has-error');
			$('#dvUOMLEQty2').removeClass('has-error');
			$('#dvUOMLEQty3').removeClass('has-error');
			$('#dvSUTLEQty1').removeClass('has-error');
			$('#dvSUTLEQty2').removeClass('has-error');
			
			document.getElementById("lblPlantName").innerHTML = null;
			document.getElementById("lblWarehouseName").innerHTML = null;
			document.getElementById("lblProductName").innerHTML = null;
			
			$("#cbApprBatchRecReq").prop("checked", false);
			
			$("#dvErrorAlert").hide();
		}
	
		function FuncButtonNew() {
			$('#btnSave').show();
			$('#btnUpdate').hide();
			document.getElementById("lblTitleModal").innerHTML = "Product Setting For Warehouse Management";
		
			$('#txtPlantID').prop('disabled', false);
			$('#txtWarehouseID').prop('disabled', false);
			$('#txtProductID').prop('disabled', false);
		}
		
		function FuncButtonUpdate(lProductID) {
			$('#btnSave').hide();
			$('#btnUpdate').show();
			document.getElementById("lblTitleModal").innerHTML = 'Product Setting For Warehouse Management';
		
			$('#txtPlantID').prop('disabled', true);
			$('#txtWarehouseID').prop('disabled', true);
			$('#txtProductID').prop('disabled', true);
			
			$('#txtProductID').val(lProductID);
			FuncShowUOM();
		}
		
		function FuncPassStringPlant(lParamPlantID,lParamPlantName){
			$("#txtPlantID").val(lParamPlantID);
			document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
			$('#mrkPlantID').hide();
			$('#dvPlantID').removeClass('has-error');
		}
		
		function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
			$("#txtWarehouseID").val(lParamWarehouseID);
			document.getElementById("lblWarehouseName").innerHTML = '(' + lParamWarehouseName + ')';
			$('#mrkWarehouseID').hide();
			$('#dvWarehouseID').removeClass('has-error');
		}
		
		function FuncPassStringProduct(lParamID,lParamName,lParamBaseUOM){
			$("#txtProductID").val(lParamID);
			document.getElementById("lblProductName").innerHTML = '(' + lParamName + ')';
			$("#txtBaseUOM").val(lParamBaseUOM);
			$('#mrkStorageTypeID').hide();
			$('#dvStorageTypeID').removeClass('has-error');
			
			FuncShowUOM();
		}
		
		function FuncPassStorageTypeIndicator(lParamID,lParamName,lParamType){
			if(lParamType=='StockRemoval')
			{
				$("#txtStockRemoval").val(lParamID);
				$('#mrkStockRemoval').hide();
				$('#dvStockRemoval').removeClass('has-error');
			}
			else if(lParamType=='StockPlacement')
			{
				$("#txtStockPlacement").val(lParamID);
				$('#mrkStockPlacement').hide();
				$('#dvStockPlacement').removeClass('has-error');
			}
		}
		
		function FuncPassStorageSectionIndicator(lParamID,lParamName){
			$("#txtStorageSectionIndicator").val(lParamID);
			$('#mrkStorageSectionIndicator').hide();
			$('#dvStorageSectionIndicator').removeClass('has-error');
		}
		
		function FuncPassStorageUnitType(lParamID,lParamName,lParamType){
			if(lParamType=='sut1')
			{
				$("#txtSUTLEQty1").val(lParamID);
			}
			else if(lParamType=='sut2')
			{
				$("#txtSUTLEQty2").val(lParamID);
			}
			else if(lParamType=='sut3')
			{
				$("#txtSUTLEQty3").val(lParamID);
			}
		}
		
		function FuncPassStorageBin(lParamID){
			$("#txtStorageBin").val(lParamID);
			$('#mrkStorageBin').hide();
			$('#dvStorageBin').removeClass('has-error');
		}
		
		function FuncValPlant(){	
			var txtPlantID = document.getElementById('txtPlantID').value;
			
			if(!txtPlantID.match(/\S/)) {    	
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    } 
			
		    return true;	
		}
		
		function FuncValPlantWarehouse(){
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			
			if(!txtPlantID.match(/\S/)){
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    }else if(!txtWarehouseID.match(/\S/)){
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		    	
		    	alert("Fill Warehouse ID First ...!!!");
		        return false;
		    }
		    return true;
		}
		
		function FuncValProduct(){	
			var txtProductID = document.getElementById('txtProductID').value;
			
			if(!txtProductID.match(/\S/)) {    	
		    	$('#txtProductID').focus();
		    	$('#dvProductID').addClass('has-error');
		    	$('#mrkProductID').show();
		    	
		    	alert("Fill Product ID First ...!!!");
		        return false;
		    } 
			
		    return true;	
		}
		
		function FuncValEmptyInput(lParambtn) {
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			var txtProductID = document.getElementById('txtProductID').value;
			var txtBaseUOM = document.getElementById('txtBaseUOM').value;
			var txtCapacityUsage = document.getElementById('txtCapacityUsage').value;
			var slMasterUOM = document.getElementById('slMasterUOM').value;
			var txtStockRemoval = document.getElementById('txtStockRemoval').value;
			var txtStockPlacement = document.getElementById('txtStockPlacement').value;
			var txtStorageSectionIndicator = document.getElementById('txtStorageSectionIndicator').value;
			var txtStorageBin = document.getElementById('txtStorageBin').value;
			var txtLEQty1 = document.getElementById('txtLEQty1').value;
			var txtLEQty2 = document.getElementById('txtLEQty2').value;
			var txtLEQty3 = document.getElementById('txtLEQty3').value;
			var slMasterUOMLEQty1 = document.getElementById('slMasterUOMLEQty1').value;
			var slMasterUOMLEQty2 = document.getElementById('slMasterUOMLEQty2').value;
			var slMasterUOMLEQty3 = document.getElementById('slMasterUOMLEQty3').value;
			var txtSUTLEQty1 = document.getElementById('txtSUTLEQty1').value;
			var txtSUTLEQty2 = document.getElementById('txtSUTLEQty2').value;
			var txtSUTLEQty3 = document.getElementById('txtSUTLEQty3').value;
			
			var cbApprBatchRecReq = false;
			
		    if(!txtPlantID.match(/\S/)) {
		    	$("#txtPlantID").focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		        return false;
		    } 
		    
		    if(!txtWarehouseID.match(/\S/)) {    	
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		        return false;
		    }
		    
		    if(!txtProductID.match(/\S/)) {
		    	$('#txtProductID').focus();
		    	$('#dvProductID').addClass('has-error');
		    	$('#mrkProductID').show();
		        return false;
		    }
		    
		    if(!txtCapacityUsage.match(/\S/)) {
		    	$('#txtCapacityUsage').focus();
		    	$('#dvCapacityUsage').addClass('has-error');
		    	$('#mrkCapacityUsage').show();
		        return false;
		    }
		    
		    if(!slMasterUOM.match(/\S/)) {
		    	$('#slMasterUOM').focus();
		    	$('#dvCapacityUsageUOM').addClass('has-error');
		    	$('#mrkCapacityUsageUOM').show();
		        return false;
		    }
		    
		    if(!txtStockRemoval.match(/\S/)) {
		    	$('#txtStockRemoval').focus();
		    	$('#dvStockRemoval').addClass('has-error');
		    	$('#mrkStockRemoval').show();
		        return false;
		    }
		    
		    if(!txtStockPlacement.match(/\S/)) {
		    	$('#txtStockPlacement').focus();
		    	$('#dvStockPlacement').addClass('has-error');
		    	$('#mrkStockPlacement').show();
		        return false;
		    }
		    
		    if(!txtStorageSectionIndicator.match(/\S/)) {
		    	$('#txtStorageSectionIndicator').focus();
		    	$('#dvStorageSectionIndicator').addClass('has-error');
		    	$('#mrkStorageSectionIndicator').show();
		        return false;
		    }
		    
		    if(!txtStorageBin.match(/\S/)) {
		    	$('#txtStorageBin').focus();
		    	$('#dvStorageBin').addClass('has-error');
		    	$('#mrkStorageBin').show();
		        return false;
		    }
		    
		    if(!txtLEQty1.match(/\S/)) {
		    	$('#txtLEQty1').focus();
		    	$('#dvLEQty1').addClass('has-error');
		    	$('#mrkLEQty1').show();
		        return false;
		    }
		    
		    if(!txtLEQty2.match(/\S/)) {
		    	$('#txtLEQty2').focus();
		    	$('#dvLEQty2').addClass('has-error');
		    	$('#mrkLEQty2').show();
		        return false;
		    }
		    
		    if(!txtLEQty3.match(/\S/)) {
		    	$('#txtLEQty3').focus();
		    	$('#dvLEQty3').addClass('has-error');
		    	$('#mrkLEQty3').show();
		        return false;
		    }
		    
		    if(!txtSUTLEQty1.match(/\S/)) {
		    	$('#txtSUTLEQty1').focus();
		    	$('#dvSUTLEQty1').addClass('has-error');
		    	$('#mrkSUTLEQty1').show();
		        return false;
		    }
		    
		    if(!txtSUTLEQty2.match(/\S/)) {
		    	$('#txtSUTLEQty2').focus();
		    	$('#dvSUTLEQty2').addClass('has-error');
		    	$('#mrkSUTLEQty2').show();
		        return false;
		    }
		    
		    if($("#cbApprBatchRecReq").prop('checked') == true)
		    	cbApprBatchRecReq = true;
		    	
		    if(slMasterUOMLEQty1 == slMasterUOMLEQty2){
		    	if((txtLEQty1!=0) && (txtLEQty2!=0)){
			    	$('#slMasterUOMLEQty2').focus();
			    	$('#dvUOMLEQty1').addClass('has-error');
			    	$('#dvUOMLEQty2').addClass('has-error');
			    	$('#mrkUOMLEQty2').show();
			        return false;
		        }
		    }
		    
		     if(slMasterUOMLEQty1 == slMasterUOMLEQty3){
		     	if((txtLEQty1!=0) && (txtLEQty3!=0)){
			    	$('#slMasterUOMLEQty3').focus();
			    	$('#dvUOMLEQty1').addClass('has-error');
			    	$('#dvUOMLEQty3').addClass('has-error');
			    	$('#mrkUOMLEQty3').show();
			        return false;
		        }
		    }
		    
		     if(slMasterUOMLEQty2 == slMasterUOMLEQty3){
		     	if((txtLEQty2!=0) && (txtLEQty3!=0)){
			    	$('#slMasterUOMLEQty3').focus();
			    	$('#dvUOMLEQty2').addClass('has-error');
			    	$('#dvUOMLEQty3').addClass('has-error');
			    	$('#mrkUOMLEQty3').show();
			        return false;
		        }
		    }
		    
		    jQuery.ajax({
		        url:'${pageContext.request.contextPath}/productWM',	
		        type:'POST',
		        data:{"key":lParambtn,"txtPlantID":txtPlantID,"txtWarehouseID":txtWarehouseID,
		        	  "txtProductID":txtProductID,"txtBaseUOM":txtBaseUOM,"txtCapacityUsage":txtCapacityUsage,
		        	  "slMasterUOM":slMasterUOM,"cbApprBatchRecReq":cbApprBatchRecReq,"txtStockRemoval":txtStockRemoval,
		        	  "txtStockPlacement":txtStockPlacement,"txtStorageSectionIndicator":txtStorageSectionIndicator,"txtStorageBin":txtStorageBin,
		        	  "txtLEQty1":txtLEQty1,"txtLEQty2":txtLEQty2,"txtLEQty3":txtLEQty3,
		        	  "slMasterUOMLEQty1":slMasterUOMLEQty1,"slMasterUOMLEQty2":slMasterUOMLEQty2,"slMasterUOMLEQty3":slMasterUOMLEQty3,
		        	  "txtSUTLEQty1":txtSUTLEQty1,"txtSUTLEQty2":txtSUTLEQty2,"txtSUTLEQty3":txtSUTLEQty3},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	if(data.split("--")[0] == 'FailedInsertProductWM')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan produk";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtPlantID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		$('#mrkUOMLEQty2').hide();
		        		$('#mrkUOMLEQty3').hide();
					    $('#dvUOMLEQty1').removeClass('has-error');
						$('#dvUOMLEQty2').removeClass('has-error');
						$('#dvUOMLEQty3').removeClass('has-error');
		        		return false;
		        	}
		        	else if(data.split("--")[0] == 'FailedUpdateProductWM')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui produk";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtCapacityUsage").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		$('#mrkUOMLEQty2').hide();
		        		$('#mrkUOMLEQty3').hide();
					    $('#dvUOMLEQty1').removeClass('has-error');
						$('#dvUOMLEQty2').removeClass('has-error');
						$('#dvUOMLEQty3').removeClass('has-error');
		        		return false;
		        	}
		        	else
		        	{
			        	var url = '${pageContext.request.contextPath}/productWM';  
			        	$(location).attr('href', url);
		        	}
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    
		    return true;
		}
		
		//action when txtCapacityUsage changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtCapacityUsage', function(e){
		  
		     $('#mrkCapacityUsage').hide();
			 $('#dvCapacityUsage').removeClass('has-error');
		
		    });
		});
		
		//action when slMasterUOM changed
		$(document).ready(function(){
	
		    $(document).on('change', '#slMasterUOM', function(e){
		  
		     $('#mrkCapacityUsageUOM').hide();
			 $('#dvCapacityUsageUOM').removeClass('has-error');
		
		    });
		});
		
		//action when txtLEQty1 changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtLEQty1', function(e){
		  
		     $('#mrkLEQty1').hide();
			 $('#dvLEQty1').removeClass('has-error');
		
		    });
		});
		
		//action when txtLEQty2 changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtLEQty2', function(e){
		  
		     $('#mrkLEQty2').hide();
			 $('#dvLEQty2').removeClass('has-error');
		
		    });
		});
		
		//action when txtLEQty3 changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtLEQty3', function(e){
		  
		     $('#mrkLEQty3').hide();
			 $('#dvLEQty3').removeClass('has-error');
		
		    });
		});
		
		//action when txtSUTLEQty1 changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtSUTLEQty1', function(e){
		  
		     $('#mrkSUTLEQty1').hide();
			 $('#dvSUTLEQty1').removeClass('has-error');
		
		    });
		});
		
		//action when txtSUTLEQty1 changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtSUTLEQty2', function(e){
		  
		     $('#mrkSUTLEQty2').hide();
			 $('#dvSUTLEQty2').removeClass('has-error');
		
		    });
		});
		
		//action when slMasterUOMLEQty1 changed
		$(document).ready(function(){
	
		    $(document).on('change', '#slMasterUOMLEQty1', function(e){
		  
		     $('#mrkUOMLEQty1').hide();
		     $('#dvUOMLEQty1').removeClass('has-error');
			 $('#dvUOMLEQty2').removeClass('has-error');
			 $('#dvUOMLEQty3').removeClass('has-error');
		
		    });
		});
		
		//action when slMasterUOMLEQty2 changed
		$(document).ready(function(){
	
		    $(document).on('change', '#slMasterUOMLEQty2', function(e){
		  
		     $('#mrkUOMLEQty2').hide();
		     $('#dvUOMLEQty1').removeClass('has-error');
			 $('#dvUOMLEQty2').removeClass('has-error');
			 $('#dvUOMLEQty3').removeClass('has-error');
		
		    });
		});
		
		//action when slMasterUOMLEQty3 changed
		$(document).ready(function(){
	
		    $(document).on('change', '#slMasterUOMLEQty3', function(e){
		  
		     $('#mrkUOMLEQty3').hide();
		     $('#dvUOMLEQty1').removeClass('has-error');
			 $('#dvUOMLEQty2').removeClass('has-error');
			 $('#dvUOMLEQty3').removeClass('has-error');
		
		    });
		});
		
		//get warehouse from plant id
		$(document).ready(function(){
	
		    $(document).on('click', '#txtWarehouseID', function(e){
		  
		     e.preventDefault();
		  
			 var plantid = document.getElementById('txtPlantID').value;
		  
		     $('#dynamic-content-warehouse').html(''); // leave this div blank
			//$('#modal-loader').show();      // load ajax loader on button click
		 
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getwarehouse',
		          type: 'POST',
		          data: 'plantid='+plantid,
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-warehouse').html(''); // blank before load.
		          $('#dynamic-content-warehouse').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-warehouse').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage type indicator by plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStockRemoval', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-content-storage-type-indicator').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragetypeindicator',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"type":'StockRemoval'},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storage-type-indicator').html(''); // blank before load.
		          $('#dynamic-content-storage-type-indicator').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storage-type-indicator').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage type indicator by plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStockPlacement', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-content-storage-type-indicator').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragetypeindicator',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"type":'StockPlacement'},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storage-type-indicator').html(''); // blank before load.
		          $('#dynamic-content-storage-type-indicator').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storage-type-indicator').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage section indicator by plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStorageSectionIndicator', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-content-storage-section-indicator').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragesectionindicator',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storage-section-indicator').html(''); // blank before load.
		          $('#dynamic-content-storage-section-indicator').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storage-section-indicator').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage bin by plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStorageBin', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-storage-bin').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getStorageBinByPlantWarehouse',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-storage-bin').html(''); // blank before load.
		          $('#dynamic-storage-bin').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-storage-bin').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage unit type 1 by plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtSUTLEQty1', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-content-storage-unit-type').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstorageunittype',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"type":'sut1'},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storage-unit-type').html(''); // blank before load.
		          $('#dynamic-content-storage-unit-type').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storage-unit-type').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage unit type 2 by plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtSUTLEQty2', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-content-storage-unit-type').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstorageunittype',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"type":'sut2'},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storage-unit-type').html(''); // blank before load.
		          $('#dynamic-content-storage-unit-type').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storage-unit-type').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get storage unit type 3 by plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtSUTLEQty3', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-content-storage-unit-type').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstorageunittype',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"type":'sut3'},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-storage-unit-type').html(''); // blank before load.
		          $('#dynamic-content-storage-unit-type').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-storage-unit-type').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get uom from product id
		function FuncShowUOM(){
	 		$('#slMasterUOM').find('option').remove();
	 		$('#slMasterUOMLEQty1').find('option').remove();
	 		$('#slMasterUOMLEQty2').find('option').remove();
	 		$('#slMasterUOMLEQty3').find('option').remove();
	 		
			var paramproductid = document.getElementById('txtProductID').value;
 
		         $.ajax({
		              url: '${pageContext.request.contextPath}/getuom',
		              type: 'POST',
		              data: {productid : paramproductid},
		              dataType: 'json'
		         })
		         .done(function(data){
		              console.log(data);
		
		             //for json data type
		             for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slMasterUOM').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
		        $('#slMasterUOMLEQty1').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
		        $('#slMasterUOMLEQty2').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
		        $('#slMasterUOMLEQty3').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
		        
		    }
		             
	         })
	         .fail(function(){
	        	 console.log('Service call failed!');
	         });
 		}
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtWarehouseID:focus').length) {
		    	$('#txtWarehouseID').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtProductID:focus').length) {
		    	$('#txtProductID').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStockRemoval:focus').length) {
		    	$('#txtStockRemoval').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStockPlacement:focus').length) {
		    	$('#txtStockPlacement').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageSectionIndicator:focus').length) {
		    	$('#txtStorageSectionIndicator').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSUTLEQty1:focus').length) {
		    	$('#txtSUTLEQty1').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSUTLEQty2:focus').length) {
		    	$('#txtSUTLEQty2').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtSUTLEQty3:focus').length) {
		    	$('#txtSUTLEQty3').click();
		    }
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageBin:focus').length) {
		    	$('#txtStorageBin').click();
		    }
		});
	</script>
</body>
</html>