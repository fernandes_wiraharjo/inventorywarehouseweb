<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Reference Movement Type</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="formrefmovementtype" name="formrefmovementtype" action = "${pageContext.request.contextPath}/refmovementtype" method="post">
		<input type="hidden" name="temp_string" value="" />
	
		<div class="wrapper">	
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					Reference Movement Type
				</h1>
				</section>
	
				<!-- Main content -->
				<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<c:if test="${condition == 'SuccessInsertReferenceMovementType'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menambahkan reference movement type.
	           						</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessUpdateReferenceMovementType'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses memperbaharui reference movement type.
	              					</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessDeleteReferenceMovementType'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menghapus reference movement type.
	              					</div>
		      					</c:if>
		      					
		      					<c:if test="${condition == 'FailedDeleteReferenceMovementType'}">
		      						<div class="alert alert-danger alert-dismissible">
		                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
		                				Gagal menghapus reference movement type. <c:out value="${conditionDescription}"/>.
	              					</div>
		     					</c:if>
								
								<!--modal update & Insert -->
								<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
										
	  										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				     						</div>
				     					
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
											</div>
		     								<div class="modal-body">
		         								<div id="dvMovementID" class="form-group">
			           								<label for="lblMovementID" class="control-label">Movement ID</label><label id="mrkMovementID" for="formrkMovementID" class="control-label"><small>*</small></label>	
			           								<input type="text" class="form-control" id="txtMovementID" name="txtMovementID">
		         								</div>
		         								<div id="dvMovement" class="form-group">
			           								<label for="lblMovement" class="control-label">Movement</label><label id="mrkMovement" for="formrkMovement" class="control-label"><small>*</small></label>
			           								<select id="slMovement" name="slMovement" class="form-control">
			           								<option>GR-Inbound</option>
								                    <option>GRC-Inbound Cancel</option>
								                    <option>GI-Outbound</option>
								                    <option>GIC-Outbound Cancel</option>
								                    <option>ST-Stock Transfer</option>
								                    <option>STC-Stock Transfer Cancel</option>
								                    </select>
		         								</div>
		    								</div>
		    								<div class="modal-footer">
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
		      									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    								</div>
										</div>
									</div>
								</div>
								<!-- end of update insert modal -->
								
								<!--modal Delete -->
								<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
	 										<div class="modal-header">            											
	   										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	     										<span aria-hidden="true">&times;</span></button>
	   										<h4 class="modal-title">Alert Delete Reference Movement Type</h4>
	 										</div>
		 									<div class="modal-body">
			 									<input type="hidden" id="temp_txtMovementID" name="temp_txtMovementID"  />
			  	 									<p>Are you sure to delete this reference movement type ?</p>
		 									</div>
		 									<div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline">Delete</button>
							              	</div>
							            </div>
							            <!-- /.modal-content -->
									</div>
							          <!-- /.modal-dialog -->
						        </div>
						        <!-- /.modal -->
						        
								
								<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
								<table id="tb_ref_movementtype" class="table table-bordered table-striped table-hover">
									<thead style="background-color: #d2d6de;">
										<tr>
											<th>Movement Type</th>
											<th>Movement Description</th>
											<th>Movement ID</th>
											<th style="width:60px"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listReferenceMovementType}" var="refmovementtype">
											<tr>
												<td><c:out value="${refmovementtype.movementType}" /></td>
												<td><c:out value="${refmovementtype.movementDescription}" /></td>
												<td><c:out value="${refmovementtype.movementID}" /></td>
												<td>
												<button <c:out value="${buttonstatus}"/>
																	id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
																	onclick="FuncButtonUpdate()"
																	data-target="#ModalUpdateInsert" 
																	data-lmovementid='<c:out value="${refmovementtype.movementID}" />'
																	data-lmovement='<c:out value="${refmovementtype.movementType}" />-<c:out value="${refmovementtype.movementDescription}" />'
																	>
																	<i class="fa fa-edit"></i>
												</button>
												<button <c:out value="${buttonstatus}"/>
																	id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" 
																	data-toggle="modal" 
																	data-target="#ModalDelete"
																	data-lmovementid='<c:out value="${refmovementtype.movementID}" />'
																	>
															<i class="fa fa-trash"></i>
												</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row --> 
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
	
			<%@ include file="/mainform/pages/master_footer.jsp"%>
	
		</div>
		<!-- ./wrapper -->
	</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
	 	$(function () {
	 		$("#tb_ref_movementtype").DataTable();
	  		$('#M002').addClass('active');
	  		$('#M060').addClass('active');
	  		
	  	//shortcut for button 'new'
		    Mousetrap.bind('n', function() {
		    	FuncButtonNew(),
		    	$('#ModalUpdateInsert').modal('show')
		    	});
  		});

		$('#ModalDelete').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var lMovementID = button.data('lmovementid');
			
			$("#temp_txtMovementID").val(lMovementID);
		})

		$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
			FuncClear();
			
	 		var button = $(event.relatedTarget);
	 		
	 		var lMovementID = button.data('lmovementid');
	 		var lMovement = button.data('lmovement');
	 		
	 		var modal = $(this);
	 		
	 		modal.find(".modal-body #txtMovementID").val(lMovementID);
	 		modal.find(".modal-body #slMovement").val(lMovement);
	 		
	 		if(lMovementID == null || lMovementID == '')
	 			$("#txtMovementID").focus();
	 		else
	 			$("#slMovement").focus();
		});
		
		function FuncClear(){
			$('#mrkMovementID').hide();
			$('#mrkMovement').hide();
			
			$('#dvMovementID').removeClass('has-error');
			$('#dvMovement').removeClass('has-error');
			
			$("#dvErrorAlert").hide();
		}
	
		function FuncButtonNew() {
			$('#btnSave').show();
			$('#btnUpdate').hide();
			document.getElementById("lblTitleModal").innerHTML = "Add Reference Movement Type";
		
			$('#txtMovementID').prop('disabled', false);
		}
		
		function FuncButtonUpdate() {
			$('#btnSave').hide();
			$('#btnUpdate').show();
			document.getElementById("lblTitleModal").innerHTML = 'Edit Reference Movement Type';
		
			$('#txtMovementID').prop('disabled', true);
		}
		
		$(document).ready(function(){
		
			//action when txtMovementID changed
		    $(document).on('change', '#txtMovementID', function(e){
		    	$('#mrkMovementID').hide();
				$('#dvMovementID').removeClass('has-error');
		    });
		    
		    //action when slMovement changed
		    $(document).on('change', '#slMovement', function(e){
		    	$('#mrkMovement').hide();
				$('#dvMovement').removeClass('has-error');
		    });
			
		})
		
		function FuncValEmptyInput(lParambtn) {
			var txtMovementID = document.getElementById('txtMovementID').value;
			var txtMovement = document.getElementById('slMovement').value;
			var txtMovementType = document.getElementById('slMovement').value.split("-")[0];
			var txtMovementDescription = document.getElementById('slMovement').value.split("-")[1];
			
		    if(!txtMovementID.match(/\S/)) {
		    	$("#txtMovementID").focus();
		    	$('#dvMovementID').addClass('has-error');
		    	$('#mrkMovementID').show();
		        return false;
		    } 
		    
		    if(!txtMovement.match(/\S/)) {    	
		    	$('#slMovement').focus();
		    	$('#dvMovement').addClass('has-error');
		    	$('#mrkMovement').show();
		        return false;
		    }
		    
		    
		    jQuery.ajax({
		        url:'${pageContext.request.contextPath}/refmovementtype',	
		        type:'POST',
		        data:{"key":lParambtn,"txtMovementID":txtMovementID,"txtMovementType":txtMovementType,
		        	  "txtMovementDescription":txtMovementDescription},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	if(data.split("--")[0] == 'FailedInsertReferenceMovementType')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan reference movement type";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtMovementID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else if(data.split("--")[0] == 'FailedUpdateReferenceMovementType')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui reference movement type";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#slMovement").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else
		        	{
			        	var url = '${pageContext.request.contextPath}/refmovementtype';  
			        	$(location).attr('href', url);
		        	}
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    
		    return true;
		}
	</script>
</body>
</html>