<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_storagebin_detail" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>Product ID</th>
			<th>Total Stock</th>
			<th>UOM</th>
			<th>Batch No</th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listStorageBinDetail}" var ="storbindetail">
	  <tr>
	  <td><c:out value="${storbindetail.productID}" /></td>
		<td><c:out value="${storbindetail.qty}" /></td>
		<td><c:out value="${storbindetail.UOM}" /></td>
		<td><c:out value="${storbindetail.batchNo}" /></td>
	  </tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_storagebin_detail").DataTable();
  	});
</script>