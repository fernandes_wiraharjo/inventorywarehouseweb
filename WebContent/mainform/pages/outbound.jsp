<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Outbound</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="Outbound" name="Outbound" action = "${pageContext.request.contextPath}/Outbound" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Outbound <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
						
	      					<c:if test="${condition == 'empty_condition'}">
	    					  <script>$('#alrUpdate').hide();</script>
	      					</c:if>
	     					
	     					<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
      											
      										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
      											
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	        								<input type="hidden" id="temp_docNo" name="temp_docNo" value='<c:out value = "${tempdocNo}"/>' />
	          								<div id="dvDocNumber" class="form-group">
	            								<label for="recipient-name" class="control-label">Doc Number</label><label id="mrkDocNumber" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDocNumber" name="txtDocNumber" readonly="readonly">
	          								</div>
	          								<div id="dvDate" class="form-group">
	            								<label for="message-text" class="control-label">Date</label><label id="mrkDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDate" name="txtDate" data-date-format="dd M yyyy">
	          								</div>
	          								<div id="dvCustomerID" class="form-group ">
	            								<label for="message-text" class="control-label">Customer ID</label><label id="mrkCustomerID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblCustomerName" name="lblCustomerName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtCustomerID" name="txtCustomerID" data-toggle="modal" 
	            								data-target="#ModalGetCustomerID">
	          								</div>
	          								<div id="dvPlantID" class="form-group ">
	            								<label for="message-text" class="control-label">Plant ID</label><label id="mrkPlantID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" 
	            								data-target="#ModalGetPlantID">
	          								</div>
	          								<div id="dvWarehouseID" class="form-group ">
	            								<label for="message-text" class="control-label">WarehouseID</label><label id="mrkWarehouseID" for="recipient-name" class="control-label"> <small>*</small></label>	
	            								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" 
	            								data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
	          								</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
									<!--modal Delete -->
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Outbound</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtDocNumber" name="temp_txtDocNumber"  />
               	 									<p>Are You Sure Delete This Outbound ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								        
								        <!--modal show customer data -->
										<div class="modal fade" id="ModalGetCustomerID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelCustomerID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelCustomerID"></h4>	
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_customer" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                  <th>ID</th>
										                  <th>Name</th>
										                  <th>Address</th>
										                  <th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listCustomer}" var ="customer">
												        <tr>
												        <td><c:out value="${customer.customerID}"/></td>
												        <td><c:out value="${customer.customerName}"/></td>
												        <td><c:out value="${customer.customerAddress}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('<c:out value="${customer.customerID}"/>','','','<c:out value="${customer.customerName}"/>','')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->
								        
								        <!--modal show plant data -->
										<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelPlantID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_plant" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                  <th>ID</th>
										                  <th>Name</th>
										                  <th>Description</th>
										                  <th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listPlant}" var ="plant">
												        <tr>
												        <td><c:out value="${plant.plantID}"/></td>
												        <td><c:out value="${plant.plantNm}"/></td>
												        <td><c:out value="${plant.desc}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('','<c:out value="${plant.plantID}"/>','','','<c:out value="${plant.plantNm}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->	
										
										<!--modal show warehouse data -->
										<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelWarehouseID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content">
           										</div>
           										
<!-- 			         								<table id="tb_master_warehouse" class="table table-bordered table-hover"> -->
<!-- 										        <thead style="background-color: #d2d6de;"> -->
<!-- 										                <tr> -->
<!-- 										                <th>Plant ID</th> -->
<!-- 														<th>Warehouse ID</th> -->
<!-- 														<th>Warehouse Name</th> -->
<!-- 														<th>Warehouse Desc</th> -->
<!-- 										                  <th style="width: 20px"></th> -->
<!-- 										                </tr> -->
<!-- 										        </thead> -->
										        
<!-- 										        <tbody> -->
										        
<%-- 										        <c:forEach items="${listWarehouse}" var ="warehouse"> --%>
<!-- 												        <tr> -->
<%-- 												        <td><c:out value="${warehouse.plantID}" /></td> --%>
<%-- 												        <td><c:out value="${warehouse.warehouseID}"/></td> --%>
<%-- 												        <td><c:out value="${warehouse.warehouseName}"/></td> --%>
<%-- 												        <td><c:out value="${warehouse.warehouseDesc}"/></td> --%>
<!-- 												        <td><button type="button" class="btn btn-primary" -->
<!-- 												        			data-toggle="modal" -->
<%-- 												        			onclick="FuncPassString('','','<c:out value="${warehouse.warehouseID}"/>')" --%>
<!-- 												        			data-dismiss="modal" -->
<!-- 												        	><i class="fa fa-fw fa-check"></i></button> -->
<!-- 												        </td> -->
<!-- 										        		</tr> -->
										        		
<%-- 										        </c:forEach> --%>
										        
<!-- 										        </tbody> -->
<!-- 										        </table> -->
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->
	      				
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>DocNumber</th>
										<th>Date</th>
										<th>CustomerID</th>
										<th>PlantID</th>
										<th>WarehouseID</th>
										<th style="width:1px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listoutbound}" var="outbound">
										<tr>
											<td><c:out value="${outbound.docNumber}" /></td>
											<td><c:out value="${outbound.date}" /></td>
											<td><c:out value="${outbound.customerID}" /></td>
											<td><c:out value="${outbound.plantID}" /></td>
											<td><c:out value="${outbound.warehouseID}" /></td>
											<td>
<!-- 											<button id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal"  -->
<!-- 														onclick="FuncButtonUpdate()" -->
<!-- 														data-target="#ModalUpdateInsert"  -->
<%-- 														data-ldocnumber='<c:out value="${outbound.docNumber}" />' --%>
<%-- 														data-ldate='<c:out value="${outbound.date}" />' --%>
<%-- 														data-lcustomerid='<c:out value="${outbound.customerID}" />' --%>
<%-- 														data-lplantid='<c:out value="${outbound.plantID}" />' --%>
<%-- 														data-lwarehouseid='<c:out value="${outbound.warehouseID}" />'> --%>
<!-- 														<i class="fa fa-edit"></i></button>  -->
<%-- 												<button id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" onclick="FuncButtonDelete()" data-toggle="modal" data-target="#ModalDelete" data-ldocnumber='<c:out value="${outbound.docNumber}" />'> --%>
<!-- 												<i class="fa fa-trash"></i> -->
<!-- 												</button> -->
												<c:if test="${buttonstatus == 'disabled'}">
													<button <c:out value="${buttonstatus}"/> type="button" id="btnOutboundDetail" name="btnOutboundDetail"  class="btn btn-default">
													<i class="fa fa-list-ul"></i>
													</button>
												</c:if>
												<c:if test="${buttonstatus != 'disabled'}">
													<a href="${pageContext.request.contextPath}/OutboundDetail?docNo=<c:out value="${outbound.docNumber}" />&docDate=<c:out value="${outbound.date}" />&PlantID=<c:out value="${outbound.plantID}" />&WarehouseID=<c:out value="${outbound.warehouseID}" />">
														<button type="button" id="btnOutboundDetail" name="btnOutboundDetail"  class="btn btn-default">
														<i class="fa fa-list-ul"></i>
														</button>
													</a>
												</c:if>
											</td>
										</tr>

									</c:forEach>
															
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 		$(function () {
  			$("#tb_itemlib").DataTable();
  			$("#tb_master_customer").DataTable();
  	  		$("#tb_master_plant").DataTable();
  	  		$("#tb_master_warehouse").DataTable();
  	  	$('#M004').addClass('active');
  		$('#M027').addClass('active');
  		
  		$("#dvErrorAlert").hide();
  		});
 		
 		//shortcut for button 'new'
 	    Mousetrap.bind('n', function() {
 	    	FuncButtonNew(),
 	    	$('#ModalUpdateInsert').modal('show')
 	    	});
	</script>
	
	<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lDocNumber = button.data('ldocnumber');
		$("#temp_txtDocNumber").val(lDocNumber);
	})
	
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		
 		var modal = $(this);
 		
 		$('#txtDate').focus();
	})
	</script>
	
<script>
// var mrkVendorID = document.getElementById('mrkVendorID').value;
// var mrkVendorName = document.getElementById('mrkVendorName').value;
// var mrkVendorAddress = document.getElementById('mrkVendorAddress').value;
// var mrkPhone = document.getElementById('mrkPhone').value;
// var mrkPIC = document.getElementById('mrkPIC').value;
// var btnSave = document.getElementById('btnSave').value;
// var btnUpdate = document.getElementById('btnUpdate').value;

function FuncClear(){
	$('#mrkDocNumber').hide();
	$('#mrkDate').hide();
	$('#mrkCustomerID').hide();
	$('#mrkPlantID').hide();
	$('#mrkWarehouseID').hide();
	
	$('#dvDocNumber').removeClass('has-error');
	$('#dvDate').removeClass('has-error');
	$('#dvCustomerID').removeClass('has-error');
	$('#dvPlantID').removeClass('has-error');
	$('#dvWarehouseID').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	var tempDocNumber = document.getElementById('temp_docNo').value;
	$('#txtDocNumber').val(tempDocNumber);
	
	$('#txtDate').val('');
	$('#txtCustomerID').val('');
	$('#txtPlantID').val('');
	$('#txtWarehouseID').val('');
	
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById('lblTitleModal').innerHTML = "Add Outbound";
	
	document.getElementById('lblCustomerName').innerHTML = null;
	document.getElementById('lblPlantName').innerHTML = null;
	document.getElementById('lblWarehouseName').innerHTML = null;

	FuncClear();
// 	$('#txtDocNumber').prop('disabled', false);
	
	$('#txtDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
		$('#txtDate').datepicker('setDate', new Date());
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById('lblTitleModal').innerHTML = 'Edit Outbound';

	FuncClear();
	$('#txtDocNumber').prop('disabled', true);
	
	$('#ModalUpdateInsert').on('show.bs.modal', function (event) {
 		var button = $(event.relatedTarget);
 		var lDocNumber = button.data('ldocnumber');
 		var lDate = button.data('ldate');
 		var lCustomerID = button.data('lcustomerid');
 		var lPlantID = button.data('lplantid');
 		var lWarehouseID = button.data('lwarehouseid');
 		
 		var modal = $(this);
 		
 		if(lDocNumber == undefined)
 		{
 			
 		}
 		else
 		{
 			modal.find(".modal-body #txtDocNumber").val(lDocNumber);
 	 		modal.find(".modal-body #txtDate").val(lDate);
 	 		modal.find(".modal-body #txtCustomerID").val(lCustomerID);
 	 		modal.find(".modal-body #txtPlantID").val(lPlantID);
 	 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
 		}
	})
	
	$('#txtDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
}

function FuncPassString(lParamCustomerID,lParamPlantID,lParamWarehouseID,lParamCustomerName,lParamPlantName){
	if (lParamCustomerID)
		$("#txtCustomerID").val(lParamCustomerID);
	
	if (lParamPlantID)
		$("#txtPlantID").val(lParamPlantID);
	
	if (lParamWarehouseID)
		$("#txtWarehouseID").val(lParamWarehouseID);
	
	if (lParamCustomerName)
		document.getElementById('lblCustomerName').innerHTML = '(' + lParamCustomerName + ')';
	
	if (lParamPlantName)
		document.getElementById('lblPlantName').innerHTML = '(' + lParamPlantName + ')';
}

function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
	$("#txtWarehouseID").val(lParamWarehouseID);
	document.getElementById('lblWarehouseName').innerHTML = '(' + lParamWarehouseName + ')';
}

function FuncValPlant(){	
	var txtPlantID = document.getElementById('txtPlantID').value;
	
// 	var table = $("#tb_master_warehouse").DataTable();
// 	table.search( txtPlantID + " " ).draw();
	
	FuncClear();
	
	if(!txtPlantID.match(/\S/)) {    	
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
    	
    	alert("Fill Plant ID First ...!!!");
        return false;
    } 
	
    return true;
	
}

function FuncValEmptyInput(lParambtn) {
	var txtDocNumber = document.getElementById('txtDocNumber').value;
	var txtDate = document.getElementById('txtDate').value;
	var txtCustomerID = document.getElementById('txtCustomerID').value;
	var txtPlantID = document.getElementById('txtPlantID').value;
	var txtWarehouseID = document.getElementById('txtWarehouseID').value;
	

	var dvDocNumber = document.getElementsByClassName('dvDocNumber');
	var dvDate = document.getElementsByClassName('dvDate');
	var dvCustomerID = document.getElementsByClassName('dvCustomerID');
	var dvPlantID = document.getElementsByClassName('dvPlantID');
	var dvWarehouseID = document.getElementsByClassName('dvWarehouseID');
	
	if(lParambtn == 'save'){
		$('#txtDocNumber').prop('disabled', false);
	}
	else{
		$('#txtDocNumber').prop('disabled', true);
	}
	
    if(!txtDocNumber.match(/\S/)) {
    	$("#txtDocNumber").focus();
    	$('#dvDocNumber').addClass('has-error');
    	$('#mrkDocNumber').show();
        return false;
    } 
    
    if(!txtDate.match(/\S/)) {    	
    	$('#txtDate').focus();
    	$('#dvDate').addClass('has-error');
    	$('#mrkDate').show();
        return false;
    } 
    
    if(!txtCustomerID.match(/\S/)) {
    	$('#txtCustomerID').focus();
    	$('#dvCustomerID').addClass('has-error');
    	$('#mrkCustomerID').show();
        return false;
    } 
	
    if(!txtPlantID.match(/\S/)) {
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
        return false;
    } 
    
    if(!txtWarehouseID.match(/\S/)) {
    	$('#txtWarehouseID').focus();
    	$('#dvWarehouseID').addClass('has-error');
    	$('#mrkWarehouseID').show();
        return false;
    } 
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/Outbound',	
        type:'POST',
        data:{"key":lParambtn,"txtDocNumber":txtDocNumber,"txtDate":txtDate,"txtCustomerID":txtCustomerID,"txtPlantID":txtPlantID,"txtWarehouseID":txtWarehouseID},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertOutbound')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan dokumen keluar barang";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/Outbound';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;    
}
</script>

<!-- get warehouse from plant id -->
<script>
$(document).ready(function(){

    $(document).on('click', '#txtWarehouseID', function(e){
  
     e.preventDefault();
  
//      var plantid = $(this).data('id'); // get id of clicked row
		var plantid = document.getElementById('txtPlantID').value;
  
     $('#dynamic-content').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getwarehouse',
          type: 'POST',
          data: 'plantid='+plantid,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content').html(''); // blank before load.
          $('#dynamic-content').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtCustomerID:focus').length) {
    	$('#txtCustomerID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtPlantID:focus').length) {
    	$('#txtPlantID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtWarehouseID:focus').length) {
    	$('#txtWarehouseID').click();
    }
});
</script>

</body>
</html>