<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>BOM Detail</title> 
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="BomDetail" name="BomDetail" action = "${pageContext.request.contextPath}/bomproductdetail" method="post">
	
	<input  type="hidden" id="temp_componentLine" name="temp_componentLine" value="<c:out value="${tempComponentLine}"/>" />
	<input  type="hidden" id="temp_productId" name="temp_productId" value="<c:out value="${id}"/>" />
	<input  type="hidden" id="temp_validDate" name="temp_validDate" value="<c:out value="${validDate}"/>" />
	<input  type="hidden" id="temp_plantId" name="temp_plantId" value="<c:out value="${plantId}"/>" />
<%-- 	<input  type="hidden" id="temp_qty" name="temp_qty" value="<c:out value="${qty}"/>" /> --%>
	
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				BOM Detail <br><br>
				<small style="color: black; font-weight: bold;">Product Id : <c:out value="${id}"/></small> <br>
				<small style="color: black; font-weight: bold;">Valid Date : <c:out value="${validDateShow}"/></small> <br>
				<small style="color: black; font-weight: bold;">Plant Id : <c:out value="${plantId}"/></small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
						
							<c:if test="${condition == 'SuccessInsertBOM'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan produk bom. Silahkan mengisi komponen.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'SuccessUpdateBOM'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui produk bom. Silahkan perbaharui komponen jika diperlukan.
              				</div>
	      					</c:if>
	      					
							<c:if test="${condition == 'SuccessInsertComponent'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan komponen.
              				</div>
	      					</c:if>
	      		
	     					
	     					<c:if test="${condition == 'SuccessUpdateComponent'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui komponen.
              				</div>
	      					</c:if>
	      					
	      					<!--modal update & Insert -->
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
<!-- 	          								<div id="dvProductID"> -->
<!-- 	            								<label for="recipient-name" class="control-label">Product ID</label><label id="mrkProductID" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<%-- 	            								<small><label class="control-label">(<c:out value="${productname}"/>)</label></small> --%>
<%-- 	            								<input type="text" class="form-control" id="txtProductID" name="txtProductID" readonly="readonly" value="<c:out value="${id}"/>"> --%>
<!-- 	          								</div> -->
	          								<div id="dvComponentLine">
	            								<label for="message-text" class="control-label">Component Line</label>
	            								<input type="text" class="form-control" id="txtComponentLine" name="txtComponentLine" readonly="readonly">
	          								</div>
	          								<div id="dvComponentID">
	            								<label for="message-text" class="control-label">Component ID</label><label id="mrkComponentID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblComponentName" name="lblComponentName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtComponentID" name="txtComponentID" data-toggle="modal" data-target="#ModalGetComponentID">
	          								</div>
	          								<div id="dvQty">
	            								<label for="message-text" class="control-label">Quantity</label><label id="mrkQty" for="recipient-name" class="control-label"><small>*</small></label>
	            								<input type="number" class="form-control" id="txtQty" name="txtQty">
	          								</div>
	          								<div id="dvUOM" class="form-group">
	            								<label for="message-text" class="control-label">UOM</label><label id="mrkUOM" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<select id="slMasterUOM" name="slMasterUOM" class="form-control" onfocus="FuncValShowUOM()">
							                    </select>
	          								</div>
      								</div>
      								
      								<div class="modal-footer">
        									<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									<!-- end of modal -->
								      
								      <!--modal show product component data -->
										<div class="modal fade" id="ModalGetComponentID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelComponentID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelComponentID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_component" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Component ID</th>
														<th>Component Name</th>
														<th>Description</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listComponent}" var ="component">
												        <tr>
												        <td><c:out value="${component.id}" /></td>
														<td><c:out value="${component.title_en}" /></td>
														<td><c:out value="${component.short_description_en}" /></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassStringComponent('<c:out value="${component.id}"/>','<c:out value="${component.title_en}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show product component data -->
										
							
							<button id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_bom_detail" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Component Line</th>
										<th>Component ID</th>
										<th>Component Name</th>
										<th>Quantity</th>
										<th>UOM</th>
										<th>Quantity Base</th>
										<th>Base UOM</th>
										<th style="width:20px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listBOMDetail}" var="bomdetail">
										<tr>
											<td><c:out value="${bomdetail.componentLine}" /></td>
											<td><c:out value="${bomdetail.componentID}" /></td>
											<td><c:out value="${bomdetail.componentName}" /></td>
											<td><c:out value="${bomdetail.qty}" /></td>
											<td><c:out value="${bomdetail.UOM}" /></td>
											<td><c:out value="${bomdetail.qtyBaseUOM}" /></td>
											<td><c:out value="${bomdetail.baseUOM}" /></td>
											<td>
												<button id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
												onclick="FuncButtonUpdate('<c:out value="${bomdetail.componentID}" />')"
												data-target="#ModalUpdateInsert" 
												data-lcomponentline='<c:out value="${bomdetail.componentLine}" />'
												data-lcomponentid='<c:out value="${bomdetail.componentID}" />'
												data-lcomponentname='<c:out value="${bomdetail.componentName}" />'
												data-lqty='<c:out value="${bomdetail.qty}" />'
												data-luom='<c:out value="${bomdetail.UOM}" />'
												>
												<i class="fa fa-edit"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_master_component").DataTable();
  		$("#tb_bom_detail").DataTable();
  		$('#M006').addClass('active');
  		$('#M033').addClass('active');
  		
  		$("#dvErrorAlert").hide();
  	});
 	
 	//shortcut for button 'new'
    Mousetrap.bind('n', function() {
    	FuncButtonNew(),
    	$('#ModalUpdateInsert').modal('show')
    	});
	</script>

<script>
$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
	$("#dvErrorAlert").hide();
	
 		var button = $(event.relatedTarget);
 		
 		var lComponentLine = button.data('lcomponentline');
 		var lComponentID = button.data('lcomponentid');
 		var lComponentName = button.data('lcomponentname');
 		var lQty = button.data('lqty');
 		var lUOM = button.data('luom');
 		
 		var modal = $(this);
 		
 		if(lComponentLine == undefined)
 			{
 			}
 		else
 			{
			modal.find(".modal-body #txtComponentLine").val(lComponentLine);
 	 		modal.find(".modal-body #txtComponentID").val(lComponentID);
 	 		
 	 		if(lComponentName == undefined)
 	 		  document.getElementById("lblComponentName").innerHTML = null;
 	 	    else
 	 	  	  document.getElementById("lblComponentName").innerHTML = '(' + lComponentName + ')';
 	 		
 	 		modal.find(".modal-body #txtQty").val(lQty);
 	 		modal.find(".modal-body #slMasterUOM").val(lUOM);
 			}
 		
 		if(lComponentID == null || lComponentID == '')
 			{
	 			$('#txtComponentID').focus();
	 	 		$('#txtComponentID').click();
 			}
 		else
 			{
 				$('#txtComponentID').focus();
 			}
	})
</script>

<script>
function FuncPassStringComponent(lParamComponentID,lParamComponentName){
	$("#txtComponentID").val(lParamComponentID);
	document.getElementById("lblComponentName").innerHTML = '(' + lParamComponentName + ')';
	
	FuncShowUOM();
}
</script>

<script>
function FuncClear(){
	$('#mrkComponentID').hide();
	$('#mrkUOM').hide();
	$('#mrkQty').hide();
	
	
	$('#dvComponentID').removeClass('has-error');
	$('#dvUOM').removeClass('has-error');
	$('#dvQty').removeClass('has-error');
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	var tempComponentLine = document.getElementById('temp_componentLine').value;
	
	$('#txtComponentLine').val(tempComponentLine);
	$('#txtComponentID').val('');
	$('#txtQty').val('');
	$('#slMasterUOM').val('');
	
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add BOM Detail";
	document.getElementById("lblComponentName").innerHTML = null;

	FuncClear();
}

function FuncValShowUOM(){	
	var txtComponentID = document.getElementById('txtComponentID').value;

	FuncClear();

	if(!txtComponentID.match(/\S/)) {    	
	    	$('#txtComponentID').focus();
	    	$('#dvComponentID').addClass('has-error');
	    	$('#mrkComponentID').show();
	    	
	    	alert("Fill Component ID First ...!!!");
	        return false;
	    } 
	    return true;
	}

function FuncButtonUpdate(lComponentID) {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById('lblTitleModal').innerHTML = 'Edit BOM Detail';
	
	FuncClear();
	
	$('#txtComponentID').val(lComponentID);
	FuncShowUOM();
}

function FuncValEmptyInput(lParambtn) {
	var txtProductID = document.getElementById('temp_productId').value;
	var txtDate = document.getElementById('temp_validDate').value;
	var txtPlantID = document.getElementById('temp_plantId').value;
	var txtComponentLine = document.getElementById('txtComponentLine').value;
	var txtComponentID = document.getElementById('txtComponentID').value;
// 	var txtQty = document.getElementById('temp_qty').value;
	var txtQty = document.getElementById('txtQty').value;
	var txtUOM = document.getElementById('slMasterUOM').value;
	
	var dvComponentID = document.getElementsByClassName('dvComponentID');
	var dvUOM = document.getElementsByClassName('dvUOM');
	var dvQty = document.getElementsByClassName('dvQty');
    
    if(!txtComponentID.match(/\S/)) {    	
    	$('#txtComponentID').focus();
    	$('#dvComponentID').addClass('has-error');
    	$('#mrkComponentID').show();
        return false;
    } 
    
    if(!txtUOM.match(/\S/)) {
    	$('#slMasterUOM').focus();
    	$('#dvUOM').addClass('has-error');
    	$('#mrkUOM').show();
        return false;
    }
    
    if(!txtQty.match(/\S/)) {
    	$('#txtQty').focus();
    	$('#dvQty').addClass('has-error');
    	$('#mrkQty').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/bomproductdetail',	
        type:'POST',
        data:{"key":lParambtn,"txtProductID":txtProductID,"txtDate":txtDate,"txtPlantID":txtPlantID,"txtComponentLine":txtComponentLine,"txtComponentID":txtComponentID,"txtQty":txtQty,"txtUOM":txtUOM},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertComponent')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan komponen";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtComponentID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateComponent')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui komponen";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtComponentID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedConversionComponent')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				//$("#txtQty").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/bomproductdetail';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<!-- get uom from product id -->
 	<script>
 	function FuncShowUOM(){	
 		$('#slMasterUOM').find('option').remove();
 		
		var paramcomponentid = document.getElementById('txtComponentID').value;
 
		         $.ajax({
		              url: '${pageContext.request.contextPath}/getuom',
		              type: 'POST',
		              data: {productid : paramcomponentid},
		              dataType: 'json'
		         })
		         .done(function(data){
		              console.log(data);
		
		             //for json data type
		             for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slMasterUOM').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
		        
		    }
		             
		         })
		         .fail(function(){
		        	 console.log('Service call failed!');
		         });
 	}
 </script>

</body>
</html>