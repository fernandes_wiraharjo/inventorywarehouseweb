<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_storagesectionindicator" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>Plant ID</th>
			<th>Warehouse ID</th>
			<th>Storage Section Indicator</th>
			<th>Description</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listStorageSectionIndicator}" var ="storsecind">
	  <tr>
	  <td><c:out value="${storsecind.plantID}" /></td>
		<td><c:out value="${storsecind.warehouseID}" /></td>
		<td><c:out value="${storsecind.storageSectionIndicatorID}" /></td>
		<td><c:out value="${storsecind.storageSectionIndicatorName}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassStorageSectionIndicator('<c:out value="${storsecind.storageSectionIndicatorID}"/>','<c:out value="${storsecind.storageSectionIndicatorName}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_storagesectionindicator").DataTable();
  	});
</script>