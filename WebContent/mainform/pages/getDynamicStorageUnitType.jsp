<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_storageunittype" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>Plant ID</th>
			<th>Warehouse ID</th>
			<th>Storage Unit Type</th>
			<th>Description</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listStorageUnitType}" var ="storunittype">
	  <tr>
	  <td><c:out value="${storunittype.plantID}" /></td>
		<td><c:out value="${storunittype.warehouseID}" /></td>
		<td><c:out value="${storunittype.storageUnitTypeID}" /></td>
		<td><c:out value="${storunittype.storageUnitTypeName}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassStorageUnitType('<c:out value="${storunittype.storageUnitTypeID}"/>','<c:out value="${storunittype.storageUnitTypeName}"/>','<c:out value="${type}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_storageunittype").DataTable();
  	});
</script>