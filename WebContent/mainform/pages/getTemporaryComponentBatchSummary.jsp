 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="divsuccess2" name="divsuccess2" class="alert alert-success alert-dismissible">
 	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<h4><i class="icon fa fa-check"></i> Success</h4>
  	<label>Sukses mengatur batch. </label>
</div>  					
<div id="divfailed2" name="divfailed2" class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<h4><i class="icon fa fa-ban"></i> Failed</h4>
	<label>Gagal mengatur batch. </label> <label id="errorlabel"></label>
</div>

<table id="tb_temp" class="table table-bordered table-hover">
    <thead style="background-color: #d2d6de;">
    <tr>
    <th style="display:none"></th>
    <th style="display:none"></th>
    <th style="display:none"></th>
    <th style="display:none"></th>
    <th>Component ID</th>
	<th>Batch No</th>
	<th>GR Date</th>
	<th>Expired</th>
	<th>PlantID</th>
	<th>WarehouseID</th>
	<th>Qty</th>
	<th>UOM</th>
	<th></th>
   </tr>
   </thead>
     
     <tbody>
     
     <c:forEach items="${listComponent}" var ="component">
     <tr>
     <td style="display:none"><c:out value="${targetqtybase}" /></td>
     <td style="display:none"><c:out value="${ordertypeid}" /></td>
     <td style="display:none"><c:out value="${orderno}" /></td>
     <td style="display:none"><c:out value="${componentline}" /></td>
	<td><c:out value="${component.componentID}" /></td>
	<td><c:out value="${component.batchNo}" /></td>
	<td><c:out value="${component.GRDate}" /></td>
	<td><c:out value="${component.expiredDate}" /></td>
	<td><c:out value="${component.plantID}" /></td>
	<td><c:out value="${component.warehouseID}" /></td>
	<td><c:out value="${component.qty}" /></td>
	<td><c:out value="${component.UOM}" /></td>
	<td>
	<div class="checkbox"><label><input type="checkbox" name="cbComponent" <c:out value="${component.status}" /> style="width:100px" value="<c:out value="${component.batchNo}" />"></label></div>
	</td>
     </tr>		
     </c:forEach>
     
     </tbody>
     </table>

<script>
$(function () {
	$("#tb_temp").DataTable(
 				{
 					 "aaSorting": []
 				});
  		$("#divsuccess2").hide();
  		$("#divfailed2").hide();
});
</script>