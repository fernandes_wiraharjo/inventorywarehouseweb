<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Department</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="Department" name="Vendor" action = "${pageContext.request.contextPath}/Department" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Department <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsert'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Insert new Department succed.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedInsert'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Insert new Department fail. Please contact admin for help.
              				</div>
	     					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdate'}">
	    					  <div id="alrUpdate" class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Update Department succed.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedUpdate'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Update Department fail. Please contact admin for help.
              				</div>
	     					</c:if> 
	     					
	     					<c:if test="${condition == 'SuccessDelete'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Delete Department succed.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'empty_condition'}">
	    					  <script>$('#alrUpdate').hide();</script>
	      					</c:if>

	      					<c:if test="${condition == 'FailedDelete'}">	
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Delete Department fail. Please contact admin for help.
              				</div>
	     					</c:if>   
	      				
<!-- 							<button id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br> -->
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>ID</th>
										<th>Code</th>
										<th>Title_EN</th>
										<th>Title_ID</th>
										<th>Slug</th>
										<th>Icon</th>
										<th>Banner</th>
										<th>Order</th>
										<th>Status</th>
<!-- 										<th>Created_at</th> -->
<!-- 										<th>Updated_at</th> -->
<!-- 										<th style="width:30px;"></th> -->
									</tr>
								</thead>	

								<tbody>

									<c:forEach items="${listdepartment}" var="department">
										<tr>
											<td><c:out value="${department.id}" /></td>
											<td><c:out value="${department.code}" /></td>
											<td><c:out value="${department.title_en}" /></td>
											<td><c:out value="${department.title_id}" /></td>
											<td><c:out value="${department.slug}" /></td>
											<td><c:out value="${department.icon}" /></td>
											<td><c:out value="${department.banner}" /></td>
											<td><c:out value="${department.order}" /></td>
											<td><c:out value="${department.status}" /></td>
<%-- 											<td><c:out value="${department.created_at}" /></td> --%>
<%-- 											<td><c:out value="${department.updated_at}" /></td> --%>
											
<!-- 											<td><button id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal"  -->
<!-- 														onclick="FuncButtonUpdate()" -->
<!-- 														data-target="#ModalUpdateInsert"  -->
<%-- 														data-lid='<c:out value="${department.id}" />' --%>
<%-- 														data-ltitle_en='<c:out value="${department.title_en}" />' --%>
<%-- 														data-ltitle_id='<c:out value="${department.title_id}" />' --%>
<%-- 														data-licon='<c:out value="${department.icon}" />' --%>
<%-- 														data-lbanner='<c:out value="${department.banner}" />' --%>
<%-- 														data-lorder='<c:out value="${department.order}" />' --%>
<%-- 														data-lstatus='<c:out value="${department.status}" />'> --%>
<!-- 														<i class="fa fa-edit"></i></button>  -->
<%-- 												<button id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" onclick="FuncButtonDelete()" data-toggle="modal" data-target="#ModalDelete" data-lvendorid='<c:out value="${department.id}" />'> --%>
<!-- 												<i class="fa fa-trash"></i> -->
<!-- 												</button> -->
<!-- 											</td> -->
										</tr>

									</c:forEach>
<!-- 									modal update & Insert -->
									
<!-- 									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"> -->
<!--  										<div class="modal-dialog" role="document"> -->
<!--     										<div class="modal-content"> -->
<!--       											<div class="modal-header"> -->
<!--         											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
<!--         											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	 -->
        												
        											
<!--       											</div> -->
<!-- 	      								<div class="modal-body"> -->
	        								
<!-- 	          								<div id="dvid" class="form-group"> -->
<!-- 	            								<label id="lblId" name="lblId" for="recipient-name" class="control-label">Id</label><label id="mrkId" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtId" name="txtId"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvTitle_En" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">Title En</label><label id="mrkTitle_En" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtTitle_En" name="txtTitle_En"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvTitle_Id" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">Title Id</label><label id="mrkTitle_Id" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtTitle_Id" name="txtTitle_Id"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvIcon" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">Icon</label><label id="mrkIcon" for="recipient-name" class="control-label"> <small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtIcon" name="txtIcon"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvBanner" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">Banner</label><label id="mrkBanner" for="recipient-name" class="control-label"> <small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtBanner" name="txtBanner"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvOrder" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">Order</label><label id="mrkOrder" for="recipient-name" class="control-label"> <small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtOrder" name="txtOrder"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvStatus" class="form-group "> -->
<!-- 	            								<label for="message-text" class="control-label">Status</label><label id="mrkStatus" for="recipient-name" class="control-label"> <small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtStatus" name="txtStatus"> -->
<!-- 	          								</div> -->
<!--       								</div> -->
      								
<!--       								<div class="modal-footer"> -->
<!--         									<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button> -->
<!--         									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button> -->
<!--         									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        								
<!--       								</div> -->
<!--     										</div> -->
<!--   										</div> -->
<!-- 									</div> -->
									
<!-- 									modal Delete -->
<!--        									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> -->
<!--           									<div class="modal-dialog" role="document"> -->
<!--            										<div class="modal-content"> -->
<!--               										<div class="modal-header">            											 -->
<!--                 										<button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
<!--                   										<span aria-hidden="true">&times;</span></button> -->
<!--                 										<h4 class="modal-title">Alert Delete Vendor</h4> -->
<!--               										</div> -->
<!--               									<div class="modal-body"> -->
<!--               									<input type="hidden" id="temp_txtId" name="temp_txtId"  /> -->
<!--                	 									<p>Are You Sure Delete This Department ?</p> -->
<!--               									</div> -->
<!-- 								              <div class="modal-footer"> -->
<!-- 								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button> -->
<!-- 								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button> -->
<!-- 								              </div> -->
<!-- 								            </div> -->
<!-- 								            /.modal-content -->
<!-- 								          </div> -->
<!-- 								          /.modal-dialog -->
<!-- 								        </div> -->
<!-- 								    /.modal							 -->
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 		$(function () {
  			$("#tb_itemlib").DataTable(); 
  			$('#M003').addClass('active');
  	  		$('#M019').addClass('active');
  		});
	</script>
	<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lId = button.data('lId');
		$("#temp_txtId").val(lId);
	})
	</script>


</body>
</html>