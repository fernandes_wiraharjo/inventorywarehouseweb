<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Product UOM/Conversion</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="productuom" name="productuom" action = "${pageContext.request.contextPath}/productuom" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Product UOM <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertProductUOM'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan konversi produk.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateProductUOM'}">
	    					  <div id="alrUpdate" class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui konversi produk.
              				</div>
	      					</c:if>
	      					
	      					<!--modal update & Insert -->
	      					<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvProductID" class="form-group">
	            								<label for="recipient-name" class="control-label">Product ID</label><label id="mrkProductID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblProductName" name="lblProductName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtProductID" name="txtProductID" data-toggle="modal" data-target="#ModalGetProductID">
	          								</div>
	          								<div id="dvUOM" class="form-group">
	            								<label for="recipient-name" class="control-label">UOM</label><label id="mrkUOM" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<select id="txtUOM" name="txtUOM" class="form-control">
	            								<c:forEach items="${listuom}" var ="uom">
												        <option><c:out value="${uom.code}" /></option>
										        </c:forEach>
							                    </select>
	          								</div>
	          								<div id="dvBaseUOM" class="form-group">
	            								<label for="recipient-name" class="control-label">Base UOM</label><label id="mrkBaseUOM" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<select id="txtBaseUOM" name="txtBaseUOM" class="form-control">
	            								<c:forEach items="${listbaseuom}" var ="baseuom">
												        <option><c:out value="${baseuom.code}" /></option>
										        </c:forEach>
							                    </select>
	          								</div>
	          								<div id="dvQty" class="form-group">
									            <label for="recipient-name" class="control-label">Quantity</label><label id="mrkQty" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtQty" name="txtQty">
								          	</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									<!-- modal -->
									
									 <!--modal show product data -->
										<div class="modal fade" id="ModalGetProductID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelProductID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelProductID">Product Data</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_product" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Product ID</th>
														<th>Product Name</th>
														<th>Description</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listProduct}" var ="product">
												        <tr>
												        <td><c:out value="${product.id}" /></td>
														<td><c:out value="${product.title_en}" /></td>
														<td><c:out value="${product.short_description_en}" /></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassStringProduct('<c:out value="${product.id}"/>','<c:out value="${product.title_en}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show product data -->
									
	      					<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Product ID</th>
										<th>Product Name</th>
										<th>UOM</th>
										<th>Base UOM</th>
										<th>Quantity</th>
										<th style="width:10px;"></th>
									</tr>
								</thead>	

								<tbody>

									<c:forEach items="${listproductuom}" var="productuom">
										<tr>
											<td><c:out value="${productuom.productID}" /></td>
											<td><c:out value="${productuom.productName}" /></td>
											<td><c:out value="${productuom.UOM}" /></td>
											<td><c:out value="${productuom.baseUOM}" /></td>
											<td><c:out value="${productuom.qty}" /></td>
											<td>
											<button <c:out value="${buttonstatus}"/>
													id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert" 
														data-lproductid='<c:out value="${productuom.productID}" />'
														data-lproductname='<c:out value="${productuom.productName}" />'
														data-luom='<c:out value="${productuom.UOM}" />'
														data-lbaseuom='<c:out value="${productuom.baseUOM}" />'
														data-lqty='<c:out value="${productuom.qty}" />'>
														<i class="fa fa-edit"></i></button>
											</td> 
										</tr>

									</c:forEach>

								</tbody>
								
							</table>
							
						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 		$(function () {
  			$("#tb_itemlib").DataTable( {
  		        "scrollX": true
  		    });
  			$("#tb_master_product").DataTable();
  			$('#M003').addClass('active');
  			$('#M025').addClass('active');
  	  		
  	  	//shortcut for button 'new'
  	  	    Mousetrap.bind('n', function() {
  	  	    	FuncButtonNew(),
  	  	    	$('#ModalUpdateInsert').modal('show')
  	  	    	});
  	  		
  	  		$("#dvErrorAlert").hide();
  		});
	</script>
	
	<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		var lProductID = button.data('lproductid');
 		var lProductName = button.data('lproductname');
 		var lUOM = button.data('luom');
 		var lBaseUOM = button.data('lbaseuom');
 		var lQty = button.data('lqty');
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtProductID").val(lProductID);
 		document.getElementById("lblProductName").innerHTML = '(' + lProductName + ')';
 		modal.find(".modal-body #txtUOM").val(lUOM);
 		modal.find(".modal-body #txtBaseUOM").val(lBaseUOM);
 		modal.find(".modal-body #txtQty").val(lQty);
 		
 		if(lProductID == null || lProductID == '')
 			{
 				$("#txtProductID").focus();
 				$("#txtProductID").click();
 				document.getElementById("lblProductName").innerHTML = null;
 			}
 		else
 			$("#txtBaseUOM").focus();
	})
</script>

	<script>
	function FuncClear(){
		$('#mrkProductID').hide();
		$('#txtProductID').prop('disabled', false);
		$('#txtUOM').prop('disabled', false);
		$('#mrkUOM').hide();
		$('#mrkBaseUOM').hide();
		$('#mrkQty').hide();
		
		$('#dvProductID').removeClass('has-error');
		$('#dvUOM').removeClass('has-error');
		$('#dvBaseUOM').removeClass('has-error');
		$('#dvQty').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
	}

	function FuncButtonNew() {
		$('#btnSave').show();
		$('#btnUpdate').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add Product UOM";

		FuncClear();
		$('#txtProductID').prop('disabled', false);
		$('#txtUOM').prop('disabled', false);
	}
	
	function FuncButtonUpdate() {
		$('#btnSave').hide();
		$('#btnUpdate').show();
		document.getElementById('lblTitleModal').innerHTML = 'Edit Product UOM';
		
		FuncClear();
		$('#txtProductID').prop('disabled', true);
		$('#txtUOM').prop('disabled', true);
	}
	
	function FuncPassStringProduct(lParamProductID,lParamProductName){
		$("#txtProductID").val(lParamProductID);
		document.getElementById("lblProductName").innerHTML = '(' + lParamProductName + ')';
	}
	</script>
	
	<script>
	function FuncValEmptyInput(lParambtn) {
		var txtProductID = document.getElementById('txtProductID').value;
		var txtUOM = document.getElementById('txtUOM').value;
		var txtBaseUOM = document.getElementById('txtBaseUOM').value;
		var txtQty = document.getElementById('txtQty').value;
		
		var dvProductID = document.getElementsByClassName('dvProductID');
		var dvUOM = document.getElementsByClassName('dvUOM');
		var dvBaseUOM = document.getElementsByClassName('dvBaseUOM');
		var dvQty = document.getElementsByClassName('dvQty');

	    if(!txtProductID.match(/\S/)) {
	    	$("#txtProductID").focus();
	    	$('#dvProductID').addClass('has-error');
	    	$('#mrkProductID').show();
	        return false;
	    } 
	    
	    if(!txtUOM.match(/\S/)) {    	
	    	$('#txtUOM').focus();
	    	$('#dvUOM').addClass('has-error');
	    	$('#mrkUOM').show();
	        return false;
	    }
	    
	    if(!txtBaseUOM.match(/\S/)) {    	
	    	$('#txtBaseUOM').focus();
	    	$('#dvBaseUOM').addClass('has-error');
	    	$('#mrkBaseUOM').show();
	        return false;
	    }
	    
	    if(!txtQty.match(/\S/)) {    	
	    	$('#txtQty').focus();
	    	$('#dvQty').addClass('has-error');
	    	$('#mrkQty').show();
	        return false;
	    }
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/productuom',	
	        type:'POST',
	        data:{"key":lParambtn,"txtProductID":txtProductID,"txtUOM":txtUOM,"txtBaseUOM":txtBaseUOM,"txtQty":txtQty},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertProductUOM')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan konversi produk";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtProductID").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateProductUOM')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui konversi produk";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtBaseUOM").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/productuom';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	</script>
</body>
</html>