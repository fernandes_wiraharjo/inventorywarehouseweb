<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Customer Branch</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="CustomerBranch" name="CustomerBranch" action = "${pageContext.request.contextPath}/CustomerBranch" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Customer Branch <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertCustomerBranch'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambah cabang pelanggan.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateCustomerBranch'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui cabang pelanggan.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessDeleteCustomerBranch'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menghapus cabang pelanggan.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedDeleteCustomerBranch'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal menghapus cabang pelanggan. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>   
	      					
	      					<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>		
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvCustBranchID" class="form-group col-xs-6">
	            								<label for="recipient-name" class="control-label">Customer Branch ID</label><label id="mrkCustBranchID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtCustBranchID" name="txtCustBranchID">
	          								</div>
	          								<div id="dvCustBranchName" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Customer Branch Name</label><label id="mrkCustBranchName" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtCustBranchName" name="txtCustBranchName">
	          								</div>
	          								<div id="dvCustBranchAddress" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Customer Branch Address</label><label id="mrkCustBranchAddress" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtCustBranchAddress" name="txtCustBranchAddress">
	          								</div>
	          								<div id="dvPhone" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Phone</label><label id="mrkPhone" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtPhone" name="txtPhone">
	          								</div>
	          								<div id="dvEmail" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Email</label><label id="mrkEmail" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtEmail" name="txtEmail">
	          								</div>
	          								<div id="dvPIC" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">PIC</label><label id="mrkPIC" for="recipient-name" class="control-label"> <small>*</small></label>	
	            								<input type="text" class="form-control" id="txtPIC" name="txtPIC">
	          								</div>
	          								<div id="dvCustomerID" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Customer ID</label><label id="mrkCustomerID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblCustomerName" name="lblCustomerName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtCustomerID" name="txtCustomerID" data-toggle="modal" data-target="#ModalGetCustID">
	          								</div>
	        								
	        								<div class="row"></div>	 
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
									<!--modal Delete -->
									<div class="example-modal">
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Customer Branch</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtCustBranchID" name="temp_txtCustBranchID">
               	 									<p>Are you sure to delete this customer branch?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								      </div>
								      
								      <!--modal show customer data-->
										<div class="modal fade bs-example-modal-lg" id="ModalGetCustID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelCustID">
												<div class="modal-dialog modal-lg" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelCustID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_customer" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Customer ID</th>
														<th>Customer Name</th>
														<th>Customer Address</th>
														<th>Customer Type ID</th>
														<th>Phone</th>
														<th>Email</th>
														<th>PIC</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listcustomer}" var ="customer">
												        <tr>
												        <td><c:out value="${customer.customerID}" /></td>
														<td><c:out value="${customer.customerName}" /></td>
														<td><c:out value="${customer.customerAddress}" /></td>
														<td><c:out value="${customer.customerTypeID}" /></td>
														<td><c:out value="${customer.phone}" /></td>
														<td><c:out value="${customer.email}" /></td>
														<td><c:out value="${customer.pic}" /></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('<c:out value="${customer.customerID}"/>','<c:out value="${customer.customerName}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Customer Branch ID</th>
										<th>Customer Branch Name</th>
										<th>Customer Branch Address</th>
										<th>Phone</th>
										<th>Email</th>
										<th>PIC</th>
										<th>Customer ID</th>
										<th style="width:60px;"></th>
									</tr>
								</thead>
	
								<tbody>

									<c:forEach items="${listcustbranch}" var="custbranch">
										<tr>
											<td><c:out value="${custbranch.custBranchID}" /></td>
											<td><c:out value="${custbranch.custBranchName}" /></td>
											<td><c:out value="${custbranch.custBranchAddress}" /></td>
											<td><c:out value="${custbranch.phone}" /></td>
											<td><c:out value="${custbranch.email}" /></td>
											<td><c:out value="${custbranch.pic}" /></td>
											<td><c:out value="${custbranch.customerID}" /></td>
											<td><button <c:out value="${buttonstatus}"/>
														id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert" 
														data-lcustbranchid='<c:out value="${custbranch.custBranchID}" />'
														data-lcustbranchname='<c:out value="${custbranch.custBranchName}" />'
														data-lcustbranchaddress='<c:out value="${custbranch.custBranchAddress}" />'
														data-lphone='<c:out value="${custbranch.phone}" />'
														data-lemail='<c:out value="${custbranch.email}" />'
														data-lpic='<c:out value="${custbranch.pic}" />'
														data-lcustomerid='<c:out value="${custbranch.customerID}" />'
														data-lcustomername='<c:out value="${custbranch.customerName}" />'>
														<i class="fa fa-edit"></i></button> 
												<button <c:out value="${buttonstatus}"/>
														id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" data-toggle="modal" 
														data-target="#ModalDelete" data-lcustbranchid='<c:out value="${custbranch.custBranchID}" />'>
												<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_itemlib").DataTable();
  		$("#tb_master_customer").DataTable();
  		$('#M002').addClass('active');
  		$('#M014').addClass('active');
  		
  	//shortcut for button 'new'
  	    Mousetrap.bind('n', function() {
  	    	FuncButtonNew(),
  	    	$('#ModalUpdateInsert').modal('show')
  	    	});
  	
  	  $("#dvErrorAlert").hide();
  	});
	</script>
	
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lCustomerBranchID = button.data('lcustbranchid');
		$("#temp_txtCustBranchID").val(lCustomerBranchID);
	})
</script>

<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		
 		var lCustBranchID = button.data('lcustbranchid');
 		var lCustBranchName = button.data('lcustbranchname');
 		var lCustBranchAddress = button.data('lcustbranchaddress');
 		var lEmail = button.data('lemail');
 		var lPhone = button.data('lphone');
 		var lPIC = button.data('lpic');
 		var lCustomerID = button.data('lcustomerid');
 		var lCustomerName = button.data('lcustomername');
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtCustBranchID").val(lCustBranchID);
//  		modal.find(".modal-body #txtCustBranchID").prop('disabled', true);
 		modal.find(".modal-body #txtCustBranchName").val(lCustBranchName);
 		modal.find(".modal-body #txtCustBranchAddress").val(lCustBranchAddress);
 		modal.find(".modal-body #txtEmail").val(lEmail);
 		modal.find(".modal-body #txtPIC").val(lPIC);
 		modal.find(".modal-body #txtPhone").val(lPhone);
 		modal.find(".modal-body #txtCustomerID").val(lCustomerID);
 		
 		if(lCustomerName != null)
 			document.getElementById('lblCustomerName').innerHTML = "(" + lCustomerName + ")";
 		
 		if(lCustBranchID == null || lCustBranchID == '')
 			$("#txtCustBranchID").focus();
 		else
 			$("#txtCustBranchName").focus();
	})
</script>

<script>
function FuncPassString(lParamCustID,lParamCustName){
	$("#txtCustomerID").val(lParamCustID);
	document.getElementById('lblCustomerName').innerHTML = "(" + lParamCustName + ")";
}
</script>

<script>
// var mrkCustBranchID = document.getElementById('mrkCustBranchID').value;
// var mrkCustBranchName = document.getElementById('mrkCustBranchName').value;
// var mrkCustBranchAddress = document.getElementById('mrkCustBranchAddress').value;
// var mrkEmail = document.getElementById('mrkEmail').value;
// var mrkPhone = document.getElementById('mrkPhone').value;
// var mrkPIC = document.getElementById('mrkPIC').value;
// var mrkCustomerID = document.getElementById('mrkCustomerID').value;

// var btnSave = document.getElementById('btnSave').value;
// var btnUpdate = document.getElementById('btnUpdate').value;

function FuncClear(){
	$('#mrkCustBranchID').hide();
	$('#txtCustBranchID').prop('disabled', false);
	$('#mrkCustBranchName').hide();
	$('#mrkCustBranchAddress').hide();
	$('#mrkEmail').hide();
	$('#mrkPhone').hide();
	$('#mrkPIC').hide();
	$('#mrkCustomerID').hide();
	
	$('#dvCustBranchID').removeClass('has-error');
	$('#dvCustBranchName').removeClass('has-error');
	$('#dvCustBranchAddress').removeClass('has-error');
	$('#dvEmail').removeClass('has-error');
	$('#dvPhone').removeClass('has-error');
	$('#dvPIC').removeClass('has-error');
	$('#dvCustomerID').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Customer Branch";
	document.getElementById("lblCustomerName").innerHTML = null;	

	FuncClear();
	$('#txtCustBranchID').prop('disabled', false);
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById("lblTitleModal").innerHTML = 'Edit Customer Branch';

	FuncClear();
	$('#txtCustBranchID').prop('disabled', true);
}

function FuncValEmptyInput(lParambtn) {
	var txtCustBranchID = document.getElementById('txtCustBranchID').value;
	var txtCustBranchName = document.getElementById('txtCustBranchName').value;
	var txtCustBranchAddress = document.getElementById('txtCustBranchAddress').value;
	var txtEmail = document.getElementById('txtEmail').value;
	var txtPhone = document.getElementById('txtPhone').value;
	var txtPIC = document.getElementById('txtPIC').value;
	var txtCustomerID = document.getElementById('txtCustomerID').value;
	
	var dvCustBranchID = document.getElementsByClassName('dvCustBranchID');
	var dvCustBranchName = document.getElementsByClassName('dvCustBranchName');
	var dvCustBranchAddress = document.getElementsByClassName('dvCustBranchAddress');
	var dvEmail = document.getElementsByClassName('dvEmail');
	var dvPhone = document.getElementsByClassName('dvPhone');
	var dvPIC = document.getElementsByClassName('dvPIC');
	var dvCustomerID = document.getElementsByClassName('dvCustomerID');
	
	if(lParambtn == 'save'){
		$('#txtCustBranchID').prop('disabled', false);
	}
	else{
		$('#txtCustBranchID').prop('disabled', true);
	}
	
    if(!txtCustBranchID.match(/\S/)) {
    	$("#txtCustBranchID").focus();
    	$('#dvCustBranchID').addClass('has-error');
    	$('#mrkCustBranchID').show();
        return false;
    } 
    
    if(!txtCustBranchName.match(/\S/)) {    	
    	$('#txtCustBranchName').focus();
    	$('#dvCustBranchName').addClass('has-error');
    	$('#mrkCustBranchName').show();
        return false;
    } 
    
    if(!txtCustBranchAddress.match(/\S/)) {
    	$('#txtCustBranchAddress').focus();
    	$('#dvCustBranchAddress').addClass('has-error');
    	$('#mrkCustBranchAddress').show();
        return false;
    }
    
    if(!txtPhone.match(/\S/)) {
    	$('#txtPhone').focus();
    	$('#dvPhone').addClass('has-error');
    	$('#mrkPhone').show();
        return false;
    } 
    
    if(!txtEmail.match(/\S/)) {
    	$('#txtEmail').focus();
    	$('#dvEmail').addClass('has-error');
    	$('#mrkEmail').show();
        return false;
    } 
    
    if(!txtPIC.match(/\S/)) {
    	$('#txtPIC').focus();
    	$('#dvPIC').addClass('has-error');
    	$('#mrkPIC').show();
        return false;
    } 
    
    if(!txtCustomerID.match(/\S/)) {
    	$('#txtCustomerID').focus();
    	$('#dvCustomerID').addClass('has-error');
    	$('#mrkCustomerID').show();
        return false;
    } 
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/CustomerBranch',	
        type:'POST',
        data:{"key":lParambtn,"txtCustBranchID":txtCustBranchID,"txtCustBranchName":txtCustBranchName,"txtCustBranchAddress":txtCustBranchAddress, "txtEmail":txtEmail, "txtPhone":txtPhone,"txtPIC":txtPIC, "txtCustomerID":txtCustomerID},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertCustomerBranch')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan cabang pelanggan";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtCustBranchID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateCustomerBranch')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui cabang pelanggan";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtCustBranchName").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/CustomerBranch';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtCustomerID:focus').length) {
    	$('#txtCustomerID').click();
    }
});
</script>


</body>
</html>