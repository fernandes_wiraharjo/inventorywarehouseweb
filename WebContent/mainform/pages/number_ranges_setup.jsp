<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>WMS Number Range</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="form_wmnumber_range" name="form_wmnumber_range" action = "${pageContext.request.contextPath}/numberranges" method="post">
<input type="hidden" name="temp_string" value="" />

	<div class="wrapper">
	
		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Number Ranges for Warehouse Management
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertNumberRanges'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan urutan penomoran.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateNumberRanges'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui urutan penomoran.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessDeleteNumberRanges'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menghapus urutan penomoran.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedDeleteNumberRanges'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal menghapus urutan penomoran. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>
							
							<!--modal update & Insert -->
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	        								<div id="dvPlantID" class="form-group">
	            								<label for="lblPlantID" class="control-label">Plant</label><label id="mrkPlantID" for="formrkPlantID" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" readonly="true">
	          								</div>
	          								<div id="dvWarehouseID" class="form-group">
	            								<label for="lblWarehouseID" class="control-label">Warehouse</label><label id="mrkWarehouseID" for="formrkWarehouseID" class="control-label"><small>*</small></label>	
	            								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
	          								</div>
	        								<div id="dvNumberRangesType" class="form-group">
	            								<label for="lblNumberRangesType" class="control-label">Number Ranges Type</label><label id="mrkNumberRangesType" class="control-label"><small>*</small></label>	
	            								<select id="slNumberRangesType" name="slNumberRangesType" class="form-control">
							                    <option>For Transfer Requirement</option>
							                    <option>For Transfer Order</option>
							                    <option>For Quant</option>
							                    <option>For Post. Change Notice</option>
							                    <option>For Group</option>
							                    <option>For Storage Unit</option>
							                    </select>
	          								</div>
	          								<div id="dvNumberFrom" class="form-group">
	            								<label for="lblNumberFrom" class="control-label">Number From</label><label id="mrkNumberFrom" for="formrkNumberFrom" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtNumberFrom" name="txtNumberFrom">
	          								</div>
	          								<div id="dvNumberTo" class="form-group">
	            								<label for="lblNumberTo" class="control-label">Number To</label><label id="mrkNumberTo" for="formrkNumberTo" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtNumberTo" name="txtNumberTo">
	          								</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									<!-- end of update insert modal -->
									
									<!--modal Delete -->
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete WMS Number Ranges Setup</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_slNumberRangesType" name="temp_slNumberRangesType"  />
              									<input type="hidden" id="temp_PlantID" name="temp_PlantID"  />
              									<input type="hidden" id="temp_WarehouseID" name="temp_WarehouseID"  />
               	 									<p>Are you sure to delete this number ranges type ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								       
								
								<div class="col-xs-8" class="form-horizontal" style="margin-top: 10px">
								<div id="dvslPlantId" class="form-group">
				         		<label for="inputEmail3" class="col-sm-2 control-label" style="Height: 34px;text-align:center; margin-top: 7px">Plant Id</label><label id="mrkslPlantId"></label>
			         			<div class="col-sm-10">
			
								<select id="slPlantId" name="slPlantId"
																	class="form-control select2" 
																	data-placeholder="Select Brand" style="width: 200px;">
																	<c:forEach items="${listPlant}" var="plant">
																		<option id="optplant" name="optplant"
																			value="<c:out value="${plant.plantID}" />"><c:out
																				value="${plant.plantID}" /> - <c:out
																			value="${plant.plantNm}" /></option>
																</c:forEach>
								</select>
								
								</div>
				       			</div>
				       			</div>
							
										<!--modal show warehouse data -->
										<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelWarehouseID">Data Warehouse</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content">
           										</div>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show warehouse data -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br><br><br>
							<table id="tb_wmnumber_ranges" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Plant ID</th>
										<th>Warehouse ID</th>
										<th>Type</th>
										<th>From Number</th>
										<th>To Number</th>
										<th style="width:60px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listWMNumberRange}" var="numberrange">
										<tr>
											<td><c:out value="${numberrange.plantID}" /></td>
											<td><c:out value="${numberrange.warehouseID}" /></td>
											<td><c:out value="${numberrange.numberRangesType}" /></td>
											<td><c:out value="${numberrange.fromNumber}" /></td>
											<td><c:out value="${numberrange.toNumber}" /></td>
											<td><button <c:out value="${buttonstatus}"/> 
														id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert"
														data-lplantid='<c:out value="${numberrange.plantID}" />'
														data-lwarehouseid='<c:out value="${numberrange.warehouseID}" />'
														data-lfrom='<c:out value="${numberrange.fromNumber}" />'
														data-lto='<c:out value="${numberrange.toNumber}" />'
														data-lnumberrangetype='<c:out value="${numberrange.numberRangesType}" />'>
														<i class="fa fa-edit"></i></button> 
												<button <c:out value="${buttonstatus}"/>
														id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger"
														data-toggle="modal" 
														data-target="#ModalDelete"
														data-lnumberrangetype='<c:out value="${numberrange.numberRangesType}" />'
														data-lplantid='<c:out value="${numberrange.plantID}" />'
														data-lwarehouseid='<c:out value="${numberrange.warehouseID}" />'
														>
												<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
																
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

</div>
<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
 		$(".select2").select2();
  		$("#tb_wmnumber_ranges").DataTable();
  		$('#M002').addClass('active');
  		$('#M046').addClass('active');
  		
  		$('#slPlantId').val(<c:out value="${tempPlantID}"/>).trigger("change");
  		$("#dvErrorAlert").hide();
  		
  		//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
  	});
  	
  	$('#slPlantId').on("select2:select", function(e) {
	var slPlantId = $('#slPlantId').val();
	FuncLoadNumberRangesSetup(slPlantId);
	$('#slPlantId').val(slPlantId).trigger("change.select2");
	})
	
	function FuncLoadNumberRangesSetup(slPlantId) {
	var slPlantId_temp = slPlantId;
	jQuery.ajax({
        url:'${pageContext.request.contextPath}/numberranges',	
        type:'POST',
        data:{"key":"LoadNumberRanges","slPlantId":slPlantId},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	var url = '${pageContext.request.contextPath}/numberranges';  
        	$(location).attr('href', url);
        	$('#slPlantId').val(slPlantId_temp).trigger("change");
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
	
	return true;
	}
	</script>
	
	<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lNumberRangeType = button.data('lnumberrangetype');
		var lPlantID = button.data('lplantid');
		var lWarehouseID = button.data('lwarehouseid');
		$("#temp_slNumberRangesType").val(lNumberRangeType);
		$("#temp_PlantID").val(lPlantID);
		$("#temp_WarehouseID").val(lWarehouseID);
	})
	</script>
	
	<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		
 		var lPlantID = button.data('lplantid');
 		var lWarehouseID = button.data('lwarehouseid');
 		var lNumberFrom = button.data('lfrom');
 		var lNumberTo = button.data('lto');
 		var lNumberRangeType = button.data('lnumberrangetype');
 		
 		var modal = $(this);
 		
 		if(lPlantID==null)
 			$('#txtPlantID').val(<c:out value="${tempPlantID}"/>);
 		else
 			modal.find(".modal-body #txtPlantID").val(lPlantID);
 			
 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
 		modal.find(".modal-body #txtNumberFrom").val(lNumberFrom);
 		modal.find(".modal-body #txtNumberTo").val(lNumberTo);
 		modal.find(".modal-body #slNumberRangesType").val(lNumberRangeType);
 		
 		$("#txtWarehouseID").focus();
	})
	</script>

	<script>
	
	function FuncClear(){
		document.getElementById("lblWarehouseName").innerHTML = "";
		$('#mrkNumberRangesType').hide();
		$('#mrkNumberFrom').hide();
		$('#mrkNumberTo').hide();
		$('#mrkPlantID').hide();
		$('#mrkWarehouseID').hide();
		
		$('#dvNumberRangesType').removeClass('has-error');
		$('#dvNumberFrom').removeClass('has-error');
		$('#dvNumberTo').removeClass('has-error');
		$('#dvPlantID').removeClass('has-error');
		$('#dvWarehouseID').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
	}
	
	function FuncButtonNew() {
		$('#btnSave').show();
		$('#btnUpdate').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add Number Ranges";
	
		FuncClear();
		$('#txtWarehouseID').prop('disabled', false);
		$('#slNumberRangesType').prop('disabled', false);
	}
	
	function FuncButtonUpdate() {
		$('#btnSave').hide();
		$('#btnUpdate').show();
		document.getElementById("lblTitleModal").innerHTML = 'Edit Number Ranges';
	
		FuncClear();
		$('#txtWarehouseID').prop('disabled', true);
		$('#slNumberRangesType').prop('disabled', true);
	}
	
	function FuncValEmptyInput(lParambtn) {
		var txtPlantID = document.getElementById('txtPlantID').value;
		var txtWarehouseID = document.getElementById('txtWarehouseID').value;
		var txtNumberFrom = document.getElementById('txtNumberFrom').value;
		var txtNumberTo = document.getElementById('txtNumberTo').value;
		var slNumberRangesType = document.getElementById('slNumberRangesType').value;
	
		var dvPlantID = document.getElementsByClassName('dvPlantID');
		var dvWarehouseID = document.getElementsByClassName('dvWarehouseID');
		var dvNumberRangesType = document.getElementsByClassName('dvNumberRangesType');
		var dvNumberFrom = document.getElementsByClassName('dvNumberFrom');
		var dvNumberTo = document.getElementsByClassName('dvNumberTo');
		
		if(!txtPlantID.match(/\S/)) {
	    	$("#txtPlantID").focus();
	    	$('#dvPlantID').addClass('has-error');
	    	$('#mrkPlantID').show();
	        return false;
	    } 
	    
	    if(!txtWarehouseID.match(/\S/)) {
	    	$("#txtWarehouseID").focus();
	    	$('#dvWarehouseID').addClass('has-error');
	    	$('#mrkWarehouseID').show();
	        return false;
	    } 
		
	    if(!slNumberRangesType.match(/\S/)) {
	    	$("#slNumberRangesType").focus();
	    	$('#dvNumberRangesType').addClass('has-error');
	    	$('#mrkNumberRangesType').show();
	        return false;
	    } 
	    
	    if(!txtNumberFrom.match(/\S/)) {    	
	    	$('#txtNumberFrom').focus();
	    	$('#dvNumberFrom').addClass('has-error');
	    	$('#mrkNumberFrom').show();
	        return false;
	    } 
	    
	    if(!txtNumberTo.match(/\S/)) {
	    	$('#txtNumberTo').focus();
	    	$('#dvNumberTo').addClass('has-error');
	    	$('#mrkNumberTo').show();
	        return false;
	    } 
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/numberranges',	
	        type:'POST',
	        data:{"key":lParambtn,"slPlantId":txtPlantID,"txtWarehouseID":txtWarehouseID,
	        		"txtNumberFrom":txtNumberFrom,"txtNumberTo":txtNumberTo,"slNumberRangesType":slNumberRangesType},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertNumberRanges')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan urutan penomoran";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtNumberFrom").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateNumberRanges')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui urutan penomoran";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtNumberFrom").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/numberranges';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	function FuncValPlant(){	
		var txtPlantID = document.getElementById('txtPlantID').value;
		
		if(!txtPlantID.match(/\S/)) {    	
	    	$('#txtPlantID').focus();
	    	$('#dvPlantID').addClass('has-error');
	    	$('#mrkPlantID').show();
	    	
	    	alert("Fill Plant ID First ...!!!");
	        return false;
	    } 
		
	    return true;	
	}
	
	function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
	$("#txtWarehouseID").val(lParamWarehouseID);
	document.getElementById('lblWarehouseName').innerHTML = '(' + lParamWarehouseName + ')';
	}
	
	// 	get warehouse from plant id
	$(document).ready(function(){
	
	    $(document).on('click', '#txtWarehouseID', function(e){
	  
	     e.preventDefault();
	  
		 var plantid = document.getElementById('txtPlantID').value;
	  
	     $('#dynamic-content').html(''); // leave this div blank
		//$('#modal-loader').show();      // load ajax loader on button click
	 
	     $.ajax({
	          url: '${pageContext.request.contextPath}/getwarehouse',
	          type: 'POST',
	          data: 'plantid='+plantid,
	          dataType: 'html'
	     })
	     .done(function(data){
	          console.log(data); 
	          $('#dynamic-content').html(''); // blank before load.
	          $('#dynamic-content').html(data); // load here
			  //$('#modal-loader').hide(); // hide loader  
	     })
	     .fail(function(){
	          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			  //$('#modal-loader').hide();
	     });
	
	    });
	});
	
	//set the focus behaviour into click
	$(window).keyup(function (e) {
	    var code = (e.keyCode ? e.keyCode : e.which);
	    if (code == 9 && $('#txtWarehouseID:focus').length) {
	    	$('#txtWarehouseID').click();
	    }
	});
	</script>


</body>
</html>