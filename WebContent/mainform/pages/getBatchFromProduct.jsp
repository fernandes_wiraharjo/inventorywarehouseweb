<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form id="getBatch" name="getBatch" action = "${pageContext.request.contextPath}/getbatch" method="post">

<table id="tb_master_batch" class="table table-bordered table-hover">
   <thead style="background-color: #d2d6de;">
              <tr>
                <th>Product ID</th>
<!--                 <th>Product Name</th> -->
                <th>Batch No</th>
                <th>Packing No</th>
                <th>Expired Date</th>
                <th>Vendor Batch</th>
                <th>Vendor ID</th>
                <th style="width: 20px"></th>
              </tr>
      </thead>
      
      <tbody>
      
      <c:forEach items="${listbatch}" var ="batch">
        <tr>
        <td><c:out value="${batch.productID}"/></td>
<%--         <td><c:out value="${batch.productNm}"/></td> --%>
        <td><c:out value="${batch.batch_No}"/></td>
        <td><c:out value="${batch.packing_No}"/></td>
        <td><c:out value="${batch.expired_Date}"/></td>
        <td><c:out value="${batch.vendor_Batch_No}"/></td>
        <td><c:out value="${batch.vendorID}"/></td>
        <td><button type="button" class="btn btn-primary"
        			data-toggle="modal"
        			onclick="FuncPassString('','','<c:out value="${batch.batch_No}"/>')"
        			data-dismiss="modal"
        	><i class="fa fa-fw fa-check"></i></button>
        </td>
      		</tr>
      		
      </c:forEach>
      
      </tbody>
      </table>

</form>

<script>
 		$(function () {
  	  		$("#tb_master_batch").DataTable();
  		});
	</script>