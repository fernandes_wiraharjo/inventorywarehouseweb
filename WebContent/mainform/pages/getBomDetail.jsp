<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- <form id="getBomDetail" name="getBomDetail" action = "${pageContext.request.contextPath}/getcanceltransactiondetail" method="post"> --%>

<table id="tb_bom_detail" class="table table-bordered table-hover">
    <thead style="background-color: #d2d6de;">
    <tr>
    <th>Component Line</th>
	<th>Component ID</th>
	<th>Qty</th>
	<th>UOM</th>
	<th>Qty Base UOM</th>
	<th>Base UOM</th>
   </tr>
   </thead>
     
     <tbody>
     
     <c:forEach items="${listBomDetail}" var ="bomdetail">
       <tr>
	<td><c:out value="${bomdetail.componentLine}" /></td>
	<td><c:out value="${bomdetail.componentID}" /></td>
	<td><c:out value="${bomdetail.qty}" /></td>
	<td><c:out value="${bomdetail.UOM}" /></td>
	<td><c:out value="${bomdetail.qtyBaseUOM}" /></td>
	<td><c:out value="${bomdetail.baseUOM}" /></td>
     </tr>
     		
     </c:forEach>
     
     </tbody>
     </table>

<!-- </form> -->

<script>
 		$(function () {
 			$("#tb_bom_detail").DataTable();
  		});
	</script>