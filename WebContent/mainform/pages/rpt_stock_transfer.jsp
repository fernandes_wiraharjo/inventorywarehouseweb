<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Report Stock Transfer</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="RptStockTransfer" name="RptStockTransfer" action = "${pageContext.request.contextPath}/RptStockTransfer" method="post" target="_blank">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Report Stock Transfer<small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<!-- Show Parameter -->
			<div class="row">
		        <div class="col-md-12">
        		  <div id="dvBoxParam" class="box">
        		  	
        		  	
        		  
            		<div class="box-header with-border">
              		<h3 class="box-title">Parameter Report</h3>

  		            <div class="box-tools pull-right">
  		            
       		        <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">Export &nbsp<i class="fa  fa-share-square-o"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li onclick="sendvalueLi('pdf')"><a >Export Pdf</a></li>
                    <li onclick="sendvalueLi('excel');"><a >Export Excel</a></li>
					<!-- <li onclick="sendvalueLi('html');"><a >Export Html</a></li> -->
                    <input type="hidden" id="exporttype" name="exporttype" value="" />
                  </ul>
              	  </div>
       		        
       		        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
       		         
                
              </div>
            </div>
            <!-- /.box-header -->
            
            
            <div class="box-body">
            
              <div class="row">
              <div class="col-md-6">
             	 <div id="dvStartDate" class="form-group">
	            								<label for="message-text" class="control-label">Start Date</label><label id="mrkStartDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtStartDate" name="txtStartDate" data-date-format="dd M yyyy">
	          								</div>
	         	 </div>
	         	 <div class="col-md-6">
	         	 <div id="dvEndDate" class="form-group">
	            								<label for="message-text" class="control-label">End Date</label><label id="mrkEndDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtEndDate" name="txtEndDate" data-date-format="dd M yyyy">
	          								</div>
	         	 </div>
	         	 <div class="col-md-6">
	         	 <div class="checkbox">
	         	 <label>
	         	 	<input id="cbCancTrans" name="cbCancTrans" value="Stock Transfer" type="checkbox"> Cancel Stock Transfer
	         	 </label>
	         	 </div>
	         	 </div>
	         	 <div class="col-md-6">
	         	 	<button type="button" class="btn btn-primary pull-right" id="btnShow" name="btnShow" onclick="FuncValEmptyInput('show')">Show</button>
	         	 </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
      
    		<!-- Jika Yg Ingin Ditampilkan Table Good Receipt -->
    		<div id="dvTableRptStockTrans">
    		</div>
    		
    		<!-- Jika Yg Ingin Ditampilkan Cancel Table Good Receipt -->
    		<div id="dvTableRptCancelStockTrans">
    		</div>
			 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 		$(function () {
//   			$("#tb_itemlib").DataTable( {
//   		        "scrollX": true
//   		    });

			FuncClear()
  			$('#M009').addClass('active');
  	  		$('#M044').addClass('active');
  	  		
  	  		$('#txtStartDate').datepicker({
		      format: 'dd M yyyy',
		      autoclose: true
		    });
  	  		$('#txtStartDate').datepicker('setDate', new Date());
	  	
	  		$('#txtEndDate').datepicker({
		      format: 'dd M yyyy',
		      autoclose: true
		    });
	  		$('#txtEndDate').datepicker('setDate', new Date());
  		});
 		
 		
	  	
	  	function sendvalueLi(str) {
 			$("#exporttype").val(str);
 			RptStockTransfer.submit() 
 		}
	  	
	  	function FuncClear(){
//  		$('#txtStartDate').val('');
//  		$('#txtEndDate').val('');

	  		$('#mrkStartDate').hide();		
 			$('#dvStartDate').removeClass('has-error');
 			
 			$('#mrkEndDate').hide();		
 			$('#dvEndDate').removeClass('has-error');
 		}
 		
 		function FuncValEmptyInput(lParambtn) {
 			var txtStartDate = document.getElementById('txtStartDate').value;
 			var txtEndDate = document.getElementById('txtEndDate').value;
 			var cbCancTrans = document.getElementById('cbCancTrans').value;		
 			
 			FuncClear();			

 		    if(!txtStartDate.match(/\S/)) {
 		    	$("#txtStartDate").focus();
 		    	$('#dvStartDate').addClass('has-error');
 		    	$('#mrkStartDate').show();
 		        return false;
 		    } 
 		    
 		   if(!txtEndDate.match(/\S/)) {
		    	$("#txtEndDate").focus();
		    	$('#dvEndDate').addClass('has-error');
		    	$('#mrkEndDate').show();
		        return false;
		    } 
 		   
 		//Buat disini Validasi Jika Dia Cancel Link ke Servlet TableCancelRptStockTransfer
			if(document.getElementById('cbCancTrans').checked) {
   			//Function jika Cancel
				$('#dvTableRptStockTrans').hide();
				$('#dvTableRptCancelStockTrans').show();
				
			 $('#dvTableRptCancelStockTrans').html(''); // leave this div blank
//		     $('#modal-loader').show();      // load ajax loader on button click
		 
		     $.ajax({
		          url: '${pageContext.request.contextPath}/TableCancTrans',
		          type: 'POST',
		          data:{key:lParambtn,txtStartDate:txtStartDate,txtEndDate:txtEndDate,cbCancTrans:cbCancTrans},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dvTableRptCancelStockTrans').html(''); // blank before load.
		          $('#dvTableRptCancelStockTrans').html(data); // load here
//		           $('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dvTableRptCancelStockTrans').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//		           $('#modal-loader').hide();
		     });
			return true;
				
			} else {
				$('#dvTableRptStockTrans').show();
				$('#dvTableRptCancelStockTrans').hide();
				
				$('#dvTableRptStockTrans').html(''); // leave this div blank
//			     $('#modal-loader').show();      // load ajax loader on button click

				$.ajax({
			          url: '${pageContext.request.contextPath}/TableRptStockTransServlet',
			          type: 'POST',
			          data:{key:lParambtn,txtStartDate:txtStartDate,txtEndDate:txtEndDate,cbCancTrans:cbCancTrans},
			          dataType: 'html'
			     })
			     .done(function(data){
			          console.log(data); 
			          $('#dvTableRptStockTrans').html(''); // blank before load.
			          $('#dvTableRptStockTrans').html(data); // load here
//			           $('#modal-loader').hide(); // hide loader  
			     })
			     .fail(function(){
			          $('#dvTableRptStockTrans').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//			           $('#modal-loader').hide();
			     });		
				
			    return true; 
 		}
 	}
	</script>
</body>
</html>