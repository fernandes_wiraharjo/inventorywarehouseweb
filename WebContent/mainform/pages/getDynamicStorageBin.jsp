<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_storagebin" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>Storage Bin ID</th>
			<th>Storage Section ID</th>
			<th>Storage Bin Type ID</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listStorageBin}" var ="storbin">
	  <tr>
	  <td><c:out value="${storbin.storageBinID}" /></td>
		<td><c:out value="${storbin.storageSectionID}" /></td>
		<td><c:out value="${storbin.storageBinTypeID}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassDynamicStorageBinData('<c:out value="${storbin.storageBinID}"/>','<c:out value="${type}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_storagebin").DataTable();
  	});
</script>