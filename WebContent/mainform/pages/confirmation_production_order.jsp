<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Confirmation Production Order</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<!-- <style type="text/css">	 -->
<!-- #ModalUpdateInsert { overflow-y:scroll } -->
<!-- </style> -->

<!-- <style type="text/css"> -->
<!-- /* <!-- Disable div -->
<!-- /* .disablediv { */ -->
<!-- /*     pointer-events: none; */ -->
<!-- /*     opacity: 0.4; */ -->
<!-- /* } */ -->
<!-- </style> -->
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="" name="" action = "${pageContext.request.contextPath}/ConfirmationProductionOrder" method="post">
<input type="hidden" name="temp_string" value="" />
<input type="hidden" class="form-control" id="temp_validate" name="temp_validate">
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Confirmation Production Order <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
	     					<c:if test="${condition == 'SuccessInsertConfirmation'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses konfirmasi produksi.
              				</div>
	      					</c:if>
	      					
	      					<!--modal update & Insert -->
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div id="ConfirmationHeader" class="modal-body">
	          								<div id="dvConfirmationID" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Confirmation ID</label><label id="mrkConfirmationID" for="recipient-name" class="control-label"><small>*</small></label>
	            								<small><label id="lblConfirmationDocName" name="lblConfirmationDocName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtConfirmationID" name="txtConfirmationID" data-toggle="modal" data-target="#ModalGetConfirmationDoc">
	          								</div>
	          								<div id="dvConfirmationNo" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Confirmation No</label><label id="mrkConfirmationNo" for="recipient-name" class="control-label"><small>*</small></label>
	            								<input type="text" class="form-control" id="txtConfirmationNo" name="txtConfirmationNo" readonly="readonly" onfocus="FuncValConfirmationNo()">
	          								</div>
	          								<div id="dvDate" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Confirmation Date</label><label id="mrkDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDate" name="txtDate" data-date-format="dd M yyyy">
	          								</div>
	          								<div id="dvProductionOrder" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Production Order</label><label id="mrkProductionOrder" for="recipient-name" class="control-label"><small>*</small></label>
	            								<input type="text" class="form-control" id="txtProductionOrder" name="txtProductionOrder" data-toggle="modal" data-target="#ModalGetProductionOrder">
	          									
	          									<input type="hidden" id="temp_txtOrderTypeID" name="temp_txtOrderTypeID"/>
	          									<input type="hidden" id="temp_txtOrderNo" name="temp_txtOrderNo"/>
	          									<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"/>
	          									<input type="hidden" id="temp_txtWarehouseID" name="temp_txtWarehouseID"/>
	          								</div>
	          								
	          								<div class="row"></div>	
	          							</div>
	          							
	          								<div class="modal-footer">
	          									<button type="button" class="btn btn-primary" id="btnNext" name="btnNext" onclick="FuncValNext()">Next</button>
	          									<button type="button" class="btn btn-default" id="btnEditHeader" name="btnEditHeader" onclick="FuncEditConfirmationHeader()">Edit</button>
	          								</div>
	          								
          								<div id="ReceivedProduct" class="modal-body">
          									<div align="center"><label for="recipient-name" class="control-label">-- RECEIVED PRODUCT --</label></div>
          									
          									<div id="dvProductID" class="form-group">
	            								<label for="recipient-name" class="control-label">Product ID</label>	
	            								<small><label id="lblProductName" name="lblProductName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtProductID" name="txtProductID" readonly="readonly">
	          								</div>
	          								<div id="dvPackingNo" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Packing No</label><label id="mrkPackingNo" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtPackingNo" name="txtPackingNo">
	          								</div>
	          								<div id="dvExpiredDate" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Expired Date</label><label id="mrkExpiredDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtExpiredDate" name="txtExpiredDate" data-date-format="dd M yyyy">
	          								</div>
	          								<div id="dvQty" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Quantity</label><label id="mrkQty" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblTargetProduction" name="lblTargetProduction" class="control-label"></label></small>
	            								<input type="hidden" class="form-control" id="temp_txtTargetQty" name="temp_txtTargetQty">
	            								<input type="hidden" class="form-control" id="temp_txtTargetUOM" name="temp_txtTargetUOM">
	            								<input type="number" class="form-control" id="txtQty" name="txtQty">
	          								</div>
	          								<div id="dvUOM" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">UOM</label><label id="mrkUOM" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtUOM" name="txtUOM" readonly="readonly">
<!-- 	            								<select id="slMasterUOM" name="slMasterUOM" class="form-control"> -->
<!-- 							                    </select> -->
	          								</div>
	          								
	          								<div class="row"></div>	 
	          								
          								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
<!--         									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button> -->
        									<button type="button" id="btnDismiss" name="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
      								</div>
    										</div>
  										</div>
									</div>
										
										<!--modal show master confirmation document data -->
										<div class="modal fade" id="ModalGetConfirmationDoc" tabindex="-1" role="dialog" aria-labelledby="ModalLabelConfirmationID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelConfirmationID"> Master Confirmation Document Data</h4>	
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_confirmation_document" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Confirmation ID</th>
										                <th>Description</th>
										                <th>From</th>
										                <th>To</th>
										                <th>Type</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listConfirmationDoc}" var ="confirmationdoc">
												        <tr>
												        <td><c:out value="${confirmationdoc.confirmationID}"/></td>
												        <td><c:out value="${confirmationdoc.confirmationDesc}"/></td>
												        <td><c:out value="${confirmationdoc.rangeFrom}"/></td>
												        <td><c:out value="${confirmationdoc.rangeTo}"/></td>
												        <td><c:out value="${confirmationdoc.confirmationType}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassConfirmationID('<c:out value="${confirmationdoc.confirmationID}"/>','<c:out value="${confirmationdoc.confirmationDesc}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show master confirmation document data -->
										
										<!--modal show production order data -->
										<div class="modal fade bs-example-modal-lg" id="ModalGetProductionOrder" tabindex="-1" role="dialog" aria-labelledby="ModalLabelProductionOrder">
												<div class="modal-dialog modal-lg" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelProductionOrderTitle"> Production Order Data</h4>	
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_production_order" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Order Type</th>
										                <th>Order No</th>
										                <th>Product</th>
										                <th>Product Name</th>
										                <th>Plant</th>
										                <th>StartDate</th>
										                <th>Warehouse</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listProductionOrder}" var ="production">
												        <tr>
												        <td><c:out value="${production.orderTypeID}"/></td>
												        <td><c:out value="${production.orderNo}"/></td>
												        <td><c:out value="${production.productID}"/></td>
												        <td><c:out value="${production.productName}"/></td>
												        <td><c:out value="${production.plantID}"/></td>
												        <td><c:out value="${production.startDate}"/></td>
												        <td><c:out value="${production.warehouseID}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassProductionOrder('<c:out value="${production.orderTypeID}"/>','<c:out value="${production.orderNo}"/>','<c:out value="${production.plantID}"/>','<c:out value="${production.warehouseID}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show production order data -->
										
										<!--modal show confirmation production detail data -->
										<div class="modal fade bs-example-modal-lg" id="ModalConfirmationProductionDetail" tabindex="-1" role="dialog" aria-labelledby="ModalLabelConfirmationProductionDetail">
												<div class="modal-dialog modal-lg" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelConfirmationDetail">Confirmation Production Detail Data</h4>
			      											<br><br>
			      											<label id="confirmationdocno"></label>	
			      												
			      											<button id="btnFinish" name="btnFinish" type="button" class="btn btn-primary pull-right">Confirm</button> <br><br>	
			      											<button id="btnaddComponent" name="btnaddComponent" type="button" 
			      											class="btn btn-primary pull-right" data-toggle="modal" 
			      											data-target="#ModalAdditionalComponent">Add Component</button>	
			    											</div>
			     								<div class="modal-body">
			     								
			     								<div id="divsuccess" name="divsuccess" class="alert alert-success alert-dismissible">
												 	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<h4><i class="icon fa fa-check"></i> Success</h4>
												  	<label id="successlabel"></label>
												</div>  					
												<div id="divfailed" name="divfailed" class="alert alert-danger alert-dismissible">
													<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<h4><i class="icon fa fa-ban"></i> Failed</h4>
													<label id="failedlabel"></label>
												</div>
			       								
			       								<!-- mysql data load confirmation production detail will be load here -->                          
           										<div id="dynamic-content-confirmation-detail">
           										</div>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show confirmation production detail data -->
										
										<!--modal show Batch data -->
										<div class="modal fade bs-example-modal-lg" id="ModalGetBatch" tabindex="-1" role="dialog" aria-labelledby="ModalLabelBatchID">
												<div class="modal-dialog modal-lg" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelBatchID"></h4>	
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load batch by componentid will be load here -->                          
           										<div id="dynamic-content-batch">
           										</div>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show Batch data -->
										
										<!--modal add additional component -->
									<div class="modal fade" id="ModalAdditionalComponent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    											<div id="divfailedcomponent" name="divfailedcomponent" class="alert alert-danger alert-dismissible">
													<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<h4><i class="icon fa fa-ban"></i> Failed</h4>
													<label id="failedlabelcomponent"></label>. <label id="lblAlertDescriptionComponent"></label>.
												</div>
												
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalAdditionalComponent" name="lblTitleModalAdditionalComponent">Add Additional Component</label></h4>	
        												
        											
      											</div>
      										<div class="modal-body">
	          								<div id="dvComponentLine" class="form-group">
	            								<label for="message-text" class="control-label">Component Line</label>
	            								<input type="text" class="form-control" id="txtComponentLine" name="txtComponentLine" readonly="readonly">
	          								</div>
	          								<div id="dvComponentID" class="form-group">
	            								<label for="message-text" class="control-label">Component Id</label><label id="mrkComponentID" for="recipient-name" class="control-label"><small>*</small></label>
	            								<small><label id="lblComponentName" name="lblComponentName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtComponentID" name="txtComponentID" data-toggle="modal" data-target="#ModalGetComponent">
	          								</div>
<!-- 	          								<div id="dvBatchNo" class="form-group"> -->
<!-- 	            								<label for="message-text" class="control-label">Batch No</label><label id="mrkBatchNo" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtBatchNo" name="txtBatchNo" data-toggle="modal" data-target="#ModalGetBatch" onfocus="FuncValComponent()"> -->
<!-- 	          								</div> -->
	          								<div id="dvQtyComponent" class="form-group">
	            								<label for="message-text" class="control-label">Quantity</label><label id="mrkQtyComponent" for="recipient-name" class="control-label"><small>*</small></label>
	            								<input type="number" class="form-control" id="txtQtyComponent" name="txtQtyComponent">
	          								</div>
	          								<div id="dvUOMComponent" class="form-group">
	            								<label for="message-text" class="control-label">UOM</label><label id="mrkUOMComponent" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<select id="slMasterUOMComponent" name="slMasterUOMComponent" class="form-control" onfocus="FuncValComponent()" >
							                    </select>
	          								</div>
	          								</div>
      								
      								<div class="modal-footer">
        									<button type="button" class="btn btn-primary" id="btnSaveComponent" name="btnSaveComponent" onclick="FuncValEmptyInputComponent('saveComponent')">Save</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      								</div>
    										</div>
  										</div>
									</div>
<!-- 									end of modal add additional component -->
									
									<!--modal show product data -->
										<div class="modal fade" id="ModalGetComponent" tabindex="-1" role="dialog" aria-labelledby="ModalLabelProductID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelProductID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_component" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Product ID</th>
														<th>Product Name</th>
														<th>Description</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listComponent}" var ="component">
												        <tr>
												        <td><c:out value="${component.id}" /></td>
														<td><c:out value="${component.title_en}" /></td>
														<td><c:out value="${component.short_description_en}" /></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassStringComponent('<c:out value="${component.id}"/>','<c:out value="${component.title_en}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show product data -->
										
										<!--modal show temporary component batch summary -->
										<div class="modal fade bs-example-modal-lg" id="ModalShowBatch" tabindex="-1" role="dialog" aria-labelledby="ModalLabelBatchNo">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
													<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<h4 class="modal-title" id="ModalLabelBatchNo">Component Batch Data</h4>
															<br>
															<label id="lblproductioncomponentinfo"></label>		
													</div>
															<div class="modal-body">
																		
															<!-- mysql data load component batch will be load here -->                          
																<div id="dynamic-content-batch-summary">
																</div>
																		
															</div>
															
															<div class="modal-footer">
																<button type="button" class="btn btn-primary" id="btnConfirmBatch" name="btnConfirmBatch" onclick="FuncSetBatch()">Set</button>		
															</div>
															</div>
													</div>
										</div>
										<!-- /. end of modal show temporary component batch summary -->
										
										<!--modal show form to update production order detail for confirmation -->
										<div class="modal fade" id="ModalUpdateProductionOrderDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    									<div class="modal-content">
    										
    										<div id="dvErrorAlert2" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert2"></label>. <label id="lblAlertDescription2"></label>.
					     					</div>
	     					
										<div class="modal-header">
 											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 											<h4 class="modal-title" id="exampleModalLabel">Edit Confirmation Production Order Detail</label></h4>
										</div>
										
	      								<div class="modal-body">
	          								<div id="dvOrderTypeID" class="form-group">
	            								<label for="recipient-name" class="control-label">Order Type ID</label>
	            								<input type="text" class="form-control" id="txtOrderTypeID" name="txtOrderTypeID">
	          								</div>
	          								<div id="dvOrderNo" class="form-group">
	            								<label for="message-text" class="control-label">Order No</label>
	            								<input type="text" class="form-control" id="txtOrderNo" name="txtOrderNo">
	          								</div>
	          								<div id="dvComponentLine2" class="form-group">
	            								<label for="message-text" class="control-label">Component Line</label>
	            								<input type="text" class="form-control" id="txtComponentLine2" name="txtComponentLine2">
	          								</div>
	          								<div id="dvComponentID2" class="form-group">
	            								<label for="message-text" class="control-label">Component ID</label>	
	            								<input type="text" class="form-control" id="txtComponentID2" name="txtComponentID2">
	          								</div>
	          								<div id="dvQty2" class="form-group">
	            								<label for="message-text" class="control-label">Quantity</label><label id="mrkQty2" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtQty2" name="txtQty2">
	          								</div>
	          								<div id="dvUOM2" class="form-group">
	            								<label for="message-text" class="control-label">UOM</label>	
	            								<select id="slUOM" name="slUOM" class="form-control">
							                    </select>
	          								</div>
	          								
	          								<div class="row"></div>	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncUpdateConfirmationDetail()">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      								</div>
    									
    								</div>
  									</div>
									</div>
									<!-- /. end of modal show form to update production order detail for confirmation -->
										
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_confirmation_production" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Confirmation ID</th>
										<th>Confirmation No</th>
										<th>Date</th>
										<th>Order Type</th>
										<th>Order No</th>
										<th>Product</th>
										<th>Product Name</th>
										<th>Batch</th>
										<th>Qty</th>
										<th>UOM</th>
										<th>Qty Base</th>
										<th>Base UOM</th>
										<th><label style="width:60px"></label></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listConfirmationProduction}" var="confirmation">
										<tr>
											<td><c:out value="${confirmation.confirmationID}" /></td>
											<td><c:out value="${confirmation.confirmationNo}" /></td>
											<td><c:out value="${confirmation.confirmationDate}" /></td>
											<td><c:out value="${confirmation.orderTypeID}" /></td>
											<td><c:out value="${confirmation.orderNo}" /></td>
											<td><c:out value="${confirmation.productID}" /></td>
											<td><c:out value="${confirmation.productName}" /></td>
											<td><c:out value="${confirmation.batchNo}" /></td>
											<td><c:out value="${confirmation.qty}" /></td>
											<td><c:out value="${confirmation.UOM}" /></td>
											<td><c:out value="${confirmation.qtyBaseUOM}" /></td>
											<td><c:out value="${confirmation.baseUOM}" /></td>
											<td>
												<button type="button" id="btnShowConfirmationDetail" name="btnShowConfirmationDetail" 
												class="btn btn-default" title="details" data-toggle="modal" data-target="#ModalConfirmationProductionDetail"
												data-lconfirmationid='<c:out value="${confirmation.confirmationID}" />'
												data-lconfirmationno='<c:out value="${confirmation.confirmationNo}" />'
												>
												<i class="fa fa-list-ul"></i>
												</button>
<!-- 												<button id="btnAdditionalComponent" name="btnAdditionalComponent" type="button" class="btn btn-info" data-toggle="modal"  -->
<!-- 														title="add additional componet" -->
<!-- 														data-target="#ModalAdditionalComponent"  -->
<%-- 														data-lconfirmationid='<c:out value="${confirmation.confirmationID}" />' --%>
<%-- 														data-lconfirmationno='<c:out value="${confirmation.confirmationNo}" />' --%>
<%-- 														data-lconfirmationdate='<c:out value="${confirmation.confirmationDate}" />' --%>
<%-- 														data-lordertype='<c:out value="${confirmation.orderTypeID}" />' --%>
<%-- 														data-lorderno='<c:out value="${confirmation.orderNo}" />' --%>
<!-- 														> -->
<!-- 														<i class="fa fa-plus"></i> -->
<!-- 												</button>  -->
											<!-- onclick="FuncButtonAdditionalComponent()" -->
											</td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>
	
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_master_confirmation_document").DataTable();
  		$("#tb_master_component").DataTable();
  		$("#tb_production_order").DataTable();
  		$("#tb_confirmation_production").DataTable(
  				{
  					 "scrollX": true
  				});
  		$('#M007').addClass('active');
  		$('#M037').addClass('active');
  		
  		$('#txtDate').datepicker({
  	      format: 'dd M yyyy',
  	      autoclose: true
  	    });
  		$('#txtDate').datepicker('setDate', new Date());
  		
  		$('#txtExpiredDate').datepicker({
    	      format: 'dd M yyyy',
    	      autoclose: true
    	    });
  		
  		$("#divfailedcomponent").hide();
  		$("#dvErrorAlert2").hide();
  	});
 	
 	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
	</script>
    
<script>
$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  
  $("#ReceivedProduct").hide();
  $("#btnSave").hide();
  $("#btnDismiss").hide();
  $("#btnNext").prop('disabled', false);
  $("#btnEditHeader").prop('disabled', true);
  $("#ConfirmationHeader :input").prop("disabled", false);
  
//   $("#txtConfirmationNo").val('1');
  
  $("#txtConfirmationID").focus();
  $('#txtConfirmationID').click();
})
</script>

<script>
function FuncPassConfirmationID(lParamConfirmationID,lParamConfirmationName){
	if(lParamConfirmationID)
		{
			$("#txtConfirmationID").val(lParamConfirmationID);
			document.getElementById("lblConfirmationDocName").innerHTML = '(' + lParamConfirmationName + ')';
		}
}

function FuncPassProductionOrder(lParamOrderTypeID,lParamOrderNo,lParamPlantID,lParamWarehouseID){
	if(lParamOrderTypeID)
		{
			$("#temp_txtOrderTypeID").val(lParamOrderTypeID);
			$("#temp_txtOrderNo").val(lParamOrderNo);
			$("#temp_txtPlantID").val(lParamPlantID);
			$("#temp_txtWarehouseID").val(lParamWarehouseID);
			$("#txtProductionOrder").val(lParamOrderTypeID + ' - ' + lParamOrderNo);
		}
}

function FuncPassStringComponent(lParamComponentID,lParamComponentName){
	if(lParamComponentID)
		{
			$("#txtComponentID").val(lParamComponentID);
			document.getElementById("lblComponentName").innerHTML = '(' + lParamComponentName + ')';
			
			FuncShowUOM();
		}
}

//for batch
function FuncPassString(lParam1,lParam2,lParamBatchNo){
	if(lParamBatchNo)
		{
			$("#txtBatchNo").val(lParamBatchNo);
		}
}
</script>

<script>
function FuncClear(){
	$('#mrkConfirmationID').hide();
	$('#mrkConfirmationNo').hide();
	$('#mrkDate').hide();
	$('#mrkProductionOrder').hide();
	$('#mrkPackingNo').hide();
	$('#mrkExpiredDate').hide();
	$('#mrkQty').hide();
	$('#mrkUOM').hide();
	$('#mrkQty2').hide();
	
	//marker for additional component modal
	$('#mrkComponentID').hide();
// 	$('#mrkBatchNo').hide();
	$('#mrkQtyComponent').hide();
	$('#mrkUOMComponent').hide();
	
	//div for additional component modal
	$('#dvComponentID').removeClass('has-error');
// 	$('#dvBatchNo').removeClass('has-error');
	$('#dvQtyComponent').removeClass('has-error');
	$('#dvUOMComponent').removeClass('has-error');
	
	$('#dvConfirmationID').removeClass('has-error');
	$('#dvConfirmationNo').removeClass('has-error');
	$('#dvDate').removeClass('has-error');
	$('#dvProductionOrder').removeClass('has-error');
	$('#dvPackingNo').removeClass('has-error');
	$('#dvExpiredDate').removeClass('has-error');
	$('#dvQty').removeClass('has-error');
	$('#dvUOM').removeClass('has-error');
	
	$("#divsuccess").hide(); //confirmation detail notif header
	$("#divfailed").hide(); //confirmation detail notif header
	$("#divsuccess2").hide(); //confirm batch notif header
	$("#divfailed2").hide(); //confirm batch notif header
	$("#divfailedcomponent").hide();
	$("#dvErrorAlert2").hide();
	
	$('#dvQty2').removeClass('has-error');
}

function FuncButtonNew() {
	$('#txtConfirmationID').val('');
	$('#txtConfirmationNo').val('');
// 	$('#txtDate').val('');
	$('#txtProductionOrder').val('');
	$("#temp_txtOrderTypeID").val('');
	$("#temp_txtOrderNo").val('');
	$("#temp_txtPlantID").val('');
	$("#temp_txtWarehouseID").val('');
	$('#txtProductID').val('');
	$('#txtQty').val('');
// 	$('#slMasterUOM').val('');
	$('#txtUOM').val('');
	$("#temp_txtTargetQty").val('');
	$("#temp_txtTargetUOM").val('');
	
	document.getElementById("lblConfirmationDocName").innerHTML = null;
	document.getElementById("lblProductName").innerHTML = null;
	document.getElementById("lblTargetProduction").innerHTML = null;
	document.getElementById("lblTitleModal").innerHTML = "Add Production Confirmation";

	FuncClear();
}

function FuncValConfirmationNo(){	
	var txtConfirmationID = document.getElementById('txtConfirmationID').value;
	
	FuncClear();
	
	if(!txtConfirmationID.match(/\S/)) {    	
    	$('#txtConfirmationID').focus();
    	$('#dvConfirmationID').addClass('has-error');
    	$('#mrkConfirmationID').show();
    	
    	alert("Fill Confirmation ID First ...!!!");
        return false;
    } 
	
    return true;
	
}

function FuncValComponent(){	
	var txtComponentID = document.getElementById('txtComponentID').value;
	
	FuncClear();
	
	if(!txtComponentID.match(/\S/)) {    	
    	$('#txtComponentID').focus();
    	$('#dvComponentID').addClass('has-error');
    	$('#mrkComponentID').show();
    	
    	alert("Fill Component ID First ...!!!");
        return false;
    } 
	
    return true;
	
}

function FuncValNext() {
	var txtConfirmationID = document.getElementById('txtConfirmationID').value;
	var txtConfirmationNo = document.getElementById('txtConfirmationNo').value;
	var txtDate = document.getElementById('txtDate').value;
	var txtProductionOrder = document.getElementById('txtProductionOrder').value;
	var txtTempOrderTypeID = document.getElementById('temp_txtOrderTypeID').value;
	var txtTempOrderNo = document.getElementById('temp_txtOrderNo').value;
	var txtTempPlantID = document.getElementById('temp_txtPlantID').value;
	var txtTempWarehouseID = document.getElementById('temp_txtWarehouseID').value;
	
	var dvConfirmationID = document.getElementsByClassName('dvConfirmationID');
	var dvConfirmationNo = document.getElementsByClassName('dvConfirmationNo');
	var dvDate = document.getElementsByClassName('dvDate');
	var dvProductionOrder = document.getElementsByClassName('dvProductionOrder');
	
	FuncClear();

    if(!txtConfirmationID.match(/\S/)) {
    	$("#txtConfirmationID").focus();
    	$('#dvConfirmationID').addClass('has-error');
    	$('#mrkConfirmationID').show();
        return false;
    } 
    
    if(!txtConfirmationNo.match(/\S/)) {
    	$("#txtConfirmationNo").focus();
    	$('#dvConfirmationNo').addClass('has-error');
    	$('#mrkConfirmationNo').show();
        return false;
    } 
    
    if(!txtDate.match(/\S/)) {
    	$("#txtDate").focus();
    	$('#dvDate').addClass('has-error');
    	$('#mrktxtDate').show();
        return false;
    }
    
    if(!(txtProductionOrder.match(/\S/) && txtTempOrderTypeID.match(/\S/) && txtTempOrderNo.match(/\S/) && txtTempPlantID.match(/\S/) && txtTempWarehouseID.match(/\S/))) {
    	$("#txtProductionOrder").focus();
    	$('#dvProductionOrder').addClass('has-error');
    	$('#mrkProductionOrder').show();
        return false;
    } 
    
    //get Production Order Header data
    $.ajax({
        url: '${pageContext.request.contextPath}/getproductionorderheader',
        type: 'POST',
        data: {"orderType":txtTempOrderTypeID,"orderNo":txtTempOrderNo},
        dataType: 'json'
   })
   .done(function(data){
        console.log(data);
        
       //  $('#modal-loader').hide(); // hide loader 

       //for json data type
       for (var i in data) {
	  var obj = data[i];
	  var index = 0;
	  var productid, productname, qty, uom;
	  for (var prop in obj) {
	      switch (index++) {
	          case 0:
	              productid = obj[prop];
	              break;
	          case 1:
	              productname = obj[prop];
	              break;
	          case 2:
	              qty = obj[prop];
	              break;
	          case 3:
	              uom = obj[prop];
	              break;
	          default:
	              break;
     	 }
  	}
	  
	  $('#txtProductID').val(productid);
	  document.getElementById("lblProductName").innerHTML = '(' + productname + ')';
	  document.getElementById("lblTargetProduction").innerHTML = '(Target : ' + qty + ' ' + uom + ')';
	  $('#temp_txtTargetQty').val(qty);
	  $('#temp_txtTargetUOM').val(uom);
	  $('#txtUOM').val(uom);
		  
		}
       
       
       
       $('#btnNext').prop('disabled', true);
       $('#btnEditHeader').prop('disabled', false);
       $('#ReceivedProduct').show();
       $("#ConfirmationHeader :input").prop("disabled", true);
       $('#btnSave').show();
       $('#btnDismiss').show();
       $('#txtPackingNo').val('');
       $('#txtExpiredDate').val('');
       $('#txtQty').val('');
//        $('#slMasterUOM').find('option').remove();
       
		   })
		   .fail(function(){
		  	 console.log('Service call failed!');
		//               $('#modal-loader').hide();
		   });
    
    return true;
    
}

function FuncEditConfirmationHeader() {
	$('#btnNext').prop('disabled', false);
	$('#btnEditHeader').prop('disabled', true);
	$("#ConfirmationHeader :input").prop("disabled", false);
	$('#ReceivedProduct').hide();
	$('#btnSave').hide();
    $('#btnDismiss').hide();
}

function FuncValEmptyInput(lParambtn) {
	var txtTempOrderTypeID = document.getElementById('temp_txtOrderTypeID').value;
	var txtTempOrderNo = document.getElementById('temp_txtOrderNo').value;
	var txtTempPlantID = document.getElementById('temp_txtPlantID').value;
	var txtTempWarehouseID = document.getElementById('temp_txtWarehouseID').value;
	
	var txtTempTargetQty = document.getElementById('temp_txtTargetQty').value;
	var txtTempTargetUOM = document.getElementById('temp_txtTargetUOM').value;
	
	var txtConfirmationID = document.getElementById('txtConfirmationID').value;
	var txtConfirmationNo = document.getElementById('txtConfirmationNo').value;
	var txtConfirmationDate = document.getElementById('txtDate').value;
	
	var txtProductID = document.getElementById('txtProductID').value;
	var txtPackingNo = document.getElementById('txtPackingNo').value;
	var txtExpiredDate = document.getElementById('txtExpiredDate').value;
	var txtQty = document.getElementById('txtQty').value;
// 	var txtUOM = document.getElementById('slMasterUOM').value;
	var txtUOM = document.getElementById('txtUOM').value;
	
	var dvPackingNo = document.getElementsByClassName('dvPackingNo');
	var dvExpiredDate = document.getElementsByClassName('dvExpiredDate');
	var dvQty = document.getElementsByClassName('dvQty');
	var dvUOM = document.getElementsByClassName('dvUOM');
	
	if(!txtPackingNo.match(/\S/)) {
    	$('#txtPackingNo').focus();
    	$('#dvPackingNo').addClass('has-error');
    	$('#mrkPackingNo').show();
    	document.getElementById('mrkPackingNo').innerHTML = '*';
        return false;
    }
    
    if(!txtExpiredDate.match(/\S/)) {
    	$('#txtExpiredDate').focus();
    	$('#dvExpiredDate').addClass('has-error');
    	$('#mrkExpiredDate').show();
        return false;
    } 
    
	if(!txtQty.match(/\S/)) {
    	$('#txtQty').focus();
    	$('#dvQty').addClass('has-error');
    	$('#mrkQty').show();
    	document.getElementById('mrkQty').innerHTML = '*';
        return false;
    }
    
    if(!txtUOM.match(/\S/)) {
    	$('#txtUOM').focus();
    	$('#dvUOM').addClass('has-error');
    	$('#mrkUOM').show();
        return false;
    } 
	
	//-- get validation of input quantity which is it cannot be bigger than target production quantity
// 	var validate = '';
	$.ajax({
        url: '${pageContext.request.contextPath}/getValidateInputQuantityConfirmation',
        type: 'POST',
        data: {"inputqty":txtQty , "inputuom":txtUOM, "targetqty":txtTempTargetQty, "targetuom":txtTempTargetUOM, "product":txtProductID},
        dataType: 'text'
   })
   .done(function(data){
        console.log(data);
        
        //for text data type
        $('#temp_validate').val(data);
        
        //process
        var validate = document.getElementById('temp_validate').value;
        
        if(validate.match('disallow')) {
			$('#txtQty').focus();
	    	$('#dvQty').addClass('has-error');
	    	$('#mrkQty').show();
	    	document.getElementById('mrkQty').innerHTML = ' &nbspmay not bigger than target ';
	    	
	    	$('#txtUOM').focus();
	    	$('#dvUOM').addClass('has-error');
	    	$('#mrkUOM').show();
	    	
	        return false;
	    }
	    
		//get confirmation production detail
	    document.getElementById("confirmationdocno").innerHTML = "Confirmation Id : " + txtConfirmationID + "&nbsp&nbsp&nbsp&nbsp" + "Confirmation No : " + txtConfirmationNo +
	    						"&nbsp&nbsp&nbsp&nbsp" + "Date : " + txtConfirmationDate + "&nbsp&nbsp&nbsp&nbsp" + "Production Order : " + txtTempOrderTypeID + " - " + txtTempOrderNo
	    						+ "<br>" + "Product ID : " + txtProductID + "&nbsp&nbsp&nbsp&nbsp" + "Packing No : " + txtPackingNo + "&nbsp&nbsp&nbsp&nbsp" + "Expired Date : " + txtExpiredDate + "&nbsp&nbsp&nbsp&nbsp" 
	    						+ "Qty : " + txtQty + "&nbsp&nbsp&nbsp&nbsp" + "UOM : " + txtUOM;
	    
	    $('#dynamic-content-confirmation-detail').html(''); // leave this div blank
//	     $('#modal-loader').show();      // load ajax loader on button click
		
// 		inner ajax
	    $.ajax({
	         url: '${pageContext.request.contextPath}/getConfirmationProductionDetail',
	         type: 'POST',
	         data: {"confirmationID":txtConfirmationID,"confirmationNo":txtConfirmationNo,"confirmationDate":txtConfirmationDate,
	        	 "orderType":txtTempOrderTypeID,"orderNo":txtTempOrderNo,"qty":txtQty,"uom":txtUOM,"product":txtProductID,
	        	 "plantID":txtTempPlantID, "warehouseID":txtTempWarehouseID},
	         dataType: 'html'
	    })
	    .done(function(data){
	         console.log(data); 
	         $('#dynamic-content-confirmation-detail').html(''); // blank before load.
	         $('#dynamic-content-confirmation-detail').html(data); // load here
//	          $('#modal-loader').hide(); // hide loader  
	    })
	    .fail(function(){
	         $('#dynamic-content-confirmation-detail').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//	          $('#modal-loader').hide();
	    });
	    //end of inner ajax
	    
	    $("#btnFinish").show();
	    $('#ModalConfirmationProductionDetail').modal('show');
	    
	    return true;
   })
   .fail(function(){
  	 console.log('Service call failed!');
   });
	//end of get validate --
	
	FuncClear();
	    
}
</script>

<!-- get confirmation no from confirmation id -->
<script>
$(document).ready(function(){
	
    $(document).on('click', '#txtConfirmationNo', function(e){
    	
    		e.preventDefault();
    		
    		var paramConfirmationID = document.getElementById('txtConfirmationID').value;
     
			         $.ajax({
			              url: '${pageContext.request.contextPath}/getConfirmationNo',
			              type: 'POST',
			              data: {confirmationid : paramConfirmationID},
			              dataType: 'text'
			         })
			         .done(function(data){
			              console.log(data);
			              
			              //for text data type
			              $('#txtConfirmationNo').val(''); // blank before load.
			              
			              if(data=='0')
			            	  {
				              	$('#dvConfirmationNo').addClass('has-error');
				              	document.getElementById('mrkConfirmationNo').innerHTML = ' &nbsp*Confirmation No has reached the maximum range';
				              	$('#mrkConfirmationNo').show();
			            	  }
			              else
							  {
			            	  	$('#txtConfirmationNo').val(data); //load
			            	  	FuncClear();
							  }
			         })
			         .fail(function(){
			        	 console.log('Service call failed!');
			         });
    });
});
</script>

<!-- get uom from product id -->
<script>
$(document).ready(function(){
	
    $(document).on('click', '#slMasterUOM', function(e){
    	
    	if($('#slMasterUOM').find('option').length > 0)
    	{
    				
    	}
    	else
    	{
    		e.preventDefault();
    		  
//          var plantid = $(this).data('id'); // get id of clicked row
    		var paramproductid = document.getElementById('txtProductID').value;
      
//          $('#dynamic-content-uom').html(''); // leave this div blank

//          $('#modal-loader').show();      // load ajax loader on button click
     
			         $.ajax({
			              url: '${pageContext.request.contextPath}/getuom',
			              type: 'POST',
			              data: {productid : paramproductid},
			              dataType: 'json'
			         })
			         .done(function(data){
			              console.log(data);
			              
// 			              $('#slMasterUOM').find('option').remove();
			              
			              //for html data type
			//               $('#dynamic-content-uom').html(''); // blank before load.
			//               $('#dynamic-content-uom').html(data); //load 
			             //  $('#modal-loader').hide(); // hide loader 
			
			             //for json data type
			             for (var i in data) {
			        var obj = data[i];
			        var index = 0;
			        var key, val;
			        for (var prop in obj) {
			            switch (index++) {
			                case 0:
			                    key = obj[prop];
			                    break;
			                case 1:
			                    val = obj[prop];
			                    break;
			                default:
			                    break;
			            }
			        }
			        
			        $('#slMasterUOM').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
			        
			    }
			             
			         })
			         .fail(function(){
			        	 console.log('Service call failed!');
			//               $('#dynamic-content-uom').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			//               $('#modal-loader').hide();
			         });
    	}
    });
});
</script>

<!-- get confirmation detail -->
<script>
$(document).ready(function(){

    $(document).on('click', '#btnShowConfirmationDetail', function(e){
    	
     e.preventDefault();
     $("#btnFinish").hide();
     $("#btnaddComponent").hide();
  
     var confirmid = $(this).data('lconfirmationid'); // get confirmation id of clicked row
     var confirmno = $(this).data('lconfirmationno'); // get confirmation no of clicked row
     
     document.getElementById("confirmationdocno").innerHTML = "Confirmation Id : " + confirmid + "<br>" + "Confirmation No : " + confirmno;
  
     $('#dynamic-content-confirmation-detail').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getConfirmationDetail',
          type: 'POST',
          data: {"confirmid":confirmid , "confirmno":confirmno},
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content-confirmation-detail').html(''); // blank before load.
          $('#dynamic-content-confirmation-detail').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content-confirmation-detail').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

     FuncClear();
    });
});
</script>

<!-- modal add additional component for confirmation -->
<script>
$('#ModalAdditionalComponent').on('shown.bs.modal', function (event) {
	$("#divfailedcomponent").hide();
	
  var button = $(event.relatedTarget) // Button that triggered the modal
  
//   var lConfirmationID = button.data('lconfirmationid');
//   var lConfirmationNo = button.data('lconfirmationno');
//   var lConfirmationDate = button.data('lconfirmationdate');
//   var lOrderTypeID = button.data('lordertype');
//   var lOrderNo = button.data('lorderno');
	var lOrderTypeID = document.getElementById('temp_txtOrderTypeID').value;
	var lOrderNo = document.getElementById('temp_txtOrderNo').value;
  
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  
//   $("#txtConfirmationID").val(lConfirmationID); //temporary data
//   $("#txtConfirmationNo").val(lConfirmationNo); //temporary data
//   $("#txtDate").val(lConfirmationDate); //temporary data
  $("#temp_txtOrderTypeID").val(lOrderTypeID); //temporary data
  $("#temp_txtOrderNo").val(lOrderNo); //temporary data
  
  $("#txtComponentID").val('');
//   $("#txtBatchNo").val('');
  $("#txtQtyComponent").val('');
  $("#slMasterUOMComponent").val('');
  document.getElementById("lblComponentName").innerHTML = null;
  
  $("#txtComponentID").focus();
  $('#txtComponentID').click();
  
				//get latest component line from temporary confirmation detail id -->
  			         $.ajax({
  			              url: '${pageContext.request.contextPath}/getComponentLine',
  			              type: 'POST',
  			              data: {"ordertypeid" : lOrderTypeID , "orderno" : lOrderNo},
  			              dataType: 'text'
  			         })
  			         .done(function(data){
  			              console.log(data);
  			              
  			              //for text data type
  			              $('#txtComponentLine').val(''); // blank before load.
  			              
  			            	  	$('#txtComponentLine').val(data); //load
//   			            	  	FuncClear();
  			         })
  			         .fail(function(){
  			        	 console.log('Service call failed!');
  			         });
  			         
  			         //get the plant and warehouse id from confirmation order
  			         $.ajax({
        url: '${pageContext.request.contextPath}/getPlantWarehouseConfirmation',
        type: 'POST',
        data: {"orderType":lOrderTypeID,"orderNo":lOrderNo},
        dataType: 'json'
   })
   .done(function(data){
        console.log(data);
        
       //  $('#modal-loader').hide(); // hide loader 

       //for json data type
       for (var i in data) {
	  var obj = data[i];
	  var index = 0;
	  var plant, warehouse;
	  for (var prop in obj) {
	      switch (index++) {
	          case 0:
	              plant = obj[prop];
	              break;
	          case 1:
	              warehouse = obj[prop];
	              break;
	          default:
	              break;
     	 }
  	}
  
	  $('#temp_txtPlantID').val(plant);
	  $('#temp_txtWarehouseID').val(warehouse);
		  
		}
		   })
		   .fail(function(){
		  	 console.log('Service call failed!');
		//               $('#modal-loader').hide();
		   });
  			         
FuncClear();
  
})
</script>

<!-- get batch from component id script -->
<script>
$(document).ready(function(){

    $(document).on('click', '#txtBatchNo', function(e){
  
     e.preventDefault();
  
//      var plantid = $(this).data('id'); // get id of clicked row
		var componentid = document.getElementById('txtComponentID').value;
  
     $('#dynamic-content-batch').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getbatch',
          type: 'POST',
          data: 'productid='+componentid,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content-batch').html(''); // blank before load.
          $('#dynamic-content-batch').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content-batch').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<!-- get uom component from component id -->
 	<script>
 	function FuncShowUOM(){	
 		$('#slMasterUOMComponent').find('option').remove();
 		
		var paramcomponentid = document.getElementById('txtComponentID').value;
 
		         $.ajax({
		              url: '${pageContext.request.contextPath}/getuom',
		              type: 'POST',
		              data: {productid : paramcomponentid},
		              dataType: 'json'
		         })
		         .done(function(data){
		              console.log(data);
		
		             //for json data type
		             for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slMasterUOMComponent').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
		        
		    }
		             
		         })
		         .fail(function(){
		        	 console.log('Service call failed!');
		         });
 	}
 </script>

<script>
function FuncValEmptyInputComponent(lParambtn) {
// 	var txtConfirmationID = document.getElementById('txtConfirmationID').value;
// 	var txtConfirmationNo = document.getElementById('txtConfirmationNo').value;
// 	var txtConfirmationDate = document.getElementById('txtDate').value;
	var txtPlantID = document.getElementById('temp_txtPlantID').value;
	var txtWarehouseID = document.getElementById('temp_txtWarehouseID').value;
	var txtOrderTypeID = document.getElementById('temp_txtOrderTypeID').value;
	var txtOrderNo = document.getElementById('temp_txtOrderNo').value;
	var txtComponentLine = document.getElementById('txtComponentLine').value;
	var txtComponentID = document.getElementById('txtComponentID').value;
// 	var txtBatchNo = document.getElementById('txtBatchNo').value;
	var txtQtyComponent = document.getElementById('txtQtyComponent').value;
	var txtUOMComponent = document.getElementById('slMasterUOMComponent').value;
	
	var dvComponentID = document.getElementsByClassName('dvComponentID');
// 	var dvBatchNo = document.getElementsByClassName('dvBatchNo');
	var dvQtyComponent = document.getElementsByClassName('dvQtyComponent');
	var dvUOMComponent = document.getElementsByClassName('dvUOMComponent');
	
// 	if(!txtConfirmationID.match(/\S/)) {
//     	$('#txtComponentID').focus();
//     	$('#dvComponentID').addClass('has-error');
//     	$('#mrkComponentID').show();
//         return false;
//     }
    
//     if(!txtConfirmationNo.match(/\S/)) {
//     	$('#txtComponentID').focus();
//     	$('#dvComponentID').addClass('has-error');
//     	$('#mrkComponentID').show();
//         return false;
//     }
    
//     if(!txtConfirmationDate.match(/\S/)) {
//     	$('#txtComponentID').focus();
//     	$('#dvComponentID').addClass('has-error');
//     	$('#mrkComponentID').show();
//         return false;
//     }
    
//     if(!txtPlantID.match(/\S/)) {
//     	$('#txtComponentID').focus();
//     	$('#dvComponentID').addClass('has-error');
//     	$('#mrkComponentID').show();
//         return false;
//     }
    
//     if(!txtWarehouseID.match(/\S/)) {
//     	$('#txtComponentID').focus();
//     	$('#dvComponentID').addClass('has-error');
//     	$('#mrkComponentID').show();
//         return false;
//     }
    
    if(!txtComponentID.match(/\S/)) {
    	$('#txtComponentID').focus();
    	$('#dvComponentID').addClass('has-error');
    	$('#mrkComponentID').show();
        return false;
    }
    
//     if(!txtBatchNo.match(/\S/)) {
//     	$('#txtBatchNo').focus();
//     	$('#dvBatchNo').addClass('has-error');
//     	$('#mrkBatchNo').show();
//         return false;
//     } 
    
    if(!txtQtyComponent.match(/\S/)) {
    	$('#txtQtyComponent').focus();
    	$('#dvQtyComponent').addClass('has-error');
    	$('#mrkQtyComponent').show();
        return false;
    }
    
    if(!txtUOMComponent.match(/\S/)) {
    	$('#slMasterUOMComponent').focus();
    	$('#dvUOMComponent').addClass('has-error');
    	$('#mrkUOMComponent').show();
        return false;
    } 
   
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/ConfirmationProductionOrder',	
        type:'POST',
        data:{"key":lParambtn,"txtOrderTypeID":txtOrderTypeID,"txtOrderNo":txtOrderNo,"txtComponentLine":txtComponentLine,"txtComponentID":txtComponentID,"txtQtyComponent":txtQtyComponent,"txtUOMComponent":txtUOMComponent},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	
	        console.log(data);
			// inner ajax to refresh the confirmation detail
		    $.ajax({
		         url: '${pageContext.request.contextPath}/getConfirmationProductionDetail',
		         type: 'GET',
		         data: {"orderType":txtOrderTypeID,"orderNo":txtOrderNo,"plantID":txtPlantID, "warehouseID":txtWarehouseID},
		         dataType: 'html'
		    })
		    .done(function(data){
		         console.log(data);
		         $('#dynamic-content-confirmation-detail').html(''); // blank before load.
		         $('#dynamic-content-confirmation-detail').html(data); // load here
			//	          $('#modal-loader').hide(); // hide loader  
		    })
		    .fail(function(){
		         $('#dynamic-content-confirmation-detail').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			//	          $('#modal-loader').hide();
		    });
		    //end of inner ajax to refresh confirmation detail
	    
		    
			if(data.split("--")[0] == 'SuccessInsertAdditionalComponent')
	      		{
					$('#ModalAdditionalComponent').modal('toggle');
	      			$("#divsuccess").show();
	      			$("#divfailedcomponent").hide();
	      			document.getElementById("successlabel").innerHTML = "Berhasil menambahkan komponen tambahan.";
	      		}
	      	else
	      		{
	      			$("#divfailedcomponent").show();
	      			$("#divsuccess").hide();
	      			document.getElementById("failedlabelcomponent").innerHTML = "Gagal menambahkan komponen tambahan";
	      			document.getElementById("lblAlertDescriptionComponent").innerHTML = data.split("--")[1];
	      			$("#txtComponentID").focus();
	      			$("#ModalAdditionalComponent").animate({scrollTop:0}, 'slow');
	        		return false;
	      		}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<!-- get production order complete validation -->
<script>
$(document).ready(function(){

    $(document).on('click', '#btnFinish', function(e){
    	
     e.preventDefault();
 	
     var keyButtonConfirm = "Confirm Production Order"
	//var keyButton = "Complete Production";
     var confirmationid = document.getElementById('txtConfirmationID').value;
     var confirmationno = document.getElementById('txtConfirmationNo').value;
     var confirmationdate = document.getElementById('txtDate').value;
     var ordertypeid = document.getElementById('temp_txtOrderTypeID').value;
     var orderno = document.getElementById('temp_txtOrderNo').value;
     var bomproductid = document.getElementById('txtProductID').value;
     var packingno = document.getElementById('txtPackingNo').value;
     var expireddate = document.getElementById('txtExpiredDate').value;
     var bomproductqtyTarget = document.getElementById('temp_txtTargetQty').value;
     var bomproductqty = document.getElementById('txtQty').value;
     var bomproductuom = document.getElementById('txtUOM').value;
     var plantid = document.getElementById('temp_txtPlantID').value;
 	 var warehouseid = document.getElementById('temp_txtWarehouseID').value;
     
   //ajax validate confirmation detail
     $.ajax({
          url: '${pageContext.request.contextPath}/getConfirmatonDetailValidation',
          type: 'POST',
          data: {"ordertypeid":ordertypeid , "orderno":orderno},
          dataType: 'text'
     })
     .done(function(data){
          console.log(data); 
          
          if(data == "Valid") {
        	  //ajax confirm the production order
        	  $.ajax({
	              url: '${pageContext.request.contextPath}/ConfirmationProductionOrder',
	              type: 'POST',
	              data: {"key" : keyButtonConfirm, "confirmationid":confirmationid, "confirmationno":confirmationno,
	            	  "confirmationdate":confirmationdate,"ordertypeid":ordertypeid, "orderno":orderno, "bomproductid":bomproductid,
	            	  "packingno":packingno,"expireddate":expireddate,"bomproductqtyTarget":bomproductqtyTarget,
	            	  "bomproductqty":bomproductqty,"bomproductuom":bomproductuom,"plantid":plantid,"warehouseid":warehouseid},
	              dataType: 'text',
	              success:function(data, textStatus, jqXHR){
	            	  	console.log(data);
	            	  	
	            	  	if(~data.indexOf("SuccessInsertConfirmation")){
	            	  		var url = '${pageContext.request.contextPath}/ConfirmationProductionOrder';  
		                	  $(location).attr('href', url); 
	                	}
	            	  	else if(~data.indexOf("FailedInsertConfirmation"))
	        	  		{
	        	      		$("#divfailed").show();
	        	  			$("#divsuccess").hide();
	        	  			document.getElementById("failedlabel").innerHTML = data.split("FailedInsertConfirmation--").pop();
	        	  		}
	    	        },
	    	        error:function(data, textStatus, jqXHR){
	    	            console.log('Service call failed!');
	    	        }
	         })
	         //end of ajax confirm production order
  	    	}
          else if(~data.indexOf("Invalid"))
	  		{
	      		$("#divfailed").show();
	  			$("#divsuccess").hide();
	  			document.getElementById("failedlabel").innerHTML = "Anda belum memilih batch untuk komponen dengan line : '" + data.split("Invalid").pop() + "'.";
	  		}
          else
        	{
        	  	$("#divfailed").show();
	  			$("#divsuccess").hide();
	  			document.getElementById("failedlabel").innerHTML = "Database Error";  
        	}
     })
     .fail(function(){
    	 console.log('Service call failed!');
     });
   //end of ajax validate confirmation detail
 	
     //ajax to check production order target qty, if fulfill the target, the status become 'complete'
		//      $.ajax({
		//           url: '${pageContext.request.contextPath}/getProductionOrderCompleteValidation',
		//           type: 'POST',
		//           data: {"ordertypeid":ordertypeid , "orderno":orderno},
		//           dataType: 'text'
		//      })
		//      .done(function(data){
		//           console.log(data); 
		          
		//         //for text data type
		//           $('#temp_validate').val(data);
		          
		//           //process
		//           var validate = document.getElementById('temp_validate').value;
		          
		//           if(validate.match('yes')) {
		//         	//ajax to complete production order proccess
		//       		$.ajax({
		// 	              url: '${pageContext.request.contextPath}/ConfirmationProductionOrder',
		// 	              type: 'POST',
		// 	              data: {"key" : keyButton, "ordertypeid":ordertypeid, "orderno":orderno},
		// 	              dataType: 'text',
		// 	              success:function(data, textStatus, jqXHR){
		// 	            	  	console.log(data);
		// 	            	  	var url = '${pageContext.request.contextPath}/ConfirmationProductionOrder';  
		// 	                	  $(location).attr('href', url);
		// 	    	        },
		// 	    	        error:function(data, textStatus, jqXHR){
		// 	    	            console.log('Service call failed!');
		// 	    	        }
		// 	         })
		// 	         //end of ajax to complete production order proccess
		//   	    	}
		//           else
		//         	  {
		// 	        	  var url = '${pageContext.request.contextPath}/ConfirmationProductionOrder';  
		// 	          	  $(location).attr('href', url);
		//         	  }
		//      })
		//      .fail(function(){
		//     	 console.log('Service call failed!');
		//      });
     //end of ajax check production order target qty

    });
    
    FuncClear();
    
    return true;
});
</script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtConfirmationID:focus').length) {
    	$('#txtConfirmationID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtConfirmationNo:focus').length) {
    	$('#txtConfirmationNo').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtProductionOrder:focus').length) {
    	$('#txtProductionOrder').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtBatchNo:focus').length) {
    	$('#txtBatchNo').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#slMasterUOMComponent:focus').length) {
    	$('#slMasterUOMComponent').click();
    }
});
</script>

<script>
function FuncSetBatch() {
	FuncClear();
	
	var checkVal = new Array();
    
	$.each($("input[name='cbComponent']:checked").closest("td").siblings("td"),
		       function () {
					checkVal.push($(this).text());
		       });
	
//     $('input[name="cbComponent"]:checked').each(function() {
//     	   checkVal.push(this.value);
//     	});

	if(checkVal.length == 0)
		{
			$("#divfailed2").show();
			$("#divsuccess2").hide();
			document.getElementById("errorlabel").innerHTML = "Anda belum memilih batch.";
			
			return false;
		}
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/tempbatchsummary',	
        type:'POST',
        dataType : 'text',
       	data:{"listParam":checkVal},
        success:function(data, textStatus, jqXHR){
//         	var url = '${pageContext.request.contextPath}/getComponentBatchSummary';  
//         	$(location).attr('href', url);
			console.log(data);
	    	        	
        	if(data == 'SuccessInsertTemporaryBatch')
        		{
        			$("#divsuccess2").show();
        			$("#divfailed2").hide();
        		}
        	else if(data == 'FailedInsertTemporaryBatch')
        		{
        			$("#divfailed2").show();
        			$("#divsuccess2").hide();
        			document.getElementById("errorlabel").innerHTML = "Terjadi error di database saat menyimpan data.";
        		}
        	else if(data == 'ComponentNotAvailable')
	    		{
	    			$("#divfailed2").show();
	    			$("#divsuccess2").hide();
	    			document.getElementById("errorlabel").innerHTML = "Komponen yang dipilih belum cukup.";
	    		}
        	else if(~data.indexOf("ExpiredDateFormatConversionError"))
        		{
	        		$("#divfailed2").show();
	    			$("#divsuccess2").hide();
	    			document.getElementById("errorlabel").innerHTML = "Terjadi error saat konversi tanggal kadaluarsa '" + data.split("ExpiredDateFormatConversionError").pop() + "'.";
        		}
        	else
    		{
        		$("#divfailed2").show();
    			$("#divsuccess2").hide();
    			document.getElementById("errorlabel").innerHTML = "Komponen dengan batch '" + data.split("ExpiredComponentBatchFound").pop() + "' sudah kadaluarsa.";
    		}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    return true;
    
}
</script>

<script>
$('#ModalUpdateProductionOrderDetail').on('shown.bs.modal', function (event) {
	$("#dvErrorAlert2").hide();
	$('#txtOrderTypeID').prop('disabled', true);
	$('#txtOrderNo').prop('disabled', true);
	$('#txtComponentLine2').prop('disabled', true);
	$('#txtComponentID2').prop('disabled', true);
	
  var button = $(event.relatedTarget) // Button that triggered the modal
  
  var lordertypeid = button.data('ordertypeid') // Extract info from data-* attributes
  var lorderno = button.data('orderno') // Extract info from data-* attributes
  var lcomponentline = button.data('componentline') // Extract info from data-* attributes
  var lcomponentid = button.data('componentid') // Extract info from data-* attributes
  var lqty = button.data('qty') // Extract info from data-* attributes
  var luom = button.data('uom') // Extract info from data-* attributes
  
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  
  modal.find('.modal-body #txtOrderTypeID').val(lordertypeid)
  modal.find('.modal-body #txtOrderNo').val(lorderno)
  modal.find('.modal-body #txtComponentLine2').val(lcomponentline)
  modal.find('.modal-body #txtComponentID2').val(lcomponentid)
  modal.find('.modal-body #txtQty2').val(lqty)
  
  FuncShowUOM2(luom);
  
  //modal.find('.modal-body #slUOM').val(luom)
  
  $("#txtQty2").focus();
})

function FuncUpdateConfirmationDetail() {
	var txtOrderTypeID = document.getElementById('txtOrderTypeID').value;
	var txtOrderNo = document.getElementById('txtOrderNo').value;
	var txtComponentLine = document.getElementById('txtComponentLine2').value;
	var txtComponentID = document.getElementById('txtComponentID2').value;
	var txtQty = document.getElementById('txtQty2').value;
	var slUOM = document.getElementById('slUOM').value;
	var txtPlantID = document.getElementById('temp_txtPlantID').value;
	var txtWarehouseID = document.getElementById('temp_txtWarehouseID').value;
	
	var dvQty = document.getElementsByClassName('dvQty2');

    if(!txtQty.match(/\S/)) {
    	$("#txtQty2").focus();
    	$('#dvQty2').addClass('has-error');
    	$('#mrkQty2').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/ConfirmationProductionOrder',	
        type:'POST',
        data:{"key":"updateconfirmationdetail","txtOrderTypeID":txtOrderTypeID,"txtOrderNo":txtOrderNo,
        	"txtComponentLine":txtComponentLine,"txtComponentID":txtComponentID,"txtQty":txtQty,"slUOM":slUOM},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedUpdateConfirmationDetail')
        	{
        		$("#dvErrorAlert2").show();
        		document.getElementById("lblAlert2").innerHTML = "Gagal memperbaharui detil konfirmasi produksi";
        		document.getElementById("lblAlertDescription2").innerHTML = data.split("--")[1];
        		$("#txtQty2").focus();
        		$("#ModalUpdateProductionOrderDetail").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedConversion')
        	{
        		$("#dvErrorAlert2").show();
        		document.getElementById("lblAlert2").innerHTML = "Gagal";
        		document.getElementById("lblAlertDescription2").innerHTML = data.split("--")[1];
				//$("#txtQty").focus();
        		$("#ModalUpdateProductionOrderDetail").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
        		$('#dynamic-content-confirmation-detail').html('');
       		
				//inner ajax
	       	    $.ajax({
	       	         url: '${pageContext.request.contextPath}/getConfirmationProductionDetail',
	       	         type: 'POST',
	       	         data: {"key":"showupdateddetail",
	       	        	 "orderType":txtOrderTypeID,"orderNo":txtOrderNo,"qty":txtQty,"uom":slUOM,
	       	        	 "plantID":txtPlantID, "warehouseID":txtWarehouseID, "componentline":txtComponentLine,
	       	        	 "componentid":txtComponentID},
	       	         dataType: 'html'
	       	    })
	       	    .done(function(data){
	       	         console.log(data); 
	       	         $('#dynamic-content-confirmation-detail').html(''); // blank before load.
	       	         $('#dynamic-content-confirmation-detail').html(data); // load here
	       	         
	       	      	$('#ModalUpdateProductionOrderDetail').modal('toggle');
	      			$("#divsuccess").show();
	      			$("#divfailedcomponent").hide();
	      			document.getElementById("successlabel").innerHTML = "Berhasil memperbaharui komponen.";
					// $('#modal-loader').hide(); // hide loader  
	       	    })
	       	    .fail(function(){
	       	         $('#dynamic-content-confirmation-detail').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
					//$('#modal-loader').hide();
	       	    });
	       	    //end of inner ajax
	        }
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}

function FuncShowUOM2(lUOM){	
		$('#slUOM').find('option').remove();
		
	var paramcomponentid = document.getElementById('txtComponentID2').value;

	         $.ajax({
	              url: '${pageContext.request.contextPath}/getuom',
	              type: 'POST',
	              data: {productid : paramcomponentid},
	              dataType: 'json'
	         })
	         .done(function(data){
	              console.log(data);
	
	             //for json data type
	             for (var i in data) {
	        var obj = data[i];
	        var index = 0;
	        var key, val;
	        for (var prop in obj) {
	            switch (index++) {
	                case 0:
	                    key = obj[prop];
	                    break;
	                case 1:
	                    val = obj[prop];
	                    break;
	                default:
	                    break;
	            }
	        }
	        
	        $('#slUOM').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
	        $("#slUOM").val(lUOM);
	    }
	             
	         })
	         .fail(function(){
	        	 console.log('Service call failed!');
	         });
	}
</script>

</body>

</html>