<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Storage Type Indicator</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="StorageTypeIndicator" name="StorageTypeIndicator" action = "${pageContext.request.contextPath}/storagetypeindicator" method="post">
		<input type="hidden" name="temp_string" value="" />
	
		<div class="wrapper">	
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					Storage Type Indicator
				</h1>
				</section>
	
				<!-- Main content -->
				<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<c:if test="${condition == 'SuccessInsertStorageTypeIndicator'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menambahkan storage type indicator.
	           						</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessUpdateStorageTypeIndicator'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses memperbaharui storage type indicator.
	              					</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessDeleteStorageTypeIndicator'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menghapus storage type indicator.
	              					</div>
		      					</c:if>
		      					
		      					<c:if test="${condition == 'FailedDeleteStorageTypeIndicator'}">
		      						<div class="alert alert-danger alert-dismissible">
		                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
		                				Gagal menghapus storage type indicator. <c:out value="${conditionDescription}"/>.
	              					</div>
		     					</c:if>
								
								<!--modal update & Insert -->
								<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
	  										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				     						</div>
				     					
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
											</div>
		     								<div class="modal-body">
		         								<div id="dvPlantID" class="form-group">
		           								<label for="lblPlantID" class="control-label">Plant ID</label><label id="mrkPlantID" for="formrkPlantID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" 
		           								data-target="#ModalGetPlantID">
		         								</div>
		         								<div id="dvWarehouseID" class="form-group">
		           								<label for="lblWarehouseID" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="formrkWarehouseID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" 
		           								data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
		         								</div>
		         								<div id="dvStorageTypeIndicatorID" class="form-group">
		           								<label for="lblStorageTypeIndicatorID" class="control-label">Storage Type Indicator ID</label><label id="mrkStorageTypeIndicatorID" for="formrkStorageTypeIndicatorID" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageTypeIndicatorID" name="txtStorageTypeIndicatorID">
		         								</div>
		         								<div id="dvStorageTypeIndicatorName" class="form-group">
		           								<label for="lblStorageTypeIndicatorName" class="control-label">Storage Type Indicator Name</label><label id="mrkStorageTypeIndicatorName" for="formrkStorageTypeIndicatorName" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageTypeIndicatorName" name="txtStorageTypeIndicatorName">
		         								</div>	        								
		    								</div>
		    								<div class="modal-footer">
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
		      									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    								</div>
										</div>
									</div>
								</div>
								<!-- end of update insert modal -->
								
								<!--modal Delete -->
								<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
	 										<div class="modal-header">            											
	   										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	     										<span aria-hidden="true">&times;</span></button>
	   										<h4 class="modal-title">Alert Delete Storage Type Indicator</h4>
	 										</div>
		 									<div class="modal-body">
			 									<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"  />
			 									<input type="hidden" id="temp_txtWarehouseID" name="temp_txtWarehouseID"  />
			 									<input type="hidden" id="temp_txtStorageTypeIndicatorID" name="temp_txtStorageTypeIndicatorID"  />
			  	 									<p>Are you sure to delete this storage type indicator ?</p>
		 									</div>
		 									<div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
							              	</div>
							            </div>
							            <!-- /.modal-content -->
									</div>
							          <!-- /.modal-dialog -->
						        </div>
						        <!-- /.modal -->   
							        
						        <!--modal show plant data -->
								<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
									<div class="modal-dialog" role="document">
	 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelPlantID"></h4>	
	   										</div>
		     								<div class="modal-body">
		         								<table id="tb_master_plant" class="table table-bordered table-hover">
											        <thead style="background-color: #d2d6de;">
											                <tr>
											                  <th>ID</th>
											                  <th>Name</th>
											                  <th>Description</th>
											                  <th style="width: 20px"></th>
											                </tr>
											        </thead>
											        <tbody>
												        <c:forEach items="${listPlant}" var ="plant">
													        <tr>
														        <td><c:out value="${plant.plantID}"/></td>
														        <td><c:out value="${plant.plantNm}"/></td>
														        <td><c:out value="${plant.desc}"/></td>
														        <td><button type="button" class="btn btn-primary"
														        			data-toggle="modal"
														        			onclick="FuncPassStringPlant('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
														        			data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
														        </td>
											        		</tr>
												        </c:forEach>
											        </tbody>
									        	</table>
		    								</div>
	   										<div class="modal-footer"></div>
	 										</div>
									</div>
								</div>
								<!-- /. end of modal show plant data -->
								
								<!--modal show warehouse data -->
								<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelWarehouseID"></h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content-warehouse">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show plant data -->
								
								
								<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
								<table id="tb_storagetypeindicator" class="table table-bordered table-striped table-hover">
									<thead style="background-color: #d2d6de;">
										<tr>
											<th>Plant ID</th>
											<th>Warehouse ID</th>
											<th>Storage Type Indicator ID</th>
											<th>Storage Type Indicator Name</th>
											<th style="width:60px;"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listStorageTypeIndicator}" var="storagetypeindicator">
											<tr>
												<td><c:out value="${storagetypeindicator.plantID}" /></td>
												<td><c:out value="${storagetypeindicator.warehouseID}" /></td>
												<td><c:out value="${storagetypeindicator.storageTypeIndicatorID}" /></td>
												<td><c:out value="${storagetypeindicator.storageTypeIndicatorName}" /></td>
												<td><button <c:out value="${buttonstatus}"/>
															id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
															onclick="FuncButtonUpdate()"
															data-target="#ModalUpdateInsert" 
															data-lplantid='<c:out value="${storagetypeindicator.plantID}" />'
															data-lplantname='<c:out value="${storagetypeindicator.plantName}" />'
															data-lwarehouseid='<c:out value="${storagetypeindicator.warehouseID}" />'
															data-lwarehousename='<c:out value="${storagetypeindicator.warehouseName}" />'
															data-lstoragetypeindicatorid='<c:out value="${storagetypeindicator.storageTypeIndicatorID}" />'
															data-lstoragetypeindicatorname='<c:out value="${storagetypeindicator.storageTypeIndicatorName}" />'>
															<i class="fa fa-edit"></i></button> 
													<button <c:out value="${buttonstatus}"/>
															id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" onclick="FuncButtonDelete()" 
															data-toggle="modal" 
															data-target="#ModalDelete"
															data-lplantid='<c:out value="${storagetypeindicator.plantID}" />' 
															data-lwarehouseid='<c:out value="${storagetypeindicator.warehouseID}" />'
															data-lstoragetypeindicatorid='<c:out value="${storagetypeindicator.storageTypeIndicatorID}" />' >
													<i class="fa fa-trash"></i>
													</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row --> 
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
	
			<%@ include file="/mainform/pages/master_footer.jsp"%>
	
		</div>
		<!-- ./wrapper -->
	</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
	 	$(function () {
	  		$("#tb_storagetypeindicator").DataTable(); 
	  		$("#tb_master_plant").DataTable();
	  		$('#M002').addClass('active');
	  		$('#M053').addClass('active');
	  		
	  	//shortcut for button 'new'
		    Mousetrap.bind('n', function() {
		    	FuncButtonNew(),
		    	$('#ModalUpdateInsert').modal('show')
		    	});
  		});

		$('#ModalDelete').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var lPlantID = button.data('lplantid');
			var lWarehouseID = button.data('lwarehouseid');
			var lStorageTypeIndicatorID = button.data('lstoragetypeindicatorid');
			$("#temp_txtPlantID").val(lPlantID);
			$("#temp_txtWarehouseID").val(lWarehouseID);
			$("#temp_txtStorageTypeIndicatorID").val(lStorageTypeIndicatorID);
			
		})

		$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
			FuncClear();
			
	 		var button = $(event.relatedTarget);
	 		
	 		var lPlantID = button.data('lplantid');
	 		var lPlantName = button.data('lplantname');
	 		var lWarehouseID = button.data('lwarehouseid');
	 		var lWarehouseName = button.data('lwarehousename');
	 		var lStorageTypeIndicatorID = button.data('lstoragetypeindicatorid');
	 		var lStorageTypeIndicatorName = button.data('lstoragetypeindicatorname');
	 		
	 		var modal = $(this);
	 		
	 		modal.find(".modal-body #txtPlantID").val(lPlantID);
	 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
	 		modal.find(".modal-body #txtStorageTypeIndicatorID").val(lStorageTypeIndicatorID);
	 		modal.find(".modal-body #txtStorageTypeIndicatorName").val(lStorageTypeIndicatorName);
	 		
	 		if(lPlantName != null)
	 			document.getElementById("lblPlantName").innerHTML = '(' + lPlantName + ')';
	 			
	 		if(lWarehouseName != null)
	 			document.getElementById("lblWarehouseName").innerHTML = '(' + lWarehouseName + ')';
	 		
	 		if(lPlantID == null || lPlantID == '')
	 			{
	 				$("#txtPlantID").focus(); 
	 				$("#txtPlantID").click();
	 			}
	 		else
	 			$("#txtStorageTypeIndicatorName").focus();
		});
		
		function FuncClear(){
			$('#mrkPlantID').hide();
			$('#mrkWarehouseID').hide();
			$('#mrkStorageTypeIndicatorID').hide();
			$('#mrkStorageTypeIndicatorName').hide();
			
			document.getElementById("lblPlantName").innerHTML = null;
			document.getElementById("lblWarehouseName").innerHTML = null;
			
			$('#dvPlantID').removeClass('has-error');
			$('#dvWarehouseID').removeClass('has-error');
			$('#dvStorageTypeIndicatorID').removeClass('has-error');
			$('#dvStorageTypeIndicatorName').removeClass('has-error');
			
			$("#dvErrorAlert").hide();
		}
	
		function FuncButtonNew() {
			$('#btnSave').show();
			$('#btnUpdate').hide();
			document.getElementById("lblTitleModal").innerHTML = "Add Storage Type Indicator";	
		
			$('#txtPlantID').prop('disabled', false);
			$('#txtWarehouseID').prop('disabled', false);
			$('#txtStorageTypeIndicatorID').prop('disabled', false);
		}
		
		function FuncButtonUpdate() {
			$('#btnSave').hide();
			$('#btnUpdate').show();
			document.getElementById("lblTitleModal").innerHTML = 'Edit Storage Type Indicator';
		
			$('#txtPlantID').prop('disabled', true);
			$('#txtWarehouseID').prop('disabled', true);
			$('#txtStorageTypeIndicatorID').prop('disabled', true);
		}
		
		function FuncPassStringPlant(lParamPlantID,lParamPlantName){
			$("#txtPlantID").val(lParamPlantID);
			document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
			$('#mrkPlantID').hide();
			$('#dvPlantID').removeClass('has-error');
		}
		
		function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
			$("#txtWarehouseID").val(lParamWarehouseID);
			document.getElementById("lblWarehouseName").innerHTML = '(' + lParamWarehouseName + ')';
			$('#mrkWarehouseID').hide();
			$('#dvWarehouseID').removeClass('has-error');
		}
		
		function FuncValPlant(){
			var txtPlantID = document.getElementById('txtPlantID').value;
			
			if(!txtPlantID.match(/\S/)){
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    }
		    
		    return true;
		}
		
		//action when txtStorageTypeIndicatorID changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtStorageTypeIndicatorID', function(e){
		  
		     $('#mrkStorageTypeIndicatorID').hide();
			 $('#dvStorageTypeIndicatorID').removeClass('has-error');
		
		    });
		});
		
		//action when txtStorageTypeIndicatorName changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtStorageTypeIndicatorName', function(e){
		  
		     $('#mrkStorageTypeIndicatorName').hide();
			 $('#dvStorageTypeIndicatorName').removeClass('has-error');
		
		    });
		});
		
		//get warehouse from plant id
		$(document).ready(function(){
	
		    $(document).on('click', '#txtWarehouseID', function(e){
		  
		     e.preventDefault();
		  
			 var plantid = document.getElementById('txtPlantID').value;
		  
		     $('#dynamic-content-warehouse').html(''); // leave this div blank
			//$('#modal-loader').show();      // load ajax loader on button click
		 
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getwarehouse',
		          type: 'POST',
		          data: 'plantid='+plantid,
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-warehouse').html(''); // blank before load.
		          $('#dynamic-content-warehouse').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-warehouse').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		function FuncValEmptyInput(lParambtn) {
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			var txtStorageTypeIndicatorID = document.getElementById('txtStorageTypeIndicatorID').value;
			var txtStorageTypeIndicatorName = document.getElementById('txtStorageTypeIndicatorName').value;
			
		    if(!txtPlantID.match(/\S/)) {
		    	$("#txtPlantID").focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		        return false;
		    } 
		    
		    if(!txtWarehouseID.match(/\S/)) {    	
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		        return false;
		    }
		    
		    if(!txtStorageTypeIndicatorID.match(/\S/)) {
		    	$('#txtStorageTypeIndicatorID').focus();
		    	$('#dvStorageTypeIndicatorID').addClass('has-error');
		    	$('#mrkStorageTypeIndicatorID').show();
		        return false;
		    } 
		    
		    if(!txtStorageTypeIndicatorName.match(/\S/)) {
		    	$('#txtStorageTypeIndicatorName').focus();
		    	$('#dvStorageTypeIndicatorName').addClass('has-error');
		    	$('#mrkStorageTypeIndicatorName').show();
		        return false;
		    } 
		    
		    jQuery.ajax({
		        url:'${pageContext.request.contextPath}/storagetypeindicator',	
		        type:'POST',
		        data:{"key":lParambtn,"txtPlantID":txtPlantID,"txtWarehouseID":txtWarehouseID,
		        	  "txtStorageTypeIndicatorID":txtStorageTypeIndicatorID,"txtStorageTypeIndicatorName":txtStorageTypeIndicatorName},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	if(data.split("--")[0] == 'FailedInsertStorageTypeIndicator')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan storage type indicator";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtPlantID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else if(data.split("--")[0] == 'FailedUpdateStorageTypeIndicator')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui storage type indicator";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtStorageTypeIndicatorName").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else
		        	{
			        	var url = '${pageContext.request.contextPath}/storagetypeindicator';  
			        	$(location).attr('href', url);
		        	}
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    
		    return true;
		}
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtWarehouseID:focus').length) {
		    	$('#txtWarehouseID:focus').click();
		    }
		});
	</script>
</body>
</html>