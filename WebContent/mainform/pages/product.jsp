<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Product</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

<style>	
/*  css for loading bar */
 .loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 35px;
  height: 35px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
/* end of css loading bar */
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="Product" name="Product" action = "${pageContext.request.contextPath}/Product" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Product <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
<%-- 							<c:if test="${condition == 'SuccessInsert'}"> --%>
<!-- 	    					  <div class="alert alert-success alert-dismissible"> -->
<!--           				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--              				  	<h4><i class="icon fa fa-check"></i> Success</h4> -->
<!--                 			  	Insert new Product succed. -->
<!--               				</div> -->
<%-- 	      					</c:if> --%>
	      					
<%-- 	      					<c:if test="${condition == 'FailedInsert'}"> --%>
<!-- 	      						<div class="alert alert-danger alert-dismissible"> -->
<!--                 				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--                 				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--                 				Insert new Product fail. <c:out value="${conditionDescription}"/>. --%>
<!--               				</div> -->
<%-- 	     					</c:if> --%>
	     					
	     					<c:if test="${condition == 'SuccessUpdateProduct'}">
	    					  <div id="alrUpdate" class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui produk.
              				</div>
	      					</c:if>
	     					
<%-- 	     					<c:if test="${condition == 'SuccessDelete'}"> --%>
<!-- 	    					  <div class="alert alert-success alert-dismissible"> -->
<!--           				      	<button type="buttPon" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--              				  	<h4><i class="icon fa fa-check"></i> Success</h4> -->
<!--                 			  	Delete Product succed. -->
<!--               				</div> -->
<%-- 	      					</c:if> --%>
	      					
<%-- 	      					<c:if test="${condition == 'empty_condition'}"> --%>
<!-- 	    					  <script>$('#alrUpdate').hide();</script> -->
<%-- 	      					</c:if> --%>

<%-- 	      					<c:if test="${condition == 'FailedDelete'}">	 --%>
<!-- 	      						<div class="alert alert-danger alert-dismissible"> -->
<!--                 				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--                 				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<!--                 				Delete Product fail. Please contact admin for help. -->
<!--               				</div> -->
<%-- 	     					</c:if>    --%>
	      					
	      					<!--modal update & Insert -->
	      					<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvProductID" class="form-group">
	            								<label for="recipient-name" class="control-label">Product ID</label><label id="mrkProductID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtProductID" name="txtProductID">
	          								</div>
	          								<div id="dvProductName" class="form-group">
	            								<label for="recipient-name" class="control-label">Product Name</label><label id="mrkProductName" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtProductName" name="txtProductName">
	          								</div>
	          								<div id="dvBaseUOM" class="form-group">
	            								<label for="recipient-name" class="control-label">Base UOM</label><label id="mrkBaseUOM" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<select id="txtBaseUOM" name="txtBaseUOM" class="form-control">
	            								<c:forEach items="${listBaseUom}" var ="baseuom">
												        <option><c:out value="${baseuom.code}" /></option>
										        </c:forEach>
							                    </select>
	          								</div>
	          								<div id="dvIsBOM" class="form-group">
	            								<label for="recipient-name" class="control-label">BOM Product</label><label id="mrkIsBOM" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<select id="txtIsBOM" name="txtIsBOM" class="form-control">
	            								<option value="true">true</option>
							                    <option value="false">false</option>
							                    </select>
	          								</div>
	          								<div id="dvDescription" class="form-group">
									            <label for="desc-text" class="control-label">Description:</label>
									            <textarea class="form-control" id="txtDescription" name="txtDescription"></textarea>
								          	</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									<!-- modal -->
									
							<div class="row">
							<div class="col-md-1 pull-right">
								<button <c:out value="${buttonstatus}"/> id="btnSync" name="btnSync" type="button" class="btn btn-primary pull-right" onclick="FuncSync()"><i class="fa fa-refresh"></i> Sync</button>
							</div>
							<div class="col-md-0 pull-right">
								<!-- loading bar -->
								<div id="dvloader" class="loader" style="display:none"></div>	
							</div>
							<br><br>
	      					</div>
	      					
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>ID</th>
<!-- 										<th>Category ID</th> -->
<!-- 										<th>Brand ID</th> -->
										<th>Title</th>
<!-- 										<th>Code</th> -->
<!-- 										<th>Short Description</th> -->
										<th>Description</th>
<!-- 										<th>Spesification</th> -->
<!-- 										<th>Detail</th> -->
<!-- 										<th>Image</th> -->
<!-- 										<th>Minimum Order</th> -->
<!-- 										<th>Is Best Seller</th> -->
<!-- 										<th>Is Featured Product</th>	 -->
<!-- 										<th>Is Only Product</th> -->
<!-- 										<th>Can Be Sold Online</th> -->
<!-- 										<th>Status</th> -->
										<th>Base UOM</th>
										<th>Is_Bom</th>
										<th style="width:10px;"></th>
									</tr>
								</thead>	

								<tbody>

									<c:forEach items="${listproduct}" var="product">
										<tr>
											<td><c:out value="${product.id}" /></td>
											<td><c:out value="${product.title_en}" /></td>
<%-- 											<td><c:out value="${product.code}" /></td> --%>
<%-- 											<td><c:out value="${product.short_description_en}" /></td> --%>
											<td><c:out value="${product.description_en}" /></td>
<%-- 											<td><c:out value="${product.spesification_en}" /></td>											 --%>
<%-- 											<td><c:out value="${product.detail_en}" /></td> --%>
<%-- 											<td><c:out value="${product.image}" /></td> --%>
											
<%-- 											<td><c:out value="${product.minimum_order}" /></td> --%>
<%-- 											<td><c:out value="${product.is_best_seller}" /></td> --%>
<%-- 											<td><c:out value="${product.is_featured_product}" /></td> --%>
<%-- 											<td><c:out value="${product.is_only_jabodetabek}" /></td> --%>
<%-- 											<td><c:out value="${product.can_be_sold_online}" /></td> --%>
											
<%-- 											<td><c:out value="${product.status}" /></td> --%>
											<td><c:out value="${product.baseUOM}" /></td>
											<td><c:out value="${product.is_bom}" /></td>
											<td>
											<button <c:out value="${buttonstatus}"/> 
													id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert" 
														data-lproductid='<c:out value="${product.id}" />'
														data-lproductname='<c:out value="${product.title_en}" />'
														data-lbaseuom='<c:out value="${product.baseUOM}" />'
														data-lisbom='<c:out value="${product.is_bom}" />'
														data-ldescription='<c:out value="${product.description_en}" />'>
														<i class="fa fa-edit"></i></button>
											</td> 
										</tr>

									</c:forEach>

								</tbody>
								
							</table>
							
						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 		$(function () {
  			$("#tb_itemlib").DataTable( {
  		        "scrollX": true
  		    }); 
  			$('#M003').addClass('active');
  	  		$('#M023').addClass('active');
  	  		
  	  	//shortcut for button 'new'
//   	  	    Mousetrap.bind('n', function() {
//   	  	    	FuncButtonNew(),
//   	  	    	$('#ModalUpdateInsert').modal('show')
//   	  	    	});
  	  		$('#dvloader').hide();
  	  		$("#dvErrorAlert").hide();
  		});
	</script>
	
	<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		var lProductID = button.data('lproductid');
 		var lProductName = button.data('lproductname');
 		var lBaseUOM = button.data('lbaseuom');
 		var lIsBOM = button.data('lisbom');
 		if(lIsBOM==true)
 			lIsBOM='true';
 		else if(lIsBOM==false)
 			lIsBOM='false';
 		else
 			lIsBOM='';
 		var lDescription = button.data('ldescription');
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtProductID").val(lProductID);
 		modal.find(".modal-body #txtProductName").val(lProductName);
 		modal.find(".modal-body #txtBaseUOM").val(lBaseUOM);
 		modal.find(".modal-body #txtIsBOM").val(lIsBOM);
 		modal.find(".modal-body #txtDescription").val(lDescription);
 		
 		if(lProductID == null || lProductID == '')
 			$("#txtProductID").focus();
 		else
 			$("#txtProductName").focus();
	})
</script>

	<script>
	function FuncClear(){
		$('#mrkProductID').hide();
		$('#txtProductID').prop('disabled', false);
		$('#mrkProductName').hide();
		$('#mrkBaseUOM').hide();
		$('#mrkIsBOM').hide();
		
		$('#dvProductID').removeClass('has-error');
		$('#dvProductName').removeClass('has-error');
		$('#dvBaseUOM').removeClass('has-error');
		$('#dvIsBOM').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
	}

	function FuncButtonNew() {
		$('#btnSave').show();
		$('#btnUpdate').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add Product";

		FuncClear();
		$('#txtProductID').prop('disabled', false);
	}
	
	function FuncButtonUpdate() {
		$('#btnSave').hide();
		$('#btnUpdate').show();
		document.getElementById('lblTitleModal').innerHTML = 'Edit Product';
		
		FuncClear();
		$('#txtProductID').prop('disabled', true);
	}
	</script>
	
	<script>
	function FuncValEmptyInput(lParambtn) {
		var txtProductID = document.getElementById('txtProductID').value;
		var txtProductName = document.getElementById('txtProductName').value;
		var txtBaseUOM = document.getElementById('txtBaseUOM').value;
		var txtIsBOM = document.getElementById('txtIsBOM').value;
		var txtDescription = document.getElementById('txtDescription').value;
		
		var dvProductID = document.getElementsByClassName('dvProductID');
		var dvProductName = document.getElementsByClassName('dvProductName');
		var dvBaseUOM = document.getElementsByClassName('dvBaseUOM');
		var dvIsBOM = document.getElementsByClassName('dvIsBOM');
		
		if(!txtProductID.match(/\S/)) {
	    	$("#txtProductID").focus();
	    	$('#dvProductID').addClass('has-error');
	    	$('#mrkProductID').show();
	        return false;
	    } 
		
		if(!txtBaseUOM.match(/\S/)) {
	    	$('#txtBaseUOM').focus();
	    	$('#dvBaseUOM').addClass('has-error');
	    	$('#mrkBaseUOM').show();
	        return false;
	    }
	    
	    if(!txtIsBOM.match(/\S/)) {
	    	$('#txtIsBOM').focus();
	    	$('#dvIsBOM').addClass('has-error');
	    	$('#mrkIsBOM').show();
	        return false;
	    }
	    
	    if(!txtProductName.match(/\S/)) {    	
	    	$('#txtProductName').focus();
	    	$('#dvProductName').addClass('has-error');
	    	$('#mrkProductName').show();
	        return false;
	    }
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Product',	
	        type:'POST',
	        data:{"key":lParambtn,"txtProductID":txtProductID,"txtProductName":txtProductName,"txtBaseUOM":txtBaseUOM, "txtIsBOM":txtIsBOM, "txtDescription":txtDescription},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedUpdateProduct')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui produk";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtProductName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/Product';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	function FuncSync() {
		 $('#dvloader').show();
		 
		jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Product',	
	        type:'GET',
	        data:{"key":"sync"},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	console.log('Service call succeed!');
	        	
	        	var url = '${pageContext.request.contextPath}/Product';  
	        	$(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	            $('#dvloader').hide();
	        }
	    });
		
	    return true;
	}
	</script>
</body>
</html>