<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Stock Transfer</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<%@ include file="/mainform/pages/master_header.jsp"%>
<form id="StockTransfer" name="StockTransfer" action = "${pageContext.request.contextPath}/StockTransfer" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">
		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Stock Transfer <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
	      					
	      					<c:if test="${condition == 'empty_condition'}">
	    					  <script>$('#alrUpdate').hide();</script>
	      					</c:if> 
	     					
	     					<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	        								<input type="hidden" id="temp_docNo" name="temp_docNo" value='<c:out value = "${tempdocNo}"/>' />
	          								<div id="dvTransferID" class="form-group">
	            								<label for="recipient-name" class="control-label">Transfer ID</label><label id="mrkTransferID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtTransferID" name="txtTransferID" readonly="readonly">
	          								</div>
	          								<div id="dvDate" class="form-group ">
	            								<label for="message-text" class="control-label">Date</label><label id="mrkDate" for="recipient-name" class="control-label"> <small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDate" name="txtDate" data-date-format="dd M yyyy">
	          								</div>
	          								<div id="dvPlantID" class="form-group">
	            								<label for="message-text" class="control-label">Plant ID</label><label id="mrkPlantID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" 
	            								data-target="#ModalGetPlantID">
	          								</div>
	          								<div id="dvFrom" class="form-group ">
	            								<label for="message-text" class="control-label">From</label><label id="mrkFrom" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblFromName" name="lblFromName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtFrom" name="txtFrom" data-toggle="modal" 
	            								data-target="#ModalGetWarehouseFromID" onfocus="FuncValPlant()">
	          								</div>
	          								<div id="dvTo" class="form-group ">
	            								<label for="message-text" class="control-label">To</label><label id="mrkTo" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblToName" name="lblToName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtTo" name="txtTo" data-toggle="modal" 
	            								data-target="#ModalGetWarehouseToID" onfocus="FuncValPlant()">
	          								</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      								</div>
    										</div>
  										</div>
									</div>
									
									<!--modal Delete -->
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Stock Transfer</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtTransferID" name="temp_txtTransferID"  />
               	 									<p>Are You Sure Delete This Stock Transfer ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								        
								        <!--modal show plant data -->
										<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelPlantID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_plant" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                  <th>ID</th>
										                  <th>Name</th>
										                  <th>Description</th>
										                  <th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listPlant}" var ="plant">
												        <tr>
												        <td><c:out value="${plant.plantID}"/></td>
												        <td><c:out value="${plant.plantNm}"/></td>
												        <td><c:out value="${plant.desc}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('<c:out value="${plant.plantID}"/>','','','<c:out value="${plant.plantNm}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->	
										
										<!--modal show warehouse From data -->
										<div class="modal fade" id="ModalGetWarehouseFromID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelWarehouseID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content">
           										</div>
           										
<!-- 			         								<table id="tb_master_warehousefrom" class="table table-bordered table-hover"> -->
<!-- 										        <thead style="background-color: #d2d6de;"> -->
<!-- 										                <tr> -->
<!-- 										                  <th>ID</th> -->
<!-- 										                  <th>Name</th> -->
<!-- 										                  <th>Description</th> -->
<!-- 										                  <th>PlantID</th> -->
<!-- 										                  <th style="width: 20px"></th> -->
<!-- 										                </tr> -->
<!-- 										        </thead> -->
										        
<!-- 										        <tbody> -->
										        
<%-- 										        <c:forEach items="${listWarehouse}" var ="warehouse"> --%>
<!-- 												        <tr> -->
<%-- 												        <td><c:out value="${warehouse.warehouseID}"/></td> --%>
<%-- 												        <td><c:out value="${warehouse.warehouseName}"/></td> --%>
<%-- 												        <td><c:out value="${warehouse.warehouseDesc}"/></td> --%>
<%-- 												        <td><c:out value="${warehouse.plantID}"/></td> --%>
<!-- 												        <td><button type="button" class="btn btn-primary" -->
<!-- 												        			data-toggle="modal" -->
<%-- 												        			onclick="FuncPassString('','<c:out value="${warehouse.warehouseID}"/>','',)" --%>
<!-- 												        			data-dismiss="modal" -->
<!-- 												        	><i class="fa fa-fw fa-check"></i></button> -->
<!-- 												        </td> -->
<!-- 										        		</tr> -->
										        		
<%-- 										        </c:forEach> --%>
										        
<!-- 										        </tbody> -->
<!-- 										        </table> -->
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show warehouse From data -->
										
										<!--modal show warehouse To data -->
										<div class="modal fade" id="ModalGetWarehouseToID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelWarehouseID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content-warehouseto">
           										</div>
           										
<!-- 			         								<table id="tb_master_warehouseto" class="table table-bordered table-hover"> -->
<!-- 										        <thead style="background-color: #d2d6de;"> -->
<!-- 										                <tr> -->
<!-- 										                  <th>ID</th> -->
<!-- 										                  <th>Name</th> -->
<!-- 										                  <th>Description</th> -->
<!-- 										                  <th>PlantID</th> -->
<!-- 										                  <th style="width: 20px"></th> -->
<!-- 										                </tr> -->
<!-- 										        </thead> -->
										        
<!-- 										        <tbody> -->
										        
<%-- 										        <c:forEach items="${listWarehouse}" var ="warehouse"> --%>
<!-- 												        <tr> -->
<%-- 												        <td><c:out value="${warehouse.warehouseID}"/></td> --%>
<%-- 												        <td><c:out value="${warehouse.warehouseName}"/></td> --%>
<%-- 												        <td><c:out value="${warehouse.warehouseDesc}"/></td> --%>
<%-- 												        <td><c:out value="${warehouse.plantID}"/></td> --%>
<!-- 												        <td><button type="button" class="btn btn-primary" -->
<!-- 												        			data-toggle="modal" -->
<%-- 												        			onclick="FuncPassString('','','<c:out value="${warehouse.warehouseID}"/>')" --%>
<!-- 												        			data-dismiss="modal" -->
<!-- 												        	><i class="fa fa-fw fa-check"></i></button> -->
<!-- 												        </td> -->
<!-- 										        		</tr> -->
										        		
<%-- 										        </c:forEach> --%>
										        
<!-- 										        </tbody> -->
<!-- 										        </table> -->
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show To From data -->
	      				
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Transfer ID</th>
										<th>Plant ID</th>
										<th>From</th>
										<th>To</th>
										<th>Date</th>
										<th style="width:1px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${liststocktransfer}" var="stocktransfer">
										<tr>
											<td><c:out value="${stocktransfer.transferID}" /></td>
											<td><c:out value="${stocktransfer.plantID}" /></td>
											<td><c:out value="${stocktransfer.from}" /></td>
											<td><c:out value="${stocktransfer.to}" /></td>
											<td><c:out value="${stocktransfer.date}" /></td>
											<td>
<!-- 											<button id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal"  -->
<!-- 														onclick="FuncButtonUpdate()" -->
<!-- 														data-target="#ModalUpdateInsert"  -->
<%-- 														data-ltransferid='<c:out value="${stocktransfer.transferID}" />' --%>
<%-- 														data-lplantid='<c:out value="${stocktransfer.plantID}" />' --%>
<%-- 														data-lfrom='<c:out value="${stocktransfer.from}" />' --%>
<%-- 														data-lto='<c:out value="${stocktransfer.to}" />' --%>
<%-- 														data-ldate='<c:out value="${stocktransfer.date}" />'> --%>
<!-- 														<i class="fa fa-edit"></i></button>  -->
<%-- 												<button id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" onclick="FuncButtonDelete()" data-toggle="modal" data-target="#ModalDelete" data-ldocnumber='<c:out value="${outbound.docNumber}" />'> --%>
<!-- 												<i class="fa fa-trash"></i> -->
<!-- 												</button> -->
												<c:if test="${buttonstatus == 'disabled'}">
													<button <c:out value="${buttonstatus}"/> type="button" id="btnStockTransfer" name="btnStockTransfer"  class="btn btn-default">
													<i class="fa fa-list-ul"></i>
													</button>
												</c:if>
												<c:if test="${buttonstatus != 'disabled'}">
													<a href="${pageContext.request.contextPath}/StockTransferDetail?transferid=<c:out value="${stocktransfer.transferID}" />&docDate=<c:out value="${stocktransfer.date}" />&PlantID=<c:out value="${stocktransfer.plantID}"/>&From=<c:out value="${stocktransfer.from}"/>&To=<c:out value="${stocktransfer.to}"/>">
													<button type="button" id="btnStockTransfer" name="btnStockTransfer"  class="btn btn-default">
													<i class="fa fa-list-ul"></i>
													</button>
												</a>
												</c:if>
											</td>
										</tr>

									</c:forEach>
															
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 		$(function () {
  			$("#tb_itemlib").DataTable();
  	  		$("#tb_master_plant").DataTable();
  	  	$("#tb_master_warehouse").DataTable();
  	  $("#tb_master_warehouse_2").DataTable();
	  	  	$('#M004').addClass('active');
	  		$('#M028').addClass('active');
  	  		
	  		$("#dvErrorAlert").hide();
  		});
 		
 		//shortcut for button 'new'
 	    Mousetrap.bind('n', function() {
 	    	FuncButtonNew(),
 	    	$('#ModalUpdateInsert').modal('show')
 	    	});
	</script>
	<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lTransferId = button.data('ltransferid');
	})
	
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		
 		var modal = $(this);
 		
 		$('#txtDate').focus();
	})
	</script>
<script>
// var mrkVendorID = document.getElementById('mrkVendorID').value;
// var mrkVendorName = document.getElementById('mrkVendorName').value;
// var mrkVendorAddress = document.getElementById('mrkVendorAddress').value;
// var mrkPhone = document.getElementById('mrkPhone').value;
// var mrkPIC = document.getElementById('mrkPIC').value;
// var btnSave = document.getElementById('btnSave').value;
// var btnUpdate = document.getElementById('btnUpdate').value;

var lFrom;
var lTo;

function FuncClear(){
	$('#mrkTransferID').hide();
	$('#mrkPlantID').hide();
	$('#mrkFrom').hide();
	$('#mrkTo').hide();
	$('#mrkDate').hide();
	
	$('#dvTransferID').removeClass('has-error');
	$('#dvPlantID').removeClass('has-error');
	$('#dvFrom').removeClass('has-error');
	$('#dvTo').removeClass('has-error');
	$('#dvDate').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	var tempDocNumber = document.getElementById('temp_docNo').value;
	$('#txtTransferID').val(tempDocNumber);
	
	$('#txtPlantID').val('');
	$('#txtFrom').val('');
	$('#txtTo').val('');
	$('#txtDate').val('');
	
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById('lblTitleModal').innerHTML = "Add Stock Transfer";	
	
	document.getElementById('lblPlantName').innerHTML = null;
	document.getElementById('lblFromName').innerHTML = null;
	document.getElementById('lblToName').innerHTML = null;

	FuncClear();
// 	$('#txtDocNumber').prop('disabled', false);
	
	$('#txtDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
		$('#txtDate').datepicker('setDate', new Date());
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById('lblTitleModal').innerHTML = 'Edit Stock Transfer';

	FuncClear();
	$('#txtTransferID').prop('disabled', true);
	
	$('#ModalUpdateInsert').on('show.bs.modal', function (event) {
 		var button = $(event.relatedTarget);
 		var lTransferID = button.data('ltransferid');
 		var lPlantID = button.data('lplantid');
 		var lFrom = button.data('lfrom');
 		var lTo = button.data('lto');
 		var lDate = button.data('ldate');
 		
 		var modal = $(this);
 		
 		if(lDocNumber == undefined)
 		{
 			
 		}
 		else
 		{
 			modal.find(".modal-body #txtTransferID").val(lTransferID);
 	 		modal.find(".modal-body #txtPlantID").val(lPlantID);
 	 		modal.find(".modal-body #txtFrom").val(lFrom);
 	 		modal.find(".modal-body #txtTo").val(lTo);
 	 		modal.find(".modal-body #txtDate").val(lDate);
 		}
	})
	
	$('#txtDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
}

function FuncPassString(lParamPlantID,lParamFrom,lParamTo,lParamPlantName){	
	if (lParamPlantID){
		$("#txtPlantID").val(lParamPlantID);		
	}
	
	if (lParamFrom){
		$("#txtFrom").val(lParamFrom);
	}
	
	if (lParamTo){
		$("#txtTo").val(lParamTo);
	}
	
	if (lParamPlantName){
		document.getElementById('lblPlantName').innerHTML = '(' + lParamPlantName + ')';
	}
	
}

function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
	$("#txtFrom").val(lParamWarehouseID);
	document.getElementById('lblFromName').innerHTML = '(' + lParamWarehouseName + ')';
}

function FuncPassStringWarehouse2(lParamWarehouseID2,lParamWarehouseName2){
	$("#txtTo").val(lParamWarehouseID2);
	document.getElementById('lblToName').innerHTML = '(' + lParamWarehouseName2 + ')';
}

function FuncValPlant(){	
	var txtPlantID = document.getElementById('txtPlantID').value;
	
// 	var table = $("#tb_master_warehousefrom").DataTable();
// 	table.search( txtPlantID + " " ).draw();
	
// 	var table2 = $("#tb_master_warehouseto").DataTable();
// 	table2.search( txtPlantID + " " ).draw();
	
	FuncClear();
	
	if(!txtPlantID.match(/\S/)) {    	
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
    	
    	$('#ModalGetWarehouseID').modal('show');
    	$('#ModalGetWarehouseID').modal('toggle');
    	
    	alert("Fill Plant ID First ...!!!");
        return false;
    } 
	
    return true;
	
}

function FuncValEmptyInput(lParambtn) {
	var txtTransferID = document.getElementById('txtTransferID').value;
	var txtPlantID = document.getElementById('txtPlantID').value;
	var txtFrom = document.getElementById('txtFrom').value;
	var txtTo = document.getElementById('txtTo').value;
	var txtDate = document.getElementById('txtDate').value;
	
	var dvTransferID = document.getElementsByClassName('dvTransferID');
	var dvPlant = document.getElementsByClassName('dvPlant');
	var dvFrom = document.getElementsByClassName('dvFrom');
	var dvTo = document.getElementsByClassName('dvTo');
	var dvDate = document.getElementsByClassName('dvDate');
	
	if(lParambtn == 'save'){
		$('#txtTransferID').prop('disabled', false);
	}
	else{
		$('#txtTransferID').prop('disabled', true);
	}
	
    if(!txtTransferID.match(/\S/)) {
    	$("#txtTransferID").focus();
    	$('#dvTransferID').addClass('has-error');
    	$('#mrkTransferID').show();
        return false;
    } 
    
    if(!txtPlantID.match(/\S/)) {    	
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
        return false;
    } 
    
    if(!txtFrom.match(/\S/)) {
    	$('#txtFrom').focus();
    	$('#dvFrom').addClass('has-error');
    	$('#mrkFrom').show();
        return false;
    } 
	
    if(!txtTo.match(/\S/)) {
    	$('#txtTo').focus();
    	$('#dvTo').addClass('has-error');
    	document.getElementById('mrkTo').innerHTML = '*';
    	$('#mrkTo').show();
        return false;
    } 
    
    if(!txtDate.match(/\S/)) {
    	$('#txtDate').focus();
    	$('#dvDate').addClass('has-error');
    	$('#mrkDate').show();
        return false;
    }
    
    if(txtFrom.match(txtTo)) {
    	$('#txtTo').focus();
    	$('#dvTo').addClass('has-error');
    	document.getElementById('mrkTo').innerHTML = ' &nbsphas the same value as From';
    	$('#mrkTo').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/StockTransfer',	
        type:'POST',
        data:{"key":lParambtn,"txtTransferID":txtTransferID,"txtPlantID":txtPlantID, "txtFrom":txtFrom,"txtTo":txtTo,"txtDate":txtDate},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertStockTransfer')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan dokumen pindah gudang";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtTransferID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/StockTransfer';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<!-- get warehouse from from plant id -->
<script>
$(document).ready(function(){

    $(document).on('click', '#txtFrom', function(e){
  
     e.preventDefault();
  
//      var plantid = $(this).data('id'); // get id of clicked row
		var plantid = document.getElementById('txtPlantID').value;
  
     $('#dynamic-content').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getwarehouse',
          type: 'POST',
          data: 'plantid='+plantid,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content').html(''); // blank before load.
          $('#dynamic-content').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<!-- get warehouse to from plant id -->
<script>
$(document).ready(function(){

    $(document).on('click', '#txtTo', function(e){
  
     e.preventDefault();
  
//      var plantid = $(this).data('id'); // get id of clicked row
		var plantid = document.getElementById('txtPlantID').value;
  
     $('#dynamic-content-warehouseto').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getwarehouse2',
          type: 'POST',
          data: 'plantid='+plantid,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content-warehouseto').html(''); // blank before load.
          $('#dynamic-content-warehouseto').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content-warehouseto').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtPlantID:focus').length) {
    	$('#txtPlantID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtFrom:focus').length) {
    	$('#txtFrom').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtTo:focus').length) {
    	$('#txtTo').click();
    }
});
</script>

</body>
</html>