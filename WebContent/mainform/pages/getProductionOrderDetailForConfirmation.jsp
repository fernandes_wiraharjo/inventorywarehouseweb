<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!--modal show product batch -->
<!-- <div class="modal fade bs-example-modal-lg" id="ModalShowBatch" tabindex="-1" role="dialog" aria-labelledby="ModalLabelBatchNo"> -->
<!-- 		<div class="modal-dialog modal-lg" role="document"> -->
<!-- 			<div class="modal-content"> -->
<!-- 			<div class="modal-header"> -->
<!-- 					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
<!-- 					<h4 class="modal-title" id="ModalLabelBatchNo">Component Batch Data</h4> -->
<!-- 					<br> -->
<!-- 					<label id="lblproductioncomponentinfo"></label>		 -->
<!-- 			</div> -->
<!-- 					<div class="modal-body"> -->
								
<!-- 					mysql data load product batch will be load here                           -->
<!-- 						<div id="dynamic-content-batch-summary"> -->
<!-- 						</div> -->
								
<!-- 					</div> -->
					
<!-- 					<div class="modal-footer"> -->
<!-- 						<button type="button" class="btn btn-primary" id="btnConfirmBatch" name="btnConfirmBatch" onclick="">Set</button>		 -->
<!-- 					</div> -->
<!-- 					</div> -->
<!-- 			</div> -->
<!-- </div> -->
<!-- /. end of modal show product batch -->

<table id="tb_production_order_detail" class="table table-bordered table-hover">
    <thead style="background-color: #d2d6de;">
    <tr>
    <th>Component Line</th>
	<th>Component ID</th>
<!-- 	<th>Target Qty</th> -->
	<th>Qty</th>
	<th>UOM</th>
<!-- 	<th>Qty Confirmation</th> -->
<!-- 	<th>UOM</th> -->
	<th>Qty Base</th>
	<th>Base UOM</th>
	<th style="width:65px;"></th>
   </tr>
   </thead>
     
     <tbody>
     
     <c:forEach items="${listProductionOrderDetail}" var ="productionorderdetail">
      <tr id="tr<c:out value="${productionorderdetail.componentLine}" />">
	<td><c:out value="${productionorderdetail.componentLine}" /></td>
	<td><c:out value="${productionorderdetail.componentID}" /></td>
	<td><c:out value="${productionorderdetail.qty}" /></td>
	<td><c:out value="${productionorderdetail.UOM}" /></td>
	<td><c:out value="${productionorderdetail.qtyBaseUOM}" /></td>
	<td><c:out value="${productionorderdetail.baseUOM}" /></td>
	<td>
	<button type="button" id="btnUpdateProductionOrderDetail" name="btnUpdateProductionOrderDetail"
	class="btn btn-primary"
	title="update"
	data-toggle="modal" 
	data-target="#ModalUpdateProductionOrderDetail" 
	data-componentline='<c:out value="${productionorderdetail.componentLine}" />'
	data-componentid='<c:out value="${productionorderdetail.componentID}" />'
	data-qty='<c:out value="${productionorderdetail.qty}" />'
	data-uom='<c:out value="${productionorderdetail.UOM}" />'
	data-ordertypeid='<c:out value="${OrderTypeID}" />'
	data-orderno='<c:out value="${OrderNo}" />'
	>
	<i class="fa fa-edit"></i>
	</button>
	
	<button type="button" id="btnShowBatch" name="btnShowBatch"
	class="btn btn-warning"
	title="choose component batch"
	data-toggle="modal" 
	data-target="#ModalShowBatch" 
	data-componentline='<c:out value="${productionorderdetail.componentLine}" />'
	data-componentid='<c:out value="${productionorderdetail.componentID}" />'
	data-qtybase='<c:out value="${productionorderdetail.qtyBaseUOM}" />'
	data-baseuom='<c:out value="${productionorderdetail.baseUOM}" />'
	data-plantid='<c:out value="${PlantID}" />'
	data-warehouseid='<c:out value="${WarehouseID}" />'
	data-ordertypeid='<c:out value="${OrderTypeID}" />'
	data-orderno='<c:out value="${OrderNo}" />'
	>
	<i class="fa fa-list"></i>
	</button>
	</td>
<!-- 	<td> -->
<%-- 	<div id="dvQtyConfirm<c:out value="${productionorderdetail.componentLine}" />" class="form-group"> --%>
<%-- 	<label id="mrkQtyConfirm<c:out value="${productionorderdetail.componentLine}" />" for="recipient-name" class="control-label"><small>*</small></label> --%>
<%-- 	<input id="txtQtyConfirm<c:out value="${productionorderdetail.componentLine}" />"  --%>
<%-- 		name="txtQtyConfirm<c:out value="${productionorderdetail.componentLine}" />"  --%>
<%-- 		type="number" class="form-control" value="<c:out value="${productionorderdetail.qtyConfirmation}" />"> --%>
<!-- 	</div> -->
<!-- 	</td> -->
<!-- 	<td> -->
<%-- 	<div id="dvUOMConfirm<c:out value="${productionorderdetail.componentLine}" />" class="form-group"> --%>
<%-- 	<label id="mrkUOMConfirm<c:out value="${productionorderdetail.componentLine}" />" for="recipient-name" class="control-label"><small>*</small></label> --%>
<%-- 	<select id="txtUOM<c:out value="${productionorderdetail.componentLine}" />" --%>
<%-- 				 name="txtUOM<c:out value="${productionorderdetail.componentLine}" />" class="form-control"> --%>
<%-- 			<option selected="selected"><c:out value="${productionorderdetail.UOM}" /></option> --%>
<%--   			<c:forEach items="${listUom}" var ="uom"> --%>
<%-- 	        <option><c:out value="${uom.code}" /></option> --%>
<%--       		</c:forEach> --%>
<!--         </select> -->
<!--         </div> -->
<!-- 	</td> -->
<!-- 	<td> -->
<!-- 	<button type="button" id="btnConfirmDetail" name="btnConfirmDetail"  -->
<!-- 		class="btn btn-success" -->
<!-- 		title="confirm" -->
<%-- 		data-confirmid='<c:out value="${ConfirmID}" />' --%>
<%-- 		data-confirmno='<c:out value="${ConfirmNo}" />' --%>
<%-- 		data-confirmdate='<c:out value="${ConfirmDate}" />' --%>
<%-- 		data-ordertypeid='<c:out value="${OrderTypeID}" />' --%>
<%-- 		data-orderno='<c:out value="${OrderNo}" />' --%>
<%-- 		data-productid='<c:out value="${ProductID}" />' --%>
<%-- 		data-productqty='<c:out value="${ProductQty}" />' --%>
<%-- 		data-productuom='<c:out value="${ProductUOM}" />' --%>
<%-- 		data-productqtybase='<c:out value="${ProductQtyBase}" />' --%>
<%-- 		data-productbaseuom='<c:out value="${ProductBaseUOM}" />' --%>
<%-- 		data-plantid='<c:out value="${PlantID}" />' --%>
<%-- 		data-warehouseid='<c:out value="${WarehouseID}" />' --%>
<%-- 		data-componentline='<c:out value="${productionorderdetail.componentLine}" />' --%>
<%-- 		data-componentid='<c:out value="${productionorderdetail.componentID}" />' --%>
<%-- 		data-targetcomponentqty='<c:out value="${productionorderdetail.qty}" />' --%>
<%-- 		data-targetcomponentuom='<c:out value="${productionorderdetail.UOM}" />' --%>
<!-- 		> -->
<!-- 		<i class="fa fa-fw fa-check"></i> -->
<!-- 		</button></td> -->
     </tr>
     		
     </c:forEach>
     
     </tbody>
     </table>

<script>
// function FuncClearforConfirmation(){
// 	$('label[id*="mrkQtyConfirm"]').hide();
// 	$('label[id*="mrkUOMConfirm"]').hide();
	
// 	$('div[id*="dvQtyConfirm"]').removeClass('has-error');
// 	$('div[id*="dvUOMConfirm"]').removeClass('has-error');
// }
</script>

<script>
 		$(function () {
  	  		$("#tb_production_order_detail").DataTable();
//   	  	$("#divsuccess").hide();
//   	  	$("#divfailed").hide();
//   	  	FuncClearforConfirmation();
  		});
</script>

<script>
$('#ModalShowBatch').on('shown.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	
	var lordertypeid = button.data('ordertypeid');
	var lorderno = button.data('orderno');
	var lcomponentline = button.data('componentline');
	var lcomponentid = button.data('componentid');
	var lqtybase = button.data('qtybase');
	var lbaseuom = button.data('baseuom');
	var lplantid = button.data('plantid');
	var lwarehouseid = button.data('warehouseid');
	
	var modal = $(this);
	
	document.getElementById("lblproductioncomponentinfo").innerHTML = 'Line : ' + lcomponentline +
								"&nbsp&nbsp&nbsp&nbsp" + "Component ID : " + lcomponentid + "&nbsp&nbsp&nbsp&nbsp" + "Qty : " + lqtybase +
								"&nbsp" + lbaseuom;
	//String(lqtybase).replace('-','')
	
	$.ajax({
        url: '${pageContext.request.contextPath}/getComponentBatchSummary',
        type: 'POST',
        data: {"componentid":lcomponentid,"componentline":lcomponentline,"plantid":lplantid,"warehouseid":lwarehouseid,"ordertypeid":lordertypeid,"orderno":lorderno,"targetqtybase":lqtybase},
        dataType: 'html'
   })
   .done(function(data){
       console.log(data); 
       $('#dynamic-content-batch-summary').html(''); // blank before load.
       $('#dynamic-content-batch-summary').html(data); // load here
  })
  .fail(function(){
       $('#dynamic-content-batch-summary').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
  });
})
</script>

<script>
// $(document).ready(function(){
	
//     $(document).on('click', '#btnConfirmDetail', function(e){
    	
//     		e.preventDefault();
    		
//     		var keyButton = "ConfirmDetail";
//     		var ConfirmID = $(this).data('confirmid');
//     		var ConfirmNo = $(this).data('confirmno');
//     		var ConfirmDate = $(this).data('confirmdate');
//     		var OrderTypeID = $(this).data('ordertypeid');
//     		var OrderNo = $(this).data('orderno');
//     		var ProductID = $(this).data('productid');
//     		var ProductQty = $(this).data('productqty');
//     		var ProductUom = $(this).data('productuom');
//     		var ProductQtyBase = $(this).data('productqtybase');
//     		var ProductBaseUom = $(this).data('productbaseuom');
//     		var PlantID = $(this).data('plantid');
//     		var WarehouseID = $(this).data('warehouseid');
//     		var ComponentLine = $(this).data('componentline');
//     		var ComponentID = $(this).data('componentid');
//     		var ComponentQty = document.getElementById('txtQtyConfirm'+ComponentLine).value;
//     		var ComponentUOM = document.getElementById('txtUOM'+ComponentLine).value;
//     		var ComponentTargetQty = $(this).data('targetcomponentqty');
//     		var ComponentTargetUOM = $(this).data('targetcomponentuom');
    		
//     		FuncClearforConfirmation();
    		
//     		if(!ComponentQty.match(/\S/)) {
//     	    	$('#txtQtyConfirm'+ComponentLine).focus();
//     	    	$('#dvQtyConfirm'+ComponentLine).addClass('has-error');
//     	    	$('#mrkQtyConfirm'+ComponentLine).show();
//     	    	document.getElementById('mrkQtyConfirm'+ComponentLine).innerHTML = '*';
//     	        return false;
//     	    }
    		
//     		if(ComponentQty == 0) {
//     	    	$('#txtQtyConfirm'+ComponentLine).focus();
//     	    	$('#dvQtyConfirm'+ComponentLine).addClass('has-error');
//     	    	$('#mrkQtyConfirm'+ComponentLine).show();
//     	    	document.getElementById('mrkQtyConfirm'+ComponentLine).innerHTML = '*';
//     	        return false;
//     	    } 
    		
//     		//ajax 1
//     		$.ajax({
//     	        url: '${pageContext.request.contextPath}/getValidateInputQuantityConfirmation',
//     	        type: 'POST',
//     	        data: {"inputqty":ComponentQty , "inputuom":ComponentUOM, "targetqty":ComponentTargetQty, "targetuom":ComponentTargetUOM, "product":ComponentID},
//     	        dataType: 'text'
//     	   })
//     	   .done(function(data){
//         		console.log(data);
        		
//         		//for text data type
//                 $('#temp_validate').val(data);
                
//                 //process
//                 var validate = document.getElementById('temp_validate').value;
                
//                 if(validate.match('disallow')) {
//                 	$('#txtQtyConfirm'+ComponentLine).focus();
//         	    	$('#dvQtyConfirm'+ComponentLine).addClass('has-error');
//         	    	$('#mrkQtyConfirm'+ComponentLine).show();
//         	    	document.getElementById('mrkQtyConfirm'+ComponentLine).innerHTML = ' &nbspmay not bigger than target ';
        	    	
//         	    	$('#txtUOM'+ComponentLine).focus();
//         	    	$('#dvUOMConfirm'+ComponentLine).addClass('has-error');
//         	    	$('#mrkUOMConfirm'+ComponentLine).show();
        	    	
//         	        return false;
//         	    }
	         
//         		//ajax 2
//         		$.ajax({
// 	              url: '${pageContext.request.contextPath}/ConfirmationProductionOrder',
// 	              type: 'POST',
// 	              data: {"key" : keyButton, "confirmid" : ConfirmID, "confirmno":ConfirmNo, "confirmdate": ConfirmDate , 
// 	            	  "ordertypeid":OrderTypeID, "orderno":OrderNo , "productid":ProductID, "productqty":ProductQty, "productuom":ProductUom ,
// 	            	  "productqtybase": ProductQtyBase, "productbaseuom":ProductBaseUom , "plantid":PlantID, "warehouseid":WarehouseID,
// 	            	  "componentline":ComponentLine, "componentid":ComponentID, "componentqty":ComponentQty, "componentuom":ComponentUOM},
// 	              dataType: 'text',
// 	              success:function(data, textStatus, jqXHR){
// 					// var url = '${pageContext.request.contextPath}/ConfirmationProductionOrder';  
// 					// $(location).attr('href', url);
// 	            	  	console.log(data);
	    	        	
// 	    	        	if(data == 'SuccessInsertConfirmation')
// 	    	        		{
// 	    	        			$("#tr"+ComponentLine).hide();
// 	    	        			$("#divsuccess").show();
// 	    	        			$("#divfailed").hide();
// 	    	        		}
// 	    	        	else
// 	    	        		{
// 	    	        			$("#divfailed").show();
// 	    	        			$("#divsuccess").hide();
// 	    	        		}
// 	    	        },
// 	    	        error:function(data, textStatus, jqXHR){
// 	    	            console.log('Service call failed!');
// 	    	        }
// 	         })
// 	         //end of ajax 2
	         
// 	         return true;
//     	   })
//     	   .fail(function(){
//   	 		console.log('Service call failed!');
//    			});
//     	   //end of ajax 1
    	   
//     });
// });
</script>