<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_storagebin" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>Storage Type</th>
			<th>Storage Bin</th>
			<th>Storage Section</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listStorageBin}" var ="storbin">
	  <tr>
	  <td><c:out value="${storbin.storageTypeID}" /></td>
		<td><c:out value="${storbin.storageBinID}" /></td>
		<td><c:out value="${storbin.storageSectionID}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassStorageBin('<c:out value="${storbin.storageBinID}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_storagebin").DataTable();
  	});
</script>