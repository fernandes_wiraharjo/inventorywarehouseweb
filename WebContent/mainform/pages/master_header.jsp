<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
//allow access only if session exists
String user = null;
if(session.getAttribute("user") == null){
	response.sendRedirect("/InventoryWarehouseWeb/Login");
}else user = (String) session.getAttribute("user");
%>

  <header class="main-header">

    <!-- Logo -->
    <a href="Dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>K</b>IW</span>
      <!-- logo for regular state and mobile dev	ices -->
      <span class="logo-lg"><b>KenMaster</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
<!--           Messages: style can be found in dropdown.less -->
<!--           <li class="dropdown messages-menu"> -->
<!--             <a href="#" class="dropdown-toggle" data-toggle="dropdown"> -->
<!--               <i class="fa fa-envelope-o"></i> -->
<!--               <span class="label label-success">4</span> -->
<!--             </a> -->
<!--             <ul class="dropdown-menu"> -->
<!--               <li class="header">You have 4 messages</li> -->
<!--               <li> -->
<!--                 inner menu: contains the actual data -->
<!--                 <ul class="menu"> -->
<!--                   <li>start message -->
<!--                     <a href="#"> -->
<!--                       <div class="pull-left"> -->
<!--                         <img src="mainform/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
<!--                       </div> -->
<!--                       <h4> -->
<!--                         Support Team -->
<!--                         <small><i class="fa fa-clock-o"></i> 5 mins</small> -->
<!--                       </h4> -->
<!--                       <p>Why not buy a new awesome theme?</p> -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   end message -->
<!--                   <li> -->
<!--                     <a href="#"> -->
<!--                       <div class="pull-left"> -->
<!--                         <img src="mainform/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image"> -->
<!--                       </div> -->
<!--                       <h4> -->
<!--                         AdminLTE Design Team -->
<!--                         <small><i class="fa fa-clock-o"></i> 2 hours</small> -->
<!--                       </h4> -->
<!--                       <p>Why not buy a new awesome theme?</p> -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   <li> -->
<!--                     <a href="#"> -->
<!--                       <div class="pull-left"> -->
<!--                         <img src="mainform/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image"> -->
<!--                       </div> -->
<!--                       <h4> -->
<!--                         Developers -->
<!--                         <small><i class="fa fa-clock-o"></i> Today</small> -->
<!--                       </h4> -->
<!--                       <p>Why not buy a new awesome theme?</p> -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   <li> -->
<!--                     <a href="#"> -->
<!--                       <div class="pull-left"> -->
<!--                         <img src="mainform/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image"> -->
<!--                       </div> -->
<!--                       <h4> -->
<!--                         Sales Department -->
<!--                         <small><i class="fa fa-clock-o"></i> Yesterday</small> -->
<!--                       </h4> -->
<!--                       <p>Why not buy a new awesome theme?</p> -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   <li> -->
<!--                     <a href="#"> -->
<!--                       <div class="pull-left"> -->
<!--                         <img src="mainform/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image"> -->
<!--                       </div> -->
<!--                       <h4> -->
<!--                         Reviewers -->
<!--                         <small><i class="fa fa-clock-o"></i> 2 days</small> -->
<!--                       </h4> -->
<!--                       <p>Why not buy a new awesome theme?</p> -->
<!--                     </a> -->
<!--                   </li> -->
<!--                 </ul> -->
<!--               </li> -->
<!--               <li class="footer"><a href="#">See All Messages</a></li> -->
<!--             </ul> -->
<!--           </li> -->
<!--           Notifications: style can be found in dropdown.less -->
<!--           <li class="dropdown notifications-menu"> -->
<!--             <a href="#" class="dropdown-toggle" data-toggle="dropdown"> -->
<!--               <i class="fa fa-bell-o"></i> -->
<!--               <span class="label label-warning">10</span> -->
<!--             </a> -->
<!--             <ul class="dropdown-menu"> -->
<!--               <li class="header">You have 10 notifications</li> -->
<!--               <li> -->
<!--                 inner menu: contains the actual data -->
<!--                 <ul class="menu"> -->
<!--                   <li> -->
<!--                     <a href="#"> -->
<!--                       <i class="fa fa-users text-aqua"></i> 5 new members joined today -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   <li> -->
<!--                     <a href="#"> -->
<!--                       <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the -->
<!--                       page and may cause design problems -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   <li> -->
<!--                     <a href="#"> -->
<!--                       <i class="fa fa-users text-red"></i> 5 new members joined -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   <li> -->
<!--                     <a href="#"> -->
<!--                       <i class="fa fa-shopping-cart text-green"></i> 25 sales made -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   <li> -->
<!--                     <a href="#"> -->
<!--                       <i class="fa fa-user text-red"></i> You changed your username -->
<!--                     </a> -->
<!--                   </li> -->
<!--                 </ul> -->
<!--               </li> -->
<!--               <li class="footer"><a href="#">View all</a></li> -->
<!--             </ul> -->
<!--           </li> -->
<!--           Tasks: style can be found in dropdown.less -->
<!--           <li class="dropdown tasks-menu"> -->
<!--             <a href="#" class="dropdown-toggle" data-toggle="dropdown"> -->
<!--               <i class="fa fa-flag-o"></i> -->
<!--               <span class="label label-danger">9</span> -->
<!--             </a> -->
<!--             <ul class="dropdown-menu"> -->
<!--               <li class="header">You have 9 tasks</li> -->
<!--               <li> -->
<!--                 inner menu: contains the actual data -->
<!--                 <ul class="menu"> -->
<!--                   <li>Task item -->
<!--                     <a href="#"> -->
<!--                       <h3> -->
<!--                         Design some buttons -->
<!--                         <small class="pull-right">20%</small> -->
<!--                       </h3> -->
<!--                       <div class="progress xs"> -->
<!--                         <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"> -->
<!--                           <span class="sr-only">20% Complete</span> -->
<!--                         </div> -->
<!--                       </div> -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   end task item -->
<!--                   <li>Task item -->
<!--                     <a href="#"> -->
<!--                       <h3> -->
<!--                         Create a nice theme -->
<!--                         <small class="pull-right">40%</small> -->
<!--                       </h3> -->
<!--                       <div class="progress xs"> -->
<!--                         <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"> -->
<!--                           <span class="sr-only">40% Complete</span> -->
<!--                         </div> -->
<!--                       </div> -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   end task item -->
<!--                   <li>Task item -->
<!--                     <a href="#"> -->
<!--                       <h3> -->
<!--                         Some task I need to do -->
<!--                         <small class="pull-right">60%</small> -->
<!--                       </h3> -->
<!--                       <div class="progress xs"> -->
<!--                         <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"> -->
<!--                           <span class="sr-only">60% Complete</span> -->
<!--                         </div> -->
<!--                       </div> -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   end task item -->
<!--                   <li>Task item -->
<!--                     <a href="#"> -->
<!--                       <h3> -->
<!--                         Make beautiful transitions -->
<!--                         <small class="pull-right">80%</small> -->
<!--                       </h3> -->
<!--                       <div class="progress xs"> -->
<!--                         <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"> -->
<!--                           <span class="sr-only">80% Complete</span> -->
<!--                         </div> -->
<!--                       </div> -->
<!--                     </a> -->
<!--                   </li> -->
<!--                   end task item -->
<!--                 </ul> -->
<!--               </li> -->
<!--               <li class="footer"> -->
<!--                 <a href="#">View all tasks</a> -->
<!--               </li> -->
<!--             </ul> -->
<!--           </li> -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="mainform/dist/img/320X320 XXXDPI_user.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><%=user %></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="mainform/dist/img/320X320 XXXDPI_user.png" class="img-circle" alt="User Image">

                <p>
                <%=user %>
<!--                   Alexander Pierce - Web Developer -->
<!--                   <small>Member since Nov. 2012</small> -->
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
<!--                   <div class="col-xs-4 text-center"> -->
<!--                     <a href="#">Followers</a> -->
<!--                   </div> -->
<!--                   <div class="col-xs-4 text-center"> -->
<!--                     <a href="#">Sales</a> -->
<!--                   </div> -->
<!--                   <div class="col-xs-4 text-center"> -->
<!--                     <a href="#">Friends</a> -->
<!--                   </div> -->
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
<!--                 <div class="pull-left"> -->
<!--                   <a href="#" class="btn btn-default btn-flat">Profile</a> -->
<!--                 </div> -->
                
                <form name="Form_Logout" action = "${pageContext.request.contextPath}/logout" method="post">
                <div class="pull-right">
                  <input type="submit" class="btn btn-default btn-flat" value="Sign out" >
<!--                   <a href="#" class="btn btn-default btn-flat">Sign out</a> -->
                </div>
                </form>
                
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
<!--       <div class="user-panel"> -->
<!--         <div class="pull-left image"> -->
<!--           <img src="mainform/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
<!--         </div> -->
<!--         <div class="pull-left info"> -->
<!--           <p>Alexander Pierce</p> -->
<!--           <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
<!--         </div> -->
<!--       </div> -->

      <!-- search form -->
<!--       <form action="#" method="get" class="sidebar-form"> -->
<!--         <div class="input-group"> -->
<!--           <input type="text" name="q" class="form-control" placeholder="Search..."> -->
<!--               <span class="input-group-btn"> -->
<!--                 <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i> -->
<!--                 </button> -->
<!--               </span> -->
<!--         </div> -->
<!--       </form> -->
      <!-- /.search form -->
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        
        <li id="M001"><a href="Dashboard"><i class="fa fa-home"></i> <span>Home</span></a></li>
        
       	<li id="M002" class="treeview">
          <a href="#">
            <i class="fa  fa-book"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li id="M010"><a href="Batch"><i class="fa fa-circle-o"></i> Batch</a></li>
            
            <li id="M011"><a href="ClosingPeriod"><i class="fa fa-circle-o"></i> Closing Period</a></li>
            
           	<li id="M012"><a href="CustomerType"><i class="fa fa-circle-o"></i>Customer Type</a></li>
          	<li id="M013"><a href="Customer"><i class="fa fa-circle-o"></i>Customer</a></li>
          	<li id="M014"><a href="CustomerBranch"><i class="fa fa-circle-o"></i>Customer Branch</a></li>
            
            <li id="M015"><a href="Vendor"><i class="fa fa-circle-o"></i> Vendor</a></li>
          	<li id="M016"><a href="plant"><i class="fa fa-circle-o"></i> Plant</a></li>
            <li id="M017"><a href="warehouse"><i class="fa fa-circle-o"></i> Warehouse</a></li>
            <li id="M018"><a href="uom"><i class="fa fa-circle-o"></i> Unit Of Measurement</a></li>
            <li id="M046"><a href="numberranges"><i class="fa fa-circle-o"></i> Number Ranges Setup</a></li>
            <li id="M047"><a href="storagetype"><i class="fa fa-circle-o"></i> Storage Type</a></li>
            <li id="M048"><a href="storagesection"><i class="fa fa-circle-o"></i> Storage Section</a></li>
            <li id="M049"><a href="door"><i class="fa fa-circle-o"></i> Door</a></li>
            <li id="M050"><a href="materialstagingarea"><i class="fa fa-circle-o"></i> Material Staging Area</a></li>
            <li id="M051"><a href="storagebintype"><i class="fa fa-circle-o"></i> Storage Bin Type</a></li>
            <li id="M052"><a href="storagebin"><i class="fa fa-circle-o"></i> Storage Bin</a></li>
            <li id="M053"><a href="storagetypeindicator"><i class="fa fa-circle-o"></i> Storage Type Indicator</a></li>
            <li id="M054"><a href="storagesectionindicator"><i class="fa fa-circle-o"></i> Storage Section Indicator</a></li>
            <li id="M055"><a href="storageunittype"><i class="fa fa-circle-o"></i> Storage Unit Type</a></li>
            <li id="M060"><a href="refmovementtype"><i class="fa fa-circle-o"></i> Ref. Movement Type For WM</a></li>
          </ul>
        </li>
        
        <li id="M003">
        	<a href="#"><i class="fa fa-cube"></i>
	        	<span class="pull-right-container">
	        	<i class="fa fa-angle-left pull-right"></i>
	        	</span>
	        	Product
        	</a>
        	<ul class="treeview-menu">
	        	<li id="M019"><a href="Department"><i class="fa fa-circle-o"></i> Department</a></li>
	        	<li id="M020"><a href="Group"><i class="fa fa-circle-o"></i> Group</a></li>
	        	<li id="M021"><a href="Category"><i class="fa fa-circle-o"></i> Category</a></li>
	        	<li id="M022"><a href="Brand"><i class="fa fa-circle-o"></i> Brand</a></li>
	        	<li id="M023"><a href="Product"><i class="fa fa-circle-o"></i> Master</a></li>
	      		<li id="M024"><a href="ProductDetail"><i class="fa fa-circle-o"></i> Detail</a></li>
	      		<li id="M025"><a href="productuom"><i class="fa fa-circle-o"></i> Product UOM</a></li>
	      		<li id="M056"><a href="productWM"><i class="fa fa-circle-o"></i> Setting For WM</a></li>
        	</ul>
        </li>
        
       <li id="M004" class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="M026"><a href="Inbound"><i class="fa fa-circle-o"></i> Inbound</a></li>
            <li id="M027"><a href="Outbound"><i class="fa fa-circle-o"></i> Outbound</a></li>
            <li id="M028"><a href="StockTransfer"><i class="fa fa-circle-o"></i> Stock Transfer</a></li>
            <li id="M029"><a href="StockSummary"><i class="fa fa-circle-o"></i> Stock Summary</a></li>
          </ul>
        </li>
		<!-- user menu -->

		<li id="M005">
            	<a href="#"><i class="fa fa-undo"></i>
            	<span class="pull-right-container">
            	<i class="fa fa-angle-left pull-right"></i>
            	</span>
            	Cancel Transaction
            	</a>
            	<ul class="treeview-menu">
            	<li id="M030"><a href="CancelInbound"><i class="fa fa-circle-o"></i> Cancel Inbound</a></li>
            	<li id="M031"><a href="CancelOutbound"><i class="fa fa-circle-o"></i> Cancel Outbound</a></li>
            	<li id="M032"><a href="CancelStockTransfer"><i class="fa fa-circle-o"></i> Cancel Stock Transfer</a></li>
            	</ul>
            </li>
            
          <li id="M006" class="treeview">
          <a href="#">
            <i class="fa fa-cubes"></i>
            <span>BOM</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li id="M033"><a href="MasterBom"><i class="fa fa-circle-o"></i> Master</a></li>
           	<li id="M034"><a href="bomordertype"><i class="fa fa-circle-o"></i> Order Type</a></li>
           	<li id="M035"><a href=BomConfirmationDocument><i class="fa fa-circle-o"></i> Confirmation Document</a></li>
          </ul>
        </li>
        
        <li id="M007">
            	<a href="#"><i class="fa fa-edit"></i>
            	<span class="pull-right-container">
            	<i class="fa fa-angle-left pull-right"></i>
            	</span>
            	BOM Transaction
            	</a>
            	<ul class="treeview-menu">
            	<li id="M036"><a href="BomProductionOrder"><i class="fa fa-circle-o"></i> Production Order</a></li>
            	<li id="M037"><a href="ConfirmationProductionOrder"><i class="fa fa-circle-o"></i> Confirmation Production</a></li>
            	<li id="M038"><a href="CancelConfirmation"><i class="fa fa-circle-o"></i> Cancel Confirmation</a></li>
            	</ul>
            </li>
            
            <li id="M057">
            	<a href="#"><i class="fa fa-gear"></i>
            	<span class="pull-right-container">
            	<i class="fa fa-angle-left pull-right"></i>
            	</span>
            	WM Setup
            	</a>
            	<ul class="treeview-menu">
            	<li id="M058"><a href="stortypesearch"><i class="fa fa-circle-o"></i> Storage Type Search</a></li>
            	<li id="M059"><a href="storsectionsearch"><i class="fa fa-circle-o"></i> Storage Section Search</a></li>
            	<li id="M061"><a href="movementtype"><i class="fa fa-circle-o"></i> Movement Type</a></li>
            	</ul>
            </li>

        <li id="M008" class="treeview">
        	<a href="#">
            <i class="fa fa-user"></i>
            <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li id="M039"><a href="userrule"><i class="fa fa-circle-o"></i> User Role</a></li>
            <li id="M040"><a href="user"><i class="fa fa-circle-o"></i> User Management</a></li>
          </ul>
        </li>
		<!-- end of user menu -->
        
        <li id="M009" class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li id="M041"><a href="RptStock"><i class="fa fa-circle-o"></i> Report Stock</a></li>
            <li id="M042"><a href="RptGoodReceipt"><i class="fa fa-circle-o"></i> Report Inbound</a></li>
            <li id="M043"><a href="RptGoodIssue"><i class="fa fa-circle-o"></i> Report Outbound</a></li>
            <li id="M044"><a href="RptStockTransfer"><i class="fa fa-circle-o"></i> Report Stock Transfer</a></li>
            <li id="M045"><a href="RptBalanceQty"><i class="fa fa-circle-o"></i> Report Balance</a></li>
          </ul>
        </li>
        
<!--         closed menu -->

<!--         <li class="treeview"> -->
<!--           <a href="#"> -->
<!--             <i class="fa fa-users"></i> -->
<!--             <span>Customers</span> -->
<!--             <span class="pull-right-container"> -->
<!--               <i class="fa fa-angle-left pull-right"></i> -->
<!--             </span> -->
<!--           </a> -->
<!--           <ul class="treeview-menu"> -->
<!--             <li><a href="#"><i class="fa fa-circle-o"></i> Customers List</a></li> -->
<!--             <li><a href="#"><i class="fa fa-circle-o"></i> Feedback</a></li> -->
<!--           </ul> -->
<!--         </li> -->
<!--         <li class="treeview"> -->
<!--           <a href="#"> -->
<!--             <i class="fa fa-briefcase"></i> -->
<!--             <span>Employees</span> -->
<!--             <span class="pull-right-container"> -->
<!--               <i class="fa fa-angle-left pull-right"></i> -->
<!--             </span> -->
<!--           </a> -->
<!--           <ul class="treeview-menu"> -->
<!--             <li><a href="#"><i class="fa fa-circle-o"></i> Staff</a></li> -->
<!--             <li><a href="#"><i class="fa fa-circle-o"></i> Permission</a></li> -->
<!--           </ul> -->
<!--         </li> -->
<!--         <li><a href="mainform/pages/dashboard.jsp"><i class="fa fa-shopping-cart"></i> <span>Outlets</span></a></li> -->
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  


 
<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>

<script>

$(function () {
	//get restricted menu
	$.ajax({
	    url: '${pageContext.request.contextPath}/site_master',
	    type: 'GET',
	    data: {"key":"RestrictedMenu"},
	    dataType: 'json'
	})
		.done(function(data){
	    console.log(data);
		
	    //for json data type
	    for (var i in data) {
	      var obj = data[i];
	      var index = 0;
	      var menuid;
	      for (var prop in obj) {
	          switch (index++) {
	              case 0:
	                  menuid = obj[prop];
	                  break;
	              default:
	                  break;
	          }
	      }
		        
	      $("#"+menuid).hide();
		        
		  }
		             
	   })
	   .fail(function(){
	  	 console.log('Service call failed!');
	   });
  
 return true;
})

</script>