<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Inbound</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="Inbound" name="Inbound" action = "${pageContext.request.contextPath}/Inbound" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Inbound <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">
					
					<div class="box-body">
	      					
	      					<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	      								
	        								<input type="hidden" id="temp_docNo" name="temp_docNo" value='<c:out value = "${tempdocNo}"/>' />
	          								<div id="dvDocNumber" class="form-group">
	            								<label for="recipient-name" class="control-label">Document No</label><label id="mrkDocNumber" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDocNumber" name="txtDocNumber" readonly="readonly">
	          								</div>
	          								<div id="dvDate" class="form-group">
	            								<label for="message-text" class="control-label">Date</label><label id="mrkDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDate" name="txtDate" data-date-format="dd M yyyy">
	          								</div>
	          								<div id="dvVendorID" class="form-group">
	            								<label for="message-text" class="control-label">Vendor ID</label><label id="mrkVendorID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblVendorName" name="lblVendorName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtVendorID" name="txtVendorID" data-toggle="modal" data-target="#ModalGetVendorID">
	          								</div>
	          								<div id="dvPlantID" class="form-group">
	            								<label for="message-text" class="control-label">Plant ID</label><label id="mrkPlantID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" data-target="#ModalGetPlantID">
	          								</div>
	          								<div id="dvWarehouseID" class="form-group">
	            								<label for="message-text" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID"
	            									data-toggle="modal" data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()"
	            									data-id="">
	          								</div>
	          								<div id="dvDoorID" class="form-group" style="display:none">
	            								<label for="message-text" class="control-label">Door ID</label><label id="mrkDoorID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblDoorName" name="lblDoorName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtDoorID" name="txtDoorID" data-toggle="modal" 
	            								data-target="#ModalGetDoorID" onfocus="FuncValPlantWarehouse()">
	          								</div>
	          								<div id="dvStagingAreaID" class="form-group" style="display:none">
		           								<label class="control-label">Staging Area ID</label><label id="mrkStagingAreaID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblStagingAreaName" name="lblStagingAreaName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtStagingAreaID" name="txtStagingAreaID" data-toggle="modal" 
	            								data-target="#ModalGetStagingAreaID" onfocus="FuncValPlantWarehouseDoor()"
		           								>
		         							</div>
		         							<div style="display:none">
		           								<input type="text" class="form-control" id="txtInboundWMSStatus" name="txtInboundWMSStatus">
		           								<input type="text" class="form-control" id="txtInboundUpdateWMFirst" name="txtInboundUpdateWMFirst">
		         							</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
									<!--modal Delete -->
									<div class="example-modal">
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Inbound</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtDocNumber" name="temp_txtDocNumber" >
               	 									<p>Are you sure to delete this inbound document ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								      </div>
								      
								      <!--modal Inbound Confirmation -->
<!-- 									<div class="example-modal"> -->
<!--        									<div class="modal modal-info" id="ModalInboundConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> -->
<!--           									<div class="modal-dialog" role="document"> -->
<!--            										<div class="modal-content"> -->
<!--               										<div class="modal-header"> -->
<!--                 										<button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
<!--                   										<span aria-hidden="true">&times;</span></button> -->
<!--                 										<h4 class="modal-title">Alert Confirmation</h4> -->
<!--               										</div> -->
<!--               									<div class="modal-body"> -->
<!--               									<label id="lblDocNumberInboundDetail" name="lblDocNumberInboundDetail"> -->
<!--               									</label> -->
<!--               									<input type="hidden" id="txtDocNumberInboundDetail" name="txtDocNumberInboundDetail"> -->
<!--                	 									<p>Are you sure to show this inbound document detail ?</p> -->
<!--               									</div> -->
<!-- 								              <div class="modal-footer"> -->
<!-- 								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button> -->
<!-- 								                <button type="submit" id="btnInboundDetail" name="btnInboundDetail"  class="btn btn-outline">Ok</button> -->
<!-- 								              </div> -->
<!-- 								            </div> -->
<!-- 								            /.modal-content -->
<!-- 								          </div> -->
<!-- 								          /.modal-dialog -->
<!-- 								        </div> -->
<!-- 								        /.modal -->
<!-- 								      </div> -->
								      
								      <!--modal show vendor data -->
										<div class="modal fade" id="ModalGetVendorID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelVendorID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelVendorID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_vendor" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Vendor ID</th>
														<th>Vendor Name</th>
														<th>Vendor Address</th>
														<th>Phone</th>
														<th>PIC</th>
														<th>Vendor Type</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listvendor}" var ="vendor">
												        <tr>
												        <td><c:out value="${vendor.vendorID}" /></td>
														<td><c:out value="${vendor.vendorName}" /></td>
														<td><c:out value="${vendor.vendorAddress}" /></td>
														<td><c:out value="${vendor.phone}" /></td>
														<td><c:out value="${vendor.PIC}" /></td>
														<td><c:out value="${vendor.vendorType}" /></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassStringVendor('<c:out value="${vendor.vendorID}"/>','<c:out value="${vendor.vendorName}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show vendor data -->
										
										<!--modal show plant data -->
										<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelPlantID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_plant" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Plant ID</th>
										                <th>Plant Name</th>
										                <th>Description</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listPlant}" var ="plant">
												        <tr>
												        <td><c:out value="${plant.plantID}"/></td>
												        <td><c:out value="${plant.plantNm}"/></td>
												        <td><c:out value="${plant.desc}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassStringPlant('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->
										
										<!--modal show warehouse data -->
										<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelWarehouseID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
<!-- 			     								<div id="modal-loader" style="display: none; text-align: center;"> -->
<!-- 										           ajax loader -->
<!-- 										           <img src="ajax-loader.gif"> -->
<!-- 										           </div> -->
			       								
			       								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content">
           										</div>
           										
<!-- 			         								<table id="tb_master_warehouse" class="table table-bordered table-hover"> -->
<!-- 										        <thead style="background-color: #d2d6de;"> -->
<!-- 										                <tr> -->
<!-- 										                <th>Plant ID</th> -->
<!-- 														<th>Warehouse ID</th> -->
<!-- 														<th>Warehouse Name</th> -->
<!-- 														<th>Warehouse Desc</th> -->
<!-- 														<th style="width: 20px"></th> -->
<!-- 										                </tr> -->
<!-- 										        </thead> -->
										        
<!-- 										        <tbody> -->
										        
<%-- 										        <c:forEach items="${listwarehouse}" var ="warehouse"> --%>
<!-- 												        <tr> -->
<%-- 												        <td><c:out value="${warehouse.plantID}" /></td> --%>
<%-- 														<td><c:out value="${warehouse.warehouseID}" /></td> --%>
<%-- 														<td><c:out value="${warehouse.warehouseName}" /></td> --%>
<%-- 														<td><c:out value="${warehouse.warehouseDesc}" /></td> --%>
<!-- 												        <td><button type="button" class="btn btn-primary" -->
<!-- 												        			data-toggle="modal" -->
<%-- 												        			onclick="FuncPassStringWarehouse('<c:out value="${warehouse.warehouseID}"/>')" --%>
<!-- 												        			data-dismiss="modal" -->
<!-- 												        	><i class="fa fa-fw fa-check"></i></button> -->
<!-- 												        </td> -->
<!-- 										        		</tr> -->
										        		
<%-- 										        </c:forEach> --%>
										        
<!-- 										        </tbody> -->
<!-- 										        </table> -->
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show warehouse data -->
										
								<!--modal show door data -->
								<div class="modal fade" id="ModalGetDoorID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelDoorID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelDoorID">Data Door</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load door by plant and warehouse id will be load here -->                          
           										<div id="dynamic-content-door">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show door data -->
								
								<!-- modal show staging area data -->
								<div class="modal fade" id="ModalGetStagingAreaID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStagingAreaID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelStagingAreaID">Data Staging Area</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load staging area by plant and warehouse id will be load here -->                          
           										<div id="dynamic-content-staging-area">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show staging area data -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_inbound" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Doc No</th>
										<th>Date</th>
										<th>Vendor ID</th>
										<th>Plant ID</th>
										<th>Warehouse ID</th>
										<th>Door ID</th>
										<th>Staging Area ID</th>
										<th>Status</th>
										<th style="width:1px;"></th>
<!-- 										<th style="width:103px;"></th> -->
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listInbound}" var="inbound">
										<tr>
											<td><c:out value="${inbound.docNumber}" /></td>
											<td><c:out value="${inbound.date}" /></td>
											<td><c:out value="${inbound.vendorID}" /></td>
											<td><c:out value="${inbound.plantID}" /></td>
											<td><c:out value="${inbound.warehouseID}" /></td>
											<td><c:out value="${inbound.doorID}" /></td>
											<td><c:out value="${inbound.stagingAreaID}" /></td>
											<td><c:out value="${inbound.inboundStatus}" /></td>
											<td>
<!-- 											<button id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal"  -->
<!-- 														onclick="FuncButtonUpdate()" -->
<!-- 														data-target="#ModalUpdateInsert"  -->
<%-- 														data-ldocnumber='<c:out value="${inbound.docNumber}" />' --%>
<%-- 														data-ldate='<c:out value="${inbound.date}" />' --%>
<%-- 														data-lvendorid='<c:out value="${inbound.vendorID}" />' --%>
<%-- 														data-lplantid='<c:out value="${inbound.plantID}" />' --%>
<%-- 														data-lwarehouseid='<c:out value="${inbound.warehouseID}" />'> --%>
<!-- 														<i class="fa fa-edit"></i></button>  -->
<!-- 												<button id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" data-toggle="modal"  -->
<%-- 														data-target="#ModalDelete" data-ldocnumber='<c:out value="${inbound.docNumber}" />'> --%>
<!-- 												<i class="fa fa-trash"></i> -->
<!-- 												</button> -->
												<c:if test="${buttonstatus == 'disabled'}">
													<button <c:out value="${buttonstatus}"/> type="button" id="btnInboundDetail" name="btnInboundDetail"  class="btn btn-default"><i class="fa fa-list-ul"></i></button>
												</c:if>
												<c:if test="${buttonstatus != 'disabled'}">
													<a href="${pageContext.request.contextPath}/InboundDetail?docNo=<c:out value="${inbound.docNumber}" />&docDate=<c:out value="${inbound.date}" />&vendorId=<c:out value="${inbound.vendorID}" />&plantId=<c:out value="${inbound.plantID}" />&warehouseId=<c:out value="${inbound.warehouseID}" />&inwmstatus=<c:out value="${inbound.WMStatus}" />&inupdatewmfirst=<c:out value="${inbound.updateWMFirst}" /> "><button type="button" id="btnInboundDetail" name="btnInboundDetail"  class="btn btn-default"><i class="fa fa-list-ul"></i></button></a>
												</c:if>
<!-- 												<button type="button" class="btn btn-default" data-toggle="modal"  -->
<%-- 														data-target="#ModalInboundConfirmation" data-ldocnumber='<c:out value="${inbound.docNumber}" />'> --%>
<!-- 												<i class="fa fa-list-ul"></i> -->
<!-- 												</button> -->
											</td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_inbound").DataTable();
  		$("#tb_master_vendor").DataTable();
  		$("#tb_master_plant").DataTable();
  		$("#tb_master_warehouse").DataTable();
  		$('#M004').addClass('active');
  		$('#M026').addClass('active');
  		
  		$("#dvErrorAlert").hide();
  	});
 	
 	//shortcut for button 'new'
    Mousetrap.bind('n', function() {
    	FuncButtonNew(),
    	$('#ModalUpdateInsert').modal('show')
    	});
	</script>
    
<script>
	$('#ModalDelete').on('shown.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lDocNumber = button.data('ldocnumber');
		$("#temp_txtDocNumber").val(lDocNumber);
	})
	
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		
 		var modal = $(this);
 		
 		$("#txtDate").focus();
	})
	
// 	$('#ModalInboundConfirmation').on('show.bs.modal', function (event) {
// 		var button = $(event.relatedTarget);
// 		var lDocNumber = button.data('ldocnumber');
// 		document.getElementById('lblDocNumberInboundDetail').innerHTML = lDocNumber;
// 		$("#txtDocNumberInboundDetail").val(lDocNumber);
// 	})
</script>

<script>
//check plant and warehouseid is WMEnabled or Not, if Yes, show door and staging area field, also return the 'updatewmfirst' value 
function FuncCheckWMStatus(){
     var plantid = document.getElementById('txtPlantID').value;
     var warehouseid = document.getElementById('txtWarehouseID').value;
     var wmstatus,updatewmfirst;
     $('#txtDoorID').val('');
	 $('#txtStagingAreaID').val('');
	
	 $.ajax({
          url: '${pageContext.request.contextPath}/getWarehouseWMStatus',
          type: 'POST',
          data: {"plantid":plantid,"warehouseid":warehouseid},
          dataType: 'text'
     })
     .done(function(data){
          console.log(data);
          wmstatus = data.split("--")[0];
          updatewmfirst = data.split("--")[1];
          
          if(wmstatus=='1'){
          	 $('#dvDoorID').show();
          	 $('#dvStagingAreaID').show();
          }
          else{
          	 $('#dvDoorID').hide();
          	 $('#dvStagingAreaID').hide();
          }
          
          $('#txtInboundWMSStatus').val(wmstatus);
          $('#txtInboundUpdateWMFirst').val(updatewmfirst);
     })
     .fail(function(data){
          console.log(data); 
     });
}

function FuncPassStringVendor(lParamVendorID,lParamVendorName){
	$("#txtVendorID").val(lParamVendorID);
	document.getElementById('lblVendorName').innerHTML = '(' + lParamVendorName + ')';
}
function FuncPassStringPlant(lParamPlantID,lParamPlantName){
	$("#txtPlantID").val(lParamPlantID);
	document.getElementById('lblPlantName').innerHTML = '(' + lParamPlantName + ')';
	FuncCheckWMStatus();
}
function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
	$("#txtWarehouseID").val(lParamWarehouseID);
	document.getElementById('lblWarehouseName').innerHTML = '(' + lParamWarehouseName + ')';
	FuncCheckWMStatus();
}
function FuncPassStringDoor(lParamDoorID,lParamDoorName){
	$("#txtDoorID").val(lParamDoorID);
	document.getElementById("lblDoorName").innerHTML = '(' + lParamDoorName + ')';
	$('#mrkDoorID').hide();
	$('#dvDoorID').removeClass('has-error');
}
function FuncPassStringStagingArea(lParamID,lParamName){
	$("#txtStagingAreaID").val(lParamID);
	document.getElementById("lblStagingAreaName").innerHTML = '(' + lParamName + ')';
}
</script>

<script>

function FuncClear(){
	$('#mrkDocNumber').hide();
	$('#txtDocNumber').prop('disabled', false);
	$('#mrkDate').hide();
	$('#mrkVendorID').hide();
	$('#mrkPlantID').hide();
	$('#mrkWarehouseID').hide();
	$('#mrkDoorID').hide();
	$('#mrkStagingAreaID').hide();
	
	$('#dvDocNumber').removeClass('has-error');
	$('#dvDate').removeClass('has-error');
	$('#dvVendorID').removeClass('has-error');
	$('#dvPlantID').removeClass('has-error');
	$('#dvWarehouseID').removeClass('has-error');
	$('#dvDoorID').removeClass('has-error');
	$('#dvStagingAreaID').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	
	var tempDocNumber = document.getElementById('temp_docNo').value;
	$('#txtDocNumber').val(tempDocNumber);	


	$('#txtDate').val('');
	$('#txtVendorID').val('');
	$('#txtPlantID').val('');
	$('#txtWarehouseID').val('');
	$('#txtDoorID').val('');
	$('#txtStagingAreaID').val('');

	$('#btnSave').show();
	$('#btnUpdate').hide();
	$('#dvDoorID').hide();
	$('#dvStagingAreaID').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Inbound";
	
	document.getElementById('lblVendorName').innerHTML = null;
	document.getElementById('lblPlantName').innerHTML = null;
	document.getElementById('lblWarehouseName').innerHTML = null;
	document.getElementById('lblDoorName').innerHTML = null;
	document.getElementById('lblStagingAreaName').innerHTML = null;

	FuncClear();
	//$('#txtDocNumber').prop('disabled', true);
	
	$('#txtDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
		$('#txtDate').datepicker('setDate', new Date());
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById('lblTitleModal').innerHTML = 'Edit Inbound';
	
	FuncClear();
	$('#txtDocNumber').prop('disabled', true);
	
	$('#ModalUpdateInsert').on('show.bs.modal', function (event) {
 		var button = $(event.relatedTarget);
 		var lDocNumber = button.data('ldocnumber');
 		var lDate = button.data('ldate');
 		var lVendorID = button.data('lvendorid');
 		var lPlantID = button.data('lplantid');
 		var lWarehouseID = button.data('lwarehouseid');
 		
 		var modal = $(this);
 		
 		if(lDocNumber == undefined)
 			{
 			
 			}
 		else
 			{
 			modal.find(".modal-body #txtDocNumber").val(lDocNumber);
 	 		modal.find(".modal-body #txtDate").val(lDate);
 	 		modal.find(".modal-body #txtVendorID").val(lVendorID);
 	 		modal.find(".modal-body #txtPlantID").val(lPlantID);
 	 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
 			}
	})
	
	$('#txtDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
}

function FuncValPlant(){	
	var txtPlantID = document.getElementById('txtPlantID').value;
	
	
// 	var table = $("#tb_master_warehouse").DataTable();
// 	table.search( txtPlantID + " " ).draw();
	
	FuncClear();
	
	if(!txtPlantID.match(/\S/)) {    	
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
    	
    	alert("Fill Plant ID First ...!!!");
        return false;
    } 
	
    return true;
	
}

function FuncValPlantWarehouse(){	
	var txtPlantID = document.getElementById('txtPlantID').value;
	var txtWarehouseID = document.getElementById('txtWarehouseID').value;
	
	FuncClear();
	
	if(!txtPlantID.match(/\S/)) {    	
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
    	
    	alert("Fill Plant ID First ...!!!");
        return false;
    } 
    if(!txtWarehouseID.match(/\S/)) {    	
    	$('#txtWarehouseID').focus();
    	$('#dvWarehouseID').addClass('has-error');
    	$('#mrkWarehouseID').show();
    	
    	alert("Fill Warehouse ID First ...!!!");
        return false;
    } 
	
    return true;
}

function FuncValPlantWarehouse(){	
	var txtPlantID = document.getElementById('txtPlantID').value;
	var txtWarehouseID = document.getElementById('txtWarehouseID').value;
	var txtDoorID = document.getElemetById('txtDoorID').value;
	
	FuncClear();
	
	if(!txtPlantID.match(/\S/)) {    	
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
    	
    	alert("Fill Plant ID First ...!!!");
        return false;
    } 
    if(!txtWarehouseID.match(/\S/)) {    	
    	$('#txtWarehouseID').focus();
    	$('#dvWarehouseID').addClass('has-error');
    	$('#mrkWarehouseID').show();
    	
    	alert("Fill Warehouse ID First ...!!!");
        return false;
    }
    if(!txtDoorID.match(/\S/)) {    	
    	$('#txtDoorID').focus();
    	$('#dvDoorID').addClass('has-error');
    	$('#mrkDoorID').show();
    	
    	alert("Fill Door ID First ...!!!");
        return false;
    }  
	
    return true;
}

function FuncValEmptyInput(lParambtn) {
	var txtDocNumber = document.getElementById('txtDocNumber').value;
	var txtDate = document.getElementById('txtDate').value;
	var txtVendorID = document.getElementById('txtVendorID').value;
	var txtPlantID = document.getElementById('txtPlantID').value;
	var txtWarehouseID = document.getElementById('txtWarehouseID').value;
	var txtDoorID = document.getElementById('txtDoorID').value;
	var txtStagingAreaID = document.getElementById('txtStagingAreaID').value;
	var txtStagingAreaID = document.getElementById('txtStagingAreaID').value;
	var txtInboundWMSStatus = document.getElementById('txtInboundWMSStatus').value;
	var txtInboundUpdateWMFirst = document.getElementById('txtInboundUpdateWMFirst').value;
	
	var dvDocNumber = document.getElementsByClassName('dvDocNumber');
	var dvDate = document.getElementsByClassName('dvDate');
	var dvVendorID = document.getElementsByClassName('dvVendorID');
	var dvPlantID = document.getElementsByClassName('dvPlantID');
	var dvWarehouseID = document.getElementsByClassName('dvWarehouseID');
	
	if(lParambtn == 'save'){
		$('#txtDocNumber').prop('disabled', false);
	}
	else{
		$('#txtDocNumber').prop('disabled', true);
	}

    if(!txtDocNumber.match(/\S/)) {
    	$("#txtDocNumber").focus();
    	$('#dvDocNumber').addClass('has-error');
    	$('#mrkDocNumber').show();
        return false;
    } 
    
    if(!txtDate.match(/\S/)) {    	
    	$('#txtDate').focus();
    	$('#dvDate').addClass('has-error');
    	$('#mrkDate').show();
        return false;
    } 
    
    if(!txtVendorID.match(/\S/)) {
    	$('#txtVendorID').focus();
    	$('#dvVendorID').addClass('has-error');
    	$('#mrkVendorID').show();
        return false;
    }
    
    if(!txtPlantID.match(/\S/)) {
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
        return false;
    } 
    
    if(!txtWarehouseID.match(/\S/)) {
    	$('#txtWarehouseID').focus();
    	$('#dvWarehouseID').addClass('has-error');
    	$('#mrkWarehouseID').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/Inbound',	
        type:'POST',
        data:{"key":lParambtn,"txtDoorID":txtDoorID,"txtStagingAreaID":txtStagingAreaID,"txtDocNumber":txtDocNumber,
        	"txtDate":txtDate,"txtVendorID":txtVendorID, "txtPlantID":txtPlantID, "txtWarehouseID":txtWarehouseID,
        	"txtInboundWMSStatus":txtInboundWMSStatus,"txtInboundUpdateWMFirst":txtInboundUpdateWMFirst},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertInbound')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan dokumen masuk barang";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/Inbound';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<script>
$(document).ready(function(){

//get warehouse from plant id
    $(document).on('click', '#txtWarehouseID', function(e){
  
     e.preventDefault();
  
	//var plantid = $(this).data('id'); // get id of clicked row
		var plantid = document.getElementById('txtPlantID').value;
  
     $('#dynamic-content').html(''); // leave this div blank
	//$('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getwarehouse',
          type: 'POST',
          data: 'plantid='+plantid,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content').html(''); // blank before load.
          $('#dynamic-content').html(data); // load here
		//$('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
		//$('#modal-loader').hide();
     });
    });
    
    
//get door from plant and warehouse id
    $(document).on('click', '#txtDoorID', function(e){
	     e.preventDefault();
	  
		 var plantid = document.getElementById('txtPlantID').value;
		 var warehouseid = document.getElementById('txtWarehouseID').value;
	  
	     $('#dynamic-content-door').html(''); // leave this div blank
		//$('#modal-loader').show();      // load ajax loader on button click
	 
	     $.ajax({
	          url: '${pageContext.request.contextPath}/getdoor',
	          type: 'POST',
	          data: {"plantid":plantid,"warehouseid":warehouseid},
	          dataType: 'html'
	     })
	     .done(function(data){
	          console.log(data); 
	          $('#dynamic-content-door').html(''); // blank before load.
	          $('#dynamic-content-door').html(data); // load here
			  //$('#modal-loader').hide(); // hide loader  
	     })
	     .fail(function(){
	          $('#dynamic-content-door').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			  //$('#modal-loader').hide();
	     });
	});
	
	
//get staging area from plant,warehouse and door id
    $(document).on('click', '#txtStagingAreaID', function(e){
	     e.preventDefault();
	  
		 var plantid = document.getElementById('txtPlantID').value;
		 var warehouseid = document.getElementById('txtWarehouseID').value;
		 var doorid = document.getElementById('txtDoorID').value;
	  
	     $('#dynamic-content-staging-area').html(''); // leave this div blank
		//$('#modal-loader').show();      // load ajax loader on button click
	 
	     $.ajax({
	          url: '${pageContext.request.contextPath}/getstagingarea',
	          type: 'POST',
	          data: {"plantid":plantid,"warehouseid":warehouseid,"doorid":doorid},
	          dataType: 'html'
	     })
	     .done(function(data){
	          console.log(data); 
	          $('#dynamic-content-staging-area').html(''); // blank before load.
	          $('#dynamic-content-staging-area').html(data); // load here
			  //$('#modal-loader').hide(); // hide loader  
	     })
	     .fail(function(){
	          $('#dynamic-content-staging-area').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			  //$('#modal-loader').hide();
	     });
	});
    
});
</script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtVendorID:focus').length) {
    	$('#txtVendorID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtPlantID:focus').length) {
    	$('#txtPlantID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtWarehouseID:focus').length) {
    	$('#txtWarehouseID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtDoorID:focus').length) {
    	$('#txtDoorID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtStagingAreaID:focus').length) {
    	$('#txtStagingAreaID').click();
    }
});
</script>

</body>

</html>