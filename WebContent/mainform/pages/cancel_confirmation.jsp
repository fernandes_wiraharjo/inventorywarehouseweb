<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Cancel Confirmation</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<!-- <style type="text/css">	 -->
<!-- #ModalUpdateInsert { overflow-y:scroll } -->
<!-- </style> -->
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="CancelConfirmation" name="CancelConfirmation" action = "${pageContext.request.contextPath}/CancelConfirmation" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Cancel Confirmation <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertCancelConfirmation'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses membatalkan konfirmasi produksi.
              				</div>
	      					</c:if>
	      					
	      					<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    											<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
							          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
							          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     						</div>
					     						
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	          								<div id="dvCancelConfirmationID" class="form-group">
	            								<label for="recipient-name" class="control-label">Cancel Confirmation ID</label><label id="mrkCancelConfirmationID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblCancelConfirmationName" name="lblCancelConfirmationName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtCancelConfirmationID" name="txtCancelConfirmationID" data-toggle="modal" data-target="#ModalGetMasterConfirmationDoc" readonly="readonly">
	          								</div>
	          								<div id="dvCancelConfirmationNo" class="form-group">
	            								<label for="message-text" class="control-label">Cancel Confirmation No</label><label id="mrkCancelConfirmationNo" for="recipient-name" class="control-label"><small>*</small></label>
	            								<input type="text" class="form-control" id="txtCancelConfirmationNo" name="txtCancelConfirmationNo" readonly="readonly" onfocus="FuncValCancelConfirmationNo()">
	          								</div>
	          								<div id="dvDate" class="form-group">
	            								<label for="message-text" class="control-label">Date</label><label id="mrkDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDate" name="txtDate" data-date-format="dd M yyyy">
	          								</div>
	          								<div id="dvRefDocNumber" class="form-group">
	            								<label for="message-text" class="control-label">Confirmation Document No</label><label id="mrkRefDocNumber" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtRefDocNumber" name="txtRefDocNumber" data-toggle="modal" data-target="#ModalGetConfirmation" readonly="readonly">
	          								
	          									<input type="hidden" id="temp_txtConfirmationID" name="temp_txtConfirmationID"/>
	          									<input type="hidden" id="temp_txtConfirmationNo" name="temp_txtConfirmationNo"/>
	          									<input type="hidden" id="temp_txtProductID" name="temp_txtProductID"/>
	          									<input type="hidden" id="temp_txtBatchNo" name="temp_txtBatchNo"/>	
	          									<input type="hidden" id="temp_txtQty" name="temp_txtQty"/>
	          									<input type="hidden" id="temp_txtUOM" name="temp_txtUOM"/>
	          									<input type="hidden" id="temp_txtQtyBase" name="temp_txtQtyBase"/>
	          									<input type="hidden" id="temp_txtBaseUOM" name="temp_txtBaseUOM"/>
	          								</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> 
        										type="button" class="btn btn-primary" id="btnSave" name="btnSave"
        										data-toggle="modal" 
												data-target="#ModalConfirmCancel">Save</button>
        									<button type="button" 
        										class="btn btn-default" 
        										data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
									
									<!--modal show master confirmation document data -->
										<div class="modal fade" id="ModalGetMasterConfirmationDoc" tabindex="-1" role="dialog" aria-labelledby="ModalLabelConfirmationID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelConfirmationID"> Master Confirmation Document Data</h4>	
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_confirmation_document" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Confirmation ID</th>
										                <th>Description</th>
										                <th>From</th>
										                <th>To</th>
										                <th>Type</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listConfirmationDoc}" var ="confirmationdoc">
												        <tr>
												        <td><c:out value="${confirmationdoc.confirmationID}"/></td>
												        <td><c:out value="${confirmationdoc.confirmationDesc}"/></td>
												        <td><c:out value="${confirmationdoc.rangeFrom}"/></td>
												        <td><c:out value="${confirmationdoc.rangeTo}"/></td>
												        <td><c:out value="${confirmationdoc.confirmationType}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassMasterConfirmation('<c:out value="${confirmationdoc.confirmationID}"/>','<c:out value="${confirmationdoc.confirmationDesc}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show master confirmation document data -->
										
										<!--modal show confirmation data -->
										<div class="modal fade bs-example-modal-lg" id="ModalGetConfirmation" tabindex="-1" role="dialog" aria-labelledby="ModalLabelConfirmation">
												<div class="modal-dialog modal-lg" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelConfirmation">Confirmation Data</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_confirmation" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Confirmation ID</th>
														<th>Confirmation No</th>
														<th>Date</th>
														<th>Order Type ID</th>
														<th>Order No</th>
														<th>Product</th>
														<th>Batch</th>
														<th>Qty</th>
														<th>UOM</th>
<!-- 														<th>Qty Base</th> -->
<!-- 														<th>Base UOM</th> -->
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listConfirmation}" var ="confirmation">
												        <tr>
												        <td><c:out value="${confirmation.confirmationID}" /></td>
														<td><c:out value="${confirmation.confirmationNo}" /></td>
														<td><c:out value="${confirmation.confirmationDate}" /></td>
														<td><c:out value="${confirmation.orderTypeID}" /></td>
														<td><c:out value="${confirmation.orderNo}" /></td>
														<td><c:out value="${confirmation.productID}" /></td>
														<td><c:out value="${confirmation.batchNo}" /></td>
														<td><c:out value="${confirmation.qty}" /></td>
														<td><c:out value="${confirmation.UOM}" /></td>
<%-- 														<td><c:out value="${confirmation.qtyBaseUOM}" /></td> --%>
<%-- 														<td><c:out value="${confirmation.baseUOM}" /></td> --%>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassConfirmation('<c:out value="${confirmation.confirmationID}"/>',
												        										  '<c:out value="${confirmation.confirmationNo}"/>',
												        										  '<c:out value="${confirmation.productID}"/>',
												        										  '<c:out value="${confirmation.batchNo}"/>',
												        										  '<c:out value="${confirmation.qty}"/>',
												        										  '<c:out value="${confirmation.UOM}"/>',
												        										  '<c:out value="${confirmation.qtyBaseUOM}"/>',
												        										  '<c:out value="${confirmation.baseUOM}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show confirmation data -->
										
										<!--modal show cancel confirmation detail data -->
										<div class="modal fade bs-example-modal-lg" id="ModalCancelConfirmationDetail" tabindex="-1" role="dialog" aria-labelledby="ModalLabelCancelConfirmationDetail">
												<div class="modal-dialog modal-lg" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelConfirmation">Cancel Confirmation Detail Data</h4>
			      											<br><br>
			      											<label id="cclconfirmationdocno"></label>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load cancel confirmation detail by cancel confirmation doc no will be load here -->                          
           										<div id="dynamic-content">
           										</div>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show cancel confirmation detail data -->
										
										<!-- modal Confirm Cancel -->
       									<div class="modal modal-danger" id="ModalConfirmCancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Cancel Confirmation</h4>
              										</div>
              									<div class="modal-body">
               	 									<p>Are you sure to cancel this confirmation production order ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button id="btnCancelConfirmationProductionOrder" 
								                		name="btnCancelConfirmationProductionOrder" 
								                		class="btn btn-outline"
								                		onclick="FuncValEmptyInput('save')">Cancel</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_cancel_confirmation" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Cancel Confirmation ID</th>
										<th>Cancel Confirmation No</th>
										<th>Date</th>
										<th>Confirmation ID</th>
										<th>Confirmation No</th>
										<th>Product</th>
										<th>Batch</th>
										<th>Quantity</th>
										<th>UOM</th>
										<th>Quantity Base</th>
										<th>Base UOM</th>
										<th style="width:1px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listCancelConfirmation}" var="cancelconfirmation">
										<tr>
											<td><c:out value="${cancelconfirmation.cancelConfirmationID}" /></td>
											<td><c:out value="${cancelconfirmation.cancelConfirmationNo}" /></td>
											<td><c:out value="${cancelconfirmation.date}" /></td>
											<td><c:out value="${cancelconfirmation.confirmationID}" /></td>
											<td><c:out value="${cancelconfirmation.confirmationNo}" /></td>
											<td><c:out value="${cancelconfirmation.productID}" /></td>
											<td><c:out value="${cancelconfirmation.batchNo}" /></td>
											<td><c:out value="${cancelconfirmation.qty}" /></td>
											<td><c:out value="${cancelconfirmation.UOM}" /></td>
											<td><c:out value="${cancelconfirmation.qtyBaseUOM}" /></td>
											<td><c:out value="${cancelconfirmation.baseUOM}" /></td>
											<td>
											<button type="button" id="btnCancelConfirmationDetail" name="btnCancelConfirmationDetail" 
												class="btn btn-default" data-toggle="modal" data-target="#ModalCancelConfirmationDetail"
												data-cancelconfirmationid='<c:out value="${cancelconfirmation.cancelConfirmationID}" />'
												data-cancelconfirmationno='<c:out value="${cancelconfirmation.cancelConfirmationNo}" />'>
												<i class="fa fa-list-ul"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>
	
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_confirmation").DataTable();
  		$("#tb_cancel_confirmation").DataTable(
  				{
 					 "scrollX": true
 				});
//   		$("#tb_cancel_confirmation_detail").DataTable();
  		$('#M007').addClass('active');
  		$('#M038').addClass('active');
  		
  		$("#dvErrorAlert").hide();
  	});
 	
 	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
	</script>
    
<script>
$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
	$("#dvErrorAlert").hide();
	
  var button = $(event.relatedTarget) // Button that triggered the modal
  
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  
  $('#txtCancelConfirmationID').focus();
  $('#txtCancelConfirmationID').click();
})
</script>

<script>
function FuncPassConfirmation(lParamConfirmationID,lParamConfirmationNo,lParamProductID,lParamBatchNo,lParamQty,lParamUOM,lParamQtyBase,lParamBaseUOM){
	
	$("#txtRefDocNumber").val(lParamConfirmationID + " - " + lParamConfirmationNo);
		$("#temp_txtConfirmationID").val(lParamConfirmationID);
		$("#temp_txtConfirmationNo").val(lParamConfirmationNo);
		$("#temp_txtProductID").val(lParamProductID);
		$("#temp_txtBatchNo").val(lParamBatchNo);
		$("#temp_txtQty").val(lParamQty);
		$("#temp_txtUOM").val(lParamUOM);
		$("#temp_txtQtyBase").val(lParamQtyBase);
		$("#temp_txtBaseUOM").val(lParamBaseUOM);
	
}

function FuncPassMasterConfirmation(lParamCancelConfirmationID,lParamCancelConfirmationName){
	if(lParamCancelConfirmationID)
		{
			$("#txtCancelConfirmationID").val(lParamCancelConfirmationID);
			document.getElementById("lblCancelConfirmationName").innerHTML = '(' + lParamCancelConfirmationName + ')';
		}
}
</script>

<script>
function FuncClear(){
	$('#mrkCancelConfirmationID').hide();
	$('#mrkCancelConfirmationNo').hide();
	$('#mrkDate').hide();
	$('#mrkRefDocNumber').hide();
	
	$('#dvCancelConfirmationID').removeClass('has-error');
	$('#dvCancelConfirmationNo').removeClass('has-error');
	$('#dvDate').removeClass('has-error');
	$('#dvRefDocNumber').removeClass('has-error');
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#txtCancelConfirmationID').val('');
	$('#txtCancelConfirmationNo').val('');
	$('#txtDate').val('');
	$('#txtRefDocNumber').val('');
	
	$("#temp_txtConfirmationID").val('');
	$("#temp_txtConfirmationNo").val('');
	$("#temp_txtProductID").val('');
	$("#temp_txtBatchNo").val('');
	$("#temp_txtQty").val('');
	$("#temp_txtUOM").val('');
	$("#temp_txtQtyBase").val('');
	$("#temp_txtBaseUOM").val('');
		
	$('#btnSave').show();
	document.getElementById("lblCancelConfirmationName").innerHTML = null;
	document.getElementById("lblTitleModal").innerHTML = "Add Cancel Confirmation";	

	FuncClear();
	
	$('#txtDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
		$('#txtDate').datepicker('setDate', new Date());
}

function FuncValCancelConfirmationNo(){	
	var txtCancelConfirmationID = document.getElementById('txtCancelConfirmationID').value;
	
	FuncClear();
	
	if(!txtCancelConfirmationID.match(/\S/)) {    	
    	$('#txtCancelConfirmationID').focus();
    	$('#dvCancelConfirmationID').addClass('has-error');
    	$('#mrkCancelConfirmationID').show();
    	
    	alert("Fill Cancel Confirmation ID First ...!!!");
        return false;
    } 
	
    return true;
	
}

// <!-- get cancel confirmation no from cancel confirmation id -->
$(document).ready(function(){
	
    $(document).on('click', '#txtCancelConfirmationNo', function(e){
    	
    		e.preventDefault();
    		
    		var paramCancelConfirmationID = document.getElementById('txtCancelConfirmationID').value;
     
			         $.ajax({
			              url: '${pageContext.request.contextPath}/getCancelConfirmationNo',
			              type: 'POST',
			              data: {cancelconfirmationid : paramCancelConfirmationID},
			              dataType: 'text'
			         })
			         .done(function(data){
			              console.log(data);
			              
			              //for text data type
			              $('#txtCancelConfirmationNo').val(''); // blank before load.
			              
			              if(data=='0')
			            	  {
				              	$('#dvCancelConfirmationNo').addClass('has-error');
				              	document.getElementById('mrkCancelConfirmationNo').innerHTML = ' &nbsp*Cancel Confirmation No has reached the maximum range';
				              	$('#mrkCancelConfirmationNo').show();
			            	  }
			              else
							  {
			            	  	$('#txtCancelConfirmationNo').val(data); //load
			            	  	FuncClear();
							  }
			         })
			         .fail(function(){
			        	 console.log('Service call failed!');
			         });
    });
});

function FuncValEmptyInput(lParambtn) {
	var txtCancelConfirmationID = document.getElementById('txtCancelConfirmationID').value;
	var txtCancelConfirmationNo = document.getElementById('txtCancelConfirmationNo').value;
	var txtDate = document.getElementById('txtDate').value;
	var txtRefDocNumber = document.getElementById('txtRefDocNumber').value;
	var temp_txtConfirmationID = document.getElementById('temp_txtConfirmationID').value;
	var temp_txtConfirmationNo = document.getElementById('temp_txtConfirmationNo').value;
	var temp_txtProductID = document.getElementById('temp_txtProductID').value;
	var temp_txtBatchNo = document.getElementById('temp_txtBatchNo').value;
	var temp_txtQty = document.getElementById('temp_txtQty').value;
	var temp_txtUOM = document.getElementById('temp_txtUOM').value;
	var temp_txtQtyBase = document.getElementById('temp_txtQtyBase').value;
	var temp_txtBaseUOM = document.getElementById('temp_txtBaseUOM').value;
	
	var dvCancelConfirmationID = document.getElementsByClassName('dvCancelConfirmationID');
	var dvCancelConfirmationNo = document.getElementsByClassName('dvCancelConfirmationNo');
	var dvDate = document.getElementsByClassName('dvDate');
	var dvRefDocNumber = document.getElementsByClassName('dvRefDocNumber');

    if(!txtCancelConfirmationID.match(/\S/)) {
    	$("#txtCancelConfirmationID").focus();
    	$('#dvCancelConfirmationID').addClass('has-error');
    	$('#mrkCancelConfirmationID').show();
        return false;
    } 
    
    if(!txtCancelConfirmationNo.match(/\S/)) {    	
    	$('#txtCancelConfirmationNo').focus();
    	$('#dvCancelConfirmationNo').addClass('has-error');
    	$('#mrkCancelConfirmationNo').show();
        return false;
    }
    
    if(!txtDate.match(/\S/)) {    	
    	$('#txtDate').focus();
    	$('#dvDate').addClass('has-error');
    	$('#mrkDate').show();
        return false;
    }
    
    if(!txtRefDocNumber.match(/\S/)) {
    	$('#txtRefDocNumber').focus();
    	$('#dvRefDocNumber').addClass('has-error');
    	$('#mrkRefDocNumber').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/CancelConfirmation',	
        type:'POST',
        data:{"key":lParambtn,"txtCancelConfirmationID":txtCancelConfirmationID,"txtCancelConfirmationNo":txtCancelConfirmationNo,
        	"txtDate":txtDate,"temp_txtConfirmationID":temp_txtConfirmationID,"temp_txtConfirmationNo":temp_txtConfirmationNo,
        	"temp_txtProductID":temp_txtProductID,"temp_txtBatchNo":temp_txtBatchNo,"temp_txtQty":temp_txtQty,
        	"temp_txtUOM":temp_txtUOM,"temp_txtQtyBase":temp_txtQtyBase,"temp_txtBaseUOM":temp_txtBaseUOM},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertCancelConfirmation')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan dokumen batal konfirmasi produksi";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/CancelConfirmation';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<!-- get cancel confirmation detail from cancel confirmation document no -->
<script>
$(document).ready(function(){

    $(document).on('click', '#btnCancelConfirmationDetail', function(e){
    	
     e.preventDefault();
  
     var cancelconfirmationid = $(this).data('cancelconfirmationid'); // get confirmation id of clicked row
     var cancelconfirmationno = $(this).data('cancelconfirmationno'); // get confirmation no of clicked row
     
     document.getElementById("cclconfirmationdocno").innerHTML = "Cancel Confirmation Id : " + cancelconfirmationid + "<br>" + "Cancel Confirmation No : " + cancelconfirmationno;
  
     $('#dynamic-content').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getcancelconfirmationdetail',
          type: 'POST',
          data: {"id":cancelconfirmationid,"no":cancelconfirmationno},
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content').html(''); // blank before load.
          $('#dynamic-content').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtCancelConfirmationNo:focus').length) {
    	$('#txtCancelConfirmationNo').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtRefDocNumber:focus').length) {
    	$('#txtRefDocNumber').click();
    }
});
</script>

</body>

</html>