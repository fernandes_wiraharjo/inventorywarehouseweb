<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Storage Section</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="StorageSection" name="StorageSection" action = "${pageContext.request.contextPath}/storagesection" method="post">
		<input type="hidden" name="temp_string" value="" />
	
		<div class="wrapper">	
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					Storage Section <small>tables</small>
				</h1>
				</section>
	
				<!-- Main content -->
				<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<c:if test="${condition == 'SuccessInsertStorageSection'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menambahkan storage section.
	           						</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessUpdateStorageSection'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses memperbaharui storage section.
	              					</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessDeleteStorageSection'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menghapus storage section.
	              					</div>
		      					</c:if>
		      					
		      					<c:if test="${condition == 'FailedDeleteStorageSection'}">
		      						<div class="alert alert-danger alert-dismissible">
		                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
		                				Gagal menghapus storage section. <c:out value="${conditionDescription}"/>.
	              					</div>
		     					</c:if>
								
								<!--modal update & Insert -->
								<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
	  										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				     						</div>
				     					
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
											</div>
		     								<div class="modal-body">
		         								<div id="dvPlantID" class="form-group">
		           								<label for="lblPlantID" class="control-label">Plant ID</label><label id="mrkPlantID" for="formrkPlantID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" 
		           								data-target="#ModalGetPlantID">
		         								</div>
		         								<div id="dvWarehouseID" class="form-group">
		           								<label for="lblWarehouseID" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="formrkWarehouseID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" 
		           								data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
		         								</div>
		         								<div id="dvStorageTypeID" class="form-group">
		           								<label for="lblStorageTypeID" class="control-label">Storage Type ID</label><label id="mrkStorageTypeID" for="formrkStorageTypeID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblStorageTypeName" name="lblStorageTypeName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtStorageTypeID" name="txtStorageTypeID" data-toggle="modal" 
		           								data-target="#ModalGetStorageTypeID" onfocus="FuncValPlantWarehouse()">
		         								</div>
		         								<div id="dvStorageSectionID" class="form-group">
		           								<label for="lblStorageSectionID" class="control-label">Storage Section ID</label><label id="mrkStorageSectionID" for="formrkStorageSectionID" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageSectionID" name="txtStorageSectionID" onchange="FuncChangeStorageSectionID()">
		         								</div>
		         								<div id="dvStorageSectionName" class="form-group">
		           								<label for="lblStorageSectionName" class="control-label">Storage Section Name</label><label id="mrkStorageSectionName" for="formrkStorageSectionName" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtStorageSectionName" name="txtStorageSectionName" onchange="FuncChangeStorageSectionName()">
		         								</div>	        								
		    								</div>
		    								<div class="modal-footer">
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
		      									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    								</div>
										</div>
									</div>
								</div>
								<!-- end of update insert modal -->
								
								<!--modal Delete -->
								<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
	 										<div class="modal-header">            											
	   										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	     										<span aria-hidden="true">&times;</span></button>
	   										<h4 class="modal-title">Alert Delete Storage Section</h4>
	 										</div>
		 									<div class="modal-body">
			 									<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"  />
			 									<input type="hidden" id="temp_txtWarehouseID" name="temp_txtWarehouseID"  />
			 									<input type="hidden" id="temp_txtStorageTypeID" name="temp_txtStorageTypeID"  />
			 									<input type="hidden" id="temp_txtStorageSectionID" name="temp_txtStorageSectionID"  />
			  	 									<p>Are you sure to delete this storage section ?</p>
		 									</div>
		 									<div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
							              	</div>
							            </div>
							            <!-- /.modal-content -->
									</div>
							          <!-- /.modal-dialog -->
						        </div>
						        <!-- /.modal -->   
							        
						        <!--modal show plant data -->
								<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
									<div class="modal-dialog" role="document">
	 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelPlantID">Data Plant</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<table id="tb_master_plant" class="table table-bordered table-hover">
											        <thead style="background-color: #d2d6de;">
											                <tr>
											                  <th>ID</th>
											                  <th>Name</th>
											                  <th>Description</th>
											                  <th style="width: 20px"></th>
											                </tr>
											        </thead>
											        <tbody>
												        <c:forEach items="${listPlant}" var ="plant">
													        <tr>
														        <td><c:out value="${plant.plantID}"/></td>
														        <td><c:out value="${plant.plantNm}"/></td>
														        <td><c:out value="${plant.desc}"/></td>
														        <td><button type="button" class="btn btn-primary"
														        			data-toggle="modal"
														        			onclick="FuncPassStringPlant('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
														        			data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
														        </td>
											        		</tr>
												        </c:forEach>
											        </tbody>
									        	</table>
		    								</div>
	   										<div class="modal-footer"></div>
	 										</div>
									</div>
								</div>
								<!-- /. end of modal show plant data -->
								
								<!--modal show warehouse data -->
										<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelWarehouseID">Data Warehouse</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content-warehouse">
           										</div>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show warehouse data -->
								
								<!--modal show storage type data -->
								<div class="modal fade" id="ModalGetStorageTypeID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageTypeID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelStorageTypeID">Data Storage Type</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<div id="dynamic-content">
		         									<!-- container StorageTypeID for table based on PlantID and WarehouseID -->
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show storage type data -->
								
								<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
								<table id="tb_storagesection" class="table table-bordered table-striped table-hover">
									<thead style="background-color: #d2d6de;">
										<tr>
											<th>Plant ID</th>
											<th>Warehouse ID</th>
											<th>Storage Type ID</th>
											<th>Storage Section ID</th>
											<th>Storage Section Name</th>
											<th style="width:60px;"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listStorageSection}" var="storagesection">
											<tr>
												<td><c:out value="${storagesection.plantID}" /></td>
												<td><c:out value="${storagesection.warehouseID}" /></td>
												<td><c:out value="${storagesection.storageTypeID}" /></td>
												<td><c:out value="${storagesection.storageSectionID}" /></td>
												<td><c:out value="${storagesection.storageSectionName}" /></td>
												<td><button <c:out value="${buttonstatus}"/>
															id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
															onclick="FuncButtonUpdate()"
															data-target="#ModalUpdateInsert" 
															data-lplantid='<c:out value="${storagesection.plantID}" />'
															data-lplantname='<c:out value="${storagesection.plantName}" />'
															data-lwarehouseid='<c:out value="${storagesection.warehouseID}" />'
															data-lwarehousename='<c:out value="${storagesection.warehouseName}" />'
															data-lstoragetypeid='<c:out value="${storagesection.storageTypeID}" />'
															data-lstoragetypename='<c:out value="${storagesection.storageTypeName}" />'
															data-lstoragesectionid='<c:out value="${storagesection.storageSectionID}" />'
															data-lstoragesectionname='<c:out value="${storagesection.storageSectionName}" />'>
															<i class="fa fa-edit"></i></button> 
													<button <c:out value="${buttonstatus}"/>
															id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" 
															data-toggle="modal" 
															data-target="#ModalDelete"
															data-lplantid='<c:out value="${storagesection.plantID}" />' 
															data-lwarehouseid='<c:out value="${storagesection.warehouseID}" />'
															data-lstoragesectionid='<c:out value="${storagesection.storageSectionID}" />' >
													<i class="fa fa-trash"></i>
													</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row --> 
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
	
			<%@ include file="/mainform/pages/master_footer.jsp"%>
	
		</div>
		<!-- ./wrapper -->
	</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
	 	$(function () {
	  		$("#tb_storagesection").DataTable(); 
	  		$("#tb_master_plant").DataTable();
	  		$('#M002').addClass('active');
	  		$('#M048').addClass('active');
	  		
	  		//shortcut for button 'new'
		    Mousetrap.bind('n', function() {
		    	FuncButtonNew(),
		    	$('#ModalUpdateInsert').modal('show')
		    	});
  		});

		$('#ModalDelete').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var lPlantID = button.data('lplantid');
			var lWarehouseID = button.data('lwarehouseid');
			var lStorageSectionID = button.data('lstoragesectionid');
			$("#temp_txtPlantID").val(lPlantID);
			$("#temp_txtWarehouseID").val(lWarehouseID);
			$("#temp_txtStorageSectionID").val(lStorageSectionID);
		})

		$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
			FuncClear();
			
	 		var button = $(event.relatedTarget);
	 		
	 		var lPlantID = button.data('lplantid');
	 		var lPlantName = button.data('lplantname');
	 		var lWarehouseID = button.data('lwarehouseid');
	 		var lWarehouseName = button.data('lwarehousename');
	 		var lStorageTypeID = button.data('lstoragetypeid');
	 		var lStorageTypeName = button.data('lstoragetypename');
	 		var lStorageSectionID = button.data('lstoragesectionid');
	 		var lStorageSectionName = button.data('lstoragesectionname');
	 		
	 		var modal = $(this);
	 		
	 		modal.find(".modal-body #txtPlantID").val(lPlantID);
	 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
	 		modal.find(".modal-body #txtStorageTypeID").val(lStorageTypeID);
	 		modal.find(".modal-body #txtStorageSectionID").val(lStorageSectionID);
	 		modal.find(".modal-body #txtStorageSectionName").val(lStorageSectionName);
	 		
	 		if(lPlantName != null)
	 			document.getElementById("lblPlantName").innerHTML = '(' + lPlantName + ')';
	 			
	 		if(lWarehouseName != null)
	 			document.getElementById("lblWarehouseName").innerHTML = '(' + lWarehouseName + ')';
	 			
	 		if(lStorageTypeName != null)
	 			document.getElementById("lblStorageTypeName").innerHTML = '(' + lStorageTypeName + ')';
	 		
	 		if(lPlantID == null || lPlantID == '')
	 			{
	 				$("#txtPlantID").focus(); 
	 				$("#txtPlantID").click();
	 			}
	 		else
	 			$("#txtStorageSectionName").focus();
		});
		
		function FuncClear(){
			$('#mrkPlantID').hide();
			$('#mrkWarehouseID').hide();
			$('#mrkStorageTypeID').hide();
			$('#mrkStorageSectionID').hide();
			$('#mrkStorageSectionName').hide();
			
			document.getElementById("lblPlantName").innerHTML = null;
			document.getElementById("lblWarehouseName").innerHTML = null;
			document.getElementById("lblStorageTypeName").innerHTML = null;
			
			$('#dvPlantID').removeClass('has-error');
			$('#dvWarehouseID').removeClass('has-error');
			$('#dvStorageTypeID').removeClass('has-error');
			$('#dvStorageSectionID').removeClass('has-error');
			$('#dvStorageSectionName').removeClass('has-error');
			
			$("#dvErrorAlert").hide();
		}
	
		function FuncButtonNew() {
			$('#btnSave').show();
			$('#btnUpdate').hide();
			document.getElementById("lblTitleModal").innerHTML = "Add Storage Section";
					
			$('#txtPlantID').prop('disabled', false);
			$('#txtWarehouseID').prop('disabled', false);
			$('#txtStorageTypeID').prop('disabled', false);
			$('#txtStorageSectionID').prop('disabled', false);
		}
		
		function FuncButtonUpdate() {
			$('#btnSave').hide();
			$('#btnUpdate').show();
			document.getElementById("lblTitleModal").innerHTML = 'Edit Storage Section';
					
			$('#txtPlantID').prop('disabled', true);
			$('#txtWarehouseID').prop('disabled', true);
			$('#txtStorageTypeID').prop('disabled', true);
			$('#txtStorageSectionID').prop('disabled', true);
		}
		
		function FuncPassStringPlant(lParamPlantID,lParamPlantName){
			$("#txtPlantID").val(lParamPlantID);
			document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
			$('#mrkPlantID').hide();
			$('#dvPlantID').removeClass('has-error');
		}
		
		function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
			$("#txtWarehouseID").val(lParamWarehouseID);
			document.getElementById("lblWarehouseName").innerHTML = '(' + lParamWarehouseName + ')';
			$('#mrkWarehouseID').hide();
			$('#dvWarehouseID').removeClass('has-error');
		}
		
		function FuncPassDynamicStorageTypeData(lParamID,lType,lParamName){
			$("#txtStorageTypeID").val(lParamID);
			document.getElementById("lblStorageTypeName").innerHTML = '(' + lParamName + ')';
			$('#mrkStorageTypeID').hide();
			$('#dvStorageTypeID').removeClass('has-error');
		}
		
		function FuncChangeStorageSectionID() {
			$('#mrkStorageSectionID').hide();
			$('#dvStorageSectionID').removeClass('has-error');
		}
		
		function FuncChangeStorageSectionName() {
			$('#mrkStorageSectionName').hide();
			$('#dvStorageSectionName').removeClass('has-error');
		}
		
		
		function FuncValPlant(){	
			var txtPlantID = document.getElementById('txtPlantID').value;
			
			if(!txtPlantID.match(/\S/)) {    	
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    } 
			
		    return true;	
		}
		
		function FuncValPlantWarehouse(){
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			
			if(!txtPlantID.match(/\S/)){
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    }else if(!txtWarehouseID.match(/\S/)){
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		    	
		    	alert("Fill Warehouse ID First ...!!!");
		        return false;
		    }
		    return true;
		}
		
		function FuncValEmptyInput(lParambtn) {
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			var txtStorageTypeID = document.getElementById('txtStorageTypeID').value;
			var txtStorageSectionID = document.getElementById('txtStorageSectionID').value;
			var txtStorageSectionName = document.getElementById('txtStorageSectionName').value;
			
		    if(!txtPlantID.match(/\S/)) {
		    	$("#txtPlantID").focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		        return false;
		    } 
		    
		    if(!txtWarehouseID.match(/\S/)) {    	
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		        return false;
		    }
		    
		    if(!txtStorageTypeID.match(/\S/)) {    	
		    	$('#txtStorageTypeID').focus();
		    	$('#dvStorageTypeID').addClass('has-error');
		    	$('#mrkStorageTypeID').show();
		        return false;
		    }
		    
		    if(!txtStorageSectionID.match(/\S/)) {
		    	$('#txtStorageSectionID').focus();
		    	$('#dvStorageSectionID').addClass('has-error');
		    	$('#mrkStorageSectionID').show();
		        return false;
		    } 
		    
		    if(!txtStorageSectionName.match(/\S/)) {
		    	$('#txtStorageSectionName').focus();
		    	$('#dvStorageSectionName').addClass('has-error');
		    	$('#mrkStorageSectionName').show();
		        return false;
		    } 
		    
		    jQuery.ajax({
		        url:'${pageContext.request.contextPath}/storagesection',	
		        type:'POST',
		        data:{"key":lParambtn,"txtPlantID":txtPlantID,"txtWarehouseID":txtWarehouseID,
		        	  "txtStorageTypeID":txtStorageTypeID,"txtStorageSectionID":txtStorageSectionID,"txtStorageSectionName":txtStorageSectionName},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	if(data.split("--")[0] == 'FailedInsertStorageSection')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan storage section";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtPlantID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else if(data.split("--")[0] == 'FailedUpdateStorageSection')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui storage section";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtStorageSectionID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else
		        	{
			        	var url = '${pageContext.request.contextPath}/storagesection';  
			        	$(location).attr('href', url);
		        	}
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    
		    return true;
		}
		
		
		//get storage type from plant and warehouse id
		$(document).ready(function(){
		
		    $(document).on('click', '#txtStorageTypeID', function(e){
		     e.preventDefault();
				var plantid = document.getElementById('txtPlantID').value;
				var warehouseid = document.getElementById('txtWarehouseID').value;
		     $('#dynamic-content').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getstoragetype',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid,"storagetypeid":'',"type":''},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content').html(''); // blank before load.
		          $('#dynamic-content').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		
		//get warehouse from plant id
		$(document).ready(function(){
	
		    $(document).on('click', '#txtWarehouseID', function(e){
		  
		     e.preventDefault();
		  
			 var plantid = document.getElementById('txtPlantID').value;
		  
		     $('#dynamic-content-warehouse').html(''); // leave this div blank
			//$('#modal-loader').show();      // load ajax loader on button click
		 
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getwarehouse',
		          type: 'POST',
		          data: 'plantid='+plantid,
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-warehouse').html(''); // blank before load.
		          $('#dynamic-content-warehouse').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-warehouse').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtWarehouseID:focus').length) {
		    	$('#txtWarehouseID').click();
		    }
		});
		
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtStorageTypeID:focus').length) {
		    	$('#txtStorageTypeID').click();
		    }
		});
	</script>

</body>
</html>