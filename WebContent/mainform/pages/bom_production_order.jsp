<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Production Order</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<!-- <style type="text/css">	 -->
<!-- #ModalUpdateInsert { overflow-y:scroll } -->
<!-- </style> -->
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="" name="" action = "${pageContext.request.contextPath}/BomProductionOrder" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Production Order <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertProductionOrder'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan production order.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateProductionOrder'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui production order.
              				</div>
	      					</c:if>
	     					
							<c:if test="${condition == 'SuccessRelease'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses rilis production order.
              				</div>
	      					</c:if>
	      					<c:if test="${condition == 'FailedRelease'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal rilis production order. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>
	      					
	      					<c:if test="${condition == 'SuccessCancel'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses batal production order.
              				</div>
	      					</c:if>
	      					<c:if test="${condition == 'FailedCancel'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal batal production order. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>
	     					
	      					
	      					<!--modal update & Insert -->
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    											<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
							          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
							          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     						</div>
					     						
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
										<%-- <input type="hidden" id="temp_docNo" name="temp_docNo" value='<c:out value = "${tempdocNo}"/>' /> --%>
	          								<div id="dvOrderTypeID" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Order Type ID</label><label id="mrkOrderTypeID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblOrderTypeName" name="lblOrderTypeName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtOrderTypeID" name="txtOrderTypeID" data-toggle="modal" data-target="#ModalGetOrderTypeID">
	          								</div>
	          								<div id="dvOrderNo" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Order No</label><label id="mrkOrderNo" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtOrderNo" name="txtOrderNo" readonly="readonly" onfocus="FuncValOrderNo()">
												<!-- type="number"  -->
	          								</div>
	          								<div id="dvProductID" class="form-group col-xs-6">
	            								<label for="recipient-name" class="control-label">Product ID</label><label id="mrkProductID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblProductName" name="lblProductName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtProductID" name="txtProductID" data-toggle="modal" data-target="#ModalGetProductID">
	          								</div>
	          								<div id="dvPlantID" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Plant ID</label><label id="mrkPlantID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" data-target="#ModalGetPlantID">
	          								</div>
	          								<div id="dvQty" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Quantity</label><label id="mrkQty" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="number" class="form-control" id="txtQty" name="txtQty">
	          								</div>
<!-- 	          								<div id="dvUOM" class="form-group col-xs-6"> -->
<!-- 	            								<label for="message-text" class="control-label">UOM</label><label id="mrkUOM" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<select id="slMasterUOM" name="slMasterUOM" class="form-control" onfocus="FuncValShowUOM()"> -->
<!-- 							                    </select> -->
<!-- 	          								</div> -->
	          								<div id="dvDate" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Start Date</label><label id="mrkDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDate" name="txtDate" data-date-format="dd M yyyy">
	          								</div>
	          								<div id="dvWarehouseID" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" data-target="#ModalGetWarehouseID" onfocus="FuncValWarehouse()">
	          								</div>
	          								
	          								<input type="hidden" id="temp_status" name="temp_status"/>
	          								
	          								<div class="row"></div>	 
	          								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      								</div>
    										</div>
  										</div>
									</div>
										
										<!--modal show order type data -->
										<div class="modal fade" id="ModalGetOrderTypeID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelOrderTypeID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelOrderTypeID"> Order Type Data</h4>	
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_ordertype" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Order Type</th>
										                <th>Description</th>
										                <th>From</th>
										                <th>To</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listOrderType}" var ="ordertype">
												        <tr>
												        <td><c:out value="${ordertype.orderTypeID}"/></td>
												        <td><c:out value="${ordertype.orderTypeDesc}"/></td>
												        <td><c:out value="${ordertype.from}"/></td>
												        <td><c:out value="${ordertype.to}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassOrderType('<c:out value="${ordertype.orderTypeID}"/>','<c:out value="${ordertype.orderTypeDesc}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show order type data -->
										
										<!--modal show plant data -->
										<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelPlantID"> Plant Data</h4>	
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_plant" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Plant ID</th>
										                <th>Plant Name</th>
										                <th>Description</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listPlant}" var ="plant">
												        <tr>
												        <td><c:out value="${plant.plantID}"/></td>
												        <td><c:out value="${plant.plantNm}"/></td>
												        <td><c:out value="${plant.desc}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassPlant('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->
										
										<!--modal show bom product data -->
										<div class="modal fade" id="ModalGetProductID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelProduct">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelProduct">Finish Good Data</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_product" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Product ID</th>
														<th>Product Name</th>
														<th>Desc</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listBOMProduct}" var ="bomproduct">
												        <tr>
												        <td><c:out value="${bomproduct.id}" /></td>
														<td><c:out value="${bomproduct.title_en}" /></td>
														<td><c:out value="${bomproduct.short_description_en}" /></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassProduct('<c:out value="${bomproduct.id}"/>','<c:out value="${bomproduct.title_en}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show bom product data -->
										
										<!--modal show warehouse data -->
										<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelWarehouseID">Warehouse Data</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content-warehouse">
           										</div>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show warehouse data -->
										
										<!--modal show bom production order detail data -->
										<div class="modal fade bs-example-modal-lg" id="ModalProductionOrderDetail" tabindex="-1" role="dialog" aria-labelledby="ModalLabelProductionOrderDetail">
												<div class="modal-dialog modal-lg" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelProductionOrder">Production Order Detail Data</h4>
			      											<br><br>
			      											<label id="production_order_key"></label>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load production order detail will be load here -->                          
           										<div id="dynamic-content-production-order-detail">
           										</div>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show production order detail data -->
										
										<!--modal Release -->
       									<div class="modal modal-success" id="ModalRelease" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Release Production Order</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtOrderTypeID" name="temp_txtOrderTypeID"  />
              									<input type="hidden" id="temp_txtOrderNo" name="temp_txtOrderNo"  />
               	 									<p>Are you sure to release this production order ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button id="btnReleaseProductionOrder" name="btnReleaseProductionOrder"  class="btn btn-outline" >Release</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								        
								        <!--modal Cancel -->
       									<div class="modal modal-danger" id="ModalCancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Cancel Production Order</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtOrderTypeID" name="temp_txtOrderTypeID"  />
              									<input type="hidden" id="temp_txtOrderNo" name="temp_txtOrderNo"  />
               	 									<p>Are you sure to cancel this production order ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button id="btnCancelProductionOrder" name="btnCancelProductionOrder"  class="btn btn-outline" >Cancel</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_production_order" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Order Type</th>
										<th style="display:none;">Name</th>
										<th>Order No</th>
										<th>Product ID</th>
										<th style="display:none;">Name</th>
										<th>Plant ID</th>
										<th style="display:none;">Name</th>
										<th>Qty</th>
										<th>UOM</th>
										<th>Qty Base UOM</th>
										<th>Base UOM</th>
										<th>Start Date</th>
										<th>Warehouse ID</th>
										<th style="display:none;">Name</th>
										<th>Status</th>
										<th style="width:145px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listBOMProductionOrder}" var="production">
										<tr>
											<td><c:out value="${production.orderTypeID}" /></td>
											<td style="display:none;"><c:out value="${production.orderTypeDesc}" /></td>
											<td><c:out value="${production.orderNo}" /></td>
											<td><c:out value="${production.productID}" /></td>
											<td style="display:none;"><c:out value="${production.productName}" /></td>
											<td><c:out value="${production.plantID}" /></td>
											<td style="display:none;"><c:out value="${production.plantName}" /></td>
											<td><c:out value="${production.qty}" /></td>
											<td><c:out value="${production.UOM}" /></td>
											<td><c:out value="${production.qtyBaseUOM}" /></td>
											<td><c:out value="${production.baseUOM}" /></td>
											<td><c:out value="${production.startDate}" /></td>
											<td><c:out value="${production.warehouseID}" /></td>
											<td style="display:none;"><c:out value="${production.warehouseName}" /></td>
											<td><c:out value="${production.status}" /></td>
											<td>
											<button <c:out value="${buttonstatus}"/> 
												id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info <c:out value="${production.buttonStatus}" />" data-toggle="modal" 
												onclick="FuncButtonUpdate()"
												title="edit"
												<c:out value="${production.buttonStatus}" />
												data-target="#ModalUpdateInsert" 
												data-lordertypeid='<c:out value="${production.orderTypeID}" />'
												data-lordertypedesc='<c:out value="${production.orderTypeDesc}" />'
												data-lorderno='<c:out value="${production.orderNo}" />'
												data-lproductid='<c:out value="${production.productID}" />'
												data-lproductnm='<c:out value="${production.productName}" />'
												data-lplantid='<c:out value="${production.plantID}" />'
												data-lplantnm='<c:out value="${production.plantName}" />'
												data-lqty='<c:out value="${production.qty}" />'
												data-luom='<c:out value="${production.UOM}" />'
												data-lstartdate='<c:out value="${production.startDate}" />'
												data-lwarehouseid='<c:out value="${production.warehouseID}" />'
												data-lwarehousenm='<c:out value="${production.warehouseName}" />'
												data-lstatus='<c:out value="${production.status}" />'
												>
												<i class="fa fa-edit"></i>
												</button>
												
												<button type="button" id="btnProductionOrderDetail" name="btnProductionOrderDetail" 
												class="btn btn-default" title="details" data-toggle="modal" data-target="#ModalProductionOrderDetail"
												data-ordertype='<c:out value="${production.orderTypeID}" />'
												data-orderno='<c:out value="${production.orderNo}" />'
												data-lproductid='<c:out value="${production.productID}" />'
												data-lplantid='<c:out value="${production.plantID}" />'
												data-lqty='<c:out value="${production.qty}" />'
												data-luom='<c:out value="${production.UOM}" />'
												data-lqtybase='<c:out value="${production.qtyBaseUOM}" />'
												data-lbaseuom='<c:out value="${production.baseUOM}" />'
												data-lstartdate='<c:out value="${production.startDate}" />'
												data-lwarehouseid='<c:out value="${production.warehouseID}" />'
												data-lstatus='<c:out value="${production.status}" />'
												>
												<i class="fa fa-list-ul"></i>
												</button>
												
												<button <c:out value="${buttonstatus}"/>
												type="button" id="btnRelease" name="btnRelease" 
												class="btn btn-success <c:out value="${production.buttonStatus}" />"
												title="release"
												data-toggle="modal" 
												data-target="#ModalRelease"
												<c:out value="${production.buttonStatus}" />
												data-ordertype='<c:out value="${production.orderTypeID}" />'
												data-orderno='<c:out value="${production.orderNo}" />'>
												<i class="fa fa-share"></i>
												</button>
												
												<button <c:out value="${buttonstatus}"/>
												type="button" id="btnCancel" name="btnCancel" 
												class="btn btn-danger <c:out value="${production.buttonCancelStatus}" />"
												title="cancel"
												data-toggle="modal" 
												data-target="#ModalCancel"
												<c:out value="${production.buttonCancelStatus}" />
												data-ordertype='<c:out value="${production.orderTypeID}" />'
												data-orderno='<c:out value="${production.orderNo}" />'>
												<i class="fa fa-ban"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>
	
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_master_ordertype").DataTable();
  		$("#tb_product").DataTable();
  		$("#tb_master_plant").DataTable();
  		$("#tb_production_order").DataTable();
  		$('#M007').addClass('active');
  		$('#M036').addClass('active');
  		
  		$('#txtDate').datepicker({
  	      format: 'dd M yyyy',
  	      autoclose: true
  	    });
  		$('#txtDate').datepicker('setDate', new Date());
  		
  		$("#dvErrorAlert").hide();
  	});
 	
 	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
	</script>
    
<script>
$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
	$("#dvErrorAlert").hide();
	
  var button = $(event.relatedTarget) // Button that triggered the modal
  
  var lordertypeid = button.data('lordertypeid') // Extract info from data-* attributes
  var lordertypedesc = button.data('lordertypedesc') // Extract info from data-* attributes
  var lorderno = button.data('lorderno') // Extract info from data-* attributes
  var lproductid = button.data('lproductid') // Extract info from data-* attributes
  var lproductnm = button.data('lproductnm') // Extract info from data-* attributes
  var lplantid = button.data('lplantid') // Extract info from data-* attributes
  var lplantnm = button.data('lplantnm') // Extract info from data-* attributes
  var lqty = button.data('lqty') // Extract info from data-* attributes
//   var luom = button.data('luom') // Extract info from data-* attributes
  var lstartdate = button.data('lstartdate') // Extract info from data-* attributes
  var lwarehouseid = button.data('lwarehouseid') // Extract info from data-* attributes
  var lwarehousenm = button.data('lwarehousenm') // Extract info from data-* attributes
  var lstatus = button.data('lstatus') // Extract info from data-* attributes
  
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  
  modal.find('.modal-body #txtOrderTypeID').val(lordertypeid)
  if(lordertypedesc == undefined)
	  document.getElementById("lblOrderTypeName").innerHTML = null;
  else
  	  document.getElementById("lblOrderTypeName").innerHTML = '(' + lordertypedesc + ')'
  
  modal.find('.modal-body #txtOrderNo').val(lorderno)
  
  modal.find('.modal-body #txtProductID').val(lproductid)
  if(lproductnm == undefined)
	  document.getElementById("lblProductName").innerHTML = null;
  else
  	  document.getElementById("lblProductName").innerHTML = '(' + lproductnm + ')'
  
  modal.find('.modal-body #txtPlantID').val(lplantid)	  
  if(lplantnm == undefined)
	  document.getElementById("lblPlantName").innerHTML = null;
  else
	  document.getElementById("lblPlantName").innerHTML = '(' + lplantnm + ')'
  
  modal.find('.modal-body #txtQty').val(lqty)
//modal.find('.modal-body #slMasterUOM').val(luom)
  if(lstartdate!=undefined)
  	modal.find('.modal-body #txtDate').val(lstartdate)
  
  modal.find('.modal-body #txtWarehouseID').val(lwarehouseid)
  if(lwarehousenm == undefined)
	  document.getElementById("lblWarehouseName").innerHTML = null;
  else
	  document.getElementById("lblWarehouseName").innerHTML = '(' + lwarehousenm + ')'
	  
  modal.find('.modal-body #temp_status').val(lstatus)
  
  if(lordertypeid == null || lordertypeid == '')
 			{
 				$("#txtOrderTypeID").focus(); 
 				$("#txtOrderTypeID").click();
 			}
 		else
 			{
 				$("#txtProductID").focus();
 				$("#txtProductID").click();
 			}
  
//   $('#slMasterUOM').find('option').remove();
})

// $('#ModalGetProductID').on('shown.bs.modal', function (event) {
// 	$('#slMasterUOM').find('option').remove();
// 	});
		
$('#ModalRelease').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var lOrderTypeID = button.data('ordertype');
	var lOrderNo = button.data('orderno');
	$("#temp_txtOrderTypeID").val(lOrderTypeID);
	$("#temp_txtOrderNo").val(lOrderNo);
})

$('#ModalCancel').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var lOrderTypeID = button.data('ordertype');
	var lOrderNo = button.data('orderno');
	$("#temp_txtOrderTypeID").val(lOrderTypeID);
	$("#temp_txtOrderNo").val(lOrderNo);
})
</script>

<script>
function FuncPassOrderType(lParamOrderTypeID,lParamOrderTypeName){
	if(lParamOrderTypeID)
		{
			$("#txtOrderTypeID").val(lParamOrderTypeID);
			document.getElementById("lblOrderTypeName").innerHTML = '(' + lParamOrderTypeName + ')';
		}
}

function FuncPassProduct(lParamProductID,lParamProductName){
	if(lParamProductID)
		{
			$("#txtProductID").val(lParamProductID);
			document.getElementById("lblProductName").innerHTML = '(' + lParamProductName + ')';
		}
}

function FuncPassPlant(lParamPlantID,lParamPlantName){
	if(lParamPlantID)
		{
			$("#txtPlantID").val(lParamPlantID);
			document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
			$("#txtWarehouseID").val('');
			document.getElementById("lblWarehouseName").innerHTML = null;
		}
}

function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
	if(lParamWarehouseID)
		{
			$("#txtWarehouseID").val(lParamWarehouseID);
			document.getElementById("lblWarehouseName").innerHTML = '(' + lParamWarehouseName + ')';
		}
}
</script>

<script>
function FuncClear(){
	$('#mrkOrderTypeID').hide();
	$('#mrkOrderNo').hide();
	$('#mrkProductID').hide();
	$('#mrkPlantID').hide();
	$('#mrkQty').hide();
// 	$('#mrkUOM').hide();
	$('#mrkDate').hide();
	$('#mrkWarehouseID').hide();
	
	$('#dvOrderTypeID').removeClass('has-error');
	$('#dvOrderNo').removeClass('has-error');
	$('#dvProductID').removeClass('has-error');
	$('#dvPlantID').removeClass('has-error');
	$('#dvQty').removeClass('has-error');
// 	$('#dvUOM').removeClass('has-error');
	$('#dvDate').removeClass('has-error');
	$('#dvWarehouseID').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#txtOrderTypeID').val('');
	$('#txtOrderNo').val('');
	$('#txtProductID').val('');
	$('#txtPlantID').val('');
	$('#txtQty').val('');
// 	$('#slMasterUOM').val('');
// 	$('#txtDate').val('');
	$('#txtWarehouseID').val('');
	
	$('#btnSave').show();
	$('#btnUpdate').hide();
	
	document.getElementById("lblTitleModal").innerHTML = "Add Production Order";
	$('#txtOrderTypeID').prop('disabled', false);
	$('#txtOrderNo').prop('disabled', false);

	FuncClear();
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	
	document.getElementById("lblTitleModal").innerHTML = 'Edit Production Order';

	FuncClear();
	
	$('#txtOrderTypeID').prop('disabled', true);
	$('#txtOrderNo').prop('disabled', true);
}


function FuncValOrderNo(){	
	var txtOrderTypeID = document.getElementById('txtOrderTypeID').value;
	
	FuncClear();
	
	if(!txtOrderTypeID.match(/\S/)) {    	
    	$('#txtOrderTypeID').focus();
    	$('#dvOrderTypeID').addClass('has-error');
    	$('#mrkOrderTypeID').show();
    	
    	alert("Fill Order Type ID First ...!!!");
        return false;
    } 
	
    return true;
	
}

// function FuncValShowUOM(){	
// 	var txtProductID = document.getElementById('txtProductID').value;

// 	FuncClear();

// 	if(!txtProductID.match(/\S/)) {    	
// 	    	$('#txtProductID').focus();
// 	    	$('#dvProductID').addClass('has-error');
// 	    	$('#mrkProductID').show();
	    	
// 	    	alert("Fill Product ID First ...!!!");
// 	        return false;
// 	    } 
// 	    return true;
// 	}
	
function FuncValWarehouse(){	
	var txtPlantID = document.getElementById('txtPlantID').value;
	
	FuncClear();
	
	if(!txtPlantID.match(/\S/)) {    	
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
    	
    	alert("Fill Plant ID First ...!!!");
        return false;
    }
}

function FuncValEmptyInput(lParambtn) {
	var txtOrderTypeID = document.getElementById('txtOrderTypeID').value;
	var txtOrderNo = document.getElementById('txtOrderNo').value;
	var txtProductID = document.getElementById('txtProductID').value;
	var txtPlantID = document.getElementById('txtPlantID').value;
	var txtQty = document.getElementById('txtQty').value;
// 	var txtUOM = document.getElementById('slMasterUOM').value;
	var txtDate = document.getElementById('txtDate').value;
	var txtWarehouseID = document.getElementById('txtWarehouseID').value;
	var txt_tempStatus = document.getElementById('temp_status').value;
	
	var dvOrderTypeID = document.getElementsByClassName('dvOrderTypeID');
	var dvOrderNo = document.getElementsByClassName('dvOrderNo');
	var dvProductID = document.getElementsByClassName('dvProductID');
	var dvPlantID = document.getElementsByClassName('dvPlantID');
	var dvQty = document.getElementsByClassName('dvQty');
// 	var dvUOM = document.getElementsByClassName('dvUOM');
	var dvDate = document.getElementsByClassName('dvDate');
	var dvWarehouseID = document.getElementsByClassName('dvWarehouseID');

    if(!txtOrderTypeID.match(/\S/)) {
    	$("#txtOrderTypeID").focus();
    	$('#dvOrderTypeID').addClass('has-error');
    	$('#mrkOrderTypeID').show();
        return false;
    } 
    
    if(!txtOrderNo.match(/\S/)) {    	
    	$('#txtOrderNo').focus();
    	$('#dvOrderNo').addClass('has-error');
    	document.getElementById('mrkTo').innerHTML = '*';
    	$('#mrkOrderNo').show();
        return false;
    }
    
    if(!txtProductID.match(/\S/)) {
    	$('#txtProductID').focus();
    	$('#dvProductID').addClass('has-error');
    	$('#mrkProductID').show();
        return false;
    } 
    
    if(!txtPlantID.match(/\S/)) {
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
        return false;
    } 
    
    if(!txtQty.match(/\S/)) {
    	$('#txtQty').focus();
    	$('#dvQty').addClass('has-error');
    	$('#mrkQty').show();
        return false;
    }
    
//     if(!txtUOM.match(/\S/)) {
//     	$('#txtUOM').focus();
//     	$('#dvUOM').addClass('has-error');
//     	$('#mrkUOM').show();
//         return false;
//     } 
    
    if(!txtDate.match(/\S/)) {
    	$('#txtDate').focus();
    	$('#dvDate').addClass('has-error');
    	$('#mrkDate').show();
        return false;
    }
    
    if(!txtWarehouseID.match(/\S/)) {
    	$('#txtWarehouseID').focus();
    	$('#dvWarehouseID').addClass('has-error');
    	$('#mrkWarehouseID').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/BomProductionOrder',	
        type:'POST',
        data:{"key":lParambtn,"txtOrderTypeID":txtOrderTypeID,"txtOrderNo":txtOrderNo,"txtProductID":txtProductID,"txtPlantID":txtPlantID,"txtQty":txtQty,"txtDate":txtDate,"txtWarehouseID":txtWarehouseID,"txt_tempStatus":txt_tempStatus},
//         "txtUOM":txtUOM,
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedLoadBOM')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				//$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedConversion')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		//$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedInsertProductionOrder')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan production order";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtProductID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateProductionOrder')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui production order";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtProductID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/BomProductionOrder';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<!-- get order no from order type -->
<script>
$(document).ready(function(){
	
    $(document).on('click', '#txtOrderNo', function(e){
    	
    		e.preventDefault();
    		
    		var paramOrderType = document.getElementById('txtOrderTypeID').value;
     
			         $.ajax({
			              url: '${pageContext.request.contextPath}/getOrderNo',
			              type: 'POST',
			              data: {ordertypeid : paramOrderType},
			              dataType: 'text'
			         })
			         .done(function(data){
			              console.log(data);
			              
			              //for text data type
			              $('#txtOrderNo').val(''); // blank before load.
			              
			              if(data=='0')
			            	  {
				              	$('#dvOrderNo').addClass('has-error');
				              	document.getElementById('mrkOrderNo').innerHTML = ' &nbsp*Order No has reached the maximum range';
				              	$('#mrkOrderNo').show();
			            	  }
			              else
							  {
			            	  	$('#txtOrderNo').val(data); //load
			            	  	FuncClear();
							  }
			         })
			         .fail(function(){
			        	 console.log('Service call failed!');
			         });
    });
});
</script>

<!-- get warehouse from plant id -->
<script>
$(document).ready(function(){

    $(document).on('click', '#txtWarehouseID', function(e){
  
     e.preventDefault();
  
//      var plantid = $(this).data('id'); // get id of clicked row
		var plantid = document.getElementById('txtPlantID').value;
  
     $('#dynamic-content-warehouse').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getwarehouse',
          type: 'POST',
          data: 'plantid='+plantid,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content-warehouse').html(''); // blank before load.
          $('#dynamic-content-warehouse').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content-warehouse').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<!-- get production order detail -->
<script>
$(document).ready(function(){

    $(document).on('click', '#btnProductionOrderDetail', function(e){
    	
     e.preventDefault();
  
     var ordertype = $(this).data('ordertype'); // get order type id of clicked row
     var orderno = $(this).data('orderno'); // get order no of clicked row
     var productid = $(this).data('lproductid');
	 var plantid = $(this).data('lplantid');
	 var qty = $(this).data('lqty');
	 var uom = $(this).data('luom');
	 var qtybase = $(this).data('lqtybase');
	 var baseuom = $(this).data('lbaseuom');
	 var startdate = $(this).data('lstartdate');
	 var warehouseid = $(this).data('lwarehouseid');
	 var status = $(this).data('lstatus');
     
     document.getElementById("production_order_key").innerHTML = "Order Type : " + ordertype + "&nbsp&nbsp&nbsp&nbsp" + "Order No : " + orderno
     						+ "&nbsp&nbsp&nbsp&nbsp" + "Product ID : " + productid + "&nbsp&nbsp&nbsp&nbsp" + "Plant ID : " + plantid
     						+ "&nbsp&nbsp&nbsp&nbsp" + "Qty : " + qty + "&nbsp&nbsp&nbsp&nbsp" + "UOM : " + uom + "&nbsp&nbsp&nbsp&nbsp" + "Qty Base UOM : " + qtybase
     						+ "&nbsp&nbsp&nbsp&nbsp" + "Base UOM : " + baseuom + "&nbsp&nbsp&nbsp&nbsp" + "<br> Start Date : " + startdate + "&nbsp&nbsp&nbsp&nbsp" + "Warehouse ID : "
     						+ warehouseid + "&nbsp&nbsp&nbsp&nbsp" + "Status : " + status;
  
     $('#dynamic-content-production-order-detail').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getproductionorderdetail',
          type: 'POST',
          data: {"orderType":ordertype,"orderNo":orderno},
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content-production-order-detail').html(''); // blank before load.
          $('#dynamic-content-production-order-detail').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content-production-order-detail').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<!-- 'RELEASED' production order data -->
<script>
$(document).ready(function(){

    $(document).on('click', '#btnReleaseProductionOrder', function(e){
    	
     e.preventDefault();
  
//      var ordertype = $(this).data('ordertype'); // get order type id of clicked row
//      var orderno = $(this).data('orderno'); // get order no of clicked row
		var ordertype = document.getElementById('temp_txtOrderTypeID').value;
		var orderno = document.getElementById('temp_txtOrderNo').value;
     
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/ReleaseProductionOrder',
          type: 'POST',
          data: {"orderType":ordertype,"orderNo":orderno},
          dataType: 'text'
     })
     .done(function(data){
          console.log(data); 
          var url = '${pageContext.request.contextPath}/ReleaseProductionOrder';  
      	  $(location).attr('href', url);

//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
    	 console.log('Service call failed!');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<!-- 'CANCELLED' production order data -->
<script>
$(document).ready(function(){

    $(document).on('click', '#btnCancelProductionOrder', function(e){
    	
     e.preventDefault();
  
		var ordertype = document.getElementById('temp_txtOrderTypeID').value;
		var orderno = document.getElementById('temp_txtOrderNo').value;
     
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/CancelProductionOrder',
          type: 'POST',
          data: {"orderType":ordertype,"orderNo":orderno},
          dataType: 'text'
     })
     .done(function(data){
          console.log(data); 
          var url = '${pageContext.request.contextPath}/BomProductionOrder';  
      	  $(location).attr('href', url);

//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
    	 console.log('Service call failed!');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtOrderNo:focus').length) {
    	$('#txtOrderNo').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtProductID:focus').length) {
    	$('#txtProductID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtPlantID:focus').length) {
    	$('#txtPlantID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtWarehouseID:focus').length) {
    	$('#txtWarehouseID').click();
    }
});
</script>

</body>

</html>