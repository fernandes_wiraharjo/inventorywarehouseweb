<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Warehouse</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="Warehouse" name="Warehouse" action = "${pageContext.request.contextPath}/warehouse" method="post">
<input type="hidden" name="temp_string" value="" />

	<div class="wrapper">
	
		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Warehouse <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertWarehouse'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan gudang.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateWarehouse'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui gudang.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessDeleteWarehouse'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menghapus gudang.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedDeleteWarehouse'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal menghapus gudang. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>
							
							<!--modal update & Insert -->
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvPlantID" class="form-group">
	            								<label for="lblPlantID" class="control-label">Plant ID</label><label id="mrkPlantID" for="formrkPlantID" class="control-label"><small>*</small></label>	
	            								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" 
	            								data-target="#ModalGetPlantID">
	            								
	          								</div>
	          								<div id="dvWarehouseID" class="form-group">
	            								<label for="lblWarehouseID" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="formrkWarehouseID" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID">
	          								</div>
	          								<div id="dvWarehouseName" class="form-group">
	            								<label for="lblWarehouseName" class="control-label">Warehouse Name</label><label id="mrkWarehouseName" for="formrkWarehouseName" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtWarehouseName" name="txtWarehouseName">
	          								</div>
	          								<div class="form-group">
	            								<label for="lblWarehouseDesc" class="control-label">Warehouse Desc</label>	
	            								<textarea class="form-control" id="desc-text" name="desc-text"></textarea>
	          								</div>
	          								<div id="dvWMStatus" class="form-group">
	            								<label for="message-text" class="control-label">WMS Status</label><label id="mrkWMStatus" class="control-label"><small>*</small></label>	
	            								<select id="slWMStatus" name="slWMStatus" class="form-control">
							                    <option>false</option>
							                    <option>true</option>
							                    </select>
	          								</div>
	          								<div id="dvUpdateWMFirst" class="form-group">
	            								<label for="message-text" class="control-label">Update WM First</label><label id="mrkUpdateWMFirst" class="control-label"><small>*</small></label>	
	            								<select id="slUpdateWMFirst" name="slUpdateWMFirst" class="form-control">
							                    <option>false</option>
							                    <option>true</option>
							                    </select>
	          								</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									<!-- end of update insert modal -->
									
									<!--modal Delete -->
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Warehouse</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"  />
              									<input type="hidden" id="temp_txtWarehouseID" name="temp_txtWarehouseID"  />
               	 									<p>Are you sure to delete this warehouse ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->   
								        
								        <!--modal show plant data -->
										<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelPlantID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_plant" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                  <th>ID</th>
										                  <th>Name</th>
										                  <th>Description</th>
										                  <th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listPlant}" var ="plant">
												        <tr>
												        <td><c:out value="${plant.plantID}"/></td>
												        <td><c:out value="${plant.plantNm}"/></td>
												        <td><c:out value="${plant.desc}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_warehouse" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Plant ID</th>
										<th>Warehouse ID</th>
										<th>Warehouse Name</th>
										<th>Warehouse Desc</th>
										<th>WMS Status</th>
										<th>Update WM First</th>
										<th style="width:60px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listwarehouse}" var="warehouse">
										<tr>
											<td><c:out value="${warehouse.plantID}" /></td>
											<td><c:out value="${warehouse.warehouseID}" /></td>
											<td><c:out value="${warehouse.warehouseName}" /></td>
											<td><c:out value="${warehouse.warehouseDesc}" /></td>
											<td><c:out value="${warehouse.WMS_Status}" /></td>
											<td><c:out value="${warehouse.updateWMFirst}" /></td>
											<td><button <c:out value="${buttonstatus}"/>
														id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert" 
														data-lplantid='<c:out value="${warehouse.plantID}" />'
														data-lplantname='<c:out value="${warehouse.plantName}" />'
														data-lwarehouseid='<c:out value="${warehouse.warehouseID}" />'
														data-lwarehousename='<c:out value="${warehouse.warehouseName}" />'
														data-lwarehousedesc='<c:out value="${warehouse.warehouseDesc}" />'
														data-lwmstatus='<c:out value="${warehouse.WMS_Status}" />'
														data-lupdatewmfirst='<c:out value="${warehouse.updateWMFirst}" />'
														>
														<i class="fa fa-edit"></i></button> 
												<button <c:out value="${buttonstatus}"/>
														id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" onclick="FuncButtonDelete()" 
														data-toggle="modal" 
														data-target="#ModalDelete"
														data-lplantid='<c:out value="${warehouse.plantID}" />' 
														data-lwarehouseid='<c:out value="${warehouse.warehouseID}" />'>
												<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
																
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_warehouse").DataTable(); 
  		$("#tb_master_plant").DataTable(); 
  		$('#M002').addClass('active');
  		$('#M017').addClass('active');
  		
  	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
  	
	    $("#dvErrorAlert").hide();
  	});
	</script>
	<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lPlantID = button.data('lplantid');
		var lWarehouseID = button.data('lwarehouseid');
		$("#temp_txtPlantID").val(lPlantID);
		$("#temp_txtWarehouseID").val(lWarehouseID);
	})
	</script>
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		
 		var lPlantID = button.data('lplantid');
 		var lWarehouseID = button.data('lwarehouseid');
 		var lWarehouseName = button.data('lwarehousename');
 		var lWarehouseDesc = button.data('lwarehousedesc');
 		var lPlantName = button.data('lplantname');
 		var lWMStatus = button.data('lwmstatus');
 		var lUpdateWMFirst = button.data('lupdatewmfirst');
 			if(lWMStatus==false)
 				lWMStatus = "false";
 			else if(lWMStatus==true)
 				lWMStatus = "true";
 			
 			if(lUpdateWMFirst==false)
 				lUpdateWMFirst = "false";
 			else if(lUpdateWMFirst==true)
 				lUpdateWMFirst = "true";	
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtPlantID").val(lPlantID);
 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
 		modal.find(".modal-body #txtWarehouseName").val(lWarehouseName)
 		modal.find(".modal-body #desc-text").val(lWarehouseDesc);
 		modal.find(".modal-body #slWMStatus").val(lWMStatus);
 		modal.find(".modal-body #slUpdateWMFirst").val(lUpdateWMFirst);
 		
 		if(lPlantName != null)
 			document.getElementById("lblPlantName").innerHTML = '(' + lPlantName + ')';
 		
 		if(lPlantID == null || lPlantID == '')
 			{
 				$("#txtPlantID").focus(); 
 				$("#txtPlantID").click();
 			}
 		else
 			$("#txtWarehouseName").focus();
	})
</script>



<script>

function FuncClear(){
	$('#mrkPlantID').hide();
	$('#mrkWarehouseID').hide();
	$('#mrkWarehouseName').hide();
	$('#mrkWMStatus').hide();
	$('#mrkUpdateWMFirst').hide();
	$('#txtPlantID').prop('disabled', false);
	$('#txtWarehouseID').prop('disabled', false);
	
	$('#dvPlantID').removeClass('has-error');
	$('#dvWarehouseID').removeClass('has-error');
	$('#dvWarehouseName').removeClass('has-error');
	$('#dvWMStatus').removeClass('has-error');
	$('#dvUpdateWMFirst').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Warehouse";	
	document.getElementById("lblPlantName").innerHTML = null;

	FuncClear();
	$('#txtPlantID').prop('disabled', false);
	$('#txtWarehouseID').prop('disabled', false);
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById("lblTitleModal").innerHTML = 'Edit Warehouse';

	FuncClear();
	$('#txtPlantID').prop('disabled', true);
	$('#txtWarehouseID').prop('disabled', true);
}

function FuncPassString(lParamPlantID,lParamPlantName){
	$("#txtPlantID").val(lParamPlantID);
	document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
}

function FuncValEmptyInput(lParambtn) {
	var txtPlantID = document.getElementById('txtPlantID').value;
	var txtWarehouseID = document.getElementById('txtWarehouseID').value;
	var txtWarehouseName = document.getElementById('txtWarehouseName').value;
	var txtDesc = document.getElementById('desc-text').value;
	var slWMStatus = document.getElementById('slWMStatus').value;
	var slUpdateWMFirst = document.getElementById('slUpdateWMFirst').value;
	

	var dvPlantID = document.getElementsByClassName('dvPlantID');
	var dvWarehouseID = document.getElementsByClassName('dvWarehouseID');
	var dvWarehouseName = document.getElementsByClassName('dvWarehouseName');
	
	if(lParambtn == 'save'){
		$('#txtPlantID').prop('disabled', false);
		$('#txtWarehouseID').prop('disabled', false);
		}
		else{
		$('#txtPlantID').prop('disabled', true);
		$('#txtWarehouseID').prop('disabled', true);
		}
	
    if(!txtPlantID.match(/\S/)) {
    	$("#txtPlantID").focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
        return false;
    } 
    
    if(!txtWarehouseID.match(/\S/)) {    	
    	$('#txtWarehouseID').focus();
    	$('#dvWarehouseID').addClass('has-error');
    	$('#mrkWarehouseID').show();
        return false;
    } 
    
    if(!txtWarehouseName.match(/\S/)) {
    	$('#txtWarehouseName').focus();
    	$('#dvWarehouseName').addClass('has-error');
    	$('#mrkWarehouseName').show();
        return false;
    } 
    
    if(!slWMStatus.match(/\S/)) {
    	$('#slWMStatus').focus();
    	$('#dvWMStatus').addClass('has-error');
    	$('#mrkWMStatus').show();
        return false;
    } 
    
    if(!slUpdateWMFirst.match(/\S/)) {
    	$('#slUpdateWMFirst').focus();
    	$('#dvUpdateWMFirst').addClass('has-error');
    	$('#mrkUpdateWMFirst').show();
        return false;
    } 
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/warehouse',	
        type:'POST',
        data:{"key":lParambtn,"txtPlantID":txtPlantID,"txtWarehouseID":txtWarehouseID,
        		"txtWarehouseName":txtWarehouseName,"txtDesc":txtDesc,"slWMStatus":slWMStatus,"slUpdateWMFirst":slUpdateWMFirst},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertWarehouse')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan gudang";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtPlantID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateWarehouse')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui gudang";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtWarehouseID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/warehouse';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}

</script>


</body>
</html>