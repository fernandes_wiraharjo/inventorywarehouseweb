<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_storagesection" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
			<th>Storage Section</th>
			<th>Storage Section Name</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listStorageSection}" var ="storsection">
	  <tr>
		<td><c:out value="${storsection.storageSectionID}" /></td>
		<td><c:out value="${storsection.storageSectionName}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassStringStorageSection('<c:out value="${storsection.storageSectionID}"/>','<c:out value="${storsection.storageSectionName}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_storagesection").DataTable();
  	});
</script>