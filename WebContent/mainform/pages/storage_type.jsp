<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Storage Type</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="form_storage_type" name="form_storage_type" action = "${pageContext.request.contextPath}/storagetype" method="post">
<input type="hidden" name="temp_string" value="" />

	<div class="wrapper">
	
		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Storage Type
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertStorageType'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan tipe rak.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateStorageType'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui tipe rak.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessDeleteStorageType'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menghapus tipe rak.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedDeleteStorageType'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal menghapus tipe rak. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>
							
							<!--modal update & Insert -->
									<div class="modal fade bs-example-modal-lg" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog modal-lg" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvPlantID" class="form-group col-xs-3">
	            								<label for="lblPlantID" class="control-label">Plant</label><label id="mrkPlantID" for="formrkPlantID" class="control-label"><small>*</small></label>	
	            								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" readonly="true">
												<!-- <input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" data-target="#ModalGetPlantID"> -->
	          								</div>
	          								<div id="dvWarehouseID" class="form-group col-xs-3">
	            								<label for="lblWarehouseID" class="control-label">Warehouse</label><label id="mrkWarehouseID" for="formrkWarehouseID" class="control-label"><small>*</small></label>	
	            								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
	          								</div>
	          								<div id="dvStorageTypeID" class="form-group col-xs-3">
	            								<label for="lblStorageTypeID" class="control-label">Storage Type ID</label><label id="mrkStorageTypeID" for="formrkStorageTypeID" class="control-label"><small>*</small></label>
	            								<input type="text" class="form-control" id="txtStorageTypeID" name="txtStorageTypeID">
	          								</div>
	          								<div id="dvStorageTypeName" class="form-group col-xs-3">
	            								<label for="lblStorageTypeName" class="control-label">Storage Type Name</label><label id="mrkStorageTypeName" for="formrkStorageTypeName" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtStorageTypeName" name="txtStorageTypeName">
	          								</div>
	          								<div class="form-group col-xs-4">
	            								<label><input type="checkbox"
	            											  id="cbSUmgmt"
															  name="cbSUmgmt">
														      SU mgmt active
												</label>
											</div>
											<div class="form-group col-xs-4">
												<label><input type="checkbox"
														id="cbStortypeisidpnt"
														name="cbStortypeisidpnt">
														      Stor.type is ID pnt
												</label>
											</div>
											<div class="form-group col-xs-4">
												<label><input type="checkbox"
															  id="cbStortypeispckpnt"
															  name="cbStortypeispckpnt">
														      Stor.type is pck pnt
												</label>
	          								</div>
	          								
											<!-- Stock Placement Control -->
	          								<label for="recipient-name" class="control-label">Stock Placement Control</label>
			   								<div class="panel panel-primary" id="StockPlacementControlPanel">
			   								<div class="panel-body fixed-panel">
			   								
				   								<div id="dvPutAwayStrategy" class="form-group col-xs-4">
		            								<label class="control-label">Putaway strategy</label><label id="mrkPutAwayStrategy" class="control-label"><small>*</small></label>	
		            								<input type="text" class="form-control" id="txtPutAwayStrategy" name="txtPutAwayStrategy" data-toggle="modal" data-target="#ModalGetPutAwayStrategy">
		          								</div>
		          								<div class="form-group col-xs-8">
		            								<label class="control-label">Assigned ID point stor. type</label>
		            								<input type="text" class="form-control" id="txtAssignedIDPointStorType" name="txtAssignedIDPointStorType" data-toggle="modal" data-target="#ModalGetDynamicStorageType" onfocus="FuncValDynamicStorageType()" style="width:250px">
		          								</div>
		          								<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbStockplcmtreqconf"
															  name="cbStockplcmtreqconf">
														      Stock plcmt req confirm
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbDstbinchduringconf"
															  name="cbDstbinchduringconf">
														      Dst bin ch. during confirm
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbMixedStorage"
															  name="cbMixedStorage">
														      Mixed storage
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbAddntoStock"
															  name="cbAddntoStock">
														      Addn to stock
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbRetainOverdeliveries"
															  name="cbRetainOverdeliveries">
														      Retain overdeliveries
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbSUTCheckActive"
															  name="cbSUTCheckActive">
														      SUT check active
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbStorageSecCheckActive"
															  name="cbStorageSecCheckActive">
														      Storage sec. check active
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbBlockUponStockPlcmt"
															  name="cbBlockUponStockPlcmt">
														      Block upon stock plcmt
												</label>
												</div>
												
			   								</div>
											</div>
											<!-- /.Stock Placement Control -->
	        								
	        								<!-- Stock Removal Control -->
	          								<label for="recipient-name" class="control-label">Stock Removal Control</label>
			   								<div class="panel panel-primary" id="StockRemovalControlPanel">
			   								<div class="panel-body fixed-panel">
			   								
				   								<div id="dvStockRemovalStrategy" class="form-group col-xs-4">
		            								<label class="control-label">Stock removal strategy</label><label id="mrkStockRemovalStrategy" class="control-label"><small>*</small></label>	
		            								<input type="text" class="form-control" id="txtStockRemovalStrategy" name="txtStockRemovalStrategy" data-toggle="modal" data-target="#ModalGetStockRemovalStrategy">
		          								</div>
		          								<div class="form-group col-xs-4">
		            								<label class="control-label">Assigned pick point stor. ty.</label>
		            								<input type="text" class="form-control" id="txtAssignedPickPointStorType" name="txtAssignedPickPointStorType" data-toggle="modal" data-target="#ModalGetDynamicStorageType" onfocus="FuncValDynamicStorageType()" style="width:250px">
		          								</div>
		          								<div class="form-group col-xs-4">
		            								<label class="control-label">Return storage type</label>
		            								<input type="text" class="form-control" id="txtReturnStorageType" name="txtReturnStorageType" data-toggle="modal" data-target="#ModalGetDynamicStorageType" onfocus="FuncValDynamicStorageType()" style="width:250px">
		          								</div>
		          								<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbStockRmvlReqConf"
															  name="cbStockRmvlReqConf">
														      Stock rmvl req confirm
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbAllowNegativeStock"
															  name="cbAllowNegativeStock">
														      Allow negative stock
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbFullStkRmvlReqAct"
															  name="cbFullStkRmvlReqAct">
														      Full stk rmvl reqmt act.
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbReturnStockToSameStorageBin"
															  name="cbReturnStockToSameStorageBin">
														      Return to same stor bin
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbExecuteZeroStockCheck"
															  name="cbExecuteZeroStockCheck">
														      Execute zero stock check
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbRoundOffQty"
															  name="cbRoundOffQty">
														      Round off qty
												</label>
												</div>
												<div class="form-group col-xs-3">
	            								<label><input type="checkbox"
	            											  id="cbBlockUponStockRemoval"
															  name="cbBlockUponStockRemoval">
														      Block upon stock rmvl
												</label>
												</div>
												
			   								</div>
											</div>
											<!-- /.End Of Stock Removal Control -->
	        								
	        								<div class="row"></div>
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									<!-- end of update insert modal -->
									
									<!--modal Delete -->
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Storage Type</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_PlantID" name="temp_PlantID"  />
              									<input type="hidden" id="temp_WarehouseID" name="temp_WarehouseID"  />
              									<input type="hidden" id="temp_StorageTypeID" name="temp_StorageTypeID"  />
               	 									<p>Are you sure to delete this storage type ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								        
								        <!--modal show plant data -->
										<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelPlantID">Data Plant</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_plant" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                  <th>ID</th>
										                  <th>Name</th>
										                  <th>Description</th>
										                  <th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listPlant}" var ="plant">
												        <tr>
												        <td><c:out value="${plant.plantID}"/></td>
												        <td><c:out value="${plant.plantNm}"/></td>
												        <td><c:out value="${plant.desc}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show plant data -->
										
										<!--modal show warehouse data -->
										<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelWarehouseID">Data Warehouse</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content">
           										</div>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show warehouse data -->
										
										<!--modal show putaway strategy list data -->
										<div class="modal fade" id="ModalGetPutAwayStrategy" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPutAwayStrategy">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelPutAwayStrategy">Data Putaway Strategy</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_putaway_strategy" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                  <th>ID</th>
										                  <th>Name</th>
										                  <th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listPutawayStrategy}" var ="putawaystrategy">
												        <tr>
												        <td><c:out value="${putawaystrategy.putawayStrategy}"/></td>
												        <td><c:out value="${putawaystrategy.description}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassPutawayStrategyData('<c:out value="${putawaystrategy.putawayStrategy}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show putaway strategy list data -->
										
										<!--modal show stock removal strategy list data -->
										<div class="modal fade" id="ModalGetStockRemovalStrategy" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStockRemovalStrategy">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelStockRemovalStrategy">Data Stock Removal Strategy</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_stockremoval_strategy" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                  <th>ID</th>
										                  <th>Name</th>
										                  <th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listPickupStrategy}" var ="pickupstrategy">
												        <tr>
												        <td><c:out value="${pickupstrategy.stockRemovalStrategy}"/></td>
												        <td><c:out value="${pickupstrategy.description}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassPickupStrategyData('<c:out value="${pickupstrategy.stockRemovalStrategy}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show putaway strategy list data -->
										
										<!-- modal show dynamic storage type data -->
										<div class="modal fade" id="ModalGetDynamicStorageType" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStorageType">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelStorageType">Data Storage Type</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load storage type by plant and warehouse id will be load here -->                          
           										<div id="dynamic-storage-type">
           										</div>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show dynamic storage type data -->
							
								<div class="col-xs-8" class="form-horizontal" style="margin-top: 10px">
								<div id="dvslPlantId" class="form-group">
				         		<label for="inputEmail3" class="col-sm-2 control-label" style="Height: 34px;text-align:center; margin-top: 7px">Plant Id</label><label id="mrkslPlantId"></label>
			         			<div class="col-sm-10">
			
								<select id="slPlantId" name="slPlantId"
																	class="form-control select2" 
																	data-placeholder="Select Brand" style="width: 200px;">
																	<c:forEach items="${listPlant}" var="plant">
																		<option id="optplant" name="optplant"
																			value="<c:out value="${plant.plantID}" />"><c:out
																				value="${plant.plantID}" /> - <c:out
																			value="${plant.plantNm}" /></option>
																</c:forEach>
								</select>
								
								</div>
				       			</div>
				       			</div>
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br><br><br>
							<table id="tb_storagetype" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Plant ID</th>
										<th>Warehouse ID</th>
										<th>Storage Type ID</th>
										<th>Storage Type Name</th>
										<th style="width:60px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listStorageType}" var="storagetype">
										<tr>
											<td><c:out value="${storagetype.plantID}" /></td>
											<td><c:out value="${storagetype.warehouseID}" /></td>
											<td><c:out value="${storagetype.storageTypeID}" /></td>
											<td><c:out value="${storagetype.storageTypeName}" /></td>
											<td><button <c:out value="${buttonstatus}"/> 
														id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert"
														data-lplantid='<c:out value="${storagetype.plantID}" />'
														data-lplantname='<c:out value="${storagetype.plantName}" />'
														data-lwarehouseid='<c:out value="${storagetype.warehouseID}" />'
														data-lwarehousename='<c:out value="${storagetype.warehouseName}" />'
														data-lsumgmt='<c:out value="${storagetype.SU_mgmt_active}" />'
														data-lstortypeisidpnt='<c:out value="${storagetype.stor_type_is_ID_pnt}" />'
														data-lstortypeispckpnt='<c:out value="${storagetype.stor_type_is_pck_pnt}" />'
														data-lstoragetypeid='<c:out value="${storagetype.storageTypeID}" />'
														data-lstoragetypename='<c:out value="${storagetype.storageTypeName}" />'
														data-lputawaystrategy='<c:out value="${storagetype.putawayStrategy}" />'
														data-lassignedidpointstortype='<c:out value="${storagetype.assigned_ID_point_stor_type}" />'
														data-lstockplcmtreqconf='<c:out value="${storagetype.stock_plcmt_req_confirmation}" />'
														data-ldstbinchduringconf='<c:out value="${storagetype.dst_bin_ch_during_confirm}" />'
														data-lmixedstorage='<c:out value="${storagetype.mixedStorage}" />'
														data-laddntostock='<c:out value="${storagetype.addn_to_stock}" />'
														data-lretainoverdeliveries='<c:out value="${storagetype.retain_overdeliveries}" />'
														data-lsutcheckactive='<c:out value="${storagetype.SUT_check_active}" />'
														data-lstorageseccheckactive='<c:out value="${storagetype.storage_sec_check_active}" />'
														data-lblockuponstockplmt='<c:out value="${storagetype.block_upon_stock_plcmt}" />'
														data-lstockremovalstrategy='<c:out value="${storagetype.stockRemovalStrategy}" />'
														data-lassignedpickpointstortype='<c:out value="${storagetype.assigned_pick_point_stor_ty}" />'
														data-lreturnstortype='<c:out value="${storagetype.return_storage_type}" />'
														data-lstockrmvlreqconf='<c:out value="${storagetype.stock_rmvl_req_confirmation}" />'
														data-lallownegativestock='<c:out value="${storagetype.allow_negative_stock}" />'
														data-lfullstkrmvlreqact='<c:out value="${storagetype.full_stk_rmvl_reqmt_act}" />'
														data-lreturnstocktosamestorbin='<c:out value="${storagetype.return_stock_to_same_storage_bin}" />'
														data-lexecutezerostockcheck='<c:out value="${storagetype.execute_zero_stock_check}" />'
														data-lroundoffqty='<c:out value="${storagetype.round_off_qty}" />'
														data-lblockuponstockrmvl='<c:out value="${storagetype.block_upon_stock_rmvl}" />'
														>
														<i class="fa fa-edit"></i></button> 
												<button <c:out value="${buttonstatus}"/>
														id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" 
														data-toggle="modal" 
														data-target="#ModalDelete"
														data-lplantid='<c:out value="${storagetype.plantID}" />'
														data-lwarehouseid='<c:out value="${storagetype.warehouseID}" />'
														data-lstoragetypeid='<c:out value="${storagetype.storageTypeID}" />'>
												<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
																
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
 		$(".select2").select2();
  		$("#tb_storagetype").DataTable();
  		$("#tb_master_plant").DataTable();
  		$("#tb_putaway_strategy").DataTable();
  		$("#tb_stockremoval_strategy").DataTable();
  		$('#M002').addClass('active');
  		$('#M047').addClass('active');
  		
  		$('#slPlantId').val(<c:out value="${tempPlantID}"/>).trigger("change");
  		
  		//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
  	});
  	
  	$('#slPlantId').on("select2:select", function(e) {
	var slPlantId = $('#slPlantId').val();
	FuncLoadNumberRangesSetup(slPlantId);
	$('#slPlantId').val(slPlantId).trigger("change.select2");
	})
	
	function FuncLoadNumberRangesSetup(slPlantId) {
	var slPlantId_temp = slPlantId;
	jQuery.ajax({
        url:'${pageContext.request.contextPath}/storagetype',	
        type:'POST',
        data:{"key":"LoadNumberRanges","slPlantId":slPlantId},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	var url = '${pageContext.request.contextPath}/storagetype';  
        	$(location).attr('href', url);
        	$('#slPlantId').val(slPlantId_temp).trigger("change");
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    return true;
	}
	</script>
	
	<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lPlantID = button.data('lplantid');
		var lWarehouseID = button.data('lwarehouseid');
		var lStorageTypeID = button.data('lstoragetypeid');
		$("#temp_PlantID").val(lPlantID);
		$("#temp_WarehouseID").val(lWarehouseID);
		$("#temp_StorageTypeID").val(lStorageTypeID);
	})
	</script>
	
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		FuncClear();
		
 		var button = $(event.relatedTarget);
 		
 		var lPlantID = button.data('lplantid');
 		var lPlantName = button.data('lplantname');
 		var lWarehouseID = button.data('lwarehouseid');
 		var lWarehouseName = button.data('lwarehousename');
 		
 		var lSUmgmt = button.data('lsumgmt');
 		var lStorTypeIsIDPnt = button.data('lstortypeisidpnt');
 		var lStorTypeIsPckPnt = button.data('lstortypeispckpnt');
 		
 		var lStorageTypeID = button.data('lstoragetypeid');
 		var lStorageTypeName = button.data('lstoragetypename');
 		
 		var lPutawayStrategy = button.data('lputawaystrategy');
 		var lAssignedIDPointStorType = button.data('lassignedidpointstortype');
 		var lStockPlcmtReqConfirmation = button.data('lstockplcmtreqconf');
 		var lDstBinChDuringConfirm = button.data('ldstbinchduringconf');
 		var lMixedStorage = button.data('lmixedstorage');
 		var lAddnToStock = button.data('laddntostock');
 		var lRetainOverDeliveries = button.data('lretainoverdeliveries');
 		var lSUTCheckActive = button.data('lsutcheckactive');
 		var lStorageSecCheckActive = button.data('lstorageseccheckactive');
 		var lBlockUponStockPlcmt = button.data('lblockuponstockplmt');
 		
 		var lStockRemovalStrategy = button.data('lstockremovalstrategy');
 		var lAssignedPickPointStorType = button.data('lassignedpickpointstortype');
 		var lReturnStorageType = button.data('lreturnstortype');
 		var lStockRemovalReqConfirmation = button.data('lstockrmvlreqconf');
 		var lAllowNegativeStock = button.data('lallownegativestock');
 		var lFullStockRemovalReqAct = button.data('lfullstkrmvlreqact');
 		var lReturnStockToSameStorageBin = button.data('lreturnstocktosamestorbin');
 		var lExecuteZeroStockCheck = button.data('lexecutezerostockcheck');
 		var lRoundOffQty = button.data('lroundoffqty');
 		var lBlockUponStockRemoval = button.data('lblockuponstockrmvl');
 		
 		var modal = $(this);
 		
 		if(lPlantID==null)
 			$('#txtPlantID').val(<c:out value="${tempPlantID}"/>);
 		else
 			modal.find(".modal-body #txtPlantID").val(lPlantID);
 			
 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
 		modal.find(".modal-body #txtStorageTypeID").val(lStorageTypeID);
 		modal.find(".modal-body #txtStorageTypeName").val(lStorageTypeName);
 		
 		modal.find(".modal-body #txtPutAwayStrategy").val(lPutawayStrategy);
 		modal.find(".modal-body #txtAssignedIDPointStorType").val(lAssignedIDPointStorType);
 		
 		modal.find(".modal-body #txtStockRemovalStrategy").val(lStockRemovalStrategy);
 		modal.find(".modal-body #txtAssignedPickPointStorType").val(lAssignedPickPointStorType);
 		modal.find(".modal-body #txtReturnStorageType").val(lReturnStorageType);
 		
 		if(lPlantName!=null)
 			document.getElementById("lblPlantName").innerHTML = '(' + lPlantName + ')';
 		if(lWarehouseName!=null)
 			document.getElementById("lblWarehouseName").innerHTML = '(' + lWarehouseName + ')';
 			
 		if(lSUmgmt==true)
 			modal.find(".modal-body #cbSUmgmt").prop("checked", true);
 		if(lStorTypeIsIDPnt==true)
 			modal.find(".modal-body #cbStortypeisidpnt").prop("checked", true);
 		if(lStorTypeIsPckPnt==true)
 			modal.find(".modal-body #cbStortypeispckpnt").prop("checked", true);
 			
 		if(lStockPlcmtReqConfirmation==true)
 			modal.find(".modal-body #cbStockplcmtreqconf").prop("checked", true);
 		if(lDstBinChDuringConfirm==true)
 			modal.find(".modal-body #cbDstbinchduringconf").prop("checked", true);
 		if(lMixedStorage==true)
 			modal.find(".modal-body #cbMixedStorage").prop("checked", true);
 		if(lAddnToStock==true)
 			modal.find(".modal-body #cbAddntoStock").prop("checked", true);
 		if(lRetainOverDeliveries==true)
 			modal.find(".modal-body #cbRetainOverdeliveries").prop("checked", true);
 		if(lSUTCheckActive==true)
 			modal.find(".modal-body #cbSUTCheckActive").prop("checked", true);
 		if(lStorageSecCheckActive==true)
 			modal.find(".modal-body #cbStorageSecCheckActive").prop("checked", true);
 		if(lBlockUponStockPlcmt==true)
 			modal.find(".modal-body #cbBlockUponStockPlcmt").prop("checked", true);
 			
 			
 		if(lStockRemovalReqConfirmation==true)
 			modal.find(".modal-body #cbStockRmvlReqConf").prop("checked", true);
 		if(lAllowNegativeStock==true)
 			modal.find(".modal-body #cbAllowNegativeStock").prop("checked", true);
 		if(lFullStockRemovalReqAct==true)
 			modal.find(".modal-body #cbFullStkRmvlReqAct").prop("checked", true);
 		if(lReturnStockToSameStorageBin==true)
 			modal.find(".modal-body #cbReturnStockToSameStorageBin").prop("checked", true);
 		if(lExecuteZeroStockCheck==true)
 			modal.find(".modal-body #cbExecuteZeroStockCheck").prop("checked", true);
 		if(lRoundOffQty==true)
 			modal.find(".modal-body #cbRoundOffQty").prop("checked", true);
 		if(lBlockUponStockRemoval==true)
 			modal.find(".modal-body #cbBlockUponStockRemoval").prop("checked", true);
 		
 		
 		//set autofocus when form pop up
 		if(lWarehouseID == null || lWarehouseID == '')
 			{
 				$("#txtWarehouseID").focus(); 
 				$("#txtWarehouseID").click();
 			}
		else
			$("#txtStorageTypeName").focus();
	})
</script>

<!-- get warehouse from plant id -->
<script>
$(document).ready(function(){

    $(document).on('click', '#txtWarehouseID', function(e){
  
     e.preventDefault();
  
	 var plantid = document.getElementById('txtPlantID').value;
  
     $('#dynamic-content').html(''); // leave this div blank
	//$('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getwarehouse',
          type: 'POST',
          data: 'plantid='+plantid,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content').html(''); // blank before load.
          $('#dynamic-content').html(data); // load here
		  //$('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
		  //$('#modal-loader').hide();
     });

    });
});
</script>

<!-- get storage type by plant and warehouse id -->
<script>
$(document).ready(function(){

    $(document).on('click', '#txtAssignedIDPointStorType', function(e){
  
     e.preventDefault();
  
	 var plantid = document.getElementById('txtPlantID').value;
	 var warehouseid = document.getElementById('txtWarehouseID').value;
	 var storagetypeid = document.getElementById('txtStorageTypeID').value;
  
     $('#dynamic-storage-type').html(''); // leave this div blank
	//$('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getstoragetype',
          type: 'POST',
          data: {"type":"AssignedIDPointStorType","plantid":plantid,"warehouseid":warehouseid,"storagetypeid":storagetypeid},
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-storage-type').html(''); // blank before load.
          $('#dynamic-storage-type').html(data); // load here
		  //$('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-storage-type').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
		  //$('#modal-loader').hide();
     });

    });
});



$(document).ready(function(){

    $(document).on('click', '#txtAssignedPickPointStorType', function(e){
  
     e.preventDefault();
  
	 var plantid = document.getElementById('txtPlantID').value;
	 var warehouseid = document.getElementById('txtWarehouseID').value;
	 var storagetypeid = document.getElementById('txtStorageTypeID').value;
  
     $('#dynamic-storage-type').html(''); // leave this div blank
	//$('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getstoragetype',
          type: 'POST',
          data: {"type":"AssignedPickPointStorType","plantid":plantid,"warehouseid":warehouseid,"storagetypeid":storagetypeid},
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-storage-type').html(''); // blank before load.
          $('#dynamic-storage-type').html(data); // load here
		  //$('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-storage-type').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
		  //$('#modal-loader').hide();
     });

    });
});



$(document).ready(function(){

    $(document).on('click', '#txtReturnStorageType', function(e){
  
     e.preventDefault();
  
	 var plantid = document.getElementById('txtPlantID').value;
	 var warehouseid = document.getElementById('txtWarehouseID').value;
	 var storagetypeid = document.getElementById('txtStorageTypeID').value;
  
     $('#dynamic-storage-type').html(''); // leave this div blank
	//$('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getstoragetype',
          type: 'POST',
          data: {"type":"ReturnStorageType","plantid":plantid,"warehouseid":warehouseid,"storagetypeid":storagetypeid},
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-storage-type').html(''); // blank before load.
          $('#dynamic-storage-type').html(data); // load here
		  //$('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-storage-type').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
		  //$('#modal-loader').hide();
     });

    });
});
</script>

<script>
function FuncValPlant(){	
	var txtPlantID = document.getElementById('txtPlantID').value;
	
	if(!txtPlantID.match(/\S/)) {    	
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
    	
    	alert("Fill Plant ID First ...!!!");
        return false;
    } 
	
    return true;	
}

function FuncValDynamicStorageType(){	
	var txtPlantID = document.getElementById('txtPlantID').value;
	var txtWarehouseID = document.getElementById('txtWarehouseID').value;
	var txtStorageTypeID = document.getElementById('txtStorageTypeID').value;
	
	if(!txtPlantID.match(/\S/)) {    	
    	$('#txtPlantID').focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
    	
    	alert("Fill Plant ID First ...!!!");
        return false;
    }
    
    if(!txtWarehouseID.match(/\S/)) {    	
    	$('#txtWarehouseID').focus();
    	$('#dvWarehouseID').addClass('has-error');
    	$('#mrkWarehouseID').show();
    	
    	alert("Fill Warehouse ID First ...!!!");
        return false;
    } 
    
    if(!txtStorageTypeID.match(/\S/)) {    	
    	$('#txtStorageTypeID').focus();
    	$('#dvStorageTypeID').addClass('has-error');
    	$('#mrkStorageTypeID').show();
    	
    	alert("Fill Storage Type ID First ...!!!");
        return false;
    }  
	
    return true;	
}

function FuncClear(){
	$('#mrkPlantID').hide();
	$('#mrkWarehouseID').hide();
	$('#mrkStorageTypeID').hide();
	$('#mrkStorageTypeName').hide();
	$('#mrkPutAwayStrategy').hide();
	$('#mrkStockRemovalStrategy').hide();
	
	$('#dvPlantID').removeClass('has-error');
	$('#dvWarehouseID').removeClass('has-error');
	$('#dvStorageTypeID').removeClass('has-error');
	$('#dvStorageTypeName').removeClass('has-error');
	$('#dvPutAwayStrategy').removeClass('has-error');
	$('#dvStockRemovalStrategy').removeClass('has-error');
	
	document.getElementById("lblPlantName").innerHTML = null;
	document.getElementById("lblWarehouseName").innerHTML = null;
	
	$("#cbSUmgmt").prop("checked", false);
	$("#cbStortypeisidpnt").prop("checked", false);
	$("#cbStortypeispckpnt").prop("checked", false);
	
	$("#cbStockplcmtreqconf").prop("checked", false);
 	$("#cbDstbinchduringconf").prop("checked", false);
 	$("#cbMixedStorage").prop("checked", false);
 	$("#cbAddntoStock").prop("checked", false);
 	$("#cbRetainOverdeliveries").prop("checked", false);
 	$("#cbSUTCheckActive").prop("checked", false);
 	$("#cbStorageSecCheckActive").prop("checked", false);
 	$("#cbBlockUponStockPlcmt").prop("checked", false);
 	
 	$("#cbStockRmvlReqConf").prop("checked", false);
 	$("#cbAllowNegativeStock").prop("checked", false);
 	$("#cbFullStkRmvlReqAct").prop("checked", false);
 	$("#cbReturnStockToSameStorageBin").prop("checked", false);
 	$("#cbExecuteZeroStockCheck").prop("checked", false);
 	$("#cbRoundOffQty").prop("checked", false);
 	$("#cbBlockUponStockRemoval").prop("checked", false);
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Storage Type";

// 	$('#txtPlantID').prop('disabled', false);
	$('#txtWarehouseID').prop('disabled', false);
	$('#txtStorageTypeID').prop('disabled', false);
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById("lblTitleModal").innerHTML = 'Edit Storage Type';

// 	$('#txtPlantID').prop('disabled', true);
	$('#txtWarehouseID').prop('disabled', true);
	$('#txtStorageTypeID').prop('disabled', true);
}

function FuncPassString(lParamPlantID,lParamPlantName){
	$("#txtPlantID").val(lParamPlantID);
	document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
}

function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
	$("#txtWarehouseID").val(lParamWarehouseID);
	document.getElementById('lblWarehouseName').innerHTML = '(' + lParamWarehouseName + ')';
}

function FuncPassPutawayStrategyData(lParamID){
	$("#txtPutAwayStrategy").val(lParamID);
}

function FuncPassPickupStrategyData(lParamID){
	$("#txtStockRemovalStrategy").val(lParamID);
}

function FuncPassDynamicStorageTypeData(lParamID,lType,lParamName){
	if(lType=='AssignedIDPointStorType')
		$("#txtAssignedIDPointStorType").val(lParamID);
	else if(lType=='AssignedPickPointStorType')
		$("#txtAssignedPickPointStorType").val(lParamID);
	else if(lType=='ReturnStorageType')
		$("#txtReturnStorageType").val(lParamID);
}

function FuncValEmptyInput(lParambtn) {
	var txtPlantID = document.getElementById('txtPlantID').value;
	var txtWarehouseID = document.getElementById('txtWarehouseID').value;
	var txtStorageTypeID = document.getElementById('txtStorageTypeID').value;
	var txtStorageTypeName = document.getElementById('txtStorageTypeName').value;
	var cbSUMgmtActive = false;
	var cbStorTypeIsIDPnt = false;
	var cbStorTypeIsPckPnt = false;
	
	var txtPutAwayStrategy = document.getElementById('txtPutAwayStrategy').value;
	var txtAssignedIDPointStorType = document.getElementById('txtAssignedIDPointStorType').value;
	var cbStockplcmtreqconf = false;
	var cbDstbinchduringconf = false;
	var cbMixedStorage = false;
	var cbAddntoStock = false;
	var cbRetainOverdeliveries = false;
	var cbSUTCheckActive = false;
	var cbStorageSecCheckActive = false;
	var cbBlockUponStockPlcmt = false;

	var txtStockRemovalStrategy = document.getElementById('txtStockRemovalStrategy').value;
	var txtAssignedPickPointStorType = document.getElementById('txtAssignedPickPointStorType').value;
	var txtReturnStorageType = document.getElementById('txtReturnStorageType').value;
	var cbStockRmvlReqConf = false;
	var cbAllowNegativeStock = false;
	var cbFullStkRmvlReqAct = false;
	var cbReturnStockToSameStorageBin = false;
	var cbExecuteZeroStockCheck = false;
	var cbRoundOffQty = false;
	var cbBlockUponStockRemoval = false;
	
	var dvPlantID = document.getElementsByClassName('dvPlantID');
	var dvWarehouseID = document.getElementsByClassName('dvWarehouseID');
	var dvStorageTypeID = document.getElementsByClassName('dvStorageTypeID');
	var dvStorageTypeName = document.getElementsByClassName('dvStorageTypeName');
	var dvPutAwayStrategy = document.getElementsByClassName('dvPutAwayStrategy');
	var dvStockRemovalStrategy = document.getElementsByClassName('dvStockRemovalStrategy');
	
    if(!txtPlantID.match(/\S/)) {
    	$("#txtPlantID").focus();
    	$('#dvPlantID').addClass('has-error');
    	$('#mrkPlantID').show();
        return false;
    }
    if(!txtWarehouseID.match(/\S/)) {
    	$("#txtWarehouseID").focus();
    	$('#dvWarehouseID').addClass('has-error');
    	$('#mrkWarehouseID').show();
        return false;
    }
    if(!txtStorageTypeID.match(/\S/)) {
    	$("#txtStorageTypeID").focus();
    	$('#dvStorageTypeID').addClass('has-error');
    	$('#mrkStorageTypeID').show();
        return false;
    }
    if(!txtStorageTypeName.match(/\S/)) {
    	$("#txtStorageTypeName").focus();
    	$('#dvStorageTypeName').addClass('has-error');
    	$('#mrkStorageTypeName').show();
        return false;
    }
    if(!txtPutAwayStrategy.match(/\S/)) {
    	$("#txtPutAwayStrategy").focus();
    	$('#dvPutAwayStrategy').addClass('has-error');
    	$('#mrkPutAwayStrategy').show();
        return false;
    }
    if(!txtStockRemovalStrategy.match(/\S/)) {
    	$("#txtStockRemovalStrategy").focus();
    	$('#dvStockRemovalStrategy').addClass('has-error');
    	$('#mrkStockRemovalStrategy').show();
        return false;
    }
     if(txtAssignedPickPointStorType.match(/\S/) && txtReturnStorageType.match(/\S/) && txtAssignedPickPointStorType.match(txtReturnStorageType)) {
    	$('#txtReturnStorageType').focus();
    	alert("'Assigned pick point stor. ty.' tidak boleh sama dengan 'Return storage type'");
        return false;
    }
   
   
    if($("#cbSUmgmt").prop('checked') == true)
    	cbSUMgmtActive = true;
    if($("#cbStortypeisidpnt").prop('checked') == true)
    	cbStorTypeIsIDPnt = true;
    if($("#cbStortypeispckpnt").prop('checked') == true)
    	cbStorTypeIsPckPnt = true;
    	
    	
    if($("#cbStockplcmtreqconf").prop('checked') == true)
    	cbStockplcmtreqconf = true;
    if($("#cbDstbinchduringconf").prop('checked') == true)
    	cbDstbinchduringconf = true;
    if($("#cbMixedStorage").prop('checked') == true)
    	cbMixedStorage = true;
    if($("#cbAddntoStock").prop('checked') == true)
    	cbAddntoStock = true;
    if($("#cbRetainOverdeliveries").prop('checked') == true)
    	cbRetainOverdeliveries = true;
    if($("#cbSUTCheckActive").prop('checked') == true)
    	cbSUTCheckActive = true;
    if($("#cbStorageSecCheckActive").prop('checked') == true)
    	cbStorageSecCheckActive = true;
    if($("#cbBlockUponStockPlcmt").prop('checked') == true)
    	cbBlockUponStockPlcmt = true;
    	
    	
    if($("#cbStockRmvlReqConf").prop('checked') == true)
    	cbStockRmvlReqConf = true;
    if($("#cbAllowNegativeStock").prop('checked') == true)
    	cbAllowNegativeStock = true;
    if($("#cbFullStkRmvlReqAct").prop('checked') == true)
    	cbFullStkRmvlReqAct = true;
    if($("#cbReturnStockToSameStorageBin").prop('checked') == true)
    	cbReturnStockToSameStorageBin = true;
    if($("#cbExecuteZeroStockCheck").prop('checked') == true)
    	cbExecuteZeroStockCheck = true;
    if($("#cbRoundOffQty").prop('checked') == true)
    	cbRoundOffQty = true;
    if($("#cbBlockUponStockRemoval").prop('checked') == true)
    	cbBlockUponStockRemoval = true;
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/storagetype',	
        type:'POST',
        data:{"key":lParambtn,"slPlantId":txtPlantID,"txtWarehouseID":txtWarehouseID,"txtStorageTypeID":txtStorageTypeID,"txtStorageTypeName":txtStorageTypeName,
        	  "cbSUmgmt":cbSUMgmtActive,"cbStorTypeIsIDPnt":cbStorTypeIsIDPnt,"cbStorTypeIsPckPnt":cbStorTypeIsPckPnt,
        	  "txtPutAwayStrategy":txtPutAwayStrategy,"txtAssignedIDPointStorType":txtAssignedIDPointStorType,
        	  "cbStockplcmtreqconf":cbStockplcmtreqconf,"cbDstbinchduringconf":cbDstbinchduringconf,"cbMixedStorage":cbMixedStorage,"cbAddntoStock":cbAddntoStock,
        	  "cbRetainOverdeliveries":cbRetainOverdeliveries,"cbSUTCheckActive":cbSUTCheckActive,"cbStorageSecCheckActive":cbStorageSecCheckActive,"cbBlockUponStockPlcmt":cbBlockUponStockPlcmt,
        	  "txtStockRemovalStrategy":txtStockRemovalStrategy,"txtAssignedPickPointStorType":txtAssignedPickPointStorType,"txtReturnStorageType":txtReturnStorageType,
        	  "cbStockRmvlReqConf":cbStockRmvlReqConf,"cbAllowNegativeStock":cbAllowNegativeStock,"cbFullStkRmvlReqAct":cbFullStkRmvlReqAct,"cbReturnStockToSameStorageBin":cbReturnStockToSameStorageBin,
        	  "cbExecuteZeroStockCheck":cbExecuteZeroStockCheck,"cbRoundOffQty":cbRoundOffQty,"cbBlockUponStockRemoval":cbBlockUponStockRemoval},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertStorageType')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan tipe rak";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtPlantID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateStorageType')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui tipe rak";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				//$("#txtNumberFrom").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/storagetype';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    return true;
}

//set the focus behaviour into click
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtWarehouseID:focus').length) {
    	$('#txtWarehouseID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtPutAwayStrategy:focus').length) {
    	$('#txtPutAwayStrategy').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtAssignedIDPointStorType:focus').length) {
    	$('#txtAssignedIDPointStorType').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtStockRemovalStrategy:focus').length) {
    	$('#txtStockRemovalStrategy').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtAssignedPickPointStorType:focus').length) {
    	$('#txtAssignedPickPointStorType').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtReturnStorageType:focus').length) {
    	$('#txtReturnStorageType').click();
    }
});
</script>

</body>
</html>