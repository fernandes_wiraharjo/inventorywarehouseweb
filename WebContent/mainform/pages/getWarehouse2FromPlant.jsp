<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form id="getWarehouse2" name="getWarehouse2" action = "${pageContext.request.contextPath}/getwarehouse2" method="post">

<table id="tb_master_warehouse_2" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>Plant ID</th>
			<th>Warehouse ID</th>
			<th>Warehouse Name</th>
			<th>Warehouse Desc</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listwarehouse}" var ="warehouse">
	  <tr>
	  <td><c:out value="${warehouse.plantID}" /></td>
		<td><c:out value="${warehouse.warehouseID}" /></td>
		<td><c:out value="${warehouse.warehouseName}" /></td>
		<td><c:out value="${warehouse.warehouseDesc}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassStringWarehouse2('<c:out value="${warehouse.warehouseID}"/>','<c:out value="${warehouse.warehouseName}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

</form>

<script>
 	$(function () {
  		$("#tb_master_warehouse_2").DataTable();
  	});
</script>