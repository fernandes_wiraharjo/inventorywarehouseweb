<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Vendor</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="Vendor" name="Vendor" action = "${pageContext.request.contextPath}/Vendor" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Vendor <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertVendor'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan vendor.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateVendor'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui vendor.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessDeleteVendor'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menghapus vendor.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedDeleteVendor'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal menghapus vendor. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>   
	      					
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Vendor ID</th>
										<th>Vendor Name</th>
										<th>Vendor Address</th>
										<th>Phone</th>
										<th>PIC</th>
										<th>Vendor Type</th>
										<th style="width:60px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listvendor}" var="vendor">
										<tr>
											<td><c:out value="${vendor.vendorID}" /></td>
											<td><c:out value="${vendor.vendorName}" /></td>
											<td><c:out value="${vendor.vendorAddress}" /></td>
											<td><c:out value="${vendor.phone}" /></td>
											<td><c:out value="${vendor.PIC}" /></td>
											<td><c:out value="${vendor.vendorType}" /></td>
											<td><button <c:out value="${buttonstatus}"/> 
														id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert" 
														data-lvendorid='<c:out value="${vendor.vendorID}" />'
														data-lvendorname='<c:out value="${vendor.vendorName}" />'
														data-lvendoraddress='<c:out value="${vendor.vendorAddress}" />'
														data-lphone='<c:out value="${vendor.phone}" />'
														data-lpic='<c:out value="${vendor.PIC}" />'
														data-lvendortype='<c:out value="${vendor.vendorType}" />' >
														<i class="fa fa-edit"></i></button> 
												<button <c:out value="${buttonstatus}"/> id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" onclick="FuncButtonDelete()" data-toggle="modal" data-target="#ModalDelete" data-lvendorid='<c:out value="${vendor.vendorID}" />'>
												<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
									<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
      											
      											<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
							          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
							          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     						</div>
      											
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvVendorID" class="form-group">
	            								<label for="recipient-name" class="control-label">Vendor ID</label><label id="mrkVendorID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtVendorID" name="txtVendorID">
	          								</div>
	          								<div id="dvVendorName" class="form-group">
	            								<label for="message-text" class="control-label">Vendor Name</label><label id="mrkVendorName" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtVendorName" name="txtVendorName">
	          								</div>
	          								<div id="dvVendorAddress" class="form-group ">
	            								<label for="message-text" class="control-label">Vendor Address</label><label id="mrkVendorAddress" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtVendorAddress" name="txtVendorAddress">
	          								</div>
	          								<div id="dvPhone" class="form-group ">
	            								<label for="message-text" class="control-label">Phone</label><label id="mrkPhone" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtPhone" name="txtPhone">
	          								</div>
	          								<div id="dvPIC" class="form-group">
	            								<label for="message-text" class="control-label">PIC</label><label id="mrkPIC" for="recipient-name" class="control-label"> <small>*</small></label>	
	            								<input type="text" class="form-control" id="txtPIC" name="txtPIC">
	          								</div>
	          								<div id="dvVendorType" class="form-group">
	            								<label for="message-text" class="control-label">Vendor Type</label><label id="mrkVendorType" for="recipient-name" class="control-label"> <small>*</small></label>	
	            								<select id="slVendorType" name="slVendorType" class="form-control">
							                    <option>Eksternal</option>
							                    <option>Internal</option>
							                    </select>
	          								</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
									<!--modal Delete -->
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">            											
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Vendor</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtVendorID" name="temp_txtVendorID"  />
               	 									<p>Are you sure to delete this vendor ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->							
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_itemlib").DataTable();
  		$('#M002').addClass('active');
  		$('#M015').addClass('active');
  		
  	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
  		
	    $("#dvErrorAlert").hide();
  	});
	</script>
	<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lVendorID = button.data('lvendorid');
		$("#temp_txtVendorID").val(lVendorID);
	})
	</script>
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		var lVendorID = button.data('lvendorid');
 		var lVendorName = button.data('lvendorname');
 		var lVendorAddress = button.data('lvendoraddress');
 		var lPhone = button.data('lphone');
 		var lPIC = button.data('lpic');
 		var lVendorType = button.data('lvendortype');
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtVendorID").val(lVendorID);
 		modal.find(".modal-body #txtVendorName").val(lVendorName);
 		modal.find(".modal-body #txtVendorAddress").val(lVendorAddress)
 		modal.find(".modal-body #txtPIC").val(lPIC);
 		modal.find(".modal-body #txtPhone").val(lPhone);
 		modal.find(".modal-body #slVendorType").val(lVendorType);
 		
 		if(lVendorID == null || lVendorID == '')
 			$("#txtVendorID").focus();
 		else
 			$("#txtVendorName").focus();
	})
</script>
<script>

function FuncClear(){
	$('#mrkVendorID').hide();
	$('#mrkVendorName').hide();
	$('#mrkVendorAddress').hide();
	$('#mrkPhone').hide();
	$('#mrkPIC').hide();
	$('#txtVendorID').prop('disabled', false);
	$('#mrkVendorType').hide();
	
	$('#dvVendorID').removeClass('has-error');
	$('#dvVendorName').removeClass('has-error');
	$('#dvVendorAddress').removeClass('has-error');
	$('#dvPhone').removeClass('has-error');
	$('#dvPIC').removeClass('has-error');
	$('#dvVendorType').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Vendor";	

	FuncClear();
	$('#txtVendorID').prop('disabled', false);
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById("lblTitleModal").innerHTML = 'Edit Vendor';

	FuncClear();
	$('#txtVendorID').prop('disabled', true);
}

function FuncValEmptyInput(lParambtn) {
	var txtVendorID = document.getElementById('txtVendorID').value;
	var txtVendorName = document.getElementById('txtVendorName').value;
	var txtVendorAddress = document.getElementById('txtVendorAddress').value;
	var txtPhone = document.getElementById('txtPhone').value;
	var txtPIC = document.getElementById('txtPIC').value;
	var slVendorType = document.getElementById('slVendorType').value;;
	
	var dvVendorID = document.getElementsByClassName('dvVendorID');
	var dvVendorName = document.getElementsByClassName('dvVendorName');
	var dvVendorAddress = document.getElementsByClassName('dvVendorAddress');
	var dvPhone = document.getElementsByClassName('dvPhone');
	var dvPIC = document.getElementsByClassName('dvPIC');
	var dvVendorType = document.getElementsByClassName('dvVendorType');
	
	if(lParambtn == 'save'){
		$('#txtVendorID').prop('disabled', false);
		}
		else{
		$('#txtVendorID').prop('disabled', true);
		}
	
    if(!txtVendorID.match(/\S/)) {
    	$("#txtVendorID").focus();
    	$('#dvVendorID').addClass('has-error');
    	$('#mrkVendorID').show();
        return false;
    } 
    
    if(!txtVendorName.match(/\S/)) {    	
    	$('#txtVendorName').focus();
    	$('#dvVendorName').addClass('has-error');
    	$('#mrkVendorName').show();
        return false;
    } 
    
    if(!txtVendorAddress.match(/\S/)) {
    	$('#txtVendorAddress').focus();
    	$('#dvVendorAddress').addClass('has-error');
    	$('#mrkVendorAddress').show();
        return false;
    } 
	
    if(!txtPhone.match(/\S/)) {
    	$('#txtPhone').focus();
    	$('#dvPhone').addClass('has-error');
    	$('#mrkPhone').show();
        return false;
    } 
    
    if(!txtPIC.match(/\S/)) {
    	$('#txtPIC').focus();
    	$('#dvPIC').addClass('has-error');
    	$('#mrkPIC').show();
        return false;
    } 
    
    if(!slVendorType.match(/\S/)) {
    	$('#slVendorType').focus();
    	$('#dvVendorType').addClass('has-error');
    	$('#mrkVendorType').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/Vendor',	
        type:'POST',
        data:{"key":lParambtn,"txtVendorID":txtVendorID,"txtVendorName":txtVendorName,"txtVendorAddress":txtVendorAddress,"txtPhone":txtPhone,"txtPIC":txtPIC,"slVendorType":slVendorType},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertVendor')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan vendor";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtVendorID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateVendor')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui vendor";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtVendorName").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/Vendor';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>


</body>
</html>