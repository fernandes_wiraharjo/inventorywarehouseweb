<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Cancel Stock Transfer</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<!-- <style type="text/css">	 -->
<!-- #ModalUpdateInsert { overflow-y:scroll } -->
<!-- </style> -->

<style>	
/*  css for loading bar */
 .loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 35px;
  height: 35px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
/* end of css loading bar */
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="CancelStockTransfer" name="CancelStockTransfer" action = "${pageContext.request.contextPath}/CancelStockTransfer" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Cancel Stock Transfer <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertCancelStockTransfer'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan dokumen batal pindah gudang.
              				</div>
	      					</c:if>
	      					
	      					<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    											<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
							          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
							          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     						</div>
					     						
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	        								<input type="hidden" id="temp_docNo" name="temp_docNo" value='<c:out value = "${tempdocNo}"/>' />
	          								<div id="dvDocNumber" class="form-group">
	            								<label for="recipient-name" class="control-label">Cancel Document No</label><label id="mrkDocNumber" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDocNumber" name="txtDocNumber" readonly="readonly">
	          								</div>
	          								<div id="dvDate" class="form-group">
	            								<label for="message-text" class="control-label">Date</label><label id="mrkDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDate" name="txtDate" data-date-format="dd M yyyy">
	          								</div>
<!-- 	          								<div id="dvPlantID" class="form-group"> -->
<!-- 	            								<label for="message-text" class="control-label">Plant ID</label><label id="mrkPlantID" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" data-target="#ModalGetPlantID"> -->
<!-- 	          								</div> -->
<!-- 	          								<div id="dvWarehouseID" class="form-group"> -->
<!-- 	            								<label for="message-text" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()"> -->
<!-- 	          								</div> -->
	          								<div id="dvRefDocNumber" class="form-group">
	            								<label for="message-text" class="control-label">Stock Transfer Document No</label><label id="mrkRefDocNumber" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtRefDocNumber" name="txtRefDocNumber" data-toggle="modal" data-target="#ModalGetStockTransfer" onfocus="FuncValPlantWarehouse()">
	          								</div>
	        								
      								</div>
      								
      								<div class="modal-footer">
      										<div class="row">
												<div class="col-md-1 pull-right">
													<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
												</div>
												<div class="col-md-0 pull-right">
												&nbsp&nbsp&nbsp&nbsp
												</div>
												<div class="col-md-1 pull-right">
													<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary pull-right" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
												</div>
												<div class="col-md-2 pull-right">
													<!-- loading bar -->
													<div id="dvloader" class="loader" style="display:none"></div>	
												</div>
					      					</div>							
      								</div>
    										</div>
  										</div>
									</div>
										
										<!--modal show plant data -->
<!-- 										<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID"> -->
<!-- 												<div class="modal-dialog" role="document"> -->
<!-- 			  										<div class="modal-content"> -->
<!-- 			    											<div class="modal-header"> -->
<!-- 			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
<!-- 			      											<h4 class="modal-title" id="ModalLabelPlantID"> Plant Data</h4>	 -->
			      												
			      											
<!-- 			    											</div> -->
<!-- 			     								<div class="modal-body"> -->
			       								
<!-- 			         								<table id="tb_master_plant" class="table table-bordered table-hover"> -->
<!-- 										        <thead style="background-color: #d2d6de;"> -->
<!-- 										                <tr> -->
<!-- 										                <th>Plant ID</th> -->
<!-- 										                <th>Plant Name</th> -->
<!-- 										                <th>Description</th> -->
<!-- 														<th style="width: 20px"></th> -->
<!-- 										                </tr> -->
<!-- 										        </thead> -->
										        
<!-- 										        <tbody> -->
										        
<%-- 										        <c:forEach items="${listPlant}" var ="plant"> --%>
<!-- 												        <tr> -->
<%-- 												        <td><c:out value="${plant.plantID}"/></td> --%>
<%-- 												        <td><c:out value="${plant.plantNm}"/></td> --%>
<%-- 												        <td><c:out value="${plant.desc}"/></td> --%>
<!-- 												        <td><button type="button" class="btn btn-primary" -->
<!-- 												        			data-toggle="modal" -->
<%-- 												        			onclick="FuncPassString('<c:out value="${plant.plantID}"/>','','')" --%>
<!-- 												        			data-dismiss="modal" -->
<!-- 												        	><i class="fa fa-fw fa-check"></i></button> -->
<!-- 												        </td> -->
<!-- 										        		</tr> -->
										        		
<%-- 										        </c:forEach> --%>
										        
<!-- 										        </tbody> -->
<!-- 										        </table> -->
			       								
<!-- 			    								</div> -->
			    								
<!-- 			    								<div class="modal-footer"> -->
			      								
<!-- 			    								</div> -->
<!-- 			  										</div> -->
<!-- 													</div> -->
<!-- 										</div> -->
										<!-- /. end of modal show plant data -->
										
										<!--modal show warehouse data -->
<!-- 										<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID"> -->
<!-- 												<div class="modal-dialog" role="document"> -->
<!-- 			  										<div class="modal-content"> -->
<!-- 			    											<div class="modal-header"> -->
<!-- 			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
<!-- 			      											<h4 class="modal-title" id="ModalLabelWarehouseID">Warehouse Data</h4>	 -->
			      												
			      											
<!-- 			    											</div> -->
<!-- 			     								<div class="modal-body"> -->
			       								
<!-- 			         								<table id="tb_master_warehouse" class="table table-bordered table-hover"> -->
<!-- 										        <thead style="background-color: #d2d6de;"> -->
<!-- 										                <tr> -->
<!-- 										                <th>Plant ID</th> -->
<!-- 														<th>Warehouse ID</th> -->
<!-- 														<th>Warehouse Name</th> -->
<!-- 														<th>Warehouse Desc</th> -->
<!-- 														<th style="width: 20px"></th> -->
<!-- 										                </tr> -->
<!-- 										        </thead> -->
										        
<!-- 										        <tbody> -->
										        
<%-- 										        <c:forEach items="${listwarehouse}" var ="warehouse"> --%>
<!-- 												        <tr> -->
<%-- 												        <td><c:out value="${warehouse.plantID}" /></td> --%>
<%-- 														<td><c:out value="${warehouse.warehouseID}" /></td> --%>
<%-- 														<td><c:out value="${warehouse.warehouseName}" /></td> --%>
<%-- 														<td><c:out value="${warehouse.warehouseDesc}" /></td> --%>
<!-- 												        <td><button type="button" class="btn btn-primary" -->
<!-- 												        			data-toggle="modal" -->
<%-- 												        			onclick="FuncPassString('','<c:out value="${warehouse.warehouseID}"/>','')" --%>
<!-- 												        			data-dismiss="modal" -->
<!-- 												        	><i class="fa fa-fw fa-check"></i></button> -->
<!-- 												        </td> -->
<!-- 										        		</tr> -->
										        		
<%-- 										        </c:forEach> --%>
										        
<!-- 										        </tbody> -->
<!-- 										        </table> -->
			       								
<!-- 			    								</div> -->
			    								
<!-- 			    								<div class="modal-footer"> -->
			      								
<!-- 			    								</div> -->
<!-- 			  										</div> -->
<!-- 													</div> -->
<!-- 										</div> -->
										<!-- /. end of modal show warehouse data -->
										
										<!--modal show Stock Transfer data -->
										<div class="modal fade" id="ModalGetStockTransfer" tabindex="-1" role="dialog" aria-labelledby="ModalLabelStockTransfer">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelStockTransfer">Stock Transfer Data</h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_stocktransfer" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>TransferID</th>
														<th>PlantID</th>
														<th>From</th>
														<th>To</th>
														<th>Date</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listStockTransfer}" var ="stocktransfer">
												        <tr>
												        <td><c:out value="${stocktransfer.transferID}" /></td>
														<td><c:out value="${stocktransfer.plantID}" /></td>
														<td><c:out value="${stocktransfer.from}" /></td>
														<td><c:out value="${stocktransfer.to}" /></td>
														<td><c:out value="${stocktransfer.date}" /></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassString('','','<c:out value="${stocktransfer.transferID}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show Stock Transfer data -->
										
										<!--modal show cancel Stock Transfer detail data -->
										<div class="modal fade bs-example-modal-lg" id="ModalCancelStockTransferDetail" tabindex="-1" role="dialog" aria-labelledby="ModalLabelCancelStockTransferDetail">
												<div class="modal-dialog modal-lg" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelCancelStockTransferDetail">Cancel Stock Transfer Detail Data</h4>	
			      											<br><br>
			      											<label id="cclstdocno"></label>
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load cancel stock transfer detail by cancel stock transfer doc no will be load here -->                          
           										<div id="dynamic-content">
           										</div>
           										
<!-- 			         								<table id="tb_cancel_stocktransfer_detail" class="table table-bordered table-hover"> -->
<!-- 										        <thead style="background-color: #d2d6de;"> -->
<!-- 										                <tr> -->
<!-- 										                <th>Transfer ID</th> -->
<!-- 														<th>Line</th> -->
<!-- 														<th>Product ID</th> -->
<!-- 														<th>Qty UOM</th> -->
<!-- 														<th>UOM</th> -->
<!-- 														<th>Qty Base UOM</th> -->
<!-- 														<th>Base UOM</th> -->
<!-- 														<th>Batch No</th> -->
<!-- 														<th>Packing No</th> -->
<!-- 										                </tr> -->
<!-- 										        </thead> -->
										        
<!-- 										        <tbody> -->
										        
<%-- 										        <c:forEach items="${listCancelStockTransferDetail}" var ="cancelstocktransferdetail"> --%>
<!-- 												        <tr> -->
<%-- 												        <td><c:out value="${cancelstocktransferdetail.docNumber}" /></td> --%>
<%-- 														<td><c:out value="${cancelstocktransferdetail.docLine}" /></td> --%>
<%-- 														<td><c:out value="${cancelstocktransferdetail.productID}" /></td> --%>
<%-- 														<td><c:out value="+${cancelstocktransferdetail.qtyUOM}" /></td> --%>
<%-- 														<td><c:out value="${cancelstocktransferdetail.UOM}" /></td> --%>
<%-- 														<td><c:out value="+${cancelstocktransferdetail.qtyBaseUOM}" /></td> --%>
<%-- 														<td><c:out value="${cancelstocktransferdetail.baseUOM}" /></td> --%>
<%-- 														<td><c:out value="${cancelstocktransferdetail.batch_No}" /></td> --%>
<%-- 														<td><c:out value="${cancelstocktransferdetail.packing_No}" /></td> --%>
<!-- 										        		</tr> -->
										        		
<%-- 										        </c:forEach> --%>
										        
<!-- 										        </tbody> -->
<!-- 										        </table> -->
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show cancel inbound detail data -->
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_cancel_stocktransfer" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Cancel Doc No</th>
										<th>Date</th>
										<th>Plant ID</th>
										<th>Warehouse ID</th>
										<th>Stock Transfer Doc No</th>
										<th>Transaction</th>
										<th style="width:1px;"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listCancelStockTransfer}" var="cancelstocktransfer">
										<tr>
											<td><c:out value="${cancelstocktransfer.docNumber}" /></td>
											<td><c:out value="${cancelstocktransfer.date}" /></td>
											<td><c:out value="${cancelstocktransfer.plantID}" /></td>
											<td><c:out value="${cancelstocktransfer.warehouseID}" /></td>
											<td><c:out value="${cancelstocktransfer.refDocNumber}" /></td>
											<td><c:out value="${cancelstocktransfer.transaction}" /></td>
											<td>
											<button type="button" id="btnCancelStockTransferDetail" name="btnCancelStockTransferDetail" 
											class="btn btn-default" data-toggle="modal" data-target="#ModalCancelStockTransferDetail"
											data-docno='<c:out value="${cancelstocktransfer.docNumber}" />'>
											<i class="fa fa-list-ul"></i>
											</button>
<%-- 											onclick="FuncShowCancelStockTransferDetail('<c:out value="${cancelstocktransfer.docNumber}"/>')" --%>
											</td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_stocktransfer").DataTable();
//   		$("#tb_master_plant").DataTable();
//   		$("#tb_master_warehouse").DataTable();
  		$("#tb_cancel_stocktransfer").DataTable();
  		$("#tb_cancel_stocktransfer_detail").DataTable();
  		$('#mtransaction').addClass('active');
  		$('#M005').addClass('active');
  		$('#M032').addClass('active');
  		
  		$("#dvErrorAlert").hide();
  		$('#dvloader').hide();
  	});
 	
 	//shortcut for button 'new'
    Mousetrap.bind('n', function() {
    	FuncButtonNew(),
    	$('#ModalUpdateInsert').modal('show')
    	});
	</script>
    
<script>
$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
	$("#dvErrorAlert").hide();
	
  var button = $(event.relatedTarget) // Button that triggered the modal
  var modal = $(this)
  $('#txtDate').focus();
  
})
</script>

<script>
function FuncPassString(lParamPlantID,lParamWarehouseID,lParamStockTransfer){
// 	if(lParamPlantID)
// 	$("#txtPlantID").val(lParamPlantID);
	
// 	if(lParamWarehouseID)
// 		$("#txtWarehouseID").val(lParamWarehouseID);
	
	if(lParamStockTransfer)
		$("#txtRefDocNumber").val(lParamStockTransfer);
}
</script>

<script>
function FuncClear(){
	$('#mrkDocNumber').hide();
	$('#mrkDate').hide();
// 	$('#mrkPlantID').hide();
// 	$('#mrkWarehouseID').hide();
	$('#mrkRefDocNumber').hide();
	
	$('#dvDocNumber').removeClass('has-error');
	$('#dvDate').removeClass('has-error');
// 	$('#dvPlantID').removeClass('has-error');
// 	$('#dvWarehouseID').removeClass('has-error');
	$('#dvRefDocNumber').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	
	var tempDocNumber = document.getElementById('temp_docNo').value;
	$('#txtDocNumber').val(tempDocNumber);
	
	$('#txtDate').val('');
// 	$('#txtPlantID').val('');
// 	$('#txtWarehouseID').val('');
	$('#txtRefDocNumber').val('');
		
	$('#btnSave').show();
	document.getElementById("lblTitleModal").innerHTML = "Add Cancel StockTransfer";	

	FuncClear();
	
	$('#txtDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
		$('#txtDate').datepicker('setDate', new Date());
}

// function FuncValPlant(){	
// 	var txtPlantID = document.getElementById('txtPlantID').value;
	
// 	var table = $("#tb_master_warehouse").DataTable();
// 	table.search( txtPlantID + " " ).draw();
	
// 	FuncClear();
	
// 	if(!txtPlantID.match(/\S/)) {    	
//     	$('#txtPlantID').focus();
//     	$('#dvPlantID').addClass('has-error');
//     	$('#mrkPlantID').show();
    	
//     	alert("Fill Plant ID First ...!!!");
//         return false;
//     } 
	
//     return true;
	
// }

// function FuncValPlantWarehouse(){	
// 	var txtPlantID = document.getElementById('txtPlantID').value;
// 	var txtWarehouseID = document.getElementById('txtWarehouseID').value;
	
// 	var table = $("#tb_stocktransfer").DataTable();
// 	table.search( txtPlantID + " " + txtWarehouseID + " ").draw();
	
// 	FuncClear();
	
// 	if(!txtPlantID.match(/\S/)) {    	
//     	$('#txtPlantID').focus();
//     	$('#dvPlantID').addClass('has-error');
//     	$('#mrkPlantID').show();
    	
//     	alert("Fill Plant ID First ...!!!");
//         return false;
//     }
	
// 	if(!txtWarehouseID.match(/\S/)) {    	
//     	$('#txtWarehouseID').focus();
//     	$('#dvWarehouseID').addClass('has-error');
//     	$('#mrkWarehouseID').show();
    	
//     	alert("Fill Warehouse ID First ...!!!");
//         return false;
//     }
	
//     return true;
	
// }

// function FuncShowCancelStockTransferDetail(lParamCancelStockTransferDocNo){	
	
// 	var table = $("#tb_cancel_stocktransfer_detail").DataTable();
// 	table.search( lParamCancelStockTransferDocNo + " " ).draw();
	
// 	document.getElementById("cclstdocno").innerHTML = "Document No : " + lParamCancelStockTransferDocNo;
	
// 	FuncClear();
	
//     return true;
	
// }

function FuncValEmptyInput(lParambtn) {
	var txtDocNumber = document.getElementById('txtDocNumber').value;
	var txtDate = document.getElementById('txtDate').value;
// 	var txtPlantID = document.getElementById('txtPlantID').value;
// 	var txtWarehouseID = document.getElementById('txtWarehouseID').value;
	var txtRefDocNumber = document.getElementById('txtRefDocNumber').value;
	
	var dvDocNumber = document.getElementsByClassName('dvDocNumber');
	var dvDate = document.getElementsByClassName('dvDate');
// 	var dvPlantID = document.getElementsByClassName('dvPlantID');
// 	var dvWarehouseID = document.getElementsByClassName('dvWarehouseID');
	var dvRefDocNumber = document.getElementsByClassName('dvRefDocNumber');

    if(!txtDocNumber.match(/\S/)) {
    	$("#txtDocNumber").focus();
    	$('#dvDocNumber').addClass('has-error');
    	$('#mrkDocNumber').show();
        return false;
    } 
    
    if(!txtDate.match(/\S/)) {    	
    	$('#txtDate').focus();
    	$('#dvDate').addClass('has-error');
    	$('#mrkDate').show();
        return false;
    }
    
//     if(!txtPlantID.match(/\S/)) {
//     	$('#txtPlantID').focus();
//     	$('#dvPlantID').addClass('has-error');
//     	$('#mrkPlantID').show();
//         return false;
//     } 
    
//     if(!txtWarehouseID.match(/\S/)) {
//     	$('#txtWarehouseID').focus();
//     	$('#dvWarehouseID').addClass('has-error');
//     	$('#mrkWarehouseID').show();
//         return false;
//     }
    
    if(!txtRefDocNumber.match(/\S/)) {
    	$('#txtRefDocNumber').focus();
    	$('#dvRefDocNumber').addClass('has-error');
    	$('#mrkRefDocNumber').show();
        return false;
    }
    
    $('#dvloader').show();
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/CancelStockTransfer',	
        type:'POST',
        data:{"key":lParambtn,"txtDocNumber":txtDocNumber,"txtDate":txtDate,"txtRefDocNumber":txtRefDocNumber},
//          "txtPlantID":txtPlantID, "txtWarehouseID":txtWarehouseID
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedLoadStockTransfer')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				//$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		$('#dvloader').hide();
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedInsertCancelStockTransfer')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan dokumen batal pindah gudang";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		$('#dvloader').hide();
        		return false;
        	}
        	else if(data.split("--")[0] == 'SuccessInsertCancelStockTransfer-1')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Sukses menambahkan dokumen batal pindah gudang tetapi proses pembatalan tidak lengkap";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				//$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		$('#dvloader').hide();
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/CancelStockTransfer';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<!-- get cancel stock transfer detail from cancel stock transfer document no -->
<script>
$(document).ready(function(){

    $(document).on('click', '#btnCancelStockTransferDetail', function(e){
    	
     e.preventDefault();
  
     var docNo = $(this).data('docno'); // get id of clicked row
// 		var docNo = document.getElementById('txtDocNumber').value;
     
     document.getElementById("cclstdocno").innerHTML = "Document No : " + docNo;
  
     $('#dynamic-content').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getcanceltransactiondetail',
          type: 'POST',
          data: 'docNo='+docNo,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content').html(''); // blank before load.
          $('#dynamic-content').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

    });
});
</script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtRefDocNumber:focus').length) {
    	$('#txtRefDocNumber').click();
    }
});
</script>

</body>

</html>