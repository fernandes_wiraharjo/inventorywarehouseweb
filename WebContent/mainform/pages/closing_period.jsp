<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Closing Transaction Period</title>
<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_Closing_Period" action = "${pageContext.request.contextPath}/ClosingPeriod" method="post">
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaction Period
        <small>tables</small>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
        
        <c:if test="${condition == 'SuccessInsertClosingPeriod'}">
		  <div class="alert alert-success alert-dismissible">
	 				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    				  	<h4><i class="icon fa fa-check"></i> Success</h4>
	       			  	Sukses menutup periode dan menambah periode baru.
	     				</div>
			</c:if>
        
        <button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    
		    <div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
	   				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	   				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	   				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
			</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
					<!-- <form> -->
					<label for="lbl-form-title" class="control-label">Enter next period (including fiscal year)</label><br><br>
		          <div id="dvPeriod" class="form-group">
		            <label for="lbl-period" class="control-label">Period (yyyy/mm) :</label><label id="mrkPeriod" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txt-period" name="txt-period" data-inputmask='"mask": "9999/99"' data-mask>
		          </div>
		          <div id="dvYear" class="form-group">
		            <label for="lbl-year" class="control-label">Fiscal Year (yyyy) :</label><label id="mrkYear" for="lbl-validation" class="control-label"><small>*</small></label>
		            <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  	</div>
		            <input type="text" class="form-control" id="txt-year" name="txt-year" data-inputmask='"mask": "9999"' data-mask>
		          </div>
<!-- 		          <div class="form-group"> -->
<!-- 		            <label for="lbl-active" class="control-label">Status :</label></label><label id="mrkStatus" for="lbl-validation" class="control-label"><small>*</small></label> -->
<!-- 		            <select class="form-control select2" style="width: 100%;"> -->
<!--                   <option selected="selected"></option> -->
<!--                   <option>Alaska</option> -->
<!--                   <option>California</option> -->
<!--                   <option>Delaware</option> -->
<!--                   <option>Tennessee</option> -->
<!--                   <option>Texas</option> -->
<!--                   <option>Washington</option> -->
<!--                 </select> -->
<!-- 		          </div> -->
					<!-- </form> -->
		      </div>
		      <div class="modal-footer">
		        <button <c:out value="${buttonstatus}"/> id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')">Update</button>
				<button <c:out value="${buttonstatus}"/> id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')">Save</button>
		      	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		<!--modal Delete -->
<!-- 		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> -->
<!-- 		<div class="modal-dialog" role="document"> -->
<!-- 				<div class="modal-content"> -->
<!-- 							<div class="modal-header">            											 -->
<!-- 									<button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
<!--  										<span aria-hidden="true">&times;</span></button> -->
<!-- 									<h4 class="modal-title">Alert Delete Plant</h4> -->
<!-- 							</div> -->
<!-- 						<div class="modal-body"> -->
<!-- 						<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"  /> -->
<!-- 									<p>Are you sure to delete this plant ?</p> -->
<!-- 						</div> -->
<!-- 		      <div class="modal-footer"> -->
<!-- 		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button> -->
<!-- 		        <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button> -->
<!-- 		      </div> -->
<!-- 		    </div> -->
<!-- 		    /.modal-content -->
<!-- 		  </div> -->
<!-- 		  /.modal-dialog -->
<!-- 		</div> -->
		<!-- /.modal -->	
        

        <table id="tb_transaction_period" class="table table-bordered table-striped table-hover">
        <thead style="background-color: #d2d6de;">
                <tr>
                  <th style="display:none;">Status</th>
                  <th id="thperiod">Period</th>
                  <th id="thYear">Fiscal Year</th>
                  <th id="thStatus">Status</th>
<!--                   <th style="width: 60px"></th> -->
                </tr>
        </thead>
        
        <tbody>
        
        <c:forEach items="${listTransactionPeriod}" var ="transaction">
        
		        <tr>
		        <td style="display:none;"><c:out value="${transaction.isActive}"/></td>
		        <td><c:out value="${transaction.period}"/></td>
		        <td><c:out value="${transaction.fiscal_Year}"/></td>
		        <td><c:out value="${transaction.isActive}"/></td>
<!-- 		        <td><button type="button" class="btn btn-info" -->
<!-- 		        			data-toggle="modal" -->
<!-- 		        			onclick="FuncButtonUpdate()"  -->
<!-- 		        			data-target="#ModalUpdateInsert"  -->
<%-- 		        			data-lplantid='<c:out value="${plant.plantID}"/>'  --%>
<%-- 		        			data-lplantnm='<c:out value="${plant.plantNm}"/>'  --%>
<%-- 		        			data-lplantdesc='<c:out value="${plant.desc}"/>'  --%>
<!-- 		        	><i class="fa fa-edit"></i></button> -->
<%-- 		        <button type="button" class="btn btn-danger" onclick="FuncButtonDelete()" data-toggle="modal" data-target="#ModalDelete" data-lplantid='<c:out value="${plant.plantID}"/>'> --%>
<!-- 		        <i class="fa fa-trash"></i> -->
<!-- 		        </button> -->
<!-- 		        </td> -->
        		</tr>
        		
        </c:forEach>
        
        </tbody>
        </table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- InputMask -->
<script src="mainform/plugins/input-mask/jquery.inputmask.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<!-- $("#tb_master_plant").DataTable(); -->
<!-- paging script -->
<script>
  $(function () {
    $('#tb_transaction_period').DataTable();
    $('#M002').addClass('active');
	$('#M011').addClass('active');
	
	//mask txtperiod
    $("#txt-period").inputmask("yyyymm", {"placeholder": "yyyymm"});
    //mask txtyear
    $("#txt-year").inputmask("yyyy", {"placeholder": "yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();
    
    //shortcut for button 'new'
    Mousetrap.bind('n', function() {
    	FuncButtonNew(),
    	$('#ModalUpdateInsert').modal('show')
    	});
    
    $("#dvErrorAlert").hide();
  });
</script>
<script>
// 	$('#ModalDelete').on('show.bs.modal', function (event) {
// 		var button = $(event.relatedTarget);
// 		var lPlantID = button.data('lplantid');
// 		$("#temp_txtPlantID").val(lPlantID);
// 	})
</script>
<!-- modal script -->
<script>
$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
	$("#dvErrorAlert").hide();
	
  var button = $(event.relatedTarget) // Button that triggered the modal
  
//   var lplantid = button.data('lplantid') // Extract info from data-* attributes
//   var lplantnm = button.data('lplantnm') // Extract info from data-* attributes
//   var lplantdesc = button.data('lplantdesc') // Extract info from data-* attributes
  
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
//   modal.find('.modal-title').text('New message to ' + recipient)

//   modal.find('.modal-body #txt-plant-id').val(lplantid)
//   modal.find('.modal-body #txt-plant-nm').val(lplantnm)
//   modal.find('.modal-body #desc-text').val(lplantdesc)
  $('#txt-period').focus();
})
</script>
<!-- end of modal script -->

<!-- General Script -->
<script>

function FuncClear(){
	$('#mrkPeriod').hide();
	$('#mrkYear').hide();
// 	$('#txt-plant-id').prop('disabled', false);
	
	$('#dvPeriod').removeClass('has-error');
	$('#dvYear').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#txt-period').val('');
	$('#txt-year').val('');
	
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Close Period";	

	FuncClear();
// 	$('#txt-plant-id').prop('disabled', false);
}

// function FuncButtonUpdate() {
// 	$('#btnSave').hide();
// 	$('#btnUpdate').show();
// 	document.getElementById("lblTitleModal").innerHTML = 'Edit Plant';

// 	FuncClear();
// 	$('#txt-plant-id').prop('disabled', true);
// }

function FuncValEmptyInput(lParambtn) {
	var txtPeriod = document.getElementById('txt-period').value;
	var txtYear = document.getElementById('txt-year').value;

	var dvPeriod = document.getElementsByClassName('dvPeriod');
	var dvYear = document.getElementsByClassName('dvYear');
	
// 	if(lParambtn == 'save'){
// 		$('#txt-plant-id').prop('disabled', false);
// 		}
// 		else{
// 		$('#txt-plant-id').prop('disabled', true);
// 		}
	
    if(!txtPeriod.match(/\S/)) {
    	$("#txt-period").focus();
    	$('#dvPeriod').addClass('has-error');
    	$('#mrkPeriod').show();
        return false;
    } 
    
    if(!txtYear.match(/\S/)) {    	
    	$('#txt-year').focus();
    	$('#dvYear').addClass('has-error');
    	$('#mrkYear').show();
        return false;
    } 
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/ClosingPeriod',	
        type:'POST',
        data:{"key":lParambtn,"txtPeriod":txtPeriod,"txtYear":txtYear},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertClosingPeriod')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menutup periode dan menambah periode baru";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txt-period").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/ClosingPeriod';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

</body>
</html>