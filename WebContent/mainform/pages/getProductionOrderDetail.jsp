<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_production_order_detail" class="table table-bordered table-hover">
    <thead style="background-color: #d2d6de;">
    <tr>
    <th>Component Line</th>
	<th>Component ID</th>
	<th>Qty</th>
	<th>UOM</th>
	<th>Qty Base UOM</th>
	<th>Base UOM</th>
   </tr>
   </thead>
     
     <tbody>
     
     <c:forEach items="${listProductionOrderDetail}" var ="productionorderdetail">
       <tr>
	<td><c:out value="${productionorderdetail.componentLine}" /></td>
	<td><c:out value="${productionorderdetail.componentID}" /></td>
	<td><c:out value="${productionorderdetail.qty}" /></td>
	<td><c:out value="${productionorderdetail.UOM}" /></td>
	<td><c:out value="${productionorderdetail.qtyBaseUOM}" /></td>
	<td><c:out value="${productionorderdetail.baseUOM}" /></td>
     </tr>
     		
     </c:forEach>
     
     </tbody>
     </table>

<script>
 		$(function () {
  	  		$("#tb_production_order_detail").DataTable();
  		});
	</script>