<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Customer Type</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="CustomerType" name="CustomerType" action = "${pageContext.request.contextPath}/CustomerType" method="post">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Customer Type <small>tables</small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
							<c:if test="${condition == 'SuccessInsertCustomerType'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambah tipe pelanggan.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessUpdateCustomerType'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses memperbaharui tipe pelanggan.
              				</div>
	      					</c:if>
	     					
	     					<c:if test="${condition == 'SuccessDeleteCustomerType'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menghapus tipe pelanggan.
              				</div>
	      					</c:if>
	      					
	      					<c:if test="${condition == 'FailedDeleteCustomerType'}">
	      						<div class="alert alert-danger alert-dismissible">
                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
                				Gagal menghapus tipe pelanggan. <c:out value="${conditionDescription}"/>.
              				</div>
	     					</c:if>   
	      					
							
							<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
							<table id="tb_itemlib" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Customer Type ID</th>
										<th>Customer Type Name</th>
										<th>Description</th>
										<th style="width: 60px"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listcusttype}" var="custtype">
										<tr>
											<td><c:out value="${custtype.customerTypeID}" /></td>
											<td><c:out value="${custtype.customerTypeName}" /></td>
											<td><c:out value="${custtype.description}" /></td>
											<td><button <c:out value="${buttonstatus}"/> 
														id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
														onclick="FuncButtonUpdate()"
														data-target="#ModalUpdateInsert" 
														data-lcusttypeid='<c:out value="${custtype.customerTypeID}" />'
														data-lcusttypename='<c:out value="${custtype.customerTypeName}" />'
														data-ldescription='<c:out value="${custtype.description}" />'>
														<i class="fa fa-edit"></i></button> 
												<button <c:out value="${buttonstatus}"/> 
														id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" data-toggle="modal" 
														data-target="#ModalDelete" data-lcusttypeid='<c:out value="${custtype.customerTypeID}"/>'>
												<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>

									</c:forEach>
									<!--modal update & Insert -->
									
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
      											
      											<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
							          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
							          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     						</div>
      											
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>		
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvCustTypeID" class="form-group">
	            								<label for="recipient-name" class="control-label">Customer Type ID</label><label id="mrkCustTypeID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtCustTypeID" name="txtCustTypeID">
	          								</div>
	          								<div id="dvCustTypeName" class="form-group">
	            								<label for="message-text" class="control-label">Customer Type Name</label><label id="mrkCustTypeName" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtCustTypeName" name="txtCustTypeName">
	          								</div>
	          								<div class="form-group ">
	            								<label for="message-text" class="control-label">Description</label>
	            								<textarea class="form-control" id="desc-text" name="desc-text"></textarea>
	          								</div>
	          								
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
									<!--modal Delete -->
									<div class="example-modal">
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete Customer Type</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtCustTypeID" name="temp_txtCustTypeID">
               	 									<p>Are you sure to delete this customer type?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								      </div>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_itemlib").DataTable(); 
  		$('#M002').addClass('active');
  		$('#M012').addClass('active');
  		
  	//shortcut for button 'new'
  	    Mousetrap.bind('n', function() {
  	    	FuncButtonNew(),
  	    	$('#ModalUpdateInsert').modal('show')
  	    	});
  	
  	  $("#dvErrorAlert").hide();
  	});
	</script>

<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		var lCustTypeID = button.data('lcusttypeid');
 		var lCustTypeName = button.data('lcusttypename');
 		var lDescription = button.data('ldescription');
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtCustTypeID").val(lCustTypeID);
//  		modal.find(".modal-body #txtCustTypeID").prop('disabled', true);
 		modal.find(".modal-body #txtCustTypeName").val(lCustTypeName);
 		modal.find(".modal-body #desc-text").val(lDescription);
 		
 		if(lCustTypeID == null || lCustTypeID == '')
 			$("#txtCustTypeID").focus();
 		else
 			$("#txtCustTypeName").focus();
	})
</script>

<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lCustTypeID = button.data('lcusttypeid');
		$("#temp_txtCustTypeID").val(lCustTypeID);
	})
	</script>
	
<script>
// var mrkCustTypeID = document.getElementById('mrkCustTypeID').value;
// var mrkCustTypeName = document.getElementById('mrkCustTypeName').value;
// var mrkDesciption = document.getElementById('mrkDesciption').value;

// var btnSave = document.getElementById('btnSave').value;
// var btnUpdate = document.getElementById('btnUpdate').value;

function FuncClear(){
	$('#mrkCustTypeID').hide();
	$('#mrkCustTypeName').hide();
	$('#txtCustTypeID').prop('disabled', false);
// 	$('#mrkDescription').hide();
	
	$('#dvCustTypeID').removeClass('has-error');
	$('#dvCustTypeName').removeClass('has-error');
// 	$('#dvDescription').removeClass('has-error');
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Customer Type";	

	FuncClear();
	$('#txtCustTypeID').prop('disabled', false);
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById("lblTitleModal").innerHTML = 'Edit Customer Type';

	FuncClear();
	$('#txtCustTypeID').prop('disabled', true);
}

function FuncValEmptyInput(lParambtn) {
	var txtCustTypeID = document.getElementById('txtCustTypeID').value;
	var txtCustTypeName = document.getElementById('txtCustTypeName').value;
	var txtDescription = document.getElementById('desc-text').value;

	var dvCustTypeID = document.getElementsByClassName('dvCustTypeID');
	var dvCustomerTypeName = document.getElementsByClassName('dvCustTypeName');
// 	var dvCustomerTypeAddress = document.getElementsByClassName('dvCustTypeAddress');
	
	if(lParambtn == 'save'){
		$('#txtCustTypeID').prop('disabled', false);
	}
	else{
		$('#txtCustTypeID').prop('disabled', true);
	}
	
    if(!txtCustTypeID.match(/\S/)) {
    	$("#txtCustTypeID").focus();
    	$('#dvCustTypeID').addClass('has-error');
    	$('#mrkCustTypeID').show();
        return false;
    } 
    
    if(!txtCustTypeName.match(/\S/)) {    	
    	$('#txtCustTypeName').focus();
    	$('#dvCustTypeName').addClass('has-error');
    	$('#mrkCustTypeName').show();
        return false;
    } 
    
//     if(!txtDescription.match(/\S/)) {
//     	$('#txtDescription').focus();
//     	$('#dvDescription').addClass('has-error');
//     	$('#mrkDescription').show();
//         return false;
//     }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/CustomerType',	
        type:'POST',
        data:{"key":lParambtn,"txtCustTypeID":txtCustTypeID,"txtCustTypeName":txtCustTypeName,"txtDescription":txtDescription},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertCustomerType')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan tipe pelanggan";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtCustTypeID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateCustomerType')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui tipe pelanggan";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtCustTypeName").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/CustomerType';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>


</body>
</html>