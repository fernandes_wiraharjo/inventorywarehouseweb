<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Material Staging Area</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="mtstgarea" name="mtstgarea" action = "${pageContext.request.contextPath}/materialstagingarea" method="post">
		<input type="hidden" name="temp_string" value="" />
	
		<div class="wrapper">	
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					Material Staging Area
				</h1>
				</section>
	
				<!-- Main content -->
				<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<c:if test="${condition == 'SuccessInsertMaterialStagingArea'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menambahkan material staging area.
	           						</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessUpdateMaterialStagingArea'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses memperbaharui material staging area.
	              					</div>
		      					</c:if>
		     					
		     					<c:if test="${condition == 'SuccessDeleteMaterialStagingArea'}">
									<div class="alert alert-success alert-dismissible">
		          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
		                			  	Sukses menghapus material staging area.
	              					</div>
		      					</c:if>
		      					
		      					<c:if test="${condition == 'FailedDeleteMaterialStagingArea'}">
		      						<div class="alert alert-danger alert-dismissible">
		                				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                				<h4><i class="icon fa fa-ban"></i> Failed</h4>
		                				Gagal menghapus material staging area. <c:out value="${conditionDescription}"/>.
	              					</div>
		     					</c:if>
								
								<!--modal update & Insert -->
								<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
										
	  										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				     						</div>
				     					
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
											</div>
		     								<div class="modal-body">
		         								<div id="dvPlantID" class="form-group">
		           								<label for="lblPlantID" class="control-label">Plant ID</label><label id="mrkPlantID" for="formrkPlantID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblPlantName" name="lblPlantName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" 
		           								data-target="#ModalGetPlantID">
		         								</div>
		         								<div id="dvWarehouseID" class="form-group">
		           								<label for="lblWarehouseID" class="control-label">Warehouse ID</label><label id="mrkWarehouseID" for="formrkWarehouseID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblWarehouseName" name="lblWarehouseName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" 
		           								data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
		         								</div>
		         								<div id="dvDoorID" class="form-group">
		           								<label for="lblDoorID" class="control-label">Door ID</label><label id="mrkDoorID" for="formrkDoorID" class="control-label"><small>*</small></label>	
		           								<small><label id="lblDoorName" name="lblDoorName" class="control-label"></label></small>
		           								<input type="text" class="form-control" id="txtDoorID" name="txtDoorID" data-toggle="modal" 
		           								data-target="#ModalGetDoorID" onfocus="FuncValPlantWarehouse()">
		         								</div>
		         								<div id="dvMaterialStagingAreaID" class="form-group">
		           								<label class="control-label">Material Staging Area ID</label><label id="mrkMaterialStagingAreaID" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtMaterialStagingAreaID" name="txtMaterialStagingAreaID">
		         								</div>
		         								<div id="dvMaterialStagingAreaName" class="form-group">
		           								<label class="control-label">Material Staging Area Name</label><label id="mrkMaterialStagingAreaName" class="control-label"><small>*</small></label>	
		           								<input type="text" class="form-control" id="txtMaterialStagingAreaName" name="txtMaterialStagingAreaName">
		         								</div>	
		         								
	          									
	          									<div class="row"></div>
		    								</div>
		    								<div class="modal-footer">
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
		      									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
		      									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    								</div>
										</div>
									</div>
								</div>
								<!-- end of update insert modal -->
								
								<!--modal Delete -->
								<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
	 										<div class="modal-header">            											
	   										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	     										<span aria-hidden="true">&times;</span></button>
	   										<h4 class="modal-title">Alert Delete Material Staging Area</h4>
	 										</div>
		 									<div class="modal-body">
			 									<input type="hidden" id="temp_txtPlantID" name="temp_txtPlantID"  />
			 									<input type="hidden" id="temp_txtWarehouseID" name="temp_txtWarehouseID"  />
			 									<input type="hidden" id="temp_txtDoorID" name="temp_txtDoorID"  />
			 									<input type="hidden" id="temp_txtMaterialStagingAreaID" name="temp_txtMaterialStagingAreaID"  />
			  	 									<p>Are you sure to delete this material staging area ?</p>
		 									</div>
		 									<div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
							              	</div>
							            </div>
							            <!-- /.modal-content -->
									</div>
							          <!-- /.modal-dialog -->
						        </div>
						        <!-- /.modal -->   
							        
						        <!--modal show plant data -->
								<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
									<div class="modal-dialog" role="document">
	 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelPlantID">Data Plant</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<table id="tb_master_plant" class="table table-bordered table-hover">
											        <thead style="background-color: #d2d6de;">
											                <tr>
											                  <th>ID</th>
											                  <th>Name</th>
											                  <th>Description</th>
											                  <th style="width: 20px"></th>
											                </tr>
											        </thead>
											        <tbody>
												        <c:forEach items="${listPlant}" var ="plant">
													        <tr>
														        <td><c:out value="${plant.plantID}"/></td>
														        <td><c:out value="${plant.plantNm}"/></td>
														        <td><c:out value="${plant.desc}"/></td>
														        <td><button type="button" class="btn btn-primary"
														        			data-toggle="modal"
														        			onclick="FuncPassStringPlant('<c:out value="${plant.plantID}"/>','<c:out value="${plant.plantNm}"/>')"
														        			data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
														        </td>
											        		</tr>
												        </c:forEach>
											        </tbody>
									        	</table>
		    								</div>
	   										<div class="modal-footer"></div>
	 										</div>
									</div>
								</div>
								<!-- /. end of modal show plant data -->
								
								<!--modal show warehouse data -->
								<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelWarehouseID">Data Warehouse</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content-warehouse">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show warehouse data -->
								
								<!--modal show door data -->
								<div class="modal fade" id="ModalGetDoorID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelDoorID">
									<div class="modal-dialog" role="document">
 										<div class="modal-content">
	   										<div class="modal-header">
	     											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     											<h4 class="modal-title" id="ModalLabelDoorID">Data Door</h4>	
	   										</div>
		     								<div class="modal-body">
		         								<!-- mysql data load door by plant and warehouse id will be load here -->                          
           										<div id="dynamic-content-door">
           										</div>
		    								</div>
	   										<div class="modal-footer"></div>
 										</div>
									</div>
								</div>
								<!-- /. end of modal show door data -->
								
								<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button><br><br>
								<table id="tb_material_staging_area" class="table table-bordered table-striped table-hover">
									<thead style="background-color: #d2d6de;">
										<tr>
											<th>Plant ID</th>
											<th>Warehouse ID</th>
											<th>Mt. Staging Area ID</th>
											<th>Mt. Staging Area Name</th>
											<th>Door ID</th>
											<th>Door Name</th>
											<th style="width:60px;"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listMaterialStagingArea}" var="stagingarea">
											<tr>
												<td><c:out value="${stagingarea.plantID}" /></td>
												<td><c:out value="${stagingarea.warehouseID}" /></td>
												<td><c:out value="${stagingarea.doorID}" /></td>
												<td><c:out value="${stagingarea.doorName}" /></td>
												<td><c:out value="${stagingarea.materialStagingAreaID}" /></td>
												<td><c:out value="${stagingarea.materialStagingAreaName}" /></td>
												<td><button <c:out value="${buttonstatus}"/>
															id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
															onclick="FuncButtonUpdate()"
															data-target="#ModalUpdateInsert" 
															data-lplantid='<c:out value="${stagingarea.plantID}" />'
															data-lplantname='<c:out value="${stagingarea.plantName}" />'
															data-lwarehouseid='<c:out value="${stagingarea.warehouseID}" />'
															data-lwarehousename='<c:out value="${stagingarea.warehouseName}" />'
															data-ldoorid='<c:out value="${stagingarea.doorID}" />'
															data-ldoorname='<c:out value="${stagingarea.doorName}" />'
															data-lmtstagingareaid='<c:out value="${stagingarea.materialStagingAreaID}" />'
															data-lmtstagingareaname='<c:out value="${stagingarea.materialStagingAreaName}" />'
															>
															<i class="fa fa-edit"></i></button> 
													<button <c:out value="${buttonstatus}"/>
															id="btnModalDelete" name="btnModalDelete" type="button" class="btn btn-danger" 
															data-toggle="modal" 
															data-target="#ModalDelete"
															data-lplantid='<c:out value="${stagingarea.plantID}" />' 
															data-lwarehouseid='<c:out value="${stagingarea.warehouseID}" />'
															data-ldoorid='<c:out value="${stagingarea.doorID}" />' 
															data-lmtstagingareaid='<c:out value="${stagingarea.materialStagingAreaID}" />' 
															>
													<i class="fa fa-trash"></i>
													</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row --> 
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
	
			<%@ include file="/mainform/pages/master_footer.jsp"%>
	
		</div>
		<!-- ./wrapper -->
	</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
	 	$(function () {
	 		$("#tb_material_staging_area").DataTable();
	  		$("#tb_master_plant").DataTable();
	  		$('#M002').addClass('active');
	  		$('#M050').addClass('active');
	  		
	  	//shortcut for button 'new'
		    Mousetrap.bind('n', function() {
		    	FuncButtonNew(),
		    	$('#ModalUpdateInsert').modal('show')
		    	});
  		});

		$('#ModalDelete').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var lPlantID = button.data('lplantid');
			var lWarehouseID = button.data('lwarehouseid');
			var lDoorID = button.data('ldoorid');
			var lMaterialStagingAreaID = button.data('lmtstagingareaid');
			$("#temp_txtPlantID").val(lPlantID);
			$("#temp_txtWarehouseID").val(lWarehouseID);
			$("#temp_txtDoorID").val(lDoorID);
			$("#temp_txtMaterialStagingAreaID").val(lMaterialStagingAreaID);
		})

		$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
			FuncClear();
			
	 		var button = $(event.relatedTarget);
	 		
	 		var lPlantID = button.data('lplantid');
	 		var lPlantName = button.data('lplantname');
	 		var lWarehouseID = button.data('lwarehouseid');
	 		var lWarehouseName = button.data('lwarehousename');
	 		var lDoorID = button.data('ldoorid');
	 		var lDoorName = button.data('ldoorname');
	 		var lMaterialStagingAreaID = button.data('lmtstagingareaid');
	 		var lMaterialStagingAreaName = button.data('lmtstagingareaname');
	 		
	 		var modal = $(this);
	 		
	 		modal.find(".modal-body #txtPlantID").val(lPlantID);
	 		modal.find(".modal-body #txtWarehouseID").val(lWarehouseID);
	 		modal.find(".modal-body #txtDoorID").val(lDoorID);
	 		modal.find(".modal-body #txtMaterialStagingAreaID").val(lMaterialStagingAreaID);
	 		modal.find(".modal-body #txtMaterialStagingAreaName").val(lMaterialStagingAreaName);
	 		
 			
	 		if(lPlantName != null)
	 			document.getElementById("lblPlantName").innerHTML = '(' + lPlantName + ')';
	 			
	 		if(lWarehouseName != null)
	 			document.getElementById("lblWarehouseName").innerHTML = '(' + lWarehouseName + ')';
	 			
	 		if(lDoorName != null)
	 			document.getElementById("lblDoorName").innerHTML = '(' + lDoorName + ')';
	 		
	 		if(lPlantID == null || lPlantID == '')
	 			{
	 				$("#txtPlantID").focus(); 
	 				$("#txtPlantID").click();
	 			}
	 		else
	 			$("#txtMaterialStagingAreaName").focus();
		});
		
		function FuncClear(){
			$('#mrkPlantID').hide();
			$('#mrkWarehouseID').hide();
			$('#mrkDoorID').hide();
			$('#mrkMaterialStagingAreaID').hide();
			$('#mrkMaterialStagingAreaName').hide();
			
			document.getElementById("lblPlantName").innerHTML = null;
			document.getElementById("lblWarehouseName").innerHTML = null;
			document.getElementById("lblDoorName").innerHTML = null;
			
			$('#dvPlantID').removeClass('has-error');
			$('#dvWarehouseID').removeClass('has-error');
			$('#dvDoorID').removeClass('has-error');
			$('#dvMaterialStagingAreaID').removeClass('has-error');
			$('#dvMaterialStagingAreaName').removeClass('has-error');
	
			$("#dvErrorAlert").hide();
		}
	
		function FuncButtonNew() {
			$('#btnSave').show();
			$('#btnUpdate').hide();
			document.getElementById("lblTitleModal").innerHTML = "Add Material Staging Area";
		
			$('#txtPlantID').prop('disabled', false);
			$('#txtWarehouseID').prop('disabled', false);
			$('#txtDoorID').prop('disabled', false);
			$('#txtMaterialStagingAreaID').prop('disabled', false);
		}
		
		function FuncButtonUpdate() {
			$('#btnSave').hide();
			$('#btnUpdate').show();
			document.getElementById("lblTitleModal").innerHTML = 'Edit Material Staging Area';
		
			$('#txtPlantID').prop('disabled', true);
			$('#txtWarehouseID').prop('disabled', true);
			$('#txtDoorID').prop('disabled', true);
			$('#txtMaterialStagingAreaID').prop('disabled', true);
		}
		
		function FuncPassStringPlant(lParamPlantID,lParamPlantName){
			$("#txtPlantID").val(lParamPlantID);
			document.getElementById("lblPlantName").innerHTML = '(' + lParamPlantName + ')';
			$('#mrkPlantID').hide();
			$('#dvPlantID').removeClass('has-error');
		}
		
		function FuncPassStringWarehouse(lParamWarehouseID,lParamWarehouseName){
			$("#txtWarehouseID").val(lParamWarehouseID);
			document.getElementById("lblWarehouseName").innerHTML = '(' + lParamWarehouseName + ')';
			$('#mrkWarehouseID').hide();
			$('#dvWarehouseID').removeClass('has-error');
		}
		
		function FuncPassStringDoor(lParamDoorID,lParamDoorName){
			$("#txtDoorID").val(lParamDoorID);
			document.getElementById("lblDoorName").innerHTML = '(' + lParamDoorName + ')';
			$('#mrkDoorID').hide();
			$('#dvDoorID').removeClass('has-error');
		}
		
		function FuncValPlant(){	
			var txtPlantID = document.getElementById('txtPlantID').value;
			
			if(!txtPlantID.match(/\S/)) {    	
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    } 
			
		    return true;	
		}
		
		function FuncValPlantWarehouse(){	
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			
			if(!txtPlantID.match(/\S/)) {    	
		    	$('#txtPlantID').focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		    	
		    	alert("Fill Plant ID First ...!!!");
		        return false;
		    } 
		    
		    if(!txtWarehouseID.match(/\S/)) {    	
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		    	
		    	alert("Fill Warehouse ID First ...!!!");
		        return false;
		    } 
			
		    return true;	
		}
		
		function FuncValEmptyInput(lParambtn) {
			var txtPlantID = document.getElementById('txtPlantID').value;
			var txtWarehouseID = document.getElementById('txtWarehouseID').value;
			var txtDoorID = document.getElementById('txtDoorID').value;
			var txtMaterialStagingAreaID = document.getElementById('txtMaterialStagingAreaID').value;
			var txtMaterialStagingAreaName = document.getElementById('txtMaterialStagingAreaName').value;
			
		    if(!txtPlantID.match(/\S/)) {
		    	$("#txtPlantID").focus();
		    	$('#dvPlantID').addClass('has-error');
		    	$('#mrkPlantID').show();
		        return false;
		    } 
		    
		    if(!txtWarehouseID.match(/\S/)) {    	
		    	$('#txtWarehouseID').focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		        return false;
		    }
		    
		    if(!txtDoorID.match(/\S/)) {
		    	$('#txtDoorID').focus();
		    	$('#dvDoorID').addClass('has-error');
		    	$('#mrkDoorID').show();
		        return false;
		    }
		    
		    if(!txtMaterialStagingAreaID.match(/\S/)) {
		    	$('#txtMaterialStagingAreaID').focus();
		    	$('#dvMaterialStagingAreaID').addClass('has-error');
		    	$('#mrkMaterialStagingAreaID').show();
		        return false;
		    }
		    
		    if(!txtMaterialStagingAreaName.match(/\S/)) {
		    	$('#txtMaterialStagingAreaName').focus();
		    	$('#dvMaterialStagingAreaName').addClass('has-error');
		    	$('#mrkMaterialStagingAreaName').show();
		        return false;
		    }
		    
		    jQuery.ajax({
		        url:'${pageContext.request.contextPath}/materialstagingarea',	
		        type:'POST',
		        data:{"key":lParambtn,"txtPlantID":txtPlantID,"txtWarehouseID":txtWarehouseID,
		        	  "txtDoorID":txtDoorID,"txtMaterialStagingAreaID":txtMaterialStagingAreaID,
		        	  "txtMaterialStagingAreaName":txtMaterialStagingAreaName},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	if(data.split("--")[0] == 'FailedInsertMaterialStagingArea')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan material staging area";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtPlantID").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else if(data.split("--")[0] == 'FailedUpdateMaterialStagingArea')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui material staging area";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtMaterialStagingAreaName").focus();
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else
		        	{
			        	var url = '${pageContext.request.contextPath}/materialstagingarea';  
			        	$(location).attr('href', url);
		        	}
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    
		    return true;
		}
		
		//action when txtMaterialStagingAreaID changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtMaterialStagingAreaID', function(e){
		  
		     $('#mrkMaterialStagingAreaID').hide();
			 $('#dvMaterialStagingAreaID').removeClass('has-error');
		
		    });
		});
		
		//action when txtMaterialStagingAreaName changed
		$(document).ready(function(){
	
		    $(document).on('change', '#txtMaterialStagingAreaName', function(e){
		  
		     $('#mrkMaterialStagingAreaName').hide();
			 $('#dvMaterialStagingAreaName').removeClass('has-error');
		
		    });
		});
		
		//get warehouse from plant id
		$(document).ready(function(){
	
		    $(document).on('click', '#txtWarehouseID', function(e){
		  
		     e.preventDefault();
		  
			 var plantid = document.getElementById('txtPlantID').value;
		  
		     $('#dynamic-content-warehouse').html(''); // leave this div blank
			//$('#modal-loader').show();      // load ajax loader on button click
		 
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getwarehouse',
		          type: 'POST',
		          data: 'plantid='+plantid,
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-warehouse').html(''); // blank before load.
		          $('#dynamic-content-warehouse').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-warehouse').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//get door from plant and warehouse id
		$(document).ready(function(){
	
		    $(document).on('click', '#txtDoorID', function(e){
		  
		     e.preventDefault();
		  
			 var plantid = document.getElementById('txtPlantID').value;
			 var warehouseid = document.getElementById('txtWarehouseID').value;
		  
		     $('#dynamic-content-door').html(''); // leave this div blank
			//$('#modal-loader').show();      // load ajax loader on button click
		 
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getdoor',
		          type: 'POST',
		          data: {"plantid":plantid,"warehouseid":warehouseid},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-door').html(''); // blank before load.
		          $('#dynamic-content-door').html(data); // load here
				  //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dynamic-content-door').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				  //$('#modal-loader').hide();
		     });
		
		    });
		});
		
		//set the focus behaviour into click
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtWarehouseID:focus').length) {
		    	$('#txtWarehouseID').click();
		    }
		});
		
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9 && $('#txtDoorID:focus').length) {
		    	$('#txtDoorID').click();
		    }
		});
	</script>
</body>
</html>