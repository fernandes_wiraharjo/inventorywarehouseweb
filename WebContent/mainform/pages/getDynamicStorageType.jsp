<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_storagetype" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>Plant ID</th>
			<th>Warehouse ID</th>
			<th>Storage Type</th>
			<th>Storage Type Name</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listStorageType}" var ="stortype">
	  <tr>
	  <td><c:out value="${stortype.plantID}" /></td>
		<td><c:out value="${stortype.warehouseID}" /></td>
		<td><c:out value="${stortype.storageTypeID}" /></td>
		<td><c:out value="${stortype.storageTypeName}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassDynamicStorageTypeData('<c:out value="${stortype.storageTypeID}"/>','<c:out value="${type}"/>','<c:out value="${stortype.storageTypeName}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_storagetype").DataTable();
  	});
</script>