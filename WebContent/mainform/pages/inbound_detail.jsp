<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- <% --%>
<!-- //get docNo from inbound.jsp -->
<!-- // String docNo = null; -->
<!-- // if(request.getParameter("docNo") == null){ -->
<!-- // 	response.sendRedirect("/InventoryWarehouseWeb/Inbound"); -->
<!-- // }else  -->
<!-- // 	{docNo = (String) request.getParameter("docNo"); -->
<!-- // 	} -->
<!-- %> -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Inbound Detail</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert,#ModalCreateTO,#ModalPalletization { overflow-y:scroll };
</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="InboundDetail" name="InboundDetail" action = "${pageContext.request.contextPath}/InboundDetail" method="post">
<input  type="hidden" id="temp_vendorid" name="temp_vendorid" value="<c:out value="${temp_vendorid}"/>" />
<input  type="hidden" id="temp_plantid" name="temp_plantid" value="<c:out value="${temp_plantid}"/>" />
<input  type="hidden" id="temp_warehouseid" name="temp_warehouseid" value="<c:out value="${temp_warehouseid}"/>" />

<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Inbound Detail <br> <br> <small style="color: black; font-weight: bold;">Document No : <c:out value="${docNo}"/></small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
						
						<!-- declare docNo from servlet -->
						<c:set var="docNo" value="${docNo}"></c:set>
						
							<c:if test="${condition == 'SuccessInsertInbound'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan dokumen masuk barang. Silahkan mengisi detil dokumen.
              				</div>
	      					</c:if>  
						
							<c:if test="${condition == 'SuccessInsertInboundDetail'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Sukses menambahkan dokumen detil masuk barang.
              				</div>
	      					</c:if> 
	      					
	      					<!--modal update & Insert -->
									<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvDocNumber" class="form-group col-xs-6">
	            								<label for="recipient-name" class="control-label">Document No</label><label id="mrkDocNumber" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDocNumber" name="txtDocNumber" readonly="readonly" value="<c:out value="${docNo}"/>">
	          								</div>
	          								<div id="dvDocLine" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Doc Line</label><label id="mrkDocLine" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtDocLine" name="txtDocLine" readonly="readonly" value="<c:out value="${tempdocLine}"/>">
												<!-- type="number"  -->
	          								</div>
	          								<div id="dvProductID" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Product ID</label><label id="mrkProductID" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<small><label id="lblProductName" name="lblProductName" class="control-label"></label></small>
	            								<input type="text" class="form-control" id="txtProductID" name="txtProductID" data-toggle="modal" data-target="#ModalGetProductID">
	          								</div>
	          								<div id="dvQtyUOM" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Qty UOM</label>	
	            								<input type="number" class="form-control" id="txtQtyUOM" name="txtQtyUOM">
	          								</div>
	          								<div id="dvUOM" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">UOM</label><label id="mrkUOM" for="recipient-name" class="control-label"><small>*</small></label>	
	            								
	            								<select id="slMasterUOM" name="slMasterUOM" class="form-control" onfocus="FuncValShowUOM()">
							                    </select>
           										
	          								</div>
<!-- 	          								<div id="dvQtyBaseUOM" class="form-group"> -->
<!-- 	            								<label for="message-text" class="control-label">Qty Base UOM (Pcs)</label><label id="mrkQtyBaseUOM" for="recipient-name" class="control-label"><small>*</small></label>	 -->
<!-- 	            								<input type="number" class="form-control" id="txtQtyBaseUOM" name="txtQtyBaseUOM"> -->
<!-- 	          								</div> -->
	          								<div id="dvVendorBatchNo" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Packing No</label><label id="mrkVendorBatchNo" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtVendorBatchNo" name="txtVendorBatchNo">
	          								</div>
											<!-- <div id="dvPackingNo" class="form-group col-xs-6"> -->
											<!-- <label for="message-text" class="control-label">Packing No</label><label id="mrkPackingNo" for="recipient-name" class="control-label"><small>*</small></label>	 -->
											<!-- <input type="text" class="form-control" id="txtPackingNo" name="txtPackingNo"> -->
											<!-- </div> -->
	          								<div id="dvBatchNo" class="form-group col-xs-6">
							            	 	<label for="message-text" class="control-label">Batch No</label><label id="mrkBatchNo" for="recipient-name" class="control-label"><small>*</small></label>	
							            	 	<input type="text" class="form-control" id="txtBatchNo" name="txtBatchNo" data-toggle="modal" data-target="#ModalGetBatchNo" onfocus="FuncValShowBatch()" readonly="readonly">
							          	 	</div>
	          								<div id="dvExpiredDate" class="form-group col-xs-6">
	            								<label for="message-text" class="control-label">Expired Date</label><label id="mrkExpiredDate" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtExpiredDate" name="txtExpiredDate" data-date-format="dd M yyyy">
	          								</div>
	          								
	          								<div class="row"></div>	 
	        								
      								</div>
      								
      								<div class="modal-footer">
        									<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')" <c:out value="${addnewButtonStatus}"/>>Save</button>
        									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
									
									<!--modal create transfer order -->
									<div class="modal fade bs-example-modal-lg" id="ModalCreateTO" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog modal-lg" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlertCreateTO" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlertCreateTO"></label>. <label id="lblAlertCreateTODescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title"><label>Create TO (Inbound Doc = '<c:out value="${docNo}"/>') : Overview of Transfer Requirement</label></h4>	
      												<br><br>
      												<div class="row">
	      												<div class="col-xs-3">
										            	 	<label for="message-text" class="control-label">Movement Type</label>	
										            	 	<small><label id="lblMovementTypeName" name="lblMovementTypeName" class="control-label"></label></small>
										            	 	<input type="text" class="form-control" id="txtMovementType" name="txtMovementType" readonly="readonly">
	      												</div>
	      												<div class="col-xs-3">
	      													<label for="message-text" class="control-label">Source Stor Type</label>	
										            	 	<input type="text" class="form-control" id="txtSourceStorType" name="txtSourceStorType" readonly="readonly">
	      												</div>
	      												<div class="col-xs-3">
	      													<label for="message-text" class="control-label">Dest Stor Type</label>	
										            	 	<input type="text" class="form-control" id="txtDestStorType" name="txtDestStorType" readonly="readonly">
	      												</div>
      												</div>
      												
      												<div class="row">
      												<div class="col-xs-3">
	      													<label for="message-text" class="control-label">Transfer Priority</label>	
										            	 	<input type="text" class="form-control" id="txtTransferPriority" name="txtTransferPriority" readonly="readonly">
	      												</div>
	      												<div class="col-xs-3">
										            	 	<label for="message-text" class="control-label">Planned Date</label>	
										            	 	<input type="text" class="form-control" id="txtPlannedDate" name="txtPlannedDate" readonly="readonly">
	      												</div>
	      												<div class="col-xs-3">
	      													<label for="message-text" class="control-label">Planned Time</label>	
										            	 	<input type="text" class="form-control" id="txtPlannedTime" name="txtPlannedTime" readonly="readonly">
	      												</div>
      												</div>
      											</div>
      											
			      								<div class="modal-body">			      									
           											<div id="dynamic-transfer-requirement"></div>
		      									</div>
      								
			      								<div class="modal-footer">
			        									<button type="button" class="btn btn-primary" id="btnSaveTO" name="btnSaveTO" onclick="FuncSaveTO()">Save</button>
			        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      								</div>
      								
    										</div>
  										</div>
									</div>
									
									<!--modal palletization -->
									<div class="modal fade bs-example-modal-lg" id="ModalPalletization" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog modal-lg" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlertPalletization" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlertPalletization"></label>. <label id="lblAlertPalletizationDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title"><label>Create TO (Inbound Doc = '<c:out value="${docNo}"/>') : Prepare for Putaway</label></h4>	
      												<br><br>
      												<div class="row">
	      												<div class="col-xs-4">
										            	 	<label for="message-text" class="control-label">Product ID</label>	
										            	 	<small><label id="lblPalletizationProductName" name="lblPalletizationProductName" class="control-label"></label></small>
										            	 	<input type="text" class="form-control" id="txtPalletizationProductID" name="txtPalletizationProductID" readonly="readonly">
	      												</div>
	      												<div class="col-xs-4">
	      													<label for="message-text" class="control-label">Plant ID</label>	
										            	 	<input type="text" class="form-control" id="txtPalletizationPlantID" name="txtPalletizationPlantID" readonly="readonly">
	      												</div>
	      												<div class="col-xs-4">
	      													<label for="message-text" class="control-label">WarehouseID</label>	
										            	 	<input type="text" class="form-control" id="txtPalletizationWarehouseID" name="txtPalletizationWarehouseID" readonly="readonly">
	      												</div>
      												</div>
      												
      												<div class="row">
      												<div class="col-xs-4">
	      													<label for="message-text" class="control-label">Movement Type</label>
	      													<small><label id="lblPalletizationMovementTypeName" name="lblPalletizationMovementTypeName" class="control-label"></label></small>	
										            	 	<input type="text" class="form-control" id="txtPalletizationMovementType" name="txtPalletizationMovementType" readonly="readonly">
	      												</div>
	      												<div class="col-xs-4">
										            	 	<label for="message-text" class="control-label">Source Stor.Bin</label>	
										            	 	<input type="text" class="form-control" id="txtPalletizationSourceStorBin" name="txtPalletizationSourceStorBin" readonly="readonly">
	      												</div>
	      												<div class="col-xs-4">
	      													<label for="message-text" class="control-label">GR Date</label>	
										            	 	<input type="text" class="form-control" id="txtGRDate" name="txtGRDate" readonly="readonly">
	      												</div>
      												</div>
      											</div>
      											
			      								<div class="modal-body">
			      										
			      									<div class="row">
			      										<div class="col-xs-6">
			      											<label for="recipient-name" class="control-label">Palletization</label>
							   								<div class="panel panel-primary" id="PalletizationControlPanel">
							   								<div class="panel-body fixed-panel">
							   									<div class="row">
								   									<div class="form-group col-xs-2">
							            								<label class="control-label">SU</label></label>	
							           									<input type="text" class="form-control" id="txtSU" name="txtSU" readonly="readonly">
																	</div>
																	<div class="form-group col-xs-7">
						            									<label class="control-label">Qty per SUnit</label></label>
						            									<input type="text" class="form-control" id="txtSUQty" name="txtSUQty" readonly="readonly">
																	</div>
																	<div class="form-group col-xs-3">
							            								<label class="control-label">SUT</label></label>	
							           									<input type="text" class="form-control" id="txtSUT" name="txtSUT" readonly="readonly">
																	</div>
																</div>
																
																<div class="row">
								   									<div class="form-group col-xs-2">
							           									<input type="text" class="form-control" id="txtSU2" name="txtSU2" readonly="readonly">
																	</div>
																	<div class="form-group col-xs-7">
						            									<input type="text" class="form-control" id="txtSUQty2" name="txtSUQty2" readonly="readonly">
																	</div>
																	<div class="form-group col-xs-3">	
							           									<input type="text" class="form-control" id="txtSUT2" name="txtSUT2" readonly="readonly">
																	</div>
																</div>
							   								</div>
							   								</div>
			      										</div>
			      										
			      										<div class="col-xs-6">
			      											<label for="recipient-name" class="control-label">Quantities</label>
							   								<div class="panel panel-primary" id="QuantitiesControlPanel">
							   								<div class="panel-body fixed-panel">
							   									<div class="row">
								   									<div class="form-group col-xs-4">
								   										<label class="control-label">Stck plcmnt qty</label></label>
																	</div>
																	<div class="form-group col-xs-4">
																		<input type="text" class="form-control" id="txtStockPlacementQty" name="txtStockPlacementQty" readonly="readonly">
																	</div>
																	<div class="form-group col-xs-4">
																		<input type="text" class="form-control" id="txtStockPlacementQtyUOM" name="txtStockPlacementQtyUOM" readonly="readonly">
																	</div>
																	<div class="form-group col-xs-4">
																		<label class="control-label">Open Quantity</label></label>
																	</div>
																	<div class="form-group col-xs-8">
							           									<input type="text" class="form-control" id="txtOpenQty" name="txtOpenQty" readonly="readonly">
																	</div>
																	<div class="form-group col-xs-4">
								   										<label class="control-label">Total TO items</label></label>
																	</div>
																	<div class="form-group col-xs-8">
							           									<input type="text" class="form-control" id="txtTotalTOItem" name="txtTotalTOItem" readonly="readonly">
																	</div>
																</div>
							   								</div>
							   								</div>
			      										</div>
			      									</div>	
			      									      									
												<div id="dynamic-placement-data"></div>
		      									</div>
      								
			      								<div class="modal-footer">
			        								<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
			      								</div>
      								
    										</div>
  										</div>
									</div>
								      
								      
								      <!--modal show product data -->
										<div class="modal fade" id="ModalGetProductID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelProductID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelProductID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_product" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Product ID</th>
														<th>Product Name</th>
														<th>Description</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listProduct}" var ="product">
												        <tr>
												        <td><c:out value="${product.id}" /></td>
														<td><c:out value="${product.title_en}" /></td>
														<td><c:out value="${product.short_description_en}" /></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassStringProduct('<c:out value="${product.id}"/>','<c:out value="${product.title_en}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
										<!-- /. end of modal show product data -->
										
										<!--modal show Batch data -->
										<div class="modal fade bs-example-modal-lg" id="ModalGetBatchNo" tabindex="-1" role="dialog" aria-labelledby="ModalLabelBatchID">
										<div class="modal-dialog modal-lg" role="document">
										  	 <div class="modal-content">
										    	 <div class="modal-header">
										      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										      	 <h4 class="modal-title" id="ModalLabelBatchID"></h4>	
										    	 </div>
										     	 <div class="modal-body">
										       	
										       	<!-- mysql data load batch by productid will be load here -->                          
           										<div id="dynamic-content">
           										</div>
           										
<!-- 										         	 <table id="tb_master_batch" class="table table-bordered table-hover"> -->
<!-- 										        <thead style="background-color: #d2d6de;"> -->
<!-- 										                <tr> -->
<!-- 										                <th>Batch No</th> -->
<!-- 										<th>Product ID</th> -->
<!-- 										<th>Packing No</th> -->
<!-- 										<th>GRDate</th> -->
<!-- 										<th>Expired Date</th> -->
<!-- 										<th>Vendor Batch No</th> -->
<!-- 										<th>Vendor ID</th> -->
<!-- 										<th style="width: 20px"></th> -->
<!-- 										                </tr> -->
<!-- 										        </thead> -->
										        
<!-- 										        <tbody> -->
										        
<%-- 										        <c:forEach items="${listBatch}" var ="batch"> --%>
<!-- 										        <tr> -->
<%-- 										        <td><c:out value="${batch.batch_No}" /></td> --%>
<%-- 										<td><c:out value="${batch.productID}" /></td> --%>
<%-- 										<td><c:out value="${batch.packing_No}" /></td> --%>
<%-- 										<td><c:out value="${batch.GRDate}" /></td> --%>
<%-- 										<td><c:out value="${batch.expired_Date}" /></td> --%>
<%-- 										<td><c:out value="${batch.vendor_Batch_No}" /></td> --%>
<%-- 										<td><c:out value="${batch.vendorID}" /></td> --%>
<!-- 										        <td><button type="button" class="btn btn-primary" -->
<!-- 										        	 data-toggle="modal" -->
<%-- 										        	 onclick="FuncPassStringBatch('<c:out value="${batch.batch_No}"/>')" --%>
<!-- 										        	 data-dismiss="modal" -->
<!-- 										        	><i class="fa fa-fw fa-check"></i></button> -->
<!-- 										        </td> -->
<!-- 										        	 </tr> -->
<%-- 										        </c:forEach> --%>
										        
<!-- 										        </tbody> -->
<!-- 										        </table> -->
										       	
										    	 </div>
										    	
										    	 <div class="modal-footer">
										      	
										    	 </div>
										  	 </div>
										</div>
										</div>
  										<!-- /. end of modal show Batch data -->
						
						
							<button id="btnCreateTO" name="btnCreateTO" type="button" class="btn btn-primary pull-left" data-toggle="modal" data-target="#ModalCreateTO" style="display: <c:out value="${createTOButtonStatus}"/>"><i class="fa fa-plus-circle"></i> Create Transfer Order</button>
							<button id="btnShowTO" name="btnShowTO" type="button" class="btn btn-primary pull-left" style="display: <c:out value="${showTOButtonStatus}"/>" onclick="FuncShowTO()"> Show Transfer Order</button>
							<button id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()" <c:out value="${addnewButtonStatus}"/>><i class="fa fa-plus-circle"></i> New</button>
							<br><br>
							<table id="tb_inbound_detail" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Doc No</th>
										<th>DocLine</th>
										<th>Product ID</th>
										<th>Qty UOM</th>
										<th>UOM</th>
										<th>Qty Base UOM</th>
										<th>Base UOM</th>
										<th>Batch No</th>
										<th>Vendor Batch No</th>
										<th>Packing No</th>
										<th>Expired Date</th>
<!-- 										<th style="width:20px;"></th> -->
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listinboundDetail}" var="inbounddetail">
										<tr>
											<td><c:out value="${inbounddetail.docNumber}" /></td>
											<td><c:out value="${inbounddetail.docLine}" /></td>
											<td><c:out value="${inbounddetail.productID}" /></td>
											<td><c:out value="${inbounddetail.qtyUom}" /></td>
											<td><c:out value="${inbounddetail.uom}" /></td>
											<td><c:out value="${inbounddetail.qtyBaseUom}" /></td>
											<td><c:out value="${inbounddetail.baseUom}" /></td>
											<td><c:out value="${inbounddetail.batchNo}" /></td>
											<td><c:out value="${inbounddetail.vendorBatchNo}" /></td>
											<td><c:out value="${inbounddetail.packingNo}" /></td>
											<td><c:out value="${inbounddetail.expiredDate}" /></td>
<!-- 											<td><button id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal"  -->
<!-- 														onclick="FuncButtonUpdate()" -->
<!-- 														data-target="#ModalUpdateInsert"  -->
<%-- 														data-ldocnumber='<c:out value="${inbounddetail.docNumber}" />' --%>
<%-- 														data-ldocline='<c:out value="${inbounddetail.docLine}" />' --%>
<%-- 														data-lproductid='<c:out value="${inbounddetail.productID}" />' --%>
<%-- 														data-lqtyuom='<c:out value="${inbounddetail.qtyUom}" />' --%>
<%-- 														data-lqtybaseuom='<c:out value="${inbounddetail.qtyBaseUom}" />' --%>
<%-- 														data-lvendorbatchno='<c:out value="${inbounddetail.vendorBatchNo}" />' --%>
<%-- 														data-lpackingno='<c:out value="${inbounddetail.packingNo}" />' --%>
<%-- 														data-lexpireddate='<c:out value="${inbounddetail.expiredDate}" />' --%>
<!-- 														> -->
<!-- 														<i class="fa fa-edit"></i></button> -->
<!-- 											</td> -->
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_inbound_detail").DataTable();
  		$("#tb_master_product").DataTable();
  		$('#M004').addClass('active');
  		$('#M026').addClass('active');
  		
  		$("#dvErrorAlert").hide();
  		$("#dvErrorAlertCreateTO").hide();
  		$("#dvErrorAlertPalletization").hide();
  	});
 	
 	//shortcut for button 'new'
    Mousetrap.bind('n', function() {
    	FuncButtonNew(),
    	$('#ModalUpdateInsert').modal('show')
    	});
	</script>

<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
	
 		var button = $(event.relatedTarget);
 		var lDocNumber = button.data('ldocnumber');
 		var lDocLine = button.data('ldocline');
 		var lProductID = button.data('lproductid');
 		var lUOM = button.data('luom');
 		var lQtyUOM = button.data('lqtyuom');
		//var lQtyBaseUOM = button.data('lqtybaseuom');
 		var lVendorBatchNo = button.data('lvendorbatchno');
 		var lPackingNo = button.data('lpackingno');
 		var lExpiredDate = button.data('lexpireddate');
 		var lBatchNo= button.data('lbatchno');
 		
 		var modal = $(this);
 		
 		if(lDocNumber == undefined)
 			{
 			}
 		else
 			{
 			modal.find(".modal-body #txtDocNumber").val(lDocNumber);
 	 		modal.find(".modal-body #txtDocLine").val(lDocLine);
 	 		modal.find(".modal-body #txtProductID").val(lProductID);
 	 		modal.find(".modal-body #slMasterUOM").val(lUOM);
 	 		modal.find(".modal-body #txtQtyUOM").val(lQtyUOM);
			//modal.find(".modal-body #txtQtyBaseUOM").val(lQtyBaseUOM);
 	 		modal.find(".modal-body #txtVendorBatchNo").val(lVendorBatchNo);
 	 		modal.find(".modal-body #txtPackingNo").val(lPackingNo);
 	 		modal.find(".modal-body #txtExpiredDate").val(lExpiredDate);
 	 		modal.find(".modal-body #txtBatchNo").val(lBatchNo);
 			}
 		
 		$('#txtProductID').focus();
 		$('#txtProductID').click();
	})
	
	$('#txtExpiredDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
	    
	    
	$('#ModalGetProductID').on('shown.bs.modal', function (event) {
		$('#slMasterUOM').find('option').remove();
	});
	
	$('#ModalCreateTO').on('shown.bs.modal', function (event) {
		//$("#dvErrorAlert").hide();
		
		//get date time now js
		Number.prototype.padLeft = function(base,chr){
	    var  len = (String(base || 10).length - String(this).length)+1;
	    return len > 0? new Array(len).join(chr || '0')+this : this;
		}
	
		var d = new Date,
	    dformat = [(d.getMonth()+1).padLeft(),
	               d.getDate().padLeft(),
	               d.getFullYear()].join('.');
	    tformat = [d.getHours().padLeft(),
	               d.getMinutes().padLeft(),
	               d.getSeconds().padLeft()].join(':');
 		
 		//declare field content
		$("#txtMovementType").val('101');
		document.getElementById("lblMovementTypeName").innerHTML = '(GR)';
		$("#txtPlannedDate").val(dformat);
		$("#txtPlannedTime").val(tformat);
		
		//get source and destination of stor type
		var txtMovementType = document.getElementById('txtMovementType').value;
		var txtPlantID = document.getElementById('temp_plantid').value;
		var txtWarehouseID = document.getElementById('temp_warehouseid').value;
		
		if(txtMovementType != '' || txtMovementType != null) {
			$.ajax({
		          url: '${pageContext.request.contextPath}/getStorTypeFromMovementType',
		          type: 'POST',
		          data: {movementtype : txtMovementType, plantid: txtPlantID, warehouseid: txtWarehouseID},
		          dataType: 'json'
		     })
		     .done(function(data){
		          console.log(data);
		          
		          for (var i in data) {
				  	var obj = data[i];
				  	var index = 0;
				  	var srcstoragetype, srcstoragebin, deststoragetype, deststoragebin;
					for (var prop in obj) {
				      switch (index++) {
				          case 0:
				              srcstoragetype = obj[prop];
				              break;
				          case 1:
				              deststoragetype = obj[prop];
				              break;
				          case 2:
				              srcstoragebin = obj[prop];
				              break;
				          case 3:
				              deststoragebin = obj[prop];
				              break;
				          default:
				              break;
			     	 	}
				  	}
				  
				  $('#txtSourceStorType').val(srcstoragetype + ' -- ' + srcstoragebin);
				  $('#txtDestStorType').val(deststoragetype + ' -- ' + deststoragebin);
				}
		     })
		     .fail(function(){
		          console.log('Service call failed!');
		     });
		}
		
		//get transfer requirement list
		var txtDocNumber = document.getElementById('txtDocNumber').value;
		
		if(txtDocNumber != '' || txtDocNumber != null){
			$('#dynamic-transfer-requirement').html('');
			
			 $.ajax({
		          url: '${pageContext.request.contextPath}/getTransferRequirement',
		          type: 'POST',
		          data: 'inbounddoc='+txtDocNumber,
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-transfer-requirement').html(''); // blank before load.
		          $('#dynamic-transfer-requirement').html(data); // load here
		     })
		     .fail(function(){
		          $('#dynamic-transfer-requirement').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
		     });
		}
		
	})
	
	//MODAL PALLETIZATION
	$('#ModalPalletization').on('shown.bs.modal', function (event) {
		
		//variable initialization
		var txtMovementType = document.getElementById('txtMovementType').value;
		
		var button = $(event.relatedTarget);
		var txtDocNumber = document.getElementById('txtDocNumber').value;
 		var lProductID = button.data('lproductid');
 		var lProductName = button.data('lproductnm');
 		var lBatchNo = button.data('lbatchno');
 		var lInboundDate = button.data('linbounddate');
 		var lPlantID = button.data('lplantid');
 		var lWarehouseID = button.data('lwarehouseid');
 		var lInboundQty = button.data('linboundqty');
 		var lInboundUOM = button.data('linbounduom');
 		
 		//declare field content
 		$("#txtPalletizationProductID").val(lProductID);
 		document.getElementById("lblPalletizationProductName").innerHTML = '('+lProductName+')';
 		$("#txtPalletizationPlantID").val(lPlantID);
 		$("#txtPalletizationWarehouseID").val(lWarehouseID);
		$("#txtPalletizationMovementType").val('101');
		document.getElementById("lblPalletizationMovementTypeName").innerHTML = '(GR)';
		$("#txtGRDate").val(lInboundDate);
		
		//get source of stor type
		if(txtMovementType != '' || txtMovementType != null) {
			$.ajax({
		          url: '${pageContext.request.contextPath}/getStorTypeFromMovementType',
		          type: 'POST',
		          data: {movementtype : txtMovementType, plantid: lPlantID, warehouseid: lWarehouseID},
		          dataType: 'json'
		     })
		     .done(function(data){
		          console.log(data);
		          
		          for (var i in data) {
				  	var obj = data[i];
				  	var index = 0;
				  	var srcstoragetype, srcstoragebin, deststoragetype, deststoragebin;
					for (var prop in obj) {
				      switch (index++) {
				          case 0:
				              srcstoragetype = obj[prop];
				              break;
				          case 1:
				              deststoragetype = obj[prop];
				              break;
				          case 2:
				              srcstoragebin = obj[prop];
				              break;
				          case 3:
				              deststoragebin = obj[prop];
				              break;
				          default:
				              break;
			     	 	}
				  	}
				  
				  $('#txtPalletizationSourceStorBin').val(srcstoragetype + ' -- ' + srcstoragebin);
				}
		     })
		     .fail(function(){
		          console.log('Service call failed!');
		     });
		}
		
		//get product palletization
		if(lProductID != null && lInboundQty != null && lInboundUOM != null) {
			$.ajax({
		          url: '${pageContext.request.contextPath}/getProductPalletization',
		          type: 'POST',
		          data: {productid: lProductID, productqty: lInboundQty, productuom: lInboundUOM, plantid: lPlantID, warehouseid: lWarehouseID},
		          dataType: 'json'
		     })
		     .done(function(data){
		          console.log(data);
		          
		          for (var i in data) {
				  	var obj = data[i];
				  	var index = 0;
				  	var SU1, SU2, QtyPerSUnit1, QtyPerSUnit2, SUT1, SUT2, StockPlacementQty, StockPlacementQtyUOM, OpenQty, TotalTO;
					for (var prop in obj) {
				      switch (index++) {
				          case 0:
				              SU1 = obj[prop];
				              break;
				          case 1:
				              SU2 = obj[prop];
				              break;
				          case 2:
				              QtyPerSUnit1 = obj[prop];
				              break;
				          case 3:
				              QtyPerSUnit2 = obj[prop];
				              break;
				          case 4:
				              SUT1 = obj[prop];
				              break;
				          case 5:
				              SUT2 = obj[prop];
				              break;
				           case 6:
				              StockPlacementQty = obj[prop];
				              break;
				          case 7:
				              StockPlacementQtyUOM = obj[prop];
				              break;
				          case 8:
				              OpenQty = obj[prop];
				              break;
				          case 9:
				              TotalTO = obj[prop];
				              break;
				          default:
				              break;
			     	 	}
				  	}
				  	
					  $('#txtSU').val(SU1);
					  $('#txtSU2').val(SU2);
					  $('#txtSUQty').val(QtyPerSUnit1);
					  $('#txtSUQty2').val(QtyPerSUnit2);
					  $('#txtSUT').val(SUT1);
					  $('#txtSUT2').val(SUT2);
					  $('#txtStockPlacementQty').val(StockPlacementQty);
					  $('#txtStockPlacementQtyUOM').val(StockPlacementQtyUOM);
					  $('#txtOpenQty').val(OpenQty);
					  $('#txtTotalTOItem').val(TotalTO);
				}
					
				//setting for placement
				if(lProductID != null && lPlantID != null && lWarehouseID != null) {
					var txtSU = document.getElementById('txtSU').value;
					if(txtSU==''){
						$('#dynamic-placement-data').html('<i class="glyphicon glyphicon-info-sign"></i> Satuan produk di inbound belum didefinisikan di palletization produk...');
						return false;
					}
					
					var txtSU2 = document.getElementById('txtSU2').value;
					var txtSUQty = document.getElementById('txtSUQty').value;
					var txtSUQty2 = document.getElementById('txtSUQty2').value;
					var txtSUT = document.getElementById('txtSUT').value;
					var txtSUT2 = document.getElementById('txtSUT2').value;
					var txtSourceStorType = document.getElementById('txtPalletizationSourceStorBin').value.split(" -- ")[0];
					var txtSourceStorBin = document.getElementById('txtPalletizationSourceStorBin').value.split(" -- ")[1];
					
					$.ajax({
				          url: '${pageContext.request.contextPath}/setPlacement',
				          type: 'POST',
				          data: {productid: lProductID, plantid: lPlantID, warehouseid: lWarehouseID, su: txtSU,
				          		su2: txtSU2, suqty: txtSUQty, suqty2: txtSUQty2, sut: txtSUT, sut2: txtSUT2, 
				          		batchno: lBatchNo, productuom: lInboundUOM, inbounddoc: txtDocNumber, 
				          		srcstortype: txtSourceStorType, srcstorbin: txtSourceStorBin},
				          dataType: 'html'
				     })
				     .done(function(data){
				          console.log(data); 
				          $('#dynamic-placement-data').html(''); // blank before load.
				          $('#dynamic-placement-data').html(data); // load here
				     })
				     .fail(function(){
				          $('#dynamic-placement-data').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				     });
				}
		     })
		     .fail(function(){
		          console.log('Service call failed!');
		     });
		}
		
	})

//SAVE TRANSFER ORDER
function FuncSaveTO() {
	var txtDocNumber = document.getElementById('txtDocNumber').value;
	   
  	var url = '${pageContext.request.contextPath}/TransferOrder?docNo='+txtDocNumber+'&key=create';  
   	$(location).attr('href', url);
}

//LOAD TRANSFER ORDER
function FuncShowTO() {
	var txtDocNumber = document.getElementById('txtDocNumber').value;
	   
  	var url = '${pageContext.request.contextPath}/TransferOrder?docNo='+txtDocNumber+'&key=load';  
   	$(location).attr('href', url);
}
</script>

<script>
function FuncPassStringProduct(lParamProductID,lParamProductName){
	$("#txtProductID").val(lParamProductID);
	document.getElementById("lblProductName").innerHTML = '(' + lParamProductName + ')';
	
	FuncShowUOM();
}

// function FuncPassStringBatch(lParamBatchNo){
// 	$("#txtBatchNo").val(lParamBatchNo);
// 	}
function FuncPassString(lParam1,lParam2,lParamBatchNo){
	$("#txtBatchNo").val(lParamBatchNo);
	}
</script>

<script>
function FuncClear(){
	$('#mrkDocNumber').hide();
	$('#mrkDocLine').hide();
	$('#mrkProductID').hide();
	$('#mrkUOM').hide();
// 	$('#mrkQtyBaseUOM').hide();
	$('#mrkVendorBatchNo').hide();
	$('#mrkPackingNo').hide();
	$('#mrkExpiredDate').hide();
	$('#mrkBatchNo').hide();
	
	$('#dvDocNumber').removeClass('has-error');
	$('#dvDocLine').removeClass('has-error');
	$('#dvProductID').removeClass('has-error');
	$('#dvUOM').removeClass('has-error');
// 	$('#dvQtyBaseUOM').removeClass('has-error');
	$('#dvVendorBatchNo').removeClass('has-error');
	$('#dvPackingNo').removeClass('has-error');
	$('#dvExpiredDate').removeClass('has-error');
	$('#dvBatchNo').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	
	$('#txtProductID').val('');
	$('#txtQtyUOM').val('');
	$('#slMasterUOM').val('');
// 	$('#txtQtyBaseUOM').val('');
	$('#txtVendorBatchNo').val('');
	$('#txtPackingNo').val('');
	$('#txtExpiredDate').val('');
	$('#txtBatchNo').val('');
	
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Inbound Detail";
	document.getElementById("lblProductName").innerHTML = null;

	FuncClear();
// 	$('#txtDocLine').prop('disabled', true);
	
// 	$('#txtDate').datepicker({
// 	      format: 'yyyy-mm-dd',
// 	      autoclose: true
// 	    });
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById('lblTitleModal').innerHTML = 'Edit Inbound Detail';
	
	FuncClear();
	$('#txtDocLine').prop('disabled', true);
	
// 	$('#txtDate').datepicker({
// 	      format: 'yyyy-mm-dd',
// 	      autoclose: true
// 	    });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function FuncValShowBatch(){	
var txtProductID = document.getElementById('txtProductID').value;
//var txtProductNm = document.getElementById('txtProductNm').value;
var vendorId = document.getElementById('temp_vendorid').value;
var txtVendorBatchNo = document.getElementById('txtVendorBatchNo').value;
//var txtVendorID = document.getElementById('txtVendorID').value;

if (!txtVendorBatchNo){
txtVendorBatchNo = "";
}

// var table = $("#tb_master_batch").DataTable();
// table.search( txtProductID + " " + vendorId + " " + txtVendorBatchNo + " " ).draw();

FuncClear();

if(!txtProductID.match(/\S/)) {    	
    	$('#txtProductID').focus();
    	$('#dvProductID').addClass('has-error');
    	$('#mrkProductID').show();
    	
    	alert("Fill Product ID First ...!!!");
        return false;
    } 
    return true;
}

function FuncValShowUOM(){	
	var txtProductID = document.getElementById('txtProductID').value;

	FuncClear();

	if(!txtProductID.match(/\S/)) {    	
	    	$('#txtProductID').focus();
	    	$('#dvProductID').addClass('has-error');
	    	$('#mrkProductID').show();
	    	
	    	alert("Fill Product ID First ...!!!");
	        return false;
	    } 
	    return true;
	}

function FuncValEmptyInput(lParambtn) {
	var txtDocNumber = document.getElementById('txtDocNumber').value;
	var txtDocLine = document.getElementById('txtDocLine').value;
	var txtProductID = document.getElementById('txtProductID').value;
	var txtQtyUOM = document.getElementById('txtQtyUOM').value;
	var slMasterUOM = document.getElementById('slMasterUOM').value;
// 	var txtQtyBaseUOM = document.getElementById('txtQtyBaseUOM').value;
	var txtVendorBatchNo = document.getElementById('txtVendorBatchNo').value;
	var txtPackingNo = document.getElementById('txtVendorBatchNo').value;
	var txtExpiredDate = document.getElementById('txtExpiredDate').value;
	var txtBatchNo = document.getElementById('txtBatchNo').value;
	
	var dvDocNumber = document.getElementsByClassName('dvDocNumber');
	var dvDocLine = document.getElementsByClassName('dvDocLine');
	var dvProductID = document.getElementsByClassName('dvProductID');
	var dvUOM = document.getElementsByClassName('dvUOM');
// 	var dvQtyBaseUOM = document.getElementsByClassName('dvQtyBaseUOM');
	var dvVendorBatchNo = document.getElementsByClassName('dvVendorBatchNo');
	var dvPackingNo = document.getElementsByClassName('dvPackingNo');
	var dvExpiredDate = document.getElementsByClassName('dvExpiredDate');
	
	if(lParambtn == 'save'){
		$('#txtDocLine').prop('disabled', false);
	}
	else{
		$('#txtDocLine').prop('disabled', true);
	}

    if(!txtDocNumber.match(/\S/)) {
    	$("#txtDocNumber").focus();
    	$('#dvDocNumber').addClass('has-error');
    	$('#mrkDocNumber').show();
        return false;
    } 
    
    if(!txtDocLine.match(/\S/)) {    	
    	$('#txtDocLine').focus();
    	$('#dvDocLine').addClass('has-error');
    	$('#mrkDocLine').show();
        return false;
    } 
    
    if(!txtProductID.match(/\S/)) {
    	$('#txtProductID').focus();
    	$('#dvProductID').addClass('has-error');
    	$('#mrkProductID').show();
        return false;
    }
    
    if(!slMasterUOM.match(/\S/)) {
    	$('#slMasterUOM').focus();
    	$('#dvUOM').addClass('has-error');
    	$('#mrkUOM').show();
        return false;
    }
    
//     if(!txtQtyBaseUOM.match(/\S/)) {
//     	$('#txtQtyBaseUOM').focus();
//     	$('#dvQtyBaseUOM').addClass('has-error');
//     	$('#mrkQtyBaseUOM').show();
//         return false;
//     } 
    
//     if(!txtVendorBatchNo.match(/\S/)) {
//     	$('#txtVendorBatchNo').focus();
//     	$('#dvVendorBatchNo').addClass('has-error');
//     	$('#mrkVendorBatchNo').show();
//         return false;
//     }
    
    if(!txtPackingNo.match(/\S/)) {
    	$('#txtPackingNo').focus();
    	$('#dvPackingNo').addClass('has-error');
    	$('#mrkPackingNo').show();
        return false;
    }
    
    if(!txtExpiredDate.match(/\S/)) {
    	$('#txtExpiredDate').focus();
    	$('#dvExpiredDate').addClass('has-error');
    	$('#mrkExpiredDate').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/InboundDetail',	
        type:'POST',
        data:{"key":lParambtn,"txtDocNumber":txtDocNumber,"txtDocLine":txtDocLine,"txtProductID":txtProductID, "txtQtyUOM":txtQtyUOM, "slUOM":slMasterUOM, "txtVendorBatchNo":txtVendorBatchNo, "txtPackingNo":txtPackingNo, "txtExpiredDate":txtExpiredDate, "txtBatchNo":txtBatchNo},
//         "txtQtyBaseUOM":txtQtyBaseUOM
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertInboundDetail')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan dokumen detil masuk barang";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtProductID").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedGenerateBatch')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				//$("#txtQty").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedConversion')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				//$("#txtQty").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/InboundDetail';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>


<script>
$(document).ready(function(){
	// get batch from product id script
    $(document).on('click', '#txtBatchNo', function(e){
  
     e.preventDefault();
  
	//var plantid = $(this).data('id'); // get id of clicked row
		var paramproductid = document.getElementById('txtProductID').value;
		var paramvendorid = document.getElementById('temp_vendorid').value;
		var paramvendorbatch = document.getElementById('txtVendorBatchNo').value;
		var lparam = 'inbounddetail';
  
     $('#dynamic-content').html(''); // leave this div blank
	//$('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getbatch',
          type: 'POST',
          data: {productid : paramproductid, param: lparam, vendorid: paramvendorid, vendorbatch: paramvendorbatch},
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content').html(''); // blank before load.
          $('#dynamic-content').html(data); // load here
			//$('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			//$('#modal-loader').hide();
     });

    });
});
</script>

<!-- get uom from product id -->
 	<script>
 	function FuncShowUOM(){	
 		$('#slMasterUOM').find('option').remove();
 		
		var paramproductid = document.getElementById('txtProductID').value;
 
		         $.ajax({
		              url: '${pageContext.request.contextPath}/getuom',
		              type: 'POST',
		              data: {productid : paramproductid},
		              dataType: 'json'
		         })
		         .done(function(data){
		              console.log(data);
		
		             //for json data type
		             for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slMasterUOM').append("<option id=\"" + key + "\" value=\"" + key + "\">" + key + "</option>");
		        
		    }
		             
		         })
		         .fail(function(){
		        	 console.log('Service call failed!');
		         });
 	}
 </script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtBatchNo:focus').length) {
    	$('#txtBatchNo').click();
    }
});
</script>

</body>
</html>