<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/webicon2.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Report Stock</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="mainform/plugins/datatables/datagrid.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp"%>

<form id="RptStock" name="RptStock" action = "${pageContext.request.contextPath}/RptStock" method="post" target="_blank">
<input type="hidden" name="temp_string" value="" />
	<div class="wrapper">
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Report Stock <small>tables</small>
			</h1>
			</section>
			
			
			
			<!-- Main content -->
			<section class="content">
			
			<!-- Show Parameter -->
			<div class="row">
		        <div class="col-md-12">
        		  <div id="dvBoxParam" class="box">
        		  	
        		  	
        		  
            		<div class="box-header with-border">
              		<h3 class="box-title">Parameter Report</h3>

  		            <div class="box-tools pull-right">
  		            
       		        <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">Export &nbsp<i class="fa  fa-share-square-o"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li onclick="sendvalueLi('pdf')"><a >Export Pdf</a></li>
                    <li onclick="sendvalueLi('excel');"><a >Export Excel</a></li>
					<!-- <li onclick="sendvalueLi('html');"><a >Export Html</a></li> -->
                    <input type="hidden" id="exporttype" name="exporttype" value="" />
                  </ul>
              	  </div>
       		        
       		        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
       		         
                
              </div>
            </div>
            <!-- /.box-header -->
            
            
            <div class="box-body">
            
              <div class="row">
              <div class="col-md-6">
             	 <div id="dvPlantID" class="form-group">
	        	  		<label for="recipient-name" class="control-label">Plant ID</label>	
	        	  		<input type="text" class="form-control" id="txtPlantID" name="txtPlantID" data-toggle="modal" data-target="#ModalGetPlantID" >
	        	  		<input type="hidden" name="temp_txtPlantID" value="" />	
	         	 </div>
	         	 </div>
	         	 <div class="col-md-6">
	         	 <div id="dvWarehouseID" class="form-group">
	        	  		<label for="recipient-name" class="control-label">Warehouse ID</label>	
	        	  		<input type="text" class="form-control" id="txtWarehouseID" name="txtWarehouseID" data-toggle="modal" data-target="#ModalGetWarehouseID" onfocus="FuncValPlant()">
	         	 		<input type="hidden" name="temp_txtWarehouseID" value="" />
	         	 </div>
	         	 </div>
	         	 <div class="col-md-12">
	         	 	<button type="button" class="btn btn-primary pull-right" id="btnShow" name="btnShow" onclick="FuncValEmptyInput('show')">Show</button>
	         	 </div>
              </div>
              
            <!--modal show warehouse data -->
			<div class="modal fade" id="ModalGetWarehouseID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelWarehouseID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelWarehouseID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			       								<!-- mysql data load warehouse by plant id will be load here -->                          
           										<div id="dynamic-content">
           										</div>
           										
<!-- 			         							<table id="tb_master_warehouse" class="table table-bordered table-hover"> -->
<!-- 										        <thead style="background-color: #d2d6de;"> -->
<!-- 										                <tr> -->
<!-- 										                <th>Plant ID</th> -->
<!-- 														<th>Warehouse ID</th> -->
<!-- 														<th>Warehouse Name</th> -->
<!-- 														<th>Warehouse Desc</th> -->
<!-- 										                  <th style="width: 20px"></th> -->
<!-- 										                </tr> -->
<!-- 										        </thead> -->
										        
<!-- 										        <tbody> -->
										        
<%-- 										        <c:forEach items="${listWarehouse}" var ="warehouse"> --%>
<!-- 												        <tr> -->
<%-- 												        <td><c:out value="${warehouse.plantID}" /></td> --%>
<%-- 												        <td><c:out value="${warehouse.warehouseID}"/></td> --%>
<%-- 												        <td><c:out value="${warehouse.warehouseName}"/></td> --%>
<%-- 												        <td><c:out value="${warehouse.warehouseDesc}"/></td> --%>
<!-- 												        <td><button type="button" class="btn btn-primary" -->
<!-- 												        			data-toggle="modal" -->
<%-- 												        			onclick="FuncPassStringWarehouse('<c:out value="${warehouse.warehouseID}"/>')" --%>
<!-- 												        			data-dismiss="modal" -->
<!-- 												        	><i class="fa fa-fw fa-check"></i></button> -->
<!-- 												        </td> -->
<!-- 										        		</tr> -->
										        		
<%-- 										        </c:forEach> --%>
										        
<!-- 										        </tbody> -->
<!-- 										        </table> -->
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
			<!-- /. end of modal show plant data -->
										
			<!--modal show plant data -->
			<div class="modal fade" id="ModalGetPlantID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPlantID">
												<div class="modal-dialog" role="document">
			  										<div class="modal-content">
			    											<div class="modal-header">
			      											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      											<h4 class="modal-title" id="ModalLabelPlantID"></h4>	
			      												
			      											
			    											</div>
			     								<div class="modal-body">
			       								
			         								<table id="tb_master_plant" class="table table-bordered table-hover">
										        <thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Plant ID</th>
										                <th>Plant Name</th>
										                <th>Description</th>
														<th style="width: 20px"></th>
										                </tr>
										        </thead>
										        
										        <tbody>
										        
										        <c:forEach items="${listPlant}" var ="plant">
												        <tr>
												        <td><c:out value="${plant.plantID}"/></td>
												        <td><c:out value="${plant.plantNm}"/></td>
												        <td><c:out value="${plant.desc}"/></td>
												        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassStringPlant('<c:out value="${plant.plantID}"/>')"
												        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
										        		
										        </c:forEach>
										        
										        </tbody>
										        </table>
			       								
			    								</div>
			    								
			    								<div class="modal-footer">
			      								
			    								</div>
			  										</div>
													</div>
										</div>
			<!-- /. end of modal show plant data -->
              
            </div>
          </div>
        </div>
      </div>
    		<!-- /.row -->
    		<div id="dvTableStock">
			</div>
			</section>
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
		
 		$(function () {
  			$("#tb_itemlib").DataTable( {
  		        "scrollX": true
  		    }); 
  			$('#M009').addClass('active');
  	  		$('#M041').addClass('active');
  	  		
  	  		$('#txtPlantID').attr('readonly', true);
  	  		$('#txtWarehouseID').attr('readonly', true);
  	  		

  	  		
  	  		
//   	  		//Created By: Brij Mohan
//  	 	  	//Website: http://techbrij.com
//   		  	function groupTable($rows, startIndex, total){
//   	  		if (total === 0){
//  	 	  	return;
//   		  	}
//   		  	var i , currentIndex = startIndex, count=1, lst=[];
//   	  		var tds = $rows.find('td:eq('+ currentIndex +')');
//   		  	var ctrl = $(tds[0]);
//   	  		lst.push($rows[0]);
//   		  	for (i=1;i<=tds.length;i++){
//   	  		if (ctrl.text() ==  $(tds[i]).text()){
//   	 	 	count++;
//   	  		$(tds[i]).addClass('deleted');
//   	  		lst.push($rows[i]);
//   	  		}
//   	  		else{
//   	  		if (count>1){
//   	  		ctrl.attr('rowspan',count);
//   	  		groupTable($(lst),startIndex+1,total-1)
//   	  		}
//   	  		count=1;
//   	  		lst = [];
//   	  		ctrl=$(tds[i]);
//   	  		lst.push($rows[i]);
//   	  		}
//   	  		}
//   	  		}
//   	  		groupTable($('#tb_itemlib tr:has(td)'),0,3);
//   	  		$('#tb_itemlib .deleted').remove();
  	  		
  		});
 		
 		function FuncPassStringPlant(lParamPlantID){
 			$("#txtPlantID").val(lParamPlantID);
 		}
 		
 		function FuncPassStringWarehouse2(lParamWarehouseID){
 			$("#txtWarehouseID").val(lParamWarehouseID);
 		}
 		
 		function sendvalueLi(str) {
 			$("#exporttype").val(str);
 			RptStock.submit() 
 		}
 		
 		function FuncClear(){
	  		$('#mrkPlantID').hide();		
 			$('#dvPlantID').removeClass('has-error');
 			
 			$('#mrkWarehouseID').hide();		
 			$('#dvWarehouseID').removeClass('has-error');
 		}
 		
 		function FuncValPlant(){	
 			var txtPlantID = document.getElementById('txtPlantID').value;
 			
 			
// 		 	var table = $("#tb_master_warehouse").DataTable();
// 		 	table.search( txtPlantID + " " ).draw();
 			
 			FuncClear();
 			
 			if(!txtPlantID.match(/\S/)) {    	
 		    	$('#txtPlantID').focus();
 		    	$('#dvPlantID').addClass('has-error');
 		    	$('#mrkPlantID').show();
 		    	
 		    	alert("Fill Plant ID First ...!!!");
 		        return false;
 		    } 
 			
 		    return true;
 			
 		}
 		
 		function FuncValEmptyInput(lParambtn) {
 			var txtPlantID = document.getElementById('txtPlantID').value;
 			var txtWarehouseID = document.getElementById('txtWarehouseID').value;	
 			
 			FuncClear();			

 		    if(!txtPlantID.match(/\S/)) {
 		    	$("#txtPlantID").focus();
 		    	$('#dvPlantID').addClass('has-error');
 		    	$('#mrkPlantID').show();
 		        return false;
 		    }
 		    
 		   if(!txtWarehouseID.match(/\S/)) {
		    	$("#txtWarehouseID").focus();
		    	$('#dvWarehouseID').addClass('has-error');
		    	$('#mrkWarehouseID').show();
		        return false;
		    } 
 		   
		     $.ajax({
		          url: '${pageContext.request.contextPath}/RptStock',
		          type: 'POST',
		          data:{key:lParambtn,txtPlantID:txtPlantID,txtWarehouseID:txtWarehouseID},
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dvTableStock').html(data); // load here
		          //$('#modal-loader').hide(); // hide loader  
		     })
		     .fail(function(){
		          $('#dvTableStock').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//		           $('#modal-loader').hide();
		     });
			return true;
 		}
	</script>
	<!-- get warehouse from plant id -->
<script>
$(document).ready(function(){

    $(document).on('click', '#txtWarehouseID', function(e){
  
     e.preventDefault();
  
//      var plantid = $(this).data('id'); // get id of clicked row
		var plantid = document.getElementById('txtPlantID').value;
  
     $('#dynamic-content').html(''); // leave this div blank
//      $('#modal-loader').show();      // load ajax loader on button click
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getwarehouse2',
          type: 'POST',
          data: 'plantid='+plantid,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content').html(''); // blank before load.
          $('#dynamic-content').html(data); // load here
//           $('#modal-loader').hide(); // hide loader  
     })
     .fail(function(){
          $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//           $('#modal-loader').hide();
     });

    });
});
</script>
	
</body>
</html>