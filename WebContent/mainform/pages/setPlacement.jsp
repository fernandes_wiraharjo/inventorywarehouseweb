<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<input type="hidden" id="temp_result" name="temp_result" value="<c:out value="${temp_result}"/>" />

<table id="tb_placement_plan" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>No</th>
			<th>Dest Target Qty</th>
			<th>SUT</th>
			<th>Stor Type</th>
			<th>Stor Section</th>
			<th>Dest Bin</th>
			<th>Dest Stor Unit</th>
			<th>Batch</th>
        	</tr>
	</thead>

	<tbody>
	<c:forEach items="${listPlacementPlan}" var ="placementplan">
	  <tr>
	  <td><c:out value="${placementplan.no}" /></td>
	  <td><c:out value="${placementplan.destinationQty}" /></td>
		<td><c:out value="${placementplan.storageUnitType}" /></td>
		<td><c:out value="${placementplan.storageTypeID}" /></td>
		<td><c:out value="${placementplan.storageSectionID}" /></td>
		<td><c:out value="${placementplan.destinationStorageBin}" /></td>
		<td><c:out value="${placementplan.destinationStorageUnit}" /></td>
		<td><c:out value="${placementplan.batchNo}" /></td>
	  </tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_placement_plan").DataTable();
  		
  		var lResult = document.getElementById('temp_result').value;
  		if(lResult=='1')
  			$('#dynamic-placement-data').html('<i class="glyphicon glyphicon-info-sign"></i> Storage tidak memadai...');
  		else if(lResult=='2')
  			$('#dynamic-placement-data').html('<i class="glyphicon glyphicon-info-sign"></i> Database Error...');
  	});
</script>