<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_storagetypeindicator" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>Plant ID</th>
			<th>Warehouse ID</th>
			<th>Storage Type Indicator</th>
			<th>Description</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>

	<c:forEach items="${listStorageTypeIndicator}" var ="stortypeind">
	  <tr>
	  <td><c:out value="${stortypeind.plantID}" /></td>
		<td><c:out value="${stortypeind.warehouseID}" /></td>
		<td><c:out value="${stortypeind.storageTypeIndicatorID}" /></td>
		<td><c:out value="${stortypeind.storageTypeIndicatorName}" /></td>
		  <td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			onclick="FuncPassStorageTypeIndicator('<c:out value="${stortypeind.storageTypeIndicatorID}"/>','<c:out value="${stortypeind.storageTypeIndicatorName}"/>','<c:out value="${type}"/>')"
		  			data-dismiss="modal"
		  	><i class="fa fa-fw fa-check"></i></button>
		  </td>
			</tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_storagetypeindicator").DataTable();
  	});
</script>