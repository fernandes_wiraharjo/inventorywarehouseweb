<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_transfer_requirement" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
            <tr>
            <th>No</th>
			<th>Product ID</th>
			<th>Quantity</th>
			<th>UOM</th>
			<th>Open Qty</th>
			<th>Batch</th>
			<th>Plant</th>
			<th>Warehouse</th>
			<th style="width: 20px"></th>
        	</tr>
	</thead>

	<tbody>
	<!-- onclick="FuncPalletization()" -->
	<c:forEach items="${listInboundDetail}" var ="inbounddetail">
	  <tr>
	  <td><c:out value="${inbounddetail.docLine}" /></td>
		<td><c:out value="${inbounddetail.productID}" /></td>
		<td><c:out value="${inbounddetail.qtyUom}" /></td>
		<td><c:out value="${inbounddetail.uom}" /></td>
		<td><c:out value="${inbounddetail.qtyUom}" /></td>
		<td><c:out value="${inbounddetail.batchNo}" /></td>
		<td><c:out value="${inbounddetail.plantID}" /></td>
		<td><c:out value="${inbounddetail.warehouseID}" /></td>
		<td><button type="button" class="btn btn-primary"
		  			data-toggle="modal"
		  			data-target="#ModalPalletization"
		  			data-lproductid='<c:out value="${inbounddetail.productID}" />'
		  			data-lproductnm='<c:out value="${inbounddetail.productName}" />'
		  			data-lbatchno='<c:out value="${inbounddetail.batchNo}" />'
		  			data-lplantid='<c:out value="${inbounddetail.plantID}" />'
		  			data-lwarehouseid='<c:out value="${inbounddetail.warehouseID}" />'
		  			data-linbounddate='<c:out value="${inbounddetail.date}" />'
		  			data-linboundqty='<c:out value="${inbounddetail.qtyUom}" />'
		  			data-linbounduom='<c:out value="${inbounddetail.uom}" />'
		  	>Palletization</button>
		</td>
	  </tr>
			
	</c:forEach>
	
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_transfer_requirement").DataTable();
  	});
</script>