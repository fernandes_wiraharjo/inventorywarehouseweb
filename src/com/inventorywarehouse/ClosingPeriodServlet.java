package com.inventorywarehouse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.javafx.scene.layout.region.Margins.Converter;

import adapter.ClosingPeriodAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.PlantAdapter;
import model.Globals;
import net.sourceforge.jtds.jdbc.DateTime;

@WebServlet(urlPatterns={"/ClosingPeriod"} , name="ClosingPeriod")
public class ClosingPeriodServlet extends HttpServlet{

private static final long serialVersionUID = 1L;
    
    public ClosingPeriodServlet() {
		// TODO Auto-generated constructor stub
    	 super();
	}
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	Boolean CheckMenu;
    	String MenuURL = "ClosingPeriod";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlClosingPeriod> listTransactionPeriod = new ArrayList<model.mdlClosingPeriod>();
    		listTransactionPeriod.addAll(ClosingPeriodAdapter.LoadTransactionPeriod(Globals.user));
    		request.setAttribute("listTransactionPeriod", listTransactionPeriod);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
        	
        	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/closing_period.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
    	
    	//String btnDelete = request.getParameter("btnDelete");
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
		//    		if (btnDelete != null){
		//    			doGet(request, response);
		//    		}
    		return;
    	}
    		
    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}
    	
    	//Declare TextBox
    	String txtPeriod = request.getParameter("txtPeriod");
        String txtYear = request.getParameter("txtYear");
        Date now = new Date();
		
		//Declare mdlClosingPeriod for global
		model.mdlClosingPeriod mdlClosingPeriod = new model.mdlClosingPeriod();
		mdlClosingPeriod.setPeriod(txtPeriod.replace("/", ""));
		mdlClosingPeriod.setFiscal_Year(txtYear);
        
		String lResult = "";
		String lCommand = "";
		if (keyBtn.equals("save")){
			try
			{
				lCommand = "Insert Current Period : " + mdlClosingPeriod.getPeriod();
				
				if(txtPeriod.substring(0,4).contentEquals(txtYear))
				{
					model.mdlClosingPeriod CheckLastPeriod = ClosingPeriodAdapter.LoadActivePeriod(Globals.user);
					String sLastPeriodDate = CheckLastPeriod.getCreated_at();
					String sLastPeriod = CheckLastPeriod.getPeriod();
					
					Date dLastPeriodDate;
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					DateFormat dfPeriod = new SimpleDateFormat("yyyy-MM");
					Date LastPeriod_On;
					
						if(sLastPeriodDate == null)
						{
							dLastPeriodDate = now;
						}
						else
						{
							dLastPeriodDate = df.parse(sLastPeriodDate);
						}
					
					Date CreatedPeriod_On = dfPeriod.parse(txtPeriod.substring(0,4)+"-"+txtPeriod.substring(5,7));
					
							if(sLastPeriod == null || sLastPeriod == "")
							{
								sLastPeriod = df.format(now);
								LastPeriod_On = dfPeriod.parse(sLastPeriod.substring(0,4)+"-"+sLastPeriod.substring(5,7));
							}
							else
							{
								LastPeriod_On = dfPeriod.parse(sLastPeriod.substring(0,4)+"-"+sLastPeriod.substring(4,6));
							}
					
					long milPerDay = 1000*60*60*24; 
								
					// GET Date range between input period and the last period
					int DateRange = (int) ((CreatedPeriod_On.getTime() - LastPeriod_On.getTime())/milPerDay);
					
					if( (now.after(dLastPeriodDate) || now.equals(dLastPeriodDate)) && (CreatedPeriod_On.after(LastPeriod_On) || CreatedPeriod_On.equals(LastPeriod_On)) && (DateRange <= 31) )
					{	
						lResult = ClosingPeriodAdapter.ClosingPeriod(mdlClosingPeriod, Globals.user);
						if(lResult == "Success Closing Period")
						{
							ClosingPeriodAdapter.InactivatePeriod(mdlClosingPeriod, Globals.user);
						}
					}
					else
					{
						LogAdapter.InsertLogExc("Period is not allowed", "ClosingPeriod", lCommand , Globals.user);
						//lResult = "Error Closing Period";
						lResult = "Periode yang diinput tidak diijinkan, mungkin karena Anda memasukkan periode "
								+ "yang lebih kecil dari periode berjalan atau yang lebih dari 1 bulan ke depan";
					}
				}
				else
				{
					LogAdapter.InsertLogExc("Fiscal year and period is not matched", "ClosingPeriod", lCommand , Globals.user);
					//lResult = "Error Closing Period";
					lResult = "Periode dan tahun fiskal yang Anda masukkan tidak cocok";
				}
			}
			catch(Exception ex)
			{
				LogAdapter.InsertLogExc(ex.toString(), "ClosingPeriod", lCommand , Globals.user);
				//lResult = "Error Closing Period";
				lResult = "Terjadi kesalahan pada sistem saat proses penguraian tanggal";
			}
			
				
			if(lResult.contains("Success Closing Period")) {  
				Globals.gCondition = "SuccessInsertClosingPeriod";
            }  
            else {
            	Globals.gCondition = "FailedInsertClosingPeriod";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
        return;
		
//		if (keyBtn.equals("update")){
//		    String lResult = PlantAdapter.UpdatePlant(mdlPlant,Globals.user);	
//		    
//		    if(lResult.contains("Success Update Plant")) {  
//		    	Globals.gCondition = "SuccessUpdate";
//            }  
//            else {
//            	Globals.gCondition = "FailedUpdate"; 
//            } 
//	
//			return;
//		}
//		
//		if (btnDelete != null){
//			String lResult = PlantAdapter.DeletePlant(temp_txtPlantID,Globals.user);
//			if(lResult.contains("Success Delete Plant")) {  
//				Globals.gCondition =  "SuccessDelete";
//            }  
//            else {
//            	Globals.gCondition = "FailedDelete"; 
//            }
//			
//			doGet(request, response);
//			
//			return;
//		}
        
	}
    
}
