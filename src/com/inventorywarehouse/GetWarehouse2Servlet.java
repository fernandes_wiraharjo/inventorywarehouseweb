package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.WarehouseAdapter;

@WebServlet(urlPatterns={"/getwarehouse2"} , name="getwarehouse2")
public class GetWarehouse2Servlet extends HttpServlet {
	
private static final long serialVersionUID = 1L; 
    
    public GetWarehouse2Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	
    	List<model.mdlWarehouse> mdlWarehouseList = new ArrayList<model.mdlWarehouse>();
		
		mdlWarehouseList.addAll(WarehouseAdapter.LoadWarehousebyPlantID(lPlantID));
		request.setAttribute("listwarehouse", mdlWarehouseList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getWarehouse2FromPlant.jsp");
		dispacther.forward(request, response);
		
	}

}
