package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.CancelConfirmationAdapter;
import adapter.CancelTransactionAdapter;
import adapter.LogAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getcancelconfirmationdetail"} , name="getcancelconfirmationdetail")
public class GetCancelConfirmationDetailServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetCancelConfirmationDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lconfirmationid = request.getParameter("id");
    	String lconfirmationno = request.getParameter("no");
    	
    	List<model.mdlBOMCancelConfirmationDetail> mdlCancelConfirmationDetailList = new ArrayList<model.mdlBOMCancelConfirmationDetail>();
		mdlCancelConfirmationDetailList.addAll(CancelConfirmationAdapter.LoadBOMCancelConfirmationDetail(lconfirmationid, lconfirmationno));
		request.setAttribute("listCancelConfirmationDetail", mdlCancelConfirmationDetailList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getCancelConfirmationDetail.jsp");
		dispacther.forward(request, response);
	}
	
}
