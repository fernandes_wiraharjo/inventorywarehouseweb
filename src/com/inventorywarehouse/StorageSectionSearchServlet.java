package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.StorageSectionSearchAdapter;
import adapter.StorageTypeSearchAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/storsectionsearch"} , name="storsectionsearch")
public class StorageSectionSearchServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public StorageSectionSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "storsectionsearch";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);
    		
    		List<model.mdlStorageSectionSearch> listStorageSectionSearch = new ArrayList<model.mdlStorageSectionSearch>();
    		listStorageSectionSearch.addAll(StorageSectionSearchAdapter.LoadStorageSectionSearch());
    		request.setAttribute("listStorageSectionSearch", listStorageSectionSearch);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/storagesection_search.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare Deleting Proccess Params
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtStorageTypeID = request.getParameter("temp_txtStorageTypeID");
		String temp_txtStorageSectionIndicatorID = request.getParameter("temp_txtStorageSectionIndicatorID");
		if (btnDelete != null){
			String lResult = StorageSectionSearchAdapter.DeleteStorageSectionSearch(temp_txtPlantID, temp_txtWarehouseID, temp_txtStorageTypeID, temp_txtStorageSectionIndicatorID);
			if(lResult.contains("Success Delete Storage Section Search")) {
				Globals.gCondition =  "SuccessDeleteStorageSectionSearch";
            }
            else {
            	Globals.gCondition = "FailedDeleteStorageSectionSearch";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
			
			return;
		}

		//Declare TextBox for insert and update
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtStorageTypeID = request.getParameter("txtStorageTypeID");
		String txtStorageSectionIndicatorID = request.getParameter("txtStorageSectionIndicatorID");
		String txtSeq1 = request.getParameter("txtSeq1");
		String txtSeq2 = request.getParameter("txtSeq2");
		String txtSeq3 = request.getParameter("txtSeq3");
		String txtSeq4 = request.getParameter("txtSeq4");
		String txtSeq5 = request.getParameter("txtSeq5");
		String txtSeq6 = request.getParameter("txtSeq6");
		String txtSeq7 = request.getParameter("txtSeq7");
		String txtSeq8 = request.getParameter("txtSeq8");
		String txtSeq9 = request.getParameter("txtSeq9");
		String txtSeq10 = request.getParameter("txtSeq10");
		String txtSeq11 = request.getParameter("txtSeq11");
		String txtSeq12 = request.getParameter("txtSeq12");
		String txtSeq13 = request.getParameter("txtSeq13");
		String txtSeq14 = request.getParameter("txtSeq14");
		String txtSeq15 = request.getParameter("txtSeq15");
		String txtSeq16 = request.getParameter("txtSeq16");
		String txtSeq17 = request.getParameter("txtSeq17");
		String txtSeq18 = request.getParameter("txtSeq18");
		String txtSeq19 = request.getParameter("txtSeq19");
		String txtSeq20 = request.getParameter("txtSeq20");
		String txtSeq21 = request.getParameter("txtSeq21");
		String txtSeq22 = request.getParameter("txtSeq22");
		String txtSeq23 = request.getParameter("txtSeq23");
		String txtSeq24 = request.getParameter("txtSeq24");
		String txtSeq25 = request.getParameter("txtSeq25");

		//Declare mdlStorageSectionSearch for global
		model.mdlStorageSectionSearch mdlStorageSectionSearch = new model.mdlStorageSectionSearch();
		mdlStorageSectionSearch.setPlantID(txtPlantID);
		mdlStorageSectionSearch.setWarehouseID(txtWarehouseID);
		mdlStorageSectionSearch.setStorageTypeID(txtStorageTypeID);
		mdlStorageSectionSearch.setStorageSectionIndicatorID(txtStorageSectionIndicatorID);
		mdlStorageSectionSearch.setStorageSection1(txtSeq1);
		mdlStorageSectionSearch.setStorageSection2(txtSeq2);
		mdlStorageSectionSearch.setStorageSection3(txtSeq3);
		mdlStorageSectionSearch.setStorageSection4(txtSeq4);
		mdlStorageSectionSearch.setStorageSection5(txtSeq5);
		mdlStorageSectionSearch.setStorageSection6(txtSeq6);
		mdlStorageSectionSearch.setStorageSection7(txtSeq7);
		mdlStorageSectionSearch.setStorageSection8(txtSeq8);
		mdlStorageSectionSearch.setStorageSection9(txtSeq9);
		mdlStorageSectionSearch.setStorageSection10(txtSeq10);
		mdlStorageSectionSearch.setStorageSection11(txtSeq11);
		mdlStorageSectionSearch.setStorageSection12(txtSeq12);
		mdlStorageSectionSearch.setStorageSection13(txtSeq13);
		mdlStorageSectionSearch.setStorageSection14(txtSeq14);
		mdlStorageSectionSearch.setStorageSection15(txtSeq15);
		mdlStorageSectionSearch.setStorageSection16(txtSeq16);
		mdlStorageSectionSearch.setStorageSection17(txtSeq17);
		mdlStorageSectionSearch.setStorageSection18(txtSeq18);
		mdlStorageSectionSearch.setStorageSection19(txtSeq19);
		mdlStorageSectionSearch.setStorageSection20(txtSeq20);
		mdlStorageSectionSearch.setStorageSection21(txtSeq21);
		mdlStorageSectionSearch.setStorageSection22(txtSeq22);
		mdlStorageSectionSearch.setStorageSection23(txtSeq23);
		mdlStorageSectionSearch.setStorageSection24(txtSeq24);
		mdlStorageSectionSearch.setStorageSection25(txtSeq25);
		
		if (keyBtn.equals("save")){
			String lResult = StorageSectionSearchAdapter.InsertStorageSectionSearch(mdlStorageSectionSearch);
			if(lResult.contains("Success Insert Storage Section Search")) {
				Globals.gCondition = "SuccessInsertStorageSectionSearch";
            }
            else {
            	Globals.gCondition = "FailedInsertStorageSectionSearch";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = StorageSectionSearchAdapter.UpdateStorageSectionSearch(mdlStorageSectionSearch);

		    if(lResult.contains("Success Update Storage Section Search")) {
		    	Globals.gCondition = "SuccessUpdateStorageSectionSearch";
            }
            else {
            	Globals.gCondition = "FailedUpdateStorageSectionSearch";
            	Globals.gConditionDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
    }
    
}
