package com.inventorywarehouse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.OutboundAdapter;
import adapter.PlantAdapter;
import adapter.StockSummaryAdapter;
import adapter.ValidateNull;
import adapter.WarehouseAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/RptGoodIssue"} , name="RptGoodIssue")
public class RptGoodIssueServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RptGoodIssueServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean CheckMenu;
    	String MenuURL = "RptGoodIssue";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		request.setAttribute("condition", Globals.gCondition);		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/rpt_good_issue.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
	}

	@SuppressWarnings("deprecation")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lStartDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtStartDate"),"dd MMM yyyy", "yyyy-MM-dd");
		String lEndDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtEndDate"),"dd MMM yyyy", "yyyy-MM-dd");
		String[] lcbCancTranslist = request.getParameterValues("cbCancTrans");
		String lcbCancTrans = "";
		
		Connection connection = null;
		try {
			connection = database.RowSetAdapter.getConnection();
		
			JasperPrint jasperPrint = null;
			JasperReport jasperReport;
			String paramJasperReport = getServletContext().getInitParameter("param_jasper_report");
			String paramJasperPrint = getServletContext().getInitParameter("param_jasper_print");
			String paramLogoPath = getServletContext().getInitParameter("param_logo");
			
			HashMap<String, Object> params = new HashMap<String, Object>();
			
			if (lcbCancTranslist != null) {
			    for (String param : lcbCancTranslist) {
			    	lcbCancTrans = param;
			    }
			}
		
			//export jadi pdf/excel/html
			String lExportType = ValidateNull.NulltoStringEmpty(request.getParameter("exporttype"));
		
			if(lcbCancTrans.equals("")){
					params = new HashMap<String, Object>();
					params.put("lStartDate", "'"+lStartDate+"'");
					params.put("lEndDate", "'"+lEndDate+"'");
					params.put("lUserArea", Globals.user_area);
					params.put("limage", paramLogoPath);
				
					jasperReport = JasperCompileManager.compileReport(paramJasperReport+"rpt_good_Issue.jrxml");
					jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);
					
					//check page is blank or not
					List<JRPrintPage> pages = jasperPrint.getPages();
				    if (pages.size()==0){
				        //No pages, do not export instead do other stuff
						//		    	Alert alert = new Alert(AlertType.INFORMATION);
						//		        alert.setTitle("a");
						//		        alert.setHeaderText("b");
						//		        alert.setContentText("c");
						//		        alert.showAndWait();
				    	PrintWriter out = response.getWriter();  
				    	response.setContentType("text/html");  
				    	out.println("<script type=\"text/javascript\">");  
				    	out.println("alert('NO AVAILABLE DATA');");  
				    	out.println("</script>");
				    }
				    else{
				    	if(lExportType.equals("pdf")){
							//coding for Pdf
								JasperExportManager.exportReportToPdfFile(jasperPrint, paramJasperPrint+"rpt_good_Issue.pdf");
							
								String pdfFileName = "rpt_good_issue.pdf";
								File pdfFile = new File(paramJasperPrint+"rpt_good_Issue.pdf");
							
								response.setContentType("application/pdf");
								response.addHeader("Content-Disposition", "inline; filename=" + pdfFileName);
								response.setContentLength((int) pdfFile.length());
								FileInputStream fileInputStream = new FileInputStream(pdfFile);
								OutputStream responseOutputStream = response.getOutputStream();
								int bytes;
							
								while ((bytes = fileInputStream.read()) != -1) {
									responseOutputStream.write(bytes);
								}
						}
						else if(lExportType.equals("excel")){	
								//coding for Excel
								ByteArrayOutputStream os = new ByteArrayOutputStream();
								JRXlsExporter xlsExporter = new JRXlsExporter();
					            xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
					            xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, os);
					            xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "rpt_good_issue.xls");
					            xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
					            xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
					            xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
					            xlsExporter.exportReport();
					            
					            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					            response.setHeader("Content-Disposition", "attachment; filename=rpt_good_issue.xls");
					            
					            //uncomment this codes if u are want to use servlet output stream
					            //servletOutputStream.write(os.toByteArray());
					            
					            response.getOutputStream().write(os.toByteArray());
					            response.getOutputStream().flush();
					            response.getOutputStream().close();
					            response.flushBuffer();
						}
						else{
						}
				    }
		}
		
		if(lcbCancTrans.equals("Outbound")){
				params = new HashMap<String, Object>();
				params.put("lStartDate", "'"+lStartDate+"'");
				params.put("lEndDate", "'"+lEndDate+"'");
				params.put("lUserArea", Globals.user_area);
				params.put("lTransaction", "'"+lcbCancTrans+"'");
				params.put("limage", paramLogoPath);
				
				jasperReport = JasperCompileManager.compileReport(paramJasperReport+"rpt_cancel_transaction.jrxml");
				jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);
			
				//check page is blank or not
				List<JRPrintPage> pages = jasperPrint.getPages();
			    if (pages.size()==0){
			        //No pages, do not export instead do other stuff
					//		    	Alert alert = new Alert(AlertType.INFORMATION);
					//		        alert.setTitle("a");
					//		        alert.setHeaderText("b");
					//		        alert.setContentText("c");
					//		        alert.showAndWait();
			    	PrintWriter out = response.getWriter();  
			    	response.setContentType("text/html");  
			    	out.println("<script type=\"text/javascript\">");  
			    	out.println("alert('NO AVAILABLE DATA');");  
			    	out.println("</script>");
			    }
			    else{
			    	if(lExportType.equals("pdf")){
						//coding for Pdf
						JasperExportManager.exportReportToPdfFile(jasperPrint, paramJasperPrint+"rpt_cancel_transaction.pdf");
					
						String pdfFileName = "rpt_cancel_transaction.pdf";
						File pdfFile = new File(paramJasperPrint+"rpt_cancel_transaction.pdf");
					
						response.setContentType("application/pdf");
						response.addHeader("Content-Disposition", "inline; filename=" + pdfFileName);
						response.setContentLength((int) pdfFile.length());
						FileInputStream fileInputStream = new FileInputStream(pdfFile);
						OutputStream responseOutputStream = response.getOutputStream();
						int bytes;
					
						while ((bytes = fileInputStream.read()) != -1) {
						responseOutputStream.write(bytes);
						}
					}
					else if(lExportType.equals("excel")){
						//coding for Excel
							ByteArrayOutputStream os = new ByteArrayOutputStream();
							JRXlsExporter xlsExporter = new JRXlsExporter();
				            xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
				            xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, os);
				            xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "rpt_cancel_transaction.xls");
				            xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
				            xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				            xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
				            xlsExporter.exportReport();
				            
				            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				            response.setHeader("Content-Disposition", "attachment; filename=rpt_cancel_transaction.xls");
				            
				            //uncomment this codes if u are want to use servlet output stream
				            //servletOutputStream.write(os.toByteArray());
				            
				            response.getOutputStream().write(os.toByteArray());
				            response.getOutputStream().flush();
				            response.getOutputStream().close();
				            response.flushBuffer();
					}
					else{
					}
			    }
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "EXPORT REPORT GOOD ISSUE", "", Globals.user);
		}
		finally{
			try {
				//close the opened connection
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "EXPORT REPORT GOOD ISSUE", "close opened connection protocol", Globals.user);
			}
		}
	
		//		String lParambtn = request.getParameter("key");
		//		//String btnShow = request.getParameter("btnShow");
		//		if (lParambtn != null){
		//			List<model.mdlRptGoodIssue> mdlRptGoodIssueList = new ArrayList<model.mdlRptGoodIssue>();			
		//			
		//			//buat adapter sesuai parameter untuk menampilkan table GoodIssue
		//			mdlRptGoodIssueList = OutboundAdapter.LoadReportGoodIssue(lStartDate, lEndDate);
		//			request.setAttribute("listgoodissue", mdlRptGoodIssueList);
		//			RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/rpt_good_issue.jsp");
		//			dispacther.forward(request, response);
		//		}
		
		//		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/rpt_good_issue.jsp");
		//		dispacther.forward(request, response);
	
	}
	
}
