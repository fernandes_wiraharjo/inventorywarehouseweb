package com.inventorywarehouse;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BomAdapter;
import adapter.LogAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getConfirmationNo"} , name="getConfirmationNo")
public class GetConfirmationNoServlet extends HttpServlet{

private static final long serialVersionUID = 1L; 
	
	public GetConfirmationNoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lConfirmationID = request.getParameter("confirmationid");
    	
    	String ResultConfirmationNo = "";
    	
    	ResultConfirmationNo = BomAdapter.GetConfirmationNo(lConfirmationID);
    	
    	if(ResultConfirmationNo.isEmpty()){
    		ResultConfirmationNo = "1";
   		}
   		else
   		{
   			ResultConfirmationNo = String.valueOf(Integer.parseInt(ResultConfirmationNo)+1);
   		}
    	
    	model.mdlBOMConfirmationDocument BOMConfirmationDoc = new model.mdlBOMConfirmationDocument();
    	BOMConfirmationDoc = BomAdapter.GetConfirmationDocumentById(lConfirmationID);
		
    	String limitMaxRange = BOMConfirmationDoc.getRangeTo();
    	
    	if( Integer.parseInt(ResultConfirmationNo) > Integer.parseInt(limitMaxRange) )
    		ResultConfirmationNo = "0";
    	
    	response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(ResultConfirmationNo);
		
	}
}
