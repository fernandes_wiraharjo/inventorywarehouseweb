package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.StorageUnitTypeAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/storageunittype"} , name="storageunittype")
public class StorageUnitTypeServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public StorageUnitTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "storageunittype";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);

    		List<model.mdlStorageUnitType> listStorageUnitType = new ArrayList<model.mdlStorageUnitType>();
    		listStorageUnitType.addAll(adapter.StorageUnitTypeAdapter.LoadStorageUnitType());
    		request.setAttribute("listStorageUnitType", listStorageUnitType);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/storage_unit_type.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}

		//Declare Deleting Proccess Params
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtStorageUnitTypeID = request.getParameter("temp_txtStorageUnitTypeID");

		if (btnDelete != null){
			String lResult = StorageUnitTypeAdapter.DeleteStorageUnitType(temp_txtPlantID, temp_txtWarehouseID, temp_txtStorageUnitTypeID);
			if(lResult.contains("Success Delete Storage Unit Type")) {
				Globals.gCondition =  "SuccessDeleteStorageUnitType";
            }
            else {
            	Globals.gCondition = "FailedDeleteStorageUnitType";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
			return;
		}

		//Declare TextBox for insert and update
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtStorageUnitTypeID = request.getParameter("txtStorageUnitTypeID");
		String txtStorageUnitTypeName = request.getParameter("txtStorageUnitTypeName");
		Double txtCapacityUsageLET = Double.valueOf(request.getParameter("txtCapacityUsageLET"));
		String txtUOMType = request.getParameter("txtUOMType");
		String txtUOMTypeName = request.getParameter("txtUOMTypeName");

		//Declare mdlStorageUnitType for global
		model.mdlStorageUnitType mdlStorageUnitType = new model.mdlStorageUnitType();
		mdlStorageUnitType.setPlantID(txtPlantID);
		mdlStorageUnitType.setWarehouseID(txtWarehouseID);
		mdlStorageUnitType.setStorageUnitTypeID(txtStorageUnitTypeID);
		mdlStorageUnitType.setStorageUnitTypeName(txtStorageUnitTypeName);
		mdlStorageUnitType.setCapacityUsageLET(txtCapacityUsageLET);
		mdlStorageUnitType.setUOMType(txtUOMType);
		mdlStorageUnitType.setUOMTypeName(txtUOMTypeName);

		if (keyBtn.equals("save")){
			String lResult = StorageUnitTypeAdapter.InsertStorageUnitType(mdlStorageUnitType);
			if(lResult.contains("Success Insert Storage Unit Type")) {
				Globals.gCondition = "SuccessInsertStorageUnitType";
            }
            else {
            	Globals.gCondition = "FailedInsertStorageUnitType";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = StorageUnitTypeAdapter.UpdateStorageUnitType(mdlStorageUnitType);

		    if(lResult.contains("Success Update Storage Unit Type")) {
		    	Globals.gCondition = "SuccessUpdateStorageUnitType";
            }
            else {
            	Globals.gCondition = "FailedUpdateStorageUnitType";
            	Globals.gConditionDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
    }

}
