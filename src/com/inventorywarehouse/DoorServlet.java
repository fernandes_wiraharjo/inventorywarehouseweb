package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.DoorAdapter;
import adapter.WarehouseAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/door"} , name="door")
public class DoorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public DoorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Boolean CheckMenu;
    	String MenuURL = "door";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlantList = new ArrayList<model.mdlPlant>();
    		listPlantList.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlantList);

    		List<model.mdlDoor> mdlDoorList = new ArrayList<model.mdlDoor>();
    		mdlDoorList.addAll(DoorAdapter.LoadDoor());
    		request.setAttribute("listDoor", mdlDoorList);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/door.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}

		//Declare TextBox
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtDoorID = request.getParameter("txtDoorID");
		String txtDoorName = request.getParameter("txtDoorName");
		Boolean cbGI_Indicator = Boolean.valueOf(request.getParameter("cbgiIndicator"));
		Boolean cbGR_Indicator = Boolean.valueOf(request.getParameter("cbgrIndicator"));
		Boolean cbCD_Indicator = Boolean.valueOf(request.getParameter("cbcdIndicator"));
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtDoorID = request.getParameter("temp_txtDoorID");

		//Declare mdlDoor for global
		model.mdlDoor mdlDoor = new model.mdlDoor();
		mdlDoor.setPlantID(txtPlantID);
		mdlDoor.setWarehouseID(txtWarehouseID);
		mdlDoor.setDoorID(txtDoorID);
		mdlDoor.setDoorName(txtDoorName);
		mdlDoor.setGI_Indicator(cbGI_Indicator);
		mdlDoor.setGR_Indicator(cbGR_Indicator);
		mdlDoor.setCD_Indicator(cbCD_Indicator);


		if (keyBtn.equals("save")){
			String lResult = DoorAdapter.InsertDoor(mdlDoor);
			if(lResult.contains("Success Insert Door")) {
				Globals.gCondition = "SuccessInsertDoor";
            }
            else {
            	Globals.gCondition = "FailedInsertDoor";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = DoorAdapter.UpdateDoor(mdlDoor);

		    if(lResult.contains("Success Update Door")) {
		    	Globals.gCondition = "SuccessUpdateDoor";
            }
            else {
            	Globals.gCondition = "FailedUpdateDoor";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (btnDelete != null){
			String lResult = DoorAdapter.DeleteDoor(temp_txtPlantID, temp_txtWarehouseID, temp_txtDoorID);
			if(lResult.contains("Success Delete Door")) {
				Globals.gCondition =  "SuccessDeleteDoor";
            }
            else {
            	Globals.gCondition = "FailedDeleteDoor";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
	}
}
