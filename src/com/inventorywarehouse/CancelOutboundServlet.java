package com.inventorywarehouse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.org.apache.xalan.internal.xsltc.dom.LoadDocument;

import adapter.CancelTransactionAdapter;
import adapter.ClosingPeriodAdapter;
import adapter.InboundAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.OutboundAdapter;
import adapter.OutboundDetailAdapter;
import adapter.PlantAdapter;
import adapter.TraceCodeAdapter;
import adapter.WarehouseAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

@WebServlet(urlPatterns={"/CancelOutbound"} , name="CancelOutbound")
public class CancelOutboundServlet extends HttpServlet {

private static final long serialVersionUID = 1L;
    
    public CancelOutboundServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String docNo = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	Boolean CheckMenu;
    	String MenuURL = "CancelOutbound";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		docNo = OutboundAdapter.GenerateDocNoOutbound(); //001 Nanda
        	request.setAttribute("tempdocNo", docNo);  //001 Nanda
    		
    		//		List<model.mdlPlant> mdlPlantList = new ArrayList<model.mdlPlant>();
    		//		
    		//		mdlPlantList.addAll(PlantAdapter.LoadPlant(Globals.user));
    		//		request.setAttribute("listPlant", mdlPlantList);
    		//		
    		//		List<model.mdlWarehouse> mdlWarehouseList = new ArrayList<model.mdlWarehouse>();
    		//		
    		//		mdlWarehouseList.addAll(WarehouseAdapter.LoadWarehouse(Globals.user));
    		//		request.setAttribute("listwarehouse", mdlWarehouseList);
    		
    		List<model.mdlOutbound> mdlOutboundList = new ArrayList<model.mdlOutbound>();
    		mdlOutboundList.addAll(OutboundAdapter.LoadNonCancelOutbound(Globals.user));
    		request.setAttribute("listOutbound", mdlOutboundList);
    		
    		List<model.mdlCancelTransaction> mdlCancelOutboundList = new ArrayList<model.mdlCancelTransaction>();
    		mdlCancelOutboundList.addAll(CancelTransactionAdapter.LoadCancelOutbound(Globals.user));
    		request.setAttribute("listCancelOutbound", mdlCancelOutboundList);
    		
    		//		List<model.mdlCancelTransaction> mdlCancelOutboundDetailList = new ArrayList<model.mdlCancelTransaction>();
    		//		
    		//		mdlCancelOutboundDetailList.addAll(CancelTransactionAdapter.LoadCancelOutboundDetail(Globals.user));
    		//		request.setAttribute("listCancelOutboundDetail", mdlCancelOutboundDetailList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/cancel_outbound.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		if (Globals.user == null || Globals.user == "")
		{ 
			return;
		}
		
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtDocNumber = request.getParameter("txtDocNumber");
		
		String newDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtDate"), "dd MMM yyyy", "yyyy-MM-dd");
		String txtDate = newDate;
		
		String txtRefDocNumber = request.getParameter("txtRefDocNumber");
		
		model.mdlOutbound mdlOutbound = adapter.OutboundAdapter.LoadOutboundbyID(txtRefDocNumber);
		if(mdlOutbound.getPlantID()==null){
			Globals.gCondition = "FailedLoadOutbound";
			Globals.gConditionDesc = "Terjadi kesalahan saat menarik data outbound/keluar barang saat proses pembatalan";
		}
		else{
			String txtPlantID = mdlOutbound.getPlantID();
			String txtWarehouseID = mdlOutbound.getWarehouseID();
			
			//Declare mdlCancelTransaction for global
			model.mdlCancelTransaction mdlCancelOutbound = new model.mdlCancelTransaction();
			mdlCancelOutbound.setDocNumber(txtDocNumber);
			mdlCancelOutbound.setDate(txtDate);
			mdlCancelOutbound.setPlantID(txtPlantID);
			mdlCancelOutbound.setWarehouseID(txtWarehouseID);
			mdlCancelOutbound.setRefDocNumber(txtRefDocNumber);
			mdlCancelOutbound.setTransaction("Outbound");
				
			String lResult = "";
			String lCommand = "";
			if (keyBtn.equals("save")){
				
				lCommand = "Insert Cancel Outbound Doc : " + mdlCancelOutbound.getDocNumber();
				
				model.mdlClosingPeriod CheckLastPeriod = ClosingPeriodAdapter.LoadActivePeriod(Globals.user);
				String sLastPeriod = CheckLastPeriod.getPeriod();
				
				if(sLastPeriod == null || sLastPeriod == "")
				{	
					LogAdapter.InsertLogExc("Period is not set", "InsertCancelOutbound", lCommand , Globals.user);
					//lResult = "Error Insert Cancel Outbound Document";
					lResult = "Periode belum ditentukan";
				}
				else
				{
					try{
						DateFormat dfPeriod = new SimpleDateFormat("yyyy-MM");
						Date LastPeriod_On = dfPeriod.parse(sLastPeriod.substring(0,4)+"-"+sLastPeriod.substring(4,6));
						Date CancelOutboundDate = dfPeriod.parse(mdlCancelOutbound.getDate().substring(0, 7));
						
						if(LastPeriod_On.equals(CancelOutboundDate))
						{
							List<model.mdlOutboundDetail> mdlLoadOutboundDetail = OutboundDetailAdapter.LoadOutboundDetail(txtRefDocNumber, Globals.user);
							if(mdlLoadOutboundDetail == null || mdlLoadOutboundDetail.size() == 0 || mdlLoadOutboundDetail.isEmpty())
							{
								Globals.gCondition = "FailedLoadOutboundDetail";
								Globals.gConditionDesc = "Terjadi kesalahan saat menarik data detil outbound/keluar barang saat proses pembatalan";
							}
							else
								lResult = CancelTransactionAdapter.TransactionCancelOutbound(mdlCancelOutbound,mdlLoadOutboundDetail,Globals.user);
						}
						else
						{
							LogAdapter.InsertLogExc("Your input date period is not activated", "InsertCancelOutbound", lCommand , Globals.user);
							lResult = "Tanggal yang Anda masukkan tidak diizinkan karena tidak sesuai dengan periode yang aktif";
						}
					}
					catch(Exception e){
						LogAdapter.InsertLogExc(e.toString(), "InsertCancelOutbound", lCommand , Globals.user);
						lResult = "Terjadi kesalahan pada sistem saat proses penguraian tanggal";
					}
				}
				
				if(lResult.contains("Success Insert Cancel Outbound Document")) {  
					Globals.gCondition = "SuccessInsertCancelOutbound";
					//TraceCodeAdapter.InsertTraceCode(txtDocNumber, LocalDateTime.now().toString().replace("T"," ") ,"Outbound"); //001 Nanda
		        }  
		        else {
			        if(!Globals.gCondition.contains("FailedLoadOutboundDetail")){
			        	Globals.gCondition = "FailedInsertCancelOutbound"; 
			        	Globals.gConditionDesc = lResult;
		        	}
		        }
			}
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);	
		
		return;
    }

}
