package com.inventorywarehouse;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BomAdapter;
import adapter.ProductUomAdapter;

@WebServlet(urlPatterns={"/getValidateInputQuantityConfirmation"} , name="getValidateInputQuantityConfirmation")
public class GetValidateConfirmationServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetValidateConfirmationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
		String lProductID = request.getParameter("product");
    	String lInputQty = request.getParameter("inputqty");
    	String lInputUOM = request.getParameter("inputuom");
	    	//Get the Conversion of input qty
    		model.mdlProductUom inputmdlProductUom = new model.mdlProductUom();
    		inputmdlProductUom = ProductUomAdapter.LoadProductUOMByKey(lProductID, lInputUOM);
			String txtInputQtyBase = String.valueOf(Integer.parseInt(inputmdlProductUom.getQty())*Integer.parseInt(lInputQty));
			String txtInputQtyBaseUOM = inputmdlProductUom.getBaseUOM();
    	String lTargetQty = request.getParameter("targetqty");
    	String lTargetUOM = request.getParameter("targetuom");
	    	//Get the Conversion of target qty
    		model.mdlProductUom targetmdlProductUom = new model.mdlProductUom();
    		targetmdlProductUom = ProductUomAdapter.LoadProductUOMByKey(lProductID, lTargetUOM);
			String txtTargetQtyBase = String.valueOf(Integer.parseInt(targetmdlProductUom.getQty())*Integer.parseInt(lTargetQty));
			String txtTargetQtyBaseUOM = targetmdlProductUom.getBaseUOM();
    	
    	String Validate = "";
    	
    	if( Integer.parseInt(txtInputQtyBase) > Integer.parseInt(txtTargetQtyBase) )
    		Validate = "disallow";
    	else
    		Validate = "allow";
    	
    	response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Validate);
	}
	
}
