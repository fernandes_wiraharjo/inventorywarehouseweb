package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.StorageSectionAdapter;
import adapter.StorageTypeAdapter;

@WebServlet(urlPatterns={"/getstoragesection"} , name="getstoragesection")
public class GetDynamicStorageSectionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicStorageSectionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lStorageTypeID = request.getParameter("storagetypeid");
    	
    	List<model.mdlStorageSection> listStorageSection = new ArrayList<model.mdlStorageSection>();
		
    	listStorageSection.addAll(StorageSectionAdapter.LoadDynamicStorageSection(lPlantID, lWarehouseID, lStorageTypeID));
		request.setAttribute("listStorageSection", listStorageSection);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicStorageSection.jsp");
		dispacther.forward(request, response);
		
	}
    
}
