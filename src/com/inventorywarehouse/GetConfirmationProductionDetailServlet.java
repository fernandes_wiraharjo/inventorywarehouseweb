package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BomAdapter;
import adapter.CancelTransactionAdapter;
import adapter.ConfirmationAdapter;
import adapter.LogAdapter;
import adapter.ProductUomAdapter;
import adapter.ProductionOrderAdapter;
import adapter.UomAdapter;
import model.Globals;
import model.mdlBOMProductionOrderDetail;
import model.mdlInbound;

@WebServlet(urlPatterns={"/getConfirmationProductionDetail"} , name="getConfirmationProductionDetail")
public class GetConfirmationProductionDetailServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetConfirmationProductionDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//to load the confirmation detail after postback (when adding a additional component)
		String lOrderTypeID = request.getParameter("orderType");	
    	String lOrderNo = request.getParameter("orderNo");
		String lPlantID = request.getParameter("plantID");
		String lWarehouseID = request.getParameter("warehouseID");
		
		//get the temporary production detail list
		List<model.mdlBOMProductionOrderDetail> mdlTempProductionOrderDetailList = new ArrayList<model.mdlBOMProductionOrderDetail>();
		mdlTempProductionOrderDetailList = ProductionOrderAdapter.LoadTempProductionOrderDetail(lOrderTypeID, lOrderNo);
		//store temporary production detail data
		request.setAttribute("listProductionOrderDetail", mdlTempProductionOrderDetailList);

		request.setAttribute("OrderTypeID", lOrderTypeID);
		request.setAttribute("OrderNo", lOrderNo);
		request.setAttribute("PlantID", lPlantID);
		request.setAttribute("WarehouseID", lWarehouseID);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getProductionOrderDetailForConfirmation.jsp");
		dispacther.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
		//List<model.mdlBOMProductionOrderDetail> newlistProductionOrderDetail = new ArrayList<model.mdlBOMProductionOrderDetail>();
		
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		String lConfirmID = request.getParameter("confirmationID");
		String lConfirmNo = request.getParameter("confirmationNo");
		String lConfirmDate = request.getParameter("confirmationDate");
		//String lProductID = request.getParameter("product");
		String lOrderTypeID = request.getParameter("orderType");	
    	String lOrderNo = request.getParameter("orderNo");
    	String lQty = request.getParameter("qty");
    	String lUom = request.getParameter("uom");
	    	//Get the Conversion
    		//model.mdlProductUom mdlProductUom = new model.mdlProductUom();
    		//mdlProductUom = ProductUomAdapter.LoadProductUOMByKey(lProductID, lUom);
		//String lQtyBase = String.valueOf(Integer.parseInt(mdlProductUom.getQty())*Integer.parseInt(lQty));
		//String lBaseUom = mdlProductUom.getBaseUOM();
		String lPlantID = request.getParameter("plantID");
		String lWarehouseID = request.getParameter("warehouseID");
		String lComponentLine = request.getParameter("componentline");
		String lComponentID = request.getParameter("componentid");
		
		if(!keyBtn.equals("showupdateddetail")){
			List<model.mdlBOMProductionOrderDetail> mdlProductionOrderDetailList = new ArrayList<model.mdlBOMProductionOrderDetail>();
			mdlProductionOrderDetailList = ConfirmationAdapter.LoadBOMProductionOrderDetailforConfirmation(lOrderTypeID, lOrderNo, lQty);
			
			//delete existing temporary production order detail
			ProductionOrderAdapter.DeleteTempProductionOrderDetail(lOrderTypeID, lOrderNo);
			
			//delete existing temporary batch summary
			ProductionOrderAdapter.DeleteAllTempBatchSummary(lOrderTypeID, lOrderNo);
			
			//insert production order detail to temporary table
			ProductionOrderAdapter.TransactionInsertTempProductionDetail(mdlProductionOrderDetailList);
		}
		else{
			//delete existing temporary batch summary by updated component
			ProductionOrderAdapter.DeleteTempBatchSummaryByUpdatedComponent(lOrderTypeID, lOrderNo, lComponentLine, lComponentID);
		}
		
		//get the temporary production detail list
		List<model.mdlBOMProductionOrderDetail> mdlTempProductionOrderDetailList = new ArrayList<model.mdlBOMProductionOrderDetail>();
		mdlTempProductionOrderDetailList = ProductionOrderAdapter.LoadTempProductionOrderDetail(lOrderTypeID, lOrderNo);
		
//		for(mdlBOMProductionOrderDetail lParamProductionOrderDetail : mdlProductionOrderDetailList)
//		{
//			//lParamProductionOrderDetail.setQtyConfirmation(lQtyBase);
//			newlistProductionOrderDetail.add(lParamProductionOrderDetail);
//		}
		
		//store temporary production detail data
		request.setAttribute("listProductionOrderDetail", mdlTempProductionOrderDetailList);
		
//		List<model.mdlUom> listUom = new ArrayList<model.mdlUom>();
//		listUom.addAll(adapter.UomAdapter.LoadMasterUOM());
//		request.setAttribute("listUom", listUom);

		request.setAttribute("ConfirmID", lConfirmID);
		request.setAttribute("ConfirmNo", lConfirmNo);
		request.setAttribute("ConfirmDate", lConfirmDate);
		request.setAttribute("OrderTypeID", lOrderTypeID);
		request.setAttribute("OrderNo", lOrderNo);
//		request.setAttribute("ProductID", lProductID);
		request.setAttribute("ProductQty", lQty);
		request.setAttribute("ProductUOM", lUom);
//		request.setAttribute("ProductQtyBase", lQtyBase);
//		request.setAttribute("ProductBaseUOM", lBaseUom);
		request.setAttribute("PlantID", lPlantID);
		request.setAttribute("WarehouseID", lWarehouseID);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getProductionOrderDetailForConfirmation.jsp");
		dispacther.forward(request, response);
		
	}
	
}
