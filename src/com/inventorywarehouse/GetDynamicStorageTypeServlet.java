package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.StorageTypeAdapter;
import adapter.WarehouseAdapter;

@WebServlet(urlPatterns={"/getstoragetype"} , name="getstoragetype")
public class GetDynamicStorageTypeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicStorageTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lStorageTypeID = request.getParameter("storagetypeid");
    	String lType = request.getParameter("type");
    	//for storage type search
    	String[] listSeq = request.getParameterValues("listSeq[]");
    	String newlistSeq = "''";
    	if(listSeq!=null)
    		newlistSeq = String.join(",", listSeq);
    	
    	List<model.mdlStorageType> listStorageType = new ArrayList<model.mdlStorageType>();
		
    	listStorageType.addAll(StorageTypeAdapter.LoadDynamicStorageType(lPlantID, lWarehouseID, lStorageTypeID, newlistSeq));
		request.setAttribute("listStorageType", listStorageType);
		request.setAttribute("type", lType);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicStorageType.jsp");
		dispacther.forward(request, response);
		
	}
    
}
