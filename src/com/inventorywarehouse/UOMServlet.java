package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.ProductAdapter;
import adapter.UomAdapter;
import model.Globals;

@WebServlet("/uom")
public class UOMServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
    
    public UOMServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "uom";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlUom> listUOM = new ArrayList<model.mdlUom>();
    		
    		listUOM = UomAdapter.LoadMasterUOM();
    		request.setAttribute("listuom", listUOM);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/uom.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);	
		
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtCode = request.getParameter("txtCode");
		String txtDescription = request.getParameter("txtDescription");
		String txtIsBaseUOM = request.getParameter("txtIsBaseUOM");
		
		//Declare mdlUOM for global
		model.mdlUom mdlUOM = new model.mdlUom();
		mdlUOM.setCode(txtCode);
		mdlUOM.setDescription(txtDescription);
		mdlUOM.setIs_BaseUOM(Boolean.valueOf(txtIsBaseUOM));
			
		String lResult = "";
		if (keyBtn.equals("save")){
			lResult = UomAdapter.InsertUom(mdlUOM);
			
			if(lResult.contains("Success Insert UOM")) {  
				Globals.gCondition = "SuccessInsertUOM";
            }  
            else {
            	Globals.gCondition = "FailedInsertUOM";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (keyBtn.equals("update")){
		    lResult = UomAdapter.UpdateUom(mdlUOM);
		    
		    if(lResult.contains("Success Update UOM")) {  
		    	Globals.gCondition = "SuccessUpdateUOM";
            }  
            else {
            	Globals.gCondition = "FailedUpdateUOM";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
	}
    
}
