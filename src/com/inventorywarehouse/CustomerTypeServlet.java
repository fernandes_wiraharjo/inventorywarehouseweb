package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CustomerAdapter;
import adapter.CustomerTypeAdapter;
import adapter.MenuAdapter;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/CustomerType"}, name="CustomerType")
public class CustomerTypeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CustomerTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Boolean CheckMenu;
    	String MenuURL = "CustomerType";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlCustomerType> mdlCustomerTypeList = new ArrayList<model.mdlCustomerType>();
    		
    		mdlCustomerTypeList.addAll(CustomerTypeAdapter.LoadCustomerType(Globals.user));
    		request.setAttribute("listcusttype", mdlCustomerTypeList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/customer_type.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String btnDelete = request.getParameter("btnDelete");
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
				
		//Declare TextBox
		String txtCustTypeID = request.getParameter("txtCustTypeID");
		String txtCustTypeName = request.getParameter("txtCustTypeName");
		String txtDescription = request.getParameter("txtDescription");
		String temp_txtCustTypeID = request.getParameter("temp_txtCustTypeID");
				
		//Declare mdlVendor for global
		model.mdlCustomerType mdlCustomerType = new model.mdlCustomerType();
		mdlCustomerType.setCustomerTypeID(txtCustTypeID);
		mdlCustomerType.setCustomerTypeName(txtCustTypeName);
		mdlCustomerType.setDescription(txtDescription);			
				
		if (keyBtn.equals("save")){
			String lResult = CustomerTypeAdapter.InsertCustomerType(mdlCustomerType, Globals.user);
			if(lResult.contains("Success Insert Customer Type")) {  
				Globals.gCondition = "SuccessInsertCustomerType";
		    }  
		    else {
		        Globals.gCondition = "FailedInsertCustomerType"; 
		        Globals.gConditionDesc = lResult;
		    }
		}
				
		if (keyBtn.equals("update")){
		    String lResult = CustomerTypeAdapter.UpdateCustomerType(mdlCustomerType,Globals.user);	
				    
		    if(lResult.contains("Success Update Customer Type")) {  
		    	Globals.gCondition = "SuccessUpdateCustomerType";
		          }  
		    else {
	           	Globals.gCondition = "FailedUpdateCustomerType"; 
	           	Globals.gConditionDesc = lResult;
            }
		}
				
		if (btnDelete != null){
			String lResult = CustomerTypeAdapter.DeleteCustomerType(temp_txtCustTypeID,Globals.user);
			if(lResult.contains("Success Delete Customer Type")) {  
				Globals.gCondition =  "SuccessDeleteCustomerType";
            }  
            else {
            	Globals.gCondition = "FailedDeleteCustomerType"; 
            	Globals.gConditionDesc = lResult;
            } 
			doGet(request, response);
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
        return;
	}

}
