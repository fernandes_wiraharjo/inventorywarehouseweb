package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.StorageBinAdapter;
import adapter.StorageSectionAdapter;

@WebServlet(urlPatterns={"/getstoragebintype"} , name="getstoragebintype")
public class GetDynamicStorageBinTypeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicStorageBinTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	
    	List<model.mdlStorageBinType> listStorageBinType = new ArrayList<model.mdlStorageBinType>();
		
    	listStorageBinType.addAll(StorageBinAdapter.LoadDynamicStorageBinType(lPlantID, lWarehouseID));
		request.setAttribute("listStorageBinType", listStorageBinType);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicStorageBinType.jsp");
		dispacther.forward(request, response);
		
	}
    
}
