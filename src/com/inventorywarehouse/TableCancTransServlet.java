package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CancelTransactionAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

/**Documentation
 * 
 */
@WebServlet(urlPatterns={"/TableCancTrans"} , name="TableCancTrans")
public class TableCancTransServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public TableCancTransServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("condition", Globals.gCondition);	
		
		//		String lParambtn = Globals.gKey;
		//		//String btnShow = request.getParameter("btnShow");
		//		if (!lParambtn.isEmpty()){		
		//			List<model.mdlRptCancelTransaction> mdlRptCancTransList = new ArrayList<model.mdlRptCancelTransaction>();			
		//			//buat adapter sesuai parameter untuk menampilkan table GoodRecipt
		//			mdlRptCancTransList = CancelTransactionAdapter.LoadReportCancelTransaction(Globals.gcbCancTrans ,Globals.gStartDate, Globals.gEndDate);
		//			request.setAttribute("listcancgoodreceipt", mdlRptCancTransList);
		//		}
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/Child_Rpt/Table_CancelTransaction.jsp");
		dispacther.forward(request, response);
		Globals.gCondition = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lStartDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtStartDate"),"dd MMM yyyy", "yyyy-MM-dd");
		String lEndDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtEndDate"),"dd MMM yyyy", "yyyy-MM-dd");
		String[] lcbCancTranslist = request.getParameterValues("cbCancTrans");
		String lcbCancTrans = "";
		
		if (lcbCancTranslist != null) {
		    for (String param : lcbCancTranslist) {
		    	lcbCancTrans = param;
		    }
		}
		
		String lParambtn = request.getParameter("key");
		//String btnShow = request.getParameter("btnShow");
		if (!lParambtn.isEmpty()){		
			List<model.mdlRptCancelTransaction> mdlRptCancTransList = new ArrayList<model.mdlRptCancelTransaction>();			
			//buat adapter sesuai parameter untuk menampilkan table GoodRecipt
			request.setAttribute("lcbCancTrans", lcbCancTrans);
			request.setAttribute("lStartDate", ConvertDateTimeHelper.formatDate(lStartDate, "yyyy-MM-dd", "dd MMM yyyy"));
			request.setAttribute("lEndDate", ConvertDateTimeHelper.formatDate(lEndDate, "yyyy-MM-dd", "dd MMM yyyy"));
			mdlRptCancTransList = CancelTransactionAdapter.LoadReportCancelTransaction(lcbCancTrans , lStartDate, lEndDate);
			request.setAttribute("listcancelgoodreceipt", mdlRptCancTransList);
		}
		

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/Child_Rpt/Table_CancelTransaction.jsp");
		dispacther.forward(request, response);
		

		//		//String btnShow = request.getParameter("btnShow");
		//		if (lParambtn != null){		
		//			Globals.gStartDate = lStartDate;
		//			Globals.gEndDate = lEndDate;
		//			Globals.gKey = lParambtn;
		//			Globals.gcbCancTrans = lcbCancInbound;
		//		}
	}


}
