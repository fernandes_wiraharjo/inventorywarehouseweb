package com.inventorywarehouse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.LogAdapter;
import adapter.ProductionOrderAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

@WebServlet(urlPatterns={"/tempbatchsummary"} , name="tempbatchsummary")
public class TempBatchSummaryServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public TempBatchSummaryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	List<model.mdlTempComponentBatchSummary> listTempComponentBatchSummary = new ArrayList<model.mdlTempComponentBatchSummary>();
    	
    	String lResult = "";
    	//Declare parameter
		String[] listParam = request.getParameterValues("listParam[]");
		int y; 
		int x;
		int i;
		int column = 12;
		int row = listParam.length / column;
		
		int targetqtybase = Integer.valueOf(listParam[0]);
		String lparamordertypeid = listParam[1];
		String lparamorderno = listParam[2];
		String lparamcomponentline = listParam[3];
		String lparamcomponentid = listParam[4];
		
		boolean checkTempTable = false;
		
		//check if the component is expired or not
		for(y=1 ; y<=row ; y++)
		{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date now = new Date();
			Date dcomponentExpiredDate;
			String scomponentExpiredDate = ConvertDateTimeHelper.formatDate(listParam[(y*column)-5], "dd MMM yyyy", "yyyy-MM-dd");
			try {
				dcomponentExpiredDate = df.parse(scomponentExpiredDate);
				
				//add 1 day for component expired date because the time is 00:00:00 by default
				Calendar c = Calendar.getInstance(); 
				c.setTime(dcomponentExpiredDate); 
				c.add(Calendar.DATE, 1);
				dcomponentExpiredDate = c.getTime();
				
				if(now.after(dcomponentExpiredDate))
				{
					lResult = "ExpiredComponentBatchFound"+listParam[(y*column)-7];
					LogAdapter.InsertLogExc("Komponen " + listParam[(y*column)-7] + " sudah kadaluarsa", "TransactionInsertTempBatchSummary", "ExpiredComponentBatchFound", Globals.user);
					break;
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogAdapter.InsertLogExc(e.toString(), "TransactionInsertTempBatchSummary", "ConvertExpiredToDateFormat", Globals.user);
				lResult = "ExpiredDateFormatConversionError"+listParam[(y*column)-5];
				break;
			}
		}
		//if expired or error convert expired date
		if(lResult.contains("ExpiredComponentBatchFound") || lResult.contains("ExpiredDateFormatConversionError"))
		{
			Globals.gCondition = lResult;
		}
		//if not expired or convert expired date success
		else
		{
			//check if the existing component qty is enough for transaction or not
			int componentQty = 0;
			for(x=1 ; x<=row ; x++)
			{
				componentQty = componentQty + Integer.valueOf(listParam[(x*column)-2]);
			}
			//if component enough
			//if(componentQty >= Integer.parseInt(String.valueOf(targetqtybase).replace("-", "")) )
			if(componentQty >= targetqtybase)
			{
				//set batch proccess
				checkTempTable = ProductionOrderAdapter.CheckTemporaryBatchData(lparamordertypeid, lparamorderno, lparamcomponentline, lparamcomponentid);
				
				if(checkTempTable)
				{
					ProductionOrderAdapter.DeleteTempBatchSummary(lparamordertypeid, lparamorderno, lparamcomponentline, lparamcomponentid);
				}
				
				for(i=1 ; i<=row ; i++)
				{
					model.mdlTempComponentBatchSummary TempComponentBatchSummary = new model.mdlTempComponentBatchSummary();
							
					TempComponentBatchSummary.setOrderTypeID(listParam[(i*column)-11]);
					TempComponentBatchSummary.setOrderNo(listParam[(i*column)-10]);
					TempComponentBatchSummary.setComponentLine(listParam[(i*column)-9]);
					TempComponentBatchSummary.setComponentID(listParam[(i*column)-8]);
					TempComponentBatchSummary.setBatchNo(listParam[(i*column)-7]);
					TempComponentBatchSummary.setGRDate(ConvertDateTimeHelper.formatDate(listParam[(i*column)-6], "dd MMM yyyy", "yyyy-MM-dd"));
					TempComponentBatchSummary.setExpiredDate(ConvertDateTimeHelper.formatDate(listParam[(i*column)-5], "dd MMM yyyy" , "yyyy-MM-dd"));
					TempComponentBatchSummary.setPlantID(listParam[(i*column)-4]);
					TempComponentBatchSummary.setWarehouseID(listParam[(i*column)-3]);
					
					//if(Integer.valueOf(listParam[(i*column)-2]) >= Integer.parseInt(String.valueOf(targetqtybase).replace("-", "")) )
					if(Integer.valueOf(listParam[(i*column)-2]) >= targetqtybase)
					{	
						TempComponentBatchSummary.setQty(targetqtybase);
						TempComponentBatchSummary.setUOM(listParam[(i*column)-1]);
						
						listTempComponentBatchSummary.add(TempComponentBatchSummary);
						break;
					}
					else
					{
						//if(String.valueOf(targetqtybase).contains("-"))
						//TempComponentBatchSummary.setQty(-Integer.valueOf(listParam[(i*column)-2]));
						//else
							TempComponentBatchSummary.setQty(Integer.valueOf(listParam[(i*column)-2]));
						
						TempComponentBatchSummary.setUOM(listParam[(i*column)-1]);
						listTempComponentBatchSummary.add(TempComponentBatchSummary);
						
						targetqtybase = targetqtybase - TempComponentBatchSummary.getQty();
					}
				}
				
				lResult = "";
				lResult = ProductionOrderAdapter.TransactionInsertTempBatchSummary(listTempComponentBatchSummary);
				
				if(lResult.contains("Success Insert Temporary Component Batch Summary")) {  
					Globals.gCondition = "SuccessInsertTemporaryBatch";
		        }  
		        else {
		        	Globals.gCondition = "FailedInsertTemporaryBatch";
		        	//Globals.gConditionDesc = lResult;
		        }
				//end of set batch proccess
			}
			//if component is not enough
			else
			{
				Globals.gCondition = "ComponentNotAvailable";
				LogAdapter.InsertLogExc("Kuantiti komponen tidak cukup", "TransactionInsertTempBatchSummary", "TransactionInsertTempBatchSummary", Globals.user);
			}
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition);
		
		return;
    }
    
}
