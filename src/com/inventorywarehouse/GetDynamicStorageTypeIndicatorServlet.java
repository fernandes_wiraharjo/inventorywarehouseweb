package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.StorageTypeAdapter;
import adapter.StorageTypeIndicatorAdapter;

@WebServlet(urlPatterns={"/getstoragetypeindicator"} , name="getstoragetypeindicator")
public class GetDynamicStorageTypeIndicatorServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicStorageTypeIndicatorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lType = request.getParameter("type");
    	
    	List<model.mdlStorageTypeIndicator> listStorageTypeIndicator = new ArrayList<model.mdlStorageTypeIndicator>();
		
    	listStorageTypeIndicator.addAll(StorageTypeIndicatorAdapter.LoadStorageTypeIndicatorByPlantWarehouse(lPlantID, lWarehouseID));
		request.setAttribute("listStorageTypeIndicator", listStorageTypeIndicator);
		request.setAttribute("type", lType);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicStorageTypeIndicator.jsp");
		dispacther.forward(request, response);
		
    }
    
}
