package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.DoorAdapter;
import adapter.StorageTypeAdapter;

@WebServlet(urlPatterns={"/getdoor"} , name="getdoor")
public class GetDynamicDoorServlet extends HttpServlet {

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicDoorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	
    	List<model.mdlDoor> listDoor = new ArrayList<model.mdlDoor>();
		
    	listDoor.addAll(DoorAdapter.LoadDoorByPlantAndWarehouse(lPlantID, lWarehouseID));
		request.setAttribute("listDoor", listDoor);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicDoor.jsp");
		dispacther.forward(request, response);
    }
    
}
