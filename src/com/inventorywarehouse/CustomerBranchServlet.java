package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CustomerAdapter;
import adapter.CustomerBranchAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.VendorAdapter;
import model.Globals;
import model.mdlCustomerBranch;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/CustomerBranch"}, name="CustomerBranch")
public class CustomerBranchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CustomerBranchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
//    private static void GetLoadCustBranch(){
//    	List<model.mdlCustomerBranch> mdlCustBranchList = new ArrayList<model.mdlCustomerBranch>();
//		mdlCustBranchList.addAll(CustomerBranchAdapter.LoadCustomerBranch());
//    	
//    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Boolean CheckMenu;
    	String MenuURL = "CustomerBranch";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlCustomer> mdlCustomerList = new ArrayList<model.mdlCustomer>();
    		mdlCustomerList.addAll(CustomerAdapter.LoadCustomer(Globals.user));
    		request.setAttribute("listcustomer", mdlCustomerList);
    		
    		List<model.mdlCustomerBranch> mdlCustBranchList = new ArrayList<model.mdlCustomerBranch>();
    		mdlCustBranchList.addAll(CustomerBranchAdapter.LoadCustomerBranchJoin(Globals.user));
    		request.setAttribute("listcustbranch", mdlCustBranchList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/customer_branch.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String btnDelete = request.getParameter("btnDelete");
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
				
		//Declare TextBox
		String txtCustBranchID = request.getParameter("txtCustBranchID");
		String txtCustBranchName = request.getParameter("txtCustBranchName");
		String txtCustBranchAddress = request.getParameter("txtCustBranchAddress");
		String txtPhone = request.getParameter("txtPhone");
		String txtEmail = request.getParameter("txtEmail");
		String txtPIC = request.getParameter("txtPIC");
		String txtCustomerID = request.getParameter("txtCustomerID");
		String temp_txtCustBranchID = request.getParameter("temp_txtCustBranchID");
				
		//Declare mdlCustomer for global
		model.mdlCustomerBranch mdlCustBranch = new model.mdlCustomerBranch();
		mdlCustBranch.setCustBranchID(txtCustBranchID);
		mdlCustBranch.setCustBranchName(txtCustBranchName);
		mdlCustBranch.setCustBranchAddress(txtCustBranchAddress);
		mdlCustBranch.setEmail(txtEmail);
		mdlCustBranch.setPic(txtPIC);
		mdlCustBranch.setPhone(txtPhone);
		mdlCustBranch.setCustomerID(txtCustomerID);
			
						
		if (keyBtn.equals("save")){
			String lResult = CustomerBranchAdapter.InsertCustomerBranch(mdlCustBranch,Globals.user);	
			
			if(lResult.contains("Success Insert Customer Branch")) {  
				Globals.gCondition = "SuccessInsertCustomerBranch";
		            }  
		    else {
		        Globals.gCondition = "FailedInsertCustomerBranch";
		        Globals.gConditionDesc = lResult;
		    }
		}
				
		if (keyBtn.equals("update")){
		    String lResult = CustomerBranchAdapter.UpdateCustomerBranch(mdlCustBranch,Globals.user);	
				    
		    if(lResult.contains("Success Update Customer Branch")) {  
		    	Globals.gCondition = "SuccessUpdateCustomerBranch";
		    }  
		    else {
		       	Globals.gCondition = "FailedUpdateCustomerBranch"; 
		       	Globals.gConditionDesc = lResult;
	        }
		}
				
 		if (btnDelete != null){
			String lResult = CustomerBranchAdapter.DeleteCustomerBranch(temp_txtCustBranchID,Globals.user);
			
			if(lResult.contains("Success Delete Customer Branch")) {  
				Globals.gCondition =  "SuccessDeleteCustomerBranch";
		    }  
		    else {
		       	Globals.gCondition = "FailedDeleteCustomerBranch"; 
		       	Globals.gConditionDesc = lResult;
		   } 
			
			doGet(request, response);
		}
 		
 		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
	}
	
}
