package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BrandAdapter;
import adapter.GroupAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/Brand"} , name="Brand")
public class BrandServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public BrandServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean CheckMenu;
    	String MenuURL = "Brand";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlBrand> mdlBrandList = new ArrayList<model.mdlBrand>();
    		
    		mdlBrandList = BrandAdapter.GetBrandAPI(Globals.user);
    		String lResult = BrandAdapter.InsertBrandlist(mdlBrandList,Globals.user);
    		request.setAttribute("listbrand", mdlBrandList);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/brand.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
