package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.DepartmentAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.TokenAdapter;
import adapter.VendorAdapter;
import model.Globals;


/** Documentation
 * 
 */
@WebServlet("/Department")
public class DepartmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DepartmentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean CheckMenu;
    	String MenuURL = "Department";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlDepartment> mdlDepartmentList = new ArrayList<model.mdlDepartment>();
    		
    		mdlDepartmentList = DepartmentAdapter.GetDepartmentAPI(Globals.user);
    		request.setAttribute("listdepartment", mdlDepartmentList);
    		
    		String lResult = DepartmentAdapter.InsertDepartmentlist(mdlDepartmentList,Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/department.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request,response);
		
//		//Declare button
//		String keyBtn = request.getParameter("key");
//		if (keyBtn == null){
//			keyBtn = new String("");
//		}
//				
//		String btnDelete = request.getParameter("btnDelete");
//			
//		//Declare TextBox
//		String txtId = request.getParameter("txtId");
//		String txtCode = request.getParameter("txtCode");
//		String txtTitle_En = request.getParameter("txtTitle_En");
//		String txtTitle_Id = request.getParameter("txtTitle_Id");
//		String txtSlug = request.getParameter("txtSlug");
//		String txtIcon = request.getParameter("txtIcon");
//		String txtBanner = request.getParameter("txtBanner");
//		String txtOrder = request.getParameter("txtOrder");
//		String txtStatus = request.getParameter("txtStatus");
//		String txtCreated_at = request.getParameter("txtCreated_at");
//		String txtUpdate_at = request.getParameter("txtUpdate_at");
//		String temp_txtId = request.getParameter("temp_txtId");
//				
//		model.mdlToken mdlToken = TokenAdapter.GetToken();
//		
//		if (keyBtn.equals("save")){
//				
//			model.mdlParInsDepartment mdlParInsDepartment = new model.mdlParInsDepartment();
//			mdlParInsDepartment.setToken(mdlToken.getToken());
//			mdlParInsDepartment.setTitle_en(txtTitle_En);
//			mdlParInsDepartment.setTitle_id(txtTitle_Id);
//			mdlParInsDepartment.setStatus(txtStatus);
//			mdlParInsDepartment.setOrder(txtOrder); 
//			mdlParInsDepartment.setIcon(txtIcon);
//			mdlParInsDepartment.setBanner(txtBanner);
//					
//			model.mdlDepartment mdlDepartment = new model.mdlDepartment();
//			//Testing
//			//mdlDepartment = DepartmentAdapter.SendInsertDepartmentbyAPI(mdlParInsDepartment);
//
//			String lResult = DepartmentAdapter.InsertDeparment(mdlDepartment);	
//			if(lResult.contains("Success Insert Department")) {  
//				Globals.gCondition = "SuccessInsert";
//            }  
//            else {
//            	Globals.gCondition = "FailedInsert"; 
//            } 
//			
//			return;
//
//		}
//				
//		if (keyBtn.equals("update")){
//			
//			model.mdlParUptDepartment mdlParUptDepartment = new model.mdlParUptDepartment();
//			mdlParUptDepartment.setToken(mdlToken.getToken());
//			mdlParUptDepartment.setId(txtId);
//			mdlParUptDepartment.setTitle_en(txtTitle_En);
//			mdlParUptDepartment.setTitle_id(txtTitle_Id);
//			mdlParUptDepartment.setStatus(txtStatus);
//			mdlParUptDepartment.setOrder(txtOrder); 
//			mdlParUptDepartment.setIcon(txtIcon);
//			mdlParUptDepartment.setBanner(txtBanner);
//			
//			model.mdlDepartment mdlDepartment = new model.mdlDepartment();
//			//Testing
//			//mdlDepartment = DepartmentAdapter.SendUpdateDepartmentbyAPI(mdlParUptDepartment);
//			
//		    String lResult = DepartmentAdapter.UpdateDepartment(mdlDepartment);	
//				    
//		    if(lResult.contains("Success Update Department")) {  
//		    	Globals.gCondition = "SuccessUpdate";
//            }  
//            else {
//            	Globals.gCondition = "FailedUpdate"; 
//            } 
//			
//			retusrn;
//		}	
	}
}
