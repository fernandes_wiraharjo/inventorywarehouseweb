package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.UomAdapter;
import adapter.ValidateNull;
import model.Globals;

@WebServlet(urlPatterns={"/getMenu"} , name="getMenu")
public class GetMenuServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetMenuServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	String lMenuID = ValidateNull.NulltoStringEmpty(request.getParameter("menuid"));
    	String lCommand = request.getParameter("command");
    	List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
    	List<model.mdlMenu> EditableMenulist = new ArrayList<model.mdlMenu>();
    	Gson gson = new Gson();
    	String json = "";
    	
    	if (lCommand.equals("Add")){
    		if(lMenuID.contentEquals(""))
    			lMenuID=null;
    		else if(lMenuID.contentEquals("All"))
    			lMenuID="";

    		if(lMenuID.contentEquals(""))
        	{
        		mdlMenuList.addAll(MenuAdapter.LoadMenu());
        	}
        	else
        	{
        		mdlMenuList.addAll(MenuAdapter.LoadMenuByID(lMenuID,lCommand));
        	}
    		
        	json = gson.toJson(mdlMenuList);
    	}
    	
    	if (lCommand.equals("Remove")){
    		
    		if(!lMenuID.contentEquals(""))
        	{
        		mdlMenuList.addAll(MenuAdapter.LoadMenuByID(lMenuID,lCommand));
        	}
    		json = gson.toJson(mdlMenuList);
    		
    	}
        
        if (lCommand.equals("Update")){
    		mdlMenuList.addAll(MenuAdapter.LoadAllowedMenu(lMenuID));

    		EditableMenulist.addAll(MenuAdapter.LoadEditableMenu(lMenuID));
    		
    	
        	String jsonmdlMenuList = gson.toJson(mdlMenuList);
        	String jsonEditableMenulist = gson.toJson(EditableMenulist);
        	
        	json = "["+jsonmdlMenuList+","+jsonEditableMenulist+"]"; 
        	
    	}
        
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}
	
}
