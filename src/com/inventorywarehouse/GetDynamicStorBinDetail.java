package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.StorageBinAdapter;

@WebServlet(urlPatterns={"/getStorBinDetail"} , name="getStorBinDetail")
public class GetDynamicStorBinDetail extends HttpServlet{

private static final long serialVersionUID = 1L; 
    
    public GetDynamicStorBinDetail() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lStorageTypeID = request.getParameter("storagetypeid");
    	String lStorageSectionID = request.getParameter("storagesectionid");
    	String lStorageBinID = request.getParameter("storagebinid");
    	
    	List<model.mdlStorageBinDetail> listStorageBinDetail = new ArrayList<model.mdlStorageBinDetail>();
		
    	listStorageBinDetail.addAll(StorageBinAdapter.LoadDynamicStorageBinDetail(lPlantID, lWarehouseID, lStorageTypeID, lStorageSectionID, lStorageBinID));
		request.setAttribute("listStorageBinDetail", listStorageBinDetail);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicStorageBinDetail.jsp");
		dispacther.forward(request, response);
		
	}
    
}
