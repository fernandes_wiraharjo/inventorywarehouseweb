package com.inventorywarehouse;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BomAdapter;
import adapter.ConfirmationAdapter;
import adapter.LogAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getComponentLine"} , name="getComponentLine")
public class GetComponentLineServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetComponentLineServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//get component line for additional component proccess
		
    	String lOrderTypeID = request.getParameter("ordertypeid");
    	String lOrderNo = request.getParameter("orderno");
    	
    	Integer ResultComponentLine = 0;
		ResultComponentLine = ConfirmationAdapter.GetComponentLine(lOrderTypeID, lOrderNo);
    	
    	response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(String.valueOf(ResultComponentLine));
	}
	
}
