package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BomAdapter;
import adapter.PlantAdapter;
import adapter.ProductAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/ReleaseProductionOrder"} , name="ReleaseProductionOrder")
public class ReleaseProductionOrderServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public ReleaseProductionOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispacther;
		dispacther = request.getRequestDispatcher("/BomProductionOrder");
		dispacther.forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	String lOrderType = request.getParameter("orderType");
    	String lOrderNo = request.getParameter("orderNo");
    	
    	String lResult = BomAdapter.ReleaseProductionOrder(lOrderType, lOrderNo);
    	
    	if(lResult.contains("Success Release Production Order")) {  
			Globals.gCondition = "SuccessRelease";
        }  
        else {
        	Globals.gCondition = "FailedRelease"; 
        	Globals.gConditionDesc = lResult;
        }
    	
	}
	
}
