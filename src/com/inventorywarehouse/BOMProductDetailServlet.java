package com.inventorywarehouse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BatchAdapter;
import adapter.BomAdapter;
import adapter.InboundAdapter;
import adapter.LogAdapter;
import adapter.ProductAdapter;
import adapter.ProductUomAdapter;
import adapter.StockSummaryAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlProductDetail;

@WebServlet(urlPatterns={"/bomproductdetail"} , name="bomproductdetail")
public class BOMProductDetailServlet extends HttpServlet{

private static final long serialVersionUID = 1L;
    
    public BOMProductDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String componentLine,validDate,validDateShow,plantId,qty;
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Globals.gParam1 = request.getParameter("id");
    	plantId = request.getParameter("plant");
    	
    	String newDate = ConvertDateTimeHelper.formatDate(request.getParameter("date"), "dd MMM yyyy", "yyyy-MM-dd");
		validDate = newDate;
		validDateShow = request.getParameter("date");
		
		//qty = request.getParameter("qty");
    	
    	if (Globals.gParam1 == null)
    	{
    	Globals.gParam1 = Globals.gtemp_Param1; //store temporary string as product id
    	plantId = Globals.gtemp_Param2; //store temporary string as plant id
    	validDate = Globals.gtemp_Param3; //store temporary string as valid date for data
    	validDateShow = ConvertDateTimeHelper.formatDate(validDate, "yyyy-MM-dd", "dd MMM yyyy");; //store temporary string as valid date for show
    	//qty = Globals.gtemp_Param4; //store temporary string as product base quantity
    	}
    	
    	if(Globals.gParam1 == null)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/MasterBom");
    		dispacther.forward(request, response);
    		
    		return;
    	}
		
    	componentLine = BomAdapter.GenerateComponentLineBOMDetail(Globals.gParam1, validDate, plantId);
    	if(componentLine.isEmpty()){
    	 componentLine = "1";
		}
		else
		{
		componentLine = String.valueOf(Integer.parseInt(componentLine)+1);
		}
		request.setAttribute("tempComponentLine", componentLine);
		
		List<model.mdlProduct> mdlProductList = new ArrayList<model.mdlProduct>();
		mdlProductList.addAll(ProductAdapter.LoadComponentProduct());
		request.setAttribute("listComponent", mdlProductList);
		
		List<model.mdlBOMDetail> mdlBOMDetailList = new ArrayList<model.mdlBOMDetail>();
		mdlBOMDetailList.addAll(BomAdapter.LoadBOMDetail(Globals.gParam1, validDate, plantId));
		request.setAttribute("listBOMDetail", mdlBOMDetailList);
		
		request.setAttribute("condition", Globals.gCondition);
		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
		
		request.setAttribute("id", Globals.gParam1); //send bom product id to jsp via jstl
		request.setAttribute("validDate", validDate); //send bom valid date for data to jsp via jstl
		request.setAttribute("validDateShow", validDateShow); //send bom valid date for display to jsp via jstl
		request.setAttribute("plantId", plantId); //send bom plant id to jsp via jstl
		//request.setAttribute("qty", qty); //send bom base quantity to jsp via jstl
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/bom_product_detail.jsp");
		dispacther.forward(request, response);
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
		
		//clear string when leave the page
		Globals.gtemp_Param1 = null;
		//productName = null;
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String lResult = "";
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtProductID = request.getParameter("txtProductID");
		String txtDate = request.getParameter("txtDate");
		String txtPlantID = request.getParameter("txtPlantID");
		String txtComponentLine = request.getParameter("txtComponentLine");
		String txtComponentID = request.getParameter("txtComponentID");
		String txtQty = request.getParameter("txtQty");
		String txtUOM = request.getParameter("txtUOM");
		
		//Get the Conversion
		model.mdlProductUom mdlProductUom = new model.mdlProductUom();
		mdlProductUom = ProductUomAdapter.LoadProductUOMByKey(txtComponentID, txtUOM);
		if(mdlProductUom.getBaseUOM() == null || mdlProductUom.getBaseUOM().contentEquals("")) {
			lResult = "Satuan terkecil untuk komponen "+txtComponentID+" belum didefinisikan";
			Globals.gCondition = "FailedConversionComponent";
			Globals.gConditionDesc = lResult;
		}
		
		if(!lResult.contains("Satuan terkecil untuk")){
			String txtQtyBaseUOM = String.valueOf(Integer.parseInt(mdlProductUom.getQty())*Integer.parseInt(txtQty));
			String txtBaseUOM = mdlProductUom.getBaseUOM();
			
			//Declare mdlBOMDetail for global
			model.mdlBOMDetail mdlBOMDetail = new model.mdlBOMDetail();
			mdlBOMDetail.setProductID(txtProductID);
			mdlBOMDetail.setValidDate(txtDate);
			mdlBOMDetail.setPlantID(txtPlantID);
			mdlBOMDetail.setComponentLine(txtComponentLine);
			mdlBOMDetail.setComponentID(txtComponentID);
			mdlBOMDetail.setQty(txtQty);
			mdlBOMDetail.setUOM(txtUOM);
			mdlBOMDetail.setQtyBaseUOM(txtQtyBaseUOM);
			mdlBOMDetail.setBaseUOM(txtBaseUOM);
			
			if (keyBtn.equals("save")){
				
				lResult = BomAdapter.InsertBOMDetail(mdlBOMDetail);
				if(lResult.contains("Success Insert BOM Detail")) {  
					Globals.gCondition = "SuccessInsertComponent";
	            }  
	            else {
	            	Globals.gCondition = "FailedInsertComponent"; 
	            	Globals.gConditionDesc = lResult;
	            }
			}
			
			if (keyBtn.equals("update")){
			    lResult = BomAdapter.UpdateBOMDetail(mdlBOMDetail);	
			    
			    if(lResult.contains("Success Update BOM Detail")) {  
			    	Globals.gCondition = "SuccessUpdateComponent";
	            }  
	            else {
	            	Globals.gCondition = "FailedUpdateComponent"; 
	            	Globals.gConditionDesc = lResult;
	            }
			}
		}
		    
	    Globals.gtemp_Param1 = Globals.gParam1;
		Globals.gtemp_Param2 = plantId;
		Globals.gtemp_Param3 = validDate;
		//Globals.gtemp_Param4 = qty;
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
    }
    
}
