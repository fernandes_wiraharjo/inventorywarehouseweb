package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.ConfirmationAdapter;
import adapter.LogAdapter;
import adapter.ProductUomAdapter;
import adapter.ProductionOrderAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getComponentBatchSummary"} , name="getComponentBatchSummary")
public class GetComponentBatchSummaryServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetComponentBatchSummaryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
		String lComponentLine = request.getParameter("componentline");
		String lComponentID = request.getParameter("componentid");
		String lPlantID = request.getParameter("plantid");
		String lWarehouseID = request.getParameter("warehouseid");
		String lOrderTypeID = request.getParameter("ordertypeid");
		String lOrderNo = request.getParameter("orderno");
		Integer lTargetQtyBase = Integer.valueOf(request.getParameter("targetqtybase")); 
    	
    	List<model.mdlTempComponentBatchSummary> TempComponentBatchSummaryList = new ArrayList<model.mdlTempComponentBatchSummary>();
		TempComponentBatchSummaryList = ProductionOrderAdapter.LoadTempComponentBatchSummary(lComponentLine, lComponentID, lPlantID, lWarehouseID, lOrderTypeID, lOrderNo);
		//store temporary component batch summary data
		request.setAttribute("listComponent", TempComponentBatchSummaryList);
		
		request.setAttribute("ordertypeid", lOrderTypeID);
		request.setAttribute("orderno", lOrderNo);
		request.setAttribute("componentline", lComponentLine);
		request.setAttribute("targetqtybase", lTargetQtyBase);
		
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getTemporaryComponentBatchSummary.jsp");
		dispacther.forward(request, response);
	}
	
}
