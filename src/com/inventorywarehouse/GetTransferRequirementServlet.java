package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.InboundAdapter;
import adapter.WarehouseAdapter;

@WebServlet(urlPatterns={"/getTransferRequirement"} , name="getTransferRequirement")
public class GetTransferRequirementServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public GetTransferRequirementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lInboundDocNo = request.getParameter("inbounddoc");
    	
    	List<model.mdlInbound> InboundDetailList = new ArrayList<model.mdlInbound>();
		
    	InboundDetailList.addAll(InboundAdapter.LoadTransferRequirement(lInboundDocNo));
		request.setAttribute("listInboundDetail", InboundDetailList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getTransferRequirement.jsp");
		dispacther.forward(request, response);
		
	}
    
}
