package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BatchAdapter;
import adapter.CancelTransactionAdapter;

@WebServlet(urlPatterns={"/getcanceltransactiondetail"} , name="getcanceltransactiondetail")
public class GetCancelTransactionDetailServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetCancelTransactionDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lDocNo = request.getParameter("docNo");
    	
    	List<model.mdlCancelTransaction> mdlCancelDetailList = new ArrayList<model.mdlCancelTransaction>();
		mdlCancelDetailList.addAll(CancelTransactionAdapter.LoadCancelTransactionDetail(lDocNo));
		request.setAttribute("listCancelTransactionDetail", mdlCancelDetailList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getCancelTransactionDetail.jsp");
		dispacther.forward(request, response);
	}

}
