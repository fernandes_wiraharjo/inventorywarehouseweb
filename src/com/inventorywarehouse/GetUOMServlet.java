package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import adapter.BatchAdapter;
import adapter.UomAdapter;

@WebServlet(urlPatterns={"/getuom"} , name="getuom")
public class GetUOMServlet extends HttpServlet{

private static final long serialVersionUID = 1L; 
	
	public GetUOMServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lProductID = request.getParameter("productid");
    	
    	List<model.mdlUom> mdlUomList = new ArrayList<model.mdlUom>();
    	
    	mdlUomList.addAll(UomAdapter.LoadUOM(lProductID));
    	
    	Gson gson = new Gson();
    	
    	String jsonlistUOM = gson.toJson(mdlUomList);
    	
    	response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonlistUOM);
        
//		request.setAttribute("listuom", mdlUomList);
//		
//		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getUomFromProduct.jsp");
//		dispacther.forward(request, response);
		
	}

}
