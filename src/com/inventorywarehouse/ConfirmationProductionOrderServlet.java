package com.inventorywarehouse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.org.apache.bcel.internal.generic.DMUL;

import adapter.BatchAdapter;
import adapter.BomAdapter;
import adapter.ClosingPeriodAdapter;
import adapter.ConfirmationAdapter;
import adapter.InboundAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.OutboundDetailAdapter;
import adapter.PlantAdapter;
import adapter.ProductAdapter;
import adapter.ProductUomAdapter;
import adapter.ProductionOrderAdapter;
import adapter.StockSummaryAdapter;
import adapter.WarehouseAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlBOMDetail;

@WebServlet(urlPatterns={"/ConfirmationProductionOrder"} , name="ConfirmationProductionOrder")
public class ConfirmationProductionOrderServlet extends HttpServlet{

private static final long serialVersionUID = 1L;
    
    public ConfirmationProductionOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	Boolean CheckMenu;
    	String MenuURL = "ConfirmationProductionOrder";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlBOMConfirmationDocument> mdlConfirmationDocList = new ArrayList<model.mdlBOMConfirmationDocument>();
    		mdlConfirmationDocList.addAll(BomAdapter.LoadBOMConfirmationDocumentByType("Confirmation"));
    		request.setAttribute("listConfirmationDoc", mdlConfirmationDocList);
    		
    		List<model.mdlBOMProductionOrder> mdlProductionOrderList = new ArrayList<model.mdlBOMProductionOrder>();
    		mdlProductionOrderList.addAll(BomAdapter.LoadReleasedBOMProductionOrder());
    		request.setAttribute("listProductionOrder", mdlProductionOrderList);
    		
    		List<model.mdlBOMConfirmationProduction> mdlConfirmationProductionList = new ArrayList<model.mdlBOMConfirmationProduction>();
    		mdlConfirmationProductionList.addAll(BomAdapter.LoadBOMConfirmationProduction());
    		request.setAttribute("listConfirmationProduction", mdlConfirmationProductionList);
    		
    		List<model.mdlProduct> mdlComponentList = new ArrayList<model.mdlProduct>();
    		mdlComponentList.addAll(ProductAdapter.LoadProduct());
    		request.setAttribute("listComponent", mdlComponentList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther;
    		dispacther = request.getRequestDispatcher("/mainform/pages/confirmation_production_order.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		if (keyBtn.equals("saveComponent"))
		{
						//Declare TextBox
			//			String txtConfirmID = request.getParameter("txtConfirmationID");
			//			String txtConfirmNo = request.getParameter("txtConfirmationNo");
			//			String txtConfirmDate = request.getParameter("txtConfirmationDate");
			//				String newDate = ConvertDateTimeHelper.formatDate(txtConfirmDate, "dd MMM yyyy", "yyyy-MM-dd");
			//				String txtNewConfirmDate = newDate;
			String txtOrderTypeID = request.getParameter("txtOrderTypeID");
			String txtOrderNo = request.getParameter("txtOrderNo");
			String txtComponentLine = request.getParameter("txtComponentLine");
			String txtComponentID = request.getParameter("txtComponentID");
			//			String txtBatchNo = request.getParameter("txtBatchNo");
			String txtQtyComponent = request.getParameter("txtQtyComponent");
			String txtUOMComponent = request.getParameter("txtUOMComponent");
				//Get the Conversion of additional bom component
				model.mdlProductUom mdlComponentUom = new model.mdlProductUom();
				mdlComponentUom = ProductUomAdapter.LoadProductUOMByKey(txtComponentID, txtUOMComponent);
			if(mdlComponentUom.getBaseUOM() == null || mdlComponentUom.getBaseUOM().contentEquals("")){
				Globals.gCondition = "FailedConversion";
				Globals.gConditionDesc = "Satuan terkecil untuk komponen "+txtComponentID+" belum didefinisikan";
			}
			else{
				String txtComponentQtyBase = String.valueOf(Integer.parseInt(mdlComponentUom.getQty())*Integer.parseInt(txtQtyComponent));
				String txtComponentBaseUOM = mdlComponentUom.getBaseUOM();
				//			String txtPlantID = request.getParameter("txtPlantID");
				//			String txtWarehouseID = request.getParameter("txtWarehouseID");
							
							//Declare mdlBOMConfirmationProductionOrderDetail for global
				//			model.mdlBOMConfirmationProductionDetail mdlConfirmationDetail = new model.mdlBOMConfirmationProductionDetail();
								
				//			mdlConfirmationDetail.setConfirmationID(txtConfirmID);
				//			mdlConfirmationDetail.setConfirmationNo(txtConfirmNo);
				//			mdlConfirmationDetail.setComponentLine(txtComponentLine);
				//			mdlConfirmationDetail.setComponentID(txtComponentID);
				//			mdlConfirmationDetail.setBatchNo(txtBatchNo);
				//			mdlConfirmationDetail.setQty(txtQtyComponent);
				//			mdlConfirmationDetail.setUOM(txtUOMComponent);
				//			mdlConfirmationDetail.setQtyBaseUOM(txtComponentQtyBase);
				//			mdlConfirmationDetail.setBaseUOM(txtComponentBaseUOM);
					
				//Declare mdlBOMProductionOrderDetail for global	
			    List<model.mdlBOMProductionOrderDetail> listmdlAdditionalComponent = new ArrayList<model.mdlBOMProductionOrderDetail>();
				model.mdlBOMProductionOrderDetail mdlAdditionalComponent = new model.mdlBOMProductionOrderDetail();
				
				mdlAdditionalComponent.setOrderTypeID(txtOrderTypeID);
				mdlAdditionalComponent.setOrderNo(txtOrderNo);
				mdlAdditionalComponent.setComponentLine(txtComponentLine);
				mdlAdditionalComponent.setComponentID(txtComponentID);
				mdlAdditionalComponent.setQty(txtQtyComponent);
				mdlAdditionalComponent.setUOM(txtUOMComponent);
				mdlAdditionalComponent.setQtyBaseUOM(txtComponentQtyBase);
				mdlAdditionalComponent.setBaseUOM(txtComponentBaseUOM);
				listmdlAdditionalComponent.add(mdlAdditionalComponent);
				
				//Declare mdlStockSummary
				//			model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
				//			
				//			mdlStockSummary.setProductID(txtComponentID);
				//			mdlStockSummary.setPlantID(txtPlantID);
				//			mdlStockSummary.setWarehouseID(txtWarehouseID);
				//			mdlStockSummary.setBatch_No(txtBatchNo);	
				//			mdlStockSummary.setUOM(txtComponentBaseUOM);
				//			mdlStockSummary.setPeriod(txtNewConfirmDate.substring(0, 7).replace("-", ""));
								
				//			Globals.gCommand = "Insert Additional Component for Production Order : " + txtOrderTypeID + " - " + txtOrderNo + " , Component : " + txtComponentID;
						
				String lResult = "";
				//			try{
				//					model.mdlStockSummary mdlStockSummary2 =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,Globals.user);
				//					
				//					//to check if qty is +, it is considered as usage (decrease stock)
				//					if(Integer.parseInt(txtQtyComponent) >= 0)
				//					{
				//						if(Integer.parseInt(txtComponentQtyBase) > Integer.parseInt(mdlStockSummary2.getQty()))
				//						{
				//							LogAdapter.InsertLogExc("The product quantity that you input is more than the available quantity in the database", "InsertBOMAdditionalComponent", Globals.gCommand , Globals.user);
				//							lResult = "Kuantiti produk yang Anda masukkan lebih besar dari kuantiti produk yang tersedia";
				//						}
				//						else
				//						{
				//							mdlStockSummary.setQty(String.valueOf(Integer.parseInt(mdlStockSummary2.getQty()) -  Integer.parseInt(txtComponentQtyBase)));
				//							lResult = ConfirmationAdapter.TransactionInsertAdditionalComponent(mdlConfirmationDetail, mdlStockSummary);
				//						}
				//					}
				//					//if qty is -, it is considered as founded component (increase stock)
				//					else
				//					{
				//						mdlStockSummary.setQty(String.valueOf(Integer.parseInt(mdlStockSummary2.getQty()) -  Integer.parseInt(txtComponentQtyBase)));
				//						lResult = ConfirmationAdapter.TransactionInsertAdditionalComponent(mdlConfirmationDetail, mdlStockSummary);
				//					}
					
					//insert additional production order detail component to temporary table
					lResult = ProductionOrderAdapter.TransactionInsertTempProductionDetail(listmdlAdditionalComponent);
				//			}
				//			catch (Exception ex){
				//					LogAdapter.InsertLogExc(ex.toString(), "InsertBOMAdditionalComponent", Globals.gCommand , Globals.user);
				//					lResult = "Database Error";
				//			}
				
				
				if(lResult.contains("Success Insert Temp Production Detail")) {  
					Globals.gCondition = "SuccessInsertAdditionalComponent";
	            }  
	            else {
	            	Globals.gCondition = "FailedInsertAdditionalComponent";
	            	Globals.gConditionDesc = lResult;
	            }
			}
			
			response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
			
			return;
		}
			//		else if(keyBtn.equals("Complete Production")) 
			//		{
			//			//Declare TextBox
			//			String txtOrderTypeID = request.getParameter("ordertypeid");
			//			String txtOrderNo = request.getParameter("orderno");
			//			
			//			String lResult = "";
			//			
			//				lResult = ConfirmationAdapter.CompleteProduction(txtOrderTypeID, txtOrderNo);	
			//			    
			//			    if(lResult.contains("Success Complete Production Order")) {  
			//			    	Globals.gCondition = "SuccessComplete";
			//	            }  
			//	            else {
			//	            	Globals.gCondition = "FailedComplete";
			//	            	Globals.gConditionDesc = lResult;
			//	            } 
			//			
			//			return;
			//		}
		else if(keyBtn.equals("updateconfirmationdetail")){
			String txtOrderTypeID = request.getParameter("txtOrderTypeID");
			String txtOrderNo = request.getParameter("txtOrderNo");
			String txtComponentLine = request.getParameter("txtComponentLine");
			String txtComponentID = request.getParameter("txtComponentID");
			String txtQtyComponent = request.getParameter("txtQty");
			String txtUOMComponent = request.getParameter("slUOM");
				//Get the Conversion of bom component
				model.mdlProductUom mdlComponentUom = new model.mdlProductUom();
				mdlComponentUom = ProductUomAdapter.LoadProductUOMByKey(txtComponentID, txtUOMComponent);
			if(mdlComponentUom.getBaseUOM() == null || mdlComponentUom.getBaseUOM().contentEquals("")){
				Globals.gCondition = "FailedConversion";
				Globals.gConditionDesc = "Satuan terkecil untuk komponen "+txtComponentID+" belum didefinisikan";
			}
			else{
				String txtQtyComponentBase = String.valueOf(Integer.parseInt(mdlComponentUom.getQty())*Integer.parseInt(txtQtyComponent));
				String txtBaseUOMComponent = mdlComponentUom.getBaseUOM();
					
				//Declare mdlBOMProductionOrderDetail for global
				model.mdlBOMProductionOrderDetail ConfirmationDetail = new model.mdlBOMProductionOrderDetail();
				
				ConfirmationDetail.setOrderTypeID(txtOrderTypeID);
				ConfirmationDetail.setOrderNo(txtOrderNo);
				ConfirmationDetail.setComponentLine(txtComponentLine);
				ConfirmationDetail.setComponentID(txtComponentID);
				ConfirmationDetail.setQty(txtQtyComponent);
				ConfirmationDetail.setUOM(txtUOMComponent);
				ConfirmationDetail.setQtyBaseUOM(txtQtyComponentBase);
				ConfirmationDetail.setBaseUOM(txtBaseUOMComponent);
						
				String lResult = "";
					
				//update production order detail component to temporary table
				lResult = ProductionOrderAdapter.UpdateTempProductionOrderDetail(ConfirmationDetail);
				
				if(lResult.contains("Success Update Confirmation Detail")) {  
					Globals.gCondition = "SuccessUpdateConfirmationDetail";
	            }  
	            else {
	            	Globals.gCondition = "FailedUpdateConfirmationDetail";
	            	Globals.gConditionDesc = lResult;
	            }
			}
			
			response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
			
			return;
		}
		else
		{
			String lResult = "";
				
			//Declare TextBox
			String txtConfirmationID = request.getParameter("confirmationid");
			String txtConfirmationNo = request.getParameter("confirmationno");
			String txtConfirmationDate = request.getParameter("confirmationdate");
					String newConfirmationDate = ConvertDateTimeHelper.formatDate(txtConfirmationDate, "dd MMM yyyy", "yyyy-MM-dd");
					String txtNewConfirmationDate = newConfirmationDate;
			String txtOrderTypeID = request.getParameter("ordertypeid");
			String txtOrderNo = request.getParameter("orderno");
			String txtBOMProductID = request.getParameter("bomproductid");
			String txtPackingNo = request.getParameter("packingno");
			String txtExpiredDate = request.getParameter("expireddate");
					String newExpiredDate = ConvertDateTimeHelper.formatDate(txtExpiredDate, "dd MMM yyyy", "yyyy-MM-dd");
					txtExpiredDate = newExpiredDate;
			String txtBOMProductQtyTarget = request.getParameter("bomproductqtyTarget");
			String txtBOMProductQty = request.getParameter("bomproductqty");
			String txtBOMProductUOM = request.getParameter("bomproductuom");
			//Get the Conversion of bom product
					model.mdlProductUom mdlBOMProductUom = new model.mdlProductUom();
					mdlBOMProductUom = ProductUomAdapter.LoadProductUOMByKey(txtBOMProductID, txtBOMProductUOM);
			if(mdlBOMProductUom.getBaseUOM() == null || mdlBOMProductUom.getBaseUOM().contentEquals("")){
				Globals.gCondition = "FailedConversion";
				Globals.gConditionDesc = "Satuan terkecil untuk produk bom "+txtBOMProductID+" belum didefinisikan";
			}
			else{
						String txtBOMProductQtyBase = String.valueOf(Integer.parseInt(mdlBOMProductUom.getQty())*Integer.parseInt(txtBOMProductQty));
						String txtBOMProductBaseUOM = mdlBOMProductUom.getBaseUOM();
				String txtPeriod = txtNewConfirmationDate.substring(0, 7).replace("-", "");
				String txtPlantID = request.getParameter("plantid");
				String txtWarehouseID = request.getParameter("warehouseid");
				
				//Declare mdlBOMConfirmationProductionOrder for global
				model.mdlBOMConfirmationProduction mdlConfirmation = new model.mdlBOMConfirmationProduction();
				
				mdlConfirmation.setConfirmationID(txtConfirmationID);
				mdlConfirmation.setConfirmationNo(txtConfirmationNo);
				mdlConfirmation.setConfirmationDate(txtNewConfirmationDate);
				mdlConfirmation.setOrderTypeID(txtOrderTypeID);
				mdlConfirmation.setOrderNo(txtOrderNo);
				mdlConfirmation.setProductID(txtBOMProductID);
				mdlConfirmation.setPackingNo(txtPackingNo);
				mdlConfirmation.setExpiredDate(txtExpiredDate);
				
				mdlConfirmation.setTargetQty(txtBOMProductQtyTarget);
				mdlConfirmation.setQty(txtBOMProductQty);
				mdlConfirmation.setUOM(txtBOMProductUOM);
				mdlConfirmation.setQtyBaseUOM(txtBOMProductQtyBase);
				mdlConfirmation.setBaseUOM(txtBOMProductBaseUOM);
				
				//Declare mdlStockSummary for global
				model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
				
				mdlStockSummary.setPeriod(txtPeriod);
				mdlStockSummary.setPlantID(txtPlantID);
				mdlStockSummary.setWarehouseID(txtWarehouseID);
					
				if (keyBtn.equals("Confirm Production Order")){
					
					String lCommand = "Insert Confirmation Production Order : " + txtOrderTypeID + " - " + txtOrderNo + " , Confirmation : " + txtConfirmationID + " - " + txtConfirmationNo;
					
					model.mdlClosingPeriod CheckLastPeriod = ClosingPeriodAdapter.LoadActivePeriod(Globals.user);
					String sLastPeriod = CheckLastPeriod.getPeriod();
					
					if(sLastPeriod == null || sLastPeriod == "")
					{	
						LogAdapter.InsertLogExc("Period is not set", "InsertConfirmation", lCommand , Globals.user);
						lResult = "Periode belum ditentukan";
					}
					else
					{
						try{
							DateFormat dfPeriod = new SimpleDateFormat("yyyy-MM");
							Date LastPeriod_On = dfPeriod.parse(sLastPeriod.substring(0,4)+"-"+sLastPeriod.substring(4,6));
							Date ConfirmDate = dfPeriod.parse(mdlConfirmation.getConfirmationDate().substring(0, 7));
							
							if(LastPeriod_On.equals(ConfirmDate))
							{
								lResult = ConfirmationAdapter.TransactionConfirmationProduction(mdlConfirmation,mdlStockSummary);
							//							int ComponentStock = StockSummaryAdapter.LoadStockSummaryByProductForConfirmation(txtComponentID, txtPlantID, txtWarehouseID, txtComponentQtyBase, txtOrderTypeID, txtOrderNo);
							//							
							//							if( Integer.parseInt(txtComponentQtyBase) > ComponentStock )
							//							{
							//								LogAdapter.InsertLogExc("The component is not available", "InsertBOMConfirmation", Globals.gCommand , Globals.user);
							//								lResult = "Komponen yang dibutuhkan tidak memadai";
							//							}
							//							else
							//							{
							//								List<model.mdlStockSummary> ComponentStockOrderByExpiry = new ArrayList<model.mdlStockSummary>();
							//								ComponentStockOrderByExpiry = StockSummaryAdapter.LoadStockByComponentBatchExpiry(txtComponentID, txtPlantID, txtWarehouseID,txtComponentQtyBase, txtOrderTypeID, txtOrderNo);
							//								
							//								lResult = ConfirmationAdapter.TransactionConfirmationProduction(mdlConfirmation,mdlConfirmationDetail,ComponentStockOrderByExpiry,mdlProductStockSummary);
							//							}
							}
							else
							{
								LogAdapter.InsertLogExc("Your input date period is not activated", "InsertConfirmation", lCommand , Globals.user);
								lResult = "Tanggal konfirmasi yang Anda masukkan tidak diizinkan karena tidak sesuai dengan periode yang aktif";
							}
						}
						catch(Exception e){
							LogAdapter.InsertLogExc(e.toString(), "InsertConfirmation", lCommand , Globals.user);
							lResult = "Terjadi kesalahan pada sistem saat proses penguraian tanggal";
						}
					}
					
					if(lResult.contains("Success Insert Confirmation")) {  
						Globals.gCondition = "SuccessInsertConfirmation";
		            }  
		            else {
		            	Globals.gCondition = "FailedInsertConfirmation"; 
		            	Globals.gConditionDesc = lResult;
		            }
				}
			}
			
			response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
			
			return;
		}
		
    }
    
}
