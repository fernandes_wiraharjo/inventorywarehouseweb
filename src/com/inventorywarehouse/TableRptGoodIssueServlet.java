package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.InboundAdapter;
import adapter.OutboundAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

/**
 * 
 */
@WebServlet(urlPatterns={"/TableRptGoodIssueServlet"} , name="TableRptGoodIssueServlet")
public class TableRptGoodIssueServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TableRptGoodIssueServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("condition", Globals.gCondition);	
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/Child_Rpt/Table_RptGoodIssue.jsp");
		dispacther.forward(request, response);
		Globals.gCondition = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lStartDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtStartDate"),"dd MMM yyyy", "yyyy-MM-dd");
		String lEndDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtEndDate"),"dd MMM yyyy", "yyyy-MM-dd");
		String[] lcbCancInboundlist = request.getParameterValues("cbCancInbound");

		
		String lParambtn = request.getParameter("key");
		//String btnShow = request.getParameter("btnShow");
		if (!lParambtn.isEmpty()){		
			List<model.mdlRptGoodIssue> mdlRptGoodIssueList = new ArrayList<model.mdlRptGoodIssue>();			
			//buat adapter sesuai parameter untuk menampilkan table GoodRecipt
			mdlRptGoodIssueList = OutboundAdapter.LoadReportGoodIssue(lStartDate, lEndDate);
			request.setAttribute("lStartDate", ConvertDateTimeHelper.formatDate(lStartDate, "yyyy-MM-dd", "dd MMM yyyy"));
			request.setAttribute("lEndDate", ConvertDateTimeHelper.formatDate(lEndDate, "yyyy-MM-dd", "dd MMM yyyy"));
			request.setAttribute("listgoodissue", mdlRptGoodIssueList);
		}
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/Child_Rpt/Table_RptGoodIssue.jsp");
		dispacther.forward(request, response);
	}

}
