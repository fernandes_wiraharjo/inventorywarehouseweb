package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import adapter.BomAdapter;
import adapter.LogAdapter;
import adapter.UomAdapter;
import model.Globals;
import model.mdlBomOrderType;

@WebServlet(urlPatterns={"/getOrderNo"} , name="getOrderNo")
public class GetOrderNoServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetOrderNoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lOrderTypeID = request.getParameter("ordertypeid");
    	
    	String ResultOrderNo = "";
    	
    	ResultOrderNo = BomAdapter.GetProductionOrderNo(lOrderTypeID);
    	
    	if(ResultOrderNo.isEmpty()){
    		ResultOrderNo = "1";
   		}
   		else
   		{
   			ResultOrderNo = String.valueOf(Integer.parseInt(ResultOrderNo)+1);
   		}
    	
    	model.mdlBomOrderType BOMOrderType = new model.mdlBomOrderType();
		BOMOrderType = BomAdapter.GetBOMOrdeTypeById(lOrderTypeID);
		
    	String limitMaxRange = BOMOrderType.To;
    	
    	if( Integer.parseInt(ResultOrderNo) > Integer.parseInt(limitMaxRange) )
    		ResultOrderNo = "0";
    	
    	response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(ResultOrderNo);
		
	}
	
}
