package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.ProductAdapter;
import adapter.ProductWMAdapter;
import adapter.StorageBinAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/productWM"} , name="productWM")
public class ProductWarehouseManagementServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public ProductWarehouseManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "productWM";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);
    		
    		List<model.mdlProduct> mdlProductList = new ArrayList<model.mdlProduct>();
    		mdlProductList.addAll(ProductAdapter.LoadProduct());
    		request.setAttribute("listProduct", mdlProductList);
    		
    		List<model.mdlProductWM> listProductWM = new ArrayList<model.mdlProductWM>();
    		listProductWM.addAll(ProductWMAdapter.LoadProductWM());
    		request.setAttribute("listProductWM", listProductWM);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/product_warehouse_management.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare Deleting Proccess Params
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtProductID = request.getParameter("temp_txtProductID");
		if (btnDelete != null){
			String lResult = ProductWMAdapter.DeleteProductWM(temp_txtPlantID, temp_txtWarehouseID, temp_txtProductID);
			if(lResult.contains("Success Delete Product")) {
				Globals.gCondition =  "SuccessDeleteProductWM";
            }
            else {
            	Globals.gCondition = "FailedDeleteProductWM";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
			
			return;
		}

		//Declare TextBox for insert and update
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtProductID = request.getParameter("txtProductID");
		String txtBaseUOM = request.getParameter("txtBaseUOM");
		Double txtCapacityUsage = Double.valueOf(request.getParameter("txtCapacityUsage"));
		String slMasterUOM = request.getParameter("slMasterUOM");
		Boolean cbApprBatchRecReq = Boolean.valueOf(request.getParameter("cbApprBatchRecReq"));
		String txtStockRemoval = request.getParameter("txtStockRemoval");
		String txtStockPlacement = request.getParameter("txtStockPlacement");
		String txtStorageSectionIndicator = request.getParameter("txtStorageSectionIndicator");
		String txtStorageBin = request.getParameter("txtStorageBin");
		Double txtLEQty1 = Double.valueOf(request.getParameter("txtLEQty1"));
		Double txtLEQty2 = Double.valueOf(request.getParameter("txtLEQty2"));
		Double txtLEQty3 = Double.valueOf(request.getParameter("txtLEQty3"));
		String slMasterUOMLEQty1 = request.getParameter("slMasterUOMLEQty1");
		String slMasterUOMLEQty2 = request.getParameter("slMasterUOMLEQty2");
		String slMasterUOMLEQty3 = request.getParameter("slMasterUOMLEQty3");
		String txtSUTLEQty1 = request.getParameter("txtSUTLEQty1");
		String txtSUTLEQty2 = request.getParameter("txtSUTLEQty2");
		String txtSUTLEQty3 = request.getParameter("txtSUTLEQty3");

		//Declare mdlProductWM for global
		model.mdlProductWM mdlProductWM = new model.mdlProductWM();
		mdlProductWM.setPlantID(txtPlantID);
		mdlProductWM.setWarehouseID(txtWarehouseID);
		mdlProductWM.setProductID(txtProductID);
		mdlProductWM.setBaseUOM(txtBaseUOM);
		mdlProductWM.setCapacityUsage(txtCapacityUsage);
		mdlProductWM.setCapacityUsageUOM(slMasterUOM);
		mdlProductWM.setApprBatchRecReq(cbApprBatchRecReq);
		mdlProductWM.setStockRemoval(txtStockRemoval);
		mdlProductWM.setStockPlacement(txtStockPlacement);
		mdlProductWM.setStorageSectionIndicator(txtStorageSectionIndicator);
		mdlProductWM.setStorageBin(txtStorageBin);
		mdlProductWM.setLEQty1(txtLEQty1);
		mdlProductWM.setLEQty2(txtLEQty2);
		mdlProductWM.setLEQty3(txtLEQty3);
		mdlProductWM.setUOMLEQty1(slMasterUOMLEQty1);
		mdlProductWM.setUOMLEQty2(slMasterUOMLEQty2);
		mdlProductWM.setUOMLEQty3(slMasterUOMLEQty3);
		mdlProductWM.setSUTLEQty1(txtSUTLEQty1);
		mdlProductWM.setSUTLEQty2(txtSUTLEQty2);
		mdlProductWM.setSUTLEQty3(txtSUTLEQty3);
		
		if (keyBtn.equals("save")){
			String lResult = ProductWMAdapter.InsertProductWM(mdlProductWM);
			if(lResult.contains("Success Insert Product")) {
				Globals.gCondition = "SuccessInsertProductWM";
            }
            else {
            	Globals.gCondition = "FailedInsertProductWM";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = ProductWMAdapter.UpdateProductWM(mdlProductWM);

		    if(lResult.contains("Success Update Product")) {
		    	Globals.gCondition = "SuccessUpdateProductWM";
            }
            else {
            	Globals.gCondition = "FailedUpdateProductWM";
            	Globals.gConditionDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
    }
    
}
