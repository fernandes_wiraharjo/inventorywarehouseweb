package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.DoorAdapter;
import adapter.MaterialStagingAreaAdapter;
import adapter.WarehouseAdapter;

@WebServlet(urlPatterns={"/getstagingarea"} , name="getstagingarea")
public class GetDynamicStagingAreaServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicStagingAreaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lDoorID = request.getParameter("doorid");
    	
    	List<model.mdlMaterialStagingArea> listStagingArea = new ArrayList<model.mdlMaterialStagingArea>();
		
    	listStagingArea.addAll(MaterialStagingAreaAdapter.LoadStagingAreaForInbound(lPlantID, lWarehouseID, lDoorID));
		request.setAttribute("listStagingArea", listStagingArea);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicStagingArea.jsp");
		dispacther.forward(request, response);
		
	}

}
