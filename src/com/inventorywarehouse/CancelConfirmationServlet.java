package com.inventorywarehouse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BomAdapter;
import adapter.CancelConfirmationAdapter;
import adapter.CancelTransactionAdapter;
import adapter.ClosingPeriodAdapter;
import adapter.ConfirmationAdapter;
import adapter.InboundAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.TraceCodeAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

@WebServlet(urlPatterns={"/CancelConfirmation"} , name="CancelConfirmation")
public class CancelConfirmationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
    
    public CancelConfirmationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   	 
    	Boolean CheckMenu;
    	String MenuURL = "CancelConfirmation";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlBOMConfirmationDocument> mdlConfirmationDocList = new ArrayList<model.mdlBOMConfirmationDocument>();
    		mdlConfirmationDocList.addAll(BomAdapter.LoadBOMConfirmationDocumentByType("Cancel"));
    		request.setAttribute("listConfirmationDoc", mdlConfirmationDocList);
    		
    		List<model.mdlBOMConfirmationProduction> mdlConfirmationList = new ArrayList<model.mdlBOMConfirmationProduction>();
    		mdlConfirmationList.addAll(ConfirmationAdapter.LoadNonCancelConfirmation());
    		request.setAttribute("listConfirmation", mdlConfirmationList);
    		
    		List<model.mdlBOMCancelConfirmation> mdlCancelConfirmationList = new ArrayList<model.mdlBOMCancelConfirmation>();
    		mdlCancelConfirmationList.addAll(ConfirmationAdapter.LoadCancelConfirmation());
    		request.setAttribute("listCancelConfirmation", mdlCancelConfirmationList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/cancel_confirmation.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		String lResult = "";
		
		//Declare TextBox
		String txtCancelConfirmationID = request.getParameter("txtCancelConfirmationID");
		String txtCancelConfirmationNo = request.getParameter("txtCancelConfirmationNo");
		
		String newDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtDate"), "dd MMM yyyy", "yyyy-MM-dd");
		String txtDate = newDate;
		
		String temp_txtConfirmationID = request.getParameter("temp_txtConfirmationID");
		String temp_txtConfirmationNo = request.getParameter("temp_txtConfirmationNo");
		String temp_txtProductID = request.getParameter("temp_txtProductID");
		String temp_txtBatchNo = request.getParameter("temp_txtBatchNo");
		String temp_txtQty = request.getParameter("temp_txtQty");
		String temp_txtUOM = request.getParameter("temp_txtUOM");
		String temp_txtQtyBase = request.getParameter("temp_txtQtyBase");
		String temp_txtBaseUOM = request.getParameter("temp_txtBaseUOM");
		String temp_txtPlantID = "";
		String temp_txtWarehouseID = "";
		
		model.mdlBOMConfirmationProduction ConfirmationOrder = new model.mdlBOMConfirmationProduction();
		ConfirmationOrder = ConfirmationAdapter.LoadProductionOrderByConfirmationDoc(temp_txtConfirmationID, temp_txtConfirmationNo);
		if(ConfirmationOrder.getOrderTypeID()==null){
			Globals.gCondition = "FailedInsertCancelConfirmation";
			Globals.gConditionDesc = "Terjadi kesalahan pada sistem saat menarik data konfirmasi produksi";
		}
		else{
			List<model.mdlBOMProductionOrder> ProductionOrderlist = new ArrayList<model.mdlBOMProductionOrder>();
			ProductionOrderlist = BomAdapter.LoadPlantWarehouseProductionOrderByKey(ConfirmationOrder.getOrderTypeID(), ConfirmationOrder.getOrderNo());
			if(ProductionOrderlist.isEmpty() || ProductionOrderlist == null || ProductionOrderlist.size() == 0)
			{
				LogAdapter.InsertLogExc("Plant and warehouse not found", "InsertCancelConfirmation", Globals.gCommand , Globals.user);
				Globals.gCondition = "FailedInsertCancelConfirmation";
				Globals.gConditionDesc = "Terjadi kesalahan pada sistem saat menarik data plant dan gudang dari produksi yang ingin dibatalkan";
			}
			else{
				for(model.mdlBOMProductionOrder ProductionOrder : ProductionOrderlist)
				{
					temp_txtPlantID = ProductionOrder.getPlantID();
					temp_txtWarehouseID = ProductionOrder.getWarehouseID();
				}
				
				//Declare mdlCancelTransaction for global
				model.mdlBOMCancelConfirmation mdlCancelConfirmation = new model.mdlBOMCancelConfirmation();
				mdlCancelConfirmation.setCancelConfirmationID(txtCancelConfirmationID);
				mdlCancelConfirmation.setCancelConfirmationNo(txtCancelConfirmationNo);
				mdlCancelConfirmation.setDate(txtDate);
				mdlCancelConfirmation.setConfirmationID(temp_txtConfirmationID);
				mdlCancelConfirmation.setConfirmationNo(temp_txtConfirmationNo);
				mdlCancelConfirmation.setProductID(temp_txtProductID);
				mdlCancelConfirmation.setBatchNo(temp_txtBatchNo);
				mdlCancelConfirmation.setQty(temp_txtQty);
				mdlCancelConfirmation.setUOM(temp_txtUOM);
				mdlCancelConfirmation.setQtyBaseUOM(temp_txtQtyBase);
				mdlCancelConfirmation.setBaseUOM(temp_txtBaseUOM);
				mdlCancelConfirmation.setPlantID(temp_txtPlantID);
				mdlCancelConfirmation.setWarehouseID(temp_txtWarehouseID);
				mdlCancelConfirmation.setOrderTypeID(ConfirmationOrder.getOrderTypeID());
				mdlCancelConfirmation.setOrderNo(ConfirmationOrder.getOrderNo());
				
				String lCommand = "";
				if (keyBtn.equals("save")){
					
					lCommand = "Insert Cancel Confirmation Id : " + txtCancelConfirmationID + " , No : " + txtCancelConfirmationNo;
					
					model.mdlClosingPeriod CheckLastPeriod = ClosingPeriodAdapter.LoadActivePeriod(Globals.user);
					String sLastPeriod = CheckLastPeriod.getPeriod();
					
					if(sLastPeriod == null || sLastPeriod == "")
					{	
						LogAdapter.InsertLogExc("Period is not set", "InsertCancelConfirmation", lCommand , Globals.user);
						lResult = "Periode belum ditentukan";
					}
					else
					{
						DateFormat dfPeriod = new SimpleDateFormat("yyyy-MM");
						try{
							Date LastPeriod_On = dfPeriod.parse(sLastPeriod.substring(0,4)+"-"+sLastPeriod.substring(4,6));
							Date CancelConfirmationDate = dfPeriod.parse(txtDate.substring(0, 7));
							
							if(LastPeriod_On.equals(CancelConfirmationDate))
							{
								List<model.mdlBOMConfirmationProductionDetail> mdlLoadConfirmationDetail = ConfirmationAdapter.LoadBOMConfirmationDetail(temp_txtConfirmationID, temp_txtConfirmationNo);
								if(mdlLoadConfirmationDetail.isEmpty() || mdlLoadConfirmationDetail == null || mdlLoadConfirmationDetail.size() == 0)
									lResult = "Terjadi kesalahan saat menarik data komponen konfirmasi produksi saat proses pembatalan";
								else
									lResult = CancelConfirmationAdapter.TransactionCancelConfirmation(mdlCancelConfirmation,mdlLoadConfirmationDetail);
							}
							else
							{
								LogAdapter.InsertLogExc("Your input date period is not activated", "InsertCancelConfirmation", lCommand , Globals.user);
								lResult = "Tanggal yang Anda masukkan tidak diizinkan karena tidak sesuai dengan periode yang aktif";
							}
						}
						catch(Exception e){
							LogAdapter.InsertLogExc(e.toString(), "InsertCancelConfirmation", lCommand , Globals.user);
							lResult = "Terjadi kesalahan pada sistem saat proses penguraian tanggal";
						}
					}
					
					if(lResult.contains("Success Insert Cancel Confirmation Production")) {  
						Globals.gCondition = "SuccessInsertCancelConfirmation";
		            }  
		            else {
		            	Globals.gCondition = "FailedInsertCancelConfirmation"; 
		            	Globals.gConditionDesc = lResult;
		            }
				}
			}
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
	}
    
}
