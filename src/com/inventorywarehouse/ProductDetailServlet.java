package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.GroupAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.ProductDetailAdapter;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/ProductDetail"} , name="ProductDetail")
public class ProductDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ProductDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Boolean CheckMenu;
    	String MenuURL = "ProductDetail";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		//Declare keybutton
    		String keyBtn = request.getParameter("key");
    		if (keyBtn == null){
    			keyBtn = new String("");
    		}
    				
    		List<model.mdlProductDetail> mdlProductDetailList = new ArrayList<model.mdlProductDetail>();
    		
    		if(keyBtn.contentEquals("sync")){
    			mdlProductDetailList =  ProductDetailAdapter.GetProductDetailAPI(Globals.user);
    		}
    		else{
    			mdlProductDetailList =  ProductDetailAdapter.LoadProductDetail();
    		}
    		
    		request.setAttribute("listproductdetail", mdlProductDetailList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/product_detail.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
