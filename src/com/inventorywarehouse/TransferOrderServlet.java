package com.inventorywarehouse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BatchAdapter;
import adapter.InboundAdapter;
import adapter.ProductAdapter;
import adapter.TransferOrderAdapter;
import model.Globals;
import model.mdlReferenceMovementType;

@WebServlet(urlPatterns={"/TransferOrder"} , name="TransferOrder")
public class TransferOrderServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
    
    public TransferOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    String inboundDocNo,key,tempinboundDocNo,tempkey;
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	inboundDocNo = request.getParameter("docNo");
    	key = request.getParameter("key");
    	if(inboundDocNo == null)
    		inboundDocNo = tempinboundDocNo;
    	if(key == null)
    		key = tempkey;
    	
		List<model.mdlTransferOrder> listTO = new ArrayList<model.mdlTransferOrder>();
		
		if(key.contentEquals("create")){
			listTO.addAll(TransferOrderAdapter.LoadTransferOrderByInbound(inboundDocNo));
			TransferOrderAdapter.UpdateTransferOrder(listTO.get(0),inboundDocNo);
			listTO = new ArrayList<model.mdlTransferOrder>();
			Globals.gUsedStorBin = new ArrayList<model.mdlStorageBin>();
			Globals.gBackupUsedStorBin = new ArrayList<model.mdlStorageBin>();
		}
		
		listTO.addAll(TransferOrderAdapter.LoadTransferOrderByInbound(inboundDocNo));
		
		request.setAttribute("lPlantID", listTO.get(0).getPlantID());
		request.setAttribute("lWarehouseID", listTO.get(0).getWarehouseID());
		request.setAttribute("lTransferOrderID", listTO.get(0).getTransferOrderID());
		request.setAttribute("lCreationDate", listTO.get(0).getCreationDate());
		request.setAttribute("lSrcStorageType", listTO.get(0).getSrcStorageType());
		request.setAttribute("lSrcStorageBin", listTO.get(0).getSrcStorageBin());
		request.setAttribute("listTO", listTO);
		
		request.setAttribute("condition", Globals.gCondition);
		request.setAttribute("conditionDescription", Globals.gConditionDesc);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/transfer_order.jsp");
		dispacther.forward(request, response);
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	tempinboundDocNo = inboundDocNo;
    	tempkey = "load";
    	
    	//Declare TextBox
    	String lPlantID = request.getParameter("temp_txtPlantID");
    	String lWarehouseID = request.getParameter("temp_txtWarehouseID");
    	String lTransferOrderID = request.getParameter("temp_txtTransferOrderID");
    	String lProductID = request.getParameter("temp_txtProductID");
    	String lBatchNo = request.getParameter("temp_txtBatchNo");
    	String lStorageTypeID = request.getParameter("temp_txtStorageTypeID");
    	String lStorageSectionID = request.getParameter("temp_txtStorageSectionID");
    	String lStorageBinID = request.getParameter("temp_txtStorageBinID");
    	String lDestStorageUnit = request.getParameter("temp_txtDestStorageUnit");
    	String lDestQty = request.getParameter("temp_txtDestQty");
    	String lDestQtyUOM = request.getParameter("temp_txtDestQtyUOM");
    	String lCapacityUsed = request.getParameter("temp_txtCapacityUsed");
    	String lStorageUnitType = request.getParameter("temp_txtStorageUnitType");
    	Boolean lWMStatus = Boolean.valueOf(request.getParameter("temp_txtWMStatus"));
    	Boolean lUpdateWMFirst = Boolean.valueOf(request.getParameter("temp_txtUpdateWMFirst"));
    	
    	//Declare mdlTransferOrder for global
    	model.mdlTransferOrder mdlTransferOrder = new model.mdlTransferOrder();
    	mdlTransferOrder.setPlantID(lPlantID);
    	mdlTransferOrder.setWarehouseID(lWarehouseID);
    	mdlTransferOrder.setTransferOrderID(lTransferOrderID);
    	mdlTransferOrder.setProductID(lProductID);
    	mdlTransferOrder.setBatchNo(lBatchNo);
    	mdlTransferOrder.setStorageTypeID(lStorageTypeID);
    	mdlTransferOrder.setStorageSectionID(lStorageSectionID);
    	mdlTransferOrder.setDestinationStorageBin(lStorageBinID);
    	mdlTransferOrder.setDestinationStorageUnit(Integer.parseInt(lDestStorageUnit));
    	mdlTransferOrder.setDestinationQty(Integer.parseInt(lDestQty));
    	mdlTransferOrder.setUOM(lDestQtyUOM);
    	mdlTransferOrder.setCapacityUsed(Double.parseDouble(lCapacityUsed));
    	mdlTransferOrder.setInboundDocID(tempinboundDocNo);
    	mdlTransferOrder.setStorageUnitType(lStorageUnitType);
    	mdlTransferOrder.setWMS_Status(lWMStatus);
    	mdlTransferOrder.setUpdateWMFirst(lUpdateWMFirst);
    	
    	//Confirm Transfer Order
	    String lResult = TransferOrderAdapter.ConfirmTO(mdlTransferOrder);	
	    
	    if(lResult.contains("Success Confirm TO Line")) {  
	    	Globals.gCondition = "SuccessConfirmTOLine";
	    }  
	    else {
	    	Globals.gCondition = "FailedConfirmTOLine"; 
	    	Globals.gConditionDesc = lResult;
	    }
    	
    	doGet(request, response);
    	return;
    }

}
