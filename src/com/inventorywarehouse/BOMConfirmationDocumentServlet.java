package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BomAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/BomConfirmationDocument"} , name="BomConfirmationDocument")
public class BOMConfirmationDocumentServlet extends HttpServlet{

private static final long serialVersionUID = 1L; 
    
    public BOMConfirmationDocumentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	Boolean CheckMenu;
    	String MenuURL = "BomConfirmationDocument";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlBOMConfirmationDocument> listBomConfirmationDocument = new ArrayList<model.mdlBOMConfirmationDocument>();
    		listBomConfirmationDocument.addAll(adapter.BomAdapter.LoadBOMConfirmationDocument());
    		request.setAttribute("listBomConfirmationDocument", listBomConfirmationDocument);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/bom_confirmation_document.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String btnDelete = request.getParameter("btnDelete");
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}
		
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtConfirmationID = request.getParameter("txtConfirmationID");
		String txtNumberFrom = request.getParameter("txtNumberFrom");
		String txtNumberTo = request.getParameter("txtNumberTo");
		String txtConfirmationDesc = request.getParameter("txtConfirmationDesc");
		String txtConfirmationType = request.getParameter("txtConfirmationType");
		String temp_txtConfirmationID = request.getParameter("temp_txtConfirmationID");
		
		//Declare mdlBomConfirmationDocument for global
		model.mdlBOMConfirmationDocument mdlBOMConfirmationDocument = new model.mdlBOMConfirmationDocument();
		mdlBOMConfirmationDocument.setConfirmationID(txtConfirmationID);
		mdlBOMConfirmationDocument.setRangeFrom(txtNumberFrom);
		mdlBOMConfirmationDocument.setRangeTo(txtNumberTo);
		mdlBOMConfirmationDocument.setConfirmationDesc(txtConfirmationDesc);
		mdlBOMConfirmationDocument.setConfirmationType(txtConfirmationType);
	
		
		if (keyBtn.equals("save")){
			String lResult = BomAdapter.InsertBomConfirmationDoc(mdlBOMConfirmationDocument);	
			if(lResult.contains("Success Insert Confirmation Document")) {  
				Globals.gCondition = "SuccessInsertConfimationDoc";
            }  
            else {
            	Globals.gCondition = "FailedInsertConfimationDoc";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (keyBtn.equals("update")){
		    String lResult = BomAdapter.UpdateBomConfirmationDoc(mdlBOMConfirmationDocument);	
		    
		    if(lResult.contains("Success Update Confirmation Document")) {  
		    	Globals.gCondition = "SuccessUpdateConfimationDoc";
            }  
            else {
            	Globals.gCondition = "FailedUpdateConfimationDoc";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (btnDelete != null){
			String lResult = BomAdapter.DeleteBomConfirmationDoc(temp_txtConfirmationID);
			if(lResult.contains("Success Delete Confirmation Document")) {  
				Globals.gCondition =  "SuccessDeleteConfimationDoc";
            }  
            else {
            	Globals.gCondition = "FailedDeleteConfimationDoc";
            	Globals.gConditionDesc = lResult;
            }
			
			doGet(request, response);
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
        return;
	}
    
}
