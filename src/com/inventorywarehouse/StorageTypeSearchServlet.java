package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.ProductAdapter;
import adapter.ProductWMAdapter;
import adapter.StorageTypeSearchAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/stortypesearch"} , name="stortypesearch")
public class StorageTypeSearchServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public StorageTypeSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "stortypesearch";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);
    		
    		List<model.mdlStorageTypeSearch> listStorageTypeSearch = new ArrayList<model.mdlStorageTypeSearch>();
    		listStorageTypeSearch.addAll(StorageTypeSearchAdapter.LoadStorageTypeSearch());
    		request.setAttribute("listStorageTypeSearch", listStorageTypeSearch);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/storagetype_search.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare Deleting Proccess Params
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtOperation = request.getParameter("temp_txtOperation");
		String temp_txtStorageTypeIndicatorID = request.getParameter("temp_txtStorageTypeIndicatorID");
		if (btnDelete != null){
			String lResult = StorageTypeSearchAdapter.DeleteStorageTypeSearch(temp_txtPlantID, temp_txtWarehouseID, temp_txtOperation, temp_txtStorageTypeIndicatorID);
			if(lResult.contains("Success Delete Storage Type Search")) {
				Globals.gCondition =  "SuccessDeleteStorageTypeSearch";
            }
            else {
            	Globals.gCondition = "FailedDeleteStorageTypeSearch";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
			
			return;
		}

		//Declare TextBox for insert and update
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtOperation = request.getParameter("txtOperation");
		String txtStorageTypeIndicatorID = request.getParameter("txtStorageTypeIndicatorID");
		String txtSeq1 = request.getParameter("txtSeq1");
		String txtSeq2 = request.getParameter("txtSeq2");
		String txtSeq3 = request.getParameter("txtSeq3");
		String txtSeq4 = request.getParameter("txtSeq4");
		String txtSeq5 = request.getParameter("txtSeq5");
		String txtSeq6 = request.getParameter("txtSeq6");
		String txtSeq7 = request.getParameter("txtSeq7");
		String txtSeq8 = request.getParameter("txtSeq8");
		String txtSeq9 = request.getParameter("txtSeq9");
		String txtSeq10 = request.getParameter("txtSeq10");
		String txtSeq11 = request.getParameter("txtSeq11");
		String txtSeq12 = request.getParameter("txtSeq12");
		String txtSeq13 = request.getParameter("txtSeq13");
		String txtSeq14 = request.getParameter("txtSeq14");
		String txtSeq15 = request.getParameter("txtSeq15");
		String txtSeq16 = request.getParameter("txtSeq16");
		String txtSeq17 = request.getParameter("txtSeq17");
		String txtSeq18 = request.getParameter("txtSeq18");
		String txtSeq19 = request.getParameter("txtSeq19");
		String txtSeq20 = request.getParameter("txtSeq20");
		String txtSeq21 = request.getParameter("txtSeq21");
		String txtSeq22 = request.getParameter("txtSeq22");
		String txtSeq23 = request.getParameter("txtSeq23");
		String txtSeq24 = request.getParameter("txtSeq24");
		String txtSeq25 = request.getParameter("txtSeq25");

		//Declare mdlStorageTypeSearch for global
		model.mdlStorageTypeSearch mdlStorageTypeSearch = new model.mdlStorageTypeSearch();
		mdlStorageTypeSearch.setPlantID(txtPlantID);
		mdlStorageTypeSearch.setWarehouseID(txtWarehouseID);
		mdlStorageTypeSearch.setOperation(txtOperation);
		mdlStorageTypeSearch.setStorageTypeIndicatorID(txtStorageTypeIndicatorID);
		mdlStorageTypeSearch.setStorageType1(txtSeq1);
		mdlStorageTypeSearch.setStorageType2(txtSeq2);
		mdlStorageTypeSearch.setStorageType3(txtSeq3);
		mdlStorageTypeSearch.setStorageType4(txtSeq4);
		mdlStorageTypeSearch.setStorageType5(txtSeq5);
		mdlStorageTypeSearch.setStorageType6(txtSeq6);
		mdlStorageTypeSearch.setStorageType7(txtSeq7);
		mdlStorageTypeSearch.setStorageType8(txtSeq8);
		mdlStorageTypeSearch.setStorageType9(txtSeq9);
		mdlStorageTypeSearch.setStorageType10(txtSeq10);
		mdlStorageTypeSearch.setStorageType11(txtSeq11);
		mdlStorageTypeSearch.setStorageType12(txtSeq12);
		mdlStorageTypeSearch.setStorageType13(txtSeq13);
		mdlStorageTypeSearch.setStorageType14(txtSeq14);
		mdlStorageTypeSearch.setStorageType15(txtSeq15);
		mdlStorageTypeSearch.setStorageType16(txtSeq16);
		mdlStorageTypeSearch.setStorageType17(txtSeq17);
		mdlStorageTypeSearch.setStorageType18(txtSeq18);
		mdlStorageTypeSearch.setStorageType19(txtSeq19);
		mdlStorageTypeSearch.setStorageType20(txtSeq20);
		mdlStorageTypeSearch.setStorageType21(txtSeq21);
		mdlStorageTypeSearch.setStorageType22(txtSeq22);
		mdlStorageTypeSearch.setStorageType23(txtSeq23);
		mdlStorageTypeSearch.setStorageType24(txtSeq24);
		mdlStorageTypeSearch.setStorageType25(txtSeq25);
		
		if (keyBtn.equals("save")){
			String lResult = StorageTypeSearchAdapter.InsertStorageTypeSearch(mdlStorageTypeSearch);
			if(lResult.contains("Success Insert Storage Type Search")) {
				Globals.gCondition = "SuccessInsertStorageTypeSearch";
            }
            else {
            	Globals.gCondition = "FailedInsertStorageTypeSearch";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = StorageTypeSearchAdapter.UpdateStorageTypeSearch(mdlStorageTypeSearch);

		    if(lResult.contains("Success Update Storage Type Search")) {
		    	Globals.gCondition = "SuccessUpdateStorageTypeSearch";
            }
            else {
            	Globals.gCondition = "FailedUpdateStorageTypeSearch";
            	Globals.gConditionDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
    }
    
}
