package com.inventorywarehouse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.OutboundAdapter;
import adapter.OutboundDetailAdapter;
import adapter.ProductAdapter;
import adapter.ProductUomAdapter;
import adapter.StockSummaryAdapter;
import adapter.TransactionAdapter;
import helper.ConvertDateTimeHelper;
import adapter.BatchAdapter;
import adapter.ClosingPeriodAdapter;
import adapter.LogAdapter;
import model.Globals;

/** Documentation
 * 001 nanda
 */
@WebServlet(urlPatterns={"/OutboundDetail"} , name="OutboundDetail")
public class OutboundDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//<<001 Nanda
	private static String lPlantID = ""; 
	private static String lWarehouseID = "";
	private static String docDate;
	//>>

	private static String docline = ""; //002 Nanda
       
    public OutboundDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Globals.gParam1 = request.getParameter("docNo");
		lPlantID = request.getParameter("PlantID");
		lWarehouseID = request.getParameter("WarehouseID");
		
		try
    	{
	    	String newDate=ConvertDateTimeHelper.formatDate(request.getParameter("docDate"), "dd MMM yyyy", "yyyy-MM-dd");
		    docDate = newDate;
    	}
    	catch(Exception ex)
    	{
    	}
		
		
		if (Globals.gParam1 == null)
    	{
			Globals.gParam1 = Globals.gtemp_Param1; //store temporary string as docNo
			lPlantID = Globals.gtemp_Param2; //store temporary string as vendor ID
			lWarehouseID = Globals.gtemp_Param3; //store temporary string as plant ID
			docDate = Globals.gtemp_Param4; //store temporary string as doc date
    	}
		
		 //<<002 Nanda
		docline = OutboundDetailAdapter.GenerateDocLineOutboundDet(Globals.gParam1);
		if(docline.isEmpty()){
		    docline = "1";
		}
		else
		{
			docline = String.valueOf(Integer.parseInt(docline)+1);
		}
		request.setAttribute("tempdocLine", docline);
		//>>
    	
    	if(Globals.gParam1 == null)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Outbound");
    		dispacther.forward(request, response);
    		
    		return;
    	}
		//>>
		
		List<model.mdlOutboundDetail> mdlOutboundDetailList = new ArrayList<model.mdlOutboundDetail>();
		mdlOutboundDetailList.addAll(OutboundDetailAdapter.LoadOutboundDetail(Globals.gParam1,Globals.user));
		request.setAttribute("listoutbounddetail", mdlOutboundDetailList);
		
		List<model.mdlProduct> listProduct = new ArrayList<model.mdlProduct>();
		listProduct.addAll(ProductAdapter.LoadProduct());
		request.setAttribute("listproduct", listProduct);
		
		List<model.mdlBatch> listBatch = new ArrayList<model.mdlBatch>();
		listBatch.addAll(BatchAdapter.LoadBatchJoin(Globals.user));
		request.setAttribute("listbatch", listBatch);
		
		request.setAttribute("condition", Globals.gCondition);
		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
		
		request.setAttribute("docNo", Globals.gParam1); //send doc no to jsp via jstl 
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/outbound_detail.jsp");
		dispacther.forward(request, response);
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
		Globals.gtemp_Param1 = null; //clear string when leave the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String lResult = "";
		
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
			
		//String btnDelete = request.getParameter("btnDelete");
				
		//Declare TextBox
		String txtDocNumber = request.getParameter("txtDocNumber");
		String txtDocLine = request.getParameter("txtDocLine");
		String txtProductID = request.getParameter("txtProductID");
		String txtQty_UOM = request.getParameter("txtQty_UOM");
		String txtUOM = request.getParameter("txtUOM");
		
		//Get the Conversion
		model.mdlProductUom mdlProductUom = new model.mdlProductUom();
		mdlProductUom = ProductUomAdapter.LoadProductUOMByKey(txtProductID, txtUOM);
		if(mdlProductUom.getBaseUOM() == null || mdlProductUom.getBaseUOM().contentEquals("")){
			lResult = "Satuan terkecil untuk produk "+txtProductID+" belum didefinisikan";
			Globals.gCondition = "FailedConversion";
			Globals.gConditionDesc = lResult;
		}
		
		if(!lResult.contains("Satuan terkecil untuk")){
			String txtQty_BaseUOM = String.valueOf(Integer.parseInt(mdlProductUom.getQty())*Integer.parseInt(txtQty_UOM));
			//String txtUOM = "Karton";	
			//String txtQty_BaseUOM = request.getParameter("txtQty_BaseUOM");
			
			String txtBaseUOM = mdlProductUom.getBaseUOM();
			String txtBatch_No = request.getParameter("txtBatch_No");
			String txtPacking_No = request.getParameter("txtPacking_No");
			
			String temp_txtDocNumber = request.getParameter("temp_txtDocNumber");
			String temp_txtDocLine = request.getParameter("temp_txtDocLine");
					
			//Declare mdlOutbound for global
			model.mdlOutboundDetail mdlOutboundDetail = new model.mdlOutboundDetail();
			mdlOutboundDetail.setDocNumber(txtDocNumber);
			mdlOutboundDetail.setDocLine(txtDocLine);
			mdlOutboundDetail.setProductID(txtProductID);
			mdlOutboundDetail.setQty_UOM(txtQty_UOM);
			mdlOutboundDetail.setUOM(txtUOM);
			mdlOutboundDetail.setBaseUOM(txtBaseUOM);
			mdlOutboundDetail.setQty_BaseUOM(txtQty_BaseUOM);
			mdlOutboundDetail.setBatch_No(txtBatch_No);
			mdlOutboundDetail.setPacking_No(txtPacking_No);
			//<<001 Nanda
			
			//Declare mdlStockSummary
			model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
			mdlStockSummary.setProductID(txtProductID);
			mdlStockSummary.setPlantID(lPlantID);
			mdlStockSummary.setWarehouseID(lWarehouseID);
			mdlStockSummary.setBatch_No(txtBatch_No);	
			mdlStockSummary.setUOM(txtBaseUOM);
			mdlStockSummary.setPeriod(docDate.substring(0, 7).replace("-", ""));
			//mdlStockSummary.setPeriod(LocalDateTime.now().toString().substring(0, 7).replace("-", ""));
			//mdlStockSummary.setPeriod(StockSummaryAdapter.GetPeriod(mdlStockSummary, Globals.user));
			
			String lCommand = "";	
			if (keyBtn.equals("save")){
				
				lCommand = "Doc Number : " + mdlOutboundDetail.getDocNumber() + " Doc Line : " + mdlOutboundDetail.getDocLine();
	
				model.mdlStockSummary mdlStockSummary2 =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,Globals.user);
				if(mdlStockSummary2.getQty()==null){
					LogAdapter.InsertLogExc("The stock product not found", "InsertOutboundDetail", lCommand , Globals.user);
					lResult = "Produk tidak ditemukan di stok gudang";
				}
				else{
					if(Integer.parseInt(txtQty_BaseUOM) > Integer.parseInt(mdlStockSummary2.getQty()))
					{
						LogAdapter.InsertLogExc("The product quantity that you input is more than the available quantity in the database", "InsertOutboundDetail", lCommand , Globals.user);
						lResult = "Stok produk tidak mencukupi";
					}
					else
					{
						mdlStockSummary.setQty(String.valueOf(Integer.parseInt(mdlStockSummary2.getQty()) -  Integer.parseInt(txtQty_BaseUOM)));
						lResult = OutboundDetailAdapter.InsertTransactionOutbound(mdlOutboundDetail, mdlStockSummary, Globals.user);
					}
				}
				
				if(lResult.contains("Success Insert Outbound Detail")) {  
					Globals.gCondition = "SuccessInsertOutboundDetail";
		        }  
		        else {
		        	Globals.gCondition = "FailedInsertOutboundDetail";
		        	Globals.gConditionDesc = lResult;
		        } 
			}
		}
		
		Globals.gtemp_Param1 = Globals.gParam1;
		Globals.gtemp_Param2 = lPlantID;
		Globals.gtemp_Param3 = lWarehouseID;
		Globals.gtemp_Param4 = docDate;
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
				
//   		if (keyBtn.equals("update")){
//   			try{
//   				//cari qty di stock summary di tambah qty yg di outbound sebelummnya lalu hasil nya dikurangi qty yang di mau di update
//   				model.mdlStockSummary mdlStockSummary2 =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,Globals.user);
//   				String lqty_old = OutboundDetailAdapter.GetQtybyID(txtDocNumber, txtDocLine, Globals.user);  			
//   				int lMount_qty = Integer.parseInt(mdlStockSummary2.getQty()) + Integer.parseInt(lqty_old);
//   				mdlStockSummary.setQty(String.valueOf(lMount_qty - Integer.parseInt(txtQty_BaseUOM)));
//   				
//   				lResult = OutboundDetailAdapter.UpdateTransactionOutbound(mdlOutboundDetail,mdlStockSummary,Globals.user);
//   			}
//			catch (Exception ex){
//				LogAdapter.InsertLogExc(ex.toString(), "TransactionOutbound", Globals.gCommand , Globals.user);
//				Globals.gReturn_Status = "Error Insert Outbound Detail ";
//			}
//   				    
//		    if(lResult.contains("Success Update Outbound Detail")) {  
//		    	Globals.gCondition = "SuccessUpdate";
//            }  
//	    	else { 
//            	Globals.gCondition = "FailedUpdate"; 
//            } 
//
//			Globals.gtemp_Param1 = Globals.gParam1;
//			Globals.gtemp_Param2 = lPlantID;
//			Globals.gtemp_Param3 = lWarehouseID;
//		    
//		    return;
//		}
   		
   	//>>
				
//		if (btnDelete != null){
//			String lResult = OutboundDetailAdapter.DeleteOutboundDetail(temp_txtDocNumber,temp_txtDocLine);
//			if(lResult.contains("Success Delete OutboundDetail")) {  
//				Globals.gCondition =  "SuccessDelete";
//            }  
//            else {
//            	Globals.gCondition = "FailedDelete"; 
//            }
//					
//			doGet(request, response);
//					
//			return;
//		}	
	}

}
