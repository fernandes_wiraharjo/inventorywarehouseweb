package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import adapter.InboundAdapter;
import adapter.MovementTypeAdapter;
import adapter.PalletizationAdapter;

@WebServlet(urlPatterns={"/getProductPalletization"} , name="getProductPalletization")
public class GetProductPalletizationServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public GetProductPalletizationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lProductID = request.getParameter("productid");
    	String lProductQty = request.getParameter("productqty");
    	String lProductUOM = request.getParameter("productuom");
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	
    	model.mdlPalletization param = new model.mdlPalletization();
    	param.setProductID(lProductID);
    	param.setProductQty(Integer.parseInt(lProductQty));
    	param.setProductUOM(lProductUOM);
    	param.setPlantID(lPlantID);
    	param.setWarehouseID(lWarehouseID);
    	
    	List<model.mdlPalletization> palletizationList = new ArrayList<model.mdlPalletization>();
    	palletizationList.addAll(PalletizationAdapter.LoadProductPalletization(param));
    	
    	Gson gson = new Gson();
    	
    	String jsonlistProductPalletization = gson.toJson(palletizationList);
    	
    	response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonlistProductPalletization);
		
    }
    
}
