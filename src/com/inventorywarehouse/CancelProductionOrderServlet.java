package com.inventorywarehouse;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BomAdapter;
import adapter.CancelProductionOrderAdapter;
import adapter.LogAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/CancelProductionOrder"} , name="CancelProductionOrder")
public class CancelProductionOrderServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public CancelProductionOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	String lOrderType = request.getParameter("orderType");
    	String lOrderNo = request.getParameter("orderNo");
    	
    	String lCommand = "Check Production Order Type : " + lOrderType + " , Order No : " + lOrderNo;
    	
    	Boolean CheckOutstandingProduction = false;
		CheckOutstandingProduction = CancelProductionOrderAdapter.CheckOutstandingProductionOrder(lOrderType, lOrderNo);
    	String lResult = "";
    	
    	if(CheckOutstandingProduction==true)
    		lResult = BomAdapter.CancelProductionOrder(lOrderType, lOrderNo);
    	else
    	{
    		LogAdapter.InsertLogExc("Outstanding transaction founded", "CheckOutstandingProductionOrder", lCommand , Globals.user);
			lResult = "Terdapat konfirmasi produksi yang outstanding";
    	}
    	
    	if(lResult.contains("Success Cancel Production Order")) {  
			Globals.gCondition = "SuccessCancel";
        }  
        else {
        	Globals.gCondition = "FailedCancel"; 
        	Globals.gConditionDesc = lResult;
        }
    	
	}
	
}
