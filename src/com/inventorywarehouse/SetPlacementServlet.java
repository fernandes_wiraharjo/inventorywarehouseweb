package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.InboundAdapter;
import adapter.ProductUomAdapter;
import adapter.ProductWMAdapter;
import adapter.TransferOrderAdapter;

@WebServlet(urlPatterns={"/setPlacement"} , name="setPlacement")
public class SetPlacementServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L; 
    
    public SetPlacementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String lProductID = request.getParameter("productid");
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lStorageUnit = request.getParameter("su");
    	String lStorageUnit2 = request.getParameter("su2");
    	String lStorageUnitQty = request.getParameter("suqty");
    	String lStorageUnitQty2 = request.getParameter("suqty2");
    	String lStorageUnitType = request.getParameter("sut");
    	String lStorageUnitType2 = request.getParameter("sut2");
    	String lBatchNo = request.getParameter("batchno");
    	String lInboundProductUOM = request.getParameter("productuom");
    	String lInboundDoc = request.getParameter("inbounddoc");
    	String lSrcStorageType = request.getParameter("srcstortype");
    	String lSrcStorageBin = request.getParameter("srcstorbin");
    	    
    	model.mdlProductWM mdlProductWM = new model.mdlProductWM();
    	mdlProductWM = ProductWMAdapter.LoadProductWMByKey(lPlantID, lWarehouseID, lProductID);
    	mdlProductWM.setBatchNo(lBatchNo);
    	if(!mdlProductWM.getCapacityUsageUOM().contentEquals(lInboundProductUOM)){
    		Integer ProductUomDefault = Integer.valueOf(ProductUomAdapter.LoadProductUOMByKey(lProductID, mdlProductWM.getCapacityUsageUOM()).getQty());
    		Integer ProductUomCustom = Integer.valueOf(ProductUomAdapter.LoadProductUOMByKey(lProductID, lInboundProductUOM).getQty());
    		Double newCapacityUsage = (Double.valueOf(ProductUomCustom)/Double.valueOf(ProductUomDefault))*mdlProductWM.getCapacityUsage();
    		
    		mdlProductWM.setCapacityUsage(newCapacityUsage);
    		mdlProductWM.setCapacityUsageUOM(lInboundProductUOM);
    	}
    	
    	model.mdlPalletization mdlPalletization = new model.mdlPalletization();
    	mdlPalletization.setStorageUnit1(Integer.parseInt(lStorageUnit));
    	mdlPalletization.setStorageUnit2(Integer.parseInt(lStorageUnit2));
    	mdlPalletization.setQtyPerStorageUnit1(Integer.parseInt(lStorageUnitQty));
    	mdlPalletization.setQtyPerStorageUnit2(Integer.parseInt(lStorageUnitQty2));
    	mdlPalletization.setStorageUnitType1(lStorageUnitType);
    	mdlPalletization.setStorageUnitType2(lStorageUnitType2);
    	mdlPalletization.setInboundDoc(lInboundDoc);
    	mdlPalletization.setSrcStorType(lSrcStorageType);
    	mdlPalletization.setSrcStorBin(lSrcStorageBin);
    	
    	List<model.mdlTransferOrder> listPlacementPlan = new ArrayList<model.mdlTransferOrder>();
    	listPlacementPlan.addAll(TransferOrderAdapter.CreatePlacementPlan(mdlProductWM, mdlPalletization));
    	request.setAttribute("listPlacementPlan", listPlacementPlan);
    	request.setAttribute("temp_result", listPlacementPlan.get(0).getResult());
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/setPlacement.jsp");
		dispacther.forward(request, response);
    }
    
}
