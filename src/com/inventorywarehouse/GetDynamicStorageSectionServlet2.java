package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.StorageSectionAdapter;

@WebServlet(urlPatterns={"/getstoragesection2"} , name="getstoragesection2")
public class GetDynamicStorageSectionServlet2 extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicStorageSectionServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lStorageTypeID = request.getParameter("storagetypeid");
    	String lType = request.getParameter("type");
    	String[] listSeq = request.getParameterValues("listSeq[]");
    	String newlistSeq = "''";
    	if(listSeq!=null)
    		newlistSeq = String.join(",", listSeq);
    	
    	List<model.mdlStorageSection> listStorageSection = new ArrayList<model.mdlStorageSection>();
		
    	listStorageSection.addAll(StorageSectionAdapter.LoadDynamicStorageSection2(lPlantID, lWarehouseID, lStorageTypeID, newlistSeq));
		request.setAttribute("listStorageSection", listStorageSection);
		request.setAttribute("type", lType);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicStorageSection2.jsp");
		dispacther.forward(request, response);
		
	}
    
}
