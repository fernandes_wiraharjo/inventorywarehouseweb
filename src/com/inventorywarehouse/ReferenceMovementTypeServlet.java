package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.ReferenceMovementTypeAdapter;
import adapter.StorageSectionSearchAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/refmovementtype"} , name="refmovementtype")
public class ReferenceMovementTypeServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public ReferenceMovementTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "refmovementtype";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlReferenceMovementType> listReferenceMovementType = new ArrayList<model.mdlReferenceMovementType>();
    		listReferenceMovementType.addAll(ReferenceMovementTypeAdapter.LoadReferenceMovementType());
    		request.setAttribute("listReferenceMovementType", listReferenceMovementType);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/reference_movement_type.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare Deleting Proccess Params
		String temp_txtMovementID = request.getParameter("temp_txtMovementID");
		if (btnDelete != null){
			String lResult = ReferenceMovementTypeAdapter.DeleteReferenceMovementType(temp_txtMovementID);
			if(lResult.contains("Success Delete Reference Movement Type")) {
				Globals.gCondition =  "SuccessDeleteReferenceMovementType";
            }
            else {
            	Globals.gCondition = "FailedDeleteReferenceMovementType";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
			
			return;
		}

		//Declare TextBox for insert and update
		String txtMovementID = request.getParameter("txtMovementID");
		String txtMovementType = request.getParameter("txtMovementType");
		String txtMovementDescription = request.getParameter("txtMovementDescription");

		//Declare mdlReferenceMovementType for global
		model.mdlReferenceMovementType mdlReferenceMovementType = new model.mdlReferenceMovementType();
		mdlReferenceMovementType.setMovementID(txtMovementID);
		mdlReferenceMovementType.setMovementType(txtMovementType);
		mdlReferenceMovementType.setMovementDescription(txtMovementDescription);
		
		
		if (keyBtn.equals("save")){
			String lResult = ReferenceMovementTypeAdapter.InsertReferenceMovementType(mdlReferenceMovementType);
			if(lResult.contains("Success Insert Reference Movement Type")) {
				Globals.gCondition = "SuccessInsertReferenceMovementType";
            }
            else {
            	Globals.gCondition = "FailedInsertReferenceMovementType";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = ReferenceMovementTypeAdapter.UpdateReferenceMovementType(mdlReferenceMovementType);

		    if(lResult.contains("Success Update Reference Movement Type")) {
		    	Globals.gCondition = "SuccessUpdateReferenceMovementType";
            }
            else {
            	Globals.gCondition = "FailedUpdateReferenceMovementType";
            	Globals.gConditionDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
    }
    
}
