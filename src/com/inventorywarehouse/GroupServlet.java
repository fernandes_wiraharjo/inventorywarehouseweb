package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.GroupAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet("/Group")
public class GroupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public GroupServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean CheckMenu;
    	String MenuURL = "Group";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlGroup> mdlGroupList = new ArrayList<model.mdlGroup>();
    		
    		mdlGroupList = GroupAdapter.GetGroupAPI(Globals.user);
    		request.setAttribute("listgroup", mdlGroupList);
    		
    		String lResult = GroupAdapter.InsertGrouplist(mdlGroupList,Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/group.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
