package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.StorageBinAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/storagebin"} , name="storagebin")
public class StorageBinServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public StorageBinServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "storagebin";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);
    		
    		List<model.mdlStorageBin> listStorageBin = new ArrayList<model.mdlStorageBin>();
    		listStorageBin.addAll(adapter.StorageBinAdapter.LoadStorageBin());
    		request.setAttribute("listStorageBin", listStorageBin);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/storage_bin.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare Deleting Proccess Params
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtStorageBinID = request.getParameter("temp_txtStorageBinID");
		String temp_txtStorageTypeID = request.getParameter("temp_txtStorageTypeID");
		if (btnDelete != null){
			String lResult = StorageBinAdapter.DeleteStorageBin(temp_txtPlantID, temp_txtWarehouseID, temp_txtStorageBinID, temp_txtStorageTypeID);
			if(lResult.contains("Success Delete Storage Bin")) {
				Globals.gCondition =  "SuccessDeleteStorageBin";
            }
            else {
            	Globals.gCondition = "FailedDeleteStorageBin";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
			
			return;
		}

		//Declare TextBox for insert and update
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtStorageTypeID = request.getParameter("txtStorageTypeID");
		String txtStorageBinID = request.getParameter("txtStorageBinID");
		String txtStorageSectionID = request.getParameter("txtStorageSectionID");
		String txtStorageBinTypeID = request.getParameter("txtStorageBinTypeID");
		Double txtTotalCapacity = Double.valueOf(request.getParameter("txtTotalCapacity"));
		Double txtCapacityUsed = Double.valueOf(request.getParameter("txtCapacityUsed"));
		Double txtUtilization = Double.valueOf(request.getParameter("txtUtilization"));
		Integer txtNoOfQuants = Integer.valueOf(request.getParameter("txtNoOfQuants"));
		Integer txtNoStorUnits = Integer.valueOf(request.getParameter("txtNoStorUnits"));
		Boolean cbPutawayBlock = Boolean.valueOf(request.getParameter("cbPutawayBlock"));
		Boolean cbStockRemovalBlock = Boolean.valueOf(request.getParameter("cbStockRemovalBlock"));
		String txtBlockReasonID = request.getParameter("txtBlockReasonID");

		//Declare mdlStorageBin for global
		model.mdlStorageBin mdlStorageBin = new model.mdlStorageBin();
		mdlStorageBin.setPlantID(txtPlantID);
		mdlStorageBin.setWarehouseID(txtWarehouseID);
		mdlStorageBin.setStorageTypeID(txtStorageTypeID);
		mdlStorageBin.setStorageBinID(txtStorageBinID);
		mdlStorageBin.setStorageSectionID(txtStorageSectionID);
		mdlStorageBin.setStorageBinTypeID(txtStorageBinTypeID);
		mdlStorageBin.setTotalCapacity(txtTotalCapacity);
		mdlStorageBin.setCapacityUsed(txtCapacityUsed);
		mdlStorageBin.setUtilization(txtUtilization);
		mdlStorageBin.setNoOfQuantity(txtNoOfQuants);
		mdlStorageBin.setNoStorageUnits(txtNoStorUnits);
		mdlStorageBin.setPutawayBlock(cbPutawayBlock);
		mdlStorageBin.setStockRemovalBlock(cbStockRemovalBlock);
		mdlStorageBin.setBlockReasonID(txtBlockReasonID);
		
		if (keyBtn.equals("save")){
			String lResult = StorageBinAdapter.InsertStorageBin(mdlStorageBin);
			if(lResult.contains("Success Insert Storage Bin")) {
				Globals.gCondition = "SuccessInsertStorageBin";
            }
            else {
            	Globals.gCondition = "FailedInsertStorageBin";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = StorageBinAdapter.UpdateStorageBin(mdlStorageBin);

		    if(lResult.contains("Success Update Storage Bin")) {
		    	Globals.gCondition = "SuccessUpdateStorageBin";
            }
            else {
            	Globals.gCondition = "FailedUpdateStorageBin";
            	Globals.gConditionDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
    }
    
}
