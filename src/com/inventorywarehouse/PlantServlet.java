package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import adapter.LoginAdapter;
import adapter.MenuAdapter;
import adapter.PlantAdapter;
import adapter.VendorAdapter;
import helper.AlertHelper;
import model.Globals;

/** Documentation
 * 
 */

@WebServlet(urlPatterns={"/plant"} , name="Plant")
public class PlantServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public PlantServlet() {
		// TODO Auto-generated constructor stub
    	 super();
	}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	Boolean CheckMenu;
    	String MenuURL = "plant";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlantList = new ArrayList<model.mdlPlant>();
    		listPlantList.addAll(adapter.PlantAdapter.LoadPlant(Globals.user));
    		request.setAttribute("listPlant", listPlantList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
        	
        	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_plant.jsp");
    		dispacther.forward(request, response);
    	}
    	
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
    	
    	String btnDelete = request.getParameter("btnDelete");
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}
    		
    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}
    	
    	//Declare TextBox
    	String lplant_id = request.getParameter("txt-plant-id");  
        String lplant_nm = request.getParameter("txt-plant-nm");
        String lplant_desc = request.getParameter("desc-text");
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		
		//Declare mdlPlant for global
		model.mdlPlant mdlPlant = new model.mdlPlant();
		mdlPlant.setPlantID(lplant_id);
		mdlPlant.setPlantNm(lplant_nm);
		mdlPlant.setDesc(lplant_desc);
        
		if (keyBtn.equals("save")){
			String lResult = PlantAdapter.InsertPlant(mdlPlant,Globals.user);	
			if(lResult.contains("Success Insert Plant")) {  
				Globals.gCondition = "SuccessInsertPlant";
            }  
            else {
            	Globals.gCondition = "FailedInsertPlant";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (keyBtn.equals("update")){
		    String lResult = PlantAdapter.UpdatePlant(mdlPlant,Globals.user);	
		    
		    if(lResult.contains("Success Update Plant")) {  
		    	Globals.gCondition = "SuccessUpdatePlant";
            }  
            else {
            	Globals.gCondition = "FailedUpdatePlant";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (btnDelete != null){
			String lResult = PlantAdapter.DeletePlant(temp_txtPlantID,Globals.user);
			if(lResult.contains("Success Delete Plant")) {  
				Globals.gCondition =  "SuccessDeletePlant";
            }  
            else {
            	Globals.gCondition = "FailedDeletePlant"; 
            	Globals.gConditionDesc = lResult;
            }
			
			doGet(request, response);
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
	}
    
}
