package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MenuAdapter;
import adapter.ProductAdapter;
import adapter.StockSummaryAdapter;
import model.Globals;


/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/StockSummary"} , name="StockSummary")
public class StockSummaryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public StockSummaryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		Boolean CheckMenu;
    	String MenuURL = "StockSummary";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlStockSummary> mdlStockSummaryList = new ArrayList<model.mdlStockSummary>();
    		
    		mdlStockSummaryList = StockSummaryAdapter.LoadStockSummary(Globals.user);
    		request.setAttribute("liststocksummary", mdlStockSummaryList);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/stock_summary.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
