package com.inventorywarehouse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BomAdapter;
import adapter.CancelTransactionAdapter;
import adapter.ClosingPeriodAdapter;
import adapter.InboundAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.PlantAdapter;
import adapter.ProductAdapter;
import adapter.ProductUomAdapter;
import adapter.TraceCodeAdapter;
import adapter.VendorAdapter;
import adapter.WarehouseAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

@WebServlet(urlPatterns={"/MasterBom"} , name="MasterBom")
public class BOMServlet extends HttpServlet {

private static final long serialVersionUID = 1L;
    
    public BOMServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
    	Boolean CheckMenu;
    	String MenuURL = "MasterBom";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> mdlPlantList = new ArrayList<model.mdlPlant>();
    		mdlPlantList.addAll(PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", mdlPlantList);
    		
    		List<model.mdlProduct> mdlProductList = new ArrayList<model.mdlProduct>();
    		mdlProductList.addAll(ProductAdapter.LoadBOMProduct());
    		request.setAttribute("listBOMProduct", mdlProductList);
    		
    		List<model.mdlBOM> mdlBOMList = new ArrayList<model.mdlBOM>();
    		mdlBOMList.addAll(BomAdapter.LoadBOM());
    		request.setAttribute("listBOM", mdlBOMList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther;
    		if(Globals.gCondition == "SuccessInsertBOM" || Globals.gCondition == "SuccessUpdateBOM")
    		{
    			dispacther = request.getRequestDispatcher("/bomproductdetail?id="+Globals.gtemp_Param1+"&date="+Globals.gtemp_Param3+"&plant="+Globals.gtemp_Param2+"");
    			//&qty="+Globals.gtemp_Param4+"
    		}
    		else
    		{
    			dispacther = request.getRequestDispatcher("/mainform/pages/bom.jsp");
    		}
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String lResult = "";
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtProductID = request.getParameter("txtProductID");
		
		String newDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtDate"), "dd MMM yyyy", "yyyy-MM-dd");
		String txtDate = newDate;
		
		String txtPlantID = request.getParameter("txtPlantID");
		String txtQty = request.getParameter("txtQty");
		String slUOM = request.getParameter("slMasterUOM");
		
		//Get the Conversion
		model.mdlProductUom mdlProductUom = new model.mdlProductUom();
		mdlProductUom = ProductUomAdapter.LoadProductUOMByKey(txtProductID, slUOM);
		if(mdlProductUom.getBaseUOM() == null || mdlProductUom.getBaseUOM().contentEquals("")){
			lResult = "Satuan terkecil untuk produk "+txtProductID+" belum didefinisikan";
			Globals.gCondition = "FailedConversion";
			Globals.gConditionDesc = lResult;
		}
		
		if(!lResult.contains("Satuan terkecil untuk")){
			String txtQtyBaseUOM = String.valueOf(Integer.parseInt(mdlProductUom.getQty())*Integer.parseInt(txtQty));
			String txtBaseUOM = mdlProductUom.getBaseUOM();
			
			//Declare mdlBOM for global
			model.mdlBOM mdlBom = new model.mdlBOM();
			
			mdlBom.setProductID(txtProductID);
			mdlBom.setValidDate(txtDate);
			mdlBom.setPlantID(txtPlantID);
			mdlBom.setQty(txtQty);
			mdlBom.setUOM(slUOM);
			mdlBom.setQtyBaseUOM(txtQtyBaseUOM);
			mdlBom.setBaseUOM(txtBaseUOM);
			
			//String lCommand = "";
			lResult = "";
			if (keyBtn.equals("save")){
				
				//lCommand = "Insert BOM : " + mdlBom.getProductID() + " " + mdlBom.getValidDate() + " " + mdlBom.getPlantID();
				
				  //List<model.mdlProductBOMDetail> mdlLoadBomProductDetail = ProductAdapter.LoadBOMProductDetail(txtProductID);
						
				  //lResult = BomAdapter.TransactionInsertBOM(mdlBom, mdlLoadBomProductDetail);
				  lResult = BomAdapter.TransactionInsertBOM(mdlBom);
				
				if(lResult.contains("Success Insert Bill Of Materials")) {  
					Globals.gCondition = "SuccessInsertBOM";
	            }  
	            else {
	            	Globals.gCondition = "FailedInsertBOM"; 
	            	Globals.gConditionDesc = lResult;
	            }
				//Globals.gtemp_Param4 = txtQtyBaseUOM;
			}
			
			if (keyBtn.equals("update")){
				
			//Globals.gCommand = "Update BOM : " + mdlBom.getProductID() + " " + mdlBom.getValidDate() + " " + mdlBom.getPlantID();
						
			//List<model.mdlProductBOMDetail> mdlLoadBomProductDetail = ProductAdapter.LoadBOMProductDetail(txtProductID);
									
			//lResult = BomAdapter.TransactionUpdateBOM(mdlBom, mdlLoadBomProductDetail);
				lResult = BomAdapter.TransactionUpdateBOM(mdlBom);
				
				if(lResult.contains("Success Update Bill Of Materials")) {  
					Globals.gCondition = "SuccessUpdateBOM";
	            }  
	            else {
	            	Globals.gCondition = "FailedUpdateBOM"; 
	            	Globals.gConditionDesc = lResult;
	            }
			}
		}
		
		Globals.gtemp_Param1 = txtProductID;
		Globals.gtemp_Param2 = txtPlantID;
		Globals.gtemp_Param3 = ConvertDateTimeHelper.formatDate(txtDate, "yyyy-MM-dd", "dd MMM yyyy");
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
    }
    
}
