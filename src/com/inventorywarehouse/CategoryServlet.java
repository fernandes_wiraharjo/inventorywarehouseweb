package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CategoryAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import model.Globals;

/** Documentation
 */
@WebServlet("/Category")
public class CategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public CategoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Boolean CheckMenu;
    	String MenuURL = "Category";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlCategory> mdlCategoryList = new ArrayList<model.mdlCategory>();
    		
    		mdlCategoryList = CategoryAdapter.GetCategoryAPI(Globals.user);
    		request.setAttribute("listcategory", mdlCategoryList);
    		
    		String lResult = CategoryAdapter.InsertCategorylist(mdlCategoryList,Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/category.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
