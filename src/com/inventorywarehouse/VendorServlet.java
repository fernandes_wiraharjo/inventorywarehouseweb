package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MenuAdapter;
import adapter.VendorAdapter;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/Vendor"} , name="Vendor")
public class VendorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L; 
       
    public VendorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean CheckMenu;
    	String MenuURL = "Vendor";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlVendor> mdlVendorList = new ArrayList<model.mdlVendor>();
    		
    		mdlVendorList.addAll(VendorAdapter.LoadVendor(Globals.user));
    		request.setAttribute("listvendor", mdlVendorList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/vendor.jsp");
    		dispacther.forward(request, response);
    	}
    	
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String btnDelete = request.getParameter("btnDelete");
		
		if (Globals.user == null || Globals.user == "")
    	{ 
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtVendorID = request.getParameter("txtVendorID");
		String txtVendorName = request.getParameter("txtVendorName");
		String txtVendorAddress = request.getParameter("txtVendorAddress");
		String txtPIC = request.getParameter("txtPIC");
		String txtPhone = request.getParameter("txtPhone");
		String slVendorType = request.getParameter("slVendorType");
		String temp_txtVendorID = request.getParameter("temp_txtVendorID");
		
		//Declare mdlVendor for global
		model.mdlVendor mdlVendor = new model.mdlVendor();
		mdlVendor.setVendorID(txtVendorID);
		mdlVendor.setVendorName(txtVendorName);
		mdlVendor.setVendorAddress(txtVendorAddress);
		mdlVendor.setPhone(txtPhone);
		mdlVendor.setPIC(txtPIC);
		mdlVendor.setVendorType(slVendorType);
	
		
		if (keyBtn.equals("save")){
			String lResult = VendorAdapter.InsertVendor(mdlVendor,Globals.user);	
			if(lResult.contains("Success Insert Vendor")) {  
				Globals.gCondition = "SuccessInsertVendor";
            }  
            else {
            	Globals.gCondition = "FailedInsertVendor";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (keyBtn.equals("update")){
		    String lResult = VendorAdapter.UpdateVendor(mdlVendor, Globals.user);	
		    
		    if(lResult.contains("Success Update Vendor")) {  
		    	Globals.gCondition = "SuccessUpdateVendor";
            }  
            else {
            	Globals.gCondition = "FailedUpdateVendor";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (btnDelete != null){
			String lResult = VendorAdapter.DeleteVendor(temp_txtVendorID,Globals.user);
			if(lResult.contains("Success Delete Vendor")) {  
				Globals.gCondition =  "SuccessDeleteVendor";
            }  
            else {
            	Globals.gCondition = "FailedDeleteVendor";
            	Globals.gConditionDesc = lResult;
            }
			
			doGet(request, response);
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
	}

}
