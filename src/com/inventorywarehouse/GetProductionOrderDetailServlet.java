package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BomAdapter;
import adapter.CancelTransactionAdapter;
import adapter.LogAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getproductionorderdetail"} , name="getproductionorderdetail")
public class GetProductionOrderDetailServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetProductionOrderDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lOrderType = request.getParameter("orderType");
    	String lOrderNo = request.getParameter("orderNo");
    	
    	List<model.mdlBOMProductionOrderDetail> mdlProductionOrderDetailList = new ArrayList<model.mdlBOMProductionOrderDetail>();
		mdlProductionOrderDetailList.addAll(BomAdapter.LoadBOMProductionOrderDetail(lOrderType, lOrderNo));
		request.setAttribute("listProductionOrderDetail", mdlProductionOrderDetailList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getProductionOrderDetail.jsp");
		dispacther.forward(request, response);
		
	}
	
}
