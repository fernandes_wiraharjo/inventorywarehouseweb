package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CustomerAdapter;
import adapter.CustomerTypeAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.VendorAdapter;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/Customer"} , name="Customer")
public class CustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CustomerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
//    private static void GetLoadCustomer(){
//    	List<model.mdlCustomer> mdlCustomerList = new ArrayList<model.mdlCustomer>();	
//		mdlCustomerList.addAll(CustomerAdapter.LoadCustomer());	
//    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Boolean CheckMenu;
    	String MenuURL = "Customer";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlCustomerType> mdlCustomerTypeList = new ArrayList<model.mdlCustomerType>();
    		mdlCustomerTypeList.addAll(CustomerTypeAdapter.LoadCustomerType(Globals.user));
    		request.setAttribute("listcusttype", mdlCustomerTypeList);
    		
    		List<model.mdlCustomer> mdlCustomerList = new ArrayList<model.mdlCustomer>();
    		mdlCustomerList.addAll(CustomerAdapter.LoadCustomerJoin(Globals.user));
    		request.setAttribute("listcustomer", mdlCustomerList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/customer.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String btnDelete = request.getParameter("btnDelete");
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtCustomerID = request.getParameter("txtCustomerID");
		String txtCustomerName = request.getParameter("txtCustomerName");
		String txtCustomerAddress = request.getParameter("txtCustomerAddress");
		String txtEmail = request.getParameter("txtEmail");
		String txtPIC = request.getParameter("txtPIC");
		String txtPhone = request.getParameter("txtPhone");
		String txtCustomerTypeID = request.getParameter("txtCustomerTypeID");
		String temp_txtCustomerID = request.getParameter("temp_txtCustomerID");
		
		//Declare mdlCustomer for global
		model.mdlCustomer mdlCustomer = new model.mdlCustomer();
		mdlCustomer.setCustomerID(txtCustomerID);
		mdlCustomer.setCustomerName(txtCustomerName);
		mdlCustomer.setCustomerAddress(txtCustomerAddress);
		mdlCustomer.setEmail(txtEmail);
		mdlCustomer.setPic(txtPIC);
		mdlCustomer.setPhone(txtPhone);
		mdlCustomer.setCustomerTypeID(txtCustomerTypeID);
			
		if (keyBtn.equals("save")){
			String lResult = CustomerAdapter.InsertCustomer(mdlCustomer,Globals.user);	
			if(lResult.contains("Success Insert Customer")) {  
				Globals.gCondition = "SuccessInsertCustomer";
            }  
            else {
            	Globals.gCondition = "FailedInsertCustomer";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (keyBtn.equals("update")){
		    String lResult = CustomerAdapter.UpdateCustomer(mdlCustomer,Globals.user);	
		    
		    if(lResult.contains("Success Update Customer")) {  
		    	Globals.gCondition = "SuccessUpdateCustomer";
            }  
            else {
            	Globals.gCondition = "FailedUpdateCustomer";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (btnDelete != null){
			String lResult = CustomerAdapter.DeleteCustomer(temp_txtCustomerID,Globals.user);
			if(lResult.contains("Success Delete Customer")) {  
				Globals.gCondition =  "SuccessDeleteCustomer";
            }  
            else {
            	Globals.gCondition = "FailedDeleteCustomer";
            	Globals.gConditionDesc = lResult;
            }
			doGet(request, response);
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
        return;
	}

}
