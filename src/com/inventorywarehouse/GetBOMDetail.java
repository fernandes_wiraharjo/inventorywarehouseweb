package com.inventorywarehouse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BomAdapter;
import adapter.CancelTransactionAdapter;
import adapter.LogAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getbomdetail"} , name="getbomdetail")
public class GetBOMDetail extends HttpServlet{

private static final long serialVersionUID = 1L; 
	
	public GetBOMDetail() {
        super();
        // TODO Auto-generated constructor stub
    }
	
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lProductID = request.getParameter("productid");
    	
    	String lnewDate = "";
    	try
    	{
    	String lDate = request.getParameter("date");
		Date initDate = new SimpleDateFormat("dd MMM yyyy").parse(lDate);
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    String newDate = formatter.format(initDate);
		lnewDate = newDate;
    	}
    	catch(Exception ex)
		{
		}
    	
    	String lPlantID = request.getParameter("plantid");
    	
    	List<model.mdlBOMDetail> mdlBomDetailList = new ArrayList<model.mdlBOMDetail>();
		mdlBomDetailList.addAll(BomAdapter.LoadBOMDetail(lProductID, lnewDate, lPlantID));
		request.setAttribute("listBomDetail", mdlBomDetailList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getBomDetail.jsp");
		dispacther.forward(request, response);
	}

}
