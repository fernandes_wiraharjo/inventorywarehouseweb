package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import adapter.BomAdapter;
import adapter.MovementTypeAdapter;
import adapter.StorageTypeIndicatorAdapter;

@WebServlet(urlPatterns={"/getStorTypeFromMovementType"} , name="getStorTypeFromMovementType")
public class GetStorTypeFromMovementTypeServlet extends HttpServlet{

private static final long serialVersionUID = 1L; 
    
    public GetStorTypeFromMovementTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lMovementType = request.getParameter("movementtype");
    	
    	List<model.mdlMovementType> movementTypeList = new ArrayList<model.mdlMovementType>();
    	movementTypeList.addAll(MovementTypeAdapter.LoadStorTypeByKey(lPlantID, lWarehouseID, lMovementType));
    	
    	Gson gson = new Gson();
    	
    	String jsonlistMovementType = gson.toJson(movementTypeList);
    	
    	response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonlistMovementType);
		
    }
    
}
