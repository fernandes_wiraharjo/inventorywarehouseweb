package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.ConfirmationAdapter;
import adapter.LogAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getConfirmatonDetailValidation"} , name="getConfirmatonDetailValidation")
public class GetConfirmationDetailValidation extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetConfirmationDetailValidation() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String lOrderTypeID = request.getParameter("ordertypeid");
    	String lOrderNo = request.getParameter("orderno");
    	String lResult = "";
    	//Globals.gCommand = "Validate Confirmation Detail On Production Order : " + lOrderTypeID + " - " + lOrderNo; 
    	
		lResult = ConfirmationAdapter.ValidateConfirmationDetail(lOrderTypeID, lOrderNo);
    	
    	response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(lResult);
	}
	
}
