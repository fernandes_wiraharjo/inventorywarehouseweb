package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BatchAdapter;
import adapter.LoginAdapter;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/Login"} , name="Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/login.jsp");
		dispacther.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lUserId = request.getParameter("userId");  
        String lPassword = request.getParameter("password"); 
        String btnLogin = request.getParameter("btnLogin"); 
        
        if (btnLogin != null)
        {   
        	if(LoginAdapter.ValidasiLogin(lUserId, lPassword)) {
        		
        		HttpSession session = request.getSession();
    			session.setAttribute("user", lUserId);
    			//setting session to expiry in 30 mins
    			session.setMaxInactiveInterval(30*60);
        		
                RequestDispatcher rd=request.getRequestDispatcher("/Dashboard");
                rd.forward(request,response);
            }  
            else {
            	request.setAttribute("condition", "1"); 
            	RequestDispatcher rd=request.getRequestDispatcher("/mainform/pages/login.jsp");
                rd.forward(request,response);
            }  
        	return;
        }
	}
}
