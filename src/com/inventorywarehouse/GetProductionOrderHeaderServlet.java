package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import adapter.BomAdapter;
import adapter.LogAdapter;
import adapter.UomAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getproductionorderheader"} , name="getproductionorderheader")
public class GetProductionOrderHeaderServlet extends HttpServlet{

private static final long serialVersionUID = 1L; 
	
	public GetProductionOrderHeaderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lOrderTypeID = request.getParameter("orderType");
    	String lOrderNo = request.getParameter("orderNo");
    	
    	List<model.mdlBOMProductionOrder> productionOrderList = new ArrayList<model.mdlBOMProductionOrder>();
		productionOrderList.addAll(BomAdapter.LoadBOMProductionOrderByKey(lOrderTypeID, lOrderNo));
    	
    	Gson gson = new Gson();
    	
    	String jsonlistProductionOrder = gson.toJson(productionOrderList);
    	
    	response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonlistProductionOrder);
		
	}
	
}
