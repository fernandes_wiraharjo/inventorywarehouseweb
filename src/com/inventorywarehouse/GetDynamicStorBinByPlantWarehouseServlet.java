package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.StorageBinAdapter;

@WebServlet(urlPatterns={"/getStorageBinByPlantWarehouse"} , name="getStorageBinByPlantWarehouse")
public class GetDynamicStorBinByPlantWarehouseServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicStorBinByPlantWarehouseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	
    	List<model.mdlStorageBin> listStorageBin = new ArrayList<model.mdlStorageBin>();
		
    	listStorageBin.addAll(StorageBinAdapter.LoadStorageBinByPlantWarehouse(lPlantID, lWarehouseID));
		request.setAttribute("listStorageBin", listStorageBin);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getStorageBinByPlantWarehouse.jsp");
		dispacther.forward(request, response);
		
	}
    
}
