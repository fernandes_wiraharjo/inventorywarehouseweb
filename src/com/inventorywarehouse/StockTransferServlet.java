package com.inventorywarehouse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.ClosingPeriodAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.OutboundAdapter;
import adapter.StockTransferAdapter;
import adapter.TraceCodeAdapter;
import model.Globals;

/** documentation
 * 
 */
@WebServlet(urlPatterns={"/StockTransfer"} , name="StockTransfer")
public class StockTransferServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public StockTransferServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String docNo = "";
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean CheckMenu;
    	String MenuURL = "StockTransfer";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		docNo = StockTransferAdapter.GenerateDocNoStockTransfer(); //001 Nanda
    		request.setAttribute("tempdocNo", docNo);  //001 Nanda
    		
    		List<model.mdlStockTransfer> mdlStockTransferList = new ArrayList<model.mdlStockTransfer>();
    		mdlStockTransferList.addAll(StockTransferAdapter.LoadStockTransfer());
    		request.setAttribute("liststocktransfer", mdlStockTransferList);
    		
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);	
    		
			//    		List<model.mdlWarehouse> listWarehouse = new ArrayList<model.mdlWarehouse>();
			//    		listWarehouse.addAll(adapter.WarehouseAdapter.LoadWarehouse(Globals.user));
			//    		request.setAttribute("listWarehouse", listWarehouse);
    		
			//    		List<model.mdlWarehouse> listWarehouseFrom = new ArrayList<model.mdlWarehouse>();
			//    		listWarehouseFrom.addAll(adapter.WarehouseAdapter.LoadWarehouseExceptECM(Globals.user));
			//    		request.setAttribute("listWarehouseFrom", listWarehouseFrom);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther;
    		if(Globals.gCondition == "SuccessInsertStockTransfer")
    		{
    			dispacther = request.getRequestDispatcher("/StockTransferDetail?transferid="+Globals.gtemp_Param1+"&docDate="+Globals.gtemp_Param2+"&PlantID="+Globals.gtemp_Param3+"&From="+Globals.gtemp_Param4+"&To="+Globals.gtemp_Param5+"");
    		}
    		else
    		{
    			dispacther = request.getRequestDispatcher("/mainform/pages/stock_transfer.jsp");
    		}
    		 
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
			
		String btnDelete = request.getParameter("btnDelete");
				
		//Declare TextBox
		String txtTransferID = request.getParameter("txtTransferID");
		String txtPlantID = request.getParameter("txtPlantID");
		String txtFrom = request.getParameter("txtFrom");
		String txtTo = request.getParameter("txtTo");
		
		String txtDate1 = request.getParameter("txtDate");
		String txtDate = "";
		try
		{
			String newDate = helper.ConvertDateTimeHelper.formatDate(txtDate1, "dd MMM yyyy", "yyyy-MM-dd");
			txtDate=newDate;
		}
		catch(Exception ex)
		{
		}
		
		String temp_txtTransferID = request.getParameter("temp_txtTransferID");
				
		//Declare mdlVendor for global
		model.mdlStockTransfer mdlStockTransfer = new model.mdlStockTransfer();
		mdlStockTransfer.setTransferID(txtTransferID);
		mdlStockTransfer.setFrom(txtFrom);
		mdlStockTransfer.setTo(txtTo);
		mdlStockTransfer.setPlantID(txtPlantID);
		mdlStockTransfer.setDate(txtDate);	
		
		//		if (keyBtn.equals("Warehouse")){
		//			List<model.mdlWarehouse> listWarehouse = new ArrayList<model.mdlWarehouse>();
		//			listWarehouse.addAll(adapter.WarehouseAdapter.LoadWarehousebyPlantID(txtPlantID, Globals.user));
		//			request.setAttribute("listWarehouse", listWarehouse);
		//			
		//			RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/stock_transfer.jsp");
		//			dispacther.forward(request, response);	
		//			return;
		//		}
			
		String lResult = "";
		String lCommand = "";
		if (keyBtn.equals("save")){
			
			lCommand = "Insert TransferID : " + mdlStockTransfer.getTransferID();
			
			model.mdlClosingPeriod CheckLastPeriod = ClosingPeriodAdapter.LoadActivePeriod(Globals.user);
			String sLastPeriod = CheckLastPeriod.getPeriod();
			
			if(sLastPeriod == null || sLastPeriod == "")
			{	
				LogAdapter.InsertLogExc("Period is not set", "InsertStockTransfer", lCommand , Globals.user);
				lResult = "Periode belum ditentukan";
			}
			else
			{
				try{
					DateFormat dfPeriod = new SimpleDateFormat("yyyy-MM");
					Date LastPeriod_On = dfPeriod.parse(sLastPeriod.substring(0,4)+"-"+sLastPeriod.substring(4,6));
					Date StockTransferDate = dfPeriod.parse(mdlStockTransfer.getDate().substring(0, 7));
					
					if(LastPeriod_On.equals(StockTransferDate))
					{
						lResult = StockTransferAdapter.InsertStockTransfer(mdlStockTransfer, Globals.user);
					}
					else
					{
						LogAdapter.InsertLogExc("Your input date period is not activated", "InsertStockTransfer", Globals.gCommand , Globals.user);
						lResult = "Tanggal yang Anda masukkan tidak diizinkan karena tidak sesuai dengan periode yang aktif";
					}
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertStockTransfer", lCommand , Globals.user);
					lResult = "Terjadi kesalahan pada sistem saat proses penguraian tanggal";
				}
			}
			
			if(lResult.contains("Success Insert StockTransfer")) {  
				Globals.gCondition = "SuccessInsertStockTransfer";
				TraceCodeAdapter.InsertTraceCode(txtTransferID, LocalDateTime.now().toString().replace("T"," ") ,"StockTransfer"); //001 Nanda
            }  
            else {
            	Globals.gCondition = "FailedInsertStockTransfer";
            	Globals.gConditionDesc = lResult;
            } 	
		}	
			
			Globals.gtemp_Param1 = txtTransferID;
			Globals.gtemp_Param2 = txtDate1;
			Globals.gtemp_Param3 = txtPlantID;
			Globals.gtemp_Param4 = txtFrom;
			Globals.gtemp_Param5 = txtTo;
					
			response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
	        
			return;
		}
				
//   		if (keyBtn.equals("update")){
// 		    String lResult = StockTransferAdapter.UpdateStockTransfer(mdlStockTransfer,Globals.user);	
//				    
//		    if(lResult.contains("Success Update StockTransfer")) {  //001 Nanda
//		    	Globals.gCondition = "SuccessUpdate";
//            }  
//	    	else {
//            	Globals.gCondition = "FailedUpdate"; 
//            } 
//		    return;
//		}
				
//		if (btnDelete != null){
//			String lResult = OutboundAdapter.DeleteOutbound(temp_txtDocNumber);
//			if(lResult.contains("Success Delete Outbound")) {  //001 Nanda
//				Globals.gCondition =  "SuccessDelete";
//            }  
//            else {
//            	Globals.gCondition = "FailedDelete"; 
//            }	
//			doGet(request, response);		
//			return;
//		}

}
