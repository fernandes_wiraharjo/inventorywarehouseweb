package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.StorageSectionIndicatorAdapter;
import adapter.WarehouseAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/storagesectionindicator"} , name="storagesectionindicator")
public class StorageSectionIndicatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public StorageSectionIndicatorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Boolean CheckMenu;
    	String MenuURL = "storagesectionindicator";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlantList = new ArrayList<model.mdlPlant>();
    		listPlantList.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlantList);

    		List<model.mdlStorageSectionIndicator> mdlStorageSectionIndicatorList = new ArrayList<model.mdlStorageSectionIndicator>();
    		mdlStorageSectionIndicatorList.addAll(StorageSectionIndicatorAdapter.LoadStorageSectionIndicator());
    		request.setAttribute("listStorageSectionIndicator", mdlStorageSectionIndicatorList);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/storage_section_indicator.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}

		//Declare TextBox
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtStorageSectionIndicatorID = request.getParameter("txtStorageSectionIndicatorID");
		String txtStorageSectionIndicatorName = request.getParameter("txtStorageSectionIndicatorName");
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtStorageSectionIndicatorID = request.getParameter("temp_txtStorageSectionIndicatorID");

		//Declare mdlWarehouse for global
		model.mdlStorageSectionIndicator mdlStorageSectionIndicator = new model.mdlStorageSectionIndicator();
		mdlStorageSectionIndicator.setPlantID(txtPlantID);
		mdlStorageSectionIndicator.setWarehouseID(txtWarehouseID);
		mdlStorageSectionIndicator.setStorageSectionIndicatorID(txtStorageSectionIndicatorID);
		mdlStorageSectionIndicator.setStorageSectionIndicatorName(txtStorageSectionIndicatorName);


		if (keyBtn.equals("save")){
			String lResult = StorageSectionIndicatorAdapter.InsertStorageSectionIndicator(mdlStorageSectionIndicator);
			if(lResult.contains("Success Insert Storage Section Indicator")) {
				Globals.gCondition = "SuccessInsertStorageSectionIndicator";
            }
            else {
            	Globals.gCondition = "FailedInsertStorageSectionIndicator";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = StorageSectionIndicatorAdapter.UpdateStorageSectionIndicator(mdlStorageSectionIndicator);

		    if(lResult.contains("Success Update Storage Section Indicator")) {
		    	Globals.gCondition = "SuccessUpdateStorageSectionIndicator";
            }
            else {
            	Globals.gCondition = "FailedUpdateStorageSectionIndicator";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (btnDelete != null){
			String lResult = StorageSectionIndicatorAdapter.DeleteStorageSectionIndicator(temp_txtPlantID, temp_txtWarehouseID, temp_txtStorageSectionIndicatorID);
			if(lResult.contains("Success Delete Storage Section Indicator")) {
				Globals.gCondition =  "SuccessDeleteStorageSectionIndicator";
            }
            else {
            	Globals.gCondition = "FailedDeleteStorageSectionIndicator";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
	}
}
