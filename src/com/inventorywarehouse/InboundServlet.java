package com.inventorywarehouse;

import java.io.IOException;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.ClosingPeriodAdapter;
import adapter.InboundAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.PlantAdapter;
import adapter.TraceCodeAdapter;
import adapter.VendorAdapter;
import adapter.WarehouseAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/Inbound"} , name="Inbound")
public class InboundServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
    
    public InboundServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String docNo = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	Boolean CheckMenu;
    	String MenuURL = "Inbound";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		docNo = InboundAdapter.GenerateDocNoInbound(); //001 Nanda
    		request.setAttribute("tempdocNo", docNo);  //001 Nanda
    		
    		List<model.mdlVendor> mdlVendorList = new ArrayList<model.mdlVendor>();
    		mdlVendorList.addAll(VendorAdapter.LoadVendor(Globals.user));
    		request.setAttribute("listvendor", mdlVendorList);
    		
    		List<model.mdlPlant> mdlPlantList = new ArrayList<model.mdlPlant>();
    		mdlPlantList.addAll(PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", mdlPlantList);
    		
			//List<model.mdlWarehouse> mdlWarehouseList = new ArrayList<model.mdlWarehouse>();
			//mdlWarehouseList.addAll(WarehouseAdapter.LoadWarehouseExceptECM(Globals.user));
			//request.setAttribute("listwarehouse", mdlWarehouseList);
    		
    		List<model.mdlInbound> mdlInboundList = new ArrayList<model.mdlInbound>();
    		mdlInboundList.addAll(InboundAdapter.LoadInbound(Globals.user));
    		request.setAttribute("listInbound", mdlInboundList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther;
    		if(Globals.gCondition == "SuccessInsertInbound")
    		{
    			dispacther = request.getRequestDispatcher("/InboundDetail?docNo="+Globals.gtemp_Param1+"&docDate="+Globals.gtemp_Param2+"&vendorId="+Globals.gtemp_Param3+"&plantId="+Globals.gtemp_Param4+"&warehouseId="+Globals.gtemp_Param5+"&inwmstatus="+Globals.gtemp_Inbound_WMStatus+"&inupdatewmfirst="+Globals.gtemp_Inbound_UpdateWMFirst+"");
    		}
    		else
    		{
    			dispacther = request.getRequestDispatcher("/mainform/pages/inbound.jsp");
    		}
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String btnDelete = request.getParameter("btnDelete");
		//String btnInboundDetail = request.getParameter("btnInboundDetail");
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		if (btnDelete != null){
    			doGet(request, response);
    		}
		//if (btnInboundDetail != null){
		//doGet(request, response);
		//}
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtDocNumber = request.getParameter("txtDocNumber");
		
		String txtDate="";
		String txtDate1= "";
		try
		{
			txtDate1 = request.getParameter("txtDate");
			Date initDate = new SimpleDateFormat("dd MMM yyyy").parse(txtDate1);
		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		    txtDate = formatter.format(initDate);
		}
		catch(Exception ex)
		{
		}
		
		String txtVendorID = request.getParameter("txtVendorID");
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String temp_txtDocNumber = request.getParameter("temp_txtDocNumber");
		String txtDoorID = request.getParameter("txtDoorID");
		String txtStagingAreaID = request.getParameter("txtStagingAreaID");
		String txtInboundWMSStatus = request.getParameter("txtInboundWMSStatus");
		String txtInboundUpdateWMFirst = request.getParameter("txtInboundUpdateWMFirst");
		
		//String txtDocNumberInboundDetail = request.getParameter("txtDocNumberInboundDetail");
		//Declare mdlInbound for global
		model.mdlInbound mdlInbound = new model.mdlInbound();
		mdlInbound.setDocNumber(txtDocNumber);
		mdlInbound.setDate(txtDate);
		mdlInbound.setVendorID(txtVendorID);
		mdlInbound.setPlantID(txtPlantID);
		mdlInbound.setWarehouseID(txtWarehouseID);
		mdlInbound.setDoorID(txtDoorID);
		mdlInbound.setStagingAreaID(txtStagingAreaID);
		mdlInbound.setWMStatus(txtInboundWMSStatus);
			
		String lResult = "";
		String lCommand = "";		
		if (keyBtn.equals("save")){
		
		lCommand = "Insert Inbound Doc : " + mdlInbound.getDocNumber();
			
			model.mdlClosingPeriod CheckLastPeriod = ClosingPeriodAdapter.LoadActivePeriod(Globals.user);
			String sLastPeriod = CheckLastPeriod.getPeriod();
			
			if(sLastPeriod == null || sLastPeriod == "")
			{	
				LogAdapter.InsertLogExc("Period is not set", "InsertInbound", lCommand , Globals.user);
				//lResult = "Error Insert Inbound";
				lResult = "Periode belum ditentukan";
			}
			else
			{
				try{
					DateFormat dfPeriod = new SimpleDateFormat("yyyy-MM");
					Date LastPeriod_On = dfPeriod.parse(sLastPeriod.substring(0,4)+"-"+sLastPeriod.substring(4,6));
					Date InboundDate = dfPeriod.parse(mdlInbound.getDate().substring(0, 7));
					
					if(LastPeriod_On.equals(InboundDate))
					{
						lResult = InboundAdapter.InsertInbound(mdlInbound,Globals.user);
					}
					else
					{
						LogAdapter.InsertLogExc("Your input date period is not activated", "InsertInbound", lCommand , Globals.user);
						//lResult = "Error Insert Inbound";
						lResult = "Tanggal yang Anda masukkan tidak diizinkan karena tidak sesuai dengan periode yang aktif";
					}
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertInbound", lCommand , Globals.user);
					lResult = "Terjadi kesalahan pada sistem saat proses penguraian tanggal";
				}
			}
			
			if(lResult.contains("Success Insert Inbound")) {  
				Globals.gCondition = "SuccessInsertInbound";
				TraceCodeAdapter.InsertTraceCode(txtDocNumber, LocalDateTime.now().toString().replace("T"," ") ,"Inbound"); //001 Nanda
            }  
            else {
            	Globals.gCondition = "FailedInsertInbound";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		Globals.gtemp_Param1 = txtDocNumber;
		Globals.gtemp_Param2 = txtDate1;
		Globals.gtemp_Param3 = txtVendorID;
		Globals.gtemp_Param4 = txtPlantID;
		Globals.gtemp_Param5 = txtWarehouseID;
		Globals.gtemp_Inbound_WMStatus = txtInboundWMSStatus;
		Globals.gtemp_Inbound_UpdateWMFirst = txtInboundUpdateWMFirst;
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
		return;
		
//		if (keyBtn.equals("update")){
//		    String lResult = InboundAdapter.UpdateInbound(mdlInbound,Globals.user);	
//		    
//		    if(lResult.contains("Success Update Inbound")) {  
//		    	Globals.gCondition = "SuccessUpdate";
//            }  
//            else {
//            	Globals.gCondition = "FailedUpdate"; 
//            }
//			return;
//		}
//		
//		if (btnDelete != null){
//			String lResult = InboundAdapter.DeleteInbound(temp_txtDocNumber,Globals.user);
//			if(lResult.contains("Success Delete Inbound")) {  
//				Globals.gCondition =  "SuccessDelete";
//            }  
//            else {
//            	Globals.gCondition = "FailedDelete"; 
//            }
//			doGet(request, response);
//			return;
//		}
		
//		if (btnInboundDetail != null){
//			
//			request.setAttribute("InboundDocNumber", txtDocNumberInboundDetail); 
//        	RequestDispatcher rd=request.getRequestDispatcher("/InboundDetail");
//            rd.forward(request,response);
//            
//			return;
//		}
	}

}
