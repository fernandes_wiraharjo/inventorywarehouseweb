package com.inventorywarehouse;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BomAdapter;
import adapter.LogAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getCancelConfirmationNo"} , name="getCancelConfirmationNo")
public class GetCancelConfirmationNoServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetCancelConfirmationNoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lCancelConfirmationID = request.getParameter("cancelconfirmationid");
    	
    	String ResultCancelConfirmationNo = "";
    	ResultCancelConfirmationNo = BomAdapter.GetCancelConfirmationNo(lCancelConfirmationID);
    	
    	if(ResultCancelConfirmationNo.isEmpty()){
    		ResultCancelConfirmationNo = "1";
   		}
   		else
   		{
   			ResultCancelConfirmationNo = String.valueOf(Integer.parseInt(ResultCancelConfirmationNo)+1);
   		}
    	
    	model.mdlBOMConfirmationDocument BOMConfirmationDoc = new model.mdlBOMConfirmationDocument();
		BOMConfirmationDoc = BomAdapter.GetConfirmationDocumentById(lCancelConfirmationID);
    	
		String limitMaxRange = BOMConfirmationDoc.getRangeTo();
    	
    	if( Integer.parseInt(ResultCancelConfirmationNo) > Integer.parseInt(limitMaxRange) )
    		ResultCancelConfirmationNo = "0";
    	
    	response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(ResultCancelConfirmationNo);
		
	}
	
}
