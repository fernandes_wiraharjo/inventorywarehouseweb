package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.WarehouseAdapter;

@WebServlet(urlPatterns={"/getWarehouseWMStatus"} , name="getWarehouseWMStatus")
public class GetWMStatusServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public GetWMStatusServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lWMStatus = "";
    	
    	lWMStatus = WarehouseAdapter.GetWMStatus(lPlantID,lWarehouseID);
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(lWMStatus);
		
	}
    
}
