package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BatchAdapter;
import adapter.CustomerTypeAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.VendorAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/Batch"} , name="Batch")
public class BatchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public BatchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Globals.gParam1 = request.getParameter("param_id");
		
		Boolean CheckMenu;
    	String MenuURL = "Batch";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlBatch> mdlBatchList = new ArrayList<model.mdlBatch>();
    		mdlBatchList.addAll(BatchAdapter.LoadBatchJoin(Globals.user));
    		request.setAttribute("listbatch", mdlBatchList);
    		
    		List<model.mdlVendor> mdlVendorList = new ArrayList<model.mdlVendor>();
    		mdlVendorList.addAll(VendorAdapter.LoadVendor(Globals.user));
    		request.setAttribute("listvendor", mdlVendorList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/batch.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		doGet(request, response);
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
//		String btnDelete = request.getParameter("btnDelete");
//				
//		//Declare TextBox
		String txtProductID = request.getParameter("txtProductID");
		String txtBatchNo = request.getParameter("txtBatchNo");
		String txtPackingNo = request.getParameter("txtPackingNo");
		
		String newGRDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtGRDate"), "dd MMM yyyy", "yyyy-MM-dd");
		String txtGRDate = newGRDate;
		
		String newExpiredDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtExpiredDate"), "dd MMM yyyy", "yyyy-MM-dd");
		String txtExpiredDate = newExpiredDate;
		
		String txtVendorBatchNo = request.getParameter("txtVendorBatchNo");
		String txtVendorId = request.getParameter("txtVendorId");
//				
//		String temp_txtVendorID = request.getParameter("temp_txtVendorID");
//				
//		//Declare mdlBatch for global
		model.mdlBatch mdlBatch = new model.mdlBatch();
		mdlBatch.setProductID(txtProductID);
		mdlBatch.setBatch_No(txtBatchNo);
		mdlBatch.setPacking_No(txtPackingNo);
		mdlBatch.setGRDate(txtGRDate);
		mdlBatch.setExpired_Date(txtExpiredDate);
		mdlBatch.setVendor_Batch_No(txtVendorBatchNo);
		mdlBatch.setVendorID(txtVendorId);
			
				
//		if (keyBtn.equals("save")){
//			String lResult = BatchAdapter.InsertBatch(mdlBatch);	
//			if(lResult.contains("Success Insert Batch")) {  
//				Globals.gCondition = "SuccessInsert";
//		    }  
//		    else {
//		    	Globals.gCondition = "FailedInsert"; 
//		    } 
//					
//			return;
//
//		}
				
		if (keyBtn.equals("update")){
		    String lResult = BatchAdapter.UpdateBatch(mdlBatch);	
		    
		    if(lResult.contains("Success Update Batch")) {  
		    	Globals.gCondition = "SuccessUpdateBatch";
		    }  
		    else {
		    	Globals.gCondition = "FailedUpdateBatch"; 
		    	Globals.gConditionDesc = lResult;
		    }
		    
		    response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
			
		    return;
		}
				
//		if (btnDelete != null){
//		String lResult = BatchAdapter.DeleteBatch(txtProductID, txtBatchNo);
//			if(lResult.contains("Success Delete Batch")) {  
//				Globals.gCondition =  "SuccessDelete";
//            }  
//            else {
//            	Globals.gCondition = "FailedDelete"; 
//            }
//					
//			doGet(request, response);
//					
//			return;
//		}	
	}

}
