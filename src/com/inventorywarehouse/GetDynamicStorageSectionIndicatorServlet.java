package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.StorageSectionIndicatorAdapter;
import adapter.StorageTypeIndicatorAdapter;

@WebServlet(urlPatterns={"/getstoragesectionindicator"} , name="getstoragesectionindicator")
public class GetDynamicStorageSectionIndicatorServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicStorageSectionIndicatorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	
    	List<model.mdlStorageSectionIndicator> listStorageSectionIndicator = new ArrayList<model.mdlStorageSectionIndicator>();
		
    	listStorageSectionIndicator.addAll(StorageSectionIndicatorAdapter.LoadStorageSectionIndicatorByPlantWarehouse(lPlantID, lWarehouseID));
		request.setAttribute("listStorageSectionIndicator", listStorageSectionIndicator);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicStorageSectionIndicator.jsp");
		dispacther.forward(request, response);
		
    }
    
}
