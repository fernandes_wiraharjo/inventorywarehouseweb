package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BomAdapter;
import adapter.MenuAdapter;
import adapter.WMNumberRangesAdapter;
import adapter.WarehouseAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/numberranges"} , name="numberranges")
public class NumberRangesSetupServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public NumberRangesSetupServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "numberranges";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);
    		
    		model.mdlPlant mdlPlantFirst = listPlant.get(0);
    		if(Globals.gPlantID.isEmpty()){
    			Globals.gPlantID = mdlPlantFirst.PlantID;
    		}
    		request.setAttribute("tempPlantID", Globals.gPlantID);
    		
    		List<model.mdlWMNumberRanges> listWMNumberRange = new ArrayList<model.mdlWMNumberRanges>();
    		listWMNumberRange.addAll(WMNumberRangesAdapter.LoadNumberRangesforWM());
    		request.setAttribute("listWMNumberRange", listWMNumberRange);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/number_ranges_setup.jsp");
    		dispacther.forward(request, response);
    		
    		Globals.gPlantID = "";
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}
		
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		Globals.gPlantID = request.getParameter("slPlantId");
		//Declare TextBox
		//String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtNumberFrom = request.getParameter("txtNumberFrom");
		String txtNumberTo = request.getParameter("txtNumberTo");
		String slNumberRangesType = request.getParameter("slNumberRangesType");
		
		String temp_slNumberRangesType = request.getParameter("temp_slNumberRangesType");
		String temp_PlantID = request.getParameter("temp_PlantID");
		String temp_WarehouseID = request.getParameter("temp_WarehouseID");
		
		//Declare mdlWMNumberRanges for global
		model.mdlWMNumberRanges mdlWMNumberRanges = new model.mdlWMNumberRanges();
		mdlWMNumberRanges.setPlantID(Globals.gPlantID);
		mdlWMNumberRanges.setWarehouseID(txtWarehouseID);
		mdlWMNumberRanges.setFromNumber(txtNumberFrom);
		mdlWMNumberRanges.setToNumber(txtNumberTo);
		mdlWMNumberRanges.setNumberRangesType(slNumberRangesType);
	
		
		if(keyBtn.equals("LoadNumberRanges")){
			return;
		}
		
		if (keyBtn.equals("save")){
			String lResult = WMNumberRangesAdapter.InsertNumberRangesforWM(mdlWMNumberRanges);	
			if(lResult.contains("Success Insert Number Ranges")) {  
				Globals.gCondition = "SuccessInsertNumberRanges";
            }  
            else {
            	Globals.gCondition = "FailedInsertNumberRanges";
            	Globals.gConditionDesc = lResult;
            }

		}
		
		if (keyBtn.equals("update")){
		    String lResult = WMNumberRangesAdapter.UpdateNumberRangesforWM(mdlWMNumberRanges);	
		    
		    if(lResult.contains("Success Update Number Ranges")) {  
		    	Globals.gCondition = "SuccessUpdateNumberRanges";
            }  
            else {
            	Globals.gCondition = "FailedUpdateNumberRanges";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (btnDelete != null){
			String lResult = WMNumberRangesAdapter.DeleteNumberRangesforWM(temp_slNumberRangesType,temp_PlantID,temp_WarehouseID);
			if(lResult.contains("Success Delete Number Ranges")) {  
				Globals.gCondition =  "SuccessDeleteNumberRanges";
            }  
            else {
            	Globals.gCondition = "FailedDeleteNumberRanges";
            	Globals.gConditionDesc = lResult;
            }
			
			doGet(request, response);
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
		return;
    }
    
}
