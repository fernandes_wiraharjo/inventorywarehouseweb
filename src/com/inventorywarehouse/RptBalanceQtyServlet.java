package com.inventorywarehouse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.ValidateNull;
import adapter.InboundAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.OutboundAdapter;
import adapter.StockSummaryAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

/** Documentation
 * 
 */

@WebServlet(urlPatterns={"/RptBalanceQty"} , name="RptBalanceQty")
public class RptBalanceQtyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public RptBalanceQtyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean CheckMenu;
    	String MenuURL = "RptBalanceQty";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		request.setAttribute("condition", Globals.gCondition);		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/rpt_balance_qty.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
	}

	@SuppressWarnings("deprecation")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lrptStartDate = request.getParameter("txtStartDate");
		String lrptEndDate = request.getParameter("txtEndDate");
		String lStartDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtStartDate"),"dd MMM yyyy", "yyyy-MM-dd");
		String lEndDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtEndDate"),"dd MMM yyyy", "yyyy-MM-dd");
		String lTotalInbound = InboundAdapter.GetRptCountInbound(lStartDate, lEndDate); 
		String lTotalOutbound = OutboundAdapter.GetRptCountOutbound(lStartDate, lEndDate);
		
		Connection connection = null;
		try {
			connection = database.RowSetAdapter.getConnection();
			
			JasperPrint jasperPrint = null;
			JasperReport jasperReport;
			String paramJasperReport = getServletContext().getInitParameter("param_jasper_report");
			String paramJasperPrint = getServletContext().getInitParameter("param_jasper_print");
			String paramLogoPath = getServletContext().getInitParameter("param_logo");
			
			//export jadi pdf/excel/html	
			String lExportType = request.getParameter("exporttype");
			
			if (lExportType == null){
				lExportType = new String("");
				}
			else{			
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("lTotalInbound", ValidateNull.NulltoStringEmpty(lTotalInbound));
				params.put("lTotalOutbound", ValidateNull.NulltoStringEmpty(lTotalOutbound));
				params.put("lrptStartDate", ValidateNull.NulltoStringEmpty(lrptStartDate));
				params.put("lrptEndDate", ValidateNull.NulltoStringEmpty(lrptEndDate));
				params.put("lStartDate", ValidateNull.NulltoStringEmpty("'"+lStartDate+"'"));
				params.put("lEndDate", ValidateNull.NulltoStringEmpty("'"+lEndDate+"'"));
				params.put("lUserArea", Globals.user_area);
				params.put("limage", ValidateNull.NulltoStringEmpty(paramLogoPath));
				
				//connection = database.RowSetAdapter.getConnection();

				jasperReport = JasperCompileManager.compileReport(paramJasperReport+"rpt_beginning_qty.jrxml");
				jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);

			}
			
				//check page is blank or not
				List<JRPrintPage> pages = jasperPrint.getPages();
			    if (pages.size()==0){
			        //No pages, do not export instead do other stuff
					//		    	Alert alert = new Alert(AlertType.INFORMATION);
					//		        alert.setTitle("a");
					//		        alert.setHeaderText("b");
					//		        alert.setContentText("c");
					//		        alert.showAndWait();
			    	PrintWriter out = response.getWriter();  
			    	response.setContentType("text/html");  
			    	out.println("<script type=\"text/javascript\">");  
			    	out.println("alert('NO AVAILABLE DATA');");  
			    	out.println("</script>");
			    }
			    else{
			    	if(lExportType.equals("pdf")){
						//coding for Pdf
						JasperExportManager.exportReportToPdfFile(jasperPrint, paramJasperPrint+"rpt_balance.pdf");
						
						String pdfFileName = "rpt_balance.pdf";
						File pdfFile = new File(paramJasperPrint+"rpt_balance.pdf");
						
						response.setContentType("application/pdf");
						response.addHeader("Content-Disposition", "inline; filename=" + pdfFileName);
						response.setContentLength((int) pdfFile.length());
						FileInputStream fileInputStream = new FileInputStream(pdfFile);
						OutputStream responseOutputStream = response.getOutputStream();
						int bytes;
						
						while ((bytes = fileInputStream.read()) != -1) {
							responseOutputStream.write(bytes);
						}
					}
					else if(lExportType.equals("excel")){
						//coding for Excel
							ByteArrayOutputStream os = new ByteArrayOutputStream();
							JRXlsExporter xlsExporter = new JRXlsExporter();
				            xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
				            xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, os);
				            xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "rpt_balance.xls");
				            xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
				            xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				            xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
				            xlsExporter.exportReport();
				            
				            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				            response.setHeader("Content-Disposition", "attachment; filename=rpt_balance.xls");
				            
				            //uncomment this codes if u are want to use servlet output stream
				            //servletOutputStream.write(os.toByteArray());
				            
				            response.getOutputStream().write(os.toByteArray());
				            response.getOutputStream().flush();
				            response.getOutputStream().close();
				            response.flushBuffer();
					}
					else{
						
					}
			    }
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "EXPORT REPORT BALANCE", "", Globals.user);
		}
		finally{
			try {
				//close the opened connection
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "EXPORT REPORT BALANCE", "close opened connection protocol", Globals.user);
			}
		}
		//-- end of export process --
		
		
		String btnShow = request.getParameter("key");
		if (btnShow != null){		
			List<model.mdlRptStock> mdlRptStockList = new ArrayList<model.mdlRptStock>();			
			
			mdlRptStockList = StockSummaryAdapter.LoadRptBalancingQty(lStartDate, lEndDate);
			request.setAttribute("liststock", mdlRptStockList);
			request.setAttribute("lStartDate", ConvertDateTimeHelper.formatDate(lStartDate, "yyyy-MM-dd", "dd MMM yyyy"));
			request.setAttribute("lEndDate", ConvertDateTimeHelper.formatDate(lEndDate, "yyyy-MM-dd", "dd MMM yyyy"));
			request.setAttribute("lTotalOutbound", lTotalOutbound);
			request.setAttribute("lTotalInbound", lTotalInbound);
			RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/Child_Rpt/Table_Balance_Qty.jsp");
			dispacther.forward(request, response);
		}
		
		return;
	}
 
}
