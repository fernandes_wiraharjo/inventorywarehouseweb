package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.org.apache.xpath.internal.operations.Bool;

import adapter.CustomerAdapter;
import adapter.GroupAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.ProductAdapter;
import adapter.ProductDetailAdapter;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet("/Product")
public class ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Boolean CheckMenu;
    	String MenuURL = "Product";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		//Declare keybutton
    		String keyBtn = request.getParameter("key");
    		if (keyBtn == null){
    			keyBtn = new String("");
    		}
    				
    		List<model.mdlProduct> mdlProductList = new ArrayList<model.mdlProduct>();
    		List<model.mdlProductDetail> mdlProductDetailList = new ArrayList<model.mdlProductDetail>();
    		
    		if(keyBtn.contentEquals("sync")){
    			mdlProductList = ProductAdapter.GetProductAPI(Globals.user);
    			mdlProductDetailList =  ProductDetailAdapter.GetProductDetailAPI(Globals.user);
    			String lResult = ProductAdapter.InsertProductlist(mdlProductList,mdlProductDetailList,Globals.user);
    		}
    		
    		List<model.mdlProduct> mdlProductListnew = new ArrayList<model.mdlProduct>();
    		mdlProductListnew = ProductAdapter.LoadProduct();
    		request.setAttribute("listproduct", mdlProductListnew);
    		
    		List<model.mdlUom> listBaseUom = new ArrayList<model.mdlUom>();
    		listBaseUom.addAll(adapter.UomAdapter.LoadMasterBaseUOM());
    		request.setAttribute("listBaseUom", listBaseUom);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/product.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtProductID = request.getParameter("txtProductID");
		String txtProductName = request.getParameter("txtProductName");
		String txtBaseUOM = request.getParameter("txtBaseUOM");
		String txtIsBOM = request.getParameter("txtIsBOM");
		String txtDescription = request.getParameter("txtDescription");
		
		//Declare mdlProduct for global
		model.mdlProduct mdlProduct = new model.mdlProduct();
		mdlProduct.setId(txtProductID);
		mdlProduct.setTitle_en(txtProductName);
		mdlProduct.setBaseUOM(txtBaseUOM);
		mdlProduct.setIs_bom(Boolean.valueOf(txtIsBOM));
		mdlProduct.setDescription_en(txtDescription);
			
		String lResult = "";
		if (keyBtn.equals("save")){
			lResult = ProductAdapter.InsertProduct(mdlProduct);
			
			if(lResult.contains("Success Insert Product")) {  
				Globals.gCondition = "SuccessInsert";
            }  
            else {
            	Globals.gCondition = "FailedInsert";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (keyBtn.equals("update")){
		    lResult = ProductAdapter.UpdateProduct(mdlProduct);
		    
		    if(lResult.contains("Success Update Product")) {  
		    	Globals.gCondition = "SuccessUpdateProduct";
            }  
            else {
            	Globals.gCondition = "FailedUpdateProduct";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
	}

}
