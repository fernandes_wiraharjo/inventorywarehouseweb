package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BomAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.WarehouseAdapter;
import model.Globals;
import model.mdlBomOrderType;

@WebServlet(urlPatterns={"/bomordertype"} , name="bomordertype")
public class BOMOrderTypeServlet extends HttpServlet {

private static final long serialVersionUID = 1L; 
    
    public BOMOrderTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	Boolean CheckMenu;
    	String MenuURL = "bomordertype";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlBomOrderType> listBomOrderType = new ArrayList<model.mdlBomOrderType>();
    		listBomOrderType.addAll(adapter.BomAdapter.LoadBOMOrderType());
    		request.setAttribute("listBomOrderType", listBomOrderType);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/bom_ordertype.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String btnDelete = request.getParameter("btnDelete");
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}
		
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtOrderTypeID = request.getParameter("txtOrderTypeID");
		String txtNumberFrom = request.getParameter("txtNumberFrom");
		String txtNumberTo = request.getParameter("txtNumberTo");
		String txtOrderTypeDesc = request.getParameter("txtOrderTypeDesc");
		String temp_txtOrderTypeID = request.getParameter("temp_txtOrderTypeID");
		
		//Declare mdlBomOrderType for global
		model.mdlBomOrderType mdlBomOrderType = new model.mdlBomOrderType();
		mdlBomOrderType.setOrderTypeID(txtOrderTypeID);
		mdlBomOrderType.setFrom(txtNumberFrom);
		mdlBomOrderType.setTo(txtNumberTo);
		mdlBomOrderType.setOrderTypeDesc(txtOrderTypeDesc);
	
		
		if (keyBtn.equals("save")){
			String lResult = BomAdapter.InsertBomOrderType(mdlBomOrderType);	
			if(lResult.contains("Success Insert Order Type")) {  
				Globals.gCondition = "SuccessInsertOrderType";
            }  
            else {
            	Globals.gCondition = "FailedInsertOrderType";
            	Globals.gConditionDesc = lResult;
            }

		}
		
		if (keyBtn.equals("update")){
		    String lResult = BomAdapter.UpdateBomOrderType(mdlBomOrderType);	
		    
		    if(lResult.contains("Success Update Order Type")) {  
		    	Globals.gCondition = "SuccessUpdateOrderType";
            }  
            else {
            	Globals.gCondition = "FailedUpdateOrderType";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (btnDelete != null){
			String lResult = BomAdapter.DeleteBomOrderType(temp_txtOrderTypeID);
			if(lResult.contains("Success Delete Order Type")) {  
				Globals.gCondition =  "SuccessDeleteOrderType";
            }  
            else {
            	Globals.gCondition = "FailedDeleteOrderType";
            	Globals.gConditionDesc = lResult;
            }
			
			doGet(request, response);
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
		return;
	}
	
}
