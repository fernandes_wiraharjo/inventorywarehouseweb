package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.StorageBinAdapter;
import adapter.StorageTypeAdapter;

@WebServlet(urlPatterns={"/getstoragebin"} , name="getstoragebin")
public class GetDynamicStorageBinServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicStorageBinServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lStorageTypeID = request.getParameter("storagetypeid");
    	String lType = request.getParameter("type");
    	
    	List<model.mdlStorageBin> listStorageBin = new ArrayList<model.mdlStorageBin>();
		
    	listStorageBin.addAll(StorageBinAdapter.LoadDynamicStorageBin(lPlantID, lWarehouseID, lStorageTypeID));
		request.setAttribute("listStorageBin", listStorageBin);
		request.setAttribute("type", lType);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicStorageBin.jsp");
		dispacther.forward(request, response);
		
	}
    
}
