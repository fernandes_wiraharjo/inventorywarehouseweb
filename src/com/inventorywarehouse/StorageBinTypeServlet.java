package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MaterialStagingAreaAdapter;
import adapter.MenuAdapter;
import adapter.StorageBinAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/storagebintype"} , name="storagebintype")
public class StorageBinTypeServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public StorageBinTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "storagebintype";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);

    		List<model.mdlStorageBinType> listStorageBinType = new ArrayList<model.mdlStorageBinType>();
    		listStorageBinType.addAll(StorageBinAdapter.LoadStorageBinType());
    		request.setAttribute("listStorageBinType", listStorageBinType);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/storagebin_type.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}

		//Declare TextBox
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtStorageBinTypeID = request.getParameter("txtStorageBinTypeID");
		String txtStorageBinTypeName = request.getParameter("txtStorageBinTypeName");
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtStorageBinTypeID = request.getParameter("temp_txtStorageBinTypeID");

		//Declare mdlStorageBinType for global
		model.mdlStorageBinType mdlStorageBinType = new model.mdlStorageBinType();
		mdlStorageBinType.setPlantID(txtPlantID);
		mdlStorageBinType.setWarehouseID(txtWarehouseID);
		mdlStorageBinType.setStorageBinTypeID(txtStorageBinTypeID);
		mdlStorageBinType.setStorageBinTypeName(txtStorageBinTypeName);


		if (keyBtn.equals("save")){
			String lResult = StorageBinAdapter.InsertStorageBinType(mdlStorageBinType);
			if(lResult.contains("Success Insert Storage Bin Type")) {
				Globals.gCondition = "SuccessInsertStorageBinType";
            }
            else {
            	Globals.gCondition = "FailedInsertStorageBinType";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = StorageBinAdapter.UpdateStorageBinType(mdlStorageBinType);

		    if(lResult.contains("Success Update Storage Bin Type")) {
		    	Globals.gCondition = "SuccessUpdateStorageBinType";
            }
            else {
            	Globals.gCondition = "FailedUpdateStorageBinType";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (btnDelete != null){
			String lResult = StorageBinAdapter.DeleteStorageBinType(temp_txtPlantID, temp_txtWarehouseID, temp_txtStorageBinTypeID);
			if(lResult.contains("Success Delete Storage Bin Type")) {
				Globals.gCondition =  "SuccessDeleteStorageBinType";
            }
            else {
            	Globals.gCondition = "FailedDeleteStorageBinType";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
    }
    
}
