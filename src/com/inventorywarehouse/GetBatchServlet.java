package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BatchAdapter;
import adapter.WarehouseAdapter;

@WebServlet(urlPatterns={"/getbatch"} , name="getbatch")
public class GetBatchServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L; 
	
	public GetBatchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
		String lParam = request.getParameter("param");
    	String lProductID = request.getParameter("productid");
    	String lVendorID = request.getParameter("vendorid");
    	String lVendorBatch = request.getParameter("vendorbatch");
    	
    	List<model.mdlBatch> mdlBatchList = new ArrayList<model.mdlBatch>();
    	
    	if(lParam == null)
    	{
    		lParam = "";
    	}
    	
    	if(lParam.contentEquals("inbounddetail"))
    	{
    		mdlBatchList.addAll(BatchAdapter.LoadBatchForInboundDetail(lProductID, lVendorID, lVendorBatch));
    	}
    	else
    	{
    		mdlBatchList.addAll(BatchAdapter.LoadBatchbyProductID(lProductID));
    	}
		
		request.setAttribute("listbatch", mdlBatchList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getBatchFromProduct.jsp");
		dispacther.forward(request, response);
	}

}
