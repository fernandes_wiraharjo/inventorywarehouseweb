package com.inventorywarehouse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CancelTransactionAdapter;
import adapter.ClosingPeriodAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.PlantAdapter;
import adapter.ProductDetailAdapter;
import adapter.StockTransferAdapter;
import adapter.StockTransferDetailAdapter;
import adapter.TraceCodeAdapter;
import adapter.WarehouseAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/CancelStockTransfer"} , name="CancelStockTransfer")
public class CancelStockTransferServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CancelStockTransferServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String docNo = "";
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Boolean CheckMenu;
    	String MenuURL = "CancelStockTransfer";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		docNo = StockTransferAdapter.GenerateDocNoStockTransfer(); //001 Nanda
    		request.setAttribute("tempdocNo", docNo);  //001 Nanda
    		
    		//request.setAttribute("listPlant", PlantAdapter.LoadPlant(Globals.user));
    		//request.setAttribute("listwarehouse", WarehouseAdapter.LoadWarehouse(Globals.user));
    		
    		request.setAttribute("listStockTransfer", StockTransferAdapter.LoadNonCancelStockTransfer());
    		request.setAttribute("listCancelStockTransfer", CancelTransactionAdapter.LoadCancelStockTransfer(Globals.user));

    		//request.setAttribute("listCancelStockTransferDetail", CancelTransactionAdapter.LoadCancelStockTransferDetail(Globals.user));
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/cancel_stocktransfer.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtDocNumber = request.getParameter("txtDocNumber");
		
		String newDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtDate"), "dd MMM yyyy", "yyyy-MM-dd");
		String txtDate = newDate;
		
		String txtRefDocNumber = request.getParameter("txtRefDocNumber");
		
		model.mdlStockTransfer stockTransfer = adapter.StockTransferAdapter.LoadStockTransferbyID(txtRefDocNumber);
		if(stockTransfer.getPlantID()==null){
			Globals.gCondition = "FailedLoadStockTransfer";
			Globals.gConditionDesc = "Terjadi kesalahan saat menarik data dokumen pindah gudang saat proses pembatalan";
		}
		else{
			String txtPlantID = stockTransfer.getPlantID();
			String txtWarehouseID = stockTransfer.getFrom();
			
			//Declare mdlCancelTransaction for global
			model.mdlCancelTransaction mdlCancelStockTransfer = new model.mdlCancelTransaction();
			mdlCancelStockTransfer.setDocNumber(txtDocNumber);
			mdlCancelStockTransfer.setDate(txtDate);
			mdlCancelStockTransfer.setPlantID(txtPlantID);
			mdlCancelStockTransfer.setWarehouseID(txtWarehouseID);
			mdlCancelStockTransfer.setRefDocNumber(txtRefDocNumber);
			mdlCancelStockTransfer.setTransaction("Stock Transfer");
				
			String lResult = "";
			String lCommand = "";
			if (keyBtn.equals("save")){
				
				lCommand = "Insert Cancel Stock Transfer Doc : " + mdlCancelStockTransfer.getDocNumber();
					
				model.mdlClosingPeriod CheckLastPeriod = ClosingPeriodAdapter.LoadActivePeriod(Globals.user);
				String sLastPeriod = CheckLastPeriod.getPeriod();
				
				if(sLastPeriod == null || sLastPeriod == "")
				{	
					LogAdapter.InsertLogExc("Period is not set", "InsertCancelStockTransfer", lCommand, Globals.user);
					//lResult = "Error Insert Cancel Stock Transfer Document";
					lResult = "Periode belum ditentukan";
				}
				else
				{
					try{
						DateFormat dfPeriod = new SimpleDateFormat("yyyy-MM");
						Date LastPeriod_On = dfPeriod.parse(sLastPeriod.substring(0,4)+"-"+sLastPeriod.substring(4,6));
						Date CancelStockTransferDate = dfPeriod.parse(mdlCancelStockTransfer.getDate().substring(0, 7));
						
						if(LastPeriod_On.equals(CancelStockTransferDate))
						{
							List<model.mdlStockTransferDetail> mdlLoadStockTransferDetail = StockTransferDetailAdapter.LoadStockTransferDetail(txtRefDocNumber);
							if(mdlLoadStockTransferDetail == null || mdlLoadStockTransferDetail.size() == 0 || mdlLoadStockTransferDetail.isEmpty())
							{
								lResult = "Terjadi kesalahan saat menarik data detil dokumen pindah gudang saat proses pembatalan";
							}
							else
								//re-sync from API
								lResult = CancelTransactionAdapter.TransactionCancelStockTransfer(mdlCancelStockTransfer,mdlLoadStockTransferDetail,Globals.user);
						}
						else
						{
							LogAdapter.InsertLogExc("Your input date period is not activated", "InsertCancelStockTransfer", lCommand , Globals.user);
							//lResult = "Error Insert Cancel Stock Transfer Document";
							lResult = "Tanggal yang Anda masukkan tidak diizinkan karena tidak sesuai dengan periode yang aktif";
						}
					}
					catch(Exception e){
						LogAdapter.InsertLogExc(e.toString(), "InsertCancelStockTransfer", lCommand , Globals.user);
						lResult = "Terjadi kesalahan pada sistem saat proses penguraian tanggal";
					}
				}
				
				if(lResult.contains("Success Insert Cancel Stock Transfer Document")) {  
					Globals.gCondition = "SuccessInsertCancelStockTransfer";
					//TraceCodeAdapter.InsertTraceCode(txtDocNumber, LocalDateTime.now().toString().replace("T"," ") ,"StockTransfer"); //001 Nanda
				} 
				else if(lResult.contains("server e-commerce")){
					Globals.gCondition = "SuccessInsertCancelStockTransfer-1";
					Globals.gConditionDesc = lResult;
				}
	            else {
	            	Globals.gCondition = "FailedInsertCancelStockTransfer";
	            	Globals.gConditionDesc = lResult;
	            }
			}
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
	}

}
