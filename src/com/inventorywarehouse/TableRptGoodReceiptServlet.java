package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.InboundAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

/**
 * 
 */
@WebServlet(urlPatterns={"/TableRptGoodReceiptServlet"} , name="TableRptGoodReceiptServlet")
public class TableRptGoodReceiptServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TableRptGoodReceiptServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("condition", Globals.gCondition);	
		
		//		String lParambtn = Globals.gKey;
		//		//String btnShow = request.getParameter("btnShow");
		//		if (!lParambtn.isEmpty()){		
		//			List<model.mdlRptGoodReceipt> mdlRptGoodReceiptList = new ArrayList<model.mdlRptGoodReceipt>();			
		//			//buat adapter sesuai parameter untuk menampilkan table GoodRecipt
		//			mdlRptGoodReceiptList = InboundAdapter.LoadReportGoodReceipt(Globals.gStartDate, Globals.gEndDate);
		//			request.setAttribute("listgoodreceipt", mdlRptGoodReceiptList);
		//		}
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/Child_Rpt/Table_RptGoodReceipt.jsp");
		dispacther.forward(request, response);
		Globals.gCondition = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lStartDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtStartDate"),"dd MMM yyyy", "yyyy-MM-dd");
		String lEndDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtEndDate"),"dd MMM yyyy", "yyyy-MM-dd");
		String[] lcbCancInboundlist = request.getParameterValues("cbCancInbound");
		String lcbCancInbound = "";
		
		if (lcbCancInboundlist != null) {
		    for (String param : lcbCancInboundlist) {
		        lcbCancInbound = param;
		    }
		}
		
		String lParambtn = request.getParameter("key");
		//String btnShow = request.getParameter("btnShow");
		if (!lParambtn.isEmpty()){		
			List<model.mdlRptGoodReceipt> mdlRptGoodReceiptList = new ArrayList<model.mdlRptGoodReceipt>();			
			//buat adapter sesuai parameter untuk menampilkan table GoodRecipt
			request.setAttribute("lStartDate", ConvertDateTimeHelper.formatDate(lStartDate, "yyyy-MM-dd", "dd MMM yyyy"));
			request.setAttribute("lEndDate", ConvertDateTimeHelper.formatDate(lEndDate, "yyyy-MM-dd", "dd MMM yyyy"));
			mdlRptGoodReceiptList = InboundAdapter.LoadReportGoodReceipt(lStartDate, lEndDate);
			request.setAttribute("listgoodreceipt", mdlRptGoodReceiptList);
		}
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/Child_Rpt/Table_RptGoodReceipt.jsp");
		dispacther.forward(request, response);
		
		//		String lParambtn = request.getParameter("key");
		//		//String btnShow = request.getParameter("btnShow");
		//		if (lParambtn != null){		
		//			Globals.gStartDate = lStartDate;
		//			Globals.gEndDate = lEndDate;
		//			Globals.gKey = lParambtn;	
		//		}
	}

}
