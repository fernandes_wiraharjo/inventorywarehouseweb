package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.MovementTypeAdapter;
import adapter.ReferenceMovementTypeAdapter;
import adapter.StorageBinAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/movementtype"} , name="movementtype")
public class MovementTypeServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public MovementTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "movementtype";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);
    		
    		List<model.mdlReferenceMovementType> listRefMovementType = new ArrayList<model.mdlReferenceMovementType>();
    		listRefMovementType.addAll(ReferenceMovementTypeAdapter.LoadReferenceMovementType());
    		request.setAttribute("listRefMovementType", listRefMovementType);
    		
    		List<model.mdlMovementType> listMovementType = new ArrayList<model.mdlMovementType>();
    		listMovementType.addAll(MovementTypeAdapter.LoadMovementType());
    		request.setAttribute("listMovementType", listMovementType);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/movement_type.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare Deleting Proccess Params
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtMovementID = request.getParameter("temp_txtMovementID");
		if (btnDelete != null){
			String lResult = MovementTypeAdapter.DeleteMovementType(temp_txtPlantID, temp_txtWarehouseID, temp_txtMovementID);
			if(lResult.contains("Success Delete Movement Type")) {
				Globals.gCondition =  "SuccessDeleteMovementType";
            }
            else {
            	Globals.gCondition = "FailedDeleteMovementType";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
			
			return;
		}

		//Declare TextBox for insert and update
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtMovementID = request.getParameter("txtMovementID");
		String txtStorageTypeIDSrc = request.getParameter("txtStorageTypeIDSrc");
		String txtStorageTypeIDDest = request.getParameter("txtStorageTypeIDDest");
		String txtStorageTypeIDRet = request.getParameter("txtStorageTypeIDRet");
		String txtStorageBinIDSrc = request.getParameter("txtStorageBinIDSrc");
		String txtStorageBinIDDest = request.getParameter("txtStorageBinIDDest");
		String txtStorageBinIDRet = request.getParameter("txtStorageBinIDRet");
		Boolean cbFxdBnSrc = Boolean.valueOf(request.getParameter("cbFxdBnSrc"));
		Boolean cbFxdBnDest = Boolean.valueOf(request.getParameter("cbFxdBnDest"));
		Boolean cbScr_Src = Boolean.valueOf(request.getParameter("cbScr_Src"));
		Boolean cbScr_Dest = Boolean.valueOf(request.getParameter("cbScr_Dest"));

		//Declare mdlMovementType for global
		model.mdlMovementType mdlMovementType = new model.mdlMovementType();
		mdlMovementType.setPlantID(txtPlantID);
		mdlMovementType.setWarehouseID(txtWarehouseID);
		mdlMovementType.setMovementID(txtMovementID);
		mdlMovementType.setStorageTypeSource(txtStorageTypeIDSrc);
		mdlMovementType.setStorageTypeDestination(txtStorageTypeIDDest);
		mdlMovementType.setStorageTypeReturn(txtStorageTypeIDRet);
		mdlMovementType.setStorageBinSource(txtStorageBinIDSrc);
		mdlMovementType.setStorageBinDestination(txtStorageBinIDDest);
		mdlMovementType.setStorageBinReturn(txtStorageBinIDRet);
		mdlMovementType.setFxdBinSource(cbFxdBnSrc);
		mdlMovementType.setFxdBinDestination(cbFxdBnDest);
		mdlMovementType.setScrSource(cbScr_Src);
		mdlMovementType.setScrDestination(cbScr_Dest);
		
		if (keyBtn.equals("save")){
			String lResult = MovementTypeAdapter.InsertMovementType(mdlMovementType);
			if(lResult.contains("Success Insert Movement Type")) {
				Globals.gCondition = "SuccessInsertMovementType";
            }
            else {
            	Globals.gCondition = "FailedInsertMovementType";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = MovementTypeAdapter.UpdateMovementType(mdlMovementType);

		    if(lResult.contains("Success Update Movement Type")) {
		    	Globals.gCondition = "SuccessUpdateMovementType";
            }
            else {
            	Globals.gCondition = "FailedUpdateMovementType";
            	Globals.gConditionDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
    }
    
}
