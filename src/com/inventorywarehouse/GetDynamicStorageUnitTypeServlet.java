package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.StorageSectionIndicatorAdapter;
import adapter.StorageUnitTypeAdapter;

@WebServlet(urlPatterns={"/getstorageunittype"} , name="getstorageunittype")
public class GetDynamicStorageUnitTypeServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
    
    public GetDynamicStorageUnitTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lPlantID = request.getParameter("plantid");
    	String lWarehouseID = request.getParameter("warehouseid");
    	String lType = request.getParameter("type");
    	
    	List<model.mdlStorageUnitType> listStorageUnitType = new ArrayList<model.mdlStorageUnitType>();
		
    	listStorageUnitType.addAll(StorageUnitTypeAdapter.LoadStorageUnitTypeByPlantWarehouse(lPlantID, lWarehouseID));
		request.setAttribute("listStorageUnitType", listStorageUnitType);
		request.setAttribute("type", lType);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDynamicStorageUnitType.jsp");
		dispacther.forward(request, response);
		
    }
    
}
