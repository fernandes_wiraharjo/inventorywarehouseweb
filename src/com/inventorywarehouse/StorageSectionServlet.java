package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.WarehouseAdapter;
import adapter.StorageSectionAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/storagesection"} , name="storagesection")
public class StorageSectionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

    public StorageSectionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Boolean CheckMenu;
    	String MenuURL = "storagesection";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlantList = new ArrayList<model.mdlPlant>();
    		listPlantList.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlantList);

			//List<model.mdlWarehouse> mdlWarehouseList = new ArrayList<model.mdlWarehouse>();
			//mdlWarehouseList.addAll(WarehouseAdapter.LoadWarehouseJoin(Globals.user));
			//request.setAttribute("listWarehouse", mdlWarehouseList);

    		List<model.mdlStorageSection> mdlStorageSectionList = new ArrayList<model.mdlStorageSection>();
    		mdlStorageSectionList.addAll(StorageSectionAdapter.LoadStorageSection());
    		request.setAttribute("listStorageSection", mdlStorageSectionList);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/storage_section.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}

		//Declare TextBox
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtStorageTypeID = request.getParameter("txtStorageTypeID");
		String txtStorageSectionID = request.getParameter("txtStorageSectionID");
		String txtStorageSectionName = request.getParameter("txtStorageSectionName");
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtStorageSectionID = request.getParameter("temp_txtStorageSectionID");

		//Declare mdlStorageSection for global
		model.mdlStorageSection mdlStorageSection = new model.mdlStorageSection();
		mdlStorageSection.setPlantID(txtPlantID);
		mdlStorageSection.setWarehouseID(txtWarehouseID);
		mdlStorageSection.setStorageTypeID(txtStorageTypeID);
		mdlStorageSection.setStorageSectionID(txtStorageSectionID);
		mdlStorageSection.setStorageSectionName(txtStorageSectionName);


		if (keyBtn.equals("save")){
			String lResult = StorageSectionAdapter.InsertStorageSection(mdlStorageSection);
			if(lResult.contains("Success Insert Storage Section")) {
				Globals.gCondition = "SuccessInsertStorageSection";
            }
            else {
            	Globals.gCondition = "FailedInsertStorageSection";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = StorageSectionAdapter.UpdateStorageSection(mdlStorageSection);

		    if(lResult.contains("Success Update Storage Section")) {
		    	Globals.gCondition = "SuccessUpdateStorageSection";
            }
            else {
            	Globals.gCondition = "FailedUpdateStorageSection";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (btnDelete != null){
			String lResult = StorageSectionAdapter.DeleteStorageSection(temp_txtPlantID, temp_txtWarehouseID, temp_txtStorageSectionID);
			if(lResult.contains("Success Delete Storage Section")) {
				Globals.gCondition =  "SuccessDeleteStorageSection";
            }
            else {
            	Globals.gCondition = "FailedDeleteStorageSection";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
	}
}
