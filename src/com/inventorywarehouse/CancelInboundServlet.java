package com.inventorywarehouse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CancelTransactionAdapter;
import adapter.ClosingPeriodAdapter;
import adapter.InboundAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.PlantAdapter;
import adapter.TraceCodeAdapter;
import adapter.VendorAdapter;
import adapter.WarehouseAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

@WebServlet(urlPatterns={"/CancelInbound"} , name="CancelInbound")
public class CancelInboundServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
    
    public CancelInboundServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String docNo = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	Boolean CheckMenu;
    	String MenuURL = "CancelInbound";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		docNo = InboundAdapter.GenerateDocNoInbound(); //001 Nanda
			request.setAttribute("tempdocNo", docNo);  //001 Nanda
			
			//		List<model.mdlPlant> mdlPlantList = new ArrayList<model.mdlPlant>();
			//		
			//		mdlPlantList.addAll(PlantAdapter.LoadPlant(Globals.user));
			//		request.setAttribute("listPlant", mdlPlantList);
			//		
			//		List<model.mdlWarehouse> mdlWarehouseList = new ArrayList<model.mdlWarehouse>();
			//		
			//		mdlWarehouseList.addAll(WarehouseAdapter.LoadWarehouse(Globals.user));
			//		request.setAttribute("listwarehouse", mdlWarehouseList);
			
			List<model.mdlInbound> mdlInboundList = new ArrayList<model.mdlInbound>();
			mdlInboundList.addAll(InboundAdapter.LoadNonCancelInbound(Globals.user));
			request.setAttribute("listInbound", mdlInboundList);
			
			List<model.mdlCancelTransaction> mdlCancelInboundList = new ArrayList<model.mdlCancelTransaction>();
			mdlCancelInboundList.addAll(CancelTransactionAdapter.LoadCancelInbound(Globals.user));
			request.setAttribute("listCancelInbound", mdlCancelInboundList);
			
			//		List<model.mdlCancelTransaction> mdlCancelInboundDetailList = new ArrayList<model.mdlCancelTransaction>();
			//		
			//		mdlCancelInboundDetailList.addAll(CancelTransactionAdapter.LoadCancelInboundDetail(Globals.user));
			//		request.setAttribute("listCancelInboundDetail", mdlCancelInboundDetailList);
			
			String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
			
			request.setAttribute("condition", Globals.gCondition);
			request.setAttribute("buttonstatus", ButtonStatus);
			//request.setAttribute("conditionDescription", Globals.gConditionDesc);
			
			RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/cancel_inbound.jsp");
			dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtDocNumber = request.getParameter("txtDocNumber");
		
		String newDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtDate"), "dd MMM yyyy", "yyyy-MM-dd");
		String txtDate = newDate;
		
		String txtRefDocNumber = request.getParameter("txtRefDocNumber");
		
		model.mdlInbound mdlInbound = InboundAdapter.LoadInboundbyDocNo(Globals.user, txtRefDocNumber);
		if(mdlInbound.getPlantID()==null){
			Globals.gCondition = "FailedLoadInbound";
			Globals.gConditionDesc = "Terjadi kesalahan saat menarik data inbound/masuk barang saat proses pembatalan";
		}
		else{
			String txtPlantID = mdlInbound.getPlantID();
			String txtWarehouseID = mdlInbound.getWarehouseID();
			
			//Declare mdlCancelTransaction for global
			model.mdlCancelTransaction mdlCancelInbound = new model.mdlCancelTransaction();
			mdlCancelInbound.setDocNumber(txtDocNumber);
			mdlCancelInbound.setDate(txtDate);
			mdlCancelInbound.setPlantID(txtPlantID);
			mdlCancelInbound.setWarehouseID(txtWarehouseID);
			mdlCancelInbound.setRefDocNumber(txtRefDocNumber);
			mdlCancelInbound.setTransaction("Inbound");
				
			String lResult = "";
			String lCommand = "";
			if (keyBtn.equals("save")){
				
				lCommand = "Insert Cancel Inbound Doc : " + mdlCancelInbound.getDocNumber();
				
				model.mdlClosingPeriod CheckLastPeriod = ClosingPeriodAdapter.LoadActivePeriod(Globals.user);
				String sLastPeriod = CheckLastPeriod.getPeriod();
				
				if(sLastPeriod == null || sLastPeriod == "")
				{	
					LogAdapter.InsertLogExc("Period is not set", "InsertCancelInbound", lCommand , Globals.user);
					//lResult = "Error Insert Cancel Inbound Document";
					lResult = "Periode belum ditentukan";
				}
				else
				{
					try{
						DateFormat dfPeriod = new SimpleDateFormat("yyyy-MM");
						Date LastPeriod_On = dfPeriod.parse(sLastPeriod.substring(0,4)+"-"+sLastPeriod.substring(4,6));
						Date CancelInboundDate = dfPeriod.parse(mdlCancelInbound.getDate().substring(0, 7));
						
						if(LastPeriod_On.equals(CancelInboundDate))
						{
							List<model.mdlInbound> mdlLoadInboundDetail = InboundAdapter.LoadInboundDetail(txtRefDocNumber, Globals.user);
							if(mdlLoadInboundDetail == null || mdlLoadInboundDetail.size() == 0 || mdlLoadInboundDetail.isEmpty())
							{
								Globals.gCondition = "FailedLoadInboundDetail";
								Globals.gConditionDesc = "Terjadi kesalahan saat menarik data detil inbound/masuk barang saat proses pembatalan";
							}
							else
								lResult = CancelTransactionAdapter.TransactionCancelInbound(mdlCancelInbound,mdlLoadInboundDetail,Globals.user);
						}
						else
						{
							LogAdapter.InsertLogExc("Your input date period is not activated", "InsertCancelInbound", lCommand , Globals.user);
							//lResult = "Error Insert Cancel Inbound Document";
							lResult = "Tanggal yang Anda masukkan tidak diizinkan karena tidak sesuai dengan periode yang aktif";
						}
					}
					catch(Exception e){
						LogAdapter.InsertLogExc(e.toString(), "InsertCancelInbound", lCommand , Globals.user);
						lResult = "Terjadi kesalahan pada sistem saat proses penguraian tanggal";
					}
				}
				
				if(lResult.contains("Success Insert Cancel Inbound Document")) {  
					Globals.gCondition = "SuccessInsertCancelInbound";
					//TraceCodeAdapter.InsertTraceCode(txtDocNumber, LocalDateTime.now().toString().replace("T"," ") ,"Inbound"); //001 Nanda
	            }  
	            else {
	            	if(!Globals.gCondition.contains("FailedLoadInboundDetail")){
		            	Globals.gCondition = "FailedInsertCancelInbound"; 
		            	Globals.gConditionDesc = lResult;
	            	}
	            }
			}
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
		return;
	}
    
}
