package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BrandAdapter;
import adapter.MenuAdapter;
import adapter.PlantAdapter;
import adapter.RoleAdapter;
import adapter.UserAdapter;
import adapter.ValidateNull;
import model.Globals;

@WebServlet(urlPatterns={"/user"} , name="user")
public class UserServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
    

    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
    	Boolean CheckMenu;
    	String MenuURL = "user";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		String param_user_area = "";
	        
	        //setting session for user area access
			String user_area = UserAdapter.LoadUserArea(Globals.user);
			
			String[] list_user_area = user_area.split(",");
			int list_user_area_length = list_user_area.length; //set the length of list
			for(int i=0; i<list_user_area_length;i++)
			{
				if(param_user_area.contentEquals("")) //if first index
					param_user_area = "'%"+list_user_area[i]+"%'";
				else
					param_user_area += " OR PlantId LIKE "+"'%"+list_user_area[i]+"%'";
			}
			
    		//User
    		List<model.mdlUser> UserList = new ArrayList<model.mdlUser>();
    		UserList.addAll(UserAdapter.LoadUser(param_user_area));
    		request.setAttribute("listuser", UserList);
    		
    		//Plant
    		List<model.mdlPlant> PlantList = new ArrayList<model.mdlPlant>();
    		PlantList.addAll(PlantAdapter.LoadPlant(Globals.user));
    		request.setAttribute("listplant", PlantList);
    		
    		//Role
    		List<model.mdlRole> RoleList = new ArrayList<model.mdlRole>();
    		RoleList.addAll(RoleAdapter.LoadRole());
    		request.setAttribute("listrole", RoleList);
    		
    		//String ButtonStatus;
    		//ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("errorDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/user_management.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	Globals.user = (String) session.getAttribute("user");
    	String lResult = "";
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}

		
		//Declare Variable
    	String btnDelete = request.getParameter("btnDelete");
    	String keyBtn =  ValidateNull.NulltoStringEmpty(request.getParameter("key"));
    	String temp_txtUserId = request.getParameter("temp_txtUserId");
    	String txtUserID = request.getParameter("txtUserID");
		String txtPassword = request.getParameter("txtPassword");
		String slRole = request.getParameter("slRole");
		
		String[] listArea = request.getParameterValues("arealist[]");
		String arealist = String.join(",",ValidateNull.NulltoStringArrayEmpty(listArea));

		
		model.mdlUser mdlUser = new model.mdlUser();
		mdlUser.setUserId(txtUserID);
		mdlUser.setPassword(txtPassword);
		mdlUser.setRole(slRole);
		mdlUser.setPlant(arealist);
		
		if (keyBtn.equals("save")){
			lResult = UserAdapter.InsertUser(mdlUser);
			
			if(lResult.contains("Success Insert User")) {  
				Globals.gCondition = "SuccessInsertUser";
            }  
            else {
            	Globals.gCondition = "FailedInsertUser";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (keyBtn.equals("update")){
			lResult = UserAdapter.UpdateUser(mdlUser);
			
			if(lResult.contains("Success Update User")) {  
				Globals.gCondition = "SuccessUpdateUser";
            }  
            else {
            	Globals.gCondition = "FailedUpdateUser";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (btnDelete != null){
			lResult = UserAdapter.DeleteUser(temp_txtUserId);
			
			if(lResult.contains("Success Delete User")) {  
				Globals.gCondition =  "SuccessDeleteUser";
            }  
            else {
            	Globals.gCondition = "FailedDeleteUser"; 
            	Globals.gConditionDesc = lResult;
            }
			
			doGet(request, response);
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
        return;
	}
    
}
