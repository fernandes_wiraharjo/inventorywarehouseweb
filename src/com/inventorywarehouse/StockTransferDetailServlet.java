package com.inventorywarehouse;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BatchAdapter;
import adapter.ClosingPeriodAdapter;
import adapter.LogAdapter;
import adapter.OutboundDetailAdapter;
import adapter.ProductAdapter;
import adapter.ProductDetailAdapter;
import adapter.ProductUomAdapter;
import adapter.StockSummaryAdapter;
import adapter.StockTransferDetailAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/StockTransferDetail"} , name="StockTransferDetail")
public class StockTransferDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static String lPlantID = ""; 
	private static String lFrom = ""; 
	private static String lTo = "";
	private static String docDate = "";
       
    public StockTransferDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String docline = "";
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Globals.gParam1 = request.getParameter("transferid");
		lPlantID = request.getParameter("PlantID");
		lFrom = request.getParameter("From");
		lTo = request.getParameter("To");
		try
		{
			String newDate = ConvertDateTimeHelper.formatDate(request.getParameter("docDate"), "dd MMM yyyy", "yyyy-MM-dd");
			docDate = newDate;
		}
		catch(Exception ex)
		{
		}
		
		if (Globals.gParam1 == null)
    	{
			Globals.gParam1 = Globals.gtemp_Param1; //store temporary string as transferid
			lPlantID = Globals.gtemp_Param2; //store temporary string as plant ID
			lFrom = Globals.gtemp_Param3; //store temporary string as From
			lTo = Globals.gtemp_Param4; //store temporary string as To
			docDate = Globals.gtemp_Param5; //store temporary string as document date
    	}
    	
		//<<001 Nanda
    	docline = StockTransferDetailAdapter.GenerateTransLineStockTransDet(Globals.gParam1);
    	if(docline.isEmpty())
    		docline = "1";
		else
			docline = String.valueOf(Integer.parseInt(docline)+1);
    	
		request.setAttribute("tempdocLine", docline);
		//>>
		
    	if(Globals.gParam1 == null)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/StockTransfer");
    		dispacther.forward(request, response);
    		
    		return;
    	}
		//>>
		
		List<model.mdlStockTransferDetail> listStockTransferDetail = new ArrayList<model.mdlStockTransferDetail>();
		listStockTransferDetail.addAll(StockTransferDetailAdapter.LoadStockTransferDetail(Globals.gParam1));
		request.setAttribute("liststocktransferdetail", listStockTransferDetail);
		
		List<model.mdlProduct> listProduct = new ArrayList<model.mdlProduct>();
		listProduct.addAll(ProductAdapter.LoadProduct());
		request.setAttribute("listproduct", listProduct);
		
		List<model.mdlBatch> listBatch = new ArrayList<model.mdlBatch>();
		listBatch.addAll(BatchAdapter.LoadBatchJoin(Globals.user));
		request.setAttribute("listbatch", listBatch);
		
		request.setAttribute("condition", Globals.gCondition);
		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
		
		request.setAttribute("transferid", Globals.gParam1); //send doc no to jsp via jstl 
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/stock_transfer_detail.jsp");
		dispacther.forward(request, response);
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
		Globals.gtemp_Param1 = null; //clear string when leave the page
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String lResult = "";
		
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
			
		//String btnDelete = request.getParameter("btnDelete");
				
		//Declare TextBox
		String txtTransferID = request.getParameter("txtTransferID");
		String txtTransferLine = request.getParameter("txtTransferLine");
		String txtProductID = request.getParameter("txtProductID");
		String txtQty_UOM = request.getParameter("txtQty_UOM");
		String txtUOM = request.getParameter("txtUOM");
		
		//Get the Conversion
		model.mdlProductUom mdlProductUom = new model.mdlProductUom();
		mdlProductUom = ProductUomAdapter.LoadProductUOMByKey(txtProductID, txtUOM);
		if(mdlProductUom.getBaseUOM() == null || mdlProductUom.getBaseUOM().contentEquals("")){
			lResult = "Satuan terkecil untuk produk "+txtProductID+" belum didefinisikan";
			Globals.gCondition = "FailedConversion";
			Globals.gConditionDesc = lResult;
		}
		else{
			String txtQty_BaseUOM = String.valueOf(Integer.parseInt(mdlProductUom.getQty())*Integer.parseInt(txtQty_UOM));
			
			//String txtUOM = "Karton";	
			//String txtQty_BaseUOM = request.getParameter("txtQty_BaseUOM");
			
			String txtBaseUOM = mdlProductUom.getBaseUOM();
			String txtBatch_No = request.getParameter("txtBatch_No");
			String txtPacking_No = request.getParameter("txtPacking_No");
			
			String temp_txtDocNumber = request.getParameter("temp_txtDocNumber");
			String temp_txtDocLine = request.getParameter("temp_txtDocLine");
					
			//Declare mdlOutbound for global
			model.mdlStockTransferDetail mdlStockTransferDetail = new model.mdlStockTransferDetail();
			mdlStockTransferDetail.setTransferID(txtTransferID);
			mdlStockTransferDetail.setTransferLine(txtTransferLine);
			mdlStockTransferDetail.setProductID(txtProductID);
			mdlStockTransferDetail.setQty_UOM(txtQty_UOM);
			mdlStockTransferDetail.setUOM(txtUOM);
			mdlStockTransferDetail.setBaseUOM(txtBaseUOM);
			mdlStockTransferDetail.setQty_BaseUOM(txtQty_BaseUOM);
			mdlStockTransferDetail.setBatch_No(txtBatch_No);
			mdlStockTransferDetail.setPacking_No(txtPacking_No);		
			//<<001 Nanda
			
			
			
			//Declare mdlStockSummary From
			model.mdlStockSummary mdlStockSummaryFrom = new model.mdlStockSummary();		
			mdlStockSummaryFrom.setProductID(txtProductID);
			mdlStockSummaryFrom.setPlantID(lPlantID);
			mdlStockSummaryFrom.setWarehouseID(lFrom);
			mdlStockSummaryFrom.setBatch_No(txtBatch_No);	
			mdlStockSummaryFrom.setUOM(txtBaseUOM);
			mdlStockSummaryFrom.setPeriod(docDate.substring(0, 7).replace("-", ""));
			//mdlStockSummaryFrom.setPeriod(LocalDateTime.now().toString().substring(0, 7).replace("-", ""));
			//mdlStockSummaryFrom.setPeriod(StockSummaryAdapter.GetPeriod(mdlStockSummaryFrom, Globals.user));
			
			//Declare mdlStockSummary From
			model.mdlStockSummary mdlStockSummaryTo = new model.mdlStockSummary();
			
			mdlStockSummaryTo.setProductID(txtProductID);
			mdlStockSummaryTo.setPlantID(lPlantID);
			mdlStockSummaryTo.setWarehouseID(lTo);
			mdlStockSummaryTo.setBatch_No(txtBatch_No);	
			mdlStockSummaryTo.setUOM(txtBaseUOM);
			mdlStockSummaryTo.setPeriod(docDate.substring(0, 7).replace("-", ""));
			//mdlStockSummaryTo.setPeriod(LocalDateTime.now().toString().substring(0, 7).replace("-", ""));
			//mdlStockSummaryTo.setPeriod(StockSummaryAdapter.GetPeriod(mdlStockSummaryTo, Globals.user));
					
			//Declare mdlParUptStockECM for global
			model.mdlParUptStockECM mdlParUptStockECM = new model.mdlParUptStockECM();
			String[] lSplitProduct = txtProductID.split("-"); //split ProductId
			mdlParUptStockECM.setProduct_detail_id(lSplitProduct[1]);
			mdlParUptStockECM.setInvoice(txtTransferID);
			mdlParUptStockECM.setQuantity(txtQty_BaseUOM);
			
			String lCommand = "";
						
			if (keyBtn.equals("save")){
				lCommand = "Insert Stock Transfer Id : " + mdlStockTransferDetail.getTransferID() + " Line : " + mdlStockTransferDetail.getTransferLine();
						
				//model.mdlClosingPeriod CheckActivatePeriod = ClosingPeriodAdapter.LoadActivePeriod(Globals.user);
				//				
				//if(mdlStockSummaryFrom.getPeriod().contentEquals(CheckActivatePeriod.getPeriod()))
				//{
						
				//calculate qty keluar barang akan mengurangi qty di stock summary (1)
				model.mdlStockSummary mdlStockSummary2From =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummaryFrom,Globals.user);
				if(mdlStockSummary2From.getQty()==null){
					LogAdapter.InsertLogExc("The stock product not found", "InsertStockTransferDetail", lCommand , Globals.user);
					lResult = "Produk tidak ditemukan di stok gudang";
				}
				else{
					if (Integer.parseInt(txtQty_BaseUOM) > Integer.parseInt(mdlStockSummary2From.getQty()))
					{
						LogAdapter.InsertLogExc("The product quantity that you want to transfer is more than the available quantity", "InsertStockTransferDetail", lCommand , Globals.user);
						//lResult = "Error Insert Stock Transfer Detail";
						lResult = "Stok produk tidak mencukupi";
					}
					else
					{
						//calculate qty keluar barang akan mengurangi qty di stock summary (2)
						mdlStockSummaryFrom.setQty(String.valueOf(Integer.parseInt(mdlStockSummary2From.getQty()) -  Integer.parseInt(txtQty_BaseUOM)));
							
						//calculate qty masuk barang akan menambah qty di stock summary (1)
						model.mdlStockSummary mdlStockSummary2To =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummaryTo,Globals.user);
						if(mdlStockSummary2To.getQty() == null)
						{
							mdlStockSummary2To.setQty("0");
						}
						//calculate qty masuk barang akan menambah qty di stock summary (2)
						mdlStockSummaryTo.setQty(String.valueOf(Integer.parseInt(mdlStockSummary2To.getQty()) +  Integer.parseInt(txtQty_BaseUOM)));
						
						lResult = StockTransferDetailAdapter.InsertTransactionStockTransfer(mdlStockTransferDetail, mdlStockSummaryFrom, mdlStockSummaryTo, Globals.user);
					
						if(lTo.equals("Website")){
							//Send API stock Ecommerce
							
							List<model.mdlProductDetail> mdlProductDetaillist = new ArrayList<model.mdlProductDetail>();
							mdlProductDetaillist = StockSummaryAdapter.UpdateStockAPI(mdlParUptStockECM);
							//,lPlantID,txtProductID -- old parameter of updatestockapi
							
							if (mdlProductDetaillist.isEmpty()){
								Globals.gCondition = "FailedSendAPI";
							}
							
							//re-sync from API
							ProductDetailAdapter.GetProductDetailAPI(Globals.user);
							Globals.gWarehouseId = "";
						}
						
					}
				}
				
				if(lResult.contains("Success Insert Stock Transfer Detail")) {  
					if(!Globals.gCondition.contains("FailedSendAPI"))
						Globals.gCondition = "SuccessInsertStockTransferDetail";
					else
						Globals.gCondition = "SuccessInsertStockTransferDetail-1";
	            }  
	            else {
	            	Globals.gCondition = "FailedInsertStockTransferDetail"; 
	            	Globals.gConditionDesc = lResult;
	            } 
			}
		}
		
		Globals.gtemp_Param1 = Globals.gParam1;
		Globals.gtemp_Param2 = lPlantID;
		Globals.gtemp_Param3 = lFrom;
		Globals.gtemp_Param4 = lTo;
		Globals.gtemp_Param5 = docDate;
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
	}

}
