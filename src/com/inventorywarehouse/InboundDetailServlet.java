package com.inventorywarehouse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BatchAdapter;
import adapter.ClosingPeriodAdapter;
import adapter.InboundAdapter;
import adapter.LogAdapter;
import adapter.ProductAdapter;
import adapter.ProductUomAdapter;
import adapter.StockSummaryAdapter;
import adapter.TransactionAdapter;
import adapter.TransferOrderAdapter;
import adapter.UomAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/InboundDetail"} , name="InboundDetail")
public class InboundDetailServlet extends HttpServlet{

private static final long serialVersionUID = 1L;
    
    public InboundDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    String docDate1,docDate,vendorId,plantId,warehouseId,inboundWMStatus,inboundUpdateWMFirst;
    String docline = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		Globals.gParam1 = (String) request.getAttribute("InboundDocNumber");
    	Globals.gParam1 = request.getParameter("docNo");
    	Globals.gVendorId = request.getParameter("vendorId");
    	
    	try
    	{
	    	docDate1 = request.getParameter("docDate");
			Date initDate = new SimpleDateFormat("dd MMM yyyy").parse(docDate1);
		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		    docDate = formatter.format(initDate);
    	}
    	catch(Exception ex)
    	{
    		
    	}
    	
    	vendorId = request.getParameter("vendorId");
    	plantId = request.getParameter("plantId");
    	warehouseId = request.getParameter("warehouseId");
    	inboundWMStatus = request.getParameter("inwmstatus");
    	inboundUpdateWMFirst = request.getParameter("inupdatewmfirst");
    	
    	if (Globals.gParam1 == null)
    	{
	    	Globals.gParam1 = Globals.gtemp_Param1; //store temporary string as docNo
	    	docDate = Globals.gtemp_Param2; // store temporary string as docDate
	    	vendorId = Globals.gtemp_Param3; //store temporary string as vendor ID
	    	plantId = Globals.gtemp_Param4; //store temporary string as plant ID
	    	warehouseId = Globals.gtemp_Param5; //store temporary string as warehouse ID
	    	Globals.gVendorId = Globals.gtemp_Param3; //store temporary string as vendor ID
	    	inboundWMStatus = Globals.gtemp_Inbound_WMStatus;
	    	inboundUpdateWMFirst = Globals.gtemp_Inbound_UpdateWMFirst;
    	}
    	
    	//<<001 Nanda
    	docline = InboundAdapter.GenerateDocLineInboundDet(Globals.gParam1);
    	if(docline.isEmpty()){
    	 docline = "1";
		}
		else
		{
			docline = String.valueOf(Integer.parseInt(docline)+1);
		}
		request.setAttribute("tempdocLine", docline);
		//>>
    	
    	if(Globals.gParam1 == null)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Inbound");
    		dispacther.forward(request, response);
    		
    		return;
    	}
		
		List<model.mdlProduct> mdlProductList = new ArrayList<model.mdlProduct>();
		mdlProductList.addAll(ProductAdapter.LoadProduct());
		request.setAttribute("listProduct", mdlProductList);
		
		List<model.mdlInbound> mdlInboundDetailList = new ArrayList<model.mdlInbound>();
		mdlInboundDetailList.addAll(InboundAdapter.LoadInboundDetail(Globals.gParam1,Globals.user));
		request.setAttribute("listinboundDetail", mdlInboundDetailList);
		
		List<model.mdlTransferOrder> mdlTOList = new ArrayList<model.mdlTransferOrder>();
		mdlTOList.addAll(TransferOrderAdapter.LoadTransferOrderByInbound(Globals.gParam1));
		
		if(inboundWMStatus.contentEquals("0") || mdlInboundDetailList.size() == 0 || mdlTOList.size() != 0)
    		request.setAttribute("createTOButtonStatus", "none");
		
		if(inboundWMStatus.contentEquals("0") || mdlInboundDetailList.size() == 0 || mdlTOList.size() == 0)
    		request.setAttribute("showTOButtonStatus", "none");
		
		if(mdlTOList.size() != 0)
			request.setAttribute("addnewButtonStatus", "disabled");
		
		request.setAttribute("condition", Globals.gCondition);
		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
		
		request.setAttribute("docNo", Globals.gParam1); //send doc no to jsp via jstl
		request.setAttribute("temp_vendorid", Globals.gVendorId);
		request.setAttribute("temp_plantid", plantId);
		request.setAttribute("temp_warehouseid", warehouseId);
		
		//load master uom for uom dropdown list
		//		List<model.mdlUom> mdlUomList = new ArrayList<model.mdlUom>();
		//		mdlUomList.addAll(UomAdapter.LoadUOM());
		//		request.setAttribute("listUom", mdlUomList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/inbound_detail.jsp");
		dispacther.forward(request, response);
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
		
		//clear string when leave the page
		Globals.gtemp_Param1 = null;
		//Globals.gtemp_Param2 = null; 
		//Globals.gtemp_Param3 = null; 
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//    	String btnDelete = request.getParameter("btnDelete");s
    	String lResult = "";
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
		//    		if (btnDelete != null){
		//    			doGet(request, response);
		//    		}
		//    		if (btnInboundDetail != null){
		//    			doGet(request, response);
		//    		}
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtDocNumber = request.getParameter("txtDocNumber");
		String txtDocLine = request.getParameter("txtDocLine");
		String txtProductID = request.getParameter("txtProductID");
		String txtQtyUOM = request.getParameter("txtQtyUOM");
		String slUOM = request.getParameter("slUOM");
		
		//Get the Conversion
		model.mdlProductUom mdlProductUom = new model.mdlProductUom();
		mdlProductUom = ProductUomAdapter.LoadProductUOMByKey(txtProductID, slUOM);
		if(mdlProductUom.getBaseUOM() == null || mdlProductUom.getBaseUOM().contentEquals("")){
			lResult = "Satuan terkecil untuk produk "+txtProductID+" belum didefinisikan";
			Globals.gCondition = "FailedConversion";
			Globals.gConditionDesc = lResult;
		}
		
		if(!lResult.contains("Satuan terkecil untuk")){
			String txtQtyBaseUOM = String.valueOf(Integer.parseInt(mdlProductUom.getQty())*Integer.parseInt(txtQtyUOM));
			//String txtQtyBaseUOM = request.getParameter("txtQtyBaseUOM");
			String txtBaseUOM = mdlProductUom.getBaseUOM();
			
			String txtVendorBatchNo = request.getParameter("txtVendorBatchNo");
			String txtPackingNo = request.getParameter("txtPackingNo");
			String txtBatchNo = request.getParameter("txtBatchNo");
			
			String txtExpiredDate="";
			try
			{
				String txtDate1 = request.getParameter("txtExpiredDate");
				Date initDate = new SimpleDateFormat("dd MMM yyyy").parse(txtDate1);
			    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			    txtExpiredDate = formatter.format(initDate);
			}
			catch(Exception ex)
			{
			}
			
			//String txtDocNumberInboundDetail = request.getParameter("txtDocNumberInboundDetail");
			//Declare mdlInboundDetail,mdlBatch and mdlStockSummary for global
			
			model.mdlInbound mdlInboundDetail = new model.mdlInbound();
			mdlInboundDetail.setDocNumber(txtDocNumber);
			mdlInboundDetail.setDocLine(txtDocLine);
			mdlInboundDetail.setProductID(txtProductID);
			mdlInboundDetail.setQtyUom(txtQtyUOM);
			mdlInboundDetail.setUom(slUOM);
			mdlInboundDetail.setQtyBaseUom(txtQtyBaseUOM);
			mdlInboundDetail.setBaseUom(txtBaseUOM);
			mdlInboundDetail.setVendorBatchNo(txtVendorBatchNo);
			mdlInboundDetail.setPackingNo(txtPackingNo);
			mdlInboundDetail.setExpiredDate(txtExpiredDate);
			mdlInboundDetail.setWMStatus(inboundWMStatus);
			mdlInboundDetail.setUpdateWMFirst(inboundUpdateWMFirst);
			
			model.mdlBatch mdlBatch = new model.mdlBatch();
			mdlBatch.setProductID(txtProductID);
			mdlBatch.setPacking_No(txtPackingNo);
			mdlBatch.setGRDate(docDate);
			mdlBatch.setExpired_Date(txtExpiredDate);
			mdlBatch.setVendor_Batch_No(txtVendorBatchNo);
			mdlBatch.setVendorID(vendorId);
			
			//Check batch table to generate
			//			String txtBatchNo = "";
			//			model.mdlBatch CheckBatch = BatchAdapter.LoadBatchbyParam(mdlBatch, Globals.user);
			//			if (CheckBatch.Batch_No == null)
			//			{
			//				txtBatchNo = BatchAdapter.GenerateBatch(Globals.user);
			//			}
			//			else
			//			{
			//				txtBatchNo = CheckBatch.getBatch_No();
			//			}
			if(txtBatchNo.contentEquals("") || txtBatchNo == null)
				txtBatchNo = BatchAdapter.GenerateBatch(Globals.user);
			
			if(txtBatchNo.contentEquals("") || txtBatchNo == null){
				lResult = "Gagal mengeluarkan nomor batch";
				Globals.gCondition = "FailedGenerateBatch";
				Globals.gConditionDesc = lResult;
			}
			else{
				//put batch_no into the model
				mdlBatch.setBatch_No(txtBatchNo);
				mdlInboundDetail.setBatchNo(txtBatchNo);
				
				model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
				
				//mdlStockSummary.setPeriod(LocalDateTime.now().toString().substring(0, 7).replace("-", ""));
				mdlStockSummary.setPeriod(docDate.substring(0, 7).replace("-", ""));
				mdlStockSummary.setProductID(txtProductID);
				mdlStockSummary.setPlantID(plantId);
				mdlStockSummary.setWarehouseID(warehouseId);
				mdlStockSummary.setBatch_No(txtBatchNo);
				mdlStockSummary.setQty(txtQtyBaseUOM);
				mdlStockSummary.setUOM(txtBaseUOM);
				
				//Check Product Last Stock Summary Quantity by period,product,plant,warehouse and batch no
				model.mdlStockSummary CheckStockSummary = StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,Globals.user);
				//end of check product last stock summary quantity
				
				if (keyBtn.equals("save")){
					lResult = InboundAdapter.TransactionInbound(mdlInboundDetail,mdlBatch,mdlStockSummary,CheckStockSummary.Qty,Globals.user);
				
					if(lResult.contains("Success Insert Inbound Detail")) {  
						Globals.gCondition = "SuccessInsertInboundDetail";
			        }  
			        else {
			        	Globals.gCondition = "FailedInsertInboundDetail"; 
			        	Globals.gConditionDesc = lResult;
			        }
				}
			}
		}
			
		Globals.gtemp_Param1 = Globals.gParam1;
		Globals.gtemp_Param2 = docDate;
		Globals.gtemp_Param3 = vendorId;
		Globals.gtemp_Param4 = plantId;
		Globals.gtemp_Param5 = warehouseId;
		Globals.gtemp_Param3 = Globals.gVendorId;
		Globals.gtemp_Inbound_WMStatus = inboundWMStatus;
		Globals.gtemp_Inbound_UpdateWMFirst = inboundUpdateWMFirst;
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
		
//		if (keyBtn.equals("update")){
//		    String lResult = InboundAdapter.UpdateInbound(mdlInbound,Globals.user);	
//		    
//		    if(lResult.contains("Success Update Inbound")) {  
//		    	Globals.gCondition = "SuccessUpdate";
//            }  
//            else {
//            	Globals.gCondition = "FailedUpdate"; 
//            }
//			return;
//		}
		
//		if (btnDelete != null){
//			String lResult = InboundAdapter.DeleteInbound(temp_txtDocNumber,Globals.user);
//			if(lResult.contains("Success Delete Inbound")) {  
//				Globals.gCondition =  "SuccessDelete";
//            }  
//            else {
//            	Globals.gCondition = "FailedDelete"; 
//            }
//			doGet(request, response);
//			return;
//		}
    }
    
}
