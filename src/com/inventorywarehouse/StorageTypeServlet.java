package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.PlantAdapter;
import adapter.StorageTypeAdapter;
import adapter.WMNumberRangesAdapter;
import adapter.WarehouseAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/storagetype"} , name="storagetype")
public class StorageTypeServlet extends HttpServlet{

private static final long serialVersionUID = 1L; 
    
    public StorageTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Boolean CheckMenu;
    	String MenuURL = "storagetype";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);
    		
    		model.mdlPlant mdlPlantFirst = listPlant.get(0);
    		if(Globals.gPlantID.isEmpty()){
    			Globals.gPlantID = mdlPlantFirst.PlantID;
    		}
    		request.setAttribute("tempPlantID", Globals.gPlantID);
    		
			//List<model.mdlWarehouse> mdlWarehouseList = new ArrayList<model.mdlWarehouse>();
			//mdlWarehouseList.addAll(WarehouseAdapter.LoadWarehouseJoin(Globals.user));
			//request.setAttribute("listwarehouse", mdlWarehouseList);
    		
    		List<model.mdlStorageType> listStorageType = new ArrayList<model.mdlStorageType>();
    		listStorageType.addAll(StorageTypeAdapter.LoadStorageType());
    		request.setAttribute("listStorageType", listStorageType);
    		
    		List<model.mdlPutawayStrategy> listPutawayStrategy = new ArrayList<model.mdlPutawayStrategy>();
    		listPutawayStrategy.addAll(StorageTypeAdapter.LoadPutawayStrategy());
    		request.setAttribute("listPutawayStrategy", listPutawayStrategy);
    		
    		List<model.mdlPickupStrategy> listPickupStrategy = new ArrayList<model.mdlPickupStrategy>();
    		listPickupStrategy.addAll(StorageTypeAdapter.LoadPickupStrategy());
    		request.setAttribute("listPickupStrategy", listPickupStrategy);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/storage_type.jsp");
    		dispacther.forward(request, response);
    		
    		Globals.gPlantID = "";
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//Declare Delete Button
    	String btnDelete = request.getParameter("btnDelete");
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}
    	
    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}
    	
    	//Declare Parameter
    	String lTempPlantID = request.getParameter("temp_PlantID");
    	String lTempWarehouseID = request.getParameter("temp_WarehouseID");
    	String lTempStorageTypeID = request.getParameter("temp_StorageTypeID");
    	
    	Globals.gPlantID = request.getParameter("slPlantId");
    	//String lPlantID = request.getParameter("txtPlantID");
    	String lWarehouseID = request.getParameter("txtWarehouseID");
    	String lStorageTypeID = request.getParameter("txtStorageTypeID");
    	String lStorageTypeName = request.getParameter("txtStorageTypeName");
    	
    	Boolean lSUmgmt = Boolean.valueOf(request.getParameter("cbSUmgmt"));
    	Boolean lStorTypeIsIDPnt = Boolean.valueOf(request.getParameter("cbStorTypeIsIDPnt"));
    	Boolean lStorTypeIsPckPnt = Boolean.valueOf(request.getParameter("cbStorTypeIsPckPnt"));
    	
    	String lPutAwayStrategy = request.getParameter("txtPutAwayStrategy");
    	String lAssignedIDPointStorType = request.getParameter("txtAssignedIDPointStorType");
    	Boolean lStockplcmtreqconf = Boolean.valueOf(request.getParameter("cbStockplcmtreqconf"));
    	Boolean lDstbinchduringconf = Boolean.valueOf(request.getParameter("cbDstbinchduringconf"));
    	Boolean lMixedStorage = Boolean.valueOf(request.getParameter("cbMixedStorage"));
    	Boolean lAddntoStock = Boolean.valueOf(request.getParameter("cbAddntoStock"));
    	Boolean lRetainOverdeliveries = Boolean.valueOf(request.getParameter("cbRetainOverdeliveries"));
    	Boolean lSUTCheckActive = Boolean.valueOf(request.getParameter("cbSUTCheckActive"));
    	Boolean lStorageSecCheckActive = Boolean.valueOf(request.getParameter("cbStorageSecCheckActive"));
    	Boolean lBlockUponStockPlcmt = Boolean.valueOf(request.getParameter("cbBlockUponStockPlcmt"));
    	
    	String lStockRemovalStrategy = request.getParameter("txtStockRemovalStrategy");
    	String lAssignedPickPointStorType = request.getParameter("txtAssignedPickPointStorType");
    	String lReturnStorageType = request.getParameter("txtReturnStorageType");
    	Boolean lStockRmvlReqConf = Boolean.valueOf(request.getParameter("cbStockRmvlReqConf"));
    	Boolean lAllowNegativeStock = Boolean.valueOf(request.getParameter("cbAllowNegativeStock"));
    	Boolean lFullStkRmvlReqAct = Boolean.valueOf(request.getParameter("cbFullStkRmvlReqAct"));
    	Boolean lReturnStockToSameStorageBin = Boolean.valueOf(request.getParameter("cbReturnStockToSameStorageBin"));
    	Boolean lExecuteZeroStockCheck = Boolean.valueOf(request.getParameter("cbExecuteZeroStockCheck"));
    	Boolean lRoundOffQty = Boolean.valueOf(request.getParameter("cbRoundOffQty"));
    	Boolean lBlockUponStockRemoval = Boolean.valueOf(request.getParameter("cbBlockUponStockRemoval"));
    	
    	//Declare mdlStorageType for global
    	model.mdlStorageType Param = new model.mdlStorageType();
    	Param.setPlantID(Globals.gPlantID);
    	Param.setWarehouseID(lWarehouseID);
    	Param.setStorageTypeID(lStorageTypeID);
    	Param.setStorageTypeName(lStorageTypeName);
    	
    	Param.setSU_mgmt_active(lSUmgmt);
    	Param.setStor_type_is_ID_pnt(lStorTypeIsIDPnt);
    	Param.setStor_type_is_pck_pnt(lStorTypeIsPckPnt);
    	
    	Param.setPutawayStrategy(lPutAwayStrategy);
    	Param.setAssigned_ID_point_stor_type(lAssignedIDPointStorType);
    	
    	Param.setStock_plcmt_req_confirmation(lStockplcmtreqconf);
    	Param.setDst_bin_ch_during_confirm(lDstbinchduringconf);
    	Param.setMixedStorage(lMixedStorage);
    	Param.setAddn_to_stock(lAddntoStock);
    	Param.setRetain_overdeliveries(lRetainOverdeliveries);
    	Param.setSUT_check_active(lSUTCheckActive);
    	Param.setStorage_sec_check_active(lStorageSecCheckActive);
    	Param.setBlock_upon_stock_plcmt(lBlockUponStockPlcmt);
    	
    	Param.setStockRemovalStrategy(lStockRemovalStrategy);
    	Param.setAssigned_pick_point_stor_ty(lAssignedPickPointStorType);
    	Param.setReturn_storage_type(lReturnStorageType);
    	Param.setStock_rmvl_req_confirmation(lStockRmvlReqConf);
    	Param.setAllow_negative_stock(lAllowNegativeStock);
    	Param.setFull_stk_rmvl_reqmt_act(lFullStkRmvlReqAct);
    	Param.setReturn_stock_to_same_storage_bin(lReturnStockToSameStorageBin);
    	Param.setExecute_zero_stock_check(lExecuteZeroStockCheck);
    	Param.setRound_off_qty(lRoundOffQty);
    	Param.setBlock_upon_stock_rmvl(lBlockUponStockRemoval);
    	
    	String lResult = "";
    	
    	if(keyBtn.equals("LoadNumberRanges")){
			return;
		}
    	
    	if (keyBtn.equals("save")){
			lResult = StorageTypeAdapter.InsertStorageType(Param);	
			if(lResult.contains("Success Insert Storage Type")) {  
				Globals.gCondition = "SuccessInsertStorageType";
            }  
            else {
            	Globals.gCondition = "FailedInsertStorageType";
            	Globals.gConditionDesc = lResult;
            }
		}
    	if (keyBtn.equals("update")){
		    lResult = StorageTypeAdapter.UpdateStorageType(Param);	
		    
		    if(lResult.contains("Success Update Storage Type")) {  
		    	Globals.gCondition = "SuccessUpdateStorageType";
            }  
            else {
            	Globals.gCondition = "FailedUpdateStorageType";
            	Globals.gConditionDesc = lResult;
            }
		}
    	if (btnDelete != null){
			lResult = StorageTypeAdapter.DeleteStorageType(lTempPlantID, lTempWarehouseID, lTempStorageTypeID);
			if(lResult.contains("Success Delete Storage Type")) {  
				Globals.gCondition =  "SuccessDeleteStorageType";
            }  
            else {
            	Globals.gCondition = "FailedDeleteStorageType"; 
            	Globals.gConditionDesc = lResult;
            }
			
			doGet(request, response);
		}
    	
    	
    	response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
    }
    
}
