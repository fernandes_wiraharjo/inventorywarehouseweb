package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.ProductAdapter;
import adapter.ProductUomAdapter;
import adapter.UomAdapter;
import model.Globals;

@WebServlet("/productuom")
public class ProductUOMServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public ProductUOMServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	Boolean CheckMenu;
    	String MenuURL = "productuom";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlUom> listUOM = new ArrayList<model.mdlUom>();
    		listUOM = UomAdapter.LoadMasterUOM();
    		request.setAttribute("listuom", listUOM);
    		
    		List<model.mdlUom> listBaseUOM = new ArrayList<model.mdlUom>();
    		listBaseUOM = UomAdapter.LoadMasterBaseUOM();
    		request.setAttribute("listbaseuom", listBaseUOM);
    		
    		List<model.mdlProduct> mdlProductList = new ArrayList<model.mdlProduct>();
    		mdlProductList.addAll(ProductAdapter.LoadProduct());
    		request.setAttribute("listProduct", mdlProductList);
    		
    		List<model.mdlProductUom> mdlProductUomList = new ArrayList<model.mdlProductUom>();
    		mdlProductUomList.addAll(ProductUomAdapter.LoadProductUOM());
    		request.setAttribute("listproductuom", mdlProductUomList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/product_uom.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtProductID = request.getParameter("txtProductID");
		String txtUOM = request.getParameter("txtUOM");
		String txtBaseUOM = request.getParameter("txtBaseUOM");
		String txtQty = request.getParameter("txtQty");
		
		//Declare mdlUOM for global
		model.mdlProductUom mdlProductUOM = new model.mdlProductUom();
		mdlProductUOM.setProductID(txtProductID);
		mdlProductUOM.setUOM(txtUOM);
		mdlProductUOM.setBaseUOM(txtBaseUOM);
		mdlProductUOM.setQty(txtQty);
			
		String lResult = "";
		if (keyBtn.equals("save")){
			lResult = ProductUomAdapter.InsertProductUom(mdlProductUOM);
				
			if(lResult.contains("Success Insert Product UOM")) {  
				Globals.gCondition = "SuccessInsertProductUOM";
            }  
            else {
            	Globals.gCondition = "FailedInsertProductUOM";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		if (keyBtn.equals("update")){
		   lResult = ProductUomAdapter.UpdateProductUom(mdlProductUOM);
		    
		    if(lResult.contains("Success Update Product UOM")) {  
		    	Globals.gCondition = "SuccessUpdateProductUOM";
            }  
            else {
            	Globals.gCondition = "FailedUpdateProductUOM";
            	Globals.gConditionDesc = lResult;
            }
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
        return;
	}
    
}
