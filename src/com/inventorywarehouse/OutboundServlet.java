package com.inventorywarehouse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.ClosingPeriodAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.OutboundAdapter;
import adapter.TraceCodeAdapter;
import adapter.VendorAdapter;
import model.Globals;
import java.util.Date;

/** Documentation
 * 001 Nanda
 */
@WebServlet(urlPatterns={"/Outbound"} , name="Outbound")
public class OutboundServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public OutboundServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String docNo = "";
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Boolean CheckMenu;
    	String MenuURL = "Outbound";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		docNo = OutboundAdapter.GenerateDocNoOutbound(); //001 Nanda
    		request.setAttribute("tempdocNo", docNo);  //001 Nanda
    		
    		List<model.mdlOutbound> mdlOutboundList = new ArrayList<model.mdlOutbound>();
    		mdlOutboundList.addAll(OutboundAdapter.LoadOutbound(Globals.user));
    		request.setAttribute("listoutbound", mdlOutboundList);
    		
    		List<model.mdlCustomer> listCustomer = new ArrayList<model.mdlCustomer>();
    		listCustomer.addAll(adapter.CustomerAdapter.LoadCustomer(Globals.user));
    		request.setAttribute("listCustomer", listCustomer);
    		
    		List<model.mdlPlant> listPlant = new ArrayList<model.mdlPlant>();
    		listPlant.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlant);
    		
			//    		List<model.mdlWarehouse> listWarehouse = new ArrayList<model.mdlWarehouse>();
			//    		listWarehouse.addAll(adapter.WarehouseAdapter.LoadWarehouseExceptECM(Globals.user));
			//    		request.setAttribute("listWarehouse", listWarehouse);		
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		//request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther;
    		if(Globals.gCondition == "SuccessInsertOutbound")
    		{
    			dispacther = request.getRequestDispatcher("/OutboundDetail?docNo="+Globals.gtemp_Param1+"&docDate="+Globals.gtemp_Param2+"&PlantID="+Globals.gtemp_Param3+"&WarehouseID="+Globals.gtemp_Param4+"");
    		}
    		else
    		{
    			dispacther = request.getRequestDispatcher("/mainform/pages/outbound.jsp");
    		}
    		
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
			
		String btnDelete = request.getParameter("btnDelete");
				
		//Declare TextBox
		String txtDocNumber = request.getParameter("txtDocNumber");
		
		String txtDate1 = request.getParameter("txtDate");
		String newDate = helper.ConvertDateTimeHelper.formatDate(txtDate1, "dd MMM yyyy", "yyyy-MM-dd");
		String txtDate = newDate;
		
		String txtCustomerID = request.getParameter("txtCustomerID");
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");				
		String temp_txtDocNumber = request.getParameter("temp_txtDocNumber");
				
		//Declare mdlVendor for global
		model.mdlOutbound mdlOutbound = new model.mdlOutbound();
		mdlOutbound.setDocNumber(txtDocNumber);
		mdlOutbound.setDate(txtDate);
		mdlOutbound.setCustomerID(txtCustomerID);
		mdlOutbound.setPlantID(txtPlantID);
		mdlOutbound.setWarehouseID(txtWarehouseID);	
			
		String lResult = "";
		String lCommand = "";
		if (keyBtn.equals("save")){
			
			lCommand = "Insert Outbound Doc : " + mdlOutbound.getDocNumber();
			
			model.mdlClosingPeriod CheckLastPeriod = ClosingPeriodAdapter.LoadActivePeriod(Globals.user);
			String sLastPeriod = CheckLastPeriod.getPeriod();
			
			if(sLastPeriod == null || sLastPeriod == "")
			{	
				LogAdapter.InsertLogExc("Period is not set", "InsertOutbound", lCommand , Globals.user);
				//lResult = "Error Insert Outbound";
				lResult = "Periode belum ditentukan";
			}
			else
			{
				try{
					DateFormat dfPeriod = new SimpleDateFormat("yyyy-MM");
					Date LastPeriod_On = dfPeriod.parse(sLastPeriod.substring(0,4)+"-"+sLastPeriod.substring(4,6));
					Date OutboundDate = dfPeriod.parse(mdlOutbound.getDate().substring(0, 7));
					
					if(LastPeriod_On.equals(OutboundDate))
					{
						lResult = OutboundAdapter.InsertOutbound(mdlOutbound,Globals.user);	
					}
					else
					{
						LogAdapter.InsertLogExc("Your input date period is not activated", "InsertOutbound", lCommand , Globals.user);
						//lResult = "Error Insert Outbound";
						lResult = "Tanggal yang Anda masukkan tidak diizinkan karena tidak sesuai dengan periode yang aktif";
					}
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertOutbound", lCommand , Globals.user);
					lResult = "Terjadi kesalahan pada sistem saat proses penguraian tanggal";
				}
			}
			
			if(lResult.contains("Success Insert Outbound")) {  
				Globals.gCondition = "SuccessInsertOutbound";
				TraceCodeAdapter.InsertTraceCode(txtDocNumber, LocalDateTime.now().toString().replace("T"," ") ,"Outbound"); //001 Nanda
            }  
            else {
            	Globals.gCondition = "FailedInsertOutbound"; 
            	Globals.gConditionDesc = lResult;
            }
		}
		
		Globals.gtemp_Param1 = txtDocNumber;
		Globals.gtemp_Param2 = txtDate1;
		Globals.gtemp_Param3 = txtPlantID;
		Globals.gtemp_Param4 = txtWarehouseID;
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		
		return;
				
//   		if (keyBtn.equals("update")){
// 		    String lResult = OutboundAdapter.UpdateOutbound(mdlOutbound,Globals.user);	
//				    
//		    if(lResult.contains("Success Update Outbound")) {  //001 Nanda
//		    	Globals.gCondition = "SuccessUpdate";
//            }  
//	    	else {
//            	Globals.gCondition = "FailedUpdate"; 
//            } 
//		    return;
//		}
//				
//		if (btnDelete != null){
//			String lResult = OutboundAdapter.DeleteOutbound(temp_txtDocNumber);
//			if(lResult.contains("Success Delete Outbound")) {  //001 Nanda
//				Globals.gCondition =  "SuccessDelete";
//            }  
//            else {
//            	Globals.gCondition = "FailedDelete"; 
//            }	
//			doGet(request, response);		
//			return;
//		}	
	}
}
