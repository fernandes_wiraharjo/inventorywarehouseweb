package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.MenuAdapter;
import adapter.StorageTypeIndicatorAdapter;
import adapter.WarehouseAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/storagetypeindicator"} , name="storagetypeindicator")
public class StorageTypeIndicatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public StorageTypeIndicatorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Boolean CheckMenu;
    	String MenuURL = "storagetypeindicator";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> listPlantList = new ArrayList<model.mdlPlant>();
    		listPlantList.addAll(adapter.PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", listPlantList);

    		List<model.mdlStorageTypeIndicator> mdlStorageTypeIndicatorList = new ArrayList<model.mdlStorageTypeIndicator>();
    		mdlStorageTypeIndicatorList.addAll(StorageTypeIndicatorAdapter.LoadStorageTypeIndicator());
    		request.setAttribute("listStorageTypeIndicator", mdlStorageTypeIndicatorList);

    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);

    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/storage_type_indicator.jsp");
    		dispacther.forward(request, response);
    	}

		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String btnDelete = request.getParameter("btnDelete");

		if (Globals.user == null || Globals.user == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}

		//Declare TextBox
		String txtPlantID = request.getParameter("txtPlantID");
		String txtWarehouseID = request.getParameter("txtWarehouseID");
		String txtStorageTypeIndicatorID = request.getParameter("txtStorageTypeIndicatorID");
		String txtStorageTypeIndicatorName = request.getParameter("txtStorageTypeIndicatorName");
		String temp_txtPlantID = request.getParameter("temp_txtPlantID");
		String temp_txtWarehouseID = request.getParameter("temp_txtWarehouseID");
		String temp_txtStorageTypeIndicatorID = request.getParameter("temp_txtStorageTypeIndicatorID");

		//Declare mdlWarehouse for global
		model.mdlStorageTypeIndicator mdlStorageTypeIndicator = new model.mdlStorageTypeIndicator();
		mdlStorageTypeIndicator.setPlantID(txtPlantID);
		mdlStorageTypeIndicator.setWarehouseID(txtWarehouseID);
		mdlStorageTypeIndicator.setStorageTypeIndicatorID(txtStorageTypeIndicatorID);
		mdlStorageTypeIndicator.setStorageTypeIndicatorName(txtStorageTypeIndicatorName);


		if (keyBtn.equals("save")){
			String lResult = StorageTypeIndicatorAdapter.InsertStorageTypeIndicator(mdlStorageTypeIndicator);
			if(lResult.contains("Success Insert Storage Type Indicator")) {
				Globals.gCondition = "SuccessInsertStorageTypeIndicator";
            }
            else {
            	Globals.gCondition = "FailedInsertStorageTypeIndicator";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = StorageTypeIndicatorAdapter.UpdateStorageTypeIndicator(mdlStorageTypeIndicator);

		    if(lResult.contains("Success Update Storage Type Indicator")) {
		    	Globals.gCondition = "SuccessUpdateStorageTypeIndicator";
            }
            else {
            	Globals.gCondition = "FailedUpdateStorageTypeIndicator";
            	Globals.gConditionDesc = lResult;
            }
		}

		if (btnDelete != null){
			String lResult = StorageTypeIndicatorAdapter.DeleteStorageTypeIndicator(temp_txtPlantID, temp_txtWarehouseID, temp_txtStorageTypeIndicatorID);
			if(lResult.contains("Success Delete Storage Type Indicator")) {
				Globals.gCondition =  "SuccessDeleteStorageTypeIndicator";
            }
            else {
            	Globals.gCondition = "FailedDeleteStorageTypeIndicator";
            	Globals.gConditionDesc = lResult;
            }

			doGet(request, response);
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
	}
}
