package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.BomAdapter;
import adapter.CancelTransactionAdapter;
import adapter.ConfirmationAdapter;
import adapter.LogAdapter;
import adapter.ProductUomAdapter;
import model.Globals;
import model.mdlBOMProductionOrderDetail;

@WebServlet(urlPatterns={"/getConfirmationDetail"} , name="getConfirmationDetail")
public class GetConfirmationDetailServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetConfirmationDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
		String lConfirmID = request.getParameter("confirmid");
		String lConfirmNo = request.getParameter("confirmno");
    	
    	List<model.mdlBOMConfirmationProductionDetail> ConfirmationDetailList = new ArrayList<model.mdlBOMConfirmationProductionDetail>();
		ConfirmationDetailList.addAll(ConfirmationAdapter.LoadBOMConfirmationDetail(lConfirmID, lConfirmNo));
		request.setAttribute("listConfirmationDetail", ConfirmationDetailList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getConfirmationDetail.jsp");
		dispacther.forward(request, response);
		
	}
}
