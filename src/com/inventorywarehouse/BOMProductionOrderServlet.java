package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.log.Log;

import adapter.BomAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.PlantAdapter;
import adapter.ProductAdapter;
import adapter.ProductUomAdapter;
import adapter.StockSummaryAdapter;
import adapter.StockTransferAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlBOMDetail;

@WebServlet(urlPatterns={"/BomProductionOrder"} , name="BomProductionOrder")
public class BOMProductionOrderServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    
    public BOMProductionOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	Boolean CheckMenu;
    	String MenuURL = "BomProductionOrder";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, Globals.user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPlant> mdlPlantList = new ArrayList<model.mdlPlant>();
    		mdlPlantList.addAll(PlantAdapter.LoadPlantByRole());
    		request.setAttribute("listPlant", mdlPlantList);
    		
    		List<model.mdlBomOrderType> mdlOrderTypeList = new ArrayList<model.mdlBomOrderType>();
    		mdlOrderTypeList.addAll(BomAdapter.LoadBOMOrderType());
    		request.setAttribute("listOrderType", mdlOrderTypeList); 
    		
    		List<model.mdlProduct> mdlProductList = new ArrayList<model.mdlProduct>();
    		mdlProductList.addAll(ProductAdapter.LoadBOMProduct());
    		request.setAttribute("listBOMProduct", mdlProductList);
    		
    		List<model.mdlBOMProductionOrder> mdlBOMProductionOrderList = new ArrayList<model.mdlBOMProductionOrder>();
    		mdlBOMProductionOrderList.addAll(BomAdapter.LoadBOMProductionOrder());
    		request.setAttribute("listBOMProductionOrder", mdlBOMProductionOrderList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, Globals.user);
    		
    		request.setAttribute("condition", Globals.gCondition);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("conditionDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther;
    		dispacther = request.getRequestDispatcher("/mainform/pages/bom_production_order.jsp");
    		dispacther.forward(request, response);
    	}
		
		Globals.gCondition = "";
		Globals.gConditionDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	if (Globals.user == null || Globals.user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtOrderTypeID = request.getParameter("txtOrderTypeID");
		String txtOrderNo = request.getParameter("txtOrderNo");
		String newDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtDate"), "dd MMM yyyy", "yyyy-MM-dd");
		String txtDate = newDate;
		String txtProductID = request.getParameter("txtProductID");
		String txtPlantID = request.getParameter("txtPlantID");
		String txtQty = request.getParameter("txtQty");
		String txtUOM="";
		
		txtUOM = BomAdapter.LoadBOMByKey(txtProductID, txtDate, txtPlantID).getUOM();
		if(txtUOM == null){
			Globals.gCondition = "FailedLoadBOM";
			Globals.gConditionDesc = "Produk bom yang ingin Anda produksi tidak ditemukan, silahkan buat produk bom terlebih dahulu";
		}
		else{
			//Get the Conversion
			model.mdlProductUom mdlProductUom = new model.mdlProductUom();
			mdlProductUom = ProductUomAdapter.LoadProductUOMByKey(txtProductID, txtUOM);
			if(mdlProductUom.getBaseUOM() == null || mdlProductUom.getBaseUOM().contentEquals("")){
				Globals.gCondition = "FailedConversion";
				Globals.gConditionDesc = "Satuan terkecil untuk produk "+txtProductID+" belum didefinisikan";
			}
			else{
				String txtQtyBaseUOM = String.valueOf(Integer.parseInt(mdlProductUom.getQty())*Integer.parseInt(txtQty));
				String txtBaseUOM = mdlProductUom.getBaseUOM();
				
				String txtWarehouseID = request.getParameter("txtWarehouseID");
				String txtStatus = "PLANNED";
				//String txt_tempStatus = request.getParameter("txt_tempStatus");
				
				//Declare mdlBOMProductionOrder for global
				model.mdlBOMProductionOrder mdlBomProductionOrder = new model.mdlBOMProductionOrder();
				
				mdlBomProductionOrder.setOrderTypeID(txtOrderTypeID);
				mdlBomProductionOrder.setOrderNo(txtOrderNo);
				mdlBomProductionOrder.setProductID(txtProductID);
				mdlBomProductionOrder.setPlantID(txtPlantID);
				mdlBomProductionOrder.setQty(txtQty);
				mdlBomProductionOrder.setUOM(txtUOM);
				mdlBomProductionOrder.setQtyBaseUOM(txtQtyBaseUOM);
				mdlBomProductionOrder.setBaseUOM(txtBaseUOM);
				mdlBomProductionOrder.setStartDate(txtDate);
				mdlBomProductionOrder.setWarehouseID(txtWarehouseID);
				mdlBomProductionOrder.setStatus(txtStatus);
					
				String lResult = "";
				String lCommand = "";
				if (keyBtn.equals("save")){
					
					lCommand = "Insert BOM Production Order : " + txtOrderTypeID + " - " + txtOrderNo;
					
					  List<model.mdlBOMDetail> listmdlLoadBomDetail = BomAdapter.LoadBOMDetailForProduction(txtProductID,txtDate,txtPlantID);
							
					  if(listmdlLoadBomDetail.size() > 0 || !listmdlLoadBomDetail.isEmpty())
					  {
						  for(mdlBOMDetail lParamBomDetail : listmdlLoadBomDetail)
						  {
							//Get the Conversion
							Integer txtQtyBomDetail = (Integer.parseInt(lParamBomDetail.getQty())/lParamBomDetail.getQtyBomHeader())*Integer.parseInt(txtQty);
							 
							model.mdlProductUom mdlComponentUom = new model.mdlProductUom();
							mdlComponentUom = ProductUomAdapter.LoadProductUOMByKey(lParamBomDetail.getComponentID(), lParamBomDetail.getUOM());
							if(mdlComponentUom.getBaseUOM() == null || mdlComponentUom.getBaseUOM().contentEquals("")){
								lResult = "Satuan terkecil untuk komponen "+lParamBomDetail.getComponentID()+" belum didefinisikan";
								
								break;
							}
							
							Integer txtQtyBaseBomDetail = txtQtyBomDetail*Integer.parseInt(mdlComponentUom.getQty());
						  
							int ComponentStock = StockSummaryAdapter.LoadStockSummaryByProductForProduction(lParamBomDetail.getComponentID(), txtPlantID, txtWarehouseID, txtQtyBaseBomDetail);
							
							if( txtQtyBaseBomDetail > ComponentStock )
							{
								LogAdapter.InsertLogExc("The component is not available", "InsertBomProductionOrder", lCommand , Globals.user);
								lResult = "Stok untuk komponen : "+lParamBomDetail.getComponentID()+", plant : "+txtPlantID+", warehouse : "+txtWarehouseID+" tidak mencukupi";
								
								break;
							}
						  }
						  
						  if(lResult.contentEquals(""))
							  lResult = BomAdapter.TransactionInsertProductionOrder(mdlBomProductionOrder, listmdlLoadBomDetail);
					  }
					  else
					  {
						  LogAdapter.InsertLogExc("The component is not defined yet", "InsertBomProductionOrder", lCommand , Globals.user);
						  lResult = "Komponen yang dibutuhkan untuk produk bom bersangkutan tidak ditemukan/belum didefinisikan";
					  }
					
					if(lResult.contains("Success Insert BOM Production Order")) {  
						Globals.gCondition = "SuccessInsertProductionOrder";
		            }  
		            else {
		            	Globals.gCondition = "FailedInsertProductionOrder"; 
		            	Globals.gConditionDesc = lResult;
		            }
				}
				
				
				if (keyBtn.equals("update")){
					lCommand = "Update BOM Production Order : " + txtOrderTypeID + " - " + txtOrderNo;
					
					  List<model.mdlBOMDetail> listmdlLoadBomDetail = BomAdapter.LoadBOMDetailForProduction(txtProductID,txtDate,txtPlantID);
						
					  if(listmdlLoadBomDetail.size() > 0 || !listmdlLoadBomDetail.isEmpty())
					  {
						  for(mdlBOMDetail lParamBomDetail : listmdlLoadBomDetail)
						  {
							//Get the Conversion
							Integer txtQtyBomDetail = (Integer.parseInt(lParamBomDetail.getQty())/lParamBomDetail.getQtyBomHeader())*Integer.parseInt(txtQty);
								 
							model.mdlProductUom mdlComponentUom = new model.mdlProductUom();
							mdlComponentUom = ProductUomAdapter.LoadProductUOMByKey(lParamBomDetail.getComponentID(), lParamBomDetail.getUOM());
							if(mdlComponentUom.getBaseUOM() == null || mdlComponentUom.getBaseUOM().contentEquals("")){
								lResult = "Satuan terkecil untuk komponen "+lParamBomDetail.getComponentID()+" belum didefinisikan";
								
								break;
							}
							
							Integer txtQtyBaseBomDetail = txtQtyBomDetail*Integer.parseInt(mdlComponentUom.getQty());
							
							int ComponentStock = StockSummaryAdapter.LoadStockSummaryByProductForProduction(lParamBomDetail.getComponentID(), txtPlantID, txtWarehouseID, txtQtyBaseBomDetail);
							
							if( txtQtyBaseBomDetail > ComponentStock )
							{
								LogAdapter.InsertLogExc("The component is not available", "UpdateBomProductionOrder", lCommand , Globals.user);
								lResult = "Stok untuk komponen : "+lParamBomDetail.getComponentID()+", plant : "+txtPlantID+", warehouse : "+txtWarehouseID+" tidak mencukupi";
								
								break;
							}
						  }
						  
						  if(lResult.contentEquals(""))
							  lResult = BomAdapter.TransactionUpdateProductionOrder(mdlBomProductionOrder, listmdlLoadBomDetail);
					  }
					  else
					  {
						  LogAdapter.InsertLogExc("The component is not defined yet", "UpdateBomProductionOrder", lCommand , Globals.user);
						  lResult = "Komponen yang dibutuhkan untuk produk bom bersangkutan tidak ditemukan/belum didefinisikan";
					  }
				
					if(lResult.contains("Success Update BOM Production Order")) {  
						Globals.gCondition = "SuccessUpdateProductionOrder";
			        }  
			        else {
			        	Globals.gCondition = "FailedUpdateProductionOrder"; 
			        	Globals.gConditionDesc = lResult;
			        }
				}
			}
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
        
		return;
    }
    
}
