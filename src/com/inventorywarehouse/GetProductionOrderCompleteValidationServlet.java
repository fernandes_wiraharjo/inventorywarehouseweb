package com.inventorywarehouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import adapter.BomAdapter;
import adapter.ConfirmationAdapter;
import adapter.LogAdapter;
import jdk.nashorn.internal.runtime.arrays.ArrayData;
import model.Globals;

@WebServlet(urlPatterns={"/getProductionOrderCompleteValidation"} , name="getProductionOrderCompleteValidation")
public class GetProductionOrderCompleteValidationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L; 
	
	public GetProductionOrderCompleteValidationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String lOrderTypeID = request.getParameter("ordertypeid");
    	String lOrderNo = request.getParameter("orderno");
    	List<String> llistTargetProduction = new ArrayList<String>();
    	String Validate = "";
    	
    	//get the target of production order header
    	List<model.mdlBOMProductionOrder> LatestBOMProductionOrder = new ArrayList<model.mdlBOMProductionOrder>();
    	LatestBOMProductionOrder = BomAdapter.LoadBOMProductionOrderByKey(lOrderTypeID, lOrderNo);
    	
    	for(model.mdlBOMProductionOrder lParam : LatestBOMProductionOrder)
    	{
    		llistTargetProduction.add(lParam.getQty());
    	}
    	
    	//get the target of production order detail
    	List<model.mdlBOMProductionOrderDetail> LatestBOMProductionOrderDetail = new ArrayList<model.mdlBOMProductionOrderDetail>();
    	//close code nanda
		//LatestBOMProductionOrderDetail = ConfirmationAdapter.LoadBOMProductionOrderDetailforConfirmation(lOrderTypeID, lOrderNo);
    	
    	for(model.mdlBOMProductionOrderDetail lParamDetail : LatestBOMProductionOrderDetail)
    	{	
    		llistTargetProduction.add(lParamDetail.getQty());
    	}
    		
    	//get the list of target and check if it is complete or not
    	String[] stringArrayTargetProduction = llistTargetProduction.toArray(new String[0]);
    	
    	if (helper.CheckAllArrayValueatOnce.allMatch(stringArrayTargetProduction, "0")) {
    	    // do this
    		Validate = "yes";
    	}
    	else
    	{
    		Validate = "no";
    	}
    	
    	
    	response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Validate);
		
	}
	
}
