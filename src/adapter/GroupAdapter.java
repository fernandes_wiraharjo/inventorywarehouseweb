package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

public class GroupAdapter {
	public static List<model.mdlGroup> GetGroupAPI(String lUser){
	List<model.mdlGroup> listmdlGroup = new ArrayList<model.mdlGroup>();
	model.mdlToken mdlToken = TokenAdapter.GetToken();
	try {
		
		Client client = Client.create();
		
		Globals.gCommand = "http://dev.webarq.info:8081/kenmaster-be/api/group/listing";
		WebResource webResource = client.resource(Globals.gCommand);
		
		Gson gson = new Gson();
		String json = gson.toJson(mdlToken);
		
		ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);		
		
		Globals.gReturn_Status = response.getEntity(String.class);
		
		//Type TypeDepartment = new TypeToken<ArrayList<model.mdlDepartment>>(){}.getClass();
		listmdlGroup = gson.fromJson(Globals.gReturn_Status, model.mdlGrouplist.class);
		
		
	  } catch (Exception ex) {
		LogAdapter.InsertLogExc(ex.toString(), "GetGroupAPI", Globals.gCommand , lUser);
//		Globals.gReturn_Status = "Error GetToken";
	  }
	
	return listmdlGroup;
	}

	public static String InsertGrouplist(List<model.mdlGroup> lParamlist, String lUser)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			Gson gson = new Gson();
			Globals.gCommand = gson.toJson(lParamlist);
			
			String sql = "INSERT INTO `group`(ID, Department_ID, Code, Title_EN, Title_ID, Slug, `Order`, Status, Created_at, Updated_at) VALUES (?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE ID=ID";
			pstm = connection.prepareStatement(sql);
			
			for(model.mdlGroup lParam : lParamlist)
			{	
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getId()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getDepartment_id()));
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getCode()));
				pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getTitle_en()));
				pstm.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getTitle_id()));
				pstm.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getSlug()));
				pstm.setString(7,  ValidateNull.NulltoStringEmpty(lParam.getOrder()));
				pstm.setString(8,  ValidateNull.NulltoStringEmpty(lParam.getStatus()));
				pstm.setString(9, ValidateNull.NulltoStringEmpty(lParam.getCreated_at()));
				pstm.setString(10, ValidateNull.NulltoStringEmpty(lParam.getUpdated_at()));
				
				//pstm.executeUpdate();
				pstm.addBatch();
			}
			pstm.executeBatch();
			
			Globals.gReturn_Status = "Success Insert list Group API";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertGroupbyAPI", Globals.gCommand , lUser);
			Globals.gReturn_Status = "Error Insert list Group API";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertGroupbyAPI", "close opened connection protocol", lUser);
			}
		 }
		
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlGroup> LoadGroup() {
		List<model.mdlGroup> listmdlGroup = new ArrayList<model.mdlGroup>();
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT ID, Department_ID, Code, Title_EN, Title_ID, Slug, `Order`, Status, Created_at, Updated_at FROM `group`");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlGroup mdlGroup = new model.mdlGroup();
				mdlGroup.setId(jrs.getString("ID"));
				mdlGroup.setDepartment_id(jrs.getString("Department_ID"));
				mdlGroup.setCode(jrs.getString("Code"));
				mdlGroup.setTitle_en(jrs.getString("Title_EN"));
				mdlGroup.setTitle_id(jrs.getString("Title_ID"));
				mdlGroup.setSlug(jrs.getString("Slug"));	
				mdlGroup.setOrder(jrs.getString("Order"));
				mdlGroup.setStatus(jrs.getString("Status"));
				mdlGroup.setCreated_at(jrs.getString("Created_at"));
				mdlGroup.setUpdated_at(jrs.getString("Updated_at"));
				
				listmdlGroup.add(mdlGroup);
			}
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadGroup", Globals.gCommand , "");
		}

		return listmdlGroup;
	}

}
