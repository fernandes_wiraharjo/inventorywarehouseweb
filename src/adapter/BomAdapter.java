package adapter;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlBOM;
import model.mdlBOMConfirmationDocument;
import model.mdlBOMDetail;
import model.mdlBOMProductionOrder;
import model.mdlBomOrderType;
import model.mdlCancelTransaction;
import model.mdlInbound;
import model.mdlProductBOMDetail;

public class BomAdapter {

	public static List<model.mdlBomOrderType> LoadBOMOrderType(){
		List<model.mdlBomOrderType> listmdlBomOrderType = new ArrayList<model.mdlBomOrderType>();
		
		Connection connection = null;
		PreparedStatement pstmLoadBomOrderType = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBomOrderType = "SELECT OrderTypeID, `From` , `To`, OrderTypeDesc "
					+ "FROM bom_order_type";
			
			Globals.gCommand = sqlLoadBomOrderType;
			
			pstmLoadBomOrderType = connection.prepareStatement(sqlLoadBomOrderType);
			
			jrs = pstmLoadBomOrderType.executeQuery();
									
			while(jrs.next()){
				model.mdlBomOrderType mdlBomOrderType = new model.mdlBomOrderType();
				
				mdlBomOrderType.setOrderTypeID(jrs.getString("OrderTypeID"));
				mdlBomOrderType.setFrom(jrs.getString("From"));
				mdlBomOrderType.setTo(jrs.getString("To"));
				mdlBomOrderType.setOrderTypeDesc(jrs.getString("OrderTypeDesc"));
				
				listmdlBomOrderType.add(mdlBomOrderType);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBomOrderType", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstmLoadBomOrderType != null) {
					 pstmLoadBomOrderType.close();
				 }
	
				 if (connection != null) {
					 connection.close();
				 }
				 
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e)
			 {
				 LogAdapter.InsertLogExc(e.toString(), "LoadBomOrderType", "close opened connection protocol", Globals.user);
			 }
		 }

		return listmdlBomOrderType;
	}

	public static model.mdlBomOrderType LoadBOMOrderTypeByKey(String lOrderTypeID){
		model.mdlBomOrderType mdlBomOrderType = new model.mdlBomOrderType();
		
		Connection connection = null;
		PreparedStatement pstmLoadBomOrderType = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBomOrderType = "SELECT OrderTypeID, `From` , `To`, OrderTypeDesc "
					+ "FROM bom_order_type WHERE OrderTypeID=?";
			
			Globals.gCommand = sqlLoadBomOrderType;
			
			pstmLoadBomOrderType = connection.prepareStatement(sqlLoadBomOrderType);
			pstmLoadBomOrderType.setString(1, lOrderTypeID);
			pstmLoadBomOrderType.addBatch();
			
			jrs = pstmLoadBomOrderType.executeQuery();
									
			while(jrs.next()){
				mdlBomOrderType.setOrderTypeID(jrs.getString("OrderTypeID"));
				mdlBomOrderType.setFrom(jrs.getString("From"));
				mdlBomOrderType.setTo(jrs.getString("To"));
				mdlBomOrderType.setOrderTypeDesc(jrs.getString("OrderTypeDesc"));
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBomOrderTypeByKey", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstmLoadBomOrderType != null) {
					 pstmLoadBomOrderType.close();
				 }
	
				 if (connection != null) {
					 connection.close();
				 }
				 
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e)
			 {
				 LogAdapter.InsertLogExc(e.toString(), "LoadBomOrderTypeByKey", "close opened connection protocol", Globals.user);
			 }
		 }

		return mdlBomOrderType;
	}
	
	@SuppressWarnings("resource")
	public static String InsertBomOrderType(model.mdlBomOrderType lParam)
	{
		Globals.gCommand = "Insert Order Type : " + lParam.getOrderTypeID();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlBomOrderType CheckDuplicateOrderType = BomAdapter.LoadBOMOrderTypeByKey(lParam.getOrderTypeID());
			if(CheckDuplicateOrderType.getOrderTypeID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();			
				
				jrs.setCommand("SELECT OrderTypeID, `From`, `To`, OrderTypeDesc FROM bom_order_type LIMIT 1");
				jrs.execute();
				
				jrs.moveToInsertRow();
				jrs.updateString("OrderTypeID",lParam.getOrderTypeID());
				jrs.updateString("From",lParam.getFrom());
				jrs.updateString("To",lParam.getTo());
				jrs.updateString("OrderTypeDesc",lParam.getOrderTypeDesc());
				
				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Order Type";
			}
			//if duplicate
			else {
				Globals.gReturn_Status = "Tipe order produksi sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertBOMOrderType", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "InsertBOMOrderType", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String UpdateBomOrderType(model.mdlBomOrderType lParam)
	{
		Globals.gCommand = "Update Order Type : " + lParam.getOrderTypeID();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT OrderTypeID, `From`, `To`, OrderTypeDesc FROM bom_order_type WHERE OrderTypeID = ? LIMIT 1");
			jrs.setString(1,  lParam.getOrderTypeID());
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("From", lParam.getFrom());
			jrs.updateString("To", lParam.getTo());
			jrs.updateString("OrderTypeDesc", lParam.getOrderTypeDesc());
			jrs.updateRow();
				
			Globals.gReturn_Status = "Success Update Order Type";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateBOMOrderType", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "UpdateBOMOrderType", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeleteBomOrderType(String lOrderTypeID)
	{
		Globals.gCommand = "Delete Order Type : " + lOrderTypeID;
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT OrderTypeID, `From`, `To`, OrderTypeDesc FROM bom_order_type WHERE OrderTypeID = ? LIMIT 1");
			jrs.setString(1, lOrderTypeID);
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			Globals.gReturn_Status = "Success Delete Order Type";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteBOMOrderType", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "DeleteBOMOrderType", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlBOMConfirmationDocument> LoadBOMConfirmationDocument() {
		List<model.mdlBOMConfirmationDocument> listmdlBomConfirmationDocument = new ArrayList<model.mdlBOMConfirmationDocument>();
		
		Connection connection = null;
		PreparedStatement pstmLoadBomConfirmationDocument = null;
		ResultSet jrs = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBomConfirmationDocument = "SELECT ConfirmationID, `From` , `To`, ConfirmationDesc, ConfirmationType "
					+ "FROM bom_confirmation_document";
			
			Globals.gCommand = sqlLoadBomConfirmationDocument;
			
			pstmLoadBomConfirmationDocument = connection.prepareStatement(sqlLoadBomConfirmationDocument);
			
			jrs = pstmLoadBomConfirmationDocument.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMConfirmationDocument mdlBomConfirmationDocument = new model.mdlBOMConfirmationDocument();
				
				mdlBomConfirmationDocument.setConfirmationID(jrs.getString("ConfirmationID"));
				mdlBomConfirmationDocument.setRangeFrom(jrs.getString("From"));
				mdlBomConfirmationDocument.setRangeTo(jrs.getString("To"));
				mdlBomConfirmationDocument.setConfirmationDesc(jrs.getString("ConfirmationDesc"));
				mdlBomConfirmationDocument.setConfirmationType(jrs.getString("ConfirmationType"));
				
				listmdlBomConfirmationDocument.add(mdlBomConfirmationDocument);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBomConfirmationDocument", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstmLoadBomConfirmationDocument != null) {
					 pstmLoadBomConfirmationDocument.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e) {
				 LogAdapter.InsertLogExc(e.toString(), "LoadBomConfirmationDocument", "close opened connection protocol", Globals.user);
			 }
		 }

		return listmdlBomConfirmationDocument;
	}

	public static model.mdlBOMConfirmationDocument LoadBOMConfirmationDocumentByKey(String lConfirmationID) {
		model.mdlBOMConfirmationDocument mdlBomConfirmationDocument = new model.mdlBOMConfirmationDocument();
		
		Connection connection = null;
		PreparedStatement pstmLoadBomConfirmationDocument = null;
		ResultSet jrs = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBomConfirmationDocument = "SELECT ConfirmationID, `From` , `To`, ConfirmationDesc, ConfirmationType "
					+ "FROM bom_confirmation_document WHERE ConfirmationID=? LIMIT 1";
			
			Globals.gCommand = sqlLoadBomConfirmationDocument;
			
			pstmLoadBomConfirmationDocument = connection.prepareStatement(sqlLoadBomConfirmationDocument);
			pstmLoadBomConfirmationDocument.setString(1, lConfirmationID);
			pstmLoadBomConfirmationDocument.addBatch();
			
			jrs = pstmLoadBomConfirmationDocument.executeQuery();
									
			while(jrs.next()){
				mdlBomConfirmationDocument.setConfirmationID(jrs.getString("ConfirmationID"));
				mdlBomConfirmationDocument.setRangeFrom(jrs.getString("From"));
				mdlBomConfirmationDocument.setRangeTo(jrs.getString("To"));
				mdlBomConfirmationDocument.setConfirmationDesc(jrs.getString("ConfirmationDesc"));
				mdlBomConfirmationDocument.setConfirmationType(jrs.getString("ConfirmationType"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBomConfirmationDocumentByKey", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstmLoadBomConfirmationDocument != null) {
					 pstmLoadBomConfirmationDocument.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e) {
				 LogAdapter.InsertLogExc(e.toString(), "LoadBomConfirmationDocumentByKey", "close opened connection protocol", Globals.user);
			 }
		 }

		return mdlBomConfirmationDocument;
	}
	
	public static List<model.mdlBOMConfirmationDocument> LoadBOMConfirmationDocumentByType(String lType) {
		List<model.mdlBOMConfirmationDocument> listmdlBomConfirmationDocument = new ArrayList<model.mdlBOMConfirmationDocument>();
		
		Connection connection = null;
		PreparedStatement pstmLoadBomConfirmationDocument = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBomConfirmationDocument = "SELECT ConfirmationID, `From` , `To`, ConfirmationDesc, ConfirmationType "
					+ "FROM bom_confirmation_document WHERE ConfirmationType=?";
			
			Globals.gCommand = sqlLoadBomConfirmationDocument;
			
			pstmLoadBomConfirmationDocument = connection.prepareStatement(sqlLoadBomConfirmationDocument);
			
			pstmLoadBomConfirmationDocument.setString(1, lType);
			pstmLoadBomConfirmationDocument.addBatch();
			
			jrs = pstmLoadBomConfirmationDocument.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMConfirmationDocument mdlBomConfirmationDocument = new model.mdlBOMConfirmationDocument();
				
				mdlBomConfirmationDocument.setConfirmationID(jrs.getString("ConfirmationID"));
				mdlBomConfirmationDocument.setRangeFrom(jrs.getString("From"));
				mdlBomConfirmationDocument.setRangeTo(jrs.getString("To"));
				mdlBomConfirmationDocument.setConfirmationDesc(jrs.getString("ConfirmationDesc"));
				mdlBomConfirmationDocument.setConfirmationType(jrs.getString("ConfirmationType"));
				
				listmdlBomConfirmationDocument.add(mdlBomConfirmationDocument);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBomConfirmationDocumentByType", Globals.gCommand, Globals.user);
		}
		finally {
			try{
				 if (pstmLoadBomConfirmationDocument != null) {
					 pstmLoadBomConfirmationDocument.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadBomConfirmationDocumentByType", "close opened connection protocol", Globals.user);
			}
		 }

		return listmdlBomConfirmationDocument;
	}
	
	@SuppressWarnings("resource")
	public static String InsertBomConfirmationDoc(model.mdlBOMConfirmationDocument lParam)
	{
		Globals.gCommand = "Insert Confirmation Document : " + lParam.getConfirmationID();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		
		try{
			mdlBOMConfirmationDocument CheckDuplicateConfirmationDoc = BomAdapter.LoadBOMConfirmationDocumentByKey(lParam.getConfirmationID());
			if(CheckDuplicateConfirmationDoc.getConfirmationID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();			
				
				jrs.setCommand("SELECT ConfirmationID, `From`, `To`, ConfirmationDesc, ConfirmationType FROM bom_confirmation_document LIMIT 1");
				jrs.execute();
				
				jrs.moveToInsertRow();
				jrs.updateString("ConfirmationID",lParam.getConfirmationID());
				jrs.updateString("From",lParam.getRangeFrom());
				jrs.updateString("To",lParam.getRangeTo());
				jrs.updateString("ConfirmationDesc",lParam.getConfirmationDesc());
				jrs.updateString("ConfirmationType",lParam.getConfirmationType());
				
				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Confirmation Document";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Dokumen konfirmasi sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertBOMConfirmationDoc", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "InsertBOMConfirmationDoc", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String UpdateBomConfirmationDoc(model.mdlBOMConfirmationDocument lParam)
	{
		Globals.gCommand = "Update Confirmation Document : " + lParam.getConfirmationID();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT ConfirmationID, `From`, `To`, ConfirmationDesc, ConfirmationType FROM bom_confirmation_document WHERE ConfirmationID = ? LIMIT 1");
			jrs.setString(1,  lParam.getConfirmationID());
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("From", lParam.getRangeFrom());
			jrs.updateString("To", lParam.getRangeTo());
			jrs.updateString("ConfirmationDesc", lParam.getConfirmationDesc());
			jrs.updateString("ConfirmationType", lParam.getConfirmationType());
			jrs.updateRow();
			
			Globals.gReturn_Status = "Success Update Confirmation Document";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateBOMConfirmationDoc", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "UpdateBOMConfirmationDoc", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeleteBomConfirmationDoc(String lConfirmationID)
	{
		Globals.gCommand = "Delete Confirmation Document : " + lConfirmationID;
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT ConfirmationID, `From`, `To`, ConfirmationDesc, ConfirmationType FROM bom_confirmation_document WHERE ConfirmationID = ? LIMIT 1");
			jrs.setString(1, lConfirmationID);
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			Globals.gReturn_Status = "Success Delete Confirmation Document";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteBOMConfirmationDoc", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "DeleteBOMConfirmationDoc", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlBOM> LoadBOM() {
		List<model.mdlBOM> listmdlBOM = new ArrayList<model.mdlBOM>();
		
		Connection connection = null;
		PreparedStatement pstmLoadBom = null;
		ResultSet jrs = null;
		Globals.gCommand="";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBom = "SELECT a.ProductID,b.Title_EN,a.ValidDate,a.PlantID,c.PlantName,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM bom a "
					+ "INNER JOIN product b ON b.ID = a.ProductID "
					+ "INNER JOIN plant c ON c.PlantID = a.PlantID "
					+ "WHERE a.PlantID IN ("+Globals.user_area+")";
			
			Globals.gCommand = sqlLoadBom;
			
			pstmLoadBom = connection.prepareStatement(sqlLoadBom);
			
			jrs = pstmLoadBom.executeQuery();
									
			while(jrs.next()){
				model.mdlBOM mdlBom = new model.mdlBOM();
				
				mdlBom.setProductID(jrs.getString("ProductID"));
				mdlBom.setProductName(jrs.getString("Title_EN"));
				
				String Date = jrs.getString("ValidDate");
				Date initDate = new SimpleDateFormat("yyyy-MM-dd").parse(Date);
			    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
			    String newDate = formatter.format(initDate);
				mdlBom.setValidDate(newDate);
				
				mdlBom.setPlantID(jrs.getString("PlantID"));
				mdlBom.setPlantName(jrs.getString("PlantName"));
				mdlBom.setQty(jrs.getString("Qty"));
				mdlBom.setUOM(jrs.getString("UOM"));
				mdlBom.setQtyBaseUOM(jrs.getString("QtyBaseUOM"));
				mdlBom.setBaseUOM(jrs.getString("BaseUOM"));
				
				listmdlBOM.add(mdlBom);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOM", Globals.gCommand , Globals.user);
		}
		finally {
			try {
				 if (pstmLoadBom != null) {
					 pstmLoadBom.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadBOM", "close opened connection protocol", Globals.user);
			}
		 }


		return listmdlBOM;
	}

	public static model.mdlBOM LoadBOMByKey(String lProductID, String lValidDate, String lPlantID) {
		model.mdlBOM mdlBom = new model.mdlBOM();
		
		Connection connection = null;
		PreparedStatement pstmLoadBom = null;
		ResultSet jrs = null;
		Globals.gCommand="";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBom = "SELECT a.ProductID,a.ValidDate,a.PlantID,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM bom a "
					+"WHERE a.ProductID=? AND a.ValidDate<=? AND a.PlantID=? ORDER BY a.ValidDate DESC LIMIT 1";
			
			Globals.gCommand = sqlLoadBom;
			
			pstmLoadBom = connection.prepareStatement(sqlLoadBom);
			
			pstmLoadBom.setString(1, lProductID);
			pstmLoadBom.setString(2, lValidDate);
			pstmLoadBom.setString(3, lPlantID);
			pstmLoadBom.addBatch();
			
			jrs = pstmLoadBom.executeQuery();
									
			while(jrs.next()){
				mdlBom.setProductID(jrs.getString("ProductID"));
				
				String Date = jrs.getString("ValidDate");
				Date initDate = new SimpleDateFormat("yyyy-MM-dd").parse(Date);
			    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
			    String newDate = formatter.format(initDate);
				mdlBom.setValidDate(newDate);
				
				mdlBom.setPlantID(jrs.getString("PlantID"));
				mdlBom.setQty(jrs.getString("Qty"));
				mdlBom.setUOM(jrs.getString("UOM"));
				mdlBom.setQtyBaseUOM(jrs.getString("QtyBaseUOM"));
				mdlBom.setBaseUOM(jrs.getString("BaseUOM"));
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOMByKey", Globals.gCommand , Globals.user);
		}
		finally {
			try {
				 if (pstmLoadBom != null) {
					 pstmLoadBom.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadBOMByKey", "close opened connection protocol", Globals.user);
			}
		 }

		return mdlBom;
	}
	
	public static List<model.mdlBOMDetail> LoadBOMDetail(String productid, String date, String plantid) {
		List<model.mdlBOMDetail> listmdlBOMDetail = new ArrayList<model.mdlBOMDetail>();
		
		Connection connection = null;
		PreparedStatement pstmLoadBomDetail = null;
		ResultSet jrs = null;
		Globals.gCommand="";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBomDetail = "SELECT a.ComponentLine,a.ComponentID,b.Title_EN,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM bom_detail a "
					+ "INNER JOIN product b ON b.ID = a.ComponentID "
					+ "WHERE ProductID=? AND ValidDate=? AND PlantID=? ORDER BY ComponentLine";
			
			pstmLoadBomDetail = connection.prepareStatement(sqlLoadBomDetail);
			
			pstmLoadBomDetail.setString(1, productid);
			pstmLoadBomDetail.setString(2, date);
			pstmLoadBomDetail.setString(3, plantid);
			pstmLoadBomDetail.addBatch();
			Globals.gCommand = pstmLoadBomDetail.toString();
			
			jrs = pstmLoadBomDetail.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMDetail mdlBomDetail = new model.mdlBOMDetail();
				
				mdlBomDetail.setComponentLine(jrs.getString("ComponentLine"));
				mdlBomDetail.setComponentID(jrs.getString("ComponentID"));
				mdlBomDetail.setComponentName(jrs.getString("Title_EN"));
				mdlBomDetail.setQty(jrs.getString("Qty"));
				mdlBomDetail.setUOM(jrs.getString("UOM"));
				mdlBomDetail.setQtyBaseUOM(jrs.getString("QtyBaseUOM"));
				mdlBomDetail.setBaseUOM(jrs.getString("BaseUOM"));
				
				listmdlBOMDetail.add(mdlBomDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOMDetail", Globals.gCommand , Globals.user);
		}
		finally {
			try {
				 if (pstmLoadBomDetail != null) {
					 pstmLoadBomDetail.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "LoadBOMDetail", "close opened connection protocol", Globals.user);
			}
		 }

		return listmdlBOMDetail;
	}

	public static List<model.mdlBOMDetail> LoadBOMDetailForProduction(String productid, String date, String plantid) {
		List<model.mdlBOMDetail> listmdlBOMDetail = new ArrayList<model.mdlBOMDetail>();
		
		Connection connection = null;
		PreparedStatement pstmLoadBomDetail = null;
		ResultSet jrs = null;
		Globals.gCommand="";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBomDetail = "SELECT a.ComponentLine,a.ComponentID,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM,"
					+ "b.Qty as QtyBomHeader,b.UOM as UOMBomHeader,b.QtyBaseUOM as QtyBaseUOMBomHeader, "
					+ "b.BaseUOM as BaseUOMBomHeader "
					+ "FROM bom_detail a "
					+ "INNER JOIN "
					+ "( "
					+ "SELECT ProductID,ValidDate,PlantID,Qty,UOM,QtyBaseUOM,BaseUOM "
					+ "FROM bom "
					+ "WHERE ProductID=? AND ValidDate<=? AND PlantID=? "
					+ "ORDER BY ValidDate DESC LIMIT 1 "
					+ ") b "
					+ "ON b.ProductID=a.ProductID AND b.ValidDate=a.ValidDate AND b.PlantID=a.PlantID "
					+ "ORDER BY a.ComponentLine";
			
			Globals.gCommand = sqlLoadBomDetail;
			
			pstmLoadBomDetail = connection.prepareStatement(sqlLoadBomDetail);
			
			pstmLoadBomDetail.setString(1, productid);
			pstmLoadBomDetail.setString(2, date);
			pstmLoadBomDetail.setString(3, plantid);
			pstmLoadBomDetail.addBatch();
			
			jrs = pstmLoadBomDetail.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMDetail mdlBomDetail = new model.mdlBOMDetail();
				
				mdlBomDetail.setComponentLine(jrs.getString("ComponentLine"));
				mdlBomDetail.setComponentID(jrs.getString("ComponentID"));
				mdlBomDetail.setQty(jrs.getString("Qty"));
				mdlBomDetail.setUOM(jrs.getString("UOM"));
				mdlBomDetail.setQtyBaseUOM(jrs.getString("QtyBaseUOM"));
				mdlBomDetail.setBaseUOM(jrs.getString("BaseUOM"));
				mdlBomDetail.setQtyBomHeader(Integer.parseInt(jrs.getString("QtyBomHeader")));
				mdlBomDetail.setUOMBomHeader(jrs.getString("UOMBomHeader"));
				mdlBomDetail.setQtyBaseUOMBomHeader(Integer.parseInt(jrs.getString("QtyBaseUOMBomHeader")));
				mdlBomDetail.setBaseUOMBomHeader(jrs.getString("BaseUOMBomHeader"));
				
				listmdlBOMDetail.add(mdlBomDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOMDetailForProduction", Globals.gCommand , Globals.user);
		}
		finally {
			try {
				 if (pstmLoadBomDetail != null) {
					 pstmLoadBomDetail.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadBOMDetailForProduction", "close opened connection protocol", Globals.user);
			}
		 }

		return listmdlBOMDetail;
	}
	
	public static String TransactionInsertBOM(mdlBOM lParamBom)
	{	
		Globals.gCommand = "";
		
			Connection connection = null;
			PreparedStatement pstmInsertBOM = null;
			
			try{
				mdlBOM CheckDuplicateBOM = LoadBOMByKey(lParamBom.getProductID(),lParamBom.getValidDate(),lParamBom.getPlantID());
				if(CheckDuplicateBOM.getProductID() == null){
					connection = database.RowSetAdapter.getConnection();
					
					connection.setAutoCommit(false);
					
					String sqlInsertBOM = "INSERT INTO bom(`ProductID`, `ValidDate`, `PlantID`, `Qty`, `UOM`, `QtyBaseUOM`, `BaseUOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
					pstmInsertBOM = connection.prepareStatement(sqlInsertBOM);
					
					//insert bom parameter
					pstmInsertBOM.setString(1,  ValidateNull.NulltoStringEmpty(lParamBom.getProductID()));
					pstmInsertBOM.setString(2,  ValidateNull.NulltoStringEmpty(lParamBom.getValidDate()));
					pstmInsertBOM.setString(3,  ValidateNull.NulltoStringEmpty(lParamBom.getPlantID()));
					pstmInsertBOM.setString(4,  ValidateNull.NulltoStringEmpty(lParamBom.getQty()));
					pstmInsertBOM.setString(5,  ValidateNull.NulltoStringEmpty(lParamBom.getUOM()));
					pstmInsertBOM.setString(6,  ValidateNull.NulltoStringEmpty(lParamBom.getQtyBaseUOM()));
					pstmInsertBOM.setString(7,  ValidateNull.NulltoStringEmpty(lParamBom.getBaseUOM())); 
					pstmInsertBOM.setString(8,  ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertBOM.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertBOM.setString(10, ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertBOM.setString(11, LocalDateTime.now().toString());
					Globals.gCommand = pstmInsertBOM.toString();
					pstmInsertBOM.executeUpdate();
						
					connection.commit(); //commit transaction if all of the proccess is running well
					
					Globals.gReturn_Status = "Success Insert Bill Of Materials";
				}
				//if duplicate
				else {
					Globals.gReturn_Status = "Produk bom sudah ada";
				}
			}
			catch (Exception e ) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionInsertBom", Globals.gCommand , Globals.user);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertBom", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try {
			        if(connection!=null){
			        	connection.setAutoCommit(true);
			        	connection.close();
			        }
			        if (pstmInsertBOM != null) {
			        	pstmInsertBOM.close();
			        }
				}
				catch(Exception e)
				{
					LogAdapter.InsertLogExc(e.toString(), "InsertBom", "close opened connection protocol", Globals.user);
				}
		    }
			
		return Globals.gReturn_Status;
	}

	public static String TransactionUpdateBOM(mdlBOM lParamBom)
	{	
		Globals.gCommand = "";
		
			Connection connection = null;
			PreparedStatement pstmUpdateBOM = null;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlUpdateBOM = "UPDATE bom SET Qty=?,UOM=?,QtyBaseUOM=?,BaseUOM=?,Updated_by=?,Updated_at=? WHERE ProductID=? AND ValidDate=? AND PlantID=?;";
				pstmUpdateBOM = connection.prepareStatement(sqlUpdateBOM);
				
				//update bom parameter
				pstmUpdateBOM.setString(1,  ValidateNull.NulltoStringEmpty(lParamBom.getQty()));
				pstmUpdateBOM.setString(2,  ValidateNull.NulltoStringEmpty(lParamBom.getUOM()));
				pstmUpdateBOM.setString(3,  ValidateNull.NulltoStringEmpty(lParamBom.getQtyBaseUOM()));
				pstmUpdateBOM.setString(4,  ValidateNull.NulltoStringEmpty(lParamBom.getBaseUOM()));
				pstmUpdateBOM.setString(5,  ValidateNull.NulltoStringEmpty(Globals.user));
				pstmUpdateBOM.setString(6,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmUpdateBOM.setString(7,  ValidateNull.NulltoStringEmpty(lParamBom.getProductID())); 
				pstmUpdateBOM.setString(8,  ValidateNull.NulltoStringEmpty(lParamBom.getValidDate()));
				pstmUpdateBOM.setString(9,  ValidateNull.NulltoStringEmpty(lParamBom.getPlantID()));
				Globals.gCommand = pstmUpdateBOM.toString();
				pstmUpdateBOM.executeUpdate();
					
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Update Bill Of Materials";
			}
			
			catch (Exception e ) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionUpdateBom", Globals.gCommand , Globals.user);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "UpdateBom", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try {
					connection.setAutoCommit(true);
					if(connection!=null)
						connection.close();
			        if (pstmUpdateBOM != null) {
			        	pstmUpdateBOM.close();
			        }
				}
				catch(Exception e) {
					LogAdapter.InsertLogExc(e.toString(), "UpdateBom", "close opened connection protocol", Globals.user);
				}
		    }
			
			return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String GenerateComponentLineBOMDetail(String ProductID,String Date,String PlantID)
	{
		String txtComponentLine ="";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
	
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
		
			jrs.setCommand("SELECT `ComponentLine` FROM `bom_detail` WHERE `ProductID` = ? AND `ValidDate` = ? AND `PlantID` = ? ORDER BY `Created_at` DESC LIMIT 1");
			jrs.setString(1, ProductID);
			jrs.setString(2, Date);
			jrs.setString(3, PlantID);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				txtComponentLine = ValidateNull.NulltoStringEmpty(jrs.getString("ComponentLine"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GenerateComponentLineBOMDetail", Globals.gCommand , Globals.user);
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "GenerateComponentLineBOMDetail", "close opened connection protocol", Globals.user);
			}
		}
	
		return txtComponentLine;
	}

	@SuppressWarnings("resource")
	public static String InsertBOMDetail(model.mdlBOMDetail lParam)
	{
		Globals.gCommand = "Insert Bom Detail Key : " + lParam.getProductID() + " " + lParam.getValidDate() + " " + lParam.getPlantID();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();			
			
			jrs.setCommand("SELECT ProductID, ValidDate, PlantID, ComponentLine, ComponentID,"
					+ "Qty, UOM, QtyBaseUOM, BaseUOM, Created_by, Created_at, Updated_by, Updated_at "
					+ "FROM bom_detail LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("ProductID",lParam.getProductID());
			jrs.updateString("ValidDate",lParam.getValidDate());
			jrs.updateString("PlantID",lParam.getPlantID());
			jrs.updateString("ComponentLine",lParam.getComponentLine());
			jrs.updateString("ComponentID",lParam.getComponentID());
			jrs.updateString("Qty",lParam.getQty());
			jrs.updateString("UOM",lParam.getUOM());
			jrs.updateString("QtyBaseUOM",lParam.getQtyBaseUOM());
			jrs.updateString("BaseUOM",lParam.getBaseUOM());
			jrs.updateString("Created_by",Globals.user);
			jrs.updateString("Created_at",LocalDateTime.now().toString());
			jrs.updateString("Updated_by",Globals.user);
			jrs.updateString("Updated_at",LocalDateTime.now().toString());
			
			jrs.insertRow();
			Globals.gReturn_Status = "Success Insert BOM Detail";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertBOMDetail", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertBOMDetail", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String UpdateBOMDetail(model.mdlBOMDetail lParam)
	{
		Globals.gCommand = "Update Bom Detail Key : " + lParam.getProductID() + " " + lParam.getValidDate() + " " + lParam.getPlantID();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT ProductID, ValidDate, PlantID, ComponentLine, ComponentID,"
					+ "Qty, UOM, QtyBaseUOM, BaseUOM, Created_by, Created_at, Updated_by, Updated_at "
					+ "FROM bom_detail WHERE ProductID=? AND ValidDate=? AND PlantID=? AND ComponentLine=? "
					+ "LIMIT 1");
			
			jrs.setString(1,  lParam.getProductID());
			jrs.setString(2,  lParam.getValidDate());
			jrs.setString(3,  lParam.getPlantID());
			jrs.setString(4,  lParam.getComponentLine());
			
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			
			jrs.updateString("ComponentID", lParam.getComponentID());
			jrs.updateString("Qty", lParam.getQty());
			jrs.updateString("UOM", lParam.getUOM());
			jrs.updateString("QtyBaseUOM", lParam.getQtyBaseUOM());
			jrs.updateString("BaseUOM", lParam.getBaseUOM());
			jrs.updateString("Updated_by", Globals.user);
			jrs.updateString("Updated_at", LocalDateTime.now().toString());
			
			jrs.updateRow();
				
			Globals.gReturn_Status = "Success Update BOM Detail";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateBOMDetail", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "UpdateBOMDetail", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlBOMProductionOrder> LoadBOMProductionOrder() {
		List<model.mdlBOMProductionOrder> listmdlBOMProductionOrder = new ArrayList<model.mdlBOMProductionOrder>();
		
		Connection connection = null;
		PreparedStatement pstmLoadProductionOrder = null;
		ResultSet jrs = null;
		Globals.gCommand="";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadProductionOrder = "SELECT a.OrderTypeID,b.OrderTypeDesc,a.OrderNo,a.ProductID,c.Title_EN,a.PlantID,d.PlantName,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM,a.StartDate,a.WarehouseID,e.WarehouseName,a.Status "
					+ "FROM bom_production_order a "
					+ "INNER JOIN bom_order_type b ON b.OrderTypeID = a.OrderTypeID "
					+ "INNER JOIN product c ON c.ID = a.ProductID "
					+ "INNER JOIN plant d ON d.PlantID = a.PlantID "
					+ "INNER JOIN warehouse e ON e.PlantID=a.PlantID AND e.WarehouseID = a.WarehouseID "
					+ "WHERE a.PlantID IN ("+Globals.user_area+")";
			
			Globals.gCommand = sqlLoadProductionOrder;
			
			pstmLoadProductionOrder = connection.prepareStatement(sqlLoadProductionOrder);
			
			jrs = pstmLoadProductionOrder.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMProductionOrder mdlBomProductionOrder = new model.mdlBOMProductionOrder();
				
				mdlBomProductionOrder.setOrderTypeID(jrs.getString("OrderTypeID"));
				mdlBomProductionOrder.setOrderTypeDesc(jrs.getString("OrderTypeDesc"));
				mdlBomProductionOrder.setOrderNo(jrs.getString("OrderNo"));
				mdlBomProductionOrder.setProductID(jrs.getString("ProductID"));
				mdlBomProductionOrder.setProductName(jrs.getString("Title_EN"));
				mdlBomProductionOrder.setPlantID(jrs.getString("PlantID"));
				mdlBomProductionOrder.setPlantName(jrs.getString("PlantName"));
				mdlBomProductionOrder.setQty(jrs.getString("Qty"));
				mdlBomProductionOrder.setUOM(jrs.getString("UOM"));
				mdlBomProductionOrder.setQtyBaseUOM(jrs.getString("QtyBaseUOM"));
				mdlBomProductionOrder.setBaseUOM(jrs.getString("BaseUOM"));
				
				String Date = jrs.getString("StartDate");
				Date initDate = new SimpleDateFormat("yyyy-MM-dd").parse(Date);
			    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
			    String newDate = formatter.format(initDate);
				mdlBomProductionOrder.setStartDate(newDate);
				
				mdlBomProductionOrder.setWarehouseID(jrs.getString("WarehouseID"));
				mdlBomProductionOrder.setWarehouseName(jrs.getString("WarehouseName"));
				mdlBomProductionOrder.setStatus(jrs.getString("Status"));
				
				if(jrs.getString("Status").contentEquals("PLANNED"))
					mdlBomProductionOrder.setButtonStatus("enabled");
				else
					mdlBomProductionOrder.setButtonStatus("disabled");
				
//				if(jrs.getString("Status").contentEquals("RELEASED"))
//					mdlBomProductionOrder.setButtonStatus("disabled");
//				else
//					mdlBomProductionOrder.setButtonStatus("enabled");
				if(jrs.getString("Status").contentEquals("CANCELLED"))
					mdlBomProductionOrder.setButtonCancelStatus("disabled");
				else
					mdlBomProductionOrder.setButtonCancelStatus("enabled");
				
				listmdlBOMProductionOrder.add(mdlBomProductionOrder);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOMProductionOrder", Globals.gCommand , Globals.user);
		}
		finally {
			try {
				 if (pstmLoadProductionOrder != null) {
					 pstmLoadProductionOrder.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadBOMProductionOrder", "close opened connection protocol", Globals.user);
			}
		 }

		return listmdlBOMProductionOrder;
	}

	public static List<model.mdlBOMProductionOrder> LoadBOMProductionOrderByKey(String lordertypeid, String lorderno) {
		List<model.mdlBOMProductionOrder> listmdlBOMProductionOrder = new ArrayList<model.mdlBOMProductionOrder>();
		int newConfirmationProductionQty = 0;
		
		Connection connection = null;
		PreparedStatement pstmLoadProductionOrder = null;
		PreparedStatement pstmLoadConfirmationProductionOrder = null;
		ResultSet jrs = null;
		ResultSet jrs2 = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			//load confirmation production order header proccess
			String sqlLoadConfirmationProductionOrder = "SELECT a.OrderTypeID,a.OrderNo,a.ProductID,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM bom_production_order_confirmation a "
					+ "WHERE a.OrderTypeID=? AND a.OrderNo=? AND a.IsCancel=0";
			
			pstmLoadConfirmationProductionOrder = connection.prepareStatement(sqlLoadConfirmationProductionOrder);
			
			pstmLoadConfirmationProductionOrder.setString(1, lordertypeid);
			pstmLoadConfirmationProductionOrder.setString(2, lorderno);
			pstmLoadConfirmationProductionOrder.addBatch();
			
			Globals.gCommand = pstmLoadConfirmationProductionOrder.toString();
			
			jrs2 = pstmLoadConfirmationProductionOrder.executeQuery();
									
			while(jrs2.next()){
				model.mdlBOMConfirmationProduction BomConfirmationProductionOrder = new model.mdlBOMConfirmationProduction();
				
				BomConfirmationProductionOrder.setOrderTypeID(jrs2.getString("OrderTypeID"));
				BomConfirmationProductionOrder.setProductName(jrs2.getString("OrderNo"));
				BomConfirmationProductionOrder.setProductID(jrs2.getString("ProductID"));
				BomConfirmationProductionOrder.setQty(jrs2.getString("Qty"));
				BomConfirmationProductionOrder.setUOM(jrs2.getString("UOM"));
				//BomConfirmationProductionOrder.setQtyBaseUOM(jrs2.getString("QtyBaseUOM"));
				//BomConfirmationProductionOrder.setBaseUOM(jrs2.getString("BaseUOM"));
				
				//String ConfirmationProductionQty = BomConfirmationProductionOrder.getQtyBaseUOM();
				String ConfirmationProductionQty = BomConfirmationProductionOrder.getQty();
				
				if(ConfirmationProductionQty.isEmpty())
					ConfirmationProductionQty = "0";
				
				newConfirmationProductionQty = newConfirmationProductionQty + Integer.parseInt(ConfirmationProductionQty);
			}
			//jrs2.close();
			//pstmLoadConfirmationProductionOrder.close();
			
			
			//load production order header proccess
			String sqlLoadProductionOrder = "SELECT a.ProductID,b.Title_EN,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM bom_production_order a "
					+ "INNER JOIN product b ON b.ID = a.ProductID "
					+ "WHERE a.OrderTypeID=? AND a.OrderNo=? LIMIT 1";
			
			pstmLoadProductionOrder = connection.prepareStatement(sqlLoadProductionOrder);
			
			pstmLoadProductionOrder.setString(1, lordertypeid);
			pstmLoadProductionOrder.setString(2, lorderno);
			pstmLoadProductionOrder.addBatch();
			
			Globals.gCommand = pstmLoadProductionOrder.toString();
			
			jrs = pstmLoadProductionOrder.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMProductionOrder mdlBomProductionOrder = new model.mdlBOMProductionOrder();
				
				mdlBomProductionOrder.setProductID(jrs.getString("ProductID"));
				mdlBomProductionOrder.setProductName(jrs.getString("Title_EN"));
				mdlBomProductionOrder.setQty(String.valueOf( Integer.parseInt(jrs.getString("Qty")) - newConfirmationProductionQty ));
				mdlBomProductionOrder.setUOM(jrs.getString("UOM"));
				//if(newConfirmationProductionQty == 0)
				//{
				//mdlBomProductionOrder.setQty(jrs.getString("Qty"));
				//mdlBomProductionOrder.setUOM(jrs.getString("UOM"));
				//}
				//else
				//{
				//mdlBomProductionOrder.setQty( String.valueOf((Integer.parseInt(jrs.getString("QtyBaseUOM")) - newConfirmationProductionQty)) );
				//mdlBomProductionOrder.setUOM(jrs.getString("BaseUOM"));
				//}
				
				listmdlBOMProductionOrder.add(mdlBomProductionOrder);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOMProductionOrderByKey", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstmLoadConfirmationProductionOrder != null) {
					 pstmLoadConfirmationProductionOrder.close();
				 }
				 if (pstmLoadProductionOrder != null) {
					 pstmLoadProductionOrder.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
				 if (jrs2 != null) {
					 jrs2.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadBOMProductionOrderByKey", "close opened connection protocol", Globals.user);
			}
		 }

		return listmdlBOMProductionOrder;
	}
	
	public static List<model.mdlBOMProductionOrder> LoadPlantWarehouseProductionOrderByKey(String lordertypeid, String lorderno) {
		List<model.mdlBOMProductionOrder> listmdlBOMProductionOrder = new ArrayList<model.mdlBOMProductionOrder>();
		
		Connection connection = null;
		PreparedStatement pstmLoadProductionOrder = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadProductionOrder = "SELECT a.PlantID,a.WarehouseID "
					+ "FROM bom_production_order a "
					+ "WHERE a.OrderTypeID=? AND a.OrderNo=? LIMIT 1";
			
			pstmLoadProductionOrder = connection.prepareStatement(sqlLoadProductionOrder);
			
			pstmLoadProductionOrder.setString(1, lordertypeid);
			pstmLoadProductionOrder.setString(2, lorderno);
			pstmLoadProductionOrder.addBatch();
			
			Globals.gCommand = pstmLoadProductionOrder.toString();
			
			jrs = pstmLoadProductionOrder.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMProductionOrder mdlBomProductionOrder = new model.mdlBOMProductionOrder();
				
				mdlBomProductionOrder.setPlantID(jrs.getString("PlantID"));
				mdlBomProductionOrder.setWarehouseID(jrs.getString("WarehouseID"));
				
				listmdlBOMProductionOrder.add(mdlBomProductionOrder);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadPlantWarehouseProductionOrderByKey", Globals.gCommand , Globals.user);
		}
		 finally {
			 try{
				 if (pstmLoadProductionOrder != null) {
					 pstmLoadProductionOrder.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e) {
				 LogAdapter.InsertLogExc(e.toString(), "LoadPlantWarehouseProductionOrderByKey", "close opened connection protocol", Globals.user);
			 }
		 }

		return listmdlBOMProductionOrder;
	}
	
	public static List<model.mdlBOMProductionOrder> LoadReleasedBOMProductionOrder() {
		List<model.mdlBOMProductionOrder> listmdlBOMProductionOrder = new ArrayList<model.mdlBOMProductionOrder>();
		
		Connection connection = null;
		PreparedStatement pstmLoadProductionOrder = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadProductionOrder = "SELECT a.OrderTypeID,a.OrderNo,a.ProductID,b.Title_EN,a.PlantID,a.StartDate,a.WarehouseID "
					+ "FROM bom_production_order a "
					+ "INNER JOIN product b ON b.ID = a.ProductID "
					+ "WHERE a.Status = 'RELEASED' AND a.PlantID IN ("+Globals.user_area+")";
			
			pstmLoadProductionOrder = connection.prepareStatement(sqlLoadProductionOrder);
			Globals.gCommand = pstmLoadProductionOrder.toString();
			
			jrs = pstmLoadProductionOrder.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMProductionOrder mdlBomProductionOrder = new model.mdlBOMProductionOrder();
				
				mdlBomProductionOrder.setOrderTypeID(jrs.getString("OrderTypeID"));
				mdlBomProductionOrder.setOrderNo(jrs.getString("OrderNo"));
				mdlBomProductionOrder.setProductID(jrs.getString("ProductID"));
				mdlBomProductionOrder.setProductName(jrs.getString("Title_EN"));
				mdlBomProductionOrder.setPlantID(jrs.getString("PlantID"));
				
				String Date = jrs.getString("StartDate");
				Date initDate = new SimpleDateFormat("yyyy-MM-dd").parse(Date);
			    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
			    String newDate = formatter.format(initDate);
				mdlBomProductionOrder.setStartDate(newDate);
				
				mdlBomProductionOrder.setWarehouseID(jrs.getString("WarehouseID"));
				
				listmdlBOMProductionOrder.add(mdlBomProductionOrder);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadReleasedBOMProductionOrder", Globals.gCommand , Globals.user);
		}
		 finally {
			 try{
				 if (pstmLoadProductionOrder != null) {
					 pstmLoadProductionOrder.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e){
				 LogAdapter.InsertLogExc(e.toString(), "LoadReleasedBOMProductionOrder", "close opened connection protocol", Globals.user);
			 }
		 }

		return listmdlBOMProductionOrder;
	}
	
	//to show bom production detail
	public static List<model.mdlBOMProductionOrderDetail> LoadBOMProductionOrderDetail(String ordertypeid, String orderno) {
		List<model.mdlBOMProductionOrderDetail> listmdlBOMProductionOrderDetail = new ArrayList<model.mdlBOMProductionOrderDetail>();
		
		Connection connection = null;
		PreparedStatement pstmLoadBomProductionOrderDetail = null;
		ResultSet jrs = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBomProductionOrderDetail = "SELECT a.OrderTypeID,a.OrderNo,a.ComponentLine,a.ComponentID,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM bom_production_order_detail a "
					+ "WHERE a.OrderTypeID=? AND a.OrderNo=? ORDER BY a.ComponentLine";
			
			pstmLoadBomProductionOrderDetail = connection.prepareStatement(sqlLoadBomProductionOrderDetail);
			
			pstmLoadBomProductionOrderDetail.setString(1, ordertypeid);
			pstmLoadBomProductionOrderDetail.setString(2, orderno);
			pstmLoadBomProductionOrderDetail.addBatch();
			
			Globals.gCommand = pstmLoadBomProductionOrderDetail.toString();
			
			jrs = pstmLoadBomProductionOrderDetail.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMProductionOrderDetail mdlBomProductionOrderDetail = new model.mdlBOMProductionOrderDetail();
				
				mdlBomProductionOrderDetail.setComponentLine(jrs.getString("ComponentLine"));
				mdlBomProductionOrderDetail.setComponentID(jrs.getString("ComponentID"));
				mdlBomProductionOrderDetail.setQty(jrs.getString("Qty"));
				mdlBomProductionOrderDetail.setUOM(jrs.getString("UOM"));
				mdlBomProductionOrderDetail.setQtyBaseUOM(jrs.getString("QtyBaseUOM"));
				mdlBomProductionOrderDetail.setBaseUOM(jrs.getString("BaseUOM"));
				
				listmdlBOMProductionOrderDetail.add(mdlBomProductionOrderDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOMProductionOrderDetail", Globals.gCommand , Globals.user);
		}
		 finally {
			 try{
				 if (pstmLoadBomProductionOrderDetail != null) {
					 pstmLoadBomProductionOrderDetail.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e){
				 LogAdapter.InsertLogExc(e.toString(), "LoadBOMProductionOrderDetail", "close opened connection protocol", Globals.user);
			 }
		 }

		return listmdlBOMProductionOrderDetail;
	}

	public static String GetProductionOrderNo(String ordertypeid) {
		String ResultOrderNo = "";
		
		Connection connection = null;
		PreparedStatement pstmGetOrderNo = null;
		ResultSet jrs = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlGetOrderNo = "SELECT a.OrderNo "
					+ "FROM bom_production_order a "
					+ "WHERE a.OrderTypeID=? ORDER BY a.Created_at DESC LIMIT 1";
			
			pstmGetOrderNo = connection.prepareStatement(sqlGetOrderNo);
			
			pstmGetOrderNo.setString(1, ordertypeid);
			pstmGetOrderNo.addBatch();
			
			Globals.gCommand = pstmGetOrderNo.toString();
			
			jrs = pstmGetOrderNo.executeQuery();
									
			while(jrs.next()){
				
				ResultOrderNo = jrs.getString("OrderNo");

			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetProductionOrderNo", Globals.gCommand , Globals.user);
		}
		 finally {
			 try{
				 if (pstmGetOrderNo != null) {
					 pstmGetOrderNo.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e){
				 LogAdapter.InsertLogExc(e.toString(), "GetProductionOrderNo", "close opened connection protocol", Globals.user);
			 }
		 }

		return ResultOrderNo;
	}
	
	public static String GetConfirmationNo(String confirmationid) {
		String ResultConfirmationNo = "";
		
		Connection connection = null;
		PreparedStatement pstmGetConfirmationNo = null;
		ResultSet jrs = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlGetConfirmationNo = "SELECT a.ConfirmationNo "
					+ "FROM bom_production_order_confirmation a "
					+ "WHERE a.ConfirmationID=? ORDER BY a.Created_at DESC LIMIT 1";
			
			pstmGetConfirmationNo = connection.prepareStatement(sqlGetConfirmationNo);
			
			pstmGetConfirmationNo.setString(1, confirmationid);
			pstmGetConfirmationNo.addBatch();
			
			Globals.gCommand = pstmGetConfirmationNo.toString();
			
			jrs = pstmGetConfirmationNo.executeQuery();
									
			while(jrs.next()){
				
				ResultConfirmationNo = jrs.getString("ConfirmationNo");

			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetConfirmationNo", Globals.gCommand , Globals.user);
		}
		 finally {
			 try{
				 if (pstmGetConfirmationNo != null) {
					 pstmGetConfirmationNo.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e){
				 LogAdapter.InsertLogExc(e.toString(), "GetConfirmationNo", "close opened connection protocol", Globals.user);
			 }
		 }

		return ResultConfirmationNo;
	}
	
	public static String GetCancelConfirmationNo(String cancelconfirmationid) {
		String ResultCancelConfirmationNo = "";
		
		Connection connection = null;
		PreparedStatement pstmGetCancelConfirmationNo = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlGetCancelConfirmationNo = "SELECT a.CancelConfirmationNo "
					+ "FROM bom_production_order_confirmation_cancel a "
					+ "WHERE a.CancelConfirmationID=? ORDER BY a.Created_at DESC LIMIT 1";
			
			pstmGetCancelConfirmationNo = connection.prepareStatement(sqlGetCancelConfirmationNo);
			
			pstmGetCancelConfirmationNo.setString(1, cancelconfirmationid);
			pstmGetCancelConfirmationNo.addBatch();
			Globals.gCommand = pstmGetCancelConfirmationNo.toString();
			
			jrs = pstmGetCancelConfirmationNo.executeQuery();
									
			while(jrs.next()){
				ResultCancelConfirmationNo = jrs.getString("CancelConfirmationNo");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetCancelConfirmationNo", Globals.gCommand , Globals.user);
		}
		 finally {
			 try{
				 if (pstmGetCancelConfirmationNo != null) {
					 pstmGetCancelConfirmationNo.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e){
				 LogAdapter.InsertLogExc(e.toString(), "GetCancelConfirmationNo", "close opened connection protocol", Globals.user);
			 }
		 }

		return ResultCancelConfirmationNo;
	}
	
	public static model.mdlBomOrderType GetBOMOrdeTypeById(String ordertypeid) {
		model.mdlBomOrderType mdlOrderType = new model.mdlBomOrderType();
		
		Connection connection = null;
		PreparedStatement pstmGetOrderType = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlGetOrderType = "SELECT OrderTypeID,OrderTypeDesc,`From`,`To` "
					+ "FROM bom_order_type "
					+ "WHERE OrderTypeID=? LIMIT 1";
			
			pstmGetOrderType = connection.prepareStatement(sqlGetOrderType);
			
			pstmGetOrderType.setString(1, ordertypeid);
			pstmGetOrderType.addBatch();
			
			Globals.gCommand = pstmGetOrderType.toString();
			
			jrs = pstmGetOrderType.executeQuery();
									
			while(jrs.next()){
				
				mdlOrderType.setOrderTypeID(jrs.getString("OrderTypeID"));
				mdlOrderType.setOrderTypeDesc(jrs.getString("OrderTypeDesc"));
				mdlOrderType.setFrom(jrs.getString("From"));
				mdlOrderType.setTo(jrs.getString("To"));

			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetBOMOrdeTypeById", Globals.gCommand , Globals.user);
		}
		 finally {
			 try{
				 if (pstmGetOrderType != null) {
					 pstmGetOrderType.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e){
				 LogAdapter.InsertLogExc(e.toString(), "GetBOMOrdeTypeById", "close opened connection protocol", Globals.user);
			 }
		 }

		return mdlOrderType;
	}
	
	public static model.mdlBOMConfirmationDocument GetConfirmationDocumentById(String confirmationid) {
		model.mdlBOMConfirmationDocument mdlConfirmationDoc = new model.mdlBOMConfirmationDocument();
		
		Connection connection = null;
		PreparedStatement pstmGetConfirmationDoc = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlGetConfirmationDoc = "SELECT ConfirmationID,ConfirmationDesc,`From`,`To` "
					+ "FROM bom_confirmation_document "
					+ "WHERE ConfirmationID=? LIMIT 1";
			
			pstmGetConfirmationDoc = connection.prepareStatement(sqlGetConfirmationDoc);
			
			pstmGetConfirmationDoc.setString(1, confirmationid);
			pstmGetConfirmationDoc.addBatch();
			
			Globals.gCommand = pstmGetConfirmationDoc.toString();
			
			jrs = pstmGetConfirmationDoc.executeQuery();
									
			while(jrs.next()){
				
				mdlConfirmationDoc.setConfirmationID(jrs.getString("ConfirmationID"));
				mdlConfirmationDoc.setConfirmationDesc(jrs.getString("ConfirmationDesc"));
				mdlConfirmationDoc.setRangeFrom(jrs.getString("From"));
				mdlConfirmationDoc.setRangeTo(jrs.getString("To"));

			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetConfirmationDocumentById", Globals.gCommand , Globals.user);
		}
		 finally {
			 try{
				 if (pstmGetConfirmationDoc != null) {
					 pstmGetConfirmationDoc.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e){
				 LogAdapter.InsertLogExc(e.toString(), "GetConfirmationDocumentById", "close opened connection protocol", Globals.user);
			 }
		 }

		return mdlConfirmationDoc;
	}

	public static String TransactionInsertProductionOrder(mdlBOMProductionOrder lParamProductionOrder, List<mdlBOMDetail> llistParamBOMDetail)
	{	
		Globals.gCommand = "";
		
			Connection connection = null;
			PreparedStatement pstmInsertProductionOrder = null;
			PreparedStatement pstmInsertProductionOrderDetail = null;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlInsertProductionOrder = "INSERT INTO bom_production_order(`OrderTypeID`, `OrderNo`, `ProductID`, `PlantID`, `Qty`, `UOM`, `QtyBaseUOM`, `BaseUOM`, `StartDate`, `WarehouseID`, `Status`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertProductionOrderDetail = "INSERT INTO bom_production_order_detail(`OrderTypeID`, `OrderNo`, `ComponentLine`, `ComponentID`, `Qty`, `UOM`, `QtyBaseUOM`, `BaseUOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
				pstmInsertProductionOrder = connection.prepareStatement(sqlInsertProductionOrder);
				pstmInsertProductionOrderDetail = connection.prepareStatement(sqlInsertProductionOrderDetail);
				
				//insert bom production order parameter
				pstmInsertProductionOrder.setString(1,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getOrderTypeID()));
				pstmInsertProductionOrder.setString(2,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getOrderNo()));
				pstmInsertProductionOrder.setString(3,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getProductID()));
				pstmInsertProductionOrder.setString(4,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getPlantID()));
				pstmInsertProductionOrder.setString(5,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getQty()));
				pstmInsertProductionOrder.setString(6,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getUOM()));
				pstmInsertProductionOrder.setString(7,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getQtyBaseUOM())); 
				pstmInsertProductionOrder.setString(8,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getBaseUOM()));
				pstmInsertProductionOrder.setString(9,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getStartDate()));
				pstmInsertProductionOrder.setString(10, ValidateNull.NulltoStringEmpty(lParamProductionOrder.getWarehouseID()));
				pstmInsertProductionOrder.setString(11, ValidateNull.NulltoStringEmpty(lParamProductionOrder.getStatus()));
				pstmInsertProductionOrder.setString(12, ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertProductionOrder.setString(13, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertProductionOrder.setString(14, ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertProductionOrder.setString(15, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertProductionOrder.toString();
				pstmInsertProductionOrder.executeUpdate();
				
				//insert bom production order detail parameter
				for(mdlBOMDetail lParamBomDetail : llistParamBOMDetail)
				{
					//Get the Conversion
					Integer txtQtyBomDetail = (Integer.parseInt(lParamBomDetail.getQty())/lParamBomDetail.getQtyBomHeader())*Integer.parseInt(lParamProductionOrder.getQty());
					 
					model.mdlProductUom mdlComponentUom = new model.mdlProductUom();
					mdlComponentUom = ProductUomAdapter.LoadProductUOMByKey(lParamBomDetail.getComponentID(), lParamBomDetail.getUOM());
					Integer txtQtyBaseBomDetail = txtQtyBomDetail*Integer.parseInt(mdlComponentUom.getQty());
					
					pstmInsertProductionOrderDetail.setString(1,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getOrderTypeID()));
					pstmInsertProductionOrderDetail.setString(2,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getOrderNo()));
					pstmInsertProductionOrderDetail.setString(3,  ValidateNull.NulltoStringEmpty(lParamBomDetail.getComponentLine()));
					pstmInsertProductionOrderDetail.setString(4,  ValidateNull.NulltoStringEmpty((lParamBomDetail.getComponentID())));
					pstmInsertProductionOrderDetail.setInt(5,  txtQtyBomDetail);
					pstmInsertProductionOrderDetail.setString(6,  ValidateNull.NulltoStringEmpty(lParamBomDetail.getUOM())); 
					pstmInsertProductionOrderDetail.setInt(7,  txtQtyBaseBomDetail);
					pstmInsertProductionOrderDetail.setString(8,  ValidateNull.NulltoStringEmpty(lParamBomDetail.getBaseUOM()));
					pstmInsertProductionOrderDetail.setString(9, ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertProductionOrderDetail.setString(10, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertProductionOrderDetail.setString(11, ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertProductionOrderDetail.setString(12, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmInsertProductionOrderDetail.toString();
					pstmInsertProductionOrderDetail.executeUpdate();
				}
					
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Insert BOM Production Order";
			}
			catch (Exception e) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionInsertBomProductionOrder", Globals.gCommand , Globals.user);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertBomProductionOrder", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try {
			        if (pstmInsertProductionOrder != null) {
			        	pstmInsertProductionOrder.close();
			        }
			        if (pstmInsertProductionOrderDetail != null) {
			        	pstmInsertProductionOrderDetail.close();
			        }
			        connection.setAutoCommit(true);
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e) {
					LogAdapter.InsertLogExc(e.toString(), "InsertBomProductionOrder", "close opened connection protocol", Globals.user);
				}
		    }
			
		return Globals.gReturn_Status;
	}

	public static String TransactionUpdateProductionOrder(mdlBOMProductionOrder lParamProductionOrder, List<mdlBOMDetail> llistParamBOMDetail)
	{	
		Globals.gCommand = "";
		
			Connection connection = null;
			PreparedStatement pstmUpdateProductionOrder = null;
			PreparedStatement pstmDeleteProductionOrderDetail = null;
			PreparedStatement pstmInsertProductionOrderDetail = null;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlUpdateProductionOrder = "UPDATE bom_production_order SET ProductID=?,PlantID=?,Qty=?,UOM=?,QtyBaseUOM=?,BaseUOM=?,StartDate=?,WarehouseID=?,Updated_by=?,Updated_at=? WHERE OrderTypeID=? AND OrderNo=?;";
				String sqlDeleteProductionOrderDetail = "DELETE FROM bom_production_order_detail WHERE OrderTypeID=? AND OrderNo=?";
				String sqlInsertProductionOrderDetail = "INSERT INTO bom_production_order_detail(`OrderTypeID`, `OrderNo`, `ComponentLine`, `ComponentID`, `Qty`, `UOM`, `QtyBaseUOM`, `BaseUOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) "
														+ "ON DUPLICATE KEY UPDATE `ComponentID`=?,`Qty`=?,`UOM`=?,`QtyBaseUOM`=?,`BaseUOM`=?,`Updated_by`=?,`Updated_at`=?;";
				pstmUpdateProductionOrder = connection.prepareStatement(sqlUpdateProductionOrder);
				pstmDeleteProductionOrderDetail = connection.prepareStatement(sqlDeleteProductionOrderDetail);
				pstmInsertProductionOrderDetail = connection.prepareStatement(sqlInsertProductionOrderDetail);
				
				//update bom production order parameter
				pstmUpdateProductionOrder.setString(1,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getProductID()));
				pstmUpdateProductionOrder.setString(2,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getPlantID()));
				pstmUpdateProductionOrder.setString(3,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getQty()));
				pstmUpdateProductionOrder.setString(4,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getUOM()));
				pstmUpdateProductionOrder.setString(5,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getQtyBaseUOM()));
				pstmUpdateProductionOrder.setString(6,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getBaseUOM()));
				pstmUpdateProductionOrder.setString(7,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getStartDate())); 
				pstmUpdateProductionOrder.setString(8,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getWarehouseID()));
				pstmUpdateProductionOrder.setString(9,  ValidateNull.NulltoStringEmpty(Globals.user));
				pstmUpdateProductionOrder.setString(10, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmUpdateProductionOrder.setString(11, ValidateNull.NulltoStringEmpty(lParamProductionOrder.getOrderTypeID()));
				pstmUpdateProductionOrder.setString(12, ValidateNull.NulltoStringEmpty(lParamProductionOrder.getOrderNo()));
				Globals.gCommand = pstmUpdateProductionOrder.toString();
				pstmUpdateProductionOrder.executeUpdate();
				
				//delete bom production order detail parameter
				pstmDeleteProductionOrderDetail.setString(1,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getOrderTypeID()));
				pstmDeleteProductionOrderDetail.setString(2,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getOrderNo()));
				Globals.gCommand = pstmDeleteProductionOrderDetail.toString();
				pstmDeleteProductionOrderDetail.executeUpdate();
				
				//insert bom production order detail parameter
				for(mdlBOMDetail lParamBomDetail : llistParamBOMDetail)
				{
					//Get the Conversion
					Integer txtQtyBomDetail = (Integer.parseInt(lParamBomDetail.getQty())/lParamBomDetail.getQtyBomHeader())*Integer.parseInt(lParamProductionOrder.getQty());
					 
					model.mdlProductUom mdlComponentUom = new model.mdlProductUom();
					mdlComponentUom = ProductUomAdapter.LoadProductUOMByKey(lParamBomDetail.getComponentID(), lParamBomDetail.getUOM());
					Integer txtQtyBaseBomDetail = txtQtyBomDetail*Integer.parseInt(mdlComponentUom.getQty());
					
					pstmInsertProductionOrderDetail.setString(1,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getOrderTypeID()));
					pstmInsertProductionOrderDetail.setString(2,  ValidateNull.NulltoStringEmpty(lParamProductionOrder.getOrderNo()));
					pstmInsertProductionOrderDetail.setString(3,  ValidateNull.NulltoStringEmpty(lParamBomDetail.getComponentLine()));
					pstmInsertProductionOrderDetail.setString(4,  ValidateNull.NulltoStringEmpty((lParamBomDetail.getComponentID())));
					pstmInsertProductionOrderDetail.setInt(5,  txtQtyBomDetail);
					pstmInsertProductionOrderDetail.setString(6,  ValidateNull.NulltoStringEmpty(lParamBomDetail.getUOM())); 
					pstmInsertProductionOrderDetail.setInt(7,  txtQtyBaseBomDetail);
					pstmInsertProductionOrderDetail.setString(8,  ValidateNull.NulltoStringEmpty(lParamBomDetail.getBaseUOM()));
					pstmInsertProductionOrderDetail.setString(9, ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertProductionOrderDetail.setString(10, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertProductionOrderDetail.setString(11, ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertProductionOrderDetail.setString(12, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertProductionOrderDetail.setString(13, ValidateNull.NulltoStringEmpty(lParamBomDetail.getComponentID()));
					pstmInsertProductionOrderDetail.setInt(14, txtQtyBomDetail);
					pstmInsertProductionOrderDetail.setString(15, ValidateNull.NulltoStringEmpty(lParamBomDetail.getUOM()));
					pstmInsertProductionOrderDetail.setInt(16, txtQtyBaseBomDetail);
					pstmInsertProductionOrderDetail.setString(17, ValidateNull.NulltoStringEmpty(lParamBomDetail.getBaseUOM()));
					pstmInsertProductionOrderDetail.setString(18, ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertProductionOrderDetail.setString(19, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmInsertProductionOrderDetail.toString();
					pstmInsertProductionOrderDetail.executeUpdate();
				}
					
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Update BOM Production Order";
			}
			
			catch (Exception e ) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionUpdateBomProductionOrder", Globals.gCommand , Globals.user);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "UpdateBomProductionOrder", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
			        if (pstmUpdateProductionOrder != null) {
			        	pstmUpdateProductionOrder.close();
			        }
			        if (pstmDeleteProductionOrderDetail != null) {
			        	pstmDeleteProductionOrderDetail.close();
			        }
			        if (pstmInsertProductionOrderDetail != null) {
			        	pstmInsertProductionOrderDetail.close();
			        }
			        connection.setAutoCommit(true);
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e) {
					LogAdapter.InsertLogExc(e.toString(), "UpdateBomProductionOrder", "close opened connection protocol", Globals.user);
				}
		    }
			
			return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String ReleaseProductionOrder(String lOrderTypeID, String lOrderNo)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM bom_production_order WHERE OrderTypeID = ? AND OrderNo = ? LIMIT 1");
			jrs.setString(1,  lOrderTypeID);
			jrs.setString(2,  lOrderNo);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("Status", "RELEASED");
			jrs.updateRow();
				
			Globals.gReturn_Status = "Success Release Production Order";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ReleaseProductionOrder", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "ReleaseProductionOrder", "close opened connection protocol", Globals.user);
			}
		}
		
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String CancelProductionOrder(String lOrderTypeID, String lOrderNo)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM bom_production_order WHERE OrderTypeID = ? AND OrderNo = ? LIMIT 1");
			Globals.gCommand = jrs.getCommand();
			jrs.setString(1,  lOrderTypeID);
			jrs.setString(2,  lOrderNo);
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("Status", "CANCELLED");
			jrs.updateRow();
			
			Globals.gReturn_Status = "Success Cancel Production Order";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CancelProductionOrder", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "CancelProductionOrder", "close opened connection protocol", Globals.user);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlBOMConfirmationProduction> LoadBOMConfirmationProduction() {
		List<model.mdlBOMConfirmationProduction> listBOMConfirmationProduction = new ArrayList<model.mdlBOMConfirmationProduction>();
		
		Connection connection = null;
		PreparedStatement pstmLoadConfirmationProduction = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadConfirmationProduction = "SELECT a.ConfirmationID,a.ConfirmationNo,a.ConfirmationDate,a.OrderTypeID,a.OrderNo,a.ProductID,b.Title_EN,a.Batch_No,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM bom_production_order_confirmation a "
					+ "INNER JOIN product b ON b.ID=a.ProductID "
					+ "INNER JOIN bom_production_order c ON c.OrderTypeID=a.OrderTypeID AND c.OrderNo=a.OrderNo "
					+ "WHERE c.PlantID IN ("+Globals.user_area+")";
			pstmLoadConfirmationProduction = connection.prepareStatement(sqlLoadConfirmationProduction);
			
			Globals.gCommand = pstmLoadConfirmationProduction.toString();
			
			jrs = pstmLoadConfirmationProduction.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMConfirmationProduction BOMConfirmationProduction = new model.mdlBOMConfirmationProduction();
				
				BOMConfirmationProduction.setConfirmationID(jrs.getString("ConfirmationID"));
				BOMConfirmationProduction.setConfirmationNo(jrs.getString("ConfirmationNo"));
				
				String Date = jrs.getString("ConfirmationDate");
				Date initDate = new SimpleDateFormat("yyyy-MM-dd").parse(Date);
			    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
			    String newDate = formatter.format(initDate);
				BOMConfirmationProduction.setConfirmationDate(newDate);
				
				BOMConfirmationProduction.setOrderTypeID(jrs.getString("OrderTypeID"));
				BOMConfirmationProduction.setOrderNo(jrs.getString("OrderNo"));
				BOMConfirmationProduction.setProductID(jrs.getString("ProductID"));
				BOMConfirmationProduction.setProductName(jrs.getString("Title_EN"));
				BOMConfirmationProduction.setBatchNo(jrs.getString("Batch_No"));
				BOMConfirmationProduction.setQty(jrs.getString("Qty"));
				BOMConfirmationProduction.setUOM(jrs.getString("UOM"));
				BOMConfirmationProduction.setQtyBaseUOM(jrs.getString("QtyBaseUOM"));
				BOMConfirmationProduction.setBaseUOM(jrs.getString("BaseUOM"));
				
				listBOMConfirmationProduction.add(BOMConfirmationProduction);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOMConfirmationProduction", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstmLoadConfirmationProduction != null) {
					 pstmLoadConfirmationProduction.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadBOMConfirmationProduction", "close opened connection protocol", Globals.user);
			}
		 }

		return listBOMConfirmationProduction;
	}
	
}
