package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import model.Globals;
import model.mdlCustomerBranch;

/** Documentation
 * 
 */
public class CustomerBranchAdapter {
	
	@SuppressWarnings("resource")
	public static model.mdlCustomerBranch LoadCustomerBranchbyID(String lCustBranchID) {
		model.mdlCustomerBranch mdlCustomerBranch = new model.mdlCustomerBranch();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT CustomerBranchName, CustomerBranchAddress, Phone, Email, PIC, CustomerID FROM Customer_Branch WHERE CustomerBranchID = ?");
			jrs.setString(1,  lCustBranchID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				mdlCustomerBranch.setCustBranchID(lCustBranchID);
				mdlCustomerBranch.setCustBranchName(jrs.getString("CustomerBranchName"));
				mdlCustomerBranch.setCustBranchAddress(jrs.getString("CustomerBranchAddress"));
				mdlCustomerBranch.setPhone(jrs.getString("Phone"));
				mdlCustomerBranch.setEmail(jrs.getString("Email"));
				mdlCustomerBranch.setPic(jrs.getString("Pic"));
				mdlCustomerBranch.setCustomerID(jrs.getString("CustomerID"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomerBranchbyID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCustomerBranchbyID", "close opened connection protocol", Globals.user);
			}
		}

		return mdlCustomerBranch;
	}
	
	public static List<model.mdlCustomerBranch> LoadCustomerBranch(String lUser) {
		List<model.mdlCustomerBranch> listmdlCustomerBranch = new ArrayList<model.mdlCustomerBranch>();
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT CustomerBranchID, CustomerBranchName, CustomerBranchAddress, Phone, Email, PIC, CustomerID FROM customer_branch");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlCustomerBranch mdlCustomerBranch = new model.mdlCustomerBranch();
				mdlCustomerBranch.setCustBranchID(jrs.getString("CustomerBranchID"));
				mdlCustomerBranch.setCustBranchName(jrs.getString("CustomerBranchName"));
				mdlCustomerBranch.setCustBranchAddress(jrs.getString("CustomerBranchAddress"));
				mdlCustomerBranch.setPhone(jrs.getString("Phone"));
				mdlCustomerBranch.setEmail(jrs.getString("Email"));
				mdlCustomerBranch.setPic(jrs.getString("Pic"));
				mdlCustomerBranch.setCustomerID(jrs.getString("CustomerID"));		
				
				listmdlCustomerBranch.add(mdlCustomerBranch);
			}
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomerBranch", Globals.gCommand , lUser);
		}

		return listmdlCustomerBranch;
	}
	
	public static List<model.mdlCustomerBranch> LoadCustomerBranchJoin(String lUser) {
		List<model.mdlCustomerBranch> listmdlCustomerBranch = new ArrayList<model.mdlCustomerBranch>();
		
		Connection connection = null;
		PreparedStatement pstmLoadCustomerBranch = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadCustomerBranch = "SELECT a.CustomerBranchID, a.CustomerBranchName, a.CustomerBranchAddress, a.Phone, a.Email, a.PIC, a.CustomerID, b.CustomerName "
					+ "FROM customer_branch a "
					+ "LEFT JOIN customer b ON b.CustomerID = a.CustomerID";
			
			pstmLoadCustomerBranch = connection.prepareStatement(sqlLoadCustomerBranch);
			Globals.gCommand = pstmLoadCustomerBranch.toString();
			
			jrs = pstmLoadCustomerBranch.executeQuery();
									
			while(jrs.next()){
				model.mdlCustomerBranch mdlCustomerBranch = new model.mdlCustomerBranch();
				mdlCustomerBranch.setCustBranchID(jrs.getString("CustomerBranchID"));
				mdlCustomerBranch.setCustBranchName(jrs.getString("CustomerBranchName"));
				mdlCustomerBranch.setCustBranchAddress(jrs.getString("CustomerBranchAddress"));
				mdlCustomerBranch.setPhone(jrs.getString("Phone"));
				mdlCustomerBranch.setEmail(jrs.getString("Email"));
				mdlCustomerBranch.setPic(jrs.getString("Pic"));
				mdlCustomerBranch.setCustomerID(jrs.getString("CustomerID"));
				mdlCustomerBranch.setCustomerName(jrs.getString("CustomerName"));
				
				listmdlCustomerBranch.add(mdlCustomerBranch);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomerBranchJoin", Globals.gCommand , lUser);
		}
		finally {
			try{
				 if (pstmLoadCustomerBranch != null) {
					 pstmLoadCustomerBranch.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCustomerBranchJoin", "close opened connection protocol", lUser);
			}
		 }

		return listmdlCustomerBranch;
	}
	
	@SuppressWarnings("resource")
	public static String InsertCustomerBranch(model.mdlCustomerBranch lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlCustomerBranch CheckDuplicateCustomerBranch = LoadCustomerBranchbyID(lParam.getCustBranchID());
			if(CheckDuplicateCustomerBranch.getCustBranchID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();			
				
				jrs.setCommand("SELECT CustomerBranchID, CustomerBranchName, CustomerBranchAddress, Phone, Email, PIC, CustomerID FROM Customer_branch LIMIT 1");
				
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
				
				jrs.moveToInsertRow();
				jrs.updateString("CustomerBranchID",lParam.getCustBranchID());
				jrs.updateString("CustomerBranchName",lParam.getCustBranchName());
				jrs.updateString("CustomerBranchAddress",lParam.getCustBranchAddress());
				jrs.updateString("Phone",lParam.getPhone());
				jrs.updateString("Email",lParam.getEmail());
				jrs.updateString("PIC",lParam.getPic());
				jrs.updateString("CustomerID",lParam.getCustomerID());
				
				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Customer Branch";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Cabang pelanggan sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertCustomerBranch", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Insert Customer Branch";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertCustomerBranch", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String UpdateCustomerBranch(model.mdlCustomerBranch lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT CustomerBranchID, CustomerBranchName, CustomerBranchAddress, Phone, Email, PIC, CustomerID FROM customer_branch WHERE CustomerBranchID=? LIMIT 1");
			jrs.setString(1,  lParam.getCustBranchID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("CustomerBranchName",lParam.getCustBranchName());
			jrs.updateString("CustomerBranchAddress",lParam.getCustBranchAddress());
			jrs.updateString("Phone",lParam.getPhone());
			jrs.updateString("Email",lParam.getEmail());
			jrs.updateString("PIC",lParam.getPic());
			jrs.updateString("CustomerID",lParam.getCustomerID());
			jrs.updateRow();
				
			Globals.gReturn_Status = "Success Update Customer Branch";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateCustomerBranch", Globals.gCommand , lUser);
//			Globals.gReturn_Status = "Error Update Customer Branch";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateCustomerBranch", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String DeleteCustomerBranch(String lCustBranchID, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT CustomerBranchID, CustomerBranchName, CustomerBranchAddress, Phone, Email, PIC, CustomerID FROM customer_branch WHERE CustomerBranchID = ? LIMIT 1");
			jrs.setString(1, lCustBranchID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			Globals.gReturn_Status = "Success Delete Customer Branch";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteCustomerBranch", Globals.gCommand , lUser);
//			Globals.gReturn_Status = "Error Delete Customer Branch";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteCustomerBranch", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}


}
