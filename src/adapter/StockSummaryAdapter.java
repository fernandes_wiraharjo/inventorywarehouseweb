	package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

/** Documentation
 * 002 nanda
 */

public class StockSummaryAdapter {
	public static String InsertStockSummary(model.mdlStockSummary lParam,String luser)
	{
		Globals.gCommand = "Insert ID : " + lParam.getPeriod() +";"+ lParam.getProductID() +";"+ lParam.getPlantID() +";"+ lParam.getWarehouseID() +";"+ lParam.getBatch_No();
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();			
			
			jrs.setCommand("SELECT `Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM stock_summary LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("Period",lParam.getPeriod());
			jrs.updateString("ProductID",lParam.getProductID());
			jrs.updateString("PlantID",lParam.getPlantID());
			jrs.updateString("WarehouseID",lParam.getWarehouseID());
			jrs.updateString("Batch_No",lParam.getBatch_No());
			jrs.updateString("Qty",lParam.getQty());
			jrs.updateString("UOM",lParam.getUOM());
			jrs.updateString("Created_by",luser);
			jrs.updateString("Created_at",LocalDateTime.now().toString());
			jrs.updateString("Updated_by",luser);
			jrs.updateString("Updated_at",LocalDateTime.now().toString());
			
			jrs.insertRow();	
			jrs.close();
			Globals.gReturn_Status = "Success Insert Stock Summary ";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertStockSummary", Globals.gCommand , "");
			Globals.gReturn_Status = "Error Insert Stock Summary " + lParam.getPeriod() +";"+ lParam.getProductID() +";"+ lParam.getPlantID() +";"+ lParam.getWarehouseID() +";"+ lParam.getBatch_No();

		}
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlStockSummary> LoadStockSummary(String lUser) {
		List<model.mdlStockSummary> listmdlStockSummary = new ArrayList<model.mdlStockSummary>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM` "
					+ "FROM stock_summary WHERE PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
				mdlStockSummary.setPeriod(jrs.getString("Period"));
				mdlStockSummary.setProductID(jrs.getString("ProductID"));
				mdlStockSummary.setPlantID(jrs.getString("PlantID"));
				mdlStockSummary.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStockSummary.setBatch_No(jrs.getString("Batch_No"));	
				mdlStockSummary.setQty(jrs.getString("Qty"));
				mdlStockSummary.setUOM(jrs.getString("UOM"));
				
				listmdlStockSummary.add(mdlStockSummary);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStockSummary", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null) {
					jrs.close();
				}
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStockSummary", "close opened connection protocol", lUser);
			}
		}

		return listmdlStockSummary;
	}
	
	public static String UpdateStockSummary(model.mdlStockSummary lParam,String luser)
	{
		Globals.gCommand = "Update VendorID : " + lParam.getPeriod() +";"+ lParam.getProductID() +";"+ lParam.getPlantID() +";"+ lParam.getWarehouseID() +";"+ lParam.getBatch_No();;
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM stock_summary "
					+ "WHERE `Period` = ? AND `ProductID`= ? AND `PlantID`= ? AND `WarehouseID`= ? AND `Batch_No` = ? LIMIT 1");
			jrs.setString(1,  lParam.getPeriod());
			jrs.setString(2,  lParam.getProductID());
			jrs.setString(3,  lParam.getPlantID());
			jrs.setString(4,  lParam.getWarehouseID());
			jrs.setString(5,  lParam.getBatch_No());
			
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("Qty", lParam.getQty());
			jrs.updateString("UOM", lParam.getUOM());
			jrs.updateString("Created_by", luser);
			jrs.updateString("Created_at", LocalDateTime.now().toString());
			jrs.updateString("Updated_by", luser);
			jrs.updateString("Updated_at", LocalDateTime.now().toString());;
			jrs.updateRow();
				
			jrs.close();
			Globals.gReturn_Status = "Success Update Stock Summary ";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStockSummary", Globals.gCommand , "");
			Globals.gReturn_Status = "Error Update Stock Summary";
		}
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static model.mdlStockSummary LoadStockSummaryByKey(model.mdlStockSummary lParamStockSummary, String lUser) {
		model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
		String lCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			//jrs.setCommand("SELECT Period, ProductID, PlantID, WarehouseID, Batch_No,Qty FROM stock_summary WHERE Period=? AND ProductID=? AND PlantID=? AND WarehouseID=? AND Batch_No=? ORDER BY Updated_at DESC LIMIT 1");
			jrs.setCommand("SELECT Period, ProductID, PlantID, WarehouseID, Batch_No,Qty FROM stock_summary WHERE ProductID=? AND PlantID=? AND WarehouseID=? AND Batch_No=? ORDER BY Updated_at DESC LIMIT 1");
			//jrs.setString(1, lParamStockSummary.getPeriod());
			jrs.setString(1, lParamStockSummary.getProductID());
			jrs.setString(2, lParamStockSummary.getPlantID());
			jrs.setString(3, lParamStockSummary.getWarehouseID());
			jrs.setString(4, lParamStockSummary.getBatch_No());
			
			lCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				
				mdlStockSummary.setPeriod(jrs.getString("Period"));
				mdlStockSummary.setProductID(jrs.getString("ProductID"));
				mdlStockSummary.setPlantID(jrs.getString("PlantID"));
				mdlStockSummary.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStockSummary.setBatch_No(jrs.getString("Batch_No"));
				mdlStockSummary.setQty(jrs.getString("Qty"));
				
				//listmdlBatch.add(mdlBatch);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStockSummaryByKey", lCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadStockSummaryByKey", "close opened connection protocol", lUser);
			}
		}

		return mdlStockSummary;
	}
	
	//<<001 Nanda
	public static String GetPeriod(model.mdlStockSummary lParamStockSummary, String lUser) {
		String lPeriod = "";
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
		jrs = database.RowSetAdapter.getJDBCRowSet();
		
		Globals.gCommand = "SELECT Period, ProductID, PlantID, WarehouseID, Batch_No,Qty FROM stock_summary WHERE ProductID=? AND PlantID=? AND WarehouseID=? AND Batch_No=? ORDER BY Updated_at DESC LIMIT 1";
		jrs.setCommand(Globals.gCommand);
		jrs.setString(1, lParamStockSummary.getProductID());
		jrs.setString(2, lParamStockSummary.getPlantID());
		jrs.setString(3, lParamStockSummary.getWarehouseID());
		jrs.setString(4, lParamStockSummary.getBatch_No());
		jrs.execute();
		while(jrs.next()){
			lPeriod = jrs.getString("Period");
		}
		
		jrs.close();	
		}
		catch(Exception ex){
		LogAdapter.InsertLogExc(ex.toString(), "GetPeriod", Globals.gCommand , lUser);
		}
			return lPeriod;
	}
	//>>
	
	public static int LoadStockSummaryByProductForConfirmation(String lProductID,String lPlantID,String lWarehouseID, String lQtyBaseBOMDetail, String lOrderTypeID, String lOrderNo) throws Exception {
//		List<model.mdlStockSummary> listmdlStockSummary = new ArrayList<model.mdlStockSummary>();
		int StockProduct = 0;
		
		Connection connection = database.RowSetAdapter.getConnection();
		PreparedStatement pstmLoadProductStock = null;
		ResultSet jrs = null;
		try{
			
			String sqlLoadProductStock = "SELECT  a.ProductID, a.Qty, a.UOM "
					+ "FROM stock_summary a "
					+ "INNER JOIN "
					+ "( "
					+ "SELECT  ProductID,Batch_No,PlantID,WarehouseID,MAX(Updated_at) max_date "
					+ "FROM stock_summary "
					+ "WHERE PlantID=? AND WarehouseID=? AND ProductID=? "
					+ "GROUP BY ProductID,Batch_No "
					+ ") b "
					+ "ON a.ProductID = b.ProductID AND a.Batch_No = b.Batch_No AND "
					+ "a.Updated_at = b.max_date AND "
					+ "a.PlantID = b.PlantID AND a.WarehouseID = b.WarehouseID "
					+ "INNER JOIN batch c ON c.Batch_No=a.Batch_No "
					+ "INNER JOIN bom_production_order d ON d.OrderTypeID=? AND d.OrderNo=? "
					+ "WHERE a.Qty <> 0 AND c.Expired_Date >= d.StartDate "
					+ "ORDER BY c.Expired_Date";
			
			Globals.gCommand = sqlLoadProductStock;
			
			pstmLoadProductStock = connection.prepareStatement(sqlLoadProductStock);
			
			pstmLoadProductStock.setString(1, lPlantID);
			pstmLoadProductStock.setString(2, lWarehouseID);
			pstmLoadProductStock.setString(3, lProductID);
			pstmLoadProductStock.setString(4, lOrderTypeID);
			pstmLoadProductStock.setString(5, lOrderNo);
			pstmLoadProductStock.addBatch();
			
			jrs = pstmLoadProductStock.executeQuery();
									
			while(jrs.next()){
//				model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
				
//				mdlStockSummary.setProductID(jrs.getString("ProductID"));
//				mdlStockSummary.setQty(jrs.getString("Qty"));
//				mdlStockSummary.setUOM(jrs.getString("UOM"));
				String Qty = jrs.getString("Qty");
				if(Qty.isEmpty())
					Qty = "0";
				
				StockProduct = Integer.parseInt(Qty)+StockProduct;
				
				if(StockProduct >= Integer.parseInt(lQtyBaseBOMDetail))
					break;
//				listmdlStockSummary.add(mdlStockSummary);
			}
//			jrs.close();
//			pstmLoadProductStock.close();			
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStockSummaryByProductForConfirmation", Globals.gCommand , Globals.user);
		}
		finally {
			 if (pstmLoadProductStock != null) {
				 pstmLoadProductStock.close();
			 }
			 if (connection != null) {
				 connection.close();
			 }
			 if (jrs != null) {
				 jrs.close();
			 }
		 }

		return StockProduct;
	}

	public static int LoadStockSummaryByProductForProduction(String lProductID,String lPlantID,String lWarehouseID, Integer lQtyBaseBOMDetail) {
		//List<model.mdlStockSummary> listmdlStockSummary = new ArrayList<model.mdlStockSummary>();
		int StockProduct = 0;
		
		Connection connection = null;
		PreparedStatement pstmLoadProductStock = null;
		ResultSet jrs = null;
		Globals.gCommand="";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadProductStock = "SELECT  a.ProductID, a.Qty, a.UOM "
					+ "FROM stock_summary a "
					+ "INNER JOIN "
					+ "( "
					+ "SELECT  ProductID,Batch_No,PlantID,WarehouseID,MAX(Updated_at) max_date "
					+ "FROM stock_summary "
					+ "WHERE PlantID=? AND WarehouseID=? AND ProductID=? "
					+ "GROUP BY ProductID,Batch_No "
					+ ") b "
					+ "ON a.ProductID = b.ProductID AND a.Batch_No = b.Batch_No AND "
					+ "a.Updated_at = b.max_date AND "
					+ "a.PlantID = b.PlantID AND a.WarehouseID = b.WarehouseID "
					+ "WHERE a.Qty <> 0";
			
			Globals.gCommand = sqlLoadProductStock;
			
			pstmLoadProductStock = connection.prepareStatement(sqlLoadProductStock);
			
			pstmLoadProductStock.setString(1, lPlantID);
			pstmLoadProductStock.setString(2, lWarehouseID);
			pstmLoadProductStock.setString(3, lProductID);
			pstmLoadProductStock.addBatch();
			
			jrs = pstmLoadProductStock.executeQuery();
									
			while(jrs.next()){
				String Qty = jrs.getString("Qty");
				if(Qty.isEmpty())
					Qty = "0";
				
				StockProduct = Integer.parseInt(Qty)+StockProduct;
				
				if(StockProduct >= lQtyBaseBOMDetail)
					break;
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStockSummaryByProductForProduction", Globals.gCommand , Globals.user);
		}
		finally {
			try {
				 if (pstmLoadProductStock != null) {
					 pstmLoadProductStock.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadStockSummaryByProductForProduction", "close opened connection protocol", Globals.user);
			}
		 }

		return StockProduct;
	}
	
	public static List<model.mdlStockSummary> LoadStockByComponentBatchExpiry(String lProductID,String lPlantID,String lWarehouseID, String lComponentQtyBase, String lOrderTypeID, String lOrderNo) throws Exception {
		List<model.mdlStockSummary> listmdlStockSummary = new ArrayList<model.mdlStockSummary>();
		int StockProduct = 0;
		
		Connection connection = database.RowSetAdapter.getConnection();
		PreparedStatement pstmLoadProductStock = null;
		ResultSet jrs = null;
		try{
			
			String sqlLoadProductStock = "SELECT a.ProductID,a.PlantID,a.WarehouseID,a.Batch_No, a.Qty, a.UOM "
					+ "FROM stock_summary a "
					+ "INNER JOIN "
					+ "( "
					+ "SELECT  ProductID,Batch_No,PlantID,WarehouseID,MAX(Updated_at) max_date "
					+ "FROM stock_summary "
					+ "WHERE PlantID=? AND WarehouseID=? AND ProductID=? "
					+ "GROUP BY ProductID,Batch_No "
					+ ") b "
					+ "ON a.ProductID = b.ProductID AND a.Batch_No = b.Batch_No AND "
					+ "a.Updated_at = b.max_date AND "
					+ "a.PlantID = b.PlantID AND a.WarehouseID = b.WarehouseID "
					+ "INNER JOIN batch c ON c.Batch_No=a.Batch_No "
					+ "INNER JOIN bom_production_order d ON d.OrderTypeID=? AND d.OrderNo=? "
					+ "WHERE a.Qty <> 0 AND c.Expired_Date >= d.StartDate "
					+ "ORDER BY c.Expired_Date";
			
			Globals.gCommand = sqlLoadProductStock;
			
			pstmLoadProductStock = connection.prepareStatement(sqlLoadProductStock);
			
			pstmLoadProductStock.setString(1, lPlantID);
			pstmLoadProductStock.setString(2, lWarehouseID);
			pstmLoadProductStock.setString(3, lProductID);
			pstmLoadProductStock.setString(4, lOrderTypeID);
			pstmLoadProductStock.setString(5, lOrderNo);
			pstmLoadProductStock.addBatch();
			
			jrs = pstmLoadProductStock.executeQuery();
									
			while(jrs.next()){
				model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
				
				mdlStockSummary.setProductID(jrs.getString("ProductID"));
				mdlStockSummary.setPlantID(jrs.getString("PlantID"));
				mdlStockSummary.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStockSummary.setBatch_No(jrs.getString("Batch_No"));
				mdlStockSummary.setQty(jrs.getString("Qty"));
				mdlStockSummary.setUOM(jrs.getString("UOM"));
				
				listmdlStockSummary.add(mdlStockSummary);
				
				String Qty = jrs.getString("Qty");
				if(Qty.isEmpty())
					Qty = "0";
				
				StockProduct = Integer.parseInt(Qty)+StockProduct;
				
				if(StockProduct >= Integer.parseInt(lComponentQtyBase))
					break;
			}
//			jrs.close();
//			pstmLoadProductStock.close();			
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStockByProductBatchExpiry", Globals.gCommand , Globals.user);
		}
		finally {
			 if (pstmLoadProductStock != null) {
				 pstmLoadProductStock.close();
			 }
			 if (connection != null) {
				 connection.close();
			 }
			 if (jrs != null) {
				 jrs.close();
			 }
		 }

		return listmdlStockSummary;
	}
	
	//<<002 Nanda
	public static List<model.mdlRptStock> LoadReportStock(String lPlant, String lWarehouse) {
		List<model.mdlRptStock> listmdlRptStock = new ArrayList<model.mdlRptStock>();
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM (SELECT * FROM stock_summary ORDER BY Period DESC) AS a "
					+ "WHERE PlantID = ? AND WarehouseID = ? GROUP BY Batch_No ORDER BY ProductID");
			jrs.setString(1, lPlant);
			jrs.setString(2, lWarehouse);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlRptStock mdlRptStock = new model.mdlRptStock();
				mdlRptStock.setPeriod(jrs.getString("Period"));
				mdlRptStock.setProductID(jrs.getString("ProductID"));
				mdlRptStock.setBatchNo(jrs.getString("Batch_No"));	
				mdlRptStock.setQty(jrs.getString("Qty"));
				mdlRptStock.setUOM(jrs.getString("UOM"));
				
				listmdlRptStock.add(mdlRptStock);
			}
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadReportStock", Globals.gCommand , Globals.user);
		}

		return listmdlRptStock;
	}
		
	@SuppressWarnings("resource")
	public static List<model.mdlRptStock> LoadRptBalancingQty(String lStartDate, String lEndDate) {
		List<model.mdlRptStock> listmdlRptStock = new ArrayList<model.mdlRptStock>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
						
			jrs.setCommand("SELECT `Period`,`ProductID`,`PlantID`,`WarehouseID`,`Batch_No`,`Qty`,`UOM` "
									+ "FROM (SELECT * FROM "
													+ "(SELECT `Period`,`ProductID`,`PlantID`,`WarehouseID`,`Batch_No`,`Qty`,`UOM`,`Updated_at` FROM stock_summary WHERE (DATE(`Updated_at`) BETWEEN ? AND DATE_ADD(?,INTERVAL 1 DAY)) AND PlantID IN ("+Globals.user_area+") ORDER BY Period DESC) AS a GROUP BY Batch_No,PlantID,WarehouseID "
																	+ "UNION "
										  + "SELECT * FROM "
										  			+ "(SELECT `Period`,`ProductID`,`PlantID`,`WarehouseID`,`Batch_No`,`Qty`,`UOM`,`Updated_at` FROM stock_summary WHERE (DATE(`Updated_at`) BETWEEN ? AND DATE_ADD(?,INTERVAL 1 DAY)) AND PlantID IN ("+Globals.user_area+") ORDER BY Period ASC) AS a  GROUP BY Batch_No,PlantID,WarehouseID "
										  + ") jt "
										  + "GROUP BY batch_No,period,PlantID,WarehouseID "
										  + "ORDER BY ProductID,WarehouseID");
			
			jrs.setString(1, lStartDate);
			jrs.setString(2, lEndDate);
			jrs.setString(3, lStartDate);
			jrs.setString(4, lEndDate);
			
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlRptStock mdlRptStock = new model.mdlRptStock();
				mdlRptStock.setPeriod(jrs.getString("Period"));
				mdlRptStock.setProductID(jrs.getString("ProductID"));
				mdlRptStock.setBatchNo(jrs.getString("Batch_No"));	
				mdlRptStock.setQty(jrs.getString("Qty"));
				mdlRptStock.setUOM(jrs.getString("UOM"));
				
				mdlRptStock.setPlantID(jrs.getString("PlantID"));
				mdlRptStock.setWarehouseID(jrs.getString("WarehouseID"));
				
				listmdlRptStock.add(mdlRptStock);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadRptBalancingQty", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadRptBalancingQty", "close opened connection protocol", Globals.user);
			}
		}
		
		return listmdlRptStock;
	}
	//>>
	
	public static int LoadQtyStock(String lProductID,String lPlantID) {
		int lQty = 0;
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT SUM(Qty) as Qty FROM stock_summary WHERE `ProductID` = ? AND `PlantID` = ? AND `WarehouseID` = 'Website' GROUP BY period ORDER BY Period Desc LIMIT 1;");
			jrs.setString(1,  lProductID);
			jrs.setString(2,  lPlantID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				lQty = Integer.parseInt(jrs.getString("Qty"));				
			}
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadQtyStock", Globals.gCommand , Globals.user);
		}

		return lQty;
	}
	
	//	,String lPlantID,String lTxtProductID - old parameter of updatestockapi
	public static List<model.mdlProductDetail> UpdateStockAPI(model.mdlParUptStockECM mdlParUptStockECM){
		model.mdlToken mdlToken = TokenAdapter.GetToken();
		//int lQty = LoadQtyStock(lTxtProductID,lPlantID);
		//mdlParUptStockECM.setQuantity(Integer.toString(lQty));
		
		mdlParUptStockECM.setToken(mdlToken.getToken());
		mdlParUptStockECM.setHarga("0");

		List<model.mdlProductDetail> listmdlProductDetail = new ArrayList<model.mdlProductDetail>();
		try {
			Client client = Client.create();
			
			Globals.gCommand = "http://dev.webarq.info:8081/kenmaster-be/api/product-detail/stock";
			WebResource webResource = client.resource(Globals.gCommand);
			
			Gson gson = new Gson();
			String json = gson.toJson(mdlParUptStockECM);
			
			ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);		
			
			Globals.gReturn_Status = response.getEntity(String.class);
			
			//Type TypeDepartment = new TypeToken<ArrayList<model.mdlDepartment>>(){}.getClass();
			listmdlProductDetail.add(gson.fromJson(Globals.gReturn_Status, model.mdlProductDetail.class));
		  } 
		catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStockAPI", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error GetToken";
		  }
		
		return listmdlProductDetail;
	}
	
	
//	public static model.mdlVendor LoadVendorbyID(String lPeriod, String lProductID, String lPlantID, String ) {
//	model.mdlVendor mdlVendor = new model.mdlVendor();
//	try{
//		
//		JdbcRowSet jrs = new JdbcRowSetImpl();
//		jrs = database.RowSetAdapter.getJDBCRowSet();	
//		jrs.setCommand("SELECT `Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM` FROM stock_summary WHERE VendorID = ?");
//		jrs.setString(1,  lVendorID);
//		Globals.gCommand = jrs.getCommand();
//		jrs.execute();
//		
//		mdlVendor.setVendorName(jrs.getString("VendorName"));
//		mdlVendor.setVendorAddress(jrs.getString("VendorAddress"));
//		mdlVendor.setPIC(jrs.getString("Pic"));
//		mdlVendor.setPhone(jrs.getString("Phone"));	
//		
//		jrs.close();		
//	}
//	catch(Exception ex){
//		LogAdapter.InsertLogExc(ex.toString(), "LoadVendorbyID", Globals.gCommand , "");
//	}
//
//	return mdlVendor;
//}
//
	
}
