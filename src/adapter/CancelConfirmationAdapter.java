package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import model.Globals;
import model.mdlBOMCancelConfirmation;
import model.mdlBOMConfirmationProductionDetail;
import model.mdlCancelTransaction;
import model.mdlInbound;

public class CancelConfirmationAdapter {

	public static String TransactionCancelConfirmation(mdlBOMCancelConfirmation lParamCancelConfirmation, List<mdlBOMConfirmationProductionDetail> lParamlistConfirmationDetail)
	{	
		Globals.gCommand = "";
		
			Connection connection = null;
			PreparedStatement pstmUpdateProductionOrder = null;
			PreparedStatement pstmUpdateFlagConfirmation = null;
			PreparedStatement pstmInsertCancelConfirmation = null;
			PreparedStatement pstmInsertCancelConfirmationDetail = null;
			PreparedStatement pstmInsertUpdateStockSummary = null;
			int newQty = 0;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlUpdateProductionOrder = "UPDATE bom_production_order SET Status='RELEASED',Updated_by=?,Updated_at=? WHERE OrderTypeID=? AND OrderNo=?";
				String sqlUpdateflagConfirmation = "UPDATE `bom_production_order_confirmation` SET `IsCancel`=1,`Updated_by`=?, `Updated_at`=? WHERE `ConfirmationID`=? AND `ConfirmationNo`=?;";
				String sqlInsertCancelConfirmation = "INSERT INTO bom_production_order_confirmation_cancel(`CancelConfirmationID`, `CancelConfirmationNo`, `CancelDate`, `ConfirmationID`, `ConfirmationNo`, `ProductID`, `Batch_No`, `Qty`, `UOM`, `QtyBaseUOM`, `BaseUOM` ,`Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertCancelConfirmationDetail = "INSERT INTO bom_production_order_detail_confirmation_cancel(`CancelConfirmationID`, `CancelConfirmationNo`, `ComponentLine`, `ComponentID`, `Batch_No`, `Qty`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertUpdateStockSummary = "INSERT INTO stock_summary(`Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
				pstmUpdateProductionOrder = connection.prepareStatement(sqlUpdateProductionOrder);
				pstmUpdateFlagConfirmation = connection.prepareStatement(sqlUpdateflagConfirmation);
				pstmInsertCancelConfirmation = connection.prepareStatement(sqlInsertCancelConfirmation);
				pstmInsertCancelConfirmationDetail = connection.prepareStatement(sqlInsertCancelConfirmationDetail);
				pstmInsertUpdateStockSummary = connection.prepareStatement(sqlInsertUpdateStockSummary);
				
				//Update Production Order proccess parameter
				pstmUpdateProductionOrder.setString(1, ValidateNull.NulltoStringEmpty(Globals.user));
				pstmUpdateProductionOrder.setString(2, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmUpdateProductionOrder.setString(3, ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getOrderTypeID()));
				pstmUpdateProductionOrder.setString(4, ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getOrderNo()));
				Globals.gCommand = pstmUpdateProductionOrder.toString();
				pstmUpdateProductionOrder.executeUpdate();
				
				//Update flag di Confirmation
				pstmUpdateFlagConfirmation.setString(1, ValidateNull.NulltoStringEmpty(Globals.user));
				pstmUpdateFlagConfirmation.setString(2, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmUpdateFlagConfirmation.setString(3, ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getConfirmationID()));
				pstmUpdateFlagConfirmation.setString(4, ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getConfirmationNo()));
				Globals.gCommand = pstmUpdateFlagConfirmation.toString();
				pstmUpdateFlagConfirmation.executeUpdate();
				
				//insert cancel confirmation process parameter
				pstmInsertCancelConfirmation.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getCancelConfirmationID()));
				pstmInsertCancelConfirmation.setString(2,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getCancelConfirmationNo()));
				pstmInsertCancelConfirmation.setString(3,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getDate()));
				pstmInsertCancelConfirmation.setString(4,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getConfirmationID()));
				pstmInsertCancelConfirmation.setString(5,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getConfirmationNo()));
				pstmInsertCancelConfirmation.setString(6,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getProductID()));
				pstmInsertCancelConfirmation.setString(7,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getBatchNo()));
				pstmInsertCancelConfirmation.setString(8,  ValidateNull.NulltoStringEmpty("-" + lParamCancelConfirmation.getQty()));
				pstmInsertCancelConfirmation.setString(9,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getUOM()));
				pstmInsertCancelConfirmation.setString(10,  ValidateNull.NulltoStringEmpty("-" + lParamCancelConfirmation.getQtyBaseUOM()));
				pstmInsertCancelConfirmation.setString(11,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getBaseUOM()));
				pstmInsertCancelConfirmation.setString(12,  ValidateNull.NulltoStringEmpty(Globals.user)); 
				pstmInsertCancelConfirmation.setString(13,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertCancelConfirmation.setString(14,  ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertCancelConfirmation.setString(15, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertCancelConfirmation.toString();
				pstmInsertCancelConfirmation.executeUpdate();
				
				//Declare mdlStockSummary for cancel confirmation header process
				model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
				mdlStockSummary.setProductID(lParamCancelConfirmation.getProductID());
				mdlStockSummary.setPlantID(lParamCancelConfirmation.getPlantID());
				mdlStockSummary.setWarehouseID(lParamCancelConfirmation.getWarehouseID());
				mdlStockSummary.setBatch_No(lParamCancelConfirmation.getBatchNo());
				mdlStockSummary.setPeriod(lParamCancelConfirmation.getDate().substring(0, 7).replace("-", ""));
				mdlStockSummary.setUOM(lParamCancelConfirmation.getBaseUOM());
				Globals.gCommand = mdlStockSummary.toString();
				model.mdlStockSummary mdlStockSummary2 =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,Globals.user);
				if(mdlStockSummary2.getQty()==null){
					returnImmediately(connection);
					Globals.gReturn_Status = "Produk : "+lParamCancelConfirmation.getProductID()+"(batch no:"+lParamCancelConfirmation.getBatchNo()+",plant:"+lParamCancelConfirmation.getPlantID()+",warehouse:"+lParamCancelConfirmation.getWarehouseID()+") tidak ditemukan di stok gudang";
					
					return Globals.gReturn_Status;
				}
				else{
					//check if the existing qty is available for cancellation 
					if( Integer.parseInt(mdlStockSummary2.getQty()) < Integer.parseInt(lParamCancelConfirmation.getQtyBaseUOM()) )
					{
						LogAdapter.InsertLogExc("The product quantity that you want to cancel is more than the available quantity in the database", "InsertCancelConfirmation", Globals.gCommand , Globals.user);
						
						returnImmediately(connection);
						Globals.gReturn_Status = "Stok untuk produk "+lParamCancelConfirmation.getProductID()+"(batch no:"+lParamCancelConfirmation.getBatchNo()+",plant:"+lParamCancelConfirmation.getPlantID()+",warehouse:"+lParamCancelConfirmation.getWarehouseID()+") tidak mencukupi untuk dilakukan proses pembatalan";
						
						return Globals.gReturn_Status;
					}
					
					newQty = Integer.parseInt(mdlStockSummary2.getQty()) - Integer.parseInt(lParamCancelConfirmation.getQtyBaseUOM());
					mdlStockSummary.setQty(String.valueOf(newQty));
					
					pstmInsertUpdateStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPeriod()));
					pstmInsertUpdateStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
					pstmInsertUpdateStockSummary.setString(3,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
					pstmInsertUpdateStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
					pstmInsertUpdateStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
					pstmInsertUpdateStockSummary.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty())); 
					pstmInsertUpdateStockSummary.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
					pstmInsertUpdateStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertUpdateStockSummary.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertUpdateStockSummary.setString(10,  ValidateNull.NulltoStringEmpty(Globals.user)); 
					pstmInsertUpdateStockSummary.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertUpdateStockSummary.setString(12,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
					pstmInsertUpdateStockSummary.setString(13,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
					pstmInsertUpdateStockSummary.setString(14,  ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertUpdateStockSummary.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmInsertUpdateStockSummary.toString();
					pstmInsertUpdateStockSummary.executeUpdate();
					//end of cancel confirmation header process
				}
				
				
				//insert cancel confirmation detail process parameter and update the stock summary after cancellation
				for(mdlBOMConfirmationProductionDetail lParamConfirmationDetail : lParamlistConfirmationDetail)
				{
					pstmInsertCancelConfirmationDetail.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getCancelConfirmationID()));
					pstmInsertCancelConfirmationDetail.setString(2,  ValidateNull.NulltoStringEmpty(lParamCancelConfirmation.getCancelConfirmationNo()));
					pstmInsertCancelConfirmationDetail.setString(3,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getComponentLine()));
					pstmInsertCancelConfirmationDetail.setString(4,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getComponentID()));
					pstmInsertCancelConfirmationDetail.setString(5,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getBatchNo()));
					pstmInsertCancelConfirmationDetail.setString(6,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getQty()));
					pstmInsertCancelConfirmationDetail.setString(7,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getUOM()));
					pstmInsertCancelConfirmationDetail.setString(8,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getQtyBaseUOM()));
					pstmInsertCancelConfirmationDetail.setString(9, ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getBaseUOM()));
					pstmInsertCancelConfirmationDetail.setString(10, ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertCancelConfirmationDetail.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertCancelConfirmationDetail.setString(12, ValidateNull.NulltoStringEmpty(Globals.user));
					pstmInsertCancelConfirmationDetail.setString(13, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmInsertCancelConfirmationDetail.toString();
					pstmInsertCancelConfirmationDetail.executeUpdate();
					
					
					//Declare mdlStockSummary for cancel confirmation detail process
					mdlStockSummary = new model.mdlStockSummary();
					mdlStockSummary.setProductID(lParamConfirmationDetail.getComponentID());
					mdlStockSummary.setPlantID(lParamCancelConfirmation.getPlantID());
					mdlStockSummary.setWarehouseID(lParamCancelConfirmation.getWarehouseID());
					mdlStockSummary.setBatch_No(lParamConfirmationDetail.getBatchNo());
					mdlStockSummary.setPeriod(lParamCancelConfirmation.getDate().substring(0, 7).replace("-", ""));
					mdlStockSummary.setUOM(lParamConfirmationDetail.getBaseUOM());
					Globals.gCommand = mdlStockSummary.toString();
					mdlStockSummary2 =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,Globals.user);
					if(mdlStockSummary2.getQty()==null){
						returnImmediately(connection);
						Globals.gReturn_Status = "Komponen : "+mdlStockSummary.getProductID()+"(batch no:"+mdlStockSummary.getBatch_No()+",plant:"+mdlStockSummary.getPlantID()+",warehouse:"+mdlStockSummary.getWarehouseID()+") tidak ditemukan di stok gudang";
						
						return Globals.gReturn_Status;
					}
					else{
						//check if the detail contain - or not ( - means we have to decrease the component quantity in stock summary for cancellation process
						if(lParamConfirmationDetail.getQty().contains("-"))
						{
							//check if the existing qty is available for cancellation
							if( Integer.parseInt(mdlStockSummary2.getQty()) < Integer.parseInt(lParamConfirmationDetail.getQtyBaseUOM().replace("-", "")) )
							{
								LogAdapter.InsertLogExc("The component quantity that you want to cancel is more than the available quantity in the database", "InsertCancelConfirmation", Globals.gCommand , Globals.user);
								
								returnImmediately(connection);
								Globals.gReturn_Status = "Stok untuk komponen "+lParamConfirmationDetail.getComponentID()+"(batch no:"+lParamConfirmationDetail.getBatchNo()+",plant:"+mdlStockSummary.getPlantID()+",warehouse:"+mdlStockSummary.getWarehouseID()+") tidak mencukupi untuk dilakukan proses pembatalan";
								
								return Globals.gReturn_Status;
							}
							else{
								newQty = Integer.parseInt(mdlStockSummary2.getQty()) - Integer.parseInt(lParamConfirmationDetail.getQtyBaseUOM().replace("-", ""));
								mdlStockSummary.setQty(String.valueOf(newQty));
								
								pstmInsertUpdateStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPeriod()));
								pstmInsertUpdateStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
								pstmInsertUpdateStockSummary.setString(3,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
								pstmInsertUpdateStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
								pstmInsertUpdateStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
								pstmInsertUpdateStockSummary.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty())); 
								pstmInsertUpdateStockSummary.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
								pstmInsertUpdateStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(Globals.user));
								pstmInsertUpdateStockSummary.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								pstmInsertUpdateStockSummary.setString(10,  ValidateNull.NulltoStringEmpty(Globals.user)); 
								pstmInsertUpdateStockSummary.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								pstmInsertUpdateStockSummary.setString(12,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
								pstmInsertUpdateStockSummary.setString(13,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
								pstmInsertUpdateStockSummary.setString(14,  ValidateNull.NulltoStringEmpty(Globals.user));
								pstmInsertUpdateStockSummary.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								Globals.gCommand = pstmInsertUpdateStockSummary.toString();
								pstmInsertUpdateStockSummary.executeUpdate();
							}
						}
						else //if not - , it means that we have to increase the component quantity in stock summary for cancellation process
						{
							newQty = Integer.parseInt(mdlStockSummary2.getQty()) + Integer.parseInt(lParamConfirmationDetail.getQtyBaseUOM());
							mdlStockSummary.setQty(String.valueOf(newQty));
							
							pstmInsertUpdateStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPeriod()));
							pstmInsertUpdateStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
							pstmInsertUpdateStockSummary.setString(3,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
							pstmInsertUpdateStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
							pstmInsertUpdateStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
							pstmInsertUpdateStockSummary.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty())); 
							pstmInsertUpdateStockSummary.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
							pstmInsertUpdateStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(Globals.user));
							pstmInsertUpdateStockSummary.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
							pstmInsertUpdateStockSummary.setString(10,  ValidateNull.NulltoStringEmpty(Globals.user)); 
							pstmInsertUpdateStockSummary.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
							pstmInsertUpdateStockSummary.setString(12,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
							pstmInsertUpdateStockSummary.setString(13,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
							pstmInsertUpdateStockSummary.setString(14,  ValidateNull.NulltoStringEmpty(Globals.user));
							pstmInsertUpdateStockSummary.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
							Globals.gCommand = pstmInsertUpdateStockSummary.toString();
							pstmInsertUpdateStockSummary.executeUpdate();
						}
						//end of cancel confirmation detail process
					}
					
				} //end of for looping
					
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Insert Cancel Confirmation Production";
			}
			catch (Exception e ) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionCancelConfirmation", Globals.gCommand , Globals.user);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertCancelConfirmation", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
			        if (pstmInsertCancelConfirmation != null) {
			        	pstmInsertCancelConfirmation.close();
			        }
			        if (pstmInsertCancelConfirmationDetail != null) {
			        	pstmInsertCancelConfirmationDetail.close();
			        }
			        if (pstmInsertUpdateStockSummary != null) {
			        	pstmInsertUpdateStockSummary.close();
			        }
			        if (pstmUpdateFlagConfirmation!= null) {
			        	pstmUpdateFlagConfirmation.close();
			        }
			        if (pstmUpdateProductionOrder!= null) {
			        	pstmUpdateProductionOrder.close();
			        }
			        connection.setAutoCommit(true);
			   		 if (connection != null) {
			   			 connection.close();
			   		 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertCancelConfirmation", "close opened connection protocol", Globals.user);
				}
		    }
			
		return Globals.gReturn_Status;
	}
	
	public static void returnImmediately(Connection connection) {
		if (connection != null) {
		    try {
		        System.err.print("Transaction is being rolled back");
		        connection.rollback();
		    } catch(SQLException excep) {
		    	LogAdapter.InsertLogExc(excep.toString(), "TransactionInsertCancelConfirmation", Globals.gCommand , Globals.user);
				Globals.gReturn_Status = "Database Error";
		    }
		}
	}
	
	public static List<model.mdlBOMCancelConfirmationDetail> LoadBOMCancelConfirmationDetail(String lCancelConfirmationID, String lCancelConfirmationNo) {
		List<model.mdlBOMCancelConfirmationDetail> listBOMCancelConfirmationDetail = new ArrayList<model.mdlBOMCancelConfirmationDetail>();
		
		Connection connection = null;
		PreparedStatement pstmLoadCancelConfirmationDetail = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadCancelConfirmationDetail = "SELECT a.ComponentLine,a.ComponentID,a.Batch_No,a.Qty,a.UOM,a.Qty_BaseUOM,a.BaseUOM "
					+ "FROM bom_production_order_detail_confirmation_cancel a "
					+ "WHERE a.CancelConfirmationID = ? AND a.CancelConfirmationNo = ? "
					+ "ORDER BY a.ComponentLine";
			
			pstmLoadCancelConfirmationDetail = connection.prepareStatement(sqlLoadCancelConfirmationDetail);
			
			pstmLoadCancelConfirmationDetail.setString(1, lCancelConfirmationID);
			pstmLoadCancelConfirmationDetail.setString(2, lCancelConfirmationNo);
			pstmLoadCancelConfirmationDetail.addBatch();
			Globals.gCommand = pstmLoadCancelConfirmationDetail.toString();
			
			jrs = pstmLoadCancelConfirmationDetail.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMCancelConfirmationDetail BOMCancelConfirmationDetail = new model.mdlBOMCancelConfirmationDetail();
				
				BOMCancelConfirmationDetail.setComponentLine(jrs.getString("ComponentLine"));
				BOMCancelConfirmationDetail.setComponentID(jrs.getString("ComponentID"));
				BOMCancelConfirmationDetail.setBatchNo(jrs.getString("Batch_No"));
				
				if(jrs.getString("Qty").contains("-"))
				{
					BOMCancelConfirmationDetail.setQty(jrs.getString("Qty"));
					BOMCancelConfirmationDetail.setQtyBaseUOM(jrs.getString("Qty_BaseUOM"));
				}
				else
				{
					BOMCancelConfirmationDetail.setQty("+" + jrs.getString("Qty"));
					BOMCancelConfirmationDetail.setQtyBaseUOM("+" + jrs.getString("Qty_BaseUOM"));
				}
					
				BOMCancelConfirmationDetail.setUOM(jrs.getString("UOM"));
				BOMCancelConfirmationDetail.setBaseUOM(jrs.getString("BaseUOM"));
				
				listBOMCancelConfirmationDetail.add(BOMCancelConfirmationDetail);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOMCancelConfirmationDetail", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstmLoadCancelConfirmationDetail != null) {
					 pstmLoadCancelConfirmationDetail.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadBOMCancelConfirmationDetail", "close opened connection protocol", Globals.user);
			}
		 }

		return listBOMCancelConfirmationDetail;
	}
}
