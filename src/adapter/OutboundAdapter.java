package adapter;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import model.Globals;

/** Documentation
 * 002 nanda
 */

public class OutboundAdapter {
	@SuppressWarnings("resource")
	public static model.mdlOutbound LoadOutboundbyID(String lDocNumber) {
		model.mdlOutbound mdlOutbound = new model.mdlOutbound();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT `DocNumber`, `Date`, `CustomerID`, `PlantID`, `WarehouseID` FROM outbound WHERE `DocNumber` = ?");
			jrs.setString(1,  lDocNumber);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next())
			{
			mdlOutbound.setDocNumber(lDocNumber);
			mdlOutbound.setDate(jrs.getString("Date"));
			mdlOutbound.setCustomerID(jrs.getString("CustomerID"));
			mdlOutbound.setPlantID(jrs.getString("PlantID"));
			mdlOutbound.setWarehouseID(jrs.getString("WarehouseID"));	
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadOutboundbyID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadOutboundbyID", "close opened connection protocol", Globals.user);
			}
		}

		return mdlOutbound;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlOutbound> LoadOutbound(String lUser) {
		List<model.mdlOutbound> listmdlOutbound = new ArrayList<model.mdlOutbound>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `DocNumber`, `Date`, `CustomerID`, `PlantID`, `WarehouseID` "
					+ "FROM outbound WHERE PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlOutbound mdlOutbound = new model.mdlOutbound();
				mdlOutbound.setDocNumber(jrs.getString("DocNumber"));
				
				String newDate=helper.ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlOutbound.setDate(newDate);
				
				mdlOutbound.setCustomerID(jrs.getString("CustomerID"));
				mdlOutbound.setPlantID(jrs.getString("PlantID"));
				mdlOutbound.setWarehouseID(jrs.getString("WarehouseID"));					
				
				listmdlOutbound.add(mdlOutbound);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadOutbound", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadOutbound", "close opened connection protocol", lUser);
			}
		}

		return listmdlOutbound;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlOutbound> LoadNonCancelOutbound(String lUser) {
		List<model.mdlOutbound> listmdlOutbound = new ArrayList<model.mdlOutbound>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `DocNumber`, `Date`, `CustomerID`, `PlantID`, `WarehouseID` FROM outbound "
					+ "WHERE `IsCancel`=0 AND PlantID IN ("+Globals.user_area+") "
					+ "ORDER BY Date");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlOutbound mdlOutbound = new model.mdlOutbound();
				mdlOutbound.setDocNumber(jrs.getString("DocNumber"));
				
				String newDate = ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlOutbound.setDate(newDate);
				
				mdlOutbound.setCustomerID(jrs.getString("CustomerID"));
				mdlOutbound.setPlantID(jrs.getString("PlantID"));
				mdlOutbound.setWarehouseID(jrs.getString("WarehouseID"));					
				
				listmdlOutbound.add(mdlOutbound);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadOutbound", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadOutbound", "close opened connection protocol", lUser);
			}
		}

		return listmdlOutbound;
	}
	
	@SuppressWarnings("resource")
	public static String InsertOutbound(model.mdlOutbound lParam,String luser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();			
			
			jrs.setCommand("SELECT `DocNumber`, `Date`, `CustomerID`, `PlantID`, `WarehouseID`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM outbound LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("DocNumber",lParam.getDocNumber());
			jrs.updateString("Date",lParam.getDate());
			jrs.updateString("CustomerID",lParam.getCustomerID());
			jrs.updateString("PlantID",lParam.getPlantID());
			jrs.updateString("WarehouseID",lParam.getWarehouseID());
			jrs.updateString("Created_by",luser);
			jrs.updateString("Created_at",LocalDateTime.now().toString());
			jrs.updateString("Updated_by",luser);
			jrs.updateString("Updated_at",LocalDateTime.now().toString());
			
			jrs.insertRow();
			Globals.gCommand = jrs.getCommand();
			
			Globals.gReturn_Status = "Success Insert Outbound";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertOutbound", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Outbound" + lParam.getDocNumber();
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertOutbound", "close opened connection protocol", Globals.user);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	public static String UpdateOutbound(model.mdlOutbound lParam, String luser)
	{
		Globals.gCommand = "Update DocNumber : " + lParam.getDocNumber();
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `DocNumber`, `Date`, `CustomerID`, `PlantID`, `WarehouseID`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM outbound WHERE `DocNumber` = ? LIMIT 1");
			jrs.setString(1,  lParam.getDocNumber());
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("Date", lParam.getDate());
			jrs.updateString("CustomerID", lParam.getCustomerID());
			jrs.updateString("PlantID", lParam.getPlantID());
			jrs.updateString("WarehouseID", lParam.getWarehouseID());
			jrs.updateString("Updated_by",LocalDateTime.now().toString());
			jrs.updateString("Updated_at",luser);
			jrs.updateRow();
				
			jrs.close();
			Globals.gReturn_Status = "Success Update Outbound ";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateOutbound", Globals.gCommand , "");
			Globals.gReturn_Status = "Error Update Outbound";
		}
		return Globals.gReturn_Status;
	}
	
	public static String DeleteOutbound(String lDocNumber)
	{
		Globals.gCommand = "Delete DocNumber : " + lDocNumber;
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT `DocNumber`, `Date`, `CustomerID`, `PlantID`, `WarehouseID`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM vendor WHERE `DocNumber` = ? LIMIT 1");
			jrs.setString(1, lDocNumber);
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			jrs.close();
			Globals.gReturn_Status = "Success Delete Outbound ";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteOutbound", Globals.gCommand , "");
			Globals.gReturn_Status = "Error Delete Outbound" + ex.toString();
		}
		return Globals.gReturn_Status;
	}

	//<<001 Nanda
		public static String GenerateDocNoOutbound()
		{
		String generateDate = LocalDateTime.now().toString();
		String lYear = generateDate.substring(0, 4);
		String txtDocNo =  "";
		model.mdlTraceCode lGetTraceCode = TraceCodeAdapter.LoadTraceCodeDocNo(lYear,"Outbound");

		if(lGetTraceCode.TraceCode == null)
		{
		txtDocNo = "K"+ generateDate.substring(2, 4) + "0000001";
		}
		else
		{
		int lRunningNumber = Integer.parseInt(lGetTraceCode.TraceCode.substring(3,10));
		String lnewRunningNumber = String.format("%07d", lRunningNumber + 1);	
		txtDocNo = "K"+ generateDate.substring(2, 4)+ lnewRunningNumber;
		}

		return txtDocNo;
		}
		//>>
		
		//<<002 Nanda
		@SuppressWarnings("resource")
		public static List<model.mdlRptGoodIssue> LoadReportGoodIssue(String lStartDate, String lEndDate) {
			List<model.mdlRptGoodIssue> mdlRptGoodIssuelist = new ArrayList<model.mdlRptGoodIssue>();
			JdbcRowSet jrs = new JdbcRowSetImpl();
			Globals.gCommand = "";
			try{
				jrs = database.RowSetAdapter.getJDBCRowSet();
						
				jrs.setCommand("SELECT a.DocNumber, a.Date, a.CustomerID, a.PlantID, a.WarehouseID, b.DocLine, "
						+ "b.ProductID, b.Qty_UOM, b.UOM, b.Qty_BaseUOM, b.BaseUOM, b.Batch_No, b.Packing_No "
						+ "FROM outbound a INNER JOIN outbound_detail b ON a.DocNumber = b.DocNumber "
						+ "WHERE (DATE(a.Date) BETWEEN ? AND DATE_ADD(?,INTERVAL 1 DAY)) AND a.PlantID IN ("+Globals.user_area+")");
				jrs.setString(1, lStartDate);
				jrs.setString(2, lEndDate);
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
												
				while(jrs.next()){
					model.mdlRptGoodIssue mdlRptGoodIssue = new model.mdlRptGoodIssue();
					mdlRptGoodIssue.setDocNo(jrs.getString("DocNumber"));
					mdlRptGoodIssue.setDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
					mdlRptGoodIssue.setCustomerID(jrs.getString("CustomerID"));	
					mdlRptGoodIssue.setPlantID(jrs.getString("PlantID"));
					mdlRptGoodIssue.setWarehouseID(jrs.getString("WarehouseID"));
					mdlRptGoodIssue.setDocLine(jrs.getString("DocLine"));
					mdlRptGoodIssue.setProductID(jrs.getString("ProductID"));
					mdlRptGoodIssue.setQtyUOM(jrs.getString("Qty_UOM"));
					mdlRptGoodIssue.setUOM(jrs.getString("UOM"));	
					mdlRptGoodIssue.setQtyBaseUOM(jrs.getString("Qty_BaseUOM"));
					mdlRptGoodIssue.setBaseUOM(jrs.getString("BaseUOM"));
					mdlRptGoodIssue.setBatchNo(jrs.getString("Batch_No"));
					mdlRptGoodIssue.setPackingNo(jrs.getString("Packing_No"));
							
					mdlRptGoodIssuelist.add(mdlRptGoodIssue);
				}		
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadReportGoodIssue", Globals.gCommand , Globals.user);
			}
			finally{
				try{
					if(jrs!=null)
						jrs.close();
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "LoadReportGoodIssue", "close opened connection protocol", Globals.user);
				}
			}

			return mdlRptGoodIssuelist;
		}
		
		public static String GetRptCountOutbound(String lStartDate, String lEndDate) {
			String lResult = "";
			JdbcRowSet jrs = new JdbcRowSetImpl();
			Globals.gCommand = "";
			try{
				jrs = database.RowSetAdapter.getJDBCRowSet();
						
				jrs.setCommand("SELECT COUNT(`DocNumber`) AS DocNumber "
						+ "FROM `outbound` "
						+ "WHERE (DATE(`Updated_at`) BETWEEN ? AND DATE_ADD(?,INTERVAL 1 DAY)) AND PlantID IN ("+Globals.user_area+")");
				jrs.setString(1, lStartDate);
				jrs.setString(2, lEndDate);
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
												
				while(jrs.next()){
					lResult = jrs.getString("DocNumber");
				}	
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "GetRptCountOutbound", Globals.gCommand , Globals.user);
			}
			finally{
				try{
					if(jrs!=null)
						jrs.close();
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "GetRptCountOutbound", "close opened connection protocol", Globals.user);
				}
			}

				return lResult;
			}
		
		//>>

}
