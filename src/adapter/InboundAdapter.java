package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlInbound;
import model.mdlPlant;

/** Documentation
 * 002 nanda
 */

public class InboundAdapter {

	@SuppressWarnings("resource")
	public static List<model.mdlInbound> LoadInbound (String lUser) {
		List<model.mdlInbound> listmdlInbound = new ArrayList<model.mdlInbound>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT a.DocNumber,a.Date,a.VendorID,a.PlantID,a.WarehouseID,a.DoorID,a.MaterialStagingAreaID,a.WMProcess_Status, "
					+ "b.WMS_Status,b.UpdateWMFirst "
					+ "FROM inbound a "
					+ "INNER JOIN warehouse b ON b.PlantID=a.PlantID AND b.WarehouseID=a.WarehouseID "
					+ "WHERE a.PlantID IN ("+Globals.user_area+") "
					+ "ORDER BY a.Date");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlInbound mdlInbound = new model.mdlInbound();
				mdlInbound.setDocNumber(jrs.getString("DocNumber"));
				
				String Date = jrs.getString("Date");
				Date initDate = new SimpleDateFormat("yyyy-MM-dd").parse(Date);
			    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
			    String newDate = formatter.format(initDate);
				mdlInbound.setDate(newDate);
				
				mdlInbound.setVendorID(jrs.getString("VendorID"));
				mdlInbound.setPlantID(jrs.getString("PlantID"));
				mdlInbound.setWarehouseID(jrs.getString("WarehouseID"));
				mdlInbound.setDoorID(jrs.getString("DoorID"));
				mdlInbound.setStagingAreaID(jrs.getString("MaterialStagingAreaID"));
				mdlInbound.setWMStatus(jrs.getString("WMS_Status"));
				mdlInbound.setUpdateWMFirst(jrs.getString("UpdateWMFirst"));
				mdlInbound.setInboundStatus(jrs.getString("WMProcess_Status"));
				
				listmdlInbound.add(mdlInbound);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadInbound", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadInbound", "close opened connection protocol", lUser);
			}
		}
		
		return listmdlInbound;
	}

	@SuppressWarnings("resource")
	public static model.mdlInbound LoadInboundbyDocNo (String lUser,String lDocNo) {
		//List<model.mdlInbound> listmdlInbound = new ArrayList<model.mdlInbound>();
		model.mdlInbound mdlInbound = new model.mdlInbound();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DocNumber,Date,VendorID,PlantID,WarehouseID FROM inbound WHERE DocNumber=?");
			jrs.setString(1, lDocNo);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			
			while(jrs.next()){
				
				mdlInbound.setPlantID(jrs.getString("PlantID"));
				mdlInbound.setWarehouseID(jrs.getString("WarehouseID"));
				
				//listmdlInbound.add(mdlInbound);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadInboundbyDocNo", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadInboundbyDocNo", "close opened connection protocol", lUser);
			}
		}
		
		return mdlInbound;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlInbound> LoadNonCancelInbound (String lUser) {
		List<model.mdlInbound> listmdlInbound = new ArrayList<model.mdlInbound>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DocNumber,Date,VendorID,PlantID,WarehouseID "
					+ "FROM inbound "
					+ "WHERE IsCancel=0 AND PlantID IN ("+Globals.user_area+") ORDER BY Date");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlInbound mdlInbound = new model.mdlInbound();
				mdlInbound.setDocNumber(jrs.getString("DocNumber"));
				
				String newDate=ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlInbound.setDate(newDate);
				
				mdlInbound.setVendorID(jrs.getString("VendorID"));
				mdlInbound.setPlantID(jrs.getString("PlantID"));
				mdlInbound.setWarehouseID(jrs.getString("WarehouseID"));
				
				listmdlInbound.add(mdlInbound);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadNonCancelInbound", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadNonCancelInbound", "close opened connection protocol", lUser);
			}
		}
		
		return listmdlInbound;
	}
	
	@SuppressWarnings("resource")
	public static String InsertInbound(mdlInbound lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();			
			
			jrs.setCommand("SELECT DocNumber,Date,VendorID,PlantID,WarehouseID,DoorID,MaterialStagingAreaID,WMProcess_Status,Created_by,Created_at,Updated_by,Updated_at FROM inbound LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("DocNumber",lParam.getDocNumber());
			jrs.updateString("Date", lParam.getDate());
			jrs.updateString("VendorID", lParam.getVendorID());
			jrs.updateString("PlantID",lParam.getPlantID());
			jrs.updateString("WarehouseID", lParam.getWarehouseID());
			jrs.updateString("DoorID", lParam.getDoorID());
			jrs.updateString("MaterialStagingAreaID", lParam.getStagingAreaID());
			if(lParam.getWMStatus().contentEquals("1"))
				jrs.updateString("WMProcess_Status", "Req WM Process");
			else
				jrs.updateString("WMProcess_Status", "");
			jrs.updateString("Created_by", lUser);
			jrs.updateString("Created_at", LocalDateTime.now().toString());
			jrs.updateString("Updated_by", lUser);
			jrs.updateString("Updated_at", LocalDateTime.now().toString());
			
			jrs.insertRow();
			
			Globals.gCommand = jrs.getCommand();
			
			Globals.gReturn_Status = "Success Insert Inbound";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertInbound", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Insert Inbound";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertInbound", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}

	public static String UpdateInbound(mdlInbound lParam, String lUser)
	{
		Globals.gCommand = "Update Inbound Doc : " + lParam.getDocNumber();
		
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DocNumber,Date,VendorID,PlantID,WarehouseID,Created_by,Created_at,Updated_by,Updated_at FROM inbound WHERE DocNumber = ? LIMIT 1");
			jrs.setString(1, lParam.getDocNumber());
		
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("Date", lParam.getDate());
			jrs.updateString("VendorID", lParam.getVendorID());
			jrs.updateString("PlantID", lParam.getPlantID());
			jrs.updateString("WarehouseID", lParam.getWarehouseID());
			jrs.updateString("Updated_by", lUser);
			jrs.updateString("Updated_at", LocalDateTime.now().toString());
			jrs.updateRow();
			
			jrs.close();
			
			Globals.gReturn_Status = "Success Update Inbound";
		}
		catch (Exception ex){
			
			LogAdapter.InsertLogExc(ex.toString(), "UpdateInbound", Globals.gCommand , lUser);
			Globals.gReturn_Status = "Error Update Inbounds";
		}
		
		return Globals.gReturn_Status;
	}
	
	public static String DeleteInbound(String id, String lUser)
	{
		Globals.gCommand = "Delete Inbound Doc : " + id;
		
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();	

			jrs.setCommand("SELECT DocNumber,Date,VendorID,PlantID,WarehouseID FROM inbound WHERE DocNumber = ? LIMIT 1");
			jrs.setString(1, id);
		
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			jrs.close();
			
			Globals.gReturn_Status = "Success Delete Inbound";
		}
		catch (Exception ex){
			
			LogAdapter.InsertLogExc(ex.toString(), "DeleteInbound", Globals.gCommand , lUser);
			Globals.gReturn_Status = "Error Delete Inbound";
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlInbound> LoadInboundDetail (String lDocNumber,String lUser) {
		List<model.mdlInbound> listmdlInboundDetail = new ArrayList<model.mdlInbound>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT a.DocNumber,a.DocLine,a.ProductID,a.Qty_UOM,a.UOM,a.Qty_BaseUOM,a.BaseUOM,a.Batch_No,a.Vendor_Batch_No,a.Packing_No,a.Expired_Date, "
					+ "b.Date,b.PlantID,b.WarehouseID "
					+ "FROM inbound_detail a "
					+ "INNER JOIN inbound b ON b.DocNumber=a.DocNumber "
					+ "WHERE a.DocNumber=? "
					+ "ORDER BY a.DocLine");
			jrs.setString(1, lDocNumber);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlInbound mdlInboundDetail = new model.mdlInbound();
				mdlInboundDetail.setPlantID(jrs.getString("PlantID"));
				mdlInboundDetail.setWarehouseID(jrs.getString("WarehouseID"));
				mdlInboundDetail.setDocNumber(jrs.getString("DocNumber"));
				mdlInboundDetail.setDate(jrs.getString("Date"));
				mdlInboundDetail.setDocLine(jrs.getString("DocLine"));
				mdlInboundDetail.setProductID(jrs.getString("ProductID"));
				mdlInboundDetail.setQtyUom(jrs.getString("Qty_UOM"));
				mdlInboundDetail.setUom(jrs.getString("UOM"));
				mdlInboundDetail.setQtyBaseUom(jrs.getString("Qty_BaseUOM"));
				mdlInboundDetail.setBaseUom(jrs.getString("BaseUOM"));
				mdlInboundDetail.setBatchNo(jrs.getString("Batch_No"));
				mdlInboundDetail.setVendorBatchNo(jrs.getString("Vendor_Batch_No"));
				mdlInboundDetail.setPackingNo(jrs.getString("Packing_No"));
				
				String Date = jrs.getString("Expired_Date");
				Date initDate = new SimpleDateFormat("yyyy-MM-dd").parse(Date);
			    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
			    String newDate = formatter.format(initDate);
				mdlInboundDetail.setExpiredDate(newDate);
				
				listmdlInboundDetail.add(mdlInboundDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadInboundDetail", Globals.gCommand , lUser);
			listmdlInboundDetail = new ArrayList<model.mdlInbound>();
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadInboundDetail", "close opened connection protocol", lUser);
				listmdlInboundDetail = new ArrayList<model.mdlInbound>();
			}
		}
		
		return listmdlInboundDetail;
	}
	
	public static String InsertInboundDetail(mdlInbound lParam, String lUser)
	{
		Globals.gCommand = "Insert Inbound Doc : " + lParam.getDocNumber() + " Line : " + lParam.getDocLine();
		
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();			
			
			jrs.setCommand("SELECT DocNumber,DocLine,ProductID,Qty_UOM,UOM,Qty_BaseUOM,BaseUOM,Batch_No,Vendor_Batch_No,Packing_No,Expired_Date,Created_by,Created_at,Updated_by,Updated_at FROM inbound_detail LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("DocNumber",lParam.getDocNumber());
			jrs.updateString("DocLine", lParam.getDocLine());
			jrs.updateString("ProductID", lParam.getProductID());
			jrs.updateString("Qty_UOM",lParam.getQtyUom());
			jrs.updateString("UOM", lParam.getUom());
			jrs.updateString("Qty_BaseUOM", lParam.getQtyBaseUom());
			jrs.updateString("BaseUOM", lParam.getBaseUom());
			jrs.updateString("Batch_No", lParam.getBatchNo());
			jrs.updateString("Vendor_Batch_No", lParam.getVendorBatchNo());
			jrs.updateString("Packing_No", lParam.getPackingNo());
			jrs.updateString("Expired_Date", lParam.getExpiredDate());
			jrs.updateString("Created_by", lUser);
			jrs.updateString("Created_at", LocalDateTime.now().toString());
			jrs.updateString("Updated_by", lUser);
			jrs.updateString("Updated_at", LocalDateTime.now().toString());
			
			
			jrs.insertRow();
			
			jrs.close();
			
			Globals.gReturn_Status = "Success Insert Inbound Detail";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertInboundDetail", Globals.gCommand , lUser);
			Globals.gReturn_Status = "Error Insert Inbound Detail";

		}
		return Globals.gReturn_Status;
	}
	
	public static String TransactionInbound(model.mdlInbound lParamInbound, model.mdlBatch lParamBatch, model.mdlStockSummary lParamStockSummary, String LastProductQty, String lUser)
	{	
		Globals.gCommand = "";
		
			Connection connection = null;
			PreparedStatement pstmInsertBatch = null;
			PreparedStatement pstmInsertInboundDetail = null;
			PreparedStatement pstmInsertStockSummary = null;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlInsertBatch = "INSERT INTO batch(`ProductID`, `Batch_No`, `Packing_No`, `GRDate`, `Expired_Date`, `Vendor_Batch_No`, `VendorID`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Batch_No`=?;";
				String sqlInsertInboundDetail = "INSERT INTO inbound_detail(`DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Vendor_Batch_No`, `Packing_No`, `Expired_Date`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertStockSummary = "INSERT INTO stock_summary(`Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
				pstmInsertBatch = connection.prepareStatement(sqlInsertBatch);
				pstmInsertInboundDetail = connection.prepareStatement(sqlInsertInboundDetail);
				pstmInsertStockSummary = connection.prepareStatement(sqlInsertStockSummary);
				
				//insert batch process parameter
				pstmInsertBatch.setString(1,  ValidateNull.NulltoStringEmpty(lParamBatch.getProductID()));
				pstmInsertBatch.setString(2,  ValidateNull.NulltoStringEmpty(lParamBatch.getBatch_No()));
				pstmInsertBatch.setString(3,  ValidateNull.NulltoStringEmpty(lParamBatch.getPacking_No()));
				pstmInsertBatch.setString(4,  ValidateNull.NulltoStringEmpty(lParamBatch.getGRDate()));
				pstmInsertBatch.setString(5,  ValidateNull.NulltoStringEmpty(lParamBatch.getExpired_Date()));
				pstmInsertBatch.setString(6,  ValidateNull.NulltoStringEmpty(lParamBatch.getVendor_Batch_No()));
				pstmInsertBatch.setString(7,  ValidateNull.NulltoStringEmpty(lParamBatch.getVendorID())); 
				pstmInsertBatch.setString(8,  ValidateNull.NulltoStringEmpty(lUser));
				pstmInsertBatch.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertBatch.setString(10, ValidateNull.NulltoStringEmpty(lUser));
				pstmInsertBatch.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertBatch.setString(12, ValidateNull.NulltoStringEmpty(lParamBatch.getBatch_No()));
				Globals.gCommand = pstmInsertBatch.toString();
				pstmInsertBatch.executeUpdate();
				
				//insert inbound detail process parameter
				pstmInsertInboundDetail.setString(1, ValidateNull.NulltoStringEmpty(lParamInbound.getDocNumber()));
				pstmInsertInboundDetail.setString(2, ValidateNull.NulltoStringEmpty(lParamInbound.getDocLine()));
				pstmInsertInboundDetail.setString(3, ValidateNull.NulltoStringEmpty(lParamInbound.getProductID()));
				pstmInsertInboundDetail.setString(4, ValidateNull.NulltoStringEmpty(lParamInbound.getQtyUom()));
				pstmInsertInboundDetail.setString(5, ValidateNull.NulltoStringEmpty(lParamInbound.getUom()));
				pstmInsertInboundDetail.setString(6, ValidateNull.NulltoStringEmpty(lParamInbound.getQtyBaseUom()));
				pstmInsertInboundDetail.setString(7, ValidateNull.NulltoStringEmpty(lParamInbound.getBaseUom()));
				pstmInsertInboundDetail.setString(8, ValidateNull.NulltoStringEmpty(lParamInbound.getBatchNo()));
				pstmInsertInboundDetail.setString(9, ValidateNull.NulltoStringEmpty(lParamInbound.getVendorBatchNo()));
				pstmInsertInboundDetail.setString(10, ValidateNull.NulltoStringEmpty(lParamInbound.getPackingNo()));
				pstmInsertInboundDetail.setString(11, ValidateNull.NulltoStringEmpty(lParamInbound.getExpiredDate()));
				pstmInsertInboundDetail.setString(12, ValidateNull.NulltoStringEmpty(lUser));
				pstmInsertInboundDetail.setString(13, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertInboundDetail.setString(14, ValidateNull.NulltoStringEmpty(lUser));
				pstmInsertInboundDetail.setString(15, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertInboundDetail.toString();
				pstmInsertInboundDetail.executeUpdate();
				
				if(lParamInbound.getWMStatus().contentEquals("0") || (lParamInbound.getWMStatus().contentEquals("1") && lParamInbound.getUpdateWMFirst().contentEquals("0"))) {
					//insert stock summary process parameter
					if (LastProductQty == null)
					{
						LastProductQty = "0";
					}
					
					int intLastProductQty = Integer.parseInt(LastProductQty);
					int intnewProductQty = Integer.parseInt(lParamInbound.getQtyBaseUom());
					int totalNewProductQty = intLastProductQty+intnewProductQty;
					
					pstmInsertStockSummary.setString(1, ValidateNull.NulltoStringEmpty(lParamStockSummary.getPeriod()));
					pstmInsertStockSummary.setString(2, ValidateNull.NulltoStringEmpty(lParamStockSummary.getProductID()));
					pstmInsertStockSummary.setString(3, ValidateNull.NulltoStringEmpty(lParamStockSummary.getPlantID()));
					pstmInsertStockSummary.setString(4, ValidateNull.NulltoStringEmpty(lParamStockSummary.getWarehouseID()));
					pstmInsertStockSummary.setString(5, ValidateNull.NulltoStringEmpty(lParamStockSummary.getBatch_No()));
					pstmInsertStockSummary.setString(6, ValidateNull.NulltoStringEmpty(String.valueOf(totalNewProductQty)));
					pstmInsertStockSummary.setString(7, ValidateNull.NulltoStringEmpty(lParamStockSummary.getUOM()));
					pstmInsertStockSummary.setString(8, ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertStockSummary.setString(9, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertStockSummary.setString(10, ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertStockSummary.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertStockSummary.setString(12, ValidateNull.NulltoStringEmpty(String.valueOf(totalNewProductQty)));
					pstmInsertStockSummary.setString(13, ValidateNull.NulltoStringEmpty(lParamStockSummary.getUOM()));
					pstmInsertStockSummary.setString(14, ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertStockSummary.setString(15, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmInsertStockSummary.toString();
					pstmInsertStockSummary.executeUpdate();
				}
				
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Insert Inbound Detail";
			}
			
			catch (Exception e ) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionInboundDetail", Globals.gCommand , lUser);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        //check if the batch no already exist in batchmaster, dont delete trace code in trace code generate table
		        model.mdlBatch checkBatchMaster = BatchAdapter.LoadBatchbyParam(lParamBatch, lUser);
		        if(checkBatchMaster.Batch_No == null || checkBatchMaster.Batch_No == "")
		        {
		        	BatchAdapter.DeleteTraceCode(lParamBatch.getBatch_No(), lUser);
		        }
		        //end of check
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertInboundDetail", Globals.gCommand , lUser);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
			        if (pstmInsertBatch != null) {
			        	pstmInsertBatch.close();
			        }
			        if (pstmInsertInboundDetail != null) {
			        	pstmInsertInboundDetail.close();
			        }
			        if (pstmInsertStockSummary != null) {
			        	pstmInsertStockSummary.close();
			        }
			        connection.setAutoCommit(true);
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertInboundDetail", "close opened connection protocol", lUser);
				}
		    }
			
		return Globals.gReturn_Status;
	}
	
	 //<<001 Nanda
	public static String GenerateDocNoInbound()
	{
		String generateDate = LocalDateTime.now().toString();
		String lYear = generateDate.substring(0, 4);
		String txtDocNo =  "";
		model.mdlTraceCode lGetTraceCode = TraceCodeAdapter.LoadTraceCodeDocNo(lYear,"Inbound");
	
		if(lGetTraceCode.TraceCode == null)
		{
		txtDocNo = "T"+ generateDate.substring(2, 4) + "0000001";
		//TraceCodeAdapter.InsertTraceCode(txtDocNo, generateDate.replace("T"," ") ,"Inbound");
		}
		else
		{
		int lRunningNumber = Integer.parseInt(lGetTraceCode.TraceCode.substring(3,10));
		String lnewRunningNumber = String.format("%07d", lRunningNumber + 1);
	
		txtDocNo = "T"+ generateDate.substring(2, 4)+ lnewRunningNumber;
		//TraceCodeAdapter.InsertTraceCode(txtDocNo, generateDate.replace("T"," ") ,"Batch");
		}
	
		return txtDocNo;
	}

	@SuppressWarnings("resource")
	public static String GenerateDocLineInboundDet(String lDocNumber)
	{
		String txtDocLine ="";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
	
			Class.forName("com.mysql.jdbc.Driver");
		
			jrs = database.RowSetAdapter.getJDBCRowSet();
		
			jrs.setCommand("SELECT `DocLine` FROM `inbound_detail` WHERE `DocNumber` = ? ORDER BY  `Created_at` DESC LIMIT 1");
			jrs.setString(1, lDocNumber);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			while(jrs.next()){
			txtDocLine = ValidateNull.NulltoStringEmpty(jrs.getString("DocLine"));
			}
		}
		catch(Exception ex){
		LogAdapter.InsertLogExc(ex.toString(), "GenerateDocLineInbound", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "GenerateDocLineInbound", "close opened connection protocol", Globals.user);
			}
		}
	
		return txtDocLine;
	}
		//>>
	
	//<<002 Nanda
		public static List<model.mdlRptGoodReceipt> LoadReportGoodReceipt(String lStartDate, String lEndDate) {
				List<model.mdlRptGoodReceipt> mdlRptGoodReceiptlist = new ArrayList<model.mdlRptGoodReceipt>();
				JdbcRowSet jrs = new JdbcRowSetImpl();
				Globals.gCommand = "";
				try{
					jrs = database.RowSetAdapter.getJDBCRowSet();
					
					jrs.setCommand("SELECT a.DocNumber, a.Date, a.VendorID, a.PlantID, a.WarehouseID, b.DocLine, b.ProductID, b.Qty_UOM, b.UOM, b.Qty_BaseUOM, b.BaseUOM, b.Batch_No, b.Packing_No FROM  inbound a "
							+ "INNER JOIN  inbound_detail  b  ON a.DocNumber = b.DocNumber "
							+ "WHERE (DATE(a.Date) BETWEEN ? AND DATE_ADD(?,INTERVAL 1 DAY)) AND a.PlantID IN ("+Globals.user_area+")");
					jrs.setString(1, ValidateNull.NulltoDateTimeEmpty(lStartDate));
					jrs.setString(2, ValidateNull.NulltoDateTimeEmpty(lEndDate));
					Globals.gCommand = jrs.getCommand();
					jrs.execute();
											
					while(jrs.next()){
						model.mdlRptGoodReceipt mdlRptGoodReceipt = new model.mdlRptGoodReceipt();
						mdlRptGoodReceipt.setDocNo(jrs.getString("DocNumber"));
						mdlRptGoodReceipt.setDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
						mdlRptGoodReceipt.setVendorID(jrs.getString("VendorID"));	
						mdlRptGoodReceipt.setPlantID(jrs.getString("PlantID"));
						mdlRptGoodReceipt.setWarehouseID(jrs.getString("WarehouseID"));
						mdlRptGoodReceipt.setDocLine(jrs.getString("DocLine"));
						mdlRptGoodReceipt.setProductID(jrs.getString("ProductID"));
						mdlRptGoodReceipt.setQtyUOM(jrs.getString("Qty_UOM"));
						mdlRptGoodReceipt.setUOM(jrs.getString("UOM"));	
						mdlRptGoodReceipt.setQtyBaseUOM(jrs.getString("Qty_BaseUOM"));
						mdlRptGoodReceipt.setBaseUOM(jrs.getString("BaseUOM"));
						mdlRptGoodReceipt.setBatchNo(jrs.getString("Batch_No"));
						mdlRptGoodReceipt.setPackingNo(jrs.getString("Packing_No"));
						
						mdlRptGoodReceiptlist.add(mdlRptGoodReceipt);
					}	
				}
				catch(Exception ex){
					LogAdapter.InsertLogExc(ex.toString(), "LoadReportGoodReceipt", Globals.gCommand , Globals.user);
				}
				finally{
					try{
						if(jrs!=null)
							jrs.close();
					}
					catch(Exception e){
						LogAdapter.InsertLogExc(e.toString(), "LoadReportGoodReceipt", "close opened connection protocol", Globals.user);
					}
				}

				return mdlRptGoodReceiptlist;
			}
		
		@SuppressWarnings("resource")
		public static String GetRptCountInbound(String lStartDate, String lEndDate) {
			String lResult = "";
			JdbcRowSet jrs = new JdbcRowSetImpl();
			Globals.gCommand = "";
			try{
				jrs = database.RowSetAdapter.getJDBCRowSet();
						
				jrs.setCommand("SELECT COUNT(`DocNumber`) AS DocNumber "
						+ "FROM `inbound` "
						+ "WHERE (DATE(`Updated_at`) BETWEEN ? AND DATE_ADD(?,INTERVAL 1 DAY)) AND PlantID IN ("+Globals.user_area+")");
				jrs.setString(1, lStartDate);
				jrs.setString(2, lEndDate);
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
												
				while(jrs.next()){
					lResult = jrs.getString("DocNumber");
				}		
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "GetRptCountInbound", Globals.gCommand , Globals.user);
			}
			finally{
				try{
					if(jrs!=null)
						jrs.close();
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "GetRptCountInbound", "close opened connection protocol", Globals.user);
				}
			}

				return lResult;
			}
		
		//>>
	
	public static List<model.mdlInbound> LoadTransferRequirement (String lDocNumber) {
		List<model.mdlInbound> listmdlInboundDetail = new ArrayList<model.mdlInbound>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT a.DocNumber,a.DocLine,a.ProductID,a.Qty_UOM,a.UOM,a.Batch_No, "
					+ "b.Date,b.PlantID,b.WarehouseID, "
					+ "c.Title_EN "
					+ "FROM inbound_detail a "
					+ "INNER JOIN inbound b ON b.DocNumber=a.DocNumber "
					+ "INNER JOIN product c ON c.ID=a.ProductID "
					+ "WHERE a.DocNumber=? ORDER BY a.DocLine");
			jrs.setString(1, lDocNumber);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlInbound mdlInboundDetail = new model.mdlInbound();
				mdlInboundDetail.setDocNumber(jrs.getString("DocNumber"));
				mdlInboundDetail.setDocLine(jrs.getString("DocLine"));
				mdlInboundDetail.setProductID(jrs.getString("ProductID"));
				mdlInboundDetail.setProductName(jrs.getString("Title_EN"));
				mdlInboundDetail.setQtyUom(jrs.getString("Qty_UOM"));
				mdlInboundDetail.setUom(jrs.getString("UOM"));
				mdlInboundDetail.setBatchNo(jrs.getString("Batch_No"));
				mdlInboundDetail.setPlantID(jrs.getString("PlantID"));
				mdlInboundDetail.setWarehouseID(jrs.getString("WarehouseID"));
				mdlInboundDetail.setDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd.MM.yyyy"));
				
				listmdlInboundDetail.add(mdlInboundDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadTransferRequirement", Globals.gCommand , Globals.user);
			listmdlInboundDetail = new ArrayList<model.mdlInbound>();
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadTransferRequirement", "close opened connection protocol", Globals.user);
				listmdlInboundDetail = new ArrayList<model.mdlInbound>();
			}
		}
		
		return listmdlInboundDetail;
	}
	
}
