package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlBOMConfirmationProduction;
import model.mdlBOMProductionOrderDetail;
import model.mdlStockSummary;
import model.mdlTempComponentBatchSummary;

public class ConfirmationAdapter {

	public static String TransactionConfirmationProduction(mdlBOMConfirmationProduction lParamConfirmation, mdlStockSummary lParamStockSummary)
	{	
		Globals.gCommand = "";
		String BOMProductBatchNo="";
		
			Connection connection = null;
			PreparedStatement pstmInsertConfirmation = null;
			PreparedStatement pstmInsertBatch = null;
			PreparedStatement pstmInsertConfirmationDetail = null;
			PreparedStatement pstmInsertUpdateStockSummary = null;
			PreparedStatement pstmUpdateProductionOrder = null;
			PreparedStatement pstmDeleteTempProductionOrderDetail = null;
			PreparedStatement pstmDeleteTempComponentBatchSummary = null;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				BOMProductBatchNo = BatchAdapter.GenerateBatch(Globals.user);
				if(BOMProductBatchNo.contentEquals("")){
					Globals.gReturn_Status = "Terjadi kesalahan pada sistem saat generate batch untuk produk bom";
					return Globals.gReturn_Status;
				}
				
				String sqlInsertConfirmation = "INSERT INTO bom_production_order_confirmation(`ConfirmationID`, `ConfirmationNo`, `ConfirmationDate`, `OrderTypeID`, `OrderNo`, `ProductID`, `Batch_No`, `Qty`, `UOM`, `QtyBaseUOM`, `BaseUOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Updated_by`=?,`Updated_at`=?;";
				String sqlInsertConfirmationDetail = "INSERT INTO bom_production_order_detail_confirmation(`ConfirmationID`, `ConfirmationNo`, `ComponentLine`, `ComponentID`, `Batch_No`, `Qty`, `UOM`, `QtyBaseUOM`, `BaseUOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertUpdateStockSummary = "INSERT INTO stock_summary(`Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
				String sqlInsertBatch = "INSERT INTO batch(`ProductID`, `Batch_No`, `Packing_No`, `GRDate`, `Expired_Date`, `Vendor_Batch_No`, `VendorID`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
				pstmInsertConfirmation = connection.prepareStatement(sqlInsertConfirmation);
				pstmInsertConfirmationDetail = connection.prepareStatement(sqlInsertConfirmationDetail);
				pstmInsertUpdateStockSummary = connection.prepareStatement(sqlInsertUpdateStockSummary);
				pstmInsertBatch = connection.prepareStatement(sqlInsertBatch);
				
				//insert confirmation process parameter
				pstmInsertConfirmation.setString(1,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getConfirmationID()));
				pstmInsertConfirmation.setString(2,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getConfirmationNo()));
				pstmInsertConfirmation.setString(3,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getConfirmationDate()));
				pstmInsertConfirmation.setString(4,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getOrderTypeID()));
				pstmInsertConfirmation.setString(5,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getOrderNo()));
				pstmInsertConfirmation.setString(6,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getProductID()));
				pstmInsertConfirmation.setString(7,  ValidateNull.NulltoStringEmpty(BOMProductBatchNo));
				pstmInsertConfirmation.setString(8,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getQty()));
				pstmInsertConfirmation.setString(9,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getUOM()));
				pstmInsertConfirmation.setString(10,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getQtyBaseUOM()));
				pstmInsertConfirmation.setString(11,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getBaseUOM()));
				pstmInsertConfirmation.setString(12,  ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertConfirmation.setString(13,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertConfirmation.setString(14,  ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertConfirmation.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertConfirmation.setString(16,  ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertConfirmation.setString(17,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand  = pstmInsertConfirmation.toString();
				pstmInsertConfirmation.executeUpdate();
				
				//insert stock summary process parameter for bom product
				pstmInsertUpdateStockSummary.setString(1, ValidateNull.NulltoStringEmpty(lParamStockSummary.getPeriod()));
				pstmInsertUpdateStockSummary.setString(2, ValidateNull.NulltoStringEmpty(lParamConfirmation.getProductID()));
				pstmInsertUpdateStockSummary.setString(3, ValidateNull.NulltoStringEmpty(lParamStockSummary.getPlantID()));
				pstmInsertUpdateStockSummary.setString(4, ValidateNull.NulltoStringEmpty(lParamStockSummary.getWarehouseID()));
				pstmInsertUpdateStockSummary.setString(5, ValidateNull.NulltoStringEmpty(BOMProductBatchNo));
				pstmInsertUpdateStockSummary.setString(6, ValidateNull.NulltoStringEmpty(lParamConfirmation.getQtyBaseUOM()));
				pstmInsertUpdateStockSummary.setString(7, ValidateNull.NulltoStringEmpty(lParamConfirmation.getBaseUOM()));
				pstmInsertUpdateStockSummary.setString(8, ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertUpdateStockSummary.setString(9, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertUpdateStockSummary.setString(10, ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertUpdateStockSummary.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertUpdateStockSummary.setString(12, ValidateNull.NulltoStringEmpty(lParamConfirmation.getQtyBaseUOM()));
				pstmInsertUpdateStockSummary.setString(13, ValidateNull.NulltoStringEmpty(lParamConfirmation.getBaseUOM()));
				pstmInsertUpdateStockSummary.setString(14, ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertUpdateStockSummary.setString(15, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertUpdateStockSummary.toString();
				pstmInsertUpdateStockSummary.executeUpdate();
				
				//insert bom product batch, tidak perlu insert update kalau terdapat konfirmasi produksi dengan konfirmasi id yang sama karena setiap konfirmasi produksi batchnya pasti baru/beda
				pstmInsertBatch.setString(1, ValidateNull.NulltoStringEmpty(lParamConfirmation.getProductID()));
				pstmInsertBatch.setString(2, ValidateNull.NulltoStringEmpty(BOMProductBatchNo));
				pstmInsertBatch.setString(3, ValidateNull.NulltoStringEmpty(lParamConfirmation.getPackingNo()));
				pstmInsertBatch.setString(4, ValidateNull.NulltoStringEmpty(lParamConfirmation.getConfirmationDate()));
				pstmInsertBatch.setString(5, ValidateNull.NulltoStringEmpty(lParamConfirmation.getExpiredDate()));
				pstmInsertBatch.setString(6, ValidateNull.NulltoStringEmpty(""));
				pstmInsertBatch.setString(7, ValidateNull.NulltoStringEmpty(lParamStockSummary.getPlantID()));
				pstmInsertBatch.setString(8, ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertBatch.setString(9, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertBatch.setString(10, ValidateNull.NulltoStringEmpty(Globals.user));
				pstmInsertBatch.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertBatch.toString();
				pstmInsertBatch.executeUpdate();
				
				//insert confirmation detail
				//get the final production detail list from temporary table
				List<model.mdlBOMProductionOrderDetail> mdlFinalProductionOrderDetailList = new ArrayList<model.mdlBOMProductionOrderDetail>();
				mdlFinalProductionOrderDetailList = ProductionOrderAdapter.LoadTempProductionOrderDetail(lParamConfirmation.getOrderTypeID(), lParamConfirmation.getOrderNo());
				if(mdlFinalProductionOrderDetailList.isEmpty() || mdlFinalProductionOrderDetailList == null || mdlFinalProductionOrderDetailList.size() == 0)
				{
					returnImmediately(connection,lParamConfirmation.getProductID(),BOMProductBatchNo);
					Globals.gReturn_Status = "Terjadi kesalahan pada sistem saat menarik data komponen dari produk bom yang akan diproduksi";
					
					return Globals.gReturn_Status;
				}
				else{
					for(mdlBOMProductionOrderDetail lParamFinalProductionOrderDetail : mdlFinalProductionOrderDetailList)
					{
						//get the component batch summary in final production order detail for confirmation
						List<model.mdlTempComponentBatchSummary> listmdlComponentBatchSummary = new ArrayList<model.mdlTempComponentBatchSummary>();
						listmdlComponentBatchSummary = ProductionOrderAdapter.LoadProductionComponentBatchSummary(lParamFinalProductionOrderDetail);
						if(listmdlComponentBatchSummary.isEmpty() || listmdlComponentBatchSummary==null || listmdlComponentBatchSummary.size() == 0)
						{
							returnImmediately(connection,lParamConfirmation.getProductID(),BOMProductBatchNo);
							Globals.gReturn_Status = "Terjadi kesalahan pada sistem saat menarik data stok komponen dari produk bom yang akan diproduksi";
							
							return Globals.gReturn_Status;
						}
						else{
							for(mdlTempComponentBatchSummary lmdlComponentBatchSummary : listmdlComponentBatchSummary)
							{
								lParamStockSummary.setProductID(lmdlComponentBatchSummary.getComponentID());
								lParamStockSummary.setBatch_No(lmdlComponentBatchSummary.getBatchNo());
								model.mdlStockSummary existingComponentStockSummary =  StockSummaryAdapter.LoadStockSummaryByKey(lParamStockSummary,Globals.user);
								if(existingComponentStockSummary.getQty()==null){
									returnImmediately(connection,lParamConfirmation.getProductID(),BOMProductBatchNo);
					    			Globals.gReturn_Status = "Produk : "+existingComponentStockSummary.getProductID()+",plant : "+existingComponentStockSummary.getPlantID()+",warehouse : "+existingComponentStockSummary.getWarehouseID()+",batch : "+existingComponentStockSummary.getBatch_No()+" tidak ditemukan di stok gudang";
					    			
					    			return Globals.gReturn_Status;
								}
								else{
									Integer ExistingComponentStockSummary = Integer.parseInt(existingComponentStockSummary.getQty());
									Integer ComponentProductionQty = lmdlComponentBatchSummary.getQty();
									//check if the component is usage or not , if component + it means usage, if - it means founded component
									if(ComponentProductionQty > 0) //usage, so we have to check component stock first
									{
										if(ComponentProductionQty > ExistingComponentStockSummary)
										{
											Globals.gCommand = lmdlComponentBatchSummary.toString();
											LogAdapter.InsertLogExc("The product quantity that you input is more than the available quantity in the database", "TransactionConfirmationProduction", Globals.gCommand , Globals.user);
											
											returnImmediately(connection,lParamConfirmation.getProductID(),BOMProductBatchNo);
											Globals.gReturn_Status = "Stok untuk komponen "+lmdlComponentBatchSummary.getComponentID()+"(batch no:"+lmdlComponentBatchSummary.getBatchNo()+",plant:"+lParamStockSummary.getPlantID()+",warehouse:"+lParamStockSummary.getWarehouseID()+") tidak mencukupi";
											
											return Globals.gReturn_Status;
										}
									}
									ExistingComponentStockSummary = ExistingComponentStockSummary - ComponentProductionQty;
									
									//insert confirmation production detail process parameter
									pstmInsertConfirmationDetail.setString(1, ValidateNull.NulltoStringEmpty(lParamConfirmation.getConfirmationID()));
									pstmInsertConfirmationDetail.setString(2, ValidateNull.NulltoStringEmpty(lParamConfirmation.getConfirmationNo()));
									pstmInsertConfirmationDetail.setString(3, ValidateNull.NulltoStringEmpty(lmdlComponentBatchSummary.getComponentLine()));
									pstmInsertConfirmationDetail.setString(4, ValidateNull.NulltoStringEmpty(lmdlComponentBatchSummary.getComponentID()));
									pstmInsertConfirmationDetail.setString(5, ValidateNull.NulltoStringEmpty(lmdlComponentBatchSummary.getBatchNo()));
									pstmInsertConfirmationDetail.setString(6, ValidateNull.NulltoStringEmpty(String.valueOf(lmdlComponentBatchSummary.getQty())));
									pstmInsertConfirmationDetail.setString(7, ValidateNull.NulltoStringEmpty(lmdlComponentBatchSummary.getUOM()));
									pstmInsertConfirmationDetail.setString(8, ValidateNull.NulltoStringEmpty(String.valueOf(lmdlComponentBatchSummary.getQty())));
									pstmInsertConfirmationDetail.setString(9, ValidateNull.NulltoStringEmpty(lmdlComponentBatchSummary.getUOM()));
									pstmInsertConfirmationDetail.setString(10, ValidateNull.NulltoStringEmpty(Globals.user));
									pstmInsertConfirmationDetail.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
									pstmInsertConfirmationDetail.setString(12, ValidateNull.NulltoStringEmpty(Globals.user));
									pstmInsertConfirmationDetail.setString(13, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
									Globals.gCommand = pstmInsertConfirmationDetail.toString();
									pstmInsertConfirmationDetail.executeUpdate();
									
									//insert update stock summary process parameter for bom component
									pstmInsertUpdateStockSummary.setString(1, ValidateNull.NulltoStringEmpty(lParamStockSummary.getPeriod()));
									pstmInsertUpdateStockSummary.setString(2, ValidateNull.NulltoStringEmpty(lmdlComponentBatchSummary.getComponentID()));
									pstmInsertUpdateStockSummary.setString(3, ValidateNull.NulltoStringEmpty(lParamStockSummary.getPlantID()));
									pstmInsertUpdateStockSummary.setString(4, ValidateNull.NulltoStringEmpty(lParamStockSummary.getWarehouseID()));
									pstmInsertUpdateStockSummary.setString(5, ValidateNull.NulltoStringEmpty(lmdlComponentBatchSummary.getBatchNo()));
									pstmInsertUpdateStockSummary.setString(6, ValidateNull.NulltoStringEmpty(String.valueOf(ExistingComponentStockSummary)));
									pstmInsertUpdateStockSummary.setString(7, ValidateNull.NulltoStringEmpty(lmdlComponentBatchSummary.getUOM()));
									pstmInsertUpdateStockSummary.setString(8, ValidateNull.NulltoStringEmpty(Globals.user));
									pstmInsertUpdateStockSummary.setString(9, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
									pstmInsertUpdateStockSummary.setString(10, ValidateNull.NulltoStringEmpty(Globals.user));
									pstmInsertUpdateStockSummary.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
									pstmInsertUpdateStockSummary.setString(12, ValidateNull.NulltoStringEmpty(String.valueOf(ExistingComponentStockSummary)));
									pstmInsertUpdateStockSummary.setString(13, ValidateNull.NulltoStringEmpty(lmdlComponentBatchSummary.getUOM()));
									pstmInsertUpdateStockSummary.setString(14, ValidateNull.NulltoStringEmpty(Globals.user));
									pstmInsertUpdateStockSummary.setString(15, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
									Globals.gCommand = pstmInsertUpdateStockSummary.toString();
									pstmInsertUpdateStockSummary.executeUpdate();
								}
							}
						}
					}
				}
				
				//check if the target of the production is already fulfilled or not
				if(lParamConfirmation.getQty().contentEquals(lParamConfirmation.getTargetQty())) {
					//if already fulfilled, change the status of production become 'COMPLETED'
					String sqlUpdateProductionOrder = "UPDATE bom_production_order SET Status='COMPLETED', Updated_by=?,Updated_at=? WHERE OrderTypeID=? AND OrderNo=?;";
					pstmUpdateProductionOrder = connection.prepareStatement(sqlUpdateProductionOrder);
					
					pstmUpdateProductionOrder.setString(1,  ValidateNull.NulltoStringEmpty(Globals.user));
					pstmUpdateProductionOrder.setString(2,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmUpdateProductionOrder.setString(3,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getOrderTypeID()));
					pstmUpdateProductionOrder.setString(4,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getOrderNo()));
					Globals.gCommand  = pstmUpdateProductionOrder.toString();
					pstmUpdateProductionOrder.executeUpdate();
				}
				
				//delete temporary bom production order detail process parameter
				String sqlDeleteTempProductionOrderDetail = "DELETE FROM tempbom_production_order_detail WHERE OrderTypeID=? AND OrderNo=?;";
				pstmDeleteTempProductionOrderDetail = connection.prepareStatement(sqlDeleteTempProductionOrderDetail);
				
				pstmDeleteTempProductionOrderDetail.setString(1,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getOrderTypeID()));
				pstmDeleteTempProductionOrderDetail.setString(2,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getOrderNo()));
				Globals.gCommand  = pstmDeleteTempProductionOrderDetail.toString();
				pstmDeleteTempProductionOrderDetail.executeUpdate();
				
				//delete temporary bom component batch summary process parameter
				String sqlDeleteTempComponentBatchSummary = "DELETE FROM tempbatch_summary WHERE OrderTypeID=? AND OrderNo=?;";
				pstmDeleteTempComponentBatchSummary = connection.prepareStatement(sqlDeleteTempComponentBatchSummary);
				
				pstmDeleteTempComponentBatchSummary.setString(1,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getOrderTypeID()));
				pstmDeleteTempComponentBatchSummary.setString(2,  ValidateNull.NulltoStringEmpty(lParamConfirmation.getOrderNo()));
				Globals.gCommand  = pstmDeleteTempComponentBatchSummary.toString();
				pstmDeleteTempComponentBatchSummary.executeUpdate();
				
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Insert Confirmation";
			}
			
			catch (Exception e) {
				if (connection != null) {
				    try {
				        System.err.print("Transaction is being rolled back");
				        connection.rollback();
				    } catch(SQLException excep) {
				    	LogAdapter.InsertLogExc(excep.toString(), "TransactionConfirmation", Globals.gCommand , Globals.user);
						Globals.gReturn_Status = "Database Error";
				    }
				}
		        
		        //check if the batch number is not exist in batch master (transaction failed but trace code has been generated), delete trace code in trace code generate table
		        model.mdlBatch lParamBatch = new model.mdlBatch();
		        lParamBatch.setProductID(lParamConfirmation.getProductID());
		        lParamBatch.setBatch_No(BOMProductBatchNo);
		        
		        model.mdlBatch checkBatchMaster = BatchAdapter.LoadBatchbyParam(lParamBatch, Globals.user);
		        if(checkBatchMaster.Batch_No == null || checkBatchMaster.Batch_No == "")
		        {
		        	BatchAdapter.DeleteTraceCode(lParamBatch.getBatch_No(), Globals.user);
		        }
		        //end of check
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertBOMConfirmation", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
					if (pstmInsertConfirmation != null) {
						pstmInsertConfirmation.close();
			        }
					if (pstmInsertConfirmationDetail != null) {
						pstmInsertConfirmationDetail.close();
			        }
			        if (pstmInsertBatch != null) {
			        	pstmInsertBatch.close();
			        }
			        if (pstmInsertUpdateStockSummary != null) {
			        	pstmInsertUpdateStockSummary.close();
			        }
			        if (pstmUpdateProductionOrder != null) {
			        	pstmUpdateProductionOrder.close();
			        }
			        if (pstmDeleteTempProductionOrderDetail != null) {
			        	pstmDeleteTempProductionOrderDetail.close();
			        }
			        if (pstmDeleteTempComponentBatchSummary != null) {
			        	pstmDeleteTempComponentBatchSummary.close();
			        }
			        connection.setAutoCommit(true);
			        if(connection!=null){
						connection.close();
			        }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertBOMConfirmation", "close opened connection protocol", Globals.user);
				}
		    }
			
		return Globals.gReturn_Status;
	}

	public static void returnImmediately(Connection connection, String ProductID, String BOMProductBatchNo) {
		if (connection != null) {
		    try {
		        System.err.print("Transaction is being rolled back");
		        connection.rollback();
		    } catch(SQLException excep) {
		    	LogAdapter.InsertLogExc(excep.toString(), "TransactionConfirmation", Globals.gCommand , Globals.user);
				Globals.gReturn_Status = "Database Error";
		    }
		}
		
		//check if the batch number is not exist in batch master (transaction failed but trace code has been generated), delete trace code in trace code generate table
        model.mdlBatch lParamBatch = new model.mdlBatch();
        lParamBatch.setProductID(ProductID);
        lParamBatch.setBatch_No(BOMProductBatchNo);
        
        model.mdlBatch checkBatchMaster = BatchAdapter.LoadBatchbyParam(lParamBatch, Globals.user);
        if(checkBatchMaster.Batch_No == null || checkBatchMaster.Batch_No == "")
        {
        	BatchAdapter.DeleteTraceCode(lParamBatch.getBatch_No(), Globals.user);
        }
        //end of check
	}

	public static List<model.mdlBOMConfirmationProductionDetail> LoadBOMConfirmationDetail(String lConfirmationID, String lConfirmationNo) {
		List<model.mdlBOMConfirmationProductionDetail> listBOMConfirmationDetail = new ArrayList<model.mdlBOMConfirmationProductionDetail>();
		
		Connection connection = null;
		PreparedStatement pstmLoadConfirmationDetail = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadConfirmationDetail = "SELECT a.ComponentLine,a.ComponentID,a.Batch_No,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM bom_production_order_detail_confirmation a "
					+ "WHERE a.ConfirmationID = ? AND a.ConfirmationNo = ? "
					+ "ORDER BY a.ComponentLine";
			
			pstmLoadConfirmationDetail = connection.prepareStatement(sqlLoadConfirmationDetail);
			
			pstmLoadConfirmationDetail.setString(1, lConfirmationID);
			pstmLoadConfirmationDetail.setString(2, lConfirmationNo);
			pstmLoadConfirmationDetail.addBatch();
			
			Globals.gCommand = pstmLoadConfirmationDetail.toString();
			
			jrs = pstmLoadConfirmationDetail.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMConfirmationProductionDetail BOMConfirmationDetail = new model.mdlBOMConfirmationProductionDetail();
				
				BOMConfirmationDetail.setComponentLine(jrs.getString("ComponentLine"));
				BOMConfirmationDetail.setComponentID(jrs.getString("ComponentID"));
				BOMConfirmationDetail.setBatchNo(jrs.getString("Batch_No"));
				BOMConfirmationDetail.setQty(jrs.getString("Qty"));
				BOMConfirmationDetail.setUOM(jrs.getString("UOM"));
				BOMConfirmationDetail.setQtyBaseUOM(jrs.getString("QtyBaseUOM"));
				BOMConfirmationDetail.setBaseUOM(jrs.getString("BaseUOM"));
				
				listBOMConfirmationDetail.add(BOMConfirmationDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOMConfirmationDetail", Globals.gCommand , Globals.user);
		}
		finally {
			try {
				 if (pstmLoadConfirmationDetail != null) {
					 pstmLoadConfirmationDetail.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadBOMConfirmationDetail", "close opened connection protocol", Globals.user);
			}
		 }

		return listBOMConfirmationDetail;
	}

	//for additional component line
	public static Integer GetComponentLine(String ordertypeid, String orderno) {
		Integer ResultComponentLine = 0;
		
		Connection connection = null;
		PreparedStatement pstmGetComponentLine = null;
		ResultSet jrs = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlGetComponentLine = "SELECT ComponentLine "
					+ "FROM tempbom_production_order_detail "
					+ "WHERE OrderTypeID=? AND OrderNo=? "
					+ "ORDER BY ComponentLine DESC LIMIT 1";
			
			pstmGetComponentLine = connection.prepareStatement(sqlGetComponentLine);
			
			pstmGetComponentLine.setString(1, ordertypeid);
			pstmGetComponentLine.setString(2, orderno);
			pstmGetComponentLine.addBatch();
			
			Globals.gCommand = pstmGetComponentLine.toString();
			
			jrs = pstmGetComponentLine.executeQuery();
									
			while(jrs.next()){
				
				ResultComponentLine = jrs.getInt("ComponentLine") + 1;

			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetComponentLine", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstmGetComponentLine != null) {
					 pstmGetComponentLine.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "GetComponentLine", "close opened connection protocol", Globals.user);
			}
		 }

		return ResultComponentLine;
	}

	public static String TransactionInsertAdditionalComponent(model.mdlBOMConfirmationProductionDetail lParamConfirmationDetail, model.mdlStockSummary lParamStockSummary) throws Exception
	{
		Globals.gCommand = "Insert BOM Confirmation Production : " + lParamConfirmationDetail.getConfirmationID() + " - " + lParamConfirmationDetail.getConfirmationNo() + " , Component : " + lParamConfirmationDetail.getComponentID();
		
		Connection connection = database.RowSetAdapter.getConnection();
		PreparedStatement pstmConfirmationDetail = null;
		PreparedStatement pstmStockSummary = null;
			
		String sqlConfirmationDetail = "INSERT INTO bom_production_order_detail_confirmation(`ConfirmationID`, `ConfirmationNO`, `ComponentLine`, `ComponentID`, `Batch_No`, `Qty`, `UOM`, `QtyBaseUOM`, `BaseUOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
		String sqlStockSummary = "INSERT INTO stock_summary(`Period`,`ProductID`,`PlantID`,`WarehouseID`,`Batch_No`,`Qty`,`UOM`,`Created_by`,`Created_at`,`Updated_by`,`Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
			
		try{
			connection.setAutoCommit(false);
			
			pstmConfirmationDetail = connection.prepareStatement(sqlConfirmationDetail);
			pstmStockSummary = connection.prepareStatement(sqlStockSummary);
			
			//parameter confirmation detail
			pstmConfirmationDetail.setString(1,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getConfirmationID()));
			pstmConfirmationDetail.setString(2,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getConfirmationNo()));
			pstmConfirmationDetail.setString(3,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getComponentLine()));
			pstmConfirmationDetail.setString(4,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getComponentID()));
			pstmConfirmationDetail.setString(5,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getBatchNo()));
			pstmConfirmationDetail.setString(6,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getQty()));
			pstmConfirmationDetail.setString(7,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getUOM()));
			pstmConfirmationDetail.setString(8,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getQtyBaseUOM()));
			pstmConfirmationDetail.setString(9,  ValidateNull.NulltoStringEmpty(lParamConfirmationDetail.getBaseUOM()));
			
			pstmConfirmationDetail.setString(10,  Globals.user);
			pstmConfirmationDetail.setString(11,  LocalDateTime.now().toString());
			pstmConfirmationDetail.setString(12,  Globals.user);
			pstmConfirmationDetail.setString(13,  LocalDateTime.now().toString());
			
			pstmConfirmationDetail.executeUpdate();
				
			//parameter stock_summary
			pstmStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(lParamStockSummary.getPeriod()));
			pstmStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(lParamStockSummary.getProductID()));
			pstmStockSummary.setString(3,  ValidateNull.NulltoStringEmpty(lParamStockSummary.getPlantID()));
			pstmStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(lParamStockSummary.getWarehouseID()));
			pstmStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(lParamStockSummary.getBatch_No()));
			pstmStockSummary.setString(6,  ValidateNull.NulltoStringEmpty(lParamStockSummary.getQty()));
			pstmStockSummary.setString(7,  ValidateNull.NulltoStringEmpty(lParamStockSummary.getUOM()));
			pstmStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(Globals.user));
			pstmStockSummary.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmStockSummary.setString(10,  ValidateNull.NulltoStringEmpty(Globals.user));
			pstmStockSummary.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmStockSummary.setString(12,  ValidateNull.NulltoStringEmpty(lParamStockSummary.getQty()));
			pstmStockSummary.setString(13,  ValidateNull.NulltoStringEmpty(lParamStockSummary.getUOM()));
			pstmStockSummary.setString(14,  ValidateNull.NulltoStringEmpty(Globals.user));
			pstmStockSummary.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
		
			pstmStockSummary.executeUpdate();
			
			connection.commit();
			Globals.gReturn_Status = "Success Insert Additional Component";
		}catch (SQLException e ) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "TransactionAdditionalComponent", Globals.gCommand , Globals.user);
	    			Globals.gReturn_Status = "Database Error";
	            }
	            LogAdapter.InsertLogExc(e.toString(), "InsertBOMAdditionalComponent", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
	        }
		}finally {
	        if (pstmConfirmationDetail != null) {
	        	pstmConfirmationDetail.close();
	        }
	        if (pstmStockSummary != null) {
	        	pstmStockSummary.close();
	        }
	        connection.setAutoCommit(true);
	        if (connection != null) {
				 connection.close();
			 }
	    }
		
		
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlBOMProductionOrderDetail> LoadBOMProductionOrderDetailforConfirmation(String lordertypeid, String lorderno, String lqty) {
		List<model.mdlBOMProductionOrderDetail> listmdlBOMProductionOrderDetail = new ArrayList<model.mdlBOMProductionOrderDetail>();
		
		Connection connection = null;
		PreparedStatement pstmLoadBomProductionOrderDetail = null;
		ResultSet jrs2 = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBomProductionOrderDetail = "SELECT a.OrderTypeID, a.OrderNo,a.ComponentLine,a.ComponentID, "
					+ "a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM,b.Qty as QtyHeader "
					+ "FROM bom_production_order_detail a "
					+ "INNER JOIN bom_production_order b ON b.OrderTypeID=a.OrderTypeID AND b.OrderNo=a.OrderNo "
					+ "WHERE a.OrderTypeID=? AND a.OrderNo=? "
					+ "ORDER BY a.ComponentLine";
			
			pstmLoadBomProductionOrderDetail = connection.prepareStatement(sqlLoadBomProductionOrderDetail);
			
			pstmLoadBomProductionOrderDetail.setString(1, lordertypeid);
			pstmLoadBomProductionOrderDetail.setString(2, lorderno);
			pstmLoadBomProductionOrderDetail.addBatch();
			
			Globals.gCommand = pstmLoadBomProductionOrderDetail.toString();
			
			jrs2 = pstmLoadBomProductionOrderDetail.executeQuery();
			//data proccess
			while(jrs2.next()){
				model.mdlBOMProductionOrderDetail mdlBomProductionOrderDetail = new model.mdlBOMProductionOrderDetail();
				String qtyProductionDetail = String.valueOf((jrs2.getInt("Qty")/jrs2.getInt("QtyHeader"))*Integer.parseInt(lqty));
				//Get the Conversion
	    		model.mdlProductUom ComponentUom = new model.mdlProductUom();
	    		ComponentUom = ProductUomAdapter.LoadProductUOMByKey(jrs2.getString("ComponentID"), jrs2.getString("UOM"));
				String txtQtyBase = String.valueOf(Integer.parseInt(ComponentUom.getQty())*Integer.parseInt(qtyProductionDetail));
//				String txtQtyBaseUOM = ComponentUom.getBaseUOM();
				
				//define model
				mdlBomProductionOrderDetail.setOrderTypeID(jrs2.getString("OrderTypeID"));
				mdlBomProductionOrderDetail.setOrderNo(jrs2.getString("OrderNo"));
				mdlBomProductionOrderDetail.setComponentLine(jrs2.getString("ComponentLine"));
				mdlBomProductionOrderDetail.setComponentID(jrs2.getString("ComponentID"));
				mdlBomProductionOrderDetail.setQty(qtyProductionDetail);
				mdlBomProductionOrderDetail.setUOM(jrs2.getString("UOM"));
				mdlBomProductionOrderDetail.setQtyBaseUOM(txtQtyBase);
				mdlBomProductionOrderDetail.setBaseUOM(jrs2.getString("BaseUOM"));
				
				listmdlBOMProductionOrderDetail.add(mdlBomProductionOrderDetail);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBOMProductionOrderDetailforConfirmation", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstmLoadBomProductionOrderDetail != null) {
					 pstmLoadBomProductionOrderDetail.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs2 != null) {
					 jrs2.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadBOMProductionOrderDetailforConfirmation", "close opened connection protocol", Globals.user);
			}
		 }

		return listmdlBOMProductionOrderDetail;
	}

	@SuppressWarnings("resource")
	public static String CompleteProduction(String lOrderTypeID, String lOrderNo)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT OrderTypeID, OrderNo, Status FROM bom_production_order WHERE OrderTypeID = ? AND OrderNo = ? LIMIT 1");
			Globals.gCommand = jrs.getCommand();
			jrs.setString(1,  lOrderTypeID);
			jrs.setString(2,  lOrderNo);
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("Status", "COMPLETED");
			jrs.updateRow();
				
			Globals.gReturn_Status = "Success Complete Production Order";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CompleteProduction", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "CompleteProduction", "close opened connection protocol", Globals.user);
			}
		}
		
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlBOMConfirmationProduction> LoadNonCancelConfirmation() {
		List<model.mdlBOMConfirmationProduction> listmdlConfirmation = new ArrayList<model.mdlBOMConfirmationProduction>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT a.ConfirmationID,a.ConfirmationNo,a.ConfirmationDate,a.OrderTypeID,a.OrderNo, "
					+ "a.ProductID,a.Batch_No,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM bom_production_order_confirmation a "
					+ "INNER JOIN bom_production_order b ON b.OrderTypeID=a.OrderTypeID AND b.OrderNo=a.OrderNo "
					+ "WHERE a.IsCancel=0 AND b.PlantID IN ("+Globals.user_area+") "
					+ "ORDER BY a.ConfirmationDate");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlBOMConfirmationProduction mdlConfirmation = new model.mdlBOMConfirmationProduction();
				mdlConfirmation.setConfirmationID(jrs.getString("ConfirmationID"));
				mdlConfirmation.setConfirmationNo(jrs.getString("ConfirmationNo"));
				
				String newDate=ConvertDateTimeHelper.formatDate(jrs.getString("ConfirmationDate"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlConfirmation.setConfirmationDate(newDate);
				
				mdlConfirmation.setOrderTypeID(jrs.getString("OrderTypeID"));
				mdlConfirmation.setOrderNo(jrs.getString("OrderNo"));
				mdlConfirmation.setProductID(jrs.getString("ProductID"));
				mdlConfirmation.setBatchNo(jrs.getString("Batch_No"));
				mdlConfirmation.setQty(jrs.getString("Qty"));
				mdlConfirmation.setUOM(jrs.getString("UOM"));
				mdlConfirmation.setQtyBaseUOM(jrs.getString("QtyBaseUOM"));
				mdlConfirmation.setBaseUOM(jrs.getString("BaseUOM"));
				
				listmdlConfirmation.add(mdlConfirmation);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadNonCancelConfirmation", Globals.gCommand , Globals.user);
		}
		finally{
			try {
				if(jrs!=null){
					jrs.close();
				}
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadNonCancelConfirmation", "close opened connection protocol", Globals.user);
			}
		}
		
		return listmdlConfirmation;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlBOMCancelConfirmation> LoadCancelConfirmation () {
		List<model.mdlBOMCancelConfirmation> listmdlCancelConfirmation = new ArrayList<model.mdlBOMCancelConfirmation>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT a.CancelConfirmationID,a.CancelConfirmationNo,a.CancelDate,a.ConfirmationID,a.ConfirmationNo, "
					+ "a.ProductID,a.Batch_No,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM bom_production_order_confirmation_cancel a "
					+ "INNER JOIN bom_production_order_confirmation b ON b.ConfirmationID=a.ConfirmationID AND b.ConfirmationNo=a.ConfirmationNo "
					+ "INNER JOIN bom_production_order c ON c.OrderTypeID=b.OrderTypeID AND c.OrderNo=b.OrderNo "
					+ "WHERE c.PlantID IN ("+Globals.user_area+") "
					+ "ORDER BY a.CancelDate");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlBOMCancelConfirmation mdlCancelConfirmation = new model.mdlBOMCancelConfirmation();
				mdlCancelConfirmation.setCancelConfirmationID(jrs.getString("CancelConfirmationID"));
				mdlCancelConfirmation.setCancelConfirmationNo(jrs.getString("CancelConfirmationNo"));
				
				String newDate=ConvertDateTimeHelper.formatDate(jrs.getString("CancelDate"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlCancelConfirmation.setDate(newDate);
				
				mdlCancelConfirmation.setConfirmationID(jrs.getString("ConfirmationID"));
				mdlCancelConfirmation.setConfirmationNo(jrs.getString("ConfirmationNo"));
				mdlCancelConfirmation.setProductID(jrs.getString("ProductID"));
				mdlCancelConfirmation.setBatchNo(jrs.getString("Batch_No"));
				mdlCancelConfirmation.setQty(jrs.getString("Qty"));
				mdlCancelConfirmation.setUOM(jrs.getString("UOM"));
				mdlCancelConfirmation.setQtyBaseUOM(jrs.getString("QtyBaseUOM"));
				mdlCancelConfirmation.setBaseUOM(jrs.getString("BaseUOM"));
				
				listmdlCancelConfirmation.add(mdlCancelConfirmation);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCancelConfirmation", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCancelConfirmation", "close opened connection protocol", Globals.user);
			}
		}
		
		return listmdlCancelConfirmation;
	}

	public static model.mdlBOMConfirmationProduction LoadProductionOrderByConfirmationDoc(String lconfirmationid, String lconfirmationno) {
	
		model.mdlBOMConfirmationProduction mdlBomConfirmation = new model.mdlBOMConfirmationProduction();
		
		Connection connection = null;
		PreparedStatement pstmLoadConfirmation = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadConfirmation = "SELECT a.OrderTypeID,a.OrderNo "
					+ "FROM bom_production_order_confirmation a "
					+ "WHERE a.ConfirmationID=? AND a.ConfirmationNo=? LIMIT 1";
			
			Globals.gCommand = sqlLoadConfirmation;
			
			pstmLoadConfirmation = connection.prepareStatement(sqlLoadConfirmation);
			
			pstmLoadConfirmation.setString(1, lconfirmationid);
			pstmLoadConfirmation.setString(2, lconfirmationno);
			pstmLoadConfirmation.addBatch();
			
			jrs = pstmLoadConfirmation.executeQuery();
									
			while(jrs.next()){
				mdlBomConfirmation.setOrderTypeID(jrs.getString("OrderTypeID"));
				mdlBomConfirmation.setOrderNo(jrs.getString("OrderNo"));
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProductionOrderByConfirmationDoc", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstmLoadConfirmation != null) {
					 pstmLoadConfirmation.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadProductionOrderByConfirmationDoc", "close opened connection protocol", Globals.user);
			}
		 }

		return mdlBomConfirmation;
	}

	public static String ValidateConfirmationDetail(String lOrderTypeID, String lOrderNo) {
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String Validate = "Valid";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT a.componentline as confirmationdetailline,b.ComponentLine as confirmationbatchline "
						+ "from tempbom_production_order_detail a "
						+ "LEFT JOIN tempbatch_summary b ON a.ComponentLine=b.ComponentLine "
						+ "WHERE a.OrderTypeID=? and a.OrderNo=?";
			
			pstm = connection.prepareStatement(sql);
			
			pstm.setString(1, lOrderTypeID);
			pstm.setString(2, lOrderNo);
			pstm.addBatch();
			
			Globals.gCommand = pstm.toString();
			
			jrs = pstm.executeQuery();
									
			while(jrs.next()){
				if(jrs.getString("confirmationbatchline") == null)
				{
					Validate = "Invalid"+jrs.getString("confirmationdetailline");
					break;
				}
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ValidateConfirmationDetail", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "ValidateConfirmationDetail", "close opened connection protocol", Globals.user);
			}
		 }

		return Validate;
	}
	
}

// ------------------- CLOSE CODE ---------------------- //

//to show join of production order detail and its confirmation in order to get the latest target production
//public static List<model.mdlBOMProductionOrderDetail> LoadBOMProductionOrderDetailforConfirmation(String ordertypeid, String orderno) throws Exception {
//	List<model.mdlBOMProductionOrderDetail> listmdlBOMProductionOrderDetail = new ArrayList<model.mdlBOMProductionOrderDetail>();
//	
//	Connection connection = database.RowSetAdapter.getConnection();
//	PreparedStatement pstmLoadBomProductionOrderDetail = null;
//	ResultSet jrs = null;
//	ResultSet jrs2 = null;
//	try{
//		
//		String sqlLoadBomProductionOrderDetail = "SELECT b.ConfirmationID,b.ConfirmationNo,a.OrderTypeID,"
//				+ "a.OrderNo,a.ComponentLine,a.ComponentID,a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM,c.Batch_No,"
//				+ "c.QtyBaseUOM AS ConfirmedQtyBase,c.BaseUOM AS ConfirmedBaseUOM "
//				+ "FROM bom_production_order_detail a "
//				+ "LEFT JOIN bom_production_order_confirmation b ON b.OrderTypeID=a.OrderTypeID AND b.OrderNo=a.OrderNo "
//				+ "LEFT JOIN bom_production_order_detail_confirmation c ON c.ConfirmationID=b.ConfirmationID AND c.ConfirmationNo=b.ConfirmationNo AND c.ComponentLine=a.ComponentLine "
//				+ "WHERE a.OrderTypeID=? AND a.OrderNo=? "
//				+ "ORDER BY a.ComponentLine";
//		
//		Globals.gCommand = sqlLoadBomProductionOrderDetail;
//		
//		pstmLoadBomProductionOrderDetail = connection.prepareStatement(sqlLoadBomProductionOrderDetail);
//		
//		pstmLoadBomProductionOrderDetail.setString(1, ordertypeid);
//		pstmLoadBomProductionOrderDetail.setString(2, orderno);
//		pstmLoadBomProductionOrderDetail.addBatch();
//		
//		jrs = pstmLoadBomProductionOrderDetail.executeQuery();
//			
//		int tempQtyTarget,tempQtyConfirmed,newQty,templastQty=0;
//		String tempComponentLine,templastComponentLine="";
//		model.mdlBOMProductionOrderDetail tempmodel = new model.mdlBOMProductionOrderDetail();
//		
//		int dataRow = 0; //define dataRow
//		int numberLoop = 1; //define loop number
//		
//		//get the total row of data
//		while(jrs.next())
//		{
//			dataRow++;
//		}
////		jrs.close();
//		
//		jrs2 = pstmLoadBomProductionOrderDetail.executeQuery();
//		//data proccess
//		while(jrs2.next()){
//			model.mdlBOMProductionOrderDetail mdlBomProductionOrderDetail = new model.mdlBOMProductionOrderDetail();
//		
//			tempComponentLine = jrs2.getString("ComponentLine"); //component line
//			tempQtyTarget = Integer.parseInt(jrs2.getString("QtyBaseUOM")); //target qty base
//			if(jrs2.getString("ConfirmedQtyBase") == null) //validation if confirmation qty is null
//				tempQtyConfirmed = 0; //confirmation qty 
//			else
//				tempQtyConfirmed = Integer.parseInt(jrs2.getString("ConfirmedQtyBase")); //confirmation qty
//			newQty = tempQtyTarget - tempQtyConfirmed; //target qty base final
//			
//			if(tempComponentLine.contentEquals(templastComponentLine)) //check if the component line before is same with the next line 
//			{
//				newQty = templastQty - tempQtyConfirmed; //if the component line is same, the previous result qty - the present input qty
//			}
//			
//			
//			//define model
//			mdlBomProductionOrderDetail.setComponentLine(jrs2.getString("ComponentLine"));
//			mdlBomProductionOrderDetail.setComponentID(jrs2.getString("ComponentID"));
//			if( (tempQtyConfirmed == 0) && (jrs2.getString("ConfirmationID") == null) )
//			{
//				mdlBomProductionOrderDetail.setQty(jrs2.getString("Qty"));
//				mdlBomProductionOrderDetail.setUOM(jrs2.getString("UOM"));
//			}
//			else
//			{
//				mdlBomProductionOrderDetail.setQty(String.valueOf(newQty));
//				mdlBomProductionOrderDetail.setUOM(jrs2.getString("BaseUOM"));
//			}
//			mdlBomProductionOrderDetail.setQtyBaseUOM(jrs2.getString("QtyBaseUOM"));
//			mdlBomProductionOrderDetail.setBaseUOM(jrs2.getString("BaseUOM"));
//			
//			if( (!tempComponentLine.contentEquals(templastComponentLine)) && (!templastComponentLine.contentEquals("")))
//				listmdlBOMProductionOrderDetail.add(tempmodel); //add data to the list
//			
//			
//			//temporary model for adding to the list
//			tempmodel = new model.mdlBOMProductionOrderDetail();
//			
//			tempmodel.setComponentLine(jrs2.getString("ComponentLine"));
//			tempmodel.setComponentID(jrs2.getString("ComponentID"));
//			if( (tempQtyConfirmed == 0) && (jrs2.getString("ConfirmationID") == null) )
//			{
//				tempmodel.setQty(jrs2.getString("Qty"));
//				tempmodel.setUOM(jrs2.getString("UOM"));
//			}
//			else
//			{
//				tempmodel.setQty(String.valueOf(newQty));
//				tempmodel.setUOM(jrs2.getString("BaseUOM"));
//			}
//			tempmodel.setQtyBaseUOM(jrs2.getString("QtyBaseUOM"));
//			tempmodel.setBaseUOM(jrs2.getString("BaseUOM"));
//			
//			if(numberLoop == dataRow)
//				listmdlBOMProductionOrderDetail.add(tempmodel); //add last data to the list 
//			
//			
//			templastComponentLine = tempComponentLine; //get the last component line
//			templastQty = newQty; //get the last target quantity
//			numberLoop++; //increase number loop
//		}
////		jrs2.close();
////		pstmLoadBomProductionOrderDetail.close();	
//	}
//	catch(Exception ex){
//		LogAdapter.InsertLogExc(ex.toString(), "LoadBOMProductionOrderDetailforConfirmation", Globals.gCommand , Globals.user);
//	}
//	finally {
//		 if (pstmLoadBomProductionOrderDetail != null) {
//			 pstmLoadBomProductionOrderDetail.close();
//		 }
//		 if (connection != null) {
//			 connection.close();
//		 }
//		 if (jrs != null) {
//			 jrs.close();
//		 }
//		 if (jrs2 != null) {
//			 jrs2.close();
//		 }
//	 }
//
//	return listmdlBOMProductionOrderDetail;
//}
