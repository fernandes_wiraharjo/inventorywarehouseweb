package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Globals;

public class CancelProductionOrderAdapter {

	public static boolean CheckOutstandingProductionOrder(String lOrderTypeID, String lOrderNo) {
		List<model.mdlBOMConfirmationProduction> listBOMConfirmation = new ArrayList<model.mdlBOMConfirmationProduction>();
		List<model.mdlBOMCancelConfirmation> listBOMCancelConfirmation = new ArrayList<model.mdlBOMCancelConfirmation>();
		boolean check = false;
		
		Connection connection = null;
		PreparedStatement pstmLoadConfirmation = null;
		PreparedStatement pstmLoadCancelConfirmation = null;
		ResultSet jrs = null;
		ResultSet jrs2 = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			//load 1
			String sqlLoadConfirmation = "select ConfirmationID,ConfirmationNo "
					+ "from bom_production_order_confirmation "
					+ "where OrderTypeID = ? AND OrderNo = ?";
			
			pstmLoadConfirmation = connection.prepareStatement(sqlLoadConfirmation);
			
			pstmLoadConfirmation.setString(1, lOrderTypeID);
			pstmLoadConfirmation.setString(2, lOrderNo);
			Globals.gCommand = pstmLoadConfirmation.toString();
			pstmLoadConfirmation.addBatch();
			
			jrs = pstmLoadConfirmation.executeQuery();
									
			while(jrs.next()){
				model.mdlBOMConfirmationProduction BOMConfirmation = new model.mdlBOMConfirmationProduction();
				
				BOMConfirmation.setConfirmationID(jrs.getString("ConfirmationID"));
				BOMConfirmation.setConfirmationNo(jrs.getString("ConfirmationNo"));
				
				listBOMConfirmation.add(BOMConfirmation);
			}
			
			//load 2
			String sqlLoadCancelConfirmation = "select a.ConfirmationID,a.ConfirmationNo "
					+ "from bom_production_order_confirmation_cancel a "
					+ "INNER JOIN bom_production_order_confirmation b ON b.ConfirmationID=a.ConfirmationID AND b.ConfirmationNo=a.ConfirmationNo "
					+ "where b.OrderTypeID = ? AND b.OrderNo = ?";
			
			pstmLoadCancelConfirmation = connection.prepareStatement(sqlLoadCancelConfirmation);
			
			pstmLoadCancelConfirmation.setString(1, lOrderTypeID);
			pstmLoadCancelConfirmation.setString(2, lOrderNo);
			Globals.gCommand = pstmLoadCancelConfirmation.toString();
			pstmLoadCancelConfirmation.addBatch();
			
			jrs2 = pstmLoadCancelConfirmation.executeQuery();
									
			while(jrs2.next()){
				model.mdlBOMCancelConfirmation BOMCancelConfirmation = new model.mdlBOMCancelConfirmation();
				
				BOMCancelConfirmation.setConfirmationID(jrs2.getString("ConfirmationID"));
				BOMCancelConfirmation.setConfirmationNo(jrs2.getString("ConfirmationNo"));
				
				listBOMCancelConfirmation.add(BOMCancelConfirmation);
			}
			
			if( (listBOMConfirmation.size() == listBOMCancelConfirmation.size()) )
			{
				check = true;
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CheckOutstandingProductionOrder", Globals.gCommand , Globals.user);
		}
		 finally {
			 try{
				 if (pstmLoadCancelConfirmation != null) {
					 pstmLoadCancelConfirmation.close();
				 }
				 if (pstmLoadConfirmation != null) {
					 pstmLoadConfirmation.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
				 if (jrs2 != null) {
					 jrs2.close();
				 }
			 }
			 catch(Exception e){
				 LogAdapter.InsertLogExc(e.toString(), "CheckOutstandingProductionOrder", "close opened connection protocol", Globals.user);
			 }
		 }

		return check;
	}
	
}
