package adapter;

import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import model.Globals;
import model.mdlVendor;


/** Documentation
 * 
 */
public class VendorAdapter {
	
	@SuppressWarnings("resource")
	public static model.mdlVendor LoadVendorbyID(String lVendorID) {
		model.mdlVendor mdlVendor = new model.mdlVendor();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT VendorID, VendorType, VendorName, VendorAddress, Phone, PIC FROM vendor WHERE VendorID = ?");
			jrs.setString(1,  lVendorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				mdlVendor.setVendorName(jrs.getString("VendorName"));
				mdlVendor.setVendorType(jrs.getString("VendorType"));
				mdlVendor.setVendorAddress(jrs.getString("VendorAddress"));
				mdlVendor.setPIC(jrs.getString("PIC"));
				mdlVendor.setPhone(jrs.getString("Phone"));	
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorbyID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadVendorbyID", "close opened connection protocol", Globals.user);
			}
		}

		return mdlVendor;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlVendor> LoadVendor(String lUser) {
		List<model.mdlVendor> listmdlVendor = new ArrayList<model.mdlVendor>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT VendorID, VendorType, VendorName, VendorAddress, Phone, PIC FROM vendor");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlVendor mdlVendor = new model.mdlVendor();
				mdlVendor.setVendorID(jrs.getString("VendorID"));
				mdlVendor.setVendorType(jrs.getString("VendorType"));
				mdlVendor.setVendorName(jrs.getString("VendorName"));
				mdlVendor.setVendorAddress(jrs.getString("VendorAddress"));
				mdlVendor.setPhone(jrs.getString("Phone"));
				mdlVendor.setPIC(jrs.getString("PIC"));				
				
				listmdlVendor.add(mdlVendor);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendor", Globals.gCommand , lUser);
		}
		finally {
			try {
				if(jrs!=null) {
					jrs.close();
				}
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "LoadVendor", "close opened connection protocol", lUser);
			}
		}

		return listmdlVendor;
	}
	
	@SuppressWarnings("resource")
	public static String InsertVendor(model.mdlVendor lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlVendor CheckDuplicateVendor = LoadVendorbyID(lParam.getVendorID());
			if(CheckDuplicateVendor.getVendorName() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();			
				
				jrs.setCommand("SELECT VendorID, VendorType, VendorName, VendorAddress, Phone, PIC FROM vendor LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
				
				jrs.moveToInsertRow();
				jrs.updateString("VendorID",lParam.getVendorID());
				jrs.updateString("VendorName",lParam.getVendorName());
				jrs.updateString("VendorAddress",lParam.getVendorAddress());
				jrs.updateString("Phone",lParam.getPhone());
				jrs.updateString("PIC",lParam.getPIC());
				jrs.updateString("VendorType",lParam.getVendorType());
				
				
				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Vendor";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Vendor sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertVendor", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Insert Vendor";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertVendor", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String UpdateVendor(model.mdlVendor lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT VendorID, VendorType, VendorName, VendorAddress, Phone, PIC FROM vendor WHERE VendorID = ? LIMIT 1");
			jrs.setString(1,  lParam.getVendorID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("VendorName", lParam.getVendorName());
			jrs.updateString("VendorAddress", lParam.getVendorAddress());
			jrs.updateString("Phone", lParam.getPhone());
			jrs.updateString("PIC", lParam.getPIC());
			jrs.updateString("VendorType",lParam.getVendorType());
			jrs.updateRow();
				
			Globals.gReturn_Status = "Success Update Vendor ";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateVendor", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Update Vendor";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateVendor", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String DeleteVendor(String lVendorID, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT VendorID, VendorType, VendorName, VendorAddress, Phone, PIC FROM vendor WHERE VendorID = ? LIMIT 1");
			jrs.setString(1, lVendorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			jrs.close();
			Globals.gReturn_Status = "Success Delete Vendor";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteVendor", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Delete Vendor";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteVendor", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}

}
