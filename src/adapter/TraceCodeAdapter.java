package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import model.Globals;
import model.mdlTraceCode;
import model.mdlWMNumberRanges;


/** Documentation
 * 001 Nanda - beginning
 */
public class TraceCodeAdapter {
	
	@SuppressWarnings("resource")
	public static void InsertTraceCode(String ltracecode, String lDate, String lFlag)
	{
		Globals.gCommand = "Insert Trace no : " + ltracecode + " Date : " + lDate ;
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();			
			
			jrs.setCommand("SELECT `Trace_Code`,`Flag_Table`, `Created_at` FROM TraceCodeGenerate LIMIT 1");  //001 Nanda
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("Trace_Code",ltracecode);
			jrs.updateString("Flag_Table",lFlag);
			jrs.updateString("Created_at",lDate);
			
			jrs.insertRow();
			Globals.gReturn_Status = "Success Insert Trace Code";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertTraceCode", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Error Insert Trace Code";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "InsertTraceCode", "close opened connection protocol", Globals.user);
			}
		}
		
		return;
	}
	
	@SuppressWarnings("resource")
	public static model.mdlTraceCode LoadBatchTraceCode(String lDate) {
//		List<model.mdlTraceCode> listmdlTraceCode = new ArrayList<model.mdlTraceCode>();
		model.mdlTraceCode mdlTraceCode = new model.mdlTraceCode();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `Trace_Code`, `Created_at` FROM TraceCodeGenerate WHERE Flag_Table='Batch' AND DATE(`Created_at`) = CURDATE() ORDER BY `Created_at` desc Limit 1");
//			jrs.setString(1, lDate);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				
				mdlTraceCode.setTraceCode(jrs.getString("Trace_Code"));
				mdlTraceCode.setDate(jrs.getString("Created_at"));		
				
//				listmdlTraceCode.add(mdlTraceCode);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadTraceCode", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadTraceCode", "close opened connection protocol", Globals.user);
			}
		}

		return mdlTraceCode;
	}
	
	public static String DeleteTraceCode(String ltracecode)
	{
//		Globals.gCommand = "Delete Trace Code : " + ltracecode;
		String Command = "Delete Trace Code : " + ltracecode;
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT `Trace_Code`, `Created_at` FROM tracecodegenerate WHERE `Trace_Code` = ? LIMIT 1");
			jrs.setString(1, ltracecode);
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			jrs.close();
			Globals.gReturn_Status = "Success Delete Trace Code Batch No";
		}
		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "DeleteTraceCode", Globals.gCommand , lUser);
			LogAdapter.InsertLogExc(ex.toString(), "DeleteTraceCode", Command , Globals.user);
			Globals.gReturn_Status = "Error Delete Trace Code Batch No";
		}
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static model.mdlTraceCode LoadTraceCodeDocNo(String lYear, String lFlagTable) {
		model.mdlTraceCode mdlTraceCode = new model.mdlTraceCode();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `Trace_Code`, `Created_at` FROM TraceCodeGenerate WHERE Flag_Table=? AND YEAR(STR_TO_DATE(`Created_at`, '%Y'))  = ? ORDER BY `Created_at` desc Limit 1");
			jrs.setString(1, lFlagTable);
			jrs.setString(2, lYear);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				
				mdlTraceCode.setTraceCode(jrs.getString("Trace_Code"));
				mdlTraceCode.setDate(jrs.getString("Created_at"));		
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadTraceCode", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadTraceCode", "close opened connection protocol", Globals.user);
			}
		}

		return mdlTraceCode;
	}

	public static model.mdlTraceCode LoadLastWMTraceCodeByFlag(String lFlagTable) {
		model.mdlTraceCode mdlTraceCode = new model.mdlTraceCode();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `Trace_Code`, `Created_at` "
					+ "FROM TraceCodeGenerate "
					+ "WHERE Flag_Table=? "
					+ "ORDER BY CAST(`Trace_Code` AS UNSIGNED) desc Limit 1");
			jrs.setString(1, lFlagTable);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				mdlTraceCode.setTraceCode(jrs.getString("Trace_Code"));	
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadLastTraceCodeByFlag", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadLastTraceCodeByFlag", "close opened connection protocol", Globals.user);
			}
		}

		return mdlTraceCode;
	}
	
	public static String DeleteTraceCode2(List<model.mdlTraceCode> listParam)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmDeleteTraceCode = null;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlDeleteTraceCode = "DELETE FROM tracecodegenerate WHERE Trace_Code=? AND Flag_Table=?;";
				pstmDeleteTraceCode = connection.prepareStatement(sqlDeleteTraceCode);
				
				//delete Trace code process parameter
				for(model.mdlTraceCode param : listParam){
					pstmDeleteTraceCode.setString(1,  param.getTraceCode());
					pstmDeleteTraceCode.setString(2,  param.getFlag_Table());
					Globals.gCommand = pstmDeleteTraceCode.toString();
					pstmDeleteTraceCode.executeUpdate();
				}
				
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Delete Trace Code";
			}
			
			catch (Exception e ) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionDeleteTraceCode2", Globals.gCommand , Globals.user);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "DeleteTraceCode2", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
			        if (pstmDeleteTraceCode != null) {
			        	pstmDeleteTraceCode.close();
			        }
			        connection.setAutoCommit(true);
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "DeleteTraceCode2", "close opened connection protocol", Globals.user);
				}
		    }
			
		return Globals.gReturn_Status;
	}
	
}
