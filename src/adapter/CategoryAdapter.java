package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

public class CategoryAdapter {
	public static List<model.mdlCategory> GetCategoryAPI(String lUser){
	List<model.mdlCategory> listmdlCategory = new ArrayList<model.mdlCategory>();
	model.mdlToken mdlToken = TokenAdapter.GetToken();
	try {
		
		Client client = Client.create();
		
		Globals.gCommand = "http://dev.webarq.info:8081/kenmaster-be/api/category/listing";
		WebResource webResource = client.resource(Globals.gCommand);
		
		Gson gson = new Gson();
		String json = gson.toJson(mdlToken);
		
		ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);		
		
		Globals.gReturn_Status = response.getEntity(String.class);
		
		//Type TypeDepartment = new TypeToken<ArrayList<model.mdlDepartment>>(){}.getClass();
		listmdlCategory = gson.fromJson(Globals.gReturn_Status, model.mdlCategorylist.class);
		
		
	  } catch (Exception ex) {
		LogAdapter.InsertLogExc(ex.toString(), "GetCategoryAPI", Globals.gCommand , lUser);
//		Globals.gReturn_Status = "Error GetToken";
	  }
	
	return listmdlCategory;
	}

	public static String InsertCategorylist(List<model.mdlCategory> lParamlist, String lUser)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			Gson gson = new Gson();
			Globals.gCommand = gson.toJson(lParamlist);
			
			String sql = "INSERT INTO `category`(ID, Group_ID, Code, Image, Title_EN, Description_EN, `Order`, Status, Slug, Created_at, Updated_at, Link_Image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE ID=ID";
			pstm = connection.prepareStatement(sql);
			
			for(model.mdlCategory lParam : lParamlist)
			{	
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getId()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getGroup_id()));
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getCode()));
				pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getImage()));
				pstm.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getTitle_en()));
				pstm.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getDescription_en()));
				pstm.setString(7,  ValidateNull.NulltoStringEmpty(lParam.getOrder()));
				pstm.setString(8,  ValidateNull.NulltoStringEmpty(lParam.getStatus()));
				pstm.setString(9, ValidateNull.NulltoStringEmpty(lParam.getSlug()));
				pstm.setString(10, ValidateNull.NulltoStringEmpty(lParam.getCreated_at()));
				pstm.setString(11, ValidateNull.NulltoStringEmpty(lParam.getUpdated_at()));
				pstm.setString(12, ValidateNull.NulltoStringEmpty(lParam.getLink_image()));
				
				//pstm.executeUpdate();
				pstm.addBatch();
			}
			pstm.executeBatch();
			
			Globals.gReturn_Status = "Success Insert list Category API";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertCategorybyAPI", Globals.gCommand , lUser);
			Globals.gReturn_Status = "Error Insert list Category API";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertCategorybyAPI", "close opened connection protocol", lUser);
			}
		 }
		
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlCategory> LoadCategory() {
		List<model.mdlCategory> listmdlCategory = new ArrayList<model.mdlCategory>();
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT ID, Group_ID, Code, Image, Title_EN, Description_EN, `Order`, Status, Slug, Created_at, Updated_at, Link_Image FROM `category`");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlCategory mdlCategory = new model.mdlCategory();
				mdlCategory.setId(jrs.getString("ID"));
				mdlCategory.setGroup_id(jrs.getString("Group_ID"));
				mdlCategory.setCode(jrs.getString("Code"));
				mdlCategory.setImage(jrs.getString("Image"));
				mdlCategory.setTitle_en(jrs.getString("Title_EN"));
				mdlCategory.setDescription_en(jrs.getString("Description_EN"));	
				mdlCategory.setOrder(jrs.getString("Order"));
				mdlCategory.setStatus(jrs.getString("Status"));
				mdlCategory.setSlug(jrs.getString("Slug"));
				mdlCategory.setCreated_at(jrs.getString("Created_at"));
				mdlCategory.setUpdated_at(jrs.getString("Updated_at"));
				mdlCategory.setLink_image(jrs.getString("Link_Image"));
				
				listmdlCategory.add(mdlCategory);
			}
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCategory", Globals.gCommand , "");
		}

		return listmdlCategory;
	}


}
