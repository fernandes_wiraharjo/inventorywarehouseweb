package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlBomOrderType;
import model.mdlWMNumberRanges;

public class WMNumberRangesAdapter {

	public static List<model.mdlWMNumberRanges> LoadNumberRangesforWM(){
		List<model.mdlWMNumberRanges> listData = new ArrayList<model.mdlWMNumberRanges>();
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT PlantID, WarehouseID, FromNumber , ToNumber, NumberRangesType "
					+ "FROM wms_number_ranges "
					+ "WHERE PlantID = "+Globals.gPlantID+"";
			
			Globals.gCommand = sql;
			
			pstm = connection.prepareStatement(sql);
			
			jrs = pstm.executeQuery();
									
			while(jrs.next()){
				model.mdlWMNumberRanges data = new model.mdlWMNumberRanges();
				
				data.setPlantID(jrs.getString("PlantID"));
				data.setWarehouseID(jrs.getString("WarehouseID"));
				data.setFromNumber(jrs.getString("FromNumber"));
				data.setToNumber(jrs.getString("ToNumber"));
				data.setNumberRangesType(jrs.getString("NumberRangesType"));
				
				listData.add(data);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadNumberRangesforWM", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstm != null) {
					 pstm.close();
				 }
	
				 if (connection != null) {
					 connection.close();
				 }
				 
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e)
			 {
				 LogAdapter.InsertLogExc(e.toString(), "LoadNumberRangesforWM", "close opened connection protocol", Globals.user);
			 }
		 }

		return listData;
	}

	public static model.mdlWMNumberRanges LoadWMNumberRangesByKey(model.mdlWMNumberRanges lParam){
		model.mdlWMNumberRanges mdlWMNumberRanges = new model.mdlWMNumberRanges();
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT PlantID, WarehouseID, FromNumber , ToNumber, NumberRangesType "
					+ "FROM wms_number_ranges WHERE NumberRangesType=? AND PlantID=? AND WarehouseID=?";
			
			Globals.gCommand = sql;
			
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, lParam.getNumberRangesType());
			pstm.setString(2, lParam.getPlantID());
			pstm.setString(3, lParam.getWarehouseID());
			pstm.addBatch();
			
			jrs = pstm.executeQuery();
									
			while(jrs.next()){
				mdlWMNumberRanges.setFromNumber(jrs.getString("FromNumber"));
				mdlWMNumberRanges.setToNumber(jrs.getString("ToNumber"));
				mdlWMNumberRanges.setNumberRangesType(jrs.getString("NumberRangesType"));
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadWMNumberRangesByKey", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstm != null) {
					 pstm.close();
				 }
	
				 if (connection != null) {
					 connection.close();
				 }
				 
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e)
			 {
				 LogAdapter.InsertLogExc(e.toString(), "LoadWMNumberRangesByKey", "close opened connection protocol", Globals.user);
			 }
		 }

		return mdlWMNumberRanges;
	}
	
	@SuppressWarnings("resource")
	public static String InsertNumberRangesforWM(model.mdlWMNumberRanges lParam)
	{
		Globals.gCommand = "Insert WM Number Ranges : " + lParam.getPlantID() + "-" + lParam.getWarehouseID() + "-" +lParam.getNumberRangesType();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlWMNumberRanges CheckDuplicate = LoadWMNumberRangesByKey(lParam);
			if(CheckDuplicate.getNumberRangesType() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();			
				
				jrs.setCommand("SELECT PlantID, WarehouseID, FromNumber, ToNumber, NumberRangesType FROM wms_number_ranges LIMIT 1");
				jrs.execute();
				
				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("FromNumber",lParam.getFromNumber());
				jrs.updateString("ToNumber",lParam.getToNumber());
				jrs.updateString("NumberRangesType",lParam.getNumberRangesType());
				
				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Number Ranges";
			}
			//if duplicate
			else {
				Globals.gReturn_Status = "Urutan penomoran sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertNumberRangesforWM", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "InsertNumberRangesforWM", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String UpdateNumberRangesforWM(model.mdlWMNumberRanges lParam)
	{
		Globals.gCommand = "Update WM Number Ranges : " + lParam.getNumberRangesType();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT PlantID, WarehouseID, FromNumber, ToNumber, NumberRangesType FROM wms_number_ranges "
					+ "WHERE NumberRangesType = ? AND PlantID=? AND WarehouseID=? "
					+ "LIMIT 1");
			jrs.setString(1,  lParam.getNumberRangesType());
			jrs.setString(2,  lParam.getPlantID());
			jrs.setString(3,  lParam.getWarehouseID());
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("FromNumber", lParam.getFromNumber());
			jrs.updateString("ToNumber", lParam.getToNumber());
			jrs.updateRow();
				
			Globals.gReturn_Status = "Success Update Number Ranges";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateNumberRangesforWM", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "UpdateNumberRangesforWM", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeleteNumberRangesforWM(String lNumberRangeType, String lPlantID, String lWarehouseID)
	{
		Globals.gCommand = "Delete WM Number Range : " + lNumberRangeType;
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT PlantID, WarehouseID, FromNumber, ToNumber, NumberRangesType "
					+ "FROM wms_number_ranges "
					+ "WHERE NumberRangesType = ? AND PlantID=? AND WarehouseID=? "
					+ "LIMIT 1");
			jrs.setString(1, lNumberRangeType);
			jrs.setString(2, lPlantID);
			jrs.setString(3, lWarehouseID);
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			Globals.gReturn_Status = "Success Delete Number Ranges";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteNumberRangesforWM", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "DeleteNumberRangesforWM", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}
	
}
