package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import model.Globals;
import model.mdlCustomer;

/** Documentation
 * 
 */
public class CustomerAdapter {
	
	@SuppressWarnings("resource")
	public static model.mdlCustomer LoadCustomerbyID(String lCustomerID) {
		model.mdlCustomer mdlCustomer = new model.mdlCustomer();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT CustomerName, CustomerAddress, Phone, Email, PIC, CustomerTypeID FROM Customer WHERE CustomerID = ?");
			jrs.setString(1,  lCustomerID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				mdlCustomer.setCustomerID(lCustomerID);
				mdlCustomer.setCustomerName(jrs.getString("CustomerName"));
				mdlCustomer.setCustomerAddress(jrs.getString("CustomerAddress"));
				mdlCustomer.setPhone(jrs.getString("Phone"));
				mdlCustomer.setEmail(jrs.getString("Email"));
				mdlCustomer.setPic(jrs.getString("Pic"));
				mdlCustomer.setCustomerTypeID(jrs.getString("CustomerTypeID"));
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomerbyID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCustomerbyID", "close opened connection protocol", Globals.user);
			}
		}

		return mdlCustomer;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlCustomer> LoadCustomer(String lUser) {
		List<model.mdlCustomer> listmdlCustomer = new ArrayList<model.mdlCustomer>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT CustomerID, CustomerName, CustomerAddress, Phone, Email, PIC, CustomerTypeID FROM Customer");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlCustomer mdlCustomer = new model.mdlCustomer();
				mdlCustomer.setCustomerID(jrs.getString("CustomerID"));
				mdlCustomer.setCustomerName(jrs.getString("CustomerName"));
				mdlCustomer.setCustomerAddress(jrs.getString("CustomerAddress"));
				mdlCustomer.setPhone(jrs.getString("Phone"));
				mdlCustomer.setEmail(jrs.getString("Email"));
				mdlCustomer.setPic(jrs.getString("Pic"));
				mdlCustomer.setCustomerTypeID(jrs.getString("CustomerTypeID"));			
				
				listmdlCustomer.add(mdlCustomer);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomer", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCustomer", "close opened connection protocol", lUser);
			}
		}

		return listmdlCustomer;
	}
	
	public static List<model.mdlCustomer> LoadCustomerJoin(String lUser) {
		List<model.mdlCustomer> listmdlCustomer = new ArrayList<model.mdlCustomer>();
		
		Connection connection = null;
		PreparedStatement pstmLoadCustomer = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadCustomer = "SELECT a.CustomerID, a.CustomerName, a.CustomerAddress, a.Phone, a.Email, a.PIC, a.CustomerTypeID, b.CustomerTypeName "
					+ "FROM customer a "
					+ "LEFT JOIN customer_type b ON b.CustomerTypeID = a.CustomerTypeID";
			
			pstmLoadCustomer = connection.prepareStatement(sqlLoadCustomer);
			Globals.gCommand = pstmLoadCustomer.toString();
			
			jrs = pstmLoadCustomer.executeQuery();
									
			while(jrs.next()){
				model.mdlCustomer mdlCustomer = new model.mdlCustomer();
				mdlCustomer.setCustomerID(jrs.getString("CustomerID"));
				mdlCustomer.setCustomerName(jrs.getString("CustomerName"));
				mdlCustomer.setCustomerAddress(jrs.getString("CustomerAddress"));
				mdlCustomer.setPhone(jrs.getString("Phone"));
				mdlCustomer.setEmail(jrs.getString("Email"));
				mdlCustomer.setPic(jrs.getString("Pic"));
				mdlCustomer.setCustomerTypeID(jrs.getString("CustomerTypeID"));	
				mdlCustomer.setCustomerTypeName(jrs.getString("CustomerTypeName"));
				
				listmdlCustomer.add(mdlCustomer);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomerJoin", Globals.gCommand , lUser);
		}
		finally {
			try{
				 if (pstmLoadCustomer != null) {
					 pstmLoadCustomer.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCustomerJoin", "close opened connection protocol", lUser);
			}
		 }

		return listmdlCustomer;
	}
	
	@SuppressWarnings("resource")
	public static String InsertCustomer(model.mdlCustomer lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlCustomer CheckDuplicateCustomer = LoadCustomerbyID(lParam.getCustomerID());
			if(CheckDuplicateCustomer.getCustomerID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();			
				
				jrs.setCommand("SELECT CustomerID, CustomerName, CustomerAddress, Phone, Email, PIC, CustomerTypeID FROM Customer LIMIT 1");
				jrs.execute();
				
				jrs.moveToInsertRow();
				jrs.updateString("CustomerID",lParam.getCustomerID());
				jrs.updateString("CustomerName",lParam.getCustomerName());
				jrs.updateString("CustomerAddress",lParam.getCustomerAddress());
				jrs.updateString("Phone",lParam.getPhone());
				jrs.updateString("Email",lParam.getEmail());
				jrs.updateString("PIC",lParam.getPic());
				jrs.updateString("CustomerTypeID",lParam.getCustomerTypeID());
				Globals.gCommand = jrs.getCommand();
				
				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Customer";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Pelanggan sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertCustomer", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Insert Customer";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertCustomer", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String UpdateCustomer(model.mdlCustomer lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT CustomerID, CustomerName, CustomerAddress, Phone, Email, PIC, CustomerTypeID FROM Customer WHERE CustomerID=? LIMIT 1");
			jrs.setString(1,  lParam.getCustomerID());
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("CustomerName",lParam.getCustomerName());
			jrs.updateString("CustomerAddress",lParam.getCustomerAddress());
			jrs.updateString("Phone",lParam.getPhone());
			jrs.updateString("Email",lParam.getEmail());
			jrs.updateString("PIC",lParam.getPic());
			jrs.updateString("CustomerTypeID",lParam.getCustomerTypeID());
			Globals.gCommand = jrs.getCommand();
			jrs.updateRow();
			
			Globals.gReturn_Status = "Success Update Customer";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateCustomer", Globals.gCommand , lUser);
//			Globals.gReturn_Status = "Error Update Customer";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateCustomer", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	public static String DeleteCustomer(String lCustomerID, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT CustomerID, CustomerName, CustomerAddress, Phone, PIC FROM Customer WHERE CustomerID = ? LIMIT 1");
			jrs.setString(1, lCustomerID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			Globals.gReturn_Status = "Success Delete Customer";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteCustomer", Globals.gCommand , lUser);
//			Globals.gReturn_Status = "Error Delete Customer";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadVendor", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}

}
