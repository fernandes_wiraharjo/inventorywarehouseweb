package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlStorageSectionIndicator;

public class StorageSectionIndicatorAdapter {
	@SuppressWarnings("resource")
	public static model.mdlStorageSectionIndicator LoadStorageSectionIndicatorByKey(String lPlantID, String lWarehouseID, String lStorageSectionIndicatorID) {
		model.mdlStorageSectionIndicator mdlStorageSectionIndicator = new model.mdlStorageSectionIndicator();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageSectionIndicatorID, StorageSectionIndicatorName FROM wms_storagesectionindicator "
							+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageSectionIndicatorID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lStorageSectionIndicatorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlStorageSectionIndicator.setPlantID(jrs.getString("PlantID"));
				mdlStorageSectionIndicator.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageSectionIndicator.setStorageSectionIndicatorID(jrs.getString("StorageSectionIndicatorID"));
				mdlStorageSectionIndicator.setStorageSectionIndicatorName(jrs.getString("StorageSectionIndicatorName"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageSectionIndicatorByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageSectionIndicatorByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlStorageSectionIndicator;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlStorageSectionIndicator> LoadStorageSectionIndicatorByPlantWarehouse(String lPlantID, String lWarehouseID) {
		List<model.mdlStorageSectionIndicator> listStorageSectionIndicator = new ArrayList<mdlStorageSectionIndicator>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageSectionIndicatorID, StorageSectionIndicatorName FROM wms_storagesectionindicator "
							+ "WHERE PlantID = ? AND WarehouseID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				model.mdlStorageSectionIndicator mdlStorageSectionIndicator = new model.mdlStorageSectionIndicator();
				
				mdlStorageSectionIndicator.setPlantID(jrs.getString("PlantID"));
				mdlStorageSectionIndicator.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageSectionIndicator.setStorageSectionIndicatorID(jrs.getString("StorageSectionIndicatorID"));
				mdlStorageSectionIndicator.setStorageSectionIndicatorName(jrs.getString("StorageSectionIndicatorName"));
				
				listStorageSectionIndicator.add(mdlStorageSectionIndicator);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageSectionIndicatorByPlantWarehouse", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageSectionIndicatorByPlantWarehouse", "close opened connection protocol", Globals.user);
			}
		}

		return listStorageSectionIndicator;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlStorageSectionIndicator> LoadStorageSectionIndicator() {
		List<model.mdlStorageSectionIndicator> listmdlStorageSectionIndicator = new ArrayList<model.mdlStorageSectionIndicator>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			String sqlLoadStorageSectionIndicator = "SELECT StorageSectionIndicator.PlantID, plant.PlantName, StorageSectionIndicator.WarehouseID, warehouse.WarehouseName, "
					+ "StorageSectionIndicator.StorageSectionIndicatorID, StorageSectionIndicator.StorageSectionIndicatorName "
					+ "FROM wms_storagesectionindicator StorageSectionIndicator "
					+ "LEFT JOIN plant ON plant.PlantID = StorageSectionIndicator.PlantID "
					+ "LEFT JOIN warehouse ON warehouse.WarehouseID = StorageSectionIndicator.WarehouseID "
					+ "AND warehouse.PlantID = StorageSectionIndicator.PlantID "
					+ "WHERE StorageSectionIndicator.PlantID IN (" + Globals.user_area + ")";

			jrs.setCommand(sqlLoadStorageSectionIndicator);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next()){
				model.mdlStorageSectionIndicator mdlStorageSectionIndicator = new model.mdlStorageSectionIndicator();
				mdlStorageSectionIndicator.setPlantID(jrs.getString("PlantID"));
				mdlStorageSectionIndicator.setPlantName(jrs.getString("PlantName"));
				mdlStorageSectionIndicator.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageSectionIndicator.setWarehouseName(jrs.getString("WarehouseName"));
				mdlStorageSectionIndicator.setStorageSectionIndicatorID(jrs.getString("StorageSectionIndicatorID"));
				mdlStorageSectionIndicator.setStorageSectionIndicatorName(jrs.getString("StorageSectionIndicatorName"));

				listmdlStorageSectionIndicator.add(mdlStorageSectionIndicator);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageSectionIndicator", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageSectionIndicator", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlStorageSectionIndicator;
	}

	@SuppressWarnings("resource")
	public static String InsertStorageSectionIndicator(model.mdlStorageSectionIndicator lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlStorageSectionIndicator CheckStorageSectionIndicator = LoadStorageSectionIndicatorByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getStorageSectionIndicatorID());
			if(CheckStorageSectionIndicator.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, StorageSectionIndicatorID, StorageSectionIndicatorName FROM wms_storagesectionindicator LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("StorageSectionIndicatorID",lParam.getStorageSectionIndicatorID());
				jrs.updateString("StorageSectionIndicatorName",lParam.getStorageSectionIndicatorName());

				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Storage Section Indicator";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Storage section indicator sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertStorageSectionIndicator", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertStorageSectionIndicator", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String UpdateStorageSectionIndicator(model.mdlStorageSectionIndicator lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID, WarehouseID, StorageSectionIndicatorID, StorageSectionIndicatorName FROM wms_storagesectionindicator WHERE PlantID = ? "
							+ "AND WarehouseID = ? AND StorageSectionIndicatorID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getStorageSectionIndicatorID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("StorageSectionIndicatorName", lParam.getStorageSectionIndicatorName());
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Storage Section Indicator";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStorageSectionIndicator", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateStorageSectionIndicator", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeleteStorageSectionIndicator(String lPlantID, String lWarehouseID, String lStorageSectionIndicatorID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageSectionIndicatorID FROM wms_storagesectionindicator WHERE PlantID = ? "
							+ "AND WarehouseID = ? AND StorageSectionIndicatorID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lStorageSectionIndicatorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Storage Section Indicator";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteStorageSectionIndicator", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteStorageSectionIndicator", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
}
