package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import model.Globals;

/** Documentation
 * 002 nanda
 */

public class StockTransferAdapter {
	@SuppressWarnings("resource")
	public static model.mdlStockTransfer LoadStockTransferbyID(String lTransferID) {
		model.mdlStockTransfer mdlStockTransfer = new model.mdlStockTransfer();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT `TransferID`, `PlantID`, `From`, `To`, `Date` FROM `stock_transfer` WHERE `TransferID` = ?");
			jrs.setString(1,  lTransferID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next())
			{
			mdlStockTransfer.setTransferID(lTransferID);
			mdlStockTransfer.setPlantID(jrs.getString("PlantID"));
			mdlStockTransfer.setFrom(jrs.getString("From"));
			mdlStockTransfer.setTo(jrs.getString("To"));
			mdlStockTransfer.setDate(jrs.getString("Date"));
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStockTransferbyID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStockTransferbyID", "close opened connection protocol", Globals.user);
			}
		}
		
		return mdlStockTransfer;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlStockTransfer> LoadStockTransfer() {
		List<model.mdlStockTransfer> listmdlStockTransfer = new ArrayList<model.mdlStockTransfer>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `TransferID`, `PlantID`, `From`, `To`, `Date` "
					+ "FROM `stock_transfer` WHERE PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlStockTransfer mdlStockTransfer = new model.mdlStockTransfer();
				mdlStockTransfer.setTransferID(jrs.getString("TransferID"));
				mdlStockTransfer.setPlantID(jrs.getString("PlantID"));
				mdlStockTransfer.setFrom(jrs.getString("From"));
				mdlStockTransfer.setTo(jrs.getString("To"));
				
				String newDate = ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlStockTransfer.setDate(newDate);					
				
				listmdlStockTransfer.add(mdlStockTransfer);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStockTransfer", Globals.gCommand , "");
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStockTransfer", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlStockTransfer;
	}
	
	@SuppressWarnings("resource")
	public static String InsertStockTransfer(model.mdlStockTransfer lParam,String luser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();			
			
			jrs.setCommand("SELECT `TransferID`, `PlantID`, `From`, `To`, `Date`,`Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM `stock_transfer` LIMIT 1");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("TransferID",lParam.getTransferID());
			jrs.updateString("PlantID",lParam.getPlantID());
			jrs.updateString("From",lParam.getFrom());
			jrs.updateString("To",lParam.getTo());
			jrs.updateString("Date",lParam.getDate());
			jrs.updateString("Created_by",luser);
			jrs.updateString("Created_at",LocalDateTime.now().toString());
			jrs.updateString("Updated_by",luser);
			jrs.updateString("Updated_at",LocalDateTime.now().toString());
			
			
			jrs.insertRow();
			Globals.gReturn_Status = "Success Insert StockTransfer ";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertStockTransfer", Globals.gCommand , luser);
			//Globals.gReturn_Status = "Error Insert StockTransfer" + lParam.getTransferID();
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertStockTransfer", "close opened connection protocol", Globals.user);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	public static String UpdateStockTransfer(model.mdlStockTransfer lParam, String luser)
	{
		Globals.gCommand = "Update TransferID : " + lParam.getTransferID();
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `TransferID`,`PlantID`, `From`, `To`, `Date`,`Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM `stock_transfer` WHERE `TransferID` =? LIMIT 1");
			jrs.setString(1,  lParam.getTransferID());
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("PlantID", lParam.getPlantID());
			jrs.updateString("From", lParam.getFrom());
			jrs.updateString("To", lParam.getTo());
			jrs.updateString("Date", lParam.getDate());
			jrs.updateString("Updated_by",LocalDateTime.now().toString());
			jrs.updateString("Updated_at",luser);
			jrs.updateRow();
				
			jrs.close();
			Globals.gReturn_Status = "Success Update StockTransfer ";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStockTransfer", Globals.gCommand , luser);
			Globals.gReturn_Status = "Error Update StockTransfer";
		}
		return Globals.gReturn_Status;
	}
	
	public static model.mdlStockTransfer GetWarehouseFromTo(String lTransferID, String lPlantID){
		model.mdlStockTransfer mdlStockTransfer = new model.mdlStockTransfer();
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT `From` , `To` FROM `stock_transfer` WHERE `TransferID` = ? AND `PlantID` =?");
			jrs.setString(1,  lTransferID);
			jrs.setString(2,  lPlantID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next())
			{
			mdlStockTransfer.setTransferID(lTransferID);
			mdlStockTransfer.setFrom(jrs.getString("From"));
			mdlStockTransfer.setTo(jrs.getString("To"));
			}
			
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetWarehouseFromTo", Globals.gCommand , Globals.user);
		}
		
		return mdlStockTransfer;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlStockTransfer> LoadNonCancelStockTransfer() {
		List<model.mdlStockTransfer> listmdlStockTransfer = new ArrayList<model.mdlStockTransfer>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `TransferID`, `PlantID`, `From`, `To`, `Date` FROM `stock_transfer` "
					+ "WHERE `IsCancel`=0 AND PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlStockTransfer mdlStockTransfer = new model.mdlStockTransfer();
				mdlStockTransfer.setTransferID(jrs.getString("TransferID"));
				mdlStockTransfer.setPlantID(jrs.getString("PlantID"));
				mdlStockTransfer.setFrom(jrs.getString("From"));
				mdlStockTransfer.setTo(jrs.getString("To"));
				
				String newDate = ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlStockTransfer.setDate(newDate);					
				
				listmdlStockTransfer.add(mdlStockTransfer);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadNonCancelStockTransfer", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadNonCancelStockTransfer", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlStockTransfer;
	}

	public static String GenerateDocNoStockTransfer()
	{
		String generateDate = LocalDateTime.now().toString();
		String lYear = generateDate.substring(0, 4);
		String txtDocNo =  "";
		model.mdlTraceCode lGetTraceCode = TraceCodeAdapter.LoadTraceCodeDocNo(lYear,"StockTransfer");
	
		if(lGetTraceCode.TraceCode == null)
		{
			txtDocNo = "M"+ generateDate.substring(2, 4) + "0000001";
		}
		else
		{
			int lRunningNumber = Integer.parseInt(lGetTraceCode.TraceCode.substring(3,10));
			String lnewRunningNumber = String.format("%07d", lRunningNumber + 1);	
			txtDocNo = "M"+ generateDate.substring(2, 4)+ lnewRunningNumber;
		}
	
		return txtDocNo;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlRptStockTransfer> LoadReportStockTransfer(String lStartDate, String lEndDate) {
		List<model.mdlRptStockTransfer> mdlRptStockTransferlist = new ArrayList<model.mdlRptStockTransfer>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
							
			jrs.setCommand("SELECT a.`TransferID`, a.`PlantID`, a.`From`, a.`To`, a.`Date`, b.TransferLine, "
					+ "b.ProductID, b.Qty_UOM, b.UOM, b.Qty_BaseUOM, b.BaseUOM, b.Batch_No, b.Packing_No "
					+ "FROM  stock_transfer a  INNER JOIN  stock_transfer_detail b ON a.TransferID = b.TransferID "
					+ "WHERE (DATE(a.Date) BETWEEN ? AND DATE_ADD(?,INTERVAL 1 DAY)) AND a.PlantID IN ("+Globals.user_area+")");
			jrs.setString(1, lStartDate);
			jrs.setString(2, lEndDate);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
													
			while(jrs.next()){
				model.mdlRptStockTransfer mdlRptStockTransfer = new model.mdlRptStockTransfer();
				mdlRptStockTransfer.setTransID(jrs.getString("TransferID"));
				mdlRptStockTransfer.setPlantID(jrs.getString("PlantID"));
				mdlRptStockTransfer.setFrom(jrs.getString("From"));	
				mdlRptStockTransfer.setTo(jrs.getString("To"));
				mdlRptStockTransfer.setDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlRptStockTransfer.setDocLine(jrs.getString("TransferLine"));
				mdlRptStockTransfer.setProductID(jrs.getString("ProductID"));
				mdlRptStockTransfer.setQtyUOM(jrs.getString("Qty_UOM"));
				mdlRptStockTransfer.setUOM(jrs.getString("UOM"));	
				mdlRptStockTransfer.setQtyBaseUOM(jrs.getString("Qty_BaseUOM"));
				mdlRptStockTransfer.setBaseUOM(jrs.getString("BaseUOM"));
				mdlRptStockTransfer.setBatchNo(jrs.getString("Batch_No"));
				mdlRptStockTransfer.setPackingNo(jrs.getString("Packing_No"));
								
				mdlRptStockTransferlist.add(mdlRptStockTransfer);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadReportStockTransfer", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadReportStockTransfer", "close opened connection protocol", Globals.user);
			}
		}

		return mdlRptStockTransferlist;
	}

}
