package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import database.RowSetAdapter;
import model.Globals;
import model.mdlLogin;
import adapter.Base64Adapter;

/** Documentation
 * 
 */
public class LoginAdapter {
	
	@SuppressWarnings("resource")
	public static boolean ValidasiLogin(String lUserId, String lPassword)
	{
		boolean lCheck = false;
		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try {
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT UserId,Password FROM user_login WHERE UserId = ? AND Password = ? LIMIT 1");		
			jrs.setString(1, lUserId);
			jrs.setString(2, lPasswordEnc);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
		    lCheck = jrs.next(); 
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "ValidasiLogin", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "ValidasiLogin", "close opened connection protocol", Globals.user);
			}
		}
		
		return lCheck;
	}
	
	public static String GetPassword(String lUserId)
	{
		String lPassword = "";
		String lPasswordEnc = "";
		try {			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Password FROM user_login WHERE UserId = ? LIMIT 1");		
			jrs.setString(1, lUserId);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			while(jrs.next()){
				lPasswordEnc = jrs.getString("Password");
			}
			
			lPassword = Base64Adapter.DecriptBase64(lPasswordEnc);
		    jrs.close();   
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetPassword", Globals.gCommand , Globals.user);
		}	
		
		return lPassword;
		
	}

}
