package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

/** Documentation
 * 
 */
public class StockTransferDetailAdapter {
	public static model.mdlStockTransferDetail LoadStockTransferDetailbyID(String lTransferID, String lTransferLine) {
		model.mdlStockTransferDetail mdlStockTransferDetail = new model.mdlStockTransferDetail();
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No` FROM `stock_transfer_detail` WHERE `TransferID`=? AND `TransferLine`=?");
			jrs.setString(1,  lTransferID);
			jrs.setString(2,  lTransferLine);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			mdlStockTransferDetail.setTransferID(lTransferID);
			mdlStockTransferDetail.setTransferLine(lTransferLine);
			mdlStockTransferDetail.setProductID(jrs.getString("ProductID"));
			mdlStockTransferDetail.setQty_UOM(jrs.getString("Qty_UOM"));
			mdlStockTransferDetail.setUOM(jrs.getString("UOM"));
			mdlStockTransferDetail.setQty_BaseUOM(jrs.getString("Qty_BaseUOM"));
			mdlStockTransferDetail.setBaseUOM(jrs.getString("BaseUOM"));
			mdlStockTransferDetail.setBatch_No(jrs.getString("Batch_No"));
			mdlStockTransferDetail.setPacking_No(jrs.getString("Packing_No"));
			
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStockTransferbyID", Globals.gCommand , "");
		}

		return mdlStockTransferDetail;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlStockTransferDetail> LoadStockTransferDetail(String lTransferID) {
		List<model.mdlStockTransferDetail> listmdlStockTransferDetail = new ArrayList<model.mdlStockTransferDetail>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `TransferID`, `TransferLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No` FROM `stock_transfer_detail` WHERE `TransferID`=? ORDER BY `TransferLine`");
			jrs.setString(1,  lTransferID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlStockTransferDetail mdlStockTransferDetail = new model.mdlStockTransferDetail();
				mdlStockTransferDetail.setTransferID(jrs.getString("TransferID"));
				mdlStockTransferDetail.setTransferLine(jrs.getString("TransferLine"));
				mdlStockTransferDetail.setProductID(jrs.getString("ProductID"));
				mdlStockTransferDetail.setQty_UOM(jrs.getString("Qty_UOM"));
				mdlStockTransferDetail.setUOM(jrs.getString("UOM"));
				mdlStockTransferDetail.setQty_BaseUOM(jrs.getString("Qty_BaseUOM"));
				mdlStockTransferDetail.setBaseUOM(jrs.getString("BaseUOM"));
				mdlStockTransferDetail.setBatch_No(jrs.getString("Batch_No"));
				mdlStockTransferDetail.setPacking_No(jrs.getString("Packing_No"));					
				
				listmdlStockTransferDetail.add(mdlStockTransferDetail);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStockTransferDetail", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStockTransferDetail", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlStockTransferDetail;
	}
	
	public static String InsertTransactionStockTransfer(model.mdlStockTransferDetail lmdlStockTransferDetail, model.mdlStockSummary lmdlStockSummaryFrom, model.mdlStockSummary lmdlStockSummaryTo, String luser)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmOutboundDetail = null;
		PreparedStatement pstmStockSummaryFrom = null;
		PreparedStatement pstmStockSummaryTo = null;
			
		try{
			connection = database.RowSetAdapter.getConnection();
			
			connection.setAutoCommit(false);
			
			String sqlOutboundDetail = "INSERT INTO `stock_transfer_detail`(`TransferID`, `TransferLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
			String sqlStockSummary = "INSERT INTO stock_summary(`Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
			//String sqlStockSummary = "UPDATE `stock_summary` SET `Qty`=? WHERE `Period`=? AND `ProductID`=? AND `PlantID`=? AND `WarehouseID`=? AND `Batch_No`=?;";
			pstmOutboundDetail = connection.prepareStatement(sqlOutboundDetail);
			pstmStockSummaryFrom = connection.prepareStatement(sqlStockSummary);
			pstmStockSummaryTo = connection.prepareStatement(sqlStockSummary);
			
			//parameter outbound_detail
			pstmOutboundDetail.setString(1,  ValidateNull.NulltoStringEmpty(lmdlStockTransferDetail.getTransferID()));
			pstmOutboundDetail.setString(2,  ValidateNull.NulltoStringEmpty(lmdlStockTransferDetail.getTransferLine()));
			pstmOutboundDetail.setString(3,  ValidateNull.NulltoStringEmpty(lmdlStockTransferDetail.getProductID()));
			pstmOutboundDetail.setString(4,  ValidateNull.NulltoStringEmpty(lmdlStockTransferDetail.getQty_UOM()));
			pstmOutboundDetail.setString(5,  ValidateNull.NulltoStringEmpty(lmdlStockTransferDetail.getUOM()));
			pstmOutboundDetail.setString(6,  ValidateNull.NulltoStringEmpty(lmdlStockTransferDetail.getQty_BaseUOM()));
			pstmOutboundDetail.setString(7,  ValidateNull.NulltoStringEmpty(lmdlStockTransferDetail.getBaseUOM()));
			pstmOutboundDetail.setString(8,  ValidateNull.NulltoStringEmpty(lmdlStockTransferDetail.getBatch_No()));
			pstmOutboundDetail.setString(9,  ValidateNull.NulltoStringEmpty(lmdlStockTransferDetail.getPacking_No()));
			pstmOutboundDetail.setString(10,  luser);
			pstmOutboundDetail.setString(11,  LocalDateTime.now().toString());
			pstmOutboundDetail.setString(12,  luser);
			pstmOutboundDetail.setString(13,  LocalDateTime.now().toString());
			Globals.gCommand = pstmOutboundDetail.toString();
			pstmOutboundDetail.executeUpdate();
				
			//parameter stock_summary From
			pstmStockSummaryFrom.setString(1, ValidateNull.NulltoStringEmpty(lmdlStockSummaryFrom.getPeriod()));
			pstmStockSummaryFrom.setString(2, ValidateNull.NulltoStringEmpty(lmdlStockSummaryFrom.getProductID()));
			pstmStockSummaryFrom.setString(3, ValidateNull.NulltoStringEmpty(lmdlStockSummaryFrom.getPlantID()));
			pstmStockSummaryFrom.setString(4, ValidateNull.NulltoStringEmpty(lmdlStockSummaryFrom.getWarehouseID()));
			pstmStockSummaryFrom.setString(5, ValidateNull.NulltoStringEmpty(lmdlStockSummaryFrom.getBatch_No()));
			pstmStockSummaryFrom.setString(6, ValidateNull.NulltoStringEmpty(lmdlStockSummaryFrom.getQty()));
			pstmStockSummaryFrom.setString(7, ValidateNull.NulltoStringEmpty(lmdlStockSummaryFrom.getUOM()));
			pstmStockSummaryFrom.setString(8, ValidateNull.NulltoStringEmpty(luser));
			pstmStockSummaryFrom.setString(9, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmStockSummaryFrom.setString(10, ValidateNull.NulltoStringEmpty(luser));
			pstmStockSummaryFrom.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmStockSummaryFrom.setString(12, ValidateNull.NulltoStringEmpty(lmdlStockSummaryFrom.getQty()));
			pstmStockSummaryFrom.setString(13, ValidateNull.NulltoStringEmpty(lmdlStockSummaryFrom.getUOM()));
			pstmStockSummaryFrom.setString(14, ValidateNull.NulltoStringEmpty(luser));
			pstmStockSummaryFrom.setString(15, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			Globals.gCommand = pstmStockSummaryFrom.toString();
			pstmStockSummaryFrom.executeUpdate();
			
			//parameter stock_summary To
			pstmStockSummaryTo.setString(1, ValidateNull.NulltoStringEmpty(lmdlStockSummaryTo.getPeriod()));
			pstmStockSummaryTo.setString(2, ValidateNull.NulltoStringEmpty(lmdlStockSummaryTo.getProductID()));
			pstmStockSummaryTo.setString(3, ValidateNull.NulltoStringEmpty(lmdlStockSummaryTo.getPlantID()));
			pstmStockSummaryTo.setString(4, ValidateNull.NulltoStringEmpty(lmdlStockSummaryTo.getWarehouseID()));
			pstmStockSummaryTo.setString(5, ValidateNull.NulltoStringEmpty(lmdlStockSummaryTo.getBatch_No()));
			pstmStockSummaryTo.setString(6, ValidateNull.NulltoStringEmpty(lmdlStockSummaryTo.getQty()));
			pstmStockSummaryTo.setString(7, ValidateNull.NulltoStringEmpty(lmdlStockSummaryTo.getUOM()));
			pstmStockSummaryTo.setString(8, ValidateNull.NulltoStringEmpty(luser));
			pstmStockSummaryTo.setString(9, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmStockSummaryTo.setString(10, ValidateNull.NulltoStringEmpty(luser));
			pstmStockSummaryTo.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmStockSummaryTo.setString(12, ValidateNull.NulltoStringEmpty(lmdlStockSummaryTo.getQty()));
			pstmStockSummaryTo.setString(13, ValidateNull.NulltoStringEmpty(lmdlStockSummaryTo.getUOM()));
			pstmStockSummaryTo.setString(14, ValidateNull.NulltoStringEmpty(luser));
			pstmStockSummaryTo.setString(15, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			Globals.gCommand = pstmStockSummaryTo.toString();
			pstmStockSummaryTo.executeUpdate();
			
			connection.commit();
			
			Globals.gReturn_Status = "Success Insert Stock Transfer Detail";
		}catch (Exception e ) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "TransactionStockTransfer", Globals.gCommand , luser);
	    			Globals.gReturn_Status = "Database Error";
	            }
	            
	            LogAdapter.InsertLogExc(e.toString(), "InsertStockTransferDetail", Globals.gCommand , luser);
	            Globals.gReturn_Status = "Database Error";
	        }
		}finally {
			try{
		        if (pstmOutboundDetail != null) {
		        	pstmOutboundDetail.close();
		        }
		        if (pstmStockSummaryFrom != null) {
		        	pstmStockSummaryFrom.close();
		        }
		        if (pstmStockSummaryTo != null) {
		        	pstmStockSummaryTo.close();
		        }
		        connection.setAutoCommit(true);
		        if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertStockTransferDetail", "close opened connection protocol", luser);
			}
	    }
		
		return Globals.gReturn_Status;
	}

	 //<<001 Nanda
	@SuppressWarnings("resource")
	public static String GenerateTransLineStockTransDet(String lTransferID)
	{
	String txtDocLine ="";
	JdbcRowSet jrs = new JdbcRowSetImpl();
	Globals.gCommand = "";
	try{
		Class.forName("com.mysql.jdbc.Driver");
		
		jrs = database.RowSetAdapter.getJDBCRowSet();
	
		jrs.setCommand("SELECT `TransferLine` FROM `stock_transfer_detail` WHERE `TransferID` = ? ORDER BY  `Created_at` DESC LIMIT 1");
		jrs.setString(1, lTransferID);
		Globals.gCommand = jrs.getCommand();
		jrs.execute();
		
		while(jrs.next()){
			txtDocLine = ValidateNull.NulltoStringEmpty(jrs.getString("TransferLine"));
		}
	}
	catch(Exception ex){
		LogAdapter.InsertLogExc(ex.toString(), "GenerateTransLineStockTransDet", Globals.gCommand , Globals.user);
	}
	finally{
		try{
			if(jrs!=null)
				jrs.close();
		}
		catch(Exception e){
			LogAdapter.InsertLogExc(e.toString(), "GenerateTransLineStockTransDet", "close opened connection protocol", Globals.user);
		}
	}

	return txtDocLine;
	}
	//>>
}
