package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import model.Globals;
import model.mdlBOMProductionOrderDetail;
import model.mdlTempComponentBatchSummary;

public class ProductionOrderAdapter {

	public static String TransactionInsertTempProductionDetail(List<model.mdlBOMProductionOrderDetail> lParamlist)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmInsertTempProductionDetail = null;
		
		try{
			connection = database.RowSetAdapter.getConnection();
			
			connection.setAutoCommit(false);
			
			String sqlInsertTempProductionDetail = "INSERT INTO tempbom_production_order_detail(`OrderTypeID`, `OrderNo`, `ComponentLine`, `ComponentID`, `Qty`, `UOM`, `QtyBaseUOM`, `BaseUOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
			pstmInsertTempProductionDetail = connection.prepareStatement(sqlInsertTempProductionDetail);

			for(mdlBOMProductionOrderDetail lParam : lParamlist)
			{
				//parameter confirmation detail
				pstmInsertTempProductionDetail.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getOrderTypeID()));
				pstmInsertTempProductionDetail.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getOrderNo()));
				pstmInsertTempProductionDetail.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getComponentLine()));
				pstmInsertTempProductionDetail.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getComponentID()));
				pstmInsertTempProductionDetail.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getQty()));
				pstmInsertTempProductionDetail.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getUOM()));
				pstmInsertTempProductionDetail.setString(7,  ValidateNull.NulltoStringEmpty(lParam.getQtyBaseUOM()));
				pstmInsertTempProductionDetail.setString(8,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				
				pstmInsertTempProductionDetail.setString(9,  Globals.user);
				pstmInsertTempProductionDetail.setString(10,  LocalDateTime.now().toString());
				pstmInsertTempProductionDetail.setString(11,  Globals.user);
				pstmInsertTempProductionDetail.setString(12,  LocalDateTime.now().toString());
				Globals.gCommand = pstmInsertTempProductionDetail.toString();
				pstmInsertTempProductionDetail.executeUpdate();
			}
			
			connection.commit();
			Globals.gReturn_Status = "Success Insert Temp Production Detail";
		}catch (Exception e ) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "TransactionInsertTempProductionDetail", "InsertTempProductionDetail" , Globals.user);
	    			Globals.gReturn_Status = "Database Error";
	            }
	            LogAdapter.InsertLogExc(e.toString(), "TransactionInsertTempProductionDetail", "InsertTempProductionDetail" , Globals.user);
    			Globals.gReturn_Status = "Database Error";
	        }
		}finally {
			try{
		        if (pstmInsertTempProductionDetail != null) {
		        	pstmInsertTempProductionDetail.close();
		        }
		        connection.setAutoCommit(true);
		        if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "TransactionInsertTempProductionDetail", "close opened connection protocol", Globals.user);
			}
	    }
		
		
		return Globals.gReturn_Status;
	}

	public static List<model.mdlBOMProductionOrderDetail> LoadTempProductionOrderDetail(String lordertypeid, String lorderno) {
		List<model.mdlBOMProductionOrderDetail> listTempProductionOrderDetail = new ArrayList<model.mdlBOMProductionOrderDetail>();
		
		Connection connection = null;
		PreparedStatement pstmLoadTempProductionOrderDetail = null;
		ResultSet jrs2 = null;
		String lCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadTempProductionOrderDetail = "SELECT a.OrderTypeID, a.OrderNo,a.ComponentLine,a.ComponentID, "
					+ "a.Qty,a.UOM,a.QtyBaseUOM,a.BaseUOM "
					+ "FROM tempbom_production_order_detail a "
					+ "WHERE a.OrderTypeID=? AND a.OrderNo=? "
					+ "ORDER BY a.ComponentLine";
			
			pstmLoadTempProductionOrderDetail = connection.prepareStatement(sqlLoadTempProductionOrderDetail);
			
			pstmLoadTempProductionOrderDetail.setString(1, lordertypeid);
			pstmLoadTempProductionOrderDetail.setString(2, lorderno);
			pstmLoadTempProductionOrderDetail.addBatch();
			lCommand = pstmLoadTempProductionOrderDetail.toString();
			
			jrs2 = pstmLoadTempProductionOrderDetail.executeQuery();
			//data proccess
			while(jrs2.next()){
				model.mdlBOMProductionOrderDetail TempProductionOrderDetail = new model.mdlBOMProductionOrderDetail();
				
				//define model
				TempProductionOrderDetail.setOrderTypeID(jrs2.getString("OrderTypeID"));
				TempProductionOrderDetail.setOrderNo(jrs2.getString("OrderNo"));
				TempProductionOrderDetail.setComponentLine(jrs2.getString("ComponentLine"));
				TempProductionOrderDetail.setComponentID(jrs2.getString("ComponentID"));
				TempProductionOrderDetail.setQty(jrs2.getString("Qty"));
				TempProductionOrderDetail.setUOM(jrs2.getString("UOM"));
				TempProductionOrderDetail.setQtyBaseUOM(jrs2.getString("QtyBaseUOM"));
				TempProductionOrderDetail.setBaseUOM(jrs2.getString("BaseUOM"));
				
				listTempProductionOrderDetail.add(TempProductionOrderDetail);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadTempProductionOrderDetail", lCommand , Globals.user);
		}
		finally {
			try{
				 if (pstmLoadTempProductionOrderDetail != null) {
					 pstmLoadTempProductionOrderDetail.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs2 != null) {
					 jrs2.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadTempProductionOrderDetail", "close opened connection protocol", Globals.user);
			}
		 }

		return listTempProductionOrderDetail;
	}

	public static List<model.mdlTempComponentBatchSummary> LoadTempComponentBatchSummary(String lComponentLine, String lComponentID,String lPlantID,String lWarehouseID, String lOrderTypeID, String lOrderNo) {
		List<model.mdlTempComponentBatchSummary> listTempComponentBatchSummary = new ArrayList<model.mdlTempComponentBatchSummary>();
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT  a.ProductID, a.Batch_No, c.GRDate, c.Expired_Date, a.PlantID, "
					+ "a.WarehouseID, a.Qty, a.UOM, d.Batch_No as TempBatch " 
					+ "FROM stock_summary a "
					+ "INNER JOIN "
					+ "( "
					+ "SELECT  ProductID,Batch_No,PlantID,WarehouseID,MAX(Updated_at) max_date "
					+ "FROM stock_summary "
					+ "WHERE PlantID=? AND WarehouseID=? AND ProductID=? "
					+ "GROUP BY ProductID,Batch_No "
					+ ") b "
					+ "ON a.ProductID = b.ProductID AND a.Batch_No = b.Batch_No AND "
					+ "a.Updated_at = b.max_date AND "
					+ "a.PlantID = b.PlantID AND a.WarehouseID = b.WarehouseID "
					+ "INNER JOIN batch c ON c.Batch_No=a.Batch_No "
					+ "LEFT JOIN tempbatch_summary d ON d.Batch_No=a.Batch_No AND d.ComponentLine=? AND d.OrderTypeID=? AND d.OrderNo=? "
					+ "WHERE a.Qty <> 0 AND a.PlantID IN ("+Globals.user_area+") "
					+ "ORDER BY c.Expired_Date";
			
			pstm = connection.prepareStatement(sql);
			
			pstm.setString(1, lPlantID);
			pstm.setString(2, lWarehouseID);
			pstm.setString(3, lComponentID);
			pstm.setString(4, lComponentLine);
			pstm.setString(5, lOrderTypeID);
			pstm.setString(6, lOrderNo);
			pstm.addBatch();
			
			Globals.gCommand = pstm.toString();
			
			jrs = pstm.executeQuery();
									
			while(jrs.next()){
				model.mdlTempComponentBatchSummary TempComponentBatchSummary = new model.mdlTempComponentBatchSummary();
				
				TempComponentBatchSummary.setComponentID(jrs.getString("ProductID"));
				TempComponentBatchSummary.setBatchNo(jrs.getString("Batch_No"));
				
				String newGRDate=helper.ConvertDateTimeHelper.formatDate(jrs.getString("GRDate"), "yyyy-MM-dd", "dd MMM yyyy");
				TempComponentBatchSummary.setGRDate(newGRDate);
				
				String newExpiredDate=helper.ConvertDateTimeHelper.formatDate(jrs.getString("Expired_Date"), "yyyy-MM-dd", "dd MMM yyyy");
				TempComponentBatchSummary.setExpiredDate(newExpiredDate);
				
				TempComponentBatchSummary.setPlantID(jrs.getString("PlantID"));
				TempComponentBatchSummary.setWarehouseID(jrs.getString("WarehouseID"));
				TempComponentBatchSummary.setQty(jrs.getInt("Qty"));
				TempComponentBatchSummary.setUOM(jrs.getString("UOM"));
				
				if(!(jrs.getString("TempBatch")==null))
					TempComponentBatchSummary.setStatus("checked");
				else
					TempComponentBatchSummary.setStatus("");
				
				listTempComponentBatchSummary.add(TempComponentBatchSummary);
			}			
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadTempComponentBatchSummary", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadTempComponentBatchSummary", "close opened connection protocol", Globals.user);
			}
		 }

		return listTempComponentBatchSummary;
	}

	public static String TransactionInsertTempBatchSummary(List<model.mdlTempComponentBatchSummary> lParamlist)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmInsertTempBatchSummary = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			connection.setAutoCommit(false);
			
			String sqlInsertTempBatchSummary = "INSERT INTO tempbatch_summary(`OrderTypeID`, `OrderNo`, `ComponentLine`, `ComponentID`, `Batch_No`, `GRDate`, `Expired_Date` , `PlantID`, `WarehouseID`, `Qty`, `UOM` , `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
			pstmInsertTempBatchSummary = connection.prepareStatement(sqlInsertTempBatchSummary);

			for(mdlTempComponentBatchSummary lParam : lParamlist)
			{
				//parameter temporary component batch summary
				pstmInsertTempBatchSummary.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getOrderTypeID()));
				pstmInsertTempBatchSummary.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getOrderNo()));
				pstmInsertTempBatchSummary.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getComponentLine()));
				pstmInsertTempBatchSummary.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getComponentID()));
				pstmInsertTempBatchSummary.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getBatchNo()));
				pstmInsertTempBatchSummary.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getGRDate()));
				pstmInsertTempBatchSummary.setString(7,  ValidateNull.NulltoStringEmpty(lParam.getExpiredDate()));
				pstmInsertTempBatchSummary.setString(8,  ValidateNull.NulltoStringEmpty(lParam.getPlantID()));
				pstmInsertTempBatchSummary.setString(9,  ValidateNull.NulltoStringEmpty(lParam.getWarehouseID()));
				pstmInsertTempBatchSummary.setInt(10,  lParam.getQty());
				pstmInsertTempBatchSummary.setString(11,  lParam.getUOM());
				pstmInsertTempBatchSummary.setString(12,  Globals.user);
				pstmInsertTempBatchSummary.setString(13,  LocalDateTime.now().toString());
				pstmInsertTempBatchSummary.setString(14,  Globals.user);
				pstmInsertTempBatchSummary.setString(15,  LocalDateTime.now().toString());
				Globals.gCommand = pstmInsertTempBatchSummary.toString();
				pstmInsertTempBatchSummary.executeUpdate();
			}
			
			connection.commit();
			Globals.gReturn_Status = "Success Insert Temporary Component Batch Summary";
		}catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "TransactionInsertTempBatchSummary", Globals.gCommand , Globals.user);
	    			Globals.gReturn_Status = "Database Error";
	            }
	            LogAdapter.InsertLogExc(e.toString(), "TransactionInsertTempBatchSummary", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
	        }
		}finally {
			try{
		        if (pstmInsertTempBatchSummary != null) {
		        	pstmInsertTempBatchSummary.close();
		        }
		        connection.setAutoCommit(true);
		        if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "TransactionInsertTempBatchSummary", "close opened connection protocol", Globals.user);
			}
	    }
		
		
		return Globals.gReturn_Status;
	}

	public static boolean CheckTemporaryBatchData(String lordertypeid, String lorderno, String lcomponentline, String lcomponentid)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		boolean check = false;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT Batch_No "
					+ "FROM tempbatch_summary "
					+ "WHERE OrderTypeID=? AND OrderNo=? AND ComponentLine=? AND ComponentID=?";
			
			pstm = connection.prepareStatement(sql);
			
			pstm.setString(1, lordertypeid);
			pstm.setString(2, lorderno);
			pstm.setString(3, lcomponentline);
			pstm.setString(4, lcomponentid);
			pstm.addBatch();
			Globals.gCommand = pstm.toString();
			
			jrs = pstm.executeQuery();
						
			if(jrs.next())
			{
				check = true;
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CheckTemporaryBatchData", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "CheckTemporaryBatchData", "close opened connection protocol", Globals.user);
			}
		 }

		return check;
	}

	public static void DeleteTempBatchSummary(String lordertypeid, String lorderno, String lcomponentline, String lcomponentid)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "DELETE FROM tempbatch_summary "
					+ "WHERE OrderTypeID=? AND OrderNo=? AND ComponentLine=? AND ComponentID=?";
			pstm = connection.prepareStatement(sql);
			
			pstm.setString(1, lordertypeid);
			pstm.setString(2, lorderno);
			pstm.setString(3, lcomponentline);
			pstm.setString(4, lcomponentid);
			
			pstm.addBatch();
			Globals.gCommand = pstm.toString();
			pstm.executeBatch();
			
			//Globals.gReturn_Status = "Success Delete UOM";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteTempBatch", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Database Error";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteTempBatch", "close opened connection protocol", Globals.user);
			}
		 }
		
		//return Globals.gReturn_Status;
		return;
	}

	public static void DeleteAllTempBatchSummary(String lordertypeid, String lorderno)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "DELETE FROM tempbatch_summary "
					+ "WHERE OrderTypeID=? AND OrderNo=?";
			pstm = connection.prepareStatement(sql);
			
			pstm.setString(1, lordertypeid);
			pstm.setString(2, lorderno);
			
			pstm.addBatch();
			Globals.gCommand = pstm.toString();
			pstm.executeBatch();
			
			//Globals.gReturn_Status = "Success Delete UOM";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteAllTempBatch", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Database Error";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteAllTempBatch", "close opened connection protocol", Globals.user);
			}
		 }
		
		//return Globals.gReturn_Status;
		return;
	}
	
	public static void DeleteTempBatchSummaryByUpdatedComponent(String lordertypeid, String lorderno, String lcomponentline, String lcomponentid)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "DELETE FROM tempbatch_summary "
					+ "WHERE OrderTypeID=? AND OrderNo=? AND ComponentLine=? AND ComponentID=?";
			pstm = connection.prepareStatement(sql);
			
			pstm.setString(1, lordertypeid);
			pstm.setString(2, lorderno);
			pstm.setString(3, lcomponentline);
			pstm.setString(4, lcomponentid);
			
			pstm.addBatch();
			Globals.gCommand = pstm.toString();
			pstm.executeBatch();
			
			//Globals.gReturn_Status = "Success Delete UOM";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteTempBatchSummaryByUpdatedComponent", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Database Error";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteTempBatchSummaryByUpdatedComponent", "close opened connection protocol", Globals.user);
			}
		 }
		
		//return Globals.gReturn_Status;
		return;
	}
	
	public static void DeleteTempProductionOrderDetail(String lordertypeid, String lorderno)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "DELETE FROM tempbom_production_order_detail "
					+ "WHERE OrderTypeID=? AND OrderNo=?";
			pstm = connection.prepareStatement(sql);
			
			pstm.setString(1, lordertypeid);
			pstm.setString(2, lorderno);
			
			pstm.addBatch();
			Globals.gCommand = pstm.toString();
			pstm.executeBatch();
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteTempProductionOrderDetail", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteTempProductionOrderDetail", "close opened connection protocol", Globals.user);
			}
		 }
		
		return;
	}
	
	public static List<model.mdlTempComponentBatchSummary> LoadProductionComponentBatchSummary(mdlBOMProductionOrderDetail lParam) {
		List<model.mdlTempComponentBatchSummary> listComponentBatchSummary = new ArrayList<model.mdlTempComponentBatchSummary>();
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT OrderTypeID, OrderNo, ComponentLine, ComponentID, Batch_No, GRDate, Expired_Date, "
					+ "PlantID, WarehouseID, Qty, UOM "
					+ "FROM tempbatch_summary "
					+ "WHERE OrderTypeID=? AND OrderNo=? AND ComponentLine=? AND ComponentID=?";
			
			pstm = connection.prepareStatement(sql);
			
			pstm.setString(1, lParam.getOrderTypeID());
			pstm.setString(2, lParam.getOrderNo());
			pstm.setString(3, lParam.getComponentLine());
			pstm.setString(4, lParam.getComponentID());
			pstm.addBatch();
			Globals.gCommand = pstm.toString();
			
			jrs = pstm.executeQuery();
									
			while(jrs.next()){
				model.mdlTempComponentBatchSummary ComponentBatchSummary = new model.mdlTempComponentBatchSummary();
				
				ComponentBatchSummary.setOrderTypeID(jrs.getString("OrderTypeID"));
				ComponentBatchSummary.setOrderNo(jrs.getString("OrderNo"));
				ComponentBatchSummary.setComponentLine(jrs.getString("ComponentLine"));
				ComponentBatchSummary.setComponentID(jrs.getString("ComponentID"));
				ComponentBatchSummary.setBatchNo(jrs.getString("Batch_No"));
				ComponentBatchSummary.setGRDate(jrs.getString("GRDate"));
				ComponentBatchSummary.setExpiredDate(jrs.getString("Expired_Date"));
				ComponentBatchSummary.setPlantID(jrs.getString("PlantID"));
				ComponentBatchSummary.setWarehouseID(jrs.getString("WarehouseID"));
				ComponentBatchSummary.setQty(jrs.getInt("Qty"));
				ComponentBatchSummary.setUOM(jrs.getString("UOM"));
				
				listComponentBatchSummary.add(ComponentBatchSummary);
			}			
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProductionComponentBatchSummary", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadProductionComponentBatchSummary", "close opened connection protocol", Globals.user);
			}
		 }

		return listComponentBatchSummary;
	}

	public static String UpdateTempProductionOrderDetail(mdlBOMProductionOrderDetail lParam)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "UPDATE tempbom_production_order_detail "
					+ "SET Qty=?,UOM=?,QtyBaseUOM=?,BaseUOM=? "
					+ "WHERE OrderTypeID=? AND OrderNo=? AND ComponentLine=?";
			pstm = connection.prepareStatement(sql);
			
			pstm.setString(1, lParam.getQty());
			pstm.setString(2, lParam.getUOM());
			pstm.setString(3, lParam.getQtyBaseUOM());
			pstm.setString(4, lParam.getBaseUOM());
			pstm.setString(5, lParam.getOrderTypeID());
			pstm.setString(6, lParam.getOrderNo());
			pstm.setString(7, lParam.getComponentLine());
			
			pstm.addBatch();
			Globals.gCommand = pstm.toString();
			pstm.executeBatch();
			
			Globals.gReturn_Status = "Success Update Confirmation Detail";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateTempProductionOrderDetail", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateTempProductionOrderDetail", "close opened connection protocol", Globals.user);
			}
		 }
		
		return Globals.gReturn_Status;
	}
	
}
