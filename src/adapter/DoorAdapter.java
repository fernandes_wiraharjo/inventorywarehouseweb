package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlDoor;

public class DoorAdapter {
	@SuppressWarnings("resource")
	public static List<model.mdlDoor> LoadDoorByPlantID(String lPlantID) {
		List<model.mdlDoor> listmdlDoor = new ArrayList<model.mdlDoor>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, DoorID, DoorName, GI_Indicator, GR_Indicator, CD_Indicator FROM wms_door WHERE PlantID = ?");
			jrs.setString(1,  lPlantID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
			model.mdlDoor mdlDoor = new model.mdlDoor();

			mdlDoor.setPlantID(jrs.getString("PlantID"));
			mdlDoor.setWarehouseID(jrs.getString("WarehouseID"));
			mdlDoor.setDoorID(jrs.getString("DoorID"));
			mdlDoor.setDoorName(jrs.getString("DoorName"));
			mdlDoor.setGI_Indicator(jrs.getBoolean("GI_Indicator"));
			mdlDoor.setGR_Indicator(jrs.getBoolean("GR_Indicator"));
			mdlDoor.setCD_Indicator(jrs.getBoolean("CD_Indicator"));
			listmdlDoor.add(mdlDoor);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDoorByPlantID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDoorByPlantID", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlDoor;
	}

	@SuppressWarnings("resource")
	public static model.mdlDoor LoadDoorByKey(String lPlantID, String lWarehouseID, String lDoorID) {
		model.mdlDoor mdlDoor = new model.mdlDoor();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, DoorID, DoorName, GI_Indicator, GR_Indicator, CD_Indicator FROM wms_door "
							+ "WHERE PlantID = ? AND WarehouseID = ? AND DoorID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lDoorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlDoor.setPlantID(jrs.getString("PlantID"));
				mdlDoor.setWarehouseID(jrs.getString("WarehouseID"));
				mdlDoor.setDoorID(jrs.getString("DoorID"));
				mdlDoor.setDoorName(jrs.getString("DoorName"));
				mdlDoor.setGI_Indicator(jrs.getBoolean("GI_Indicator"));
				mdlDoor.setGR_Indicator(jrs.getBoolean("GR_Indicator"));
				mdlDoor.setCD_Indicator(jrs.getBoolean("CD_Indicator"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDoorByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDoorByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlDoor;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlDoor> LoadDoorByPlantAndWarehouse(String lPlantID, String lWarehouseID) {
		List<model.mdlDoor> listmdlDoor = new ArrayList<model.mdlDoor>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, DoorID, DoorName "
					+ "FROM wms_door "
					+ "WHERE PlantID = ? AND WarehouseID =?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				model.mdlDoor mdlDoor = new model.mdlDoor();
	
				mdlDoor.setPlantID(jrs.getString("PlantID"));
				mdlDoor.setWarehouseID(jrs.getString("WarehouseID"));
				mdlDoor.setDoorID(jrs.getString("DoorID"));
				mdlDoor.setDoorName(jrs.getString("DoorName"));
				
				listmdlDoor.add(mdlDoor);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDoorByPlantAndWarehouse", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDoorByPlantAndWarehouse", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlDoor;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlDoor> LoadDoor() {
		List<model.mdlDoor> listmdlDoor = new ArrayList<model.mdlDoor>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT a.PlantID, b.PlantName, a.WarehouseID, c.WarehouseName, a.DoorID, a.DoorName, a.GI_Indicator, a.GR_Indicator, a.CD_Indicator "
					+ "FROM wms_door a "
					+ "INNER JOIN plant b ON b.PlantID=a.PlantID "
					+ "INNER JOIN warehouse c ON c.PlantID=a.PlantID AND c.WarehouseID=a.WarehouseID "
					+ "WHERE a.PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next()){
				model.mdlDoor mdlDoor = new model.mdlDoor();
				mdlDoor.setPlantID(jrs.getString("PlantID"));
				mdlDoor.setPlantName(jrs.getString("PlantName"));
				mdlDoor.setWarehouseID(jrs.getString("WarehouseID"));
				mdlDoor.setWarehouseName(jrs.getString("WarehouseName"));
				mdlDoor.setDoorID(jrs.getString("DoorID"));
				mdlDoor.setDoorName(jrs.getString("DoorName"));
				mdlDoor.setGI_Indicator(jrs.getBoolean("GI_Indicator"));
				mdlDoor.setGR_Indicator(jrs.getBoolean("GR_Indicator"));
				mdlDoor.setCD_Indicator(jrs.getBoolean("CD_Indicator"));
				listmdlDoor.add(mdlDoor);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDoor", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDoor", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlDoor;
	}

	public static List<model.mdlDoor> LoadDoorJoin(String lUser) {
		List<model.mdlDoor> listmdlDoor = new ArrayList<model.mdlDoor>();

		Connection connection = null;
		PreparedStatement pstmLoadDoor = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();

			String sqlLoadDoor = "SELECT door.PlantID, plant.PlantName, door.WarehouseID, warehouse.WarehouseName, "
					+ "door.DoorID, door.DoorName, door.GI_Indicator, door.GR_Indicator, door.CD_Indicator "
					+ "FROM ums_door door "
					+ "LEFT JOIN plant ON plant.PlantID = Door.PlantID "
					+ "LEFT JOIN warehouse ON warehouse.WarehouseID = Door.WarehouseID ";
					//+ "WHERE Door.PlantID IN (" + Globals.user_area + ")";

			pstmLoadDoor = connection.prepareStatement(sqlLoadDoor);
			//pstmLoadWarehouse.setString(1,  Globals.user_area);

			Globals.gCommand = pstmLoadDoor.toString();

			jrs = pstmLoadDoor.executeQuery();

			while(jrs.next()){
				model.mdlDoor mdlDoor = new model.mdlDoor();
				mdlDoor.setPlantID(jrs.getString("PlantID"));
				mdlDoor.setPlantName(jrs.getString("PlantName"));
				mdlDoor.setWarehouseID(jrs.getString("WarehouseID"));
				mdlDoor.setWarehouseName(jrs.getString("WarehouseName"));
				mdlDoor.setDoorID(jrs.getString("DoorID"));
				mdlDoor.setDoorName(jrs.getString("DoorName"));
				mdlDoor.setGI_Indicator(jrs.getBoolean("GI_Indicator"));
				mdlDoor.setGR_Indicator(jrs.getBoolean("GR_Indicator"));
				mdlDoor.setCD_Indicator(jrs.getBoolean("CD_Indicator"));

				listmdlDoor.add(mdlDoor);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDoorJoin", Globals.gCommand , lUser);
		}
		finally {
			try{
				 if (pstmLoadDoor != null) {
					 pstmLoadDoor.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDoorJoin", "close opened connection protocol", lUser);
			}
		 }

		return listmdlDoor;
	}

	@SuppressWarnings("resource")
	public static String InsertDoor(model.mdlDoor lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlDoor CheckDoor = LoadDoorByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getDoorID());
			if(CheckDoor.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, DoorID, DoorName, GI_Indicator, GR_Indicator, CD_Indicator "
								+ "FROM wms_door LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("DoorID",lParam.getDoorID());
				jrs.updateString("DoorName",lParam.getDoorName());
				jrs.updateBoolean("GI_Indicator", lParam.getGI_Indicator());
				jrs.updateBoolean("GR_Indicator", lParam.getGR_Indicator());
				jrs.updateBoolean("CD_Indicator", lParam.getCD_Indicator());

				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Door";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Door sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertDoor", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertDoor", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String UpdateDoor(model.mdlDoor lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID, WarehouseID, DoorID, DoorName, GI_Indicator, GR_Indicator, CD_Indicator "
							+ "FROM wms_door WHERE PlantID = ? AND WarehouseID = ? AND DoorID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getDoorID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("DoorName", lParam.getDoorName());
			jrs.updateBoolean("GI_Indicator", lParam.getGI_Indicator());
			jrs.updateBoolean("GR_Indicator", lParam.getGR_Indicator());
			jrs.updateBoolean("CD_Indicator", lParam.getCD_Indicator());
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Door";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateDoor", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateDoor", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeleteDoor(String lPlantID, String lWarehouseID, String lDoorID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, DoorID FROM wms_door "
					+ "WHERE PlantID = ? AND WarehouseID = ? AND DoorID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lDoorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Door";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteDoor", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteDoor", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
}
