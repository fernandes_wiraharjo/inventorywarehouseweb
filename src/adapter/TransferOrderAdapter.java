package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlPalletization;
import model.mdlProductWM;
import model.mdlStorageBin;
import model.mdlTraceCode;
import model.mdlTransferOrder;
import model.mdlWMNumberRanges;

public class TransferOrderAdapter {

	public static List<model.mdlTransferOrder> CreatePlacementPlan (model.mdlProductWM lParam, model.mdlPalletization lParamPalletization) {
		List<model.mdlTransferOrder> listPlacementPlan = new ArrayList<model.mdlTransferOrder>();
		model.mdlTransferOrder lResult = new model.mdlTransferOrder();
		//JdbcRowSet jrsStorageTypeSearch = new JdbcRowSetImpl();
		//JdbcRowSet jrsStorageSectionSearch = new JdbcRowSetImpl();
		//JdbcRowSet jrsStorageBin = new JdbcRowSetImpl();
		Globals.gCommand = "";
		String lPlantID = lParam.getPlantID();
		String lWarehouseID = lParam.getWarehouseID();
		String lProductID = lParam.getProductID();
		String lProductUOM = lParam.getCapacityUsageUOM();
		String lPlacementStorageTypeIndicator = lParam.getStockPlacement();
		String lStorageSectionIndicator = lParam.getStorageSectionIndicator();
		Double lProductCapacityUsage = lParam.getCapacityUsage();
		Integer lStorageUnit = lParamPalletization.getStorageUnit1();
		Integer lStorageUnit2 = lParamPalletization.getStorageUnit2();
		Integer lQtyPerStorageUnit = lParamPalletization.getQtyPerStorageUnit1();
		Integer lQtyPerStorageUnit2 = lParamPalletization.getQtyPerStorageUnit2();
		String lStorageUnitType = lParamPalletization.getStorageUnitType1();
		String lStorageUnitType2 = lParamPalletization.getStorageUnitType2();
		String lBatchNo = lParam.getBatchNo();
		String lInboundDoc = lParamPalletization.getInboundDoc();
		String lSrcStorType = lParamPalletization.getSrcStorType();
		String lSrcStorBin = lParamPalletization.getSrcStorBin();
		int indexStorBin;
		int indexSUT = 0;
		int indexPlacementPlan = 1;
		int indexStorTypeSearchSeq = 0;
		List<model.mdlTraceCode> listTraceCode = new ArrayList<model.mdlTraceCode>();
		Boolean breakStorageSectionSearch=false;
		Boolean breakStorageSectionSearchFixBin = false;
		
		try{
			//CHECK TEMPORARY USED STORAGE BIN, IF 0, GET THE DATA FROM THE BACK UP
			if(Globals.gUsedStorBin.size() == 0)
				Globals.gUsedStorBin.addAll(Globals.gBackupUsedStorBin);
			
				//GET PRODUCT CAPACITY USAGE, SUT, SUTQTY FROM PALLETIZATION
				//PALLETE 1
				List<model.mdlProductWM> listCapacity = new ArrayList<model.mdlProductWM>();
				for (int x = 1; x <= lStorageUnit; x++) {
					model.mdlProductWM capacity = new model.mdlProductWM();
					
					capacity.setCapacityUsage(lQtyPerStorageUnit*lProductCapacityUsage);
					capacity.setQtyPerStorageUnit(lQtyPerStorageUnit);
					capacity.setStorageUnitType(lStorageUnitType);
					
					listCapacity.add(capacity);
				}
				//PALLETE 2
				for (int y = 1; y <= lStorageUnit2; y++) {
					model.mdlProductWM capacity = new model.mdlProductWM();
					
					capacity.setCapacityUsage(lQtyPerStorageUnit2*lProductCapacityUsage);
					capacity.setQtyPerStorageUnit(lQtyPerStorageUnit2);
					capacity.setStorageUnitType(lStorageUnitType2);
					
					listCapacity.add(capacity);
				}
				
				//GET STORAGE TYPE SEQUENCE
				String listStorageSeq = "";
				
				for (int i = 1; i < 26; i++) {
					if(listStorageSeq.contentEquals(""))
						listStorageSeq = "Sequence1";
					else
						listStorageSeq += ",Sequence"+i;
				}
				
				JdbcRowSet jrsStorageTypeSearch = database.RowSetAdapter.getJDBCRowSet();
				jrsStorageTypeSearch.setCommand("SELECT "+listStorageSeq+" "
						+ "FROM wms_storagetype_search "
						+ "WHERE PlantID=? AND WarehouseID=? AND StorageTypeIndicatorID=? AND Operation=?");
				jrsStorageTypeSearch.setString(1, lPlantID);
				jrsStorageTypeSearch.setString(2, lWarehouseID);
				jrsStorageTypeSearch.setString(3, lPlacementStorageTypeIndicator);
				jrsStorageTypeSearch.setString(4, "E");
				
				Globals.gCommand = jrsStorageTypeSearch.getCommand();
				jrsStorageTypeSearch.execute();
				while(jrsStorageTypeSearch.next()){
				//START PLACEMENT LOGIC
					//ITERATE STORAGE TYPE
					for (int i = 1; i < 26; i++) {
						if(breakStorageSectionSearch)
							i=1;
						
						indexStorTypeSearchSeq=i;
						
						if(indexSUT >= listCapacity.size())
							break;
						
						if( !(jrsStorageTypeSearch.getString("Sequence"+i).contentEquals("") || jrsStorageTypeSearch.getString("Sequence"+i)==null) )
						{
							String StorageType = jrsStorageTypeSearch.getString("Sequence"+i);
							
							//GET STORAGE TYPE PUTAWAY STRATEGY
							String PutawayStrategy = StorageTypeAdapter.LoadStorageTypeByKey(StorageType, lPlantID, lWarehouseID).getPutawayStrategy();
							
							//GET STORAGE SECTION SEQUENCE
							JdbcRowSet jrsStorageSectionSearch = database.RowSetAdapter.getJDBCRowSet();
							jrsStorageSectionSearch.setCommand("SELECT "+listStorageSeq+" "
									+ "FROM wms_storagesection_search "
									+ "WHERE PlantID=? AND WarehouseID=? AND StorageTypeID=? AND StorageSectionIndicatorID=?");
							jrsStorageSectionSearch.setString(1, lPlantID);
							jrsStorageSectionSearch.setString(2, lWarehouseID);
							jrsStorageSectionSearch.setString(3, StorageType);
							jrsStorageSectionSearch.setString(4, lStorageSectionIndicator);
							
							Globals.gCommand = jrsStorageSectionSearch.getCommand();
							jrsStorageSectionSearch.execute();
							while(jrsStorageSectionSearch.next()){
								//ITERATE STORAGE SECTION
								breakStorageSectionSearch = false;
								breakStorageSectionSearchFixBin = false;
								
								for (int j = 1; j < 26; j++) {
									//if(indexSUT >= listCapacity.size())
									//break;
									
								if(breakStorageSectionSearch)
									break;
								
								if(breakStorageSectionSearchFixBin)
									break;
									
									if( !(jrsStorageSectionSearch.getString("Sequence"+j).contentEquals("") || jrsStorageSectionSearch.getString("Sequence"+j)==null) )
									{
										String StorageSection = jrsStorageSectionSearch.getString("Sequence"+j);
										
										List<model.mdlStorageBin> listStorageBin = new ArrayList<model.mdlStorageBin>();
										if(!PutawayStrategy.contentEquals("F")){
											//GET STORAGE BIN AND PROCCESS
											JdbcRowSet jrsStorageBin = database.RowSetAdapter.getJDBCRowSet();
											jrsStorageBin.setCommand("SELECT StorageBinID,TotalCapacity,CapacityUsed "
													+ "FROM wms_storagebin "
													+ "WHERE PlantID=? AND WarehouseID=? AND StorageTypeID=? AND StorageSectionID=? AND StorageBinTypeID=? AND PutawayBlock=0");
											jrsStorageBin.setString(1, lPlantID);
											jrsStorageBin.setString(2, lWarehouseID);
											jrsStorageBin.setString(3, StorageType);
											jrsStorageBin.setString(4, StorageSection);
											jrsStorageBin.setString(5, listCapacity.get(indexSUT).getStorageUnitType());
											
											Globals.gCommand = jrsStorageBin.getCommand();
											jrsStorageBin.execute();
											
											while(jrsStorageBin.next()){
												model.mdlStorageBin mdlStorageBin = new model.mdlStorageBin();
												mdlStorageBin.setStorageBinID(jrsStorageBin.getString("StorageBinID"));
												mdlStorageBin.setTotalCapacity(jrsStorageBin.getDouble("TotalCapacity"));
												mdlStorageBin.setCapacityUsed(jrsStorageBin.getDouble("CapacityUsed"));
												mdlStorageBin.setCapacityLeft(mdlStorageBin.getTotalCapacity() - mdlStorageBin.getCapacityUsed());
												
												listStorageBin.add(mdlStorageBin);
											}
											jrsStorageBin.close();
										}
										
										//PLACEMENT BY PUTAWAY STRATEGY
										
											//"L PUTAWAY STRATEGY -- Next Empty Bin"
											if(PutawayStrategy.contentEquals("L")){
													//for(indexStorBin = 0; indexSUT < listCapacity.size(); indexStorBin++){
													//check if storage bin is still available or not
													for(indexStorBin = 0; (listStorageBin.size()-1) >= indexStorBin; indexStorBin++){
														
														//SUT PLACEMENT TO STORAGE BIN PROCCESS
														//check if storage bin has been used or not
														Boolean checkStorBin = true;
														for(mdlStorageBin usedstorbin : Globals.gUsedStorBin){
															if(usedstorbin.getStorageBinID().contentEquals(listStorageBin.get(indexStorBin).getStorageBinID())){
																checkStorBin = false;
																break;
															}
														}
														//check the condition that matches with the "L" strategy
														if( (listStorageBin.get(indexStorBin).getCapacityUsed() == 0) 
																&& (listStorageBin.get(indexStorBin).getCapacityLeft() >= listCapacity.get(indexSUT).getCapacityUsage()) 
																&& checkStorBin
															)
														{
															//GET STORAGE UNIT RUNNING NUMBER
															Integer SURunningNumber;
															mdlTraceCode mdlTraceCode = TraceCodeAdapter.LoadLastWMTraceCodeByFlag("WM_StorageUnit_"+lPlantID+"_"+lWarehouseID);
															if(mdlTraceCode.getTraceCode() == null){
																mdlWMNumberRanges mdlWMNumberRanges = new model.mdlWMNumberRanges();
																mdlWMNumberRanges.setPlantID(lPlantID);
																mdlWMNumberRanges.setWarehouseID(lWarehouseID);
																mdlWMNumberRanges.setNumberRangesType("For Storage Unit");
																
																SURunningNumber = Integer.parseInt(WMNumberRangesAdapter.LoadWMNumberRangesByKey(mdlWMNumberRanges).getFromNumber());
																TraceCodeAdapter.InsertTraceCode(String.valueOf(SURunningNumber), LocalDateTime.now().toString(), "WM_StorageUnit_"+lPlantID+"_"+lWarehouseID);
																
																mdlTraceCode = new model.mdlTraceCode();
																mdlTraceCode.setTraceCode(String.valueOf(SURunningNumber));
																mdlTraceCode.setFlag_Table(String.valueOf("WM_StorageUnit_"+lPlantID+"_"+lWarehouseID));
															}
															else{
																SURunningNumber = Integer.parseInt(mdlTraceCode.getTraceCode())+1;
																TraceCodeAdapter.InsertTraceCode(String.valueOf(SURunningNumber), LocalDateTime.now().toString(), "WM_StorageUnit_"+lPlantID+"_"+lWarehouseID);
															
																mdlTraceCode = new model.mdlTraceCode();
																mdlTraceCode.setTraceCode(String.valueOf(SURunningNumber));
																mdlTraceCode.setFlag_Table(String.valueOf("WM_StorageUnit_"+lPlantID+"_"+lWarehouseID));
															}
															listTraceCode.add(mdlTraceCode);
															
															//STORE PLACEMENT PLAN
															model.mdlTransferOrder PlacementPlan = new model.mdlTransferOrder();
															PlacementPlan.setNo(indexPlacementPlan);
															PlacementPlan.setPlantID(lPlantID);
															PlacementPlan.setWarehouseID(lWarehouseID);
															PlacementPlan.setSrcStorageType(lSrcStorType);
															PlacementPlan.setSrcStorageBin(lSrcStorBin);
															PlacementPlan.setProductID(lProductID);
															PlacementPlan.setDestinationQty(listCapacity.get(indexSUT).getQtyPerStorageUnit());
															PlacementPlan.setStorageUnitType(listCapacity.get(indexSUT).getStorageUnitType());
															PlacementPlan.setStorageTypeID(StorageType);
															PlacementPlan.setStorageSectionID(StorageSection);
															PlacementPlan.setDestinationStorageBin(listStorageBin.get(indexStorBin).getStorageBinID());
															PlacementPlan.setDestinationStorageUnit(SURunningNumber);
															PlacementPlan.setBatchNo(lBatchNo);
															PlacementPlan.setUOM(lProductUOM);
															PlacementPlan.setInboundDocID(lInboundDoc);
															PlacementPlan.setCapacityUsed(listCapacity.get(indexSUT).getCapacityUsage());
															PlacementPlan.setResult("0");
															
															listPlacementPlan.add(PlacementPlan);
															indexSUT++;
															indexPlacementPlan++;
															//break the storage section search if placement is finished and get back to the first storage type search again
															breakStorageSectionSearch = true;
															
															//save the used bin into temporary variable
															model.mdlStorageBin UsedStorBin = new model.mdlStorageBin();
															UsedStorBin.setStorageBinID(PlacementPlan.getDestinationStorageBin());
															Globals.gUsedStorBin.add(UsedStorBin);
															
															break;
														}
														//if((listStorageBin.size()-1) == indexStorBin)
														//break;
													}
											} //end of "L" PUTAWAY STRATEGY
											
											
											//"I PUTAWAY STRATEGY -- Addition to existing stock"
											if(PutawayStrategy.contentEquals("I")){
													//for(indexStorBin = 0; indexSUT < listCapacity.size(); indexStorBin++){
													//check if storage bin is still available or not
													for(indexStorBin = 0; (listStorageBin.size()-1) >= indexStorBin; indexStorBin++){
														
														//SUT PLACEMENT TO STORAGE BIN PROCCESS
														//get used storage bin's capacity left
														for(mdlStorageBin usedstorbin : Globals.gUsedStorBin){
															if(usedstorbin.getStorageBinID().contentEquals(listStorageBin.get(indexStorBin).getStorageBinID()))
																listStorageBin.get(indexStorBin).setCapacityLeft(usedstorbin.getCapacityLeft());
														}
														//check the condition that matches with the "I" strategy
														if( listStorageBin.get(indexStorBin).getCapacityLeft() >= listCapacity.get(indexSUT).getCapacityUsage() )
														{
															//GET STORAGE UNIT RUNNING NUMBER
															Integer SURunningNumber;
															mdlTraceCode mdlTraceCode = TraceCodeAdapter.LoadLastWMTraceCodeByFlag("WM_StorageUnit_"+lPlantID+"_"+lWarehouseID);
															if(mdlTraceCode.getTraceCode() == null){
																mdlWMNumberRanges mdlWMNumberRanges = new model.mdlWMNumberRanges();
																mdlWMNumberRanges.setPlantID(lPlantID);
																mdlWMNumberRanges.setWarehouseID(lWarehouseID);
																mdlWMNumberRanges.setNumberRangesType("For Storage Unit");
																
																SURunningNumber = Integer.parseInt(WMNumberRangesAdapter.LoadWMNumberRangesByKey(mdlWMNumberRanges).getFromNumber());
																TraceCodeAdapter.InsertTraceCode(String.valueOf(SURunningNumber), LocalDateTime.now().toString(), "WM_StorageUnit_"+lPlantID+"_"+lWarehouseID);
																
																mdlTraceCode = new model.mdlTraceCode();
																mdlTraceCode.setTraceCode(String.valueOf(SURunningNumber));
																mdlTraceCode.setFlag_Table(String.valueOf("WM_StorageUnit_"+lPlantID+"_"+lWarehouseID));
															}
															else{
																SURunningNumber = Integer.parseInt(mdlTraceCode.getTraceCode())+1;
																TraceCodeAdapter.InsertTraceCode(String.valueOf(SURunningNumber), LocalDateTime.now().toString(), "WM_StorageUnit_"+lPlantID+"_"+lWarehouseID);
															
																mdlTraceCode = new model.mdlTraceCode();
																mdlTraceCode.setTraceCode(String.valueOf(SURunningNumber));
																mdlTraceCode.setFlag_Table(String.valueOf("WM_StorageUnit_"+lPlantID+"_"+lWarehouseID));
															}
															listTraceCode.add(mdlTraceCode);
															
															//STORE PLACEMENT PLAN
															model.mdlTransferOrder PlacementPlan = new model.mdlTransferOrder();
															PlacementPlan.setNo(indexPlacementPlan);
															PlacementPlan.setPlantID(lPlantID);
															PlacementPlan.setWarehouseID(lWarehouseID);
															PlacementPlan.setSrcStorageType(lSrcStorType);
															PlacementPlan.setSrcStorageBin(lSrcStorBin);
															PlacementPlan.setProductID(lProductID);
															PlacementPlan.setDestinationQty(listCapacity.get(indexSUT).getQtyPerStorageUnit());
															PlacementPlan.setStorageUnitType(listCapacity.get(indexSUT).getStorageUnitType());
															PlacementPlan.setStorageTypeID(StorageType);
															PlacementPlan.setStorageSectionID(StorageSection);
															PlacementPlan.setDestinationStorageBin(listStorageBin.get(indexStorBin).getStorageBinID());
															PlacementPlan.setDestinationStorageUnit(SURunningNumber);
															PlacementPlan.setBatchNo(lBatchNo);
															PlacementPlan.setUOM(lProductUOM);
															PlacementPlan.setInboundDocID(lInboundDoc);
															PlacementPlan.setCapacityUsed(listCapacity.get(indexSUT).getCapacityUsage());
															PlacementPlan.setResult("0");
															
															listPlacementPlan.add(PlacementPlan);
															
															Double lastBinCapacity = listStorageBin.get(indexStorBin).getCapacityLeft() - listCapacity.get(indexSUT).getCapacityUsage();
															//listStorageBin.get(indexStorBin).setCapacityLeft(lastBinCapacity);
															
															indexSUT++;
															indexPlacementPlan++;														
															//break the storage section search if placement is finished and get back to the first storage type search again
															breakStorageSectionSearch = true;
															
															//save the used bin into temporary variable
															model.mdlStorageBin UsedStorBin = new model.mdlStorageBin();
															UsedStorBin.setStorageBinID(PlacementPlan.getDestinationStorageBin());
															UsedStorBin.setCapacityLeft(lastBinCapacity);
															Globals.gUsedStorBin.add(UsedStorBin);
															
															break;
														}
														//if((listStorageBin.size()-1) == indexStorBin)
														//break;
													}
											} //end of "I" PUTAWAY STRATEGY
											
											
											//"F PUTAWAY STRATEGY -- Fixed Bin"
											if(PutawayStrategy.contentEquals("F")){
												
												String fixedStorBin = ProductWMAdapter.LoadProductWMByKey(lPlantID, lWarehouseID, lProductID).getStorageBin();
												model.mdlStorageBin Storbin = StorageBinAdapter.LoadStorageBinByKey2(lPlantID, lWarehouseID, fixedStorBin, StorageType,StorageSection);
												String findStorBin = Storbin.getStorageBinID();
												//check the condition that matches with the "F" strategy
												if(findStorBin != null)
												{
													//while(indexSUT < listCapacity.size()){
														//SUT PLACEMENT TO STORAGE BIN PROCCESS
														//get used fix storage bin's capacity left
														for(mdlStorageBin usedstorbin : Globals.gUsedStorBin){
															if(usedstorbin.getStorageBinID().contentEquals(fixedStorBin))
																Storbin.setCapacityLeft(usedstorbin.getCapacityLeft());
														}
														
														Double StorBinCapacityLeft = Storbin.getCapacityLeft();
														String StorBinTypeID = Storbin.getStorageBinTypeID();
														
														if( (StorBinCapacityLeft >= listCapacity.get(indexSUT).getCapacityUsage())
																&& (StorBinTypeID.contentEquals(listCapacity.get(indexSUT).getStorageUnitType()))
																)
														{
															//GET STORAGE UNIT RUNNING NUMBER
															Integer SURunningNumber;
															mdlTraceCode mdlTraceCode = TraceCodeAdapter.LoadLastWMTraceCodeByFlag("WM_StorageUnit_"+lPlantID+"_"+lWarehouseID);
															if(mdlTraceCode.getTraceCode() == null){
																mdlWMNumberRanges mdlWMNumberRanges = new model.mdlWMNumberRanges();
																mdlWMNumberRanges.setPlantID(lPlantID);
																mdlWMNumberRanges.setWarehouseID(lWarehouseID);
																mdlWMNumberRanges.setNumberRangesType("For Storage Unit");
																
																SURunningNumber = Integer.parseInt(WMNumberRangesAdapter.LoadWMNumberRangesByKey(mdlWMNumberRanges).getFromNumber());
																TraceCodeAdapter.InsertTraceCode(String.valueOf(SURunningNumber), LocalDateTime.now().toString(), "WM_StorageUnit_"+lPlantID+"_"+lWarehouseID);
																
																mdlTraceCode = new model.mdlTraceCode();
																mdlTraceCode.setTraceCode(String.valueOf(SURunningNumber));
																mdlTraceCode.setFlag_Table(String.valueOf("WM_StorageUnit_"+lPlantID+"_"+lWarehouseID));
															}
															else{
																SURunningNumber = Integer.parseInt(mdlTraceCode.getTraceCode())+1;
																TraceCodeAdapter.InsertTraceCode(String.valueOf(SURunningNumber), LocalDateTime.now().toString(), "WM_StorageUnit_"+lPlantID+"_"+lWarehouseID);
															
																mdlTraceCode = new model.mdlTraceCode();
																mdlTraceCode.setTraceCode(String.valueOf(SURunningNumber));
																mdlTraceCode.setFlag_Table(String.valueOf("WM_StorageUnit_"+lPlantID+"_"+lWarehouseID));
															}
															listTraceCode.add(mdlTraceCode);
															
															//STORE PLACEMENT PLAN
															model.mdlTransferOrder PlacementPlan = new model.mdlTransferOrder();
															PlacementPlan.setNo(indexPlacementPlan);
															PlacementPlan.setPlantID(lPlantID);
															PlacementPlan.setWarehouseID(lWarehouseID);
															PlacementPlan.setSrcStorageType(lSrcStorType);
															PlacementPlan.setSrcStorageBin(lSrcStorBin);
															PlacementPlan.setProductID(lProductID);
															PlacementPlan.setDestinationQty(listCapacity.get(indexSUT).getQtyPerStorageUnit());
															PlacementPlan.setStorageUnitType(listCapacity.get(indexSUT).getStorageUnitType());
															PlacementPlan.setStorageTypeID(StorageType);
															PlacementPlan.setStorageSectionID(StorageSection);
															PlacementPlan.setDestinationStorageBin(fixedStorBin);
															PlacementPlan.setDestinationStorageUnit(SURunningNumber);
															PlacementPlan.setBatchNo(lBatchNo);
															PlacementPlan.setUOM(lProductUOM);
															PlacementPlan.setInboundDocID(lInboundDoc);
															PlacementPlan.setCapacityUsed(listCapacity.get(indexSUT).getCapacityUsage());
															PlacementPlan.setResult("0");
															
															listPlacementPlan.add(PlacementPlan);
															
															StorBinCapacityLeft = StorBinCapacityLeft - listCapacity.get(indexSUT).getCapacityUsage();
															indexSUT++;
															indexPlacementPlan++;
															//break the storage section search if placement is finished and get back to the first storage type search again
															breakStorageSectionSearch = true;
															
															//save the used fix bin into temporary variable
															model.mdlStorageBin UsedStorBin = new model.mdlStorageBin();
															UsedStorBin.setStorageBinID(PlacementPlan.getDestinationStorageBin());
															UsedStorBin.setCapacityLeft(StorBinCapacityLeft);
															Globals.gUsedStorBin.add(UsedStorBin);
														}
														else
															breakStorageSectionSearchFixBin = true; //break the storage section search if placement is failed and get to the next storage type
													//}
												}
											} //end of "F" PUTAWAY STRATEGY
									}
									else
										break;
								}//end of iterate storage section
							}
							jrsStorageSectionSearch.close();
							
						}
						else{
							listPlacementPlan = new ArrayList<model.mdlTransferOrder>();
							lResult = new model.mdlTransferOrder();
							lResult.setResult("1");
							listPlacementPlan.add(lResult);
							TraceCodeAdapter.DeleteTraceCode2(listTraceCode);
							Globals.gUsedStorBin = new ArrayList<model.mdlStorageBin>();
							break;
						}
					}//end of iterate storage type
					
					//this check is especially for the condition where all the storage type search have been reached and there is still no space for placement
					if( ((indexSUT < listCapacity.size()) && (indexStorTypeSearchSeq==25) && !(jrsStorageTypeSearch.getString("Sequence25").contentEquals("") || jrsStorageTypeSearch.getString("Sequence25")==null)) )
					{
						listPlacementPlan = new ArrayList<model.mdlTransferOrder>();
						lResult = new model.mdlTransferOrder();
						lResult.setResult("1");
						listPlacementPlan.add(lResult);
						TraceCodeAdapter.DeleteTraceCode2(listTraceCode);
						Globals.gUsedStorBin = new ArrayList<model.mdlStorageBin>();
					}
				}
				jrsStorageTypeSearch.close();
				
				if(listPlacementPlan.get(0).getPlantID() != null){
					Globals.gBackupUsedStorBin = new ArrayList<model.mdlStorageBin>();
					Globals.gBackupUsedStorBin.addAll(Globals.gUsedStorBin);
					InsertTransferOrder(listPlacementPlan); //INSERT TEMPORARY TRANSFER ORDER
				}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CreatePlacementPlan", Globals.gCommand , Globals.user);
			listPlacementPlan = new ArrayList<model.mdlTransferOrder>();
			lResult = new model.mdlTransferOrder();
			lResult.setResult("2");
			listPlacementPlan.add(lResult);
			TraceCodeAdapter.DeleteTraceCode2(listTraceCode);
			Globals.gUsedStorBin = new ArrayList<model.mdlStorageBin>();
		}
		
		return listPlacementPlan;
	}
	
	public static String InsertTransferOrder(List<model.mdlTransferOrder> lParam)
	{	
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmInsertTO = null;
		PreparedStatement pstmInsertTODetail = null;
		String lPlantID = lParam.get(0).getPlantID();
		String lWarehouseID = lParam.get(0).getWarehouseID();
		
		//GET TRANSFER ORDER DOC NUMBER
		Integer TODocNumber;
		mdlTraceCode mdlTraceCode = TraceCodeAdapter.LoadLastWMTraceCodeByFlag("WM_TransferOrder_"+lPlantID+"_"+lWarehouseID);
		if(mdlTraceCode.getTraceCode() == null){
			mdlWMNumberRanges mdlWMNumberRanges = new model.mdlWMNumberRanges();
			mdlWMNumberRanges.setPlantID(lPlantID);
			mdlWMNumberRanges.setWarehouseID(lWarehouseID);
			mdlWMNumberRanges.setNumberRangesType("For Transfer Order");
			
			TODocNumber = Integer.parseInt(WMNumberRangesAdapter.LoadWMNumberRangesByKey(mdlWMNumberRanges).getFromNumber());
			TraceCodeAdapter.InsertTraceCode(String.valueOf(TODocNumber), LocalDateTime.now().toString(), "WM_TransferOrder_"+lPlantID+"_"+lWarehouseID);
		}
		else{
			TODocNumber = Integer.parseInt(mdlTraceCode.getTraceCode())+1;
			TraceCodeAdapter.InsertTraceCode(String.valueOf(TODocNumber), LocalDateTime.now().toString(), "WM_TransferOrder_"+lPlantID+"_"+lWarehouseID);
		}
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlInsertTO = "INSERT INTO wms_transfer_order(`PlantID`, `WarehouseID`, `TransferOrderID`, `SrcStorageTypeID`, `SrcStorageBinID`, `InboundDocNo`, `CreatedDate`, `LastDate`, `CreatedBy`, `LastUpdateBy`) VALUES (?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertTODetail = "INSERT INTO wms_transfer_order_detail(`PlantID`, `WarehouseID`, `TransferOrderID`, `ProductID`, `BatchNo`, `StorageTypeID`, `StorageSectionID`, `StorageBinID`, `DestStorageUnit`, `Status`, `DestQty`, `DestQtyUOM`, `StorageUnitType`, `CapacityUsed`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
				pstmInsertTO = connection.prepareStatement(sqlInsertTO);
				pstmInsertTODetail = connection.prepareStatement(sqlInsertTODetail);
				
				//insert Transfer Order process parameter
				pstmInsertTO.setString(1,  lParam.get(0).getPlantID());
				pstmInsertTO.setString(2,  lParam.get(0).getWarehouseID());
				pstmInsertTO.setInt(3,  TODocNumber);
				pstmInsertTO.setString(4,  lParam.get(0).getSrcStorageType());
				pstmInsertTO.setString(5,  lParam.get(0).getSrcStorageBin());
				pstmInsertTO.setString(6,  lParam.get(0).getInboundDocID());
				pstmInsertTO.setString(7,  LocalDateTime.now().toString()); 
				pstmInsertTO.setString(8,  LocalDateTime.now().toString());
				pstmInsertTO.setString(9,  Globals.user);
				pstmInsertTO.setString(10, Globals.user);
				Globals.gCommand = pstmInsertTO.toString();
				pstmInsertTO.executeUpdate();
				
				//insert transfer order detail process parameter
				for(model.mdlTransferOrder to : lParam){
					pstmInsertTODetail.setString(1,  to.getPlantID());
					pstmInsertTODetail.setString(2,  to.getWarehouseID());
					pstmInsertTODetail.setInt(3,  TODocNumber);
					pstmInsertTODetail.setString(4,  to.getProductID());
					pstmInsertTODetail.setString(5,  to.getBatchNo());
					pstmInsertTODetail.setString(6,  to.getStorageTypeID());
					pstmInsertTODetail.setString(7,  to.getStorageSectionID()); 
					pstmInsertTODetail.setString(8,  to.getDestinationStorageBin());
					pstmInsertTODetail.setInt(9,  to.getDestinationStorageUnit());
					pstmInsertTODetail.setString(10, "PLANNED");
					pstmInsertTODetail.setInt(11, to.getDestinationQty());
					pstmInsertTODetail.setString(12, to.getUOM());
					pstmInsertTODetail.setString(13, to.getStorageUnitType());
					pstmInsertTODetail.setDouble(14, to.getCapacityUsed());
					Globals.gCommand = pstmInsertTODetail.toString();
					pstmInsertTODetail.executeUpdate();
				}
				
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Insert Temporary TO";
			}
			
			catch (Exception e ) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionInsertTransferOrder", Globals.gCommand , Globals.user);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertTransferOrder", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
			        if (pstmInsertTO != null) {
			        	pstmInsertTO.close();
			        }
			        if (pstmInsertTODetail != null) {
			        	pstmInsertTODetail.close();
			        }
			        connection.setAutoCommit(true);
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertTransferOrder", "close opened connection protocol", Globals.user);
				}
		    }
			
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlTransferOrder> LoadTransferOrderByInbound(String lInboundDoc) {
	List<model.mdlTransferOrder> listTODetail = new ArrayList<model.mdlTransferOrder>();
	JdbcRowSet jrs = new JdbcRowSetImpl();
	Globals.gCommand = "";
	try{
		
		jrs = database.RowSetAdapter.getJDBCRowSet();
		
		jrs.setCommand("SELECT a.ProductID, a.BatchNo, a.Status, a.StorageUnitType, a.StorageTypeID, "
				+ "a.StorageSectionID, a.StorageBinID, a.DestStorageUnit, a.DestQty, a.DestQtyUOM, a.CapacityUsed, "
				+ "a.PlantID, a.WarehouseID, a.TransferOrderID, b.LastDate, b.SrcStorageTypeID, b.SrcStorageBinID, "
				+ "c.WMS_Status, c.UpdateWMFirst "
				+ "FROM wms_transfer_order_detail a "
				+ "INNER JOIN wms_transfer_order b ON b.PlantID=a.PlantID AND b.WarehouseID=a.WarehouseID AND b.TransferOrderID=a.TransferOrderID "
				+ "INNER JOIN warehouse c ON c.PlantID=a.PlantID AND c.WarehouseID=a.WarehouseID "
				+ "WHERE b.InboundDocNo=? "
				+ "ORDER BY a.DestStorageUnit");
		jrs.setString(1, lInboundDoc);
		
		Globals.gCommand = jrs.getCommand();
		jrs.execute();
		
		int no = 1;
		while(jrs.next()){
			model.mdlTransferOrder TODetail = new model.mdlTransferOrder();
			TODetail.setNo(no);
			TODetail.setProductID(jrs.getString("ProductID"));
			TODetail.setBatchNo(jrs.getString("BatchNo"));
			TODetail.setStatus(jrs.getString("Status"));
			TODetail.setStorageUnitType(jrs.getString("StorageUnitType"));
			TODetail.setStorageTypeID(jrs.getString("StorageTypeID"));
			TODetail.setStorageSectionID(jrs.getString("StorageSectionID"));
			TODetail.setDestinationStorageBin(jrs.getString("StorageBinID"));
			TODetail.setDestinationStorageUnit(jrs.getInt("DestStorageUnit"));
			TODetail.setDestinationQty(jrs.getInt("DestQty"));
			TODetail.setUOM(jrs.getString("DestQtyUOM"));
			TODetail.setPlantID(jrs.getString("PlantID"));
			TODetail.setWarehouseID(jrs.getString("WarehouseID"));
			TODetail.setTransferOrderID(jrs.getString("TransferOrderID"));
			TODetail.setCreationDate(ConvertDateTimeHelper.formatDate(jrs.getString("LastDate"), "yyyy-MM-dd", "dd.MM.yyyy"));
			TODetail.setSrcStorageType(jrs.getString("SrcStorageTypeID"));
			TODetail.setSrcStorageBin(jrs.getString("SrcStorageBinID"));
			TODetail.setCapacityUsed(jrs.getDouble("CapacityUsed"));
			if(jrs.getString("Status").contentEquals("CONFIRMED"))
				TODetail.setButtonStatus("disabled");
			else
				TODetail.setButtonStatus("");
			TODetail.setWMS_Status(jrs.getBoolean("WMS_Status"));
			TODetail.setUpdateWMFirst(jrs.getBoolean("UpdateWMFirst"));
			
			listTODetail.add(TODetail);
			no++;
		}
	}
	catch(Exception ex){
		LogAdapter.InsertLogExc(ex.toString(), "LoadTransferOrderByInbound", Globals.gCommand , Globals.user);
	}
	finally{
		try{
			if(jrs!=null)
				jrs.close();
		}
		catch(Exception e){
			LogAdapter.InsertLogExc(e.toString(), "LoadTransferOrderByInbound", "close opened connection protocol", Globals.user);
		}
	}
	
	return listTODetail;
}
	
	public static String UpdateTransferOrder(model.mdlTransferOrder lParam, String lInboundDocNo)
	{	
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmUpdateTO = null;
		PreparedStatement pstmUpdateTODetail = null;
		PreparedStatement pstmUpdateInbound = null;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlUpdateInbound = "UPDATE inbound SET WMProcess_Status='WM in Process', Updated_by=?, Updated_at=? WHERE DocNumber=?";
				String sqlUpdateTO = "UPDATE wms_transfer_order SET LastUpdateBy=?, LastDate=? WHERE PlantID=? AND WarehouseID=? AND TransferOrderID=?;";
				String sqlUpdateTODetail = "UPDATE wms_transfer_order_detail SET `Status`='PROCESS' WHERE PlantID=? AND WarehouseID=? AND TransferOrderID=?;";
				pstmUpdateTO = connection.prepareStatement(sqlUpdateTO);
				pstmUpdateTODetail = connection.prepareStatement(sqlUpdateTODetail);
				pstmUpdateInbound = connection.prepareStatement(sqlUpdateInbound);
				
				//update Transfer Order process parameter
				pstmUpdateTO.setString(1,  Globals.user);
				pstmUpdateTO.setString(2,  LocalDateTime.now().toString());
				pstmUpdateTO.setString(3,  lParam.getPlantID());
				pstmUpdateTO.setString(4,  lParam.getWarehouseID());
				pstmUpdateTO.setString(5,  lParam.getTransferOrderID());
				Globals.gCommand = pstmUpdateTO.toString();
				pstmUpdateTO.executeUpdate();
				
				//update Transfer Order Detail process parameter
				pstmUpdateTODetail.setString(1,  lParam.getPlantID());
				pstmUpdateTODetail.setString(2,  lParam.getWarehouseID());
				pstmUpdateTODetail.setString(3,  lParam.getTransferOrderID());
				pstmUpdateTODetail.executeUpdate();
				
				//update Inbound process parameter
				pstmUpdateInbound.setString(1,  Globals.user);
				pstmUpdateInbound.setString(2,  LocalDateTime.now().toString());
				pstmUpdateInbound.setString(3,  lInboundDocNo);
				pstmUpdateInbound.executeUpdate();
				
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Update TO";
			}
			
			catch (Exception e ) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionUpdateTransferOrder", Globals.gCommand , Globals.user);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "UpdateTransferOrder", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
			        if (pstmUpdateTO != null) {
			        	pstmUpdateTO.close();
			        }
			        if (pstmUpdateTODetail != null) {
			        	pstmUpdateTODetail.close();
			        }
			        if (pstmUpdateInbound != null) {
			        	pstmUpdateInbound.close();
			        }
			        connection.setAutoCommit(true);
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "UpdateTransferOrder", "close opened connection protocol", Globals.user);
				}
		    }
			
		return Globals.gReturn_Status;
	}
	
	public static Boolean CheckTransferOrderStatus(mdlTransferOrder lParam) {
	Boolean Check = false;
	JdbcRowSet jrs = new JdbcRowSetImpl();
	Globals.gCommand = "";
	try{
		
		jrs = database.RowSetAdapter.getJDBCRowSet();
		
		jrs.setCommand("SELECT `Status` "
				+ "FROM wms_transfer_order_detail "
				+ "WHERE PlantID=? AND WarehouseID=? AND TransferOrderID=?");
		jrs.setString(1, lParam.getPlantID());
		jrs.setString(2, lParam.getWarehouseID());
		jrs.setString(3, lParam.getTransferOrderID());
		
		Globals.gCommand = jrs.getCommand();
		jrs.execute();
		
		int indexConfirmation = 1;
		int rowCount = 0;
		while(jrs.next()){
			if(jrs.getString("Status").contentEquals("CONFIRMED"))
				indexConfirmation++;
			
			rowCount++;
		}
		
		if(indexConfirmation == rowCount)
			Check = true;
	}
	catch(Exception ex){
		LogAdapter.InsertLogExc(ex.toString(), "CheckTransferOrderStatus", Globals.gCommand , Globals.user);
	}
	finally{
		try{
			if(jrs!=null)
				jrs.close();
		}
		catch(Exception e){
			LogAdapter.InsertLogExc(e.toString(), "CheckTransferOrderStatus", "close opened connection protocol", Globals.user);
		}
	}
	
	return Check;
}
	
	public static String ConfirmTO(model.mdlTransferOrder lParam)
	{	
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmUpdateTODetailStatus = null;
		PreparedStatement pstmUpdateStorageBin = null;
		PreparedStatement pstmInsertUpdateStorageBinDetail = null;
		PreparedStatement pstmUpdateInboundStatus = null;
		PreparedStatement pstmInsertUpdateStockSummary = null;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlUpdateTODetailStatus = "UPDATE wms_transfer_order_detail SET `Status`='CONFIRMED' WHERE PlantID=? AND WarehouseID=? AND TransferOrderID=? AND StorageTypeID=? AND StorageSectionID=? AND StorageBinID=? AND DestStorageUnit=?;";
				String sqlUpdateStorageBin = "UPDATE wms_storagebin SET Utilization=?, CapacityUsed=?, NoOfQuantity=?, NoStorageUnits=? WHERE PlantID=? AND WarehouseID=? AND StorageTypeID=? AND StorageBinID=? AND StorageSectionID=?;";
				String sqlInsertUpdateStorageBinDetail = "INSERT INTO wms_storagebin_detail(`PlantID`, `WarehouseID`, `StorageType`, `StorageSection`, `StorageBin`, `DestStorageUnit`, `StorageUnitType`, `ProductID`, `BatchNo`, `Qty`, `UOM`, `CreatedBy`, `UpdateBy`, `CreatedDate`, `UpdateDate`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `StorageUnitType`=?, `ProductID`=?, `BatchNo`=?, `Qty`=?, `UOM`=?, UpdateBy=?, UpdateDate=?;";
				String sqlUpdateInboundStatus = "UPDATE inbound SET WMProcess_Status='Completed',Updated_by=?,Updated_at=? WHERE DocNumber=?";
				String sqlInsertUpdateStockSummary = "INSERT INTO stock_summary(`Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
				
				pstmUpdateTODetailStatus = connection.prepareStatement(sqlUpdateTODetailStatus);
				pstmUpdateStorageBin = connection.prepareStatement(sqlUpdateStorageBin);
				pstmInsertUpdateStorageBinDetail = connection.prepareStatement(sqlInsertUpdateStorageBinDetail);
				pstmUpdateInboundStatus = connection.prepareStatement(sqlUpdateInboundStatus);
				pstmInsertUpdateStockSummary = connection.prepareStatement(sqlInsertUpdateStockSummary);
				
				//update Transfer Order Detail Status process parameter
				pstmUpdateTODetailStatus.setString(1,  lParam.getPlantID());
				pstmUpdateTODetailStatus.setString(2,  lParam.getWarehouseID());
				pstmUpdateTODetailStatus.setString(3,  lParam.getTransferOrderID());
				pstmUpdateTODetailStatus.setString(4,  lParam.getStorageTypeID());
				pstmUpdateTODetailStatus.setString(5,  lParam.getStorageSectionID()); 
				pstmUpdateTODetailStatus.setString(6,  lParam.getDestinationStorageBin());
				pstmUpdateTODetailStatus.setInt(7,  lParam.getDestinationStorageUnit());
				Globals.gCommand = pstmUpdateTODetailStatus.toString();
				pstmUpdateTODetailStatus.executeUpdate();
				
				//insert update Storage Bin Detail process parameter
				pstmInsertUpdateStorageBinDetail.setString(1,  lParam.getPlantID());
				pstmInsertUpdateStorageBinDetail.setString(2,  lParam.getWarehouseID());
				pstmInsertUpdateStorageBinDetail.setString(3,  lParam.getStorageTypeID());
				pstmInsertUpdateStorageBinDetail.setString(4,  lParam.getStorageSectionID());
				pstmInsertUpdateStorageBinDetail.setString(5,  lParam.getDestinationStorageBin()); 
				pstmInsertUpdateStorageBinDetail.setInt(6,  lParam.getDestinationStorageUnit());
				pstmInsertUpdateStorageBinDetail.setString(7,  lParam.getStorageUnitType());
				pstmInsertUpdateStorageBinDetail.setString(8,  lParam.getProductID());
				pstmInsertUpdateStorageBinDetail.setString(9,  lParam.getBatchNo());
				pstmInsertUpdateStorageBinDetail.setInt(10,  lParam.getDestinationQty());
				pstmInsertUpdateStorageBinDetail.setString(11,  lParam.getUOM());
				pstmInsertUpdateStorageBinDetail.setString(12,  Globals.user); 
				pstmInsertUpdateStorageBinDetail.setString(13,  Globals.user);
				pstmInsertUpdateStorageBinDetail.setString(14,  LocalDateTime.now().toString());
				pstmInsertUpdateStorageBinDetail.setString(15,  LocalDateTime.now().toString());
				pstmInsertUpdateStorageBinDetail.setString(16,  lParam.getStorageUnitType());
				pstmInsertUpdateStorageBinDetail.setString(17,  lParam.getProductID());
				pstmInsertUpdateStorageBinDetail.setString(18,  lParam.getBatchNo());
				pstmInsertUpdateStorageBinDetail.setInt(19,  lParam.getDestinationQty()); 
				pstmInsertUpdateStorageBinDetail.setString(20,  lParam.getUOM());
				pstmInsertUpdateStorageBinDetail.setString(21,  Globals.user);
				pstmInsertUpdateStorageBinDetail.setString(22,  LocalDateTime.now().toString());
				Globals.gCommand = pstmInsertUpdateStorageBinDetail.toString();
				pstmInsertUpdateStorageBinDetail.executeUpdate();
				
				//update Storage Bin process parameter
				//get last storage bin data
				model.mdlStorageBin StorBin = new model.mdlStorageBin();
				StorBin = StorageBinAdapter.LoadStorageBinByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getDestinationStorageBin(), lParam.getStorageTypeID());
				Double StorBinTotalCapacity = StorBin.getTotalCapacity();
				Double StorBinCapacityUsed = StorBin.getCapacityUsed();
				Double newStorBinCapacityUsed = StorBinCapacityUsed + lParam.getCapacityUsed(); //add the new capacity to stor bin
				Double StorBinUtilization;
				if(StorBinTotalCapacity >= newStorBinCapacityUsed) //if stor bin is enough
				{
					StorBinUtilization = (newStorBinCapacityUsed/StorBinTotalCapacity)*100;
					
					StorBin = new model.mdlStorageBin();
					StorBin = StorageBinAdapter.LoadStorageBinDetailForPlacement(lParam);
					int StorBinNoOfQty = StorBin.getNoOfQuantity()+1;
					int StorBinNoStorUnit = StorBin.getNoStorageUnits()+1;
					
					//update Storage Bin process parameter
					pstmUpdateStorageBin.setDouble(1,  StorBinUtilization);
					pstmUpdateStorageBin.setDouble(2,  newStorBinCapacityUsed);
					pstmUpdateStorageBin.setInt(3,  StorBinNoOfQty);
					pstmUpdateStorageBin.setInt(4,  StorBinNoStorUnit);
					pstmUpdateStorageBin.setString(5,  lParam.getPlantID()); 
					pstmUpdateStorageBin.setString(6,  lParam.getWarehouseID());
					pstmUpdateStorageBin.setString(7,  lParam.getStorageTypeID());
					pstmUpdateStorageBin.setString(8,  lParam.getDestinationStorageBin());
					pstmUpdateStorageBin.setString(9,  lParam.getStorageSectionID());
					Globals.gCommand = pstmUpdateStorageBin.toString();
					pstmUpdateStorageBin.executeUpdate();
					
					//update Inbound Status process parameter
					//check transfer order status
					Boolean Check = CheckTransferOrderStatus(lParam);
					if(Check){
						pstmUpdateInboundStatus.setString(1,  Globals.user);
						pstmUpdateInboundStatus.setString(2,  LocalDateTime.now().toString());
						pstmUpdateInboundStatus.setString(3,  lParam.getInboundDocID());
						Globals.gCommand = pstmUpdateInboundStatus.toString();
						pstmUpdateInboundStatus.executeUpdate();
						
						//update IM
						if(lParam.getWMS_Status() && lParam.getUpdateWMFirst()){
							List<model.mdlInbound> listInboundDetail = new ArrayList<model.mdlInbound>();
							listInboundDetail = InboundAdapter.LoadInboundDetail(lParam.getInboundDocID(), Globals.user);
							for(model.mdlInbound inbounddetail : listInboundDetail){
								model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
								mdlStockSummary.setPeriod(inbounddetail.getDate().substring(0, 7).replace("-", ""));
								mdlStockSummary.setProductID(inbounddetail.getProductID());
								mdlStockSummary.setPlantID(inbounddetail.getPlantID());
								mdlStockSummary.setWarehouseID(inbounddetail.getWarehouseID());
								mdlStockSummary.setBatch_No(inbounddetail.getBatchNo());
								
								//Check Product Last Stock Summary Quantity by period,product,plant,warehouse and batch no
								String LastProductQty = StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,Globals.user).getQty();
								
								//insert stock summary process parameter
								if (LastProductQty == null)
								{
									LastProductQty = "0";
								}
								
								int intLastProductQty = Integer.parseInt(LastProductQty);
								int intnewProductQty = Integer.parseInt(inbounddetail.getQtyBaseUom());
								int totalNewProductQty = intLastProductQty+intnewProductQty;
								
								pstmInsertUpdateStockSummary.setString(1, ValidateNull.NulltoStringEmpty(mdlStockSummary.getPeriod()));
								pstmInsertUpdateStockSummary.setString(2, ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
								pstmInsertUpdateStockSummary.setString(3, ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
								pstmInsertUpdateStockSummary.setString(4, ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
								pstmInsertUpdateStockSummary.setString(5, ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
								pstmInsertUpdateStockSummary.setInt(6, totalNewProductQty);
								pstmInsertUpdateStockSummary.setString(7, ValidateNull.NulltoStringEmpty(inbounddetail.getBaseUom()));
								pstmInsertUpdateStockSummary.setString(8, ValidateNull.NulltoStringEmpty(Globals.user));
								pstmInsertUpdateStockSummary.setString(9, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								pstmInsertUpdateStockSummary.setString(10, ValidateNull.NulltoStringEmpty(Globals.user));
								pstmInsertUpdateStockSummary.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								pstmInsertUpdateStockSummary.setInt(12, totalNewProductQty);
								pstmInsertUpdateStockSummary.setString(13, ValidateNull.NulltoStringEmpty(inbounddetail.getBaseUom()));
								pstmInsertUpdateStockSummary.setString(14, ValidateNull.NulltoStringEmpty(Globals.user));
								pstmInsertUpdateStockSummary.setString(15, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								Globals.gCommand = pstmInsertUpdateStockSummary.toString();
								pstmInsertUpdateStockSummary.executeUpdate();
							}
						}
					}
					
					connection.commit(); //commit transaction if all of the proccess is running well
					
					Globals.gReturn_Status = "Success Confirm TO Line";
				}
				else{//if stor bin is not enough
					connection.rollback();
					
					 LogAdapter.InsertLogExc("Storage Bin Doesnt Have Enough Space", "ConfirmTO", "ConfirmTO" , Globals.user);
		    		 Globals.gReturn_Status = "Storage bin tidak cukup";
				}
			}
			
			catch (Exception e) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionConfirmTO", Globals.gCommand , Globals.user);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "ConfirmTO", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
			        if (pstmUpdateTODetailStatus != null) {
			        	pstmUpdateTODetailStatus.close();
			        }
			        if (pstmUpdateStorageBin != null) {
			        	pstmUpdateStorageBin.close();
			        }
			        if (pstmInsertUpdateStorageBinDetail != null) {
			        	pstmInsertUpdateStorageBinDetail.close();
			        }
			        if (pstmUpdateInboundStatus != null) {
			        	pstmUpdateInboundStatus.close();
			        }
			        if (pstmInsertUpdateStockSummary != null) {
			        	pstmInsertUpdateStockSummary.close();
			        }
			        connection.setAutoCommit(true);
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "ConfirmTO", "close opened connection protocol", Globals.user);
				}
		    }
			
		return Globals.gReturn_Status;
	}
	
}
