package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlClosingPeriod;
import model.mdlPlant;

public class ClosingPeriodAdapter {

	@SuppressWarnings("resource")
	public static model.mdlClosingPeriod LoadActivePeriod (String lUser) {
		model.mdlClosingPeriod mdlClosingPeriod = new model.mdlClosingPeriod();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Period,Fiscal_Year,IsActive,Created_by,Created_at FROM closing_period WHERE IsActive=1 ORDER BY Created_at DESC LIMIT 1");
			//jrs.setString(1, "1");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				//model.mdlInbound mdlInboundDetail = new model.mdlInbound();
				mdlClosingPeriod.setPeriod(jrs.getString("Period"));
				mdlClosingPeriod.setFiscal_Year(jrs.getString("Fiscal_Year"));
				mdlClosingPeriod.setIsActive(jrs.getString("IsActive"));
				mdlClosingPeriod.setCreated_by(jrs.getString("Created_by"));
				mdlClosingPeriod.setCreated_at(jrs.getString("Created_at"));
				//mdlClosingPeriod.set(jrs.getString("Qty_UOM"));
				//mdlClosingPeriod.setUom(jrs.getString("UOM"));
				
				//listmdlInboundDetail.add(mdlInboundDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadActivatePeriod", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadActivatePeriod", "close opened connection protocol", lUser);
			}
		}
		
		return mdlClosingPeriod;
	}

	@SuppressWarnings("resource")
	public static model.mdlClosingPeriod LoadPeriodByKey(String lPeriod) {
		model.mdlClosingPeriod mdlClosingPeriod = new model.mdlClosingPeriod();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Period,Fiscal_Year,IsActive,Created_by,Created_at FROM closing_period WHERE Period=? ORDER BY Created_at DESC LIMIT 1");
			jrs.setString(1, lPeriod);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				//model.mdlInbound mdlInboundDetail = new model.mdlInbound();
				mdlClosingPeriod.setPeriod(jrs.getString("Period"));
				mdlClosingPeriod.setFiscal_Year(jrs.getString("Fiscal_Year"));
				mdlClosingPeriod.setIsActive(jrs.getString("IsActive"));
				mdlClosingPeriod.setCreated_by(jrs.getString("Created_by"));
				mdlClosingPeriod.setCreated_at(jrs.getString("Created_at"));
				//mdlClosingPeriod.set(jrs.getString("Qty_UOM"));
				//mdlClosingPeriod.setUom(jrs.getString("UOM"));
				
				//listmdlInboundDetail.add(mdlInboundDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadPeriodByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadPeriodByKey", "close opened connection protocol", Globals.user);
			}
		}
		
		return mdlClosingPeriod;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlClosingPeriod> LoadTransactionPeriod (String lUser) {
		List<model.mdlClosingPeriod> mdlClosingPeriodList = new ArrayList<model.mdlClosingPeriod>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Period,Fiscal_Year,IsActive,Created_by,Created_at FROM closing_period ORDER BY Created_at DESC");
			//jrs.setString(1, "1");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlClosingPeriod mdlClosingPeriod = new model.mdlClosingPeriod();
				mdlClosingPeriod.setPeriod(jrs.getString("Period"));
				mdlClosingPeriod.setFiscal_Year(jrs.getString("Fiscal_Year").substring(0,4));
				
				if(jrs.getString("IsActive").contentEquals("0"))
					mdlClosingPeriod.setIsActive("Nonactive");
				else
					mdlClosingPeriod.setIsActive("Active");
				//mdlClosingPeriod.set(jrs.getString("Qty_UOM"));
				//mdlClosingPeriod.setUom(jrs.getString("UOM"));
				
				mdlClosingPeriodList.add(mdlClosingPeriod);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadTransactionPeriod", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadTransactionPeriod", "close opened connection protocol", lUser);
			}
		}
		
		return mdlClosingPeriodList;
	}

	public static String ClosingPeriod(mdlClosingPeriod lCurrentPeriod, String lUser)
	{
		Globals.gCommand = "Insert Current Period : " + lCurrentPeriod.getPeriod();
		
		try{
			mdlClosingPeriod CheckDuplicatePeriod = LoadPeriodByKey(lCurrentPeriod.getPeriod());
			if(CheckDuplicatePeriod.getPeriod() == null){
				JdbcRowSet jrs = new JdbcRowSetImpl();
				jrs = database.RowSetAdapter.getJDBCRowSet();			
				
				jrs.setCommand("SELECT Period,Fiscal_Year,IsActive,Created_by,Created_at FROM closing_period LIMIT 1");
				jrs.execute();
				
				jrs.moveToInsertRow();
				jrs.updateString("Period",lCurrentPeriod.getPeriod());
				jrs.updateString("Fiscal_Year", lCurrentPeriod.getFiscal_Year());
				jrs.updateBoolean("IsActive", true);
				jrs.updateString("Created_by", lUser);
				jrs.updateString("Created_at", LocalDateTime.now().toString());
				
				jrs.insertRow();
				
				jrs.close();
				Globals.gReturn_Status = "Success Closing Period";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Periode sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ClosingPeriod", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Closing Period";
			Globals.gReturn_Status = "Database Error";

		}
		
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String InactivatePeriod(mdlClosingPeriod lCurrentPeriod, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Period, Fiscal_Year, IsActive, Created_by, Created_at FROM closing_period WHERE Period!=?");
			Globals.gCommand = jrs.getCommand();
			jrs.setString(1,  lCurrentPeriod.getPeriod());
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateBoolean("IsActive",false);
			jrs.updateRow();
			
			Globals.gReturn_Status = "Success Inactivate Period";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InactivatePeriod", Globals.gCommand , lUser);
			Globals.gReturn_Status = "Error Inactivate Period";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InactivatePeriod", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
}
