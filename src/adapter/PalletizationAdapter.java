package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import model.Globals;

public class PalletizationAdapter {
	
	public static List<model.mdlPalletization> LoadProductPalletization (model.mdlPalletization lParam) {
		List<model.mdlPalletization> listProductPalletization = new ArrayList<model.mdlPalletization>();
		//JdbcRowSet jrs = database.RowSetAdapter.getJDBCRowSet();
		Globals.gCommand = "";
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			JdbcRowSet jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT LEQty1,UOMLEQty1,StorageUnitTypeIDLEQty1, "
					+ "LEQty2,UOMLEQty2,StorageUnitTypeIDLEQty2,"
					+ "LEQty3,UOMLEQty3,StorageUnitTypeIDLEQty3 "
					+ "FROM wms_product "
					+ "WHERE ProductID=? AND PlantID=? AND WarehouseID=?");
			jrs.setString(1, lParam.getProductID());
			jrs.setString(2, lParam.getPlantID());
			jrs.setString(3, lParam.getWarehouseID());
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlPalletization productPalletization = new model.mdlPalletization();
				
				if(lParam.getProductUOM().contentEquals(jrs.getString("UOMLEQty1")))
					productPalletization.setQtyPerStorageUnit1(jrs.getInt("LEQty1"));
				else if(lParam.getProductUOM().contentEquals(jrs.getString("UOMLEQty2")))
					productPalletization.setQtyPerStorageUnit1(jrs.getInt("LEQty2"));
				else if(lParam.getProductUOM().contentEquals(jrs.getString("UOMLEQty3")))
					productPalletization.setQtyPerStorageUnit1(jrs.getInt("LEQty3"));
				else
					break;
				
				productPalletization.setQtyPerStorageUnit2(lParam.getProductQty()%productPalletization.getQtyPerStorageUnit1());
				productPalletization.setStorageUnitType1(jrs.getString("StorageUnitTypeIDLEQty1"));
				//productPalletization.setStorageUnitType2(jrs.getString("StorageUnitTypeIDLEQty2"));
				productPalletization.setStorageUnitType2("PP");
				productPalletization.setStockPlacementQty(lParam.getProductQty());
				productPalletization.setStockPlacementUOM(lParam.getProductUOM());
				productPalletization.setStorageUnit1(lParam.getProductQty()/productPalletization.getQtyPerStorageUnit1());
				productPalletization.setStorageUnit2(1);
				if(productPalletization.getQtyPerStorageUnit2() == 0)
					productPalletization.setStorageUnit2(0);
				productPalletization.setTotalTransferOrder( (productPalletization.getStorageUnit1() * productPalletization.getQtyPerStorageUnit1()) + (productPalletization.getStorageUnit2() * productPalletization.getQtyPerStorageUnit2()) );
				productPalletization.setOpenQty(productPalletization.getStockPlacementQty()-productPalletization.getTotalTransferOrder());
				
				listProductPalletization.add(productPalletization);
			}
			
			jrs.close();
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProductPalletization", Globals.gCommand , Globals.user);
			listProductPalletization = new ArrayList<model.mdlPalletization>();
		}
		//		finally{
		//			try{
		//				if(jrs!=null)
		//					jrs.close();
		//			}
		//			catch(Exception e){
		//				LogAdapter.InsertLogExc(e.toString(), "LoadProductPalletization", "close opened connection protocol", Globals.user);
		//				listProductPalletization = new ArrayList<model.mdlPalletization>();
		//			}
		//		}
		
		return listProductPalletization;
	}
	
}
