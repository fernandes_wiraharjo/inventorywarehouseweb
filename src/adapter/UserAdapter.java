package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import model.Globals;

public class UserAdapter {

	public static List<model.mdlUser> LoadUser(String lUserArea){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Globals.gCommand = "";
		List<model.mdlUser> mdlUserList = new ArrayList<model.mdlUser>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("SELECT `UserId`,`Password`,`PlantId`,`RoleId` "
					+ "FROM  user_login "
					+ "WHERE PlantId LIKE "+lUserArea+"");
			Globals.gCommand = pstm.toString();
			
			//insert sql parameter
			//			pstm.setString(1,  Globals.user_area2);
			//			pstm.setString(2,  Globals.user_brand2);
			//			pstm.addBatch();
			//			pstm.executeBatch();
			
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				model.mdlUser mdlUser = new model.mdlUser();				
				mdlUser.setUserId(rs.getString("UserId"));
				mdlUser.setPassword(rs.getString("Password"));
				mdlUser.setPlant(rs.getString("PlantId"));
				mdlUser.setRole(rs.getString("RoleId"));
				mdlUserList.add(mdlUser);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUser", Globals.gCommand, Globals.user);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadUser", "close opened connection protocol", Globals.user);
			}
		}

		return mdlUserList;
	}
	
	public static String InsertUser(model.mdlUser lParam)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		PreparedStatement pstmCheck = null;
		//ResultSet rs = null;
		ResultSet rsCheck = null;
		Boolean check = false;
		Globals.gCommand = "";
		try{
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//check duplicate user id
			String sqlCheck = "SELECT UserId,PlantId "
					+ "FROM user_login "
					+ "WHERE UserId = ?";
			pstmCheck = connection.prepareStatement(sqlCheck);
			
			//insert sql parameter and execute for check
			pstmCheck.setString(1,  lParam.getUserId());
			rsCheck = pstmCheck.executeQuery();
			Globals.gCommand = pstmCheck.toString();
			
			while(rsCheck.next()){
				check = true;
			}
				//if no duplicate
				if(check==false)
				{
					pstm = connection.prepareStatement("INSERT INTO user_login VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
					pstm.setString(1, ValidateNull.NulltoStringEmpty(lParam.getUserId()));
					pstm.setString(2, ValidateNull.NulltoStringEmpty(Base64Adapter.EncriptBase64(lParam.getPassword())));
					pstm.setString(3, ValidateNull.NulltoStringEmpty(lParam.getPlant()));
					pstm.setString(4, ValidateNull.NulltoStringEmpty(lParam.getRole()));
					pstm.setString(5, "");
					pstm.setString(6, "");
					pstm.setString(7, Globals.user);
					pstm.setString(8, Globals.user);
					pstm.setString(9, LocalDateTime.now().toString());
					pstm.setString(10, LocalDateTime.now().toString());
					pstm.setString(11, "0000-00-00 00:00:00");
					pstm.setString(12, "");

					Globals.gCommand = pstm.toString();
					pstm.executeUpdate();
					Globals.gReturn_Status = "Success Insert User";
				}
				//if duplicate
				else {
					Globals.gReturn_Status = "The User Id Already Exist";
				}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertUser", Globals.gCommand, Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (pstmCheck != null) {
					 pstmCheck.close();
				 }
				if (connection != null) {
					connection.close();
				}
				//				if (rs != null) {
				//					rs.close();
				//				}
				 if (rsCheck != null) {
					 rsCheck.close();
				 }
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertUser", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}

	public static String UpdateUser(model.mdlUser lParam)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		//ResultSet rs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("UPDATE user_login SET `Password`=?,PlantId=?,RoleId=?,LastUpdateBy=?,LastDate=? WHERE UserId=?");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(Base64Adapter.EncriptBase64(lParam.getPassword())));
			pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getPlant()));
			pstm.setString(3, ValidateNull.NulltoStringEmpty(lParam.getRole()));
			pstm.setString(4, ValidateNull.NulltoStringEmpty(Globals.user));
			pstm.setString(5, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstm.setString(6, ValidateNull.NulltoStringEmpty(lParam.getUserId()));
		
			Globals.gCommand = pstm.toString();
			pstm.executeUpdate();
			Globals.gReturn_Status = "Success Update User";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateUser", Globals.gCommand, Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				//				if (rs != null) {
				//					rs.close();
				//				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "UpdateUser", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;	
	}

	public static String DeleteUser(String lUserId){
		Connection connection = null;
		PreparedStatement pstm = null;
		//ResultSet rs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("DELETE FROM user_login WHERE UserId = ?");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lUserId));
			
			Globals.gReturn_Status = "Success Delete User";
			Globals.gCommand = pstm.toString();
			pstm.executeUpdate();	
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteUser", Globals.gCommand, Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				//				if (rs != null) {
				//					rs.close();
				//				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteUser", "close opened connection protocol", Globals.user);
			}
		}
		
		return Globals.gReturn_Status;	
	}
	
	public static String LoadUserArea(String lUserID){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Globals.gCommand = "";
		String PlantID = "";
		
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("SELECT UserId,PlantId FROM user_login WHERE UserId = ?");
			
			//insert sql parameter
			pstm.setString(1,  lUserID);
			Globals.gCommand = pstm.toString();
			//pstm.addBatch();
			//pstm.executeBatch();
			
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				PlantID = rs.getString("PlantId");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUserArea", Globals.gCommand, lUserID);
		}
		finally{
			//close the opened connection
			try{
					if (pstm != null) {
						pstm.close();
					}
					if (connection != null) {
						connection.close();
					}
					if (rs != null) {
						rs.close();
					}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadUserArea", "close opened connection protocol", lUserID);
			}
		}

		return PlantID;
	}

	public static List<model.mdlMenu> LoadRestrictedMenu(String lUserID){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Globals.gCommand = "";
		List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("SELECT a.MenuID FROM access_role a "
					+ "INNER JOIN user_login b ON b.RoleId=a.RoleID "
					+ "WHERE a.IsAccess=0 AND b.UserId=?");
			
			//insert sql parameter
			pstm.setString(1,  lUserID);
			Globals.gCommand = pstm.toString();
			//pstm.addBatch();
			//pstm.executeBatch();
			
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				model.mdlMenu mdlMenu = new model.mdlMenu();				
				mdlMenu.setMenuID(rs.getString("MenuID"));
				mdlMenuList.add(mdlMenu);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadRestrictedMenu", Globals.gCommand, Globals.user);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadRestrictedMenu", "close opened connection protocol", Globals.user);
			}
		}

		return mdlMenuList;
	}
	
}
