package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlProductUom;

/** Documentation
 * 001 Nanda -- Beginning
 */
public class ProductUomAdapter {

	public static List<model.mdlProductUom> LoadProductUOM() {
		List<model.mdlProductUom> listmdlProductUom = new ArrayList<model.mdlProductUom>();
		
		Connection connection = null;
		PreparedStatement pstmLoadProductUOM = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadProductUOM = "SELECT a.ProductID, b.Title_EN, a.UOM, a.BaseUOM, a.Qty "
					+ "FROM product_uom a "
					+ "INNER JOIN product b ON b.ID = a.ProductID";
			
			pstmLoadProductUOM = connection.prepareStatement(sqlLoadProductUOM);
			Globals.gCommand = pstmLoadProductUOM.toString();
			
			jrs = pstmLoadProductUOM.executeQuery();
									
			while(jrs.next()){
				model.mdlProductUom mdlProductUOM = new model.mdlProductUom();
				
				mdlProductUOM.setProductID(jrs.getString("ProductID"));
				mdlProductUOM.setProductName(jrs.getString("Title_EN"));
				mdlProductUOM.setUOM(jrs.getString("UOM"));
				mdlProductUOM.setBaseUOM(jrs.getString("BaseUOM"));
				mdlProductUOM.setQty(jrs.getString("Qty"));
				
				listmdlProductUom.add(mdlProductUOM);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProductUOM", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstmLoadProductUOM != null) {
					 pstmLoadProductUOM.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadProductUOM", "close opened connection protocol", Globals.user);
			}
		 }

		return listmdlProductUom;
	}

	public static String InsertProductUom(model.mdlProductUom lParam)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			mdlProductUom CheckProductUOM = LoadProductUOMByKey(lParam.getProductID(), lParam.getUOM());
			if(CheckProductUOM.getBaseUOM() == null){
				connection = database.RowSetAdapter.getConnection();
				
				String sql = "INSERT INTO `product_uom`(`ProductID`, `UOM`, `BaseUOM`, `Qty`) VALUES (?,?,?,?)";
				pstm = connection.prepareStatement(sql);
				
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getProductID()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getUOM()));
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getQty()));
				
				pstm.addBatch();
				Globals.gCommand = pstm.toString();
					
				pstm.executeBatch();
				
				Globals.gReturn_Status = "Success Insert Product UOM";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Konversi produk sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertProductUOM", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertProductUOM", "close opened connection protocol", Globals.user);
			}
		 }
		
		return Globals.gReturn_Status;
	}

	public static String UpdateProductUom(model.mdlProductUom lParam)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "UPDATE `product_uom` SET `BaseUOM`=?,`Qty`=? "
					+ "WHERE `ProductID`=? AND `UOM`=?";
			pstm = connection.prepareStatement(sql);
			
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getQty()));
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getProductID()));
				pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getUOM()));
				
				pstm.addBatch();
				Globals.gCommand = pstm.toString();
				
			pstm.executeBatch();
			
			Globals.gReturn_Status = "Success Update Product UOM";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateProductUOM", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateProductUOM", "close opened connection protocol", Globals.user);
			}
		 }
		
		return Globals.gReturn_Status;
	}
	
	public static String InsertProductUomlist(List<model.mdlProductUom> lParamlist)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			Gson gson = new Gson();
			Globals.gCommand = gson.toJson(lParamlist);
			
			String sql = "INSERT INTO `product_uom`(`ProductID`, `UOM`, `BaseUOM`, `Qty`) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE `BaseUOM`=?,`Qty`=?";
			pstm = connection.prepareStatement(sql);
			
			for(model.mdlProductUom lParam : lParamlist)
			{	
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getProductID()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getUOM()));
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getQty()));
				pstm.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				pstm.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getQty()));
				
				pstm.addBatch();	
			}
			Globals.gCommand = pstm.toString();
			pstm.executeBatch();
			
			Globals.gReturn_Status = "Success Insert list Product Uom API";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertProductUomlist", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert list Product Uom API";
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertProductUomlist", "close opened connection protocol", Globals.user);
			}
		 }
		
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static model.mdlProductUom LoadProductUOMByKey(String lProductID, String lUOM) {
		model.mdlProductUom mdlProductUom = new model.mdlProductUom();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand="";
		//String QtyConversion="";
		try{
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT BaseUOM,Qty FROM product_uom WHERE ProductID=? AND UOM=? LIMIT 1");
			jrs.setString(1,  lProductID);
			jrs.setString(2,  lUOM);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				mdlProductUom.setBaseUOM(jrs.getString("BaseUOM"));
				mdlProductUom.setQty(jrs.getString("Qty"));
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProductUOMByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "LoadProductUOMByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlProductUom;
	}

	public static String LoadMasterConversion(String lUOM) {
//		model.mdlProductUom mdlProductUom = new model.mdlProductUom();
		String QtyConversion="";
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Qty FROM product_uom WHERE UOM=? LIMIT 1");
			jrs.setString(1,  lUOM);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				QtyConversion = jrs.getString("Qty");
			}
			
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMasterConversion", Globals.gCommand , Globals.user);
		}

		return QtyConversion;
	}
	
	public static String DeleteProductUom(String ProductID, String UOM) throws Exception
	{	
		String sql = "DELETE FROM `product_uom` WHERE `ProductID`=? AND `UOM`=?";
		Globals.gCommand = sql;
		
		Connection connection = database.RowSetAdapter.getConnection();
		PreparedStatement pstm = connection.prepareStatement(sql);
		try{
				
			pstm.setString(1,  ValidateNull.NulltoStringEmpty(ProductID));
			pstm.setString(2,  ValidateNull.NulltoStringEmpty(UOM));
			
			pstm.addBatch();
				
			pstm.executeBatch();
			
//			pstm.close();
//			connection.close();	
			Globals.gReturn_Status = "Success Delete Product Uom";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteProductUom", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			 if (pstm != null) {
				 pstm.close();
			 }
			 if (connection != null) {
				 connection.close();
			 }
		 }
		
		return Globals.gReturn_Status;
	}
	
}
