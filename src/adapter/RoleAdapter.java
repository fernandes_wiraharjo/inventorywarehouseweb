package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlBOM;
import model.mdlRole;

public class RoleAdapter {

	@SuppressWarnings("resource")
	public static List<model.mdlRole> LoadRole() {
		List<model.mdlRole> listmdlRole = new ArrayList<model.mdlRole>();
		
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `RoleID`, `RoleName` FROM role ORDER BY `RoleID`");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlRole mdlRole = new model.mdlRole();
				mdlRole.setRoleID(jrs.getString("RoleID"));
				mdlRole.setRoleName(jrs.getString("RoleName"));			
				
				listmdlRole.add(mdlRole);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadRole", Globals.gCommand , Globals.user);
		}
		finally
		{
			try{
				if(jrs != null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadRole", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlRole;
	}
	
	public static String InsertRole(mdlRole lParamRole)
	{	
			Connection connection = null;
			
			PreparedStatement pstmInsertRole = null;
			Globals.gCommand = "";
			try{
				connection = database.RowSetAdapter.getConnection();
				
				String sqlInsertRole = "INSERT INTO role(`RoleID`, `RoleName`) VALUES (?,?);";
				pstmInsertRole = connection.prepareStatement(sqlInsertRole);
				
				//insert role parameter
				pstmInsertRole.setString(1,  ValidateNull.NulltoStringEmpty(lParamRole.getRoleID()));
				pstmInsertRole.setString(2,  ValidateNull.NulltoStringEmpty(lParamRole.getRoleName()));
				pstmInsertRole.addBatch();
				Globals.gCommand = pstmInsertRole.toString();
				
				pstmInsertRole.executeBatch();
				
				Globals.gReturn_Status = "Success Insert Role";
			}
			
			catch (Exception e) 
			{
            	LogAdapter.InsertLogExc(e.toString(), "InsertRole", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
		    }
			finally {
				try{
			        if (pstmInsertRole != null) {
			        	pstmInsertRole.close();
			        }
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertRole", "close opened connection protocol", Globals.user);
				}
		    }
			
			return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String LoadDefaultRole() {
		model.mdlRole mdlRole = new model.mdlRole();
		String DefaultRole = "";
		
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `RoleID`, `RoleName` FROM role ORDER BY `RoleID` LIMIT 1");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				mdlRole.setRoleID(jrs.getString("RoleID"));
				mdlRole.setRoleName(jrs.getString("RoleName"));
			}
			DefaultRole = mdlRole.getRoleID();
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDefaultRole", Globals.gCommand , Globals.user);
		}
		finally
		{
			try{
				if(jrs != null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDefaultRole", "close opened connection protocol", Globals.user);
			}
		}

		return DefaultRole;
	}
}
