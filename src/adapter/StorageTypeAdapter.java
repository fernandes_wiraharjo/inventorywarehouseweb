package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Globals;

public class StorageTypeAdapter {

	public static List<model.mdlStorageType> LoadStorageType(){
		List<model.mdlStorageType> listData = new ArrayList<model.mdlStorageType>();
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT a.*, b.PlantName, c.WarehouseName "
					+ "FROM wms_storage_type a "
					+ "INNER JOIN plant b ON b.PlantID=a.PlantID "
					+ "INNER JOIN warehouse c ON c.PlantID=a.PlantID AND c.WarehouseID=a.WarehouseID "
					+ "WHERE a.PlantID = "+Globals.gPlantID+"";
					//+ "WHERE a.PlantID IN ("+Globals.user_area+")";
			
			Globals.gCommand = sql;
			
			pstm = connection.prepareStatement(sql);
			
			jrs = pstm.executeQuery();
									
			while(jrs.next()){
				model.mdlStorageType data = new model.mdlStorageType();
				
				data.setPlantID(jrs.getString("PlantID"));
				data.setPlantName(jrs.getString("PlantName"));
				data.setWarehouseID(jrs.getString("WarehouseID"));
				data.setWarehouseName(jrs.getString("WarehouseName"));
				data.setStorageTypeID(jrs.getString("StorageTypeID"));
				data.setStorageTypeName(jrs.getString("StorageTypeName"));
				data.setSU_mgmt_active(jrs.getBoolean("SU_mgmt_active"));
				data.setStor_type_is_ID_pnt(jrs.getBoolean("Stor_type_is_ID_pnt"));
				data.setStor_type_is_pck_pnt(jrs.getBoolean("Stor_type_is_pck_pnt"));
				data.setPutawayStrategy(jrs.getString("PutawayStrategy"));
				data.setStock_plcmt_req_confirmation(jrs.getBoolean("Stock_plcmt_req_confirmation"));
				data.setDst_bin_ch_during_confirm(jrs.getBoolean("Dst_bin_ch_during_confirm"));
				data.setMixedStorage(jrs.getBoolean("MixedStorage"));
				data.setAddn_to_stock(jrs.getBoolean("Addn_to_stock"));
				data.setRetain_overdeliveries(jrs.getBoolean("Retain_overdeliveries"));
				data.setSUT_check_active(jrs.getBoolean("SUT_check_active"));
				data.setStorage_sec_check_active(jrs.getBoolean("Storage_sec_check_active"));
				data.setBlock_upon_stock_plcmt(jrs.getBoolean("Block_upon_stock_plcmt"));
				data.setAssigned_ID_point_stor_type(jrs.getString("Assigned_ID_point_stor_type"));
				data.setStockRemovalStrategy(jrs.getString("Stock_removal_strategy"));
				data.setStock_rmvl_req_confirmation(jrs.getBoolean("Stock_rmvl_req_confirmation"));
				data.setAllow_negative_stock(jrs.getBoolean("Allow_negative_stock"));
				data.setFull_stk_rmvl_reqmt_act(jrs.getBoolean("Full_stk_rmvl_reqmt_act"));
				data.setReturn_stock_to_same_storage_bin(jrs.getBoolean("Return_stock_to_same_storage_bin"));
				data.setExecute_zero_stock_check(jrs.getBoolean("Execute_zero_stock_check"));
				data.setRound_off_qty(jrs.getBoolean("Round_off_qty"));
				data.setBlock_upon_stock_rmvl(jrs.getBoolean("Block_upon_stock_rmvl"));
				data.setAssigned_pick_point_stor_ty(jrs.getString("Assigned_pick_point_stor_ty"));
				data.setReturn_storage_type(jrs.getString("Return_storage_type"));
				
				listData.add(data);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageType", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstm != null) {
					 pstm.close();
				 }
	
				 if (connection != null) {
					 connection.close();
				 }
				 
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e)
			 {
				 LogAdapter.InsertLogExc(e.toString(), "LoadStorageType", "close opened connection protocol", Globals.user);
			 }
		 }

		return listData;
	}

	public static List<model.mdlPutawayStrategy> LoadPutawayStrategy(){
		List<model.mdlPutawayStrategy> listData = new ArrayList<model.mdlPutawayStrategy>();
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT PutawayStrategy,Description "
					+ "FROM wms_putaway_strategy";
			
			Globals.gCommand = sql;
			
			pstm = connection.prepareStatement(sql);
			
			jrs = pstm.executeQuery();
									
			while(jrs.next()){
				model.mdlPutawayStrategy data = new model.mdlPutawayStrategy();
				
				data.setPutawayStrategy(jrs.getString("PutawayStrategy"));
				data.setDescription(jrs.getString("Description"));
				
				listData.add(data);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadPutawayStrategy", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstm != null) {
					 pstm.close();
				 }
	
				 if (connection != null) {
					 connection.close();
				 }
				 
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e)
			 {
				 LogAdapter.InsertLogExc(e.toString(), "LoadPutawayStrategy", "close opened connection protocol", Globals.user);
			 }
		 }

		return listData;
	}
	
	public static List<model.mdlPickupStrategy> LoadPickupStrategy(){
		List<model.mdlPickupStrategy> listData = new ArrayList<model.mdlPickupStrategy>();
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT StockRemovalStrategy,Description "
					+ "FROM wms_pickup_strategy";
			
			Globals.gCommand = sql;
			
			pstm = connection.prepareStatement(sql);
			
			jrs = pstm.executeQuery();
									
			while(jrs.next()){
				model.mdlPickupStrategy data = new model.mdlPickupStrategy();
				
				data.setStockRemovalStrategy(jrs.getString("StockRemovalStrategy"));
				data.setDescription(jrs.getString("Description"));
				
				listData.add(data);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadPickupStrategy", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstm != null) {
					 pstm.close();
				 }
	
				 if (connection != null) {
					 connection.close();
				 }
				 
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e)
			 {
				 LogAdapter.InsertLogExc(e.toString(), "LoadPickupStrategy", "close opened connection protocol", Globals.user);
			 }
		 }

		return listData;
	}
	
	public static List<model.mdlStorageType> LoadDynamicStorageType(String lPlantID, String lWarehouseID, String lStorageTypeID, String listSeq){
		List<model.mdlStorageType> listData = new ArrayList<model.mdlStorageType>();
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT PlantID,WarehouseID,StorageTypeID,StorageTypeName "
					+ "FROM wms_storage_type "
					+ "WHERE StorageTypeID != ? AND PlantID = ? AND WarehouseID = ? AND StorageTypeID NOT IN ("+listSeq+")";
			
			pstm = connection.prepareStatement(sql);
			pstm.setString(1,  ValidateNull.NulltoStringEmpty(lStorageTypeID));
			pstm.setString(2,  ValidateNull.NulltoStringEmpty(lPlantID));
			pstm.setString(3,  ValidateNull.NulltoStringEmpty(lWarehouseID));
			
			Globals.gCommand = pstm.toString();
			
			jrs = pstm.executeQuery();
									
			while(jrs.next()){
				model.mdlStorageType data = new model.mdlStorageType();
				
				data.setPlantID(jrs.getString("PlantID"));
				data.setWarehouseID(jrs.getString("WarehouseID"));
				data.setStorageTypeID(jrs.getString("StorageTypeID"));
				data.setStorageTypeName(jrs.getString("StorageTypeName"));
				
				listData.add(data);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDynamicStorageType", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstm != null) {
					 pstm.close();
				 }
	
				 if (connection != null) {
					 connection.close();
				 }
				 
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e)
			 {
				 LogAdapter.InsertLogExc(e.toString(), "LoadDynamicStorageType", "close opened connection protocol", Globals.user);
			 }
		 }

		return listData;
	}
	
	public static String InsertStorageType(model.mdlStorageType lParam)
	{
		Connection connection = null;
		PreparedStatement pstmCheck = null;
		PreparedStatement pstm = null;
		ResultSet jrsCheck = null;
		Boolean check = false;
		Globals.gCommand = "";
		
		
		try{
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//check duplicate product id
			String sqlCheck = "SELECT PlantID,WarehouseID,StorageTypeID "
					+ "FROM wms_storage_type "
					+ "WHERE PlantID=? AND WarehouseID=? AND StorageTypeID=?";
			pstmCheck = connection.prepareStatement(sqlCheck);
			
			//insert sql parameter and execute for check
			pstmCheck.setString(1,  lParam.getPlantID());
			pstmCheck.setString(2,  lParam.getWarehouseID());
			pstmCheck.setString(3,  lParam.getStorageTypeID());
			jrsCheck = pstmCheck.executeQuery();
			Globals.gCommand = pstmCheck.toString();
			
			while(jrsCheck.next()){
				check = true;
			}
				//if no duplicate
				if(check==false)
				{			
					pstm = connection.prepareStatement("INSERT INTO wms_storage_type VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
					pstm.setString(1, lParam.getPlantID());
					pstm.setString(2, lParam.getWarehouseID());
					pstm.setString(3, lParam.getStorageTypeID());
					pstm.setString(4, lParam.getStorageTypeName());
					pstm.setBoolean(5, lParam.getSU_mgmt_active());
					pstm.setBoolean(6, lParam.getStor_type_is_ID_pnt());
					pstm.setBoolean(7, lParam.getStor_type_is_pck_pnt());
					pstm.setString(8, lParam.getPutawayStrategy());
					pstm.setBoolean(9, lParam.getStock_plcmt_req_confirmation());
					pstm.setBoolean(10, lParam.getDst_bin_ch_during_confirm());
					pstm.setBoolean(11, lParam.getMixedStorage());
					pstm.setBoolean(12, lParam.getAddn_to_stock());
					pstm.setBoolean(13, lParam.getRetain_overdeliveries());
					pstm.setBoolean(14, lParam.getSUT_check_active());
					pstm.setBoolean(15, lParam.getStorage_sec_check_active());
					pstm.setBoolean(16, lParam.getBlock_upon_stock_plcmt());
					pstm.setString(17, lParam.getAssigned_ID_point_stor_type());
					pstm.setString(18, lParam.getStockRemovalStrategy());
					pstm.setBoolean(19, lParam.getStock_rmvl_req_confirmation());
					pstm.setBoolean(20, lParam.getAllow_negative_stock());
					pstm.setBoolean(21, lParam.getFull_stk_rmvl_reqmt_act());
					pstm.setBoolean(22, lParam.getReturn_stock_to_same_storage_bin());
					pstm.setBoolean(23, lParam.getExecute_zero_stock_check());
					pstm.setBoolean(24, lParam.getRound_off_qty());
					pstm.setBoolean(25, lParam.getBlock_upon_stock_rmvl());
					pstm.setString(26, lParam.getAssigned_pick_point_stor_ty());
					pstm.setString(27, lParam.getReturn_storage_type());

					Globals.gCommand = pstm.toString();
					pstm.executeUpdate();
					
					Globals.gReturn_Status = "Success Insert Storage Type";
				}
				//if duplicate
				else {
					Globals.gReturn_Status = "Koed tipe rak sudah ada";
				}
		}
		catch (Exception e) {
		        LogAdapter.InsertLogExc(e.toString(), "InsertStorageType", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (pstmCheck != null) {
					 pstmCheck.close();
				 }
				connection.setAutoCommit(true);
				if (connection != null) {
					connection.close();
				}
				if (jrsCheck != null) {
					 jrsCheck.close();
				 }
			
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertStorageType", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}

	public static String UpdateStorageType(model.mdlStorageType lParam)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		Globals.gCommand = "";
		
		try{
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			pstm = connection.prepareStatement("UPDATE wms_storage_type SET StorageTypeName=?,SU_mgmt_active=?,Stor_type_is_ID_pnt=?, "
					+ "Stor_type_is_pck_pnt=?,PutawayStrategy=?,Stock_plcmt_req_confirmation=?,Dst_bin_ch_during_confirm=?,MixedStorage=?,Addn_to_stock=?,Retain_overdeliveries=?, "
					+ "SUT_check_active=?,Storage_sec_check_active=?,Block_upon_stock_plcmt=?,Assigned_ID_point_stor_type=?,Stock_removal_strategy=?,Stock_rmvl_req_confirmation=?, "
					+ "Allow_negative_stock=?,Full_stk_rmvl_reqmt_act=?,Return_stock_to_same_storage_bin=?,Execute_zero_stock_check=?,Round_off_qty=?,Block_upon_stock_rmvl=?,Assigned_pick_point_stor_ty=?, "
					+ "Return_storage_type=? "
					+ "WHERE PlantID=? AND WarehouseID=? AND StorageTypeID=?");
			
			pstm.setString(1, lParam.getStorageTypeName());
			pstm.setBoolean(2, lParam.getSU_mgmt_active());
			pstm.setBoolean(3, lParam.getStor_type_is_ID_pnt());
			pstm.setBoolean(4, lParam.getStor_type_is_pck_pnt());
			pstm.setString(5, lParam.getPutawayStrategy());
			pstm.setBoolean(6, lParam.getStock_plcmt_req_confirmation());
			pstm.setBoolean(7, lParam.getDst_bin_ch_during_confirm());
			pstm.setBoolean(8, lParam.getMixedStorage());
			pstm.setBoolean(9, lParam.getAddn_to_stock());
			pstm.setBoolean(10, lParam.getRetain_overdeliveries());
			pstm.setBoolean(11, lParam.getSUT_check_active());
			pstm.setBoolean(12, lParam.getStorage_sec_check_active());
			pstm.setBoolean(13, lParam.getBlock_upon_stock_plcmt());
			pstm.setString(14, lParam.getAssigned_ID_point_stor_type());
			pstm.setString(15, lParam.getStockRemovalStrategy());
			pstm.setBoolean(16, lParam.getStock_rmvl_req_confirmation());
			pstm.setBoolean(17, lParam.getAllow_negative_stock());
			pstm.setBoolean(18, lParam.getFull_stk_rmvl_reqmt_act());
			pstm.setBoolean(19, lParam.getReturn_stock_to_same_storage_bin());
			pstm.setBoolean(20, lParam.getExecute_zero_stock_check());
			pstm.setBoolean(21, lParam.getRound_off_qty());
			pstm.setBoolean(22, lParam.getBlock_upon_stock_rmvl());
			pstm.setString(23, lParam.getAssigned_pick_point_stor_ty());
			pstm.setString(24, lParam.getReturn_storage_type());
			pstm.setString(25, lParam.getPlantID());
			pstm.setString(26, lParam.getWarehouseID());
			pstm.setString(27, lParam.getStorageTypeID());

			Globals.gCommand = pstm.toString();
			pstm.executeUpdate();
			
			Globals.gReturn_Status = "Success Update Storage Type";
		}
		catch (Exception e) {
		        LogAdapter.InsertLogExc(e.toString(), "UpdateStorageType", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				connection.setAutoCommit(true);
				if (connection != null) {
					connection.close();
				}
			
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "UpdateStorageType", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}
	
	public static String DeleteStorageType(String lPlantID, String lWarehouseID, String lStorageTypeID)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		Globals.gCommand = "";
		
		try{
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			pstm = connection.prepareStatement("DELETE FROM wms_storage_type WHERE PlantID=? AND WarehouseID=? AND StorageTypeID=?");
			pstm.setString(1, lPlantID);
			pstm.setString(2, lWarehouseID);
			pstm.setString(3, lStorageTypeID);

			Globals.gCommand = pstm.toString();
			pstm.executeUpdate();
			
			Globals.gReturn_Status = "Success Delete Storage Type";
		}
		catch (Exception e) {
		        LogAdapter.InsertLogExc(e.toString(), "DeleteStorageType", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				connection.setAutoCommit(true);
				if (connection != null) {
					connection.close();
				}
			
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteStorageType", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}
	
	public static model.mdlStorageType LoadStorageTypeByKey(String lStorageTypeID, String lPlantID, String lWarehouseID){
		model.mdlStorageType data = new model.mdlStorageType();
		
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "SELECT PlantID,WarehouseID,StorageTypeID,StorageTypeName,PutawayStrategy,Stock_removal_strategy "
					+ "FROM wms_storage_type "
					+ "WHERE StorageTypeID = ? AND PlantID = ? AND WarehouseID = ?";
			
			pstm = connection.prepareStatement(sql);
			pstm.setString(1,  ValidateNull.NulltoStringEmpty(lStorageTypeID));
			pstm.setString(2,  ValidateNull.NulltoStringEmpty(lPlantID));
			pstm.setString(3,  ValidateNull.NulltoStringEmpty(lWarehouseID));
			
			Globals.gCommand = pstm.toString();
			
			jrs = pstm.executeQuery();
									
			while(jrs.next()){
				data.setPlantID(jrs.getString("PlantID"));
				data.setWarehouseID(jrs.getString("WarehouseID"));
				data.setStorageTypeID(jrs.getString("StorageTypeID"));
				data.setStorageTypeName(jrs.getString("StorageTypeName"));
				data.setPutawayStrategy(jrs.getString("PutawayStrategy"));
				data.setStockRemovalStrategy(jrs.getString("Stock_removal_strategy"));
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageTypeByKey", Globals.gCommand, Globals.user);
		}
		 finally {
			 try {
				 if (pstm != null) {
					 pstm.close();
				 }
	
				 if (connection != null) {
					 connection.close();
				 }
				 
				 if (jrs != null) {
					 jrs.close();
				 }
			 }
			 catch(Exception e)
			 {
				 LogAdapter.InsertLogExc(e.toString(), "LoadStorageTypeByKey", "close opened connection protocol", Globals.user);
			 }
		 }

		return data;
	}
	
}
