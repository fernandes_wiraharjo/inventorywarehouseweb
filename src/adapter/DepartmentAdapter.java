package adapter;

import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.sql.Connection;
import java.sql.PreparedStatement;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import java.lang.reflect.Type;
import adapter.ValidateNull;

public class DepartmentAdapter {
	public static List<model.mdlDepartment> GetDepartmentAPI(String lUser){
	List<model.mdlDepartment> listmdlDepartment = new ArrayList<model.mdlDepartment>();
	model.mdlToken mdlToken = TokenAdapter.GetToken();
	try {
		
		Client client = Client.create();
		
		Globals.gCommand = "http://dev.webarq.info:8081/kenmaster-be/api/department/listing";
		WebResource webResource = client.resource(Globals.gCommand);
		
		Gson gson = new Gson();
		String json = gson.toJson(mdlToken);
		
		ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);		
		
		Globals.gReturn_Status = response.getEntity(String.class);
		
		//Type TypeDepartment = new TypeToken<ArrayList<model.mdlDepartment>>(){}.getClass();
		listmdlDepartment = gson.fromJson(Globals.gReturn_Status, model.mdlDepartmentlist.class);
		
		
	  } catch (Exception ex) {
		LogAdapter.InsertLogExc(ex.toString(), "GetDepartmentAPI", Globals.gCommand , lUser);
//		Globals.gReturn_Status = "Error GetToken";
	  }
	
	return listmdlDepartment;
	}

	public static String InsertDepartmentlist(List<model.mdlDepartment> lParamlist, String lUser)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			Gson gson = new Gson();
			Globals.gCommand = gson.toJson(lParamlist);
			
			String sql = "INSERT INTO department(ID, Code, Title_EN, Title_ID,Slug,Icon,Banner,`Order`,Status,Created_at,Updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE ID=ID";
			pstm = connection.prepareStatement(sql);
			
			for(model.mdlDepartment lParam : lParamlist)
			{	
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getId()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getCode()));
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getTitle_en()));
				pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getTitle_id()));
				pstm.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getSlug()));
				pstm.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getIcon()));
				pstm.setString(7,  ValidateNull.NulltoStringEmpty(lParam.getBanner())); 
				pstm.setString(8,  ValidateNull.NulltoStringEmpty(lParam.getOrder()));
				pstm.setString(9,  ValidateNull.NulltoStringEmpty(lParam.getStatus()));
				pstm.setString(10, ValidateNull.NulltoStringEmpty(lParam.getCreated_at()));
				pstm.setString(11, ValidateNull.NulltoStringEmpty(lParam.getUpdated_at()));
				
				pstm.addBatch();	
			}
			pstm.executeBatch();
			
			Globals.gReturn_Status = "Success Insert list Department API";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertDepartmentbyAPI", Globals.gCommand , lUser);
			Globals.gReturn_Status = "Error Insert list Department API";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertDepartmentbyAPI", "close opened connection protocol", lUser);
			}
		 }
		
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlDepartment> LoadDepartment() {
		List<model.mdlDepartment> listmdlDepartment = new ArrayList<model.mdlDepartment>();
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT ID, Code, Title_EN, Title_ID, Slug, Icon, Banner,Order, Status, CreatedDate, LastDate FROM department");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlDepartment mdlDepartment = new model.mdlDepartment();
				mdlDepartment.setId(jrs.getString("ID"));
				mdlDepartment.setCode(jrs.getString("Code"));
				mdlDepartment.setTitle_en(jrs.getString("Title_EN"));
				mdlDepartment.setTitle_id(jrs.getString("Title_ID"));
				mdlDepartment.setSlug(jrs.getString("Slug"));	
				mdlDepartment.setIcon(jrs.getString("Icon"));
				mdlDepartment.setBanner(jrs.getString("Banner"));
				mdlDepartment.setOrder(jrs.getString("Order"));
				mdlDepartment.setStatus(jrs.getString("Status"));
				mdlDepartment.setCreated_at(jrs.getString("Created_at"));
				mdlDepartment.setUpdated_at(jrs.getString("Updated_at"));
				
				listmdlDepartment.add(mdlDepartment);
			}
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDepartment", Globals.gCommand , "");
		}

		return listmdlDepartment;
	}
	
	
	
	
//------------------------------------------------------------------------------------	
//	public static model.mdlDepartment LoadDepartmentbyID(String lId) {
//		model.mdlDepartment mdlDepartment = new model.mdlDepartment();
//		try{
//			
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();	
//			jrs.setCommand("SELECT ID, Code, Title_EN, Title_ID, Slug, Icon, Banner,Order, Status, Created_at, Updated_at FROM department WHERE ID = ?");
//			jrs.setString(1,  lId);
//			Globals.gCommand = jrs.getCommand();
//			jrs.execute();
//			
//			mdlDepartment.setId(jrs.getString(lId));
//			mdlDepartment.setCode(jrs.getString("Code"));
//			mdlDepartment.setTitle_en(jrs.getString("Title_EN"));
//			mdlDepartment.setTitle_id(jrs.getString("Title_ID"));
//			mdlDepartment.setSlug(jrs.getString("Slug"));	
//			mdlDepartment.setIcon(jrs.getString("Icon"));
//			mdlDepartment.setBanner(jrs.getString("Banner"));
//			mdlDepartment.setOrder(jrs.getString("Order"));
//			mdlDepartment.setStatus(jrs.getString("Status"));
//			mdlDepartment.setCreated_at(jrs.getString("Created_at"));
//			mdlDepartment.setUpdated_at(jrs.getString("Updated_at"));
//			
//			jrs.close();		
//		}
//		catch(Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "LoadDepartmentbyID", Globals.gCommand , "");
//		}
//
//		return mdlDepartment;
//	}
		
//	public static String InsertDeparment(model.mdlDepartment lParam)
//	{
//		Globals.gCommand = "Insert ID : " + lParam.getId();
//		try{
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();			
//			
//			jrs.setCommand("SELECT ID,Code,Title_EN,Title_ID, Slug, Icon, Banner, Order, Status, Created_at, Updated_at FROM vendor LIMIT 1");
//			jrs.execute();
//			
//			jrs.moveToInsertRow();
//			jrs.updateString("ID",lParam.getId());
//			jrs.updateString("Code",lParam.getCode());
//			jrs.updateString("Title_EN",lParam.getTitle_en());
//			jrs.updateString("Title_ID",lParam.getTitle_id());
//			jrs.updateString("Slug",lParam.getSlug());
//			jrs.updateString("Icon",lParam.getIcon());
//			jrs.updateString("Banner",lParam.getBanner());
//			jrs.updateString("Order",lParam.getOrder());
//			jrs.updateString("Status",lParam.getStatus());
//			jrs.updateString("Created_at",lParam.getCreated_at());
//			jrs.updateString("Updated_at",lParam.getUpdated_at());
//			
//			
//			jrs.insertRow();	
//			jrs.close();
//			Globals.gReturn_Status = "Success Insert Department ";
//		}
//		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "InsertDepartment", Globals.gCommand , "");
//			Globals.gReturn_Status = "Error Insert Department" + lParam.getId();
//
//		}
//		return Globals.gReturn_Status;
//	}
//	
//	public static String UpdateDepartment(model.mdlDepartment lParam)
//	{
//		Globals.gCommand = "ID : " + lParam.getId();
//		try{
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			
//			jrs.setCommand("SELECT Code,Title_EN,Title_ID, Slug, Icon, Banner, Order, Status, Created_at, Updated_at WHERE ID = ? LIMIT 1");
//			jrs.setString(1,  lParam.getId());
//			jrs.execute();
//			
//			jrs.last();
//			int rowNum = jrs.getRow();
//			
//			jrs.absolute(rowNum);
//			jrs.updateString("Code",lParam.getCode());
//			jrs.updateString("Title_EN",lParam.getTitle_en());
//			jrs.updateString("Title_ID",lParam.getTitle_id());
//			jrs.updateString("Slug",lParam.getSlug());
//			jrs.updateString("Icon",lParam.getIcon());
//			jrs.updateString("Banner",lParam.getBanner());
//			jrs.updateString("Order",lParam.getOrder());
//			jrs.updateString("Status",lParam.getStatus());
//			jrs.updateString("Created_at",lParam.getCreated_at());
//			jrs.updateString("Updated_at",lParam.getUpdated_at());
//			jrs.updateRow();
//				
//			jrs.close();
//			Globals.gReturn_Status = "Success Update Department ";
//		}	
//		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "UpdateDepartment", Globals.gCommand , "");
//			Globals.gReturn_Status = "Error Update Department";
//		}
//		return Globals.gReturn_Status;
//	}
//	
//	public static model.mdlDepartment SendInsertDepartmentbyAPI(model.mdlParInsDepartment lParam)
//	{
//		model.mdlDepartment mdlDepartment = new model.mdlDepartment();
//		try {
//			Client client = Client.create();
//			
//			Globals.gCommand = "http://dev.webarq.info:8081/kenmaster-be/api/department/create";
//			WebResource webResource = client.resource(Globals.gCommand);
//			
//			Gson gson = new Gson();
//			String json = gson.toJson(lParam);
//			
//			ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);
//			
//			Globals.gReturn_Status = response.getEntity(String.class);
//			
//			mdlDepartment = gson.fromJson(Globals.gReturn_Status, mdlDepartment.getClass());
//		}
//		catch (Exception ex) {
//			LogAdapter.InsertLogExc(ex.toString(), "SendInsertDepartmentbyAPI", Globals.gCommand , "");
////			Globals.gReturn_Status = "Error SendInsertDepartmentbyAPI";
//		}
//		return mdlDepartment;
//	}
//	
//	public static model.mdlDepartment SendUpdateDepartmentbyAPI(model.mdlParUptDepartment lParam)
//	{
//		model.mdlDepartment mdlDepartment = new model.mdlDepartment();
//		try {
//			Client client = Client.create();
//			
//			Globals.gCommand = "dev.webarq.info:8081/kenmaster-be/api/department/update";
//			WebResource webResource = client.resource(Globals.gCommand);
//			
//			Gson gson = new Gson();
//			String json = gson.toJson(lParam);
//			
//			ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);
//			
//			Globals.gReturn_Status = response.getEntity(String.class);
//			
//			mdlDepartment = gson.fromJson(Globals.gReturn_Status, mdlDepartment.getClass());
//		}
//		catch (Exception ex) {
//			LogAdapter.InsertLogExc(ex.toString(), "GetToken", Globals.gCommand , "");
////			Globals.gReturn_Status = "Error SendInsertDepartmentbyAPI";
//		}
//		return mdlDepartment;
//	}
	
}
