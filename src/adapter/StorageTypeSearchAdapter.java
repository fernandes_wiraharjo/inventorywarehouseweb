package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlProductWM;
import model.mdlStorageTypeSearch;
import sun.util.logging.resources.logging;

public class StorageTypeSearchAdapter {

	public static List<model.mdlStorageTypeSearch> LoadStorageTypeSearch() {
		List<model.mdlStorageTypeSearch> listStorageTypeSearch = new ArrayList<model.mdlStorageTypeSearch>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand="";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT a.PlantID,b.PlantName,a.WarehouseID,c.WarehouseName,a.Operation,a.StorageTypeIndicatorID,d.StorageTypeIndicatorName, "
					+ "Sequence1,Sequence2,Sequence3,Sequence4,Sequence5,Sequence6,Sequence7,Sequence8,Sequence9,Sequence10, "
					+ "Sequence11,Sequence12,Sequence13,Sequence14,Sequence15,Sequence16,Sequence17,Sequence18,Sequence19,Sequence20, "
					+ "Sequence21,Sequence22,Sequence23,Sequence24,Sequence25 "
					+ "FROM wms_storagetype_search a "
					+ "INNER JOIN plant b ON b.PlantID=a.PlantID "
					+ "INNER JOIN warehouse c ON c.PlantID=a.PlantID AND c.WarehouseID=a.WarehouseID "
					+ "INNER JOIN wms_storagetypeindicator d ON d.PlantID=a.PlantID AND d.WarehouseID=a.WarehouseID AND d.StorageTypeIndicatorID=a.StorageTypeIndicatorID  "
					+ "WHERE a.PlantID IN (" + Globals.user_area + ")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlStorageTypeSearch StorageTypeSearch = new model.mdlStorageTypeSearch();
				
				StorageTypeSearch.setPlantID(jrs.getString("PlantID"));
				StorageTypeSearch.setPlantName(jrs.getString("PlantName"));
				StorageTypeSearch.setWarehouseID(jrs.getString("WarehouseID"));
				StorageTypeSearch.setWarehouseName(jrs.getString("WarehouseName"));
				StorageTypeSearch.setOperation(jrs.getString("Operation"));
				StorageTypeSearch.setStorageTypeIndicatorID(jrs.getString("StorageTypeIndicatorID"));
				StorageTypeSearch.setStorageTypeIndicatorName(jrs.getString("StorageTypeIndicatorName"));
				StorageTypeSearch.setStorageType1(jrs.getString("Sequence1"));
				StorageTypeSearch.setStorageType2(jrs.getString("Sequence2"));
				StorageTypeSearch.setStorageType3(jrs.getString("Sequence3"));
				StorageTypeSearch.setStorageType4(jrs.getString("Sequence4"));
				StorageTypeSearch.setStorageType5(jrs.getString("Sequence5"));
				StorageTypeSearch.setStorageType6(jrs.getString("Sequence6"));
				StorageTypeSearch.setStorageType7(jrs.getString("Sequence7"));
				StorageTypeSearch.setStorageType8(jrs.getString("Sequence8"));
				StorageTypeSearch.setStorageType9(jrs.getString("Sequence9"));
				StorageTypeSearch.setStorageType10(jrs.getString("Sequence10"));
				StorageTypeSearch.setStorageType11(jrs.getString("Sequence11"));
				StorageTypeSearch.setStorageType12(jrs.getString("Sequence12"));
				StorageTypeSearch.setStorageType13(jrs.getString("Sequence13"));
				StorageTypeSearch.setStorageType14(jrs.getString("Sequence14"));
				StorageTypeSearch.setStorageType15(jrs.getString("Sequence15"));
				StorageTypeSearch.setStorageType16(jrs.getString("Sequence16"));
				StorageTypeSearch.setStorageType17(jrs.getString("Sequence17"));
				StorageTypeSearch.setStorageType18(jrs.getString("Sequence18"));
				StorageTypeSearch.setStorageType19(jrs.getString("Sequence19"));
				StorageTypeSearch.setStorageType20(jrs.getString("Sequence20"));
				StorageTypeSearch.setStorageType21(jrs.getString("Sequence21"));
				StorageTypeSearch.setStorageType22(jrs.getString("Sequence22"));
				StorageTypeSearch.setStorageType23(jrs.getString("Sequence23"));
				StorageTypeSearch.setStorageType24(jrs.getString("Sequence24"));
				StorageTypeSearch.setStorageType25(jrs.getString("Sequence25"));
				
				listStorageTypeSearch.add(StorageTypeSearch);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageTypeSearch", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageTypeSearch", "close opened connection protocol", Globals.user);
			}
		}

		return listStorageTypeSearch;
	}
	
	public static model.mdlStorageTypeSearch LoadStorageTypeSearchByKey(String lPlantID, String lWarehouseID, String lOperation, String lStorageTypeIndicatorID) {
		model.mdlStorageTypeSearch mdlStorageTypeSearch = new model.mdlStorageTypeSearch();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, Operation, StorageTypeIndicatorID FROM wms_storagetype_search "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND Operation = ? AND StorageTypeIndicatorID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lOperation);
			jrs.setString(4,  lStorageTypeIndicatorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlStorageTypeSearch.setPlantID(jrs.getString("PlantID"));
				mdlStorageTypeSearch.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageTypeSearch.setOperation(jrs.getString("Operation"));
				mdlStorageTypeSearch.setStorageTypeIndicatorID(jrs.getString("StorageTypeIndicatorID"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageTypeSearchByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageTypeSearchByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlStorageTypeSearch;
	}
	
	public static String DeleteStorageTypeSearch(String lPlantID, String lWarehouseID, String lOperation, String lStorageTypeIndicatorID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, Operation, StorageTypeIndicatorID "
							+ "FROM wms_storagetype_search WHERE PlantID = ? AND WarehouseID = ? AND Operation = ? AND StorageTypeIndicatorID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lOperation);
			jrs.setString(4, lStorageTypeIndicatorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Storage Type Search";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteStorageTypeSearch", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteStorageTypeSearch", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	public static String InsertStorageTypeSearch(model.mdlStorageTypeSearch lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlStorageTypeSearch Check = LoadStorageTypeSearchByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getOperation(), lParam.getStorageTypeIndicatorID());
			if(Check.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, Operation, StorageTypeIndicatorID, Sequence1, Sequence2, "
						+ "Sequence3, Sequence4, Sequence5, Sequence6, "
						+ "Sequence7, Sequence8, Sequence9, Sequence10, Sequence11, Sequence12, "
						+ "Sequence13, Sequence14, Sequence15, Sequence16, "
						+ "Sequence17, Sequence18, Sequence19,Sequence20, Sequence21, Sequence22, "
						+ "Sequence23, Sequence24, Sequence25 "
						+ "FROM wms_storagetype_search LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("Operation",lParam.getOperation());
				jrs.updateString("StorageTypeIndicatorID",lParam.getStorageTypeIndicatorID());
				jrs.updateString("Sequence1",lParam.getStorageType1());
				jrs.updateString("Sequence2",lParam.getStorageType2());
				jrs.updateString("Sequence3",lParam.getStorageType3());
				jrs.updateString("Sequence4",lParam.getStorageType4());
				jrs.updateString("Sequence5",lParam.getStorageType5());
				jrs.updateString("Sequence6",lParam.getStorageType6());
				jrs.updateString("Sequence7",lParam.getStorageType7());
				jrs.updateString("Sequence8",lParam.getStorageType8());
				jrs.updateString("Sequence9",lParam.getStorageType9());
				jrs.updateString("Sequence10",lParam.getStorageType10());
				jrs.updateString("Sequence11",lParam.getStorageType11());
				jrs.updateString("Sequence12",lParam.getStorageType12());
				jrs.updateString("Sequence13",lParam.getStorageType13());
				jrs.updateString("Sequence14",lParam.getStorageType14());
				jrs.updateString("Sequence15",lParam.getStorageType15());
				jrs.updateString("Sequence16",lParam.getStorageType16());
				jrs.updateString("Sequence17",lParam.getStorageType17());
				jrs.updateString("Sequence18",lParam.getStorageType18());
				jrs.updateString("Sequence19",lParam.getStorageType19());
				jrs.updateString("Sequence20",lParam.getStorageType20());
				jrs.updateString("Sequence21",lParam.getStorageType21());
				jrs.updateString("Sequence22",lParam.getStorageType22());
				jrs.updateString("Sequence23",lParam.getStorageType23());
				jrs.updateString("Sequence24",lParam.getStorageType24());
				jrs.updateString("Sequence25",lParam.getStorageType25());
				
				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Storage Type Search";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Storage type search sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertStorageTypeSearch", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertStorageTypeSearch", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
	public static String UpdateStorageTypeSearch(model.mdlStorageTypeSearch lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID, WarehouseID, Operation, StorageTypeIndicatorID, Sequence1, Sequence2, "
						+ "Sequence3, Sequence4, Sequence5, Sequence6, "
						+ "Sequence7, Sequence8, Sequence9, Sequence10, Sequence11, Sequence12, "
						+ "Sequence13, Sequence14, Sequence15, Sequence16, "
						+ "Sequence17, Sequence18, Sequence19,Sequence20, Sequence21, Sequence22, "
						+ "Sequence23, Sequence24, Sequence25 "
						+ "FROM wms_storagetype_search WHERE PlantID = ? AND WarehouseID = ? AND Operation = ? AND StorageTypeIndicatorID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getOperation());
			jrs.setString(4,  lParam.getStorageTypeIndicatorID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("Sequence1", lParam.getStorageType1());
			jrs.updateString("Sequence2", lParam.getStorageType2());
			jrs.updateString("Sequence3", lParam.getStorageType3());
			jrs.updateString("Sequence4", lParam.getStorageType4());
			jrs.updateString("Sequence5", lParam.getStorageType5());
			jrs.updateString("Sequence6", lParam.getStorageType6());
			jrs.updateString("Sequence7", lParam.getStorageType7());
			jrs.updateString("Sequence8", lParam.getStorageType8());
			jrs.updateString("Sequence9", lParam.getStorageType9());
			jrs.updateString("Sequence10", lParam.getStorageType10());
			jrs.updateString("Sequence11", lParam.getStorageType11());
			jrs.updateString("Sequence12", lParam.getStorageType12());
			jrs.updateString("Sequence13", lParam.getStorageType13());
			jrs.updateString("Sequence14", lParam.getStorageType14());
			jrs.updateString("Sequence15", lParam.getStorageType15());
			jrs.updateString("Sequence16", lParam.getStorageType16());
			jrs.updateString("Sequence17", lParam.getStorageType17());
			jrs.updateString("Sequence18", lParam.getStorageType18());
			jrs.updateString("Sequence19", lParam.getStorageType19());
			jrs.updateString("Sequence20", lParam.getStorageType20());
			jrs.updateString("Sequence21", lParam.getStorageType21());
			jrs.updateString("Sequence22", lParam.getStorageType22());
			jrs.updateString("Sequence23", lParam.getStorageType23());
			jrs.updateString("Sequence24", lParam.getStorageType24());
			jrs.updateString("Sequence25", lParam.getStorageType25());
			
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Storage Type Search";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStorageTypeSearch", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateStorageTypeSearch", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
}
