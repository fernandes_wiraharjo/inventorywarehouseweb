package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlStorageTypeIndicator;

public class StorageTypeIndicatorAdapter {

	@SuppressWarnings("resource")
	public static model.mdlStorageTypeIndicator LoadStorageTypeIndicatorByKey(String lPlantID, String lWarehouseID, String lStorageTypeIndicatorID) {
		model.mdlStorageTypeIndicator mdlStorageTypeIndicator = new model.mdlStorageTypeIndicator();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeIndicatorID, StorageTypeIndicatorName FROM wms_storagetypeindicator "
							+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageTypeIndicatorID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lStorageTypeIndicatorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlStorageTypeIndicator.setPlantID(jrs.getString("PlantID"));
				mdlStorageTypeIndicator.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageTypeIndicator.setStorageTypeIndicatorID(jrs.getString("StorageTypeIndicatorID"));
				mdlStorageTypeIndicator.setStorageTypeIndicatorName(jrs.getString("StorageTypeIndicatorName"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageTypeIndicatorByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageTypeIndicatorByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlStorageTypeIndicator;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlStorageTypeIndicator> LoadStorageTypeIndicatorByPlantWarehouse(String lPlantID, String lWarehouseID) {
		List<model.mdlStorageTypeIndicator> listStorageTypeIndicator = new ArrayList<mdlStorageTypeIndicator>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeIndicatorID, StorageTypeIndicatorName FROM wms_storagetypeindicator "
							+ "WHERE PlantID = ? AND WarehouseID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				model.mdlStorageTypeIndicator mdlStorageTypeIndicator = new model.mdlStorageTypeIndicator();
				
				mdlStorageTypeIndicator.setPlantID(jrs.getString("PlantID"));
				mdlStorageTypeIndicator.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageTypeIndicator.setStorageTypeIndicatorID(jrs.getString("StorageTypeIndicatorID"));
				mdlStorageTypeIndicator.setStorageTypeIndicatorName(jrs.getString("StorageTypeIndicatorName"));
				
				listStorageTypeIndicator.add(mdlStorageTypeIndicator);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageTypeIndicatorByPlantWarehouse", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageTypeIndicatorByPlantWarehouse", "close opened connection protocol", Globals.user);
			}
		}

		return listStorageTypeIndicator;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlStorageTypeIndicator> LoadStorageTypeIndicator() {
		List<model.mdlStorageTypeIndicator> listmdlStorageTypeIndicator = new ArrayList<model.mdlStorageTypeIndicator>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			String sqlLoadStorageTypeIndicator = "SELECT StorageTypeIndicator.PlantID, plant.PlantName, StorageTypeIndicator.WarehouseID, warehouse.WarehouseName, "
					+ "StorageTypeIndicator.StorageTypeIndicatorID, StorageTypeIndicator.StorageTypeIndicatorName "
					+ "FROM wms_storagetypeindicator StorageTypeIndicator "
					+ "LEFT JOIN plant ON plant.PlantID = StorageTypeIndicator.PlantID "
					+ "LEFT JOIN warehouse ON warehouse.WarehouseID = StorageTypeIndicator.WarehouseID "
					+ "AND warehouse.PlantID = StorageTypeIndicator.PlantID "
					+ "WHERE StorageTypeIndicator.PlantID IN (" + Globals.user_area + ")";

			jrs.setCommand(sqlLoadStorageTypeIndicator);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next()){
				model.mdlStorageTypeIndicator mdlStorageTypeIndicator = new model.mdlStorageTypeIndicator();
				mdlStorageTypeIndicator.setPlantID(jrs.getString("PlantID"));
				mdlStorageTypeIndicator.setPlantName(jrs.getString("PlantName"));
				mdlStorageTypeIndicator.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageTypeIndicator.setWarehouseName(jrs.getString("WarehouseName"));
				mdlStorageTypeIndicator.setStorageTypeIndicatorID(jrs.getString("StorageTypeIndicatorID"));
				mdlStorageTypeIndicator.setStorageTypeIndicatorName(jrs.getString("StorageTypeIndicatorName"));

				listmdlStorageTypeIndicator.add(mdlStorageTypeIndicator);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageTypeIndicator", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageTypeIndicator", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlStorageTypeIndicator;
	}

	@SuppressWarnings("resource")
	public static String InsertStorageTypeIndicator(model.mdlStorageTypeIndicator lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlStorageTypeIndicator CheckStorageTypeIndicator = LoadStorageTypeIndicatorByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getStorageTypeIndicatorID());
			if(CheckStorageTypeIndicator.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeIndicatorID, StorageTypeIndicatorName FROM wms_storagetypeindicator LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("StorageTypeIndicatorID",lParam.getStorageTypeIndicatorID());
				jrs.updateString("StorageTypeIndicatorName",lParam.getStorageTypeIndicatorName());

				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Storage Type Indicator";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Storage type indicator sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertStorageTypeIndicator", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertStorageTypeIndicator", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String UpdateStorageTypeIndicator(model.mdlStorageTypeIndicator lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeIndicatorID, StorageTypeIndicatorName FROM wms_storagetypeindicator WHERE PlantID = ? "
							+ "AND WarehouseID = ? AND StorageTypeIndicatorID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getStorageTypeIndicatorID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("StorageTypeIndicatorName", lParam.getStorageTypeIndicatorName());
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Storage Type Indicator";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStorageTypeIndicator", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateStorageTypeIndicator", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeleteStorageTypeIndicator(String lPlantID, String lWarehouseID, String lStorageTypeIndicatorID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeIndicatorID FROM wms_storagetypeindicator WHERE PlantID = ? "
							+ "AND WarehouseID = ? AND StorageTypeIndicatorID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lStorageTypeIndicatorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Storage Type Indicator";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteStorageTypeIndicator", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteStorageTypeIndicator", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
}
