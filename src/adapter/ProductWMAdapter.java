package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlProductWM;
import model.mdlStorageBin;

public class ProductWMAdapter {

	@SuppressWarnings("resource")
	public static List<model.mdlProductWM> LoadProductWM() {
		List<model.mdlProductWM> listProductWM = new ArrayList<model.mdlProductWM>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand="";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT a.ProductID, b.Title_EN, a.PlantID, c.PlantName, a.WarehouseID, d.WarehouseName, a.BaseUOM, a.CapacityUsage, a.CapacityUsageUOM, a.ApprBatchRecReq, a.StorageTypeIndicatorIDRemoval, "
					+ "a.StorageTypeIndicatorIDPlacement, a.StorageSectionIndicatorID, a.StorageBinID, a.LEQty1, a.UOMLEQty1, a.StorageUnitTypeIDLEQty1, a.LEQty2, a.UOMLEQty2, a.StorageUnitTypeIDLEQty2, "
					+ "a.LEQty3, a.UOMLEQty3, a.StorageUnitTypeIDLEQty3 "
					+ "FROM wms_product a "
					+ "INNER JOIN product b ON b.ID=a.ProductID "
					+ "INNER JOIN plant c ON c.PlantID=a.PlantID "
					+ "INNER JOIN warehouse d ON d.WarehouseID=a.WarehouseID AND d.PlantID=a.PlantID "
					+ "WHERE a.PlantID IN (" + Globals.user_area + ")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlProductWM ProductWM = new model.mdlProductWM();
				
				ProductWM.setProductID(jrs.getString("ProductID"));
				ProductWM.setProductName(jrs.getString("Title_EN"));
				ProductWM.setPlantID(jrs.getString("PlantID"));
				ProductWM.setPlantName(jrs.getString("PlantName"));
				ProductWM.setWarehouseID(jrs.getString("WarehouseID"));
				ProductWM.setWarehouseName(jrs.getString("WarehouseName"));
				ProductWM.setBaseUOM(jrs.getString("BaseUOM"));
				ProductWM.setCapacityUsage(jrs.getDouble("CapacityUsage"));
				ProductWM.setCapacityUsageUOM(jrs.getString("CapacityUsageUOM"));
				ProductWM.setApprBatchRecReq(jrs.getBoolean("ApprBatchRecReq"));
				ProductWM.setStockRemoval(jrs.getString("StorageTypeIndicatorIDRemoval"));
				ProductWM.setStockPlacement(jrs.getString("StorageTypeIndicatorIDPlacement"));
				ProductWM.setStorageSectionIndicator(jrs.getString("StorageSectionIndicatorID"));
				ProductWM.setStorageBin(jrs.getString("StorageBinID"));
				ProductWM.setLEQty1(jrs.getDouble("LEQty1"));
				ProductWM.setUOMLEQty1(jrs.getString("UOMLEQty1"));				
				ProductWM.setSUTLEQty1(jrs.getString("StorageUnitTypeIDLEQty1"));
				ProductWM.setLEQty2(jrs.getDouble("LEQty2"));
				ProductWM.setUOMLEQty2(jrs.getString("UOMLEQty2"));
				ProductWM.setSUTLEQty2(jrs.getString("StorageUnitTypeIDLEQty2"));
				ProductWM.setLEQty3(jrs.getDouble("LEQty3"));
				ProductWM.setUOMLEQty3(jrs.getString("UOMLEQty3"));
				ProductWM.setSUTLEQty3(jrs.getString("StorageUnitTypeIDLEQty3"));
				
				listProductWM.add(ProductWM);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProductWM", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadProductWM", "close opened connection protocol", Globals.user);
			}
		}

		return listProductWM;
	}

	public static model.mdlProductWM LoadProductWMByKey(String lPlantID, String lWarehouseID, String lProductID) {
		model.mdlProductWM mdlProductWM = new model.mdlProductWM();
		Globals.gCommand = "";
		try{

			JdbcRowSet jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, ProductID, StorageTypeIndicatorIDPlacement, StorageTypeIndicatorIDRemoval, StorageSectionIndicatorID, "
					+ "CapacityUsage,CapacityUsageUOM,StorageBinID "
					+ "FROM wms_product "
					+ "WHERE PlantID = ? AND WarehouseID = ? AND ProductID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lProductID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlProductWM.setPlantID(jrs.getString("PlantID"));
				mdlProductWM.setWarehouseID(jrs.getString("WarehouseID"));
				mdlProductWM.setProductID(jrs.getString("ProductID"));
				mdlProductWM.setStockPlacement(jrs.getString("StorageTypeIndicatorIDPlacement"));
				mdlProductWM.setStockRemoval(jrs.getString("StorageTypeIndicatorIDRemoval"));
				mdlProductWM.setStorageSectionIndicator(jrs.getString("StorageSectionIndicatorID"));
				mdlProductWM.setCapacityUsage(jrs.getDouble("CapacityUsage"));
				mdlProductWM.setCapacityUsageUOM(jrs.getString("CapacityUsageUOM"));
				mdlProductWM.setStorageBin(jrs.getString("StorageBinID"));
			}
			jrs.close();
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProductWMByKey", Globals.gCommand , Globals.user);
		}

		return mdlProductWM;
	}
	
	public static String DeleteProductWM(String lPlantID, String lWarehouseID, String lProductID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, ProductID "
							+ "FROM wms_product WHERE PlantID = ? AND WarehouseID = ? AND ProductID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lProductID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Product";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteProductWM", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteProductWM", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
	public static String InsertProductWM(model.mdlProductWM lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlProductWM Check = LoadProductWMByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getProductID());
			if(Check.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, ProductID, BaseUOM, CapacityUsage, CapacityUsageUOM, "
						+ "ApprBatchRecReq, StorageTypeIndicatorIDRemoval, StorageTypeIndicatorIDPlacement, StorageSectionIndicatorID,StorageBinID, "
						+ "LEQty1, UOMLEQty1, StorageUnitTypeIDLEQty1,LEQty2, UOMLEQty2, StorageUnitTypeIDLEQty2, LEQty3, UOMLEQty3, StorageUnitTypeIDLEQty3, "
						+ "CreatedBy, CreatedDate, LastUpdateBy, LastDate "
						+ "FROM wms_product LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("ProductID",lParam.getProductID());
				jrs.updateString("BaseUOM",lParam.getBaseUOM());
				jrs.updateDouble("CapacityUsage",lParam.getCapacityUsage());
				jrs.updateString("CapacityUsageUOM",lParam.getCapacityUsageUOM());
				jrs.updateBoolean("ApprBatchRecReq",lParam.getApprBatchRecReq());
				jrs.updateString("StorageTypeIndicatorIDRemoval",lParam.getStockRemoval());
				jrs.updateString("StorageTypeIndicatorIDPlacement",lParam.getStockPlacement());
				jrs.updateString("StorageSectionIndicatorID",lParam.getStorageSectionIndicator());
				jrs.updateString("StorageBinID",lParam.getStorageBin());
				jrs.updateDouble("LEQty1",lParam.getLEQty1());
				jrs.updateDouble("LEQty2",lParam.getLEQty2());
				jrs.updateDouble("LEQty3",lParam.getLEQty3());
				jrs.updateString("UOMLEQty1",lParam.getUOMLEQty1());
				jrs.updateString("UOMLEQty2",lParam.getUOMLEQty2());
				jrs.updateString("UOMLEQty3",lParam.getUOMLEQty3());
				jrs.updateString("StorageUnitTypeIDLEQty1",lParam.getSUTLEQty1());
				jrs.updateString("StorageUnitTypeIDLEQty2",lParam.getSUTLEQty2());
				jrs.updateString("StorageUnitTypeIDLEQty3",lParam.getSUTLEQty3());
				jrs.updateString("CreatedBy",Globals.user);
				jrs.updateString("LastUpdateBy",Globals.user);
				jrs.updateString("CreatedDate",LocalDateTime.now().toString());
				jrs.updateString("LastDate",LocalDateTime.now().toString());

				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Product";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Produk sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertProductWM", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertProductWM", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	public static String UpdateProductWM(model.mdlProductWM lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID,WarehouseID,ProductID, BaseUOM, CapacityUsage, CapacityUsageUOM, "
						+ "ApprBatchRecReq, StorageTypeIndicatorIDRemoval, StorageTypeIndicatorIDPlacement, StorageSectionIndicatorID, StorageBinID, "
						+ "LEQty1, LEQty2, LEQty3, "
						+ "UOMLEQty1, UOMLEQty2, UOMLEQty3, StorageUnitTypeIDLEQty1, StorageUnitTypeIDLEQty2, StorageUnitTypeIDLEQty3, "
						+ "LastUpdateBy, LastDate "
						+ "FROM wms_product WHERE PlantID = ? AND WarehouseID = ? AND ProductID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getProductID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateDouble("CapacityUsage", lParam.getCapacityUsage());
			jrs.updateString("CapacityUsageUOM", lParam.getCapacityUsageUOM());
			jrs.updateBoolean("ApprBatchRecReq", lParam.getApprBatchRecReq());
			jrs.updateString("StorageTypeIndicatorIDRemoval", lParam.getStockRemoval());
			jrs.updateString("StorageTypeIndicatorIDPlacement", lParam.getStockPlacement());
			jrs.updateString("StorageSectionIndicatorID", lParam.getStorageSectionIndicator());
			jrs.updateString("StorageBinID", lParam.getStorageBin());
			jrs.updateDouble("LEQty1", lParam.getLEQty1());
			jrs.updateDouble("LEQty2", lParam.getLEQty2());
			jrs.updateDouble("LEQty3", lParam.getLEQty3());
			jrs.updateString("UOMLEQty1", lParam.getUOMLEQty1());
			jrs.updateString("UOMLEQty2", lParam.getUOMLEQty2());
			jrs.updateString("UOMLEQty3", lParam.getUOMLEQty3());
			jrs.updateString("StorageUnitTypeIDLEQty1", lParam.getSUTLEQty1());
			jrs.updateString("StorageUnitTypeIDLEQty2", lParam.getSUTLEQty2());
			jrs.updateString("StorageUnitTypeIDLEQty3", lParam.getSUTLEQty3());
			jrs.updateString("LastUpdateBy", Globals.user);
			jrs.updateString("LastDate", LocalDateTime.now().toString());
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Product";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateProductWM", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateProductWM", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
}
