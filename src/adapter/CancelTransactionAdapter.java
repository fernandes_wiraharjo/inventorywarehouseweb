package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import javax.xml.bind.ParseConversionEvent;

import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import jdk.nashorn.internal.ir.ReturnNode;
import model.Globals;
import model.mdlCancelTransaction;
import model.mdlInbound;
import model.mdlOutboundDetail;
import model.mdlStockSummary;
import model.mdlStockTransferDetail;

/** Documentation
 * 002 - Nandha
 */

public class CancelTransactionAdapter {

	@SuppressWarnings("resource")
	public static List<model.mdlCancelTransaction> LoadCancelInbound (String lUser) {
		List<model.mdlCancelTransaction> listmdlCancelInbound = new ArrayList<model.mdlCancelTransaction>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DocNumber,Date,PlantID,WarehouseID,RefDocNumber,Transaction "
					+ "FROM cancel_transaction "
					+ "WHERE Transaction='Inbound' AND PlantID IN ("+Globals.user_area+") ORDER BY Date");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlCancelTransaction mdlCancelInbound = new model.mdlCancelTransaction();
				mdlCancelInbound.setDocNumber(jrs.getString("DocNumber"));
				
				String newDate=ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlCancelInbound.setDate(newDate);
				
				mdlCancelInbound.setPlantID(jrs.getString("PlantID"));
				mdlCancelInbound.setWarehouseID(jrs.getString("WarehouseID"));
				mdlCancelInbound.setRefDocNumber(jrs.getString("RefDocNumber"));
				mdlCancelInbound.setTransaction(jrs.getString("Transaction"));
				
				listmdlCancelInbound.add(mdlCancelInbound);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCancelInbound", Globals.gCommand , lUser);
		}
		finally {
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadCancelInbound", "close opened connection protocol", lUser);
			}
		}
		
		return listmdlCancelInbound;
	}

	public static String TransactionCancelInbound(mdlCancelTransaction lParamCancelInbound, List<mdlInbound> lParamlistInboundDetail ,String lUser)
	{	
		Globals.gCommand = "";
		
			Connection connection = null;
			PreparedStatement pstmInsertCancelInbound = null;
			PreparedStatement pstmInsertCancelInboundDetail = null;
			PreparedStatement pstmInsertUpdateStockSummary = null;
			//PreparedStatement pstmUpdateStockSummary = null;
			PreparedStatement pstmUpdateFlagInbound = null;
			PreparedStatement pstmInsertTraceCode = null;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlInsertCancelInbound = "INSERT INTO cancel_transaction(`DocNumber`, `Date`, `PlantID`, `WarehouseID`, `RefDocNumber`, `Transaction`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertCancelInboundDetail = "INSERT INTO cancel_transaction_detail(`DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertUpdateStockSummary = "INSERT INTO stock_summary(`Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
				//String sqlUpdateStockSummary = "UPDATE stock_summary SET `Qty`=?,`Updated_by`=?, `Updated_at`=? WHERE `Period`=? AND `ProductID`=? AND `PlantID`=? AND `WarehouseID`=? AND `Batch_No`=?;";
				String sqlUpdateflagInbound = "UPDATE `inbound` SET `IsCancel`=1,`Updated_by`=?, `Updated_at`=? WHERE `DocNumber`=?;";
				String sqlInsertTranceCode = "INSERT INTO TraceCodeGenerate(`Trace_Code`,`Flag_Table`,`Created_at`) VALUES (?,?,?);";
				pstmInsertCancelInbound = connection.prepareStatement(sqlInsertCancelInbound);
				pstmInsertCancelInboundDetail = connection.prepareStatement(sqlInsertCancelInboundDetail);
				pstmInsertUpdateStockSummary = connection.prepareStatement(sqlInsertUpdateStockSummary);
//				pstmUpdateStockSummary = connection.prepareStatement(sqlUpdateStockSummary);
				pstmUpdateFlagInbound = connection.prepareStatement(sqlUpdateflagInbound);
				pstmInsertTraceCode = connection.prepareStatement(sqlInsertTranceCode);
				
				//Update flag di Inbound
				pstmUpdateFlagInbound.setString(1,  ValidateNull.NulltoStringEmpty(lUser));
				pstmUpdateFlagInbound.setString(2,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmUpdateFlagInbound.setString(3, ValidateNull.NulltoStringEmpty(lParamCancelInbound.getRefDocNumber()));
				Globals.gCommand = pstmUpdateFlagInbound.toString();
				pstmUpdateFlagInbound.executeUpdate();
				
				//Insert trace code
				pstmInsertTraceCode.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelInbound.getDocNumber()));
				pstmInsertTraceCode.setString(2,  ValidateNull.NulltoStringEmpty("Inbound"));
				pstmInsertTraceCode.setString(3, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertTraceCode.toString();
				pstmInsertTraceCode.executeUpdate();
				
				//insert cancel inbound process parameter
				pstmInsertCancelInbound.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelInbound.getDocNumber()));
				pstmInsertCancelInbound.setString(2,  ValidateNull.NulltoStringEmpty(lParamCancelInbound.getDate()));
				pstmInsertCancelInbound.setString(3,  ValidateNull.NulltoStringEmpty(lParamCancelInbound.getPlantID()));
				pstmInsertCancelInbound.setString(4,  ValidateNull.NulltoStringEmpty(lParamCancelInbound.getWarehouseID()));
				pstmInsertCancelInbound.setString(5,  ValidateNull.NulltoStringEmpty(lParamCancelInbound.getRefDocNumber()));
				pstmInsertCancelInbound.setString(6,  ValidateNull.NulltoStringEmpty(lParamCancelInbound.getTransaction()));
				pstmInsertCancelInbound.setString(7,  ValidateNull.NulltoStringEmpty(lUser)); 
				pstmInsertCancelInbound.setString(8,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertCancelInbound.setString(9,  ValidateNull.NulltoStringEmpty(lUser));
				pstmInsertCancelInbound.setString(10, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertCancelInbound.toString();
				pstmInsertCancelInbound.executeUpdate();
				
				//insert cancel inbound detail process parameter and update the stock summary after cancellation
				for(mdlInbound lParamInboundDetail : lParamlistInboundDetail)
				{
					pstmInsertCancelInboundDetail.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelInbound.getDocNumber()));
					pstmInsertCancelInboundDetail.setString(2,  ValidateNull.NulltoStringEmpty(lParamInboundDetail.getDocLine()));
					pstmInsertCancelInboundDetail.setString(3,  ValidateNull.NulltoStringEmpty(lParamInboundDetail.getProductID()));
					pstmInsertCancelInboundDetail.setString(4,  ValidateNull.NulltoStringEmpty("-" + lParamInboundDetail.getQtyUom()));
					pstmInsertCancelInboundDetail.setString(5,  ValidateNull.NulltoStringEmpty(lParamInboundDetail.getUom()));
					pstmInsertCancelInboundDetail.setString(6,  ValidateNull.NulltoStringEmpty("-" + lParamInboundDetail.getQtyBaseUom())); 
					pstmInsertCancelInboundDetail.setString(7,  ValidateNull.NulltoStringEmpty(lParamInboundDetail.getBaseUom()));
					pstmInsertCancelInboundDetail.setString(8,  ValidateNull.NulltoStringEmpty(lParamInboundDetail.getBatchNo()));
					pstmInsertCancelInboundDetail.setString(9, ValidateNull.NulltoStringEmpty(lParamInboundDetail.getPackingNo()));
					pstmInsertCancelInboundDetail.setString(10, ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertCancelInboundDetail.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertCancelInboundDetail.setString(12, ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertCancelInboundDetail.setString(13, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmInsertCancelInbound.toString();
					pstmInsertCancelInboundDetail.executeUpdate();
					
					
					//Declare mdlStockSummary
					model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
					mdlStockSummary.setProductID(lParamInboundDetail.getProductID());
					mdlStockSummary.setPlantID(lParamCancelInbound.getPlantID());
					mdlStockSummary.setWarehouseID(lParamCancelInbound.getWarehouseID());
					mdlStockSummary.setBatch_No(lParamInboundDetail.getBatchNo());
					mdlStockSummary.setPeriod(lParamCancelInbound.getDate().substring(0, 7).replace("-", ""));
					mdlStockSummary.setUOM(lParamInboundDetail.getBaseUom());
					Globals.gCommand = mdlStockSummary.toString();
					
					model.mdlStockSummary mdlStockSummary2 =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,lUser);
					if(mdlStockSummary2.getQty()==null){
						returnImmediately(connection,"TransactionCancelInbound");
						
						LogAdapter.InsertLogExc("The stock product not found", "InsertCancelInbound", Globals.gCommand , lUser);
		    			Globals.gReturn_Status = "Produk : "+mdlStockSummary.getProductID()+",plant : "+mdlStockSummary.getPlantID()+",warehouse : "+mdlStockSummary.getWarehouseID()+",batch : "+mdlStockSummary.getBatch_No()+" tidak ditemukan di stok gudang";
		    			
		    			return Globals.gReturn_Status;
					}
					//check if the existing qty is available for cancellation
					else if( Integer.parseInt(mdlStockSummary2.getQty()) < Integer.parseInt(lParamInboundDetail.getQtyBaseUom()) )
					{
						returnImmediately(connection,"TransactionCancelInbound");
						
						LogAdapter.InsertLogExc("The product quantity that you cancel is more than the available quantity in the database", "InsertCancelInbound", Globals.gCommand , lUser);
		    			Globals.gReturn_Status = "Stok untuk produk : "+mdlStockSummary.getProductID()+",plant : "+mdlStockSummary.getPlantID()+",warehouse : "+mdlStockSummary.getWarehouseID()+",batch : "+mdlStockSummary.getBatch_No()+" tidak mencukupi untuk dilakukan proses pembatalan";
		    			
		    			return Globals.gReturn_Status;
					}
					
					int newQty = Integer.parseInt(mdlStockSummary2.getQty()) - Integer.parseInt(lParamInboundDetail.getQtyBaseUom());
					mdlStockSummary.setQty(String.valueOf(newQty));
					
					//					pstmUpdateStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
					//					pstmUpdateStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(lUser));
					//					pstmUpdateStockSummary.setString(3,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					//					pstmUpdateStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary2.getPeriod()));
					//					pstmUpdateStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
					//					pstmUpdateStockSummary.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
					//					pstmUpdateStockSummary.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
					//					pstmUpdateStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
					//					
					//					pstmUpdateStockSummary.executeUpdate();
					
					pstmInsertUpdateStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPeriod()));
					pstmInsertUpdateStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
					pstmInsertUpdateStockSummary.setString(3,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
					pstmInsertUpdateStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
					pstmInsertUpdateStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
					pstmInsertUpdateStockSummary.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty())); 
					pstmInsertUpdateStockSummary.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
					pstmInsertUpdateStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertUpdateStockSummary.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertUpdateStockSummary.setString(10,  ValidateNull.NulltoStringEmpty(lUser)); 
					pstmInsertUpdateStockSummary.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertUpdateStockSummary.setString(12,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
					pstmInsertUpdateStockSummary.setString(13,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
					pstmInsertUpdateStockSummary.setString(14,  ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertUpdateStockSummary.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmInsertUpdateStockSummary.toString();
					pstmInsertUpdateStockSummary.executeUpdate();
				}
					
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Insert Cancel Inbound Document";
			}
			catch (Exception e ) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionCancelInbound", Globals.gCommand , lUser);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertCancelInbound", Globals.gCommand , lUser);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
			        if (pstmInsertCancelInbound != null) {
			        	pstmInsertCancelInbound.close();
			        }
			        if (pstmInsertCancelInboundDetail != null) {
			        	pstmInsertCancelInboundDetail.close();
			        }
			        if (pstmInsertUpdateStockSummary != null) {
			        	pstmInsertUpdateStockSummary.close();
			        }
			        if (pstmUpdateFlagInbound != null) {
			        	pstmUpdateFlagInbound.close();
			        }
			        if (pstmInsertTraceCode != null) {
			        	pstmInsertTraceCode.close();
			        }
			        connection.setAutoCommit(true);
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e) {
					LogAdapter.InsertLogExc(e.toString(), "InsertCancelInbound", "close opened connection protocol", lUser);
				}
		    }
			
			return Globals.gReturn_Status;
	}
	
	public static void returnImmediately(Connection connection, String function) {
		if (connection != null) {
		    try {
		        System.err.print("Transaction is being rolled back");
		        connection.rollback();
		    } catch(SQLException excep) {
		    	LogAdapter.InsertLogExc(excep.toString(), function, Globals.gCommand , Globals.user);
				Globals.gReturn_Status = "Database Error";
		    }
		}
	}

	@SuppressWarnings("resource")
	public static List<model.mdlCancelTransaction> LoadCancelInboundDetail (String lDocNo) {
		List<model.mdlCancelTransaction> listmdlCancelInboundDetail = new ArrayList<model.mdlCancelTransaction>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DocNumber,DocLine,ProductID,Qty_UOM,UOM,Qty_BaseUOM,BaseUOM,Batch_No,Packing_No FROM cancel_transaction_detail WHERE DocNumber = ? ORDER BY DocLine");
			jrs.setString(1,  lDocNo);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlCancelTransaction mdlCancelInboundDetail = new model.mdlCancelTransaction();
				mdlCancelInboundDetail.setDocNumber(jrs.getString("DocNumber"));
				mdlCancelInboundDetail.setDocLine(jrs.getString("DocLine"));
				mdlCancelInboundDetail.setProductID(jrs.getString("ProductID"));
				mdlCancelInboundDetail.setQtyUOM(jrs.getString("Qty_UOM"));
				mdlCancelInboundDetail.setUOM(jrs.getString("UOM"));
				mdlCancelInboundDetail.setQtyBaseUOM(jrs.getString("Qty_BaseUOM"));
				mdlCancelInboundDetail.setBaseUOM(jrs.getString("BaseUOM"));
				mdlCancelInboundDetail.setBatch_No(jrs.getString("Batch_No"));
				mdlCancelInboundDetail.setPacking_No(jrs.getString("Packing_No"));
				
				listmdlCancelInboundDetail.add(mdlCancelInboundDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCancelInboundDetail", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCancelInboundDetail", "close opened connection protocol", Globals.user);
			}
		}
		
		return listmdlCancelInboundDetail;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlCancelTransaction> LoadCancelTransactionDetail (String lDocNo) {
		List<model.mdlCancelTransaction> listmdlCancelTransactionDetail = new ArrayList<model.mdlCancelTransaction>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DocNumber,DocLine,ProductID,Qty_UOM,UOM,Qty_BaseUOM,BaseUOM,Batch_No,Packing_No FROM cancel_transaction_detail WHERE DocNumber = ? ORDER BY DocLine");
			jrs.setString(1,  lDocNo);
			jrs.execute();
			
			Globals.gCommand = jrs.getCommand();
			
			while(jrs.next()){
				model.mdlCancelTransaction mdlCancelTransactionDetail = new model.mdlCancelTransaction();
				mdlCancelTransactionDetail.setDocNumber(jrs.getString("DocNumber"));
				mdlCancelTransactionDetail.setDocLine(jrs.getString("DocLine"));
				mdlCancelTransactionDetail.setProductID(jrs.getString("ProductID"));
				
				if(jrs.getString("Qty_UOM").contains("-"))
				{
					mdlCancelTransactionDetail.setQtyUOM(jrs.getString("Qty_UOM")); 
					mdlCancelTransactionDetail.setQtyBaseUOM(jrs.getString("Qty_BaseUOM"));
				}
				else
				{
					mdlCancelTransactionDetail.setQtyUOM("+" + jrs.getString("Qty_UOM"));
					mdlCancelTransactionDetail.setQtyBaseUOM("+" + jrs.getString("Qty_BaseUOM"));
				}
				
				mdlCancelTransactionDetail.setUOM(jrs.getString("UOM"));
				mdlCancelTransactionDetail.setBaseUOM(jrs.getString("BaseUOM"));
				mdlCancelTransactionDetail.setBatch_No(jrs.getString("Batch_No"));
				mdlCancelTransactionDetail.setPacking_No(jrs.getString("Packing_No"));
				
				listmdlCancelTransactionDetail.add(mdlCancelTransactionDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCancelTransactionDetail", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCancelTransactionDetail", "close opened connection protocol", Globals.user);
			}
		}
		
		return listmdlCancelTransactionDetail;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlCancelTransaction> LoadCancelOutbound (String lUser) {
		List<model.mdlCancelTransaction> listmdlCancelOutbound = new ArrayList<model.mdlCancelTransaction>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DocNumber,Date,PlantID,WarehouseID,RefDocNumber,Transaction "
					+ "FROM cancel_transaction "
					+ "WHERE Transaction='Outbound' AND PlantID IN ("+Globals.user_area+") ORDER BY Date");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlCancelTransaction mdlCancelOutbound = new model.mdlCancelTransaction();
				mdlCancelOutbound.setDocNumber(jrs.getString("DocNumber"));
				
				String newDate = ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlCancelOutbound.setDate(newDate);
				
				mdlCancelOutbound.setPlantID(jrs.getString("PlantID"));
				mdlCancelOutbound.setWarehouseID(jrs.getString("WarehouseID"));
				mdlCancelOutbound.setRefDocNumber(jrs.getString("RefDocNumber"));
				mdlCancelOutbound.setTransaction(jrs.getString("Transaction"));
				
				listmdlCancelOutbound.add(mdlCancelOutbound);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCancelOutbound", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCancelOutbound", "close opened connection protocol", lUser);
			}
		}
		
		return listmdlCancelOutbound;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlCancelTransaction> LoadCancelOutboundDetail (String lUser) {
		List<model.mdlCancelTransaction> listmdlCancelOutboundDetail = new ArrayList<model.mdlCancelTransaction>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DocNumber,DocLine,ProductID,Qty_UOM,UOM,Qty_BaseUOM,BaseUOM,Batch_No,Packing_No FROM cancel_transaction_detail WHERE DocNumber IN (SELECT DocNumber FROM cancel_transaction WHERE Transaction='Outbound') ORDER BY DocLine");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlCancelTransaction mdlCancelOutboundDetail = new model.mdlCancelTransaction();
				mdlCancelOutboundDetail.setDocNumber(jrs.getString("DocNumber"));
				mdlCancelOutboundDetail.setDocLine(jrs.getString("DocLine"));
				mdlCancelOutboundDetail.setProductID(jrs.getString("ProductID"));
				mdlCancelOutboundDetail.setQtyUOM(jrs.getString("Qty_UOM"));
				mdlCancelOutboundDetail.setUOM(jrs.getString("UOM"));
				mdlCancelOutboundDetail.setQtyBaseUOM(jrs.getString("Qty_BaseUOM"));
				mdlCancelOutboundDetail.setBaseUOM(jrs.getString("BaseUOM"));
				mdlCancelOutboundDetail.setBatch_No(jrs.getString("Batch_No"));
				mdlCancelOutboundDetail.setPacking_No(jrs.getString("Packing_No"));
				
				listmdlCancelOutboundDetail.add(mdlCancelOutboundDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCancelOutboundDetail", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCancelOutboundDetail", "close opened connection protocol", lUser);
			}
		}
		
		return listmdlCancelOutboundDetail;
	}
	
	public static String TransactionCancelOutbound(mdlCancelTransaction lParamCancelOutbound, List<mdlOutboundDetail> lParamlistOutboundDetail ,String lUser)
	{	
		Globals.gCommand = "";
		
			Connection connection = null;
			PreparedStatement pstmInsertCancelOutbound = null;
			PreparedStatement pstmInsertCancelOutboundDetail = null;
			PreparedStatement pstmUpdateStockSummary = null;
			PreparedStatement pstmUpdateOutbound  = null;
			PreparedStatement pstmInsertTraceCode = null;
	
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlInsertCancelOutbound = "INSERT INTO cancel_transaction(`DocNumber`, `Date`, `PlantID`, `WarehouseID`, `RefDocNumber`, `Transaction`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertCancelOutboundDetail = "INSERT INTO cancel_transaction_detail(`DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
				String sqlUpdateStockSummary = "INSERT INTO stock_summary(`Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
				//String sqlUpdateStockSummary = "UPDATE stock_summary SET `Qty`=?,`Updated_by`=?, `Updated_at`=? WHERE `Period`=? AND `ProductID`=? AND `PlantID`=? AND `WarehouseID`=? AND `Batch_No`=?;";
				String sqlUpdateflagOutbound = "UPDATE `outbound` SET `IsCancel`=1,`Updated_by`=?, `Updated_at`=? WHERE `DocNumber`=?;";
				String sqlInsertTranceCode = "INSERT INTO TraceCodeGenerate(`Trace_Code`,`Flag_Table`,`Created_at`) VALUES (?,?,?);";
				pstmInsertCancelOutbound = connection.prepareStatement(sqlInsertCancelOutbound);
				pstmInsertCancelOutboundDetail = connection.prepareStatement(sqlInsertCancelOutboundDetail);
				pstmUpdateStockSummary = connection.prepareStatement(sqlUpdateStockSummary);
				pstmUpdateOutbound = connection.prepareStatement(sqlUpdateflagOutbound);
				pstmInsertTraceCode = connection.prepareStatement(sqlInsertTranceCode);
				

				//Update flag di Outbound
				pstmUpdateOutbound.setString(1,  ValidateNull.NulltoStringEmpty(lUser));
				pstmUpdateOutbound.setString(2,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmUpdateOutbound.setString(3,  ValidateNull.NulltoStringEmpty(lParamCancelOutbound.getRefDocNumber()));
				Globals.gCommand = pstmUpdateOutbound.toString();
				pstmUpdateOutbound.executeUpdate();
				
				//Insert trace code
				pstmInsertTraceCode.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelOutbound.getDocNumber()));
				pstmInsertTraceCode.setString(2,  ValidateNull.NulltoStringEmpty("Outbound"));
				pstmInsertTraceCode.setString(3,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertTraceCode.toString();
				pstmInsertTraceCode.executeUpdate();
				
				//insert cancel outbound process parameter
				pstmInsertCancelOutbound.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelOutbound.getDocNumber()));
				pstmInsertCancelOutbound.setString(2,  ValidateNull.NulltoStringEmpty(lParamCancelOutbound.getDate()));
				pstmInsertCancelOutbound.setString(3,  ValidateNull.NulltoStringEmpty(lParamCancelOutbound.getPlantID()));
				pstmInsertCancelOutbound.setString(4,  ValidateNull.NulltoStringEmpty(lParamCancelOutbound.getWarehouseID()));
				pstmInsertCancelOutbound.setString(5,  ValidateNull.NulltoStringEmpty(lParamCancelOutbound.getRefDocNumber()));
				pstmInsertCancelOutbound.setString(6,  ValidateNull.NulltoStringEmpty(lParamCancelOutbound.getTransaction()));
				pstmInsertCancelOutbound.setString(7,  ValidateNull.NulltoStringEmpty(lUser)); 
				pstmInsertCancelOutbound.setString(8,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertCancelOutbound.setString(9,  ValidateNull.NulltoStringEmpty(lUser));
				pstmInsertCancelOutbound.setString(10, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertCancelOutbound.toString();
				pstmInsertCancelOutbound.executeUpdate();
				
				//insert cancel outbound detail process parameter and update the stock summary after cancellation
				for(mdlOutboundDetail lParamOutboundDetail : lParamlistOutboundDetail)
				{
					pstmInsertCancelOutboundDetail.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelOutbound.getDocNumber()));
					pstmInsertCancelOutboundDetail.setString(2,  ValidateNull.NulltoStringEmpty(lParamOutboundDetail.getDocLine()));
					pstmInsertCancelOutboundDetail.setString(3,  ValidateNull.NulltoStringEmpty(lParamOutboundDetail.getProductID()));
					pstmInsertCancelOutboundDetail.setString(4,  ValidateNull.NulltoStringEmpty(lParamOutboundDetail.getQty_UOM()));
					pstmInsertCancelOutboundDetail.setString(5,  ValidateNull.NulltoStringEmpty(lParamOutboundDetail.getUOM()));
					pstmInsertCancelOutboundDetail.setString(6,  ValidateNull.NulltoStringEmpty(lParamOutboundDetail.getQty_BaseUOM())); 
					pstmInsertCancelOutboundDetail.setString(7,  ValidateNull.NulltoStringEmpty(lParamOutboundDetail.getBaseUOM()));
					pstmInsertCancelOutboundDetail.setString(8,  ValidateNull.NulltoStringEmpty(lParamOutboundDetail.getBatch_No()));
					pstmInsertCancelOutboundDetail.setString(9, ValidateNull.NulltoStringEmpty(lParamOutboundDetail.getPacking_No()));
					pstmInsertCancelOutboundDetail.setString(10, ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertCancelOutboundDetail.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertCancelOutboundDetail.setString(12, ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertCancelOutboundDetail.setString(13, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmInsertCancelOutboundDetail.toString();
					pstmInsertCancelOutboundDetail.executeUpdate();
					
					
					//Declare mdlStockSummary
					model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
					mdlStockSummary.setProductID(lParamOutboundDetail.getProductID());
					mdlStockSummary.setPlantID(lParamCancelOutbound.getPlantID());
					mdlStockSummary.setWarehouseID(lParamCancelOutbound.getWarehouseID());
					mdlStockSummary.setBatch_No(lParamOutboundDetail.getBatch_No());
					mdlStockSummary.setPeriod(lParamCancelOutbound.getDate().substring(0, 7).replace("-", ""));
					mdlStockSummary.setUOM(lParamOutboundDetail.getBaseUOM());
					
					model.mdlStockSummary mdlStockSummary2 =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,lUser);
					if(mdlStockSummary2.getQty()==null){
						returnImmediately(connection,"TransactionCancelOutbound");
						
						LogAdapter.InsertLogExc("The stock product not found", "InsertCancelOutbound", Globals.gCommand , lUser);
		    			Globals.gReturn_Status = "Produk : "+mdlStockSummary.getProductID()+",plant : "+mdlStockSummary.getPlantID()+",warehouse : "+mdlStockSummary.getWarehouseID()+",batch : "+mdlStockSummary.getBatch_No()+" tidak ditemukan di stok gudang";
		    			
		    			return Globals.gReturn_Status;
					}
					int newQty = Integer.parseInt(mdlStockSummary2.getQty()) + Integer.parseInt(lParamOutboundDetail.getQty_BaseUOM());
					mdlStockSummary.setQty(String.valueOf(newQty));
					
					//					pstmUpdateStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
					//					pstmUpdateStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(lUser));
					//					pstmUpdateStockSummary.setString(3,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					//					pstmUpdateStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary2.getPeriod()));
					//					pstmUpdateStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
					//					pstmUpdateStockSummary.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
					//					pstmUpdateStockSummary.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
					//					pstmUpdateStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
					//					
					//					pstmUpdateStockSummary.executeUpdate();
					
					pstmUpdateStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPeriod()));
					pstmUpdateStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
					pstmUpdateStockSummary.setString(3,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
					pstmUpdateStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
					pstmUpdateStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
					pstmUpdateStockSummary.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty())); 
					pstmUpdateStockSummary.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
					pstmUpdateStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(lUser));
					pstmUpdateStockSummary.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmUpdateStockSummary.setString(10,  ValidateNull.NulltoStringEmpty(lUser)); 
					pstmUpdateStockSummary.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmUpdateStockSummary.setString(12,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
					pstmUpdateStockSummary.setString(13,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
					pstmUpdateStockSummary.setString(14,  ValidateNull.NulltoStringEmpty(lUser));
					pstmUpdateStockSummary.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmUpdateStockSummary.toString();
					pstmUpdateStockSummary.executeUpdate();
				}
					
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Insert Cancel Outbound Document";
			}
			
			catch (Exception e) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionCancelOutbound", Globals.gCommand , lUser);
		    			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertCancelOutbound", Globals.gCommand , lUser);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
			        if (pstmInsertCancelOutbound != null) {
			        	pstmInsertCancelOutbound.close();
			        }
			        if (pstmInsertCancelOutboundDetail != null) {
			        	pstmInsertCancelOutboundDetail.close();
			        }
			        if (pstmUpdateStockSummary != null) {
			        	pstmUpdateStockSummary.close();
			        }
			        if (pstmUpdateOutbound != null) {
			        	pstmUpdateOutbound.close();
			        }
			        if (pstmInsertTraceCode != null) {
			        	pstmInsertTraceCode.close();
			        }
			        connection.setAutoCommit(true);
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertCancelOutbound", "close opened connection protocol", lUser);
				}
		    }
			
		return Globals.gReturn_Status;
	}
	
	//<<Nanda
	@SuppressWarnings("resource")
	public static List<model.mdlCancelTransaction> LoadCancelStockTransfer(String lUser) {
		List<model.mdlCancelTransaction> listmdlCancelTransfer = new ArrayList<model.mdlCancelTransaction>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DocNumber,Date,PlantID,WarehouseID,RefDocNumber,Transaction "
					+ "FROM cancel_transaction "
					+ "WHERE Transaction='Stock Transfer' AND PlantID IN ("+Globals.user_area+") "
					+ "ORDER BY Date");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
		
			while(jrs.next()){
				model.mdlCancelTransaction mdlCancelTransfer = new model.mdlCancelTransaction();
				mdlCancelTransfer.setDocNumber(jrs.getString("DocNumber"));
				
				String newDate = ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlCancelTransfer.setDate(newDate);
				
				mdlCancelTransfer.setPlantID(jrs.getString("PlantID"));
				mdlCancelTransfer.setWarehouseID(jrs.getString("WarehouseID"));
				mdlCancelTransfer.setRefDocNumber(jrs.getString("RefDocNumber"));
				mdlCancelTransfer.setTransaction(jrs.getString("Transaction"));
				
				listmdlCancelTransfer.add(mdlCancelTransfer);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCancelStockTransfer", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCancelStockTransfer", "close opened connection protocol", lUser);
			}
		}
		
		return listmdlCancelTransfer;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlCancelTransaction> LoadCancelStockTransferDetail (String lUser) {
		List<model.mdlCancelTransaction> listmdlCancelStockTransferDetail = new ArrayList<model.mdlCancelTransaction>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DocNumber,DocLine,ProductID,Qty_UOM,UOM,Qty_BaseUOM,BaseUOM,Batch_No,Packing_No FROM cancel_transaction_detail WHERE DocNumber IN (SELECT DocNumber FROM cancel_transaction WHERE Transaction='Stock Transfer') ORDER BY DocLine");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlCancelTransaction mdlCancelStockTransferDetail = new model.mdlCancelTransaction();
				mdlCancelStockTransferDetail.setDocNumber(jrs.getString("DocNumber"));
				mdlCancelStockTransferDetail.setDocLine(jrs.getString("DocLine"));
				mdlCancelStockTransferDetail.setProductID(jrs.getString("ProductID"));
				mdlCancelStockTransferDetail.setQtyUOM(jrs.getString("Qty_UOM"));
				mdlCancelStockTransferDetail.setUOM(jrs.getString("UOM"));
				mdlCancelStockTransferDetail.setQtyBaseUOM(jrs.getString("Qty_BaseUOM"));
				mdlCancelStockTransferDetail.setBaseUOM(jrs.getString("BaseUOM"));
				mdlCancelStockTransferDetail.setBatch_No(jrs.getString("Batch_No"));
				mdlCancelStockTransferDetail.setPacking_No(jrs.getString("Packing_No"));
				
				listmdlCancelStockTransferDetail.add(mdlCancelStockTransferDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCancelStockTransferDetail", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCancelStockTransferDetail", "close opened connection protocol", lUser);
			}
		}
		
		return listmdlCancelStockTransferDetail;
	}
	
	public static String TransactionCancelStockTransfer(mdlCancelTransaction lParamCancelStockTransfer, List<mdlStockTransferDetail> lParamlistStockTransferDetail ,String lUser)
	{	
		Globals.gCommand = "";
			Connection connection = null;
			PreparedStatement pstmInsertCancelStockTransfer = null;
			PreparedStatement pstmInsertCancelStockTransferDetail = null;
			PreparedStatement pstmUpdateStockSummaryFrom = null;
			PreparedStatement pstmUpdateStockSummaryTo = null;
			PreparedStatement pstmUpdateStockTransfer = null;
			PreparedStatement pstmInsertTraceCode = null;
			
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sqlInsertCancelStockTransfer = "INSERT INTO cancel_transaction(`DocNumber`, `Date`, `PlantID`, `WarehouseID`, `RefDocNumber`, `Transaction`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertCancelStockTransferDetail = "INSERT INTO cancel_transaction_detail(`DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
				String sqlInsertUpdateStockSummary = "INSERT INTO stock_summary(`Period`, `ProductID`, `PlantID`, `WarehouseID`, `Batch_No`, `Qty`, `UOM`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
				//String sqlUpdateStockSummary = "UPDATE stock_summary SET `Qty`=?,`Updated_by`=?, `Updated_at`=? WHERE `Period`=? AND `ProductID`=? AND `PlantID`=? AND `WarehouseID`=? AND `Batch_No`=?;";
				String sqlUpdateflagStockTransfer = "UPDATE stock_transfer SET `IsCancel`=1,`Updated_by`=?, `Updated_at`=? WHERE `TransferID`=?;";
				String sqlInsertTraceCode = "INSERT INTO TraceCodeGenerate(`Trace_Code`,`Flag_Table`,`Created_at`) VALUES (?,?,?);";
				pstmInsertCancelStockTransfer = connection.prepareStatement(sqlInsertCancelStockTransfer);
				pstmInsertCancelStockTransferDetail = connection.prepareStatement(sqlInsertCancelStockTransferDetail);
				//pstmInsertUpdateStockSummary = connection.prepareStatement(sqlInsertUpdateStockSummary);
				//pstmUpdateStockSummaryFrom = connection.prepareStatement(sqlUpdateStockSummary);
				//pstmUpdateStockSummaryTo = connection.prepareStatement(sqlUpdateStockSummary);
				pstmUpdateStockSummaryFrom = connection.prepareStatement(sqlInsertUpdateStockSummary);
				pstmUpdateStockSummaryTo = connection.prepareStatement(sqlInsertUpdateStockSummary);
				pstmUpdateStockTransfer = connection.prepareStatement(sqlUpdateflagStockTransfer);
				pstmInsertTraceCode = connection.prepareStatement(sqlInsertTraceCode);
				
				//Update flag di Stock Transfer
				pstmUpdateStockTransfer.setString(1,  ValidateNull.NulltoStringEmpty(lUser));
				pstmUpdateStockTransfer.setString(2,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmUpdateStockTransfer.setString(3, ValidateNull.NulltoStringEmpty(lParamCancelStockTransfer.getRefDocNumber()));
				Globals.gCommand = pstmUpdateStockTransfer.toString();
				pstmUpdateStockTransfer.executeUpdate();
				
				//Insert trace code
				pstmInsertTraceCode.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelStockTransfer.getDocNumber()));
				pstmInsertTraceCode.setString(2,  ValidateNull.NulltoStringEmpty("StockTransfer"));
				pstmInsertTraceCode.setString(3, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertTraceCode.toString();
				pstmInsertTraceCode.executeUpdate();
				
				//insert cancel Stock Transfer process parameter
				pstmInsertCancelStockTransfer.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelStockTransfer.getDocNumber()));
				pstmInsertCancelStockTransfer.setString(2,  ValidateNull.NulltoStringEmpty(lParamCancelStockTransfer.getDate()));
				pstmInsertCancelStockTransfer.setString(3,  ValidateNull.NulltoStringEmpty(lParamCancelStockTransfer.getPlantID()));
				pstmInsertCancelStockTransfer.setString(4,  ValidateNull.NulltoStringEmpty(lParamCancelStockTransfer.getWarehouseID()));
				pstmInsertCancelStockTransfer.setString(5,  ValidateNull.NulltoStringEmpty(lParamCancelStockTransfer.getRefDocNumber()));
				pstmInsertCancelStockTransfer.setString(6,  ValidateNull.NulltoStringEmpty(lParamCancelStockTransfer.getTransaction()));
				pstmInsertCancelStockTransfer.setString(7,  ValidateNull.NulltoStringEmpty(lUser)); 
				pstmInsertCancelStockTransfer.setString(8,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				pstmInsertCancelStockTransfer.setString(9,  ValidateNull.NulltoStringEmpty(lUser));
				pstmInsertCancelStockTransfer.setString(10, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
				Globals.gCommand = pstmInsertCancelStockTransfer.toString();
				pstmInsertCancelStockTransfer.executeUpdate();
				
				//insert cancel stock transfer detail process parameter and update the stock summary after cancellation
				for(mdlStockTransferDetail lParamStockTransferDetail : lParamlistStockTransferDetail)
				{
					pstmInsertCancelStockTransferDetail.setString(1,  ValidateNull.NulltoStringEmpty(lParamCancelStockTransfer.getDocNumber()));
					pstmInsertCancelStockTransferDetail.setString(2,  ValidateNull.NulltoStringEmpty(lParamStockTransferDetail.getTransferLine()));
					pstmInsertCancelStockTransferDetail.setString(3,  ValidateNull.NulltoStringEmpty(lParamStockTransferDetail.getProductID()));
					pstmInsertCancelStockTransferDetail.setString(4,  ValidateNull.NulltoStringEmpty(lParamStockTransferDetail.getQty_UOM()));
					pstmInsertCancelStockTransferDetail.setString(5,  ValidateNull.NulltoStringEmpty(lParamStockTransferDetail.getUOM()));
					pstmInsertCancelStockTransferDetail.setString(6,  ValidateNull.NulltoStringEmpty(lParamStockTransferDetail.getQty_BaseUOM())); 
					pstmInsertCancelStockTransferDetail.setString(7,  ValidateNull.NulltoStringEmpty(lParamStockTransferDetail.getBaseUOM()));
					pstmInsertCancelStockTransferDetail.setString(8,  ValidateNull.NulltoStringEmpty(lParamStockTransferDetail.getBatch_No()));
					pstmInsertCancelStockTransferDetail.setString(9,  ValidateNull.NulltoStringEmpty(lParamStockTransferDetail.getPacking_No()));
					pstmInsertCancelStockTransferDetail.setString(10, ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertCancelStockTransferDetail.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmInsertCancelStockTransferDetail.setString(12, ValidateNull.NulltoStringEmpty(lUser));
					pstmInsertCancelStockTransferDetail.setString(13, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmInsertCancelStockTransferDetail.toString();
					pstmInsertCancelStockTransferDetail.executeUpdate();
					
					//Declare mdlStockSummary
					model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
					mdlStockSummary.setProductID(lParamStockTransferDetail.getProductID());
					mdlStockSummary.setPlantID(lParamCancelStockTransfer.getPlantID());
					mdlStockSummary.setBatch_No(lParamStockTransferDetail.getBatch_No());
					mdlStockSummary.setPeriod(lParamCancelStockTransfer.getDate().substring(0, 7).replace("-", ""));
					mdlStockSummary.setUOM(lParamStockTransferDetail.getBaseUOM());
					
					model.mdlStockTransfer mdlStockTransfer = new model.mdlStockTransfer();
					mdlStockTransfer = StockTransferAdapter.GetWarehouseFromTo(lParamStockTransferDetail.getTransferID(), lParamCancelStockTransfer.getPlantID());
					if(mdlStockTransfer.getFrom() == null){
						returnImmediately(connection,"TransactionCancelStockTransfer");
						
						LogAdapter.InsertLogExc("The warehouse data not found", "TransactionCancelStockTransfer", Globals.gCommand , lUser);
		    			Globals.gReturn_Status = "Terjadi kesalahan saat menarik data gudang dari dokumen pindah gudang yang bersangkutan";
		    			
		    			return Globals.gReturn_Status;
					}
					
					//Cancel the transaction from the warehouse 1
					mdlStockSummary.setWarehouseID(mdlStockTransfer.getFrom());
					model.mdlStockSummary mdlStockSummary2 =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,lUser);
					if(mdlStockSummary2.getQty()==null){
						returnImmediately(connection,"TransactionCancelStockTransfer");
						
						LogAdapter.InsertLogExc("The stock product not found", "TransactionCancelStockTransfer", Globals.gCommand , lUser);
		    			Globals.gReturn_Status = "Produk : "+mdlStockSummary.getProductID()+",plant : "+mdlStockSummary.getPlantID()+",warehouse : "+mdlStockSummary.getWarehouseID()+",batch : "+mdlStockSummary.getBatch_No()+" tidak ditemukan di stok gudang";
		    			
		    			return Globals.gReturn_Status;
					}
					
					int newQty = Integer.parseInt(mdlStockSummary2.getQty()) + Integer.parseInt(lParamStockTransferDetail.getQty_BaseUOM());
					mdlStockSummary.setQty(String.valueOf(newQty));
					
					//					pstmUpdateStockSummaryFrom.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
					//					pstmUpdateStockSummaryFrom.setString(2,  ValidateNull.NulltoStringEmpty(lUser));
					//					pstmUpdateStockSummaryFrom.setString(3,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					//					pstmUpdateStockSummaryFrom.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary2.getPeriod()));
					//					pstmUpdateStockSummaryFrom.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
					//					pstmUpdateStockSummaryFrom.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
					//					pstmUpdateStockSummaryFrom.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
					//					pstmUpdateStockSummaryFrom.setString(8,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
					//					
					//					pstmUpdateStockSummaryFrom.executeUpdate();
					
					pstmUpdateStockSummaryFrom.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPeriod()));
					pstmUpdateStockSummaryFrom.setString(2,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
					pstmUpdateStockSummaryFrom.setString(3,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
					pstmUpdateStockSummaryFrom.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
					pstmUpdateStockSummaryFrom.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
					pstmUpdateStockSummaryFrom.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty())); 
					pstmUpdateStockSummaryFrom.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
					pstmUpdateStockSummaryFrom.setString(8,  ValidateNull.NulltoStringEmpty(lUser));
					pstmUpdateStockSummaryFrom.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmUpdateStockSummaryFrom.setString(10,  ValidateNull.NulltoStringEmpty(lUser)); 
					pstmUpdateStockSummaryFrom.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmUpdateStockSummaryFrom.setString(12,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
					pstmUpdateStockSummaryFrom.setString(13,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
					pstmUpdateStockSummaryFrom.setString(14,  ValidateNull.NulltoStringEmpty(lUser));
					pstmUpdateStockSummaryFrom.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmUpdateStockSummaryFrom.toString();
					pstmUpdateStockSummaryFrom.executeUpdate();
					
					//cancel the transaction from warehouse 2 (destination)
					mdlStockSummary.setWarehouseID(mdlStockTransfer.getTo());
					Globals.gWarehouseId = mdlStockSummary.getWarehouseID();
					
					mdlStockSummary2 =  StockSummaryAdapter.LoadStockSummaryByKey(mdlStockSummary,lUser);
					if(mdlStockSummary2.getQty()==null){
						returnImmediately(connection,"TransactionCancelStockTransfer");
						
						LogAdapter.InsertLogExc("The stock product not found", "TransactionCancelStockTransfer", Globals.gCommand , lUser);
		    			Globals.gReturn_Status = "Produk : "+mdlStockSummary.getProductID()+",plant : "+mdlStockSummary.getPlantID()+",warehouse : "+mdlStockSummary.getWarehouseID()+",batch : "+mdlStockSummary.getBatch_No()+" tidak ditemukan di stok gudang";
		    			
		    			return Globals.gReturn_Status;
					}
					
					////check if the existing qty is available for cancellation 
					if(Integer.parseInt(mdlStockSummary2.getQty()) < Integer.parseInt(lParamStockTransferDetail.getQty_BaseUOM()))
					{
						returnImmediately(connection,"TransactionCancelStockTransfer");
						
						LogAdapter.InsertLogExc("The product quantity that you cancel is more than the available quantity in the database", "TransactionCancelStockTransfer", Globals.gCommand , lUser);
		    			Globals.gReturn_Status = "Stok untuk produk : "+mdlStockSummary.getProductID()+",plant : "+mdlStockSummary.getPlantID()+",warehouse : "+mdlStockSummary.getWarehouseID()+",batch : "+mdlStockSummary.getBatch_No()+" tidak mencukupi untuk dilakukan proses pembatalan";
		    			
		    			return Globals.gReturn_Status;
					}
					
					newQty = Integer.parseInt(mdlStockSummary2.getQty()) - Integer.parseInt(lParamStockTransferDetail.getQty_BaseUOM());
					mdlStockSummary.setQty(String.valueOf(newQty));
					
					//					pstmUpdateStockSummaryTo.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
					//					pstmUpdateStockSummaryTo.setString(2,  ValidateNull.NulltoStringEmpty(lUser));
					//					pstmUpdateStockSummaryTo.setString(3,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					//					pstmUpdateStockSummaryTo.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary2.getPeriod()));
					//					pstmUpdateStockSummaryTo.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
					//					pstmUpdateStockSummaryTo.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
					//					pstmUpdateStockSummaryTo.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
					//					pstmUpdateStockSummaryTo.setString(8,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
					
					pstmUpdateStockSummaryTo.setString(1,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPeriod()));
					pstmUpdateStockSummaryTo.setString(2,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getProductID()));
					pstmUpdateStockSummaryTo.setString(3,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getPlantID()));
					pstmUpdateStockSummaryTo.setString(4,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getWarehouseID()));
					pstmUpdateStockSummaryTo.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getBatch_No()));
					pstmUpdateStockSummaryTo.setString(6,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty())); 
					pstmUpdateStockSummaryTo.setString(7,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
					pstmUpdateStockSummaryTo.setString(8,  ValidateNull.NulltoStringEmpty(lUser));
					pstmUpdateStockSummaryTo.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmUpdateStockSummaryTo.setString(10,  ValidateNull.NulltoStringEmpty(lUser)); 
					pstmUpdateStockSummaryTo.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					pstmUpdateStockSummaryTo.setString(12,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getQty()));
					pstmUpdateStockSummaryTo.setString(13,  ValidateNull.NulltoStringEmpty(mdlStockSummary.getUOM()));
					pstmUpdateStockSummaryTo.setString(14,  ValidateNull.NulltoStringEmpty(lUser));
					pstmUpdateStockSummaryTo.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstmUpdateStockSummaryTo.toString();
					pstmUpdateStockSummaryTo.executeUpdate();
				}
					
				connection.commit(); //commit transaction if all of the proccess is running well
				
				
				if(Globals.gWarehouseId.contentEquals("Website")){
					//Send API stock Ecommerce
					for(mdlStockTransferDetail lParamStockTransferDetail : lParamlistStockTransferDetail)
					{
						//Declare mdlParUptStockECM
						model.mdlParUptStockECM mdlParUptStockECM = new model.mdlParUptStockECM();
						String[] lSplitProduct = lParamStockTransferDetail.getProductID().split("-"); //split ProductId
						mdlParUptStockECM.setProduct_detail_id(lSplitProduct[1]);
						mdlParUptStockECM.setInvoice(lParamCancelStockTransfer.getDocNumber());
						mdlParUptStockECM.setQuantity("-"+lParamStockTransferDetail.getQty_BaseUOM());
						
						List<model.mdlProductDetail> mdlProductDetaillist = new ArrayList<model.mdlProductDetail>();
						mdlProductDetaillist = StockSummaryAdapter.UpdateStockAPI(mdlParUptStockECM);
						
						if (mdlProductDetaillist.isEmpty()){
								LogAdapter.InsertLogExc("Cancel not complete for product detail id : "+mdlParUptStockECM.getProduct_detail_id()+" in e-commerce server", "TransactionCancelStockTransfer", "UpdateStockAPI" , lUser);
								Globals.gReturn_Status = "Terjadi kesalahan saat mengurangi stok produk "+mdlParUptStockECM.getProduct_detail_id()+" di server e-commerce";
													
								return Globals.gReturn_Status;
						}
					}
					
					//re-sync from API
					ProductDetailAdapter.GetProductDetailAPI(Globals.user);
					Globals.gWarehouseId = "";
				}
				
				Globals.gReturn_Status = "Success Insert Cancel Stock Transfer Document";
				
			}
			catch (Exception e) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "TransactionCancelStockTransfer", Globals.gCommand , lUser);
		            	//Globals.gReturn_Status = "Error Insert Cancel Stock Transfer Document";
		            	Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertCancelStockTransfer", Globals.gCommand , lUser);
		        //Globals.gReturn_Status = "Error Insert Cancel Stock Transfer Document";
		        Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
			        if (pstmInsertCancelStockTransfer != null) {
			        	pstmInsertCancelStockTransfer.close();
			        }
			        if (pstmInsertCancelStockTransferDetail != null) {
			        	pstmInsertCancelStockTransferDetail.close();
			        }
			        if (pstmUpdateStockSummaryFrom != null) {
			        	pstmUpdateStockSummaryFrom.close();
			        }
			        if (pstmUpdateStockSummaryTo != null) {
			        	pstmUpdateStockSummaryTo.close();
			        }
			        if (pstmUpdateStockTransfer != null) {
			        	pstmUpdateStockTransfer.close();
			        }
			        connection.setAutoCommit(true);
			        if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertCancelStockTransfer", "close opened connection protocol", lUser);
				}
		    }
			
		return Globals.gReturn_Status;
	}
	//>>
	
	//<<001 Nanda
	public static String GenerateDocNoInbound()
	{
	String generateDate = LocalDateTime.now().toString();
	String lYear = generateDate.substring(0, 4);
	String txtDocNo =  "";
	model.mdlTraceCode lGetTraceCode = TraceCodeAdapter.LoadTraceCodeDocNo(lYear,"CancelInbound");

	if(lGetTraceCode.TraceCode == null)
	{
	txtDocNo = "T"+ generateDate.substring(2, 4) + "0000001";
	}
	else
	{
	int lRunningNumber = Integer.parseInt(lGetTraceCode.TraceCode.substring(3,10));
	String lnewRunningNumber = String.format("%07d", lRunningNumber + 1);	
	txtDocNo = "T"+ generateDate.substring(2, 4)+ lnewRunningNumber;
	}

	return txtDocNo;
	}

	public static String GenerateDocNoOutbound()
	{
	String generateDate = LocalDateTime.now().toString();
	String lYear = generateDate.substring(0, 4);
	String txtDocNo =  "";
	model.mdlTraceCode lGetTraceCode = TraceCodeAdapter.LoadTraceCodeDocNo(lYear,"CancelOutbound");

	if(lGetTraceCode.TraceCode == null)
	{
	txtDocNo = "K"+ generateDate.substring(2, 4) + "0000001";
	}
	else
	{
	int lRunningNumber = Integer.parseInt(lGetTraceCode.TraceCode.substring(3,10));
	String lnewRunningNumber = String.format("%07d", lRunningNumber + 1);	
	txtDocNo = "K"+ generateDate.substring(2, 4)+ lnewRunningNumber;
	}

	return txtDocNo;
	}

	public static String GenerateDocNoStockTransfer()
	{
	String generateDate = LocalDateTime.now().toString();
	String lYear = generateDate.substring(0, 4);
	String txtDocNo =  "";
	model.mdlTraceCode lGetTraceCode = TraceCodeAdapter.LoadTraceCodeDocNo(lYear,"CancelStockTransfer");

	if(lGetTraceCode.TraceCode == null)
	{
	txtDocNo = "M"+ generateDate.substring(2, 4) + "0000001";
	}
	else
	{
	int lRunningNumber = Integer.parseInt(lGetTraceCode.TraceCode.substring(3,10));
	String lnewRunningNumber = String.format("%07d", lRunningNumber + 1);	
	txtDocNo = "M"+ generateDate.substring(2, 4)+ lnewRunningNumber;
	}

	return txtDocNo;
	}
	//>>
	
	
	@SuppressWarnings("resource")
	public static List<model.mdlRptCancelTransaction> LoadReportCancelTransaction(String lTransaction,String lStartDate, String lEndDate) {
	List<model.mdlRptCancelTransaction> mdlRptCancTranslist = new ArrayList<model.mdlRptCancelTransaction>();
	JdbcRowSet jrs = new JdbcRowSetImpl();
	Globals.gCommand = "";
	try{
		jrs = database.RowSetAdapter.getJDBCRowSet();
	
		jrs.setCommand("SELECT a.DocNumber, a.`Date` , a.PlantID, a.WarehouseID, a.RefDocNumber, b.DocLine, b.ProductID,b.Qty_UOM, b.UOM, b.Qty_BaseUOM, b.BaseUOM, b.Batch_No, b.Packing_No FROM cancel_transaction a "
				+ "INNER JOIN cancel_transaction_detail b ON a.DocNumber = b.DocNumber "
				+ "WHERE a.Transaction = ? AND (a.Date BETWEEN ? AND ?) AND a.PlantID IN ("+Globals.user_area+")");
		jrs.setString(1, lTransaction);
		jrs.setString(2, lStartDate);
		jrs.setString(3, lEndDate);
		Globals.gCommand = jrs.getCommand();
		jrs.execute();
	
		while(jrs.next()){
			model.mdlRptCancelTransaction mdlRptCancelTransaction = new model.mdlRptCancelTransaction();
			mdlRptCancelTransaction.setDocNumber(jrs.getString("DocNumber"));
			mdlRptCancelTransaction.setDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
			mdlRptCancelTransaction.setPlantID(jrs.getString("PlantID"));	
			mdlRptCancelTransaction.setWarehouseID(jrs.getString("WarehouseID"));
			mdlRptCancelTransaction.setRefDocNumber(jrs.getString("RefDocNumber"));
			mdlRptCancelTransaction.setDocLine(jrs.getString("DocLine"));
			mdlRptCancelTransaction.setProductID(jrs.getString("ProductID"));
			mdlRptCancelTransaction.setQtyUOM(jrs.getString("Qty_UOM"));
			mdlRptCancelTransaction.setUOM(jrs.getString("UOM"));	
			mdlRptCancelTransaction.setQtyBaseUOM(jrs.getString("Qty_BaseUOM"));
			mdlRptCancelTransaction.setBaseUOM(jrs.getString("BaseUOM"));
			mdlRptCancelTransaction.setBatchNo(jrs.getString("Batch_No"));
			mdlRptCancelTransaction.setPackingNo(jrs.getString("Packing_No"));
		
			mdlRptCancTranslist.add(mdlRptCancelTransaction);
		}
	}
	catch(Exception ex){
		LogAdapter.InsertLogExc(ex.toString(), "LoadReportCancelTransaction", Globals.gCommand , Globals.user);
	}
	finally{
		try{
			if(jrs!=null)
				jrs.close();
		}
		catch(Exception e){
			LogAdapter.InsertLogExc(e.toString(), "LoadReportCancelTransaction", "close opened connection protocol", Globals.user);
		}
	}

	return mdlRptCancTranslist;
	}
	//>>

}
