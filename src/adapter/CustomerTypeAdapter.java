package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlCustomerType;

public class CustomerTypeAdapter {
	
	@SuppressWarnings("resource")
	public static model.mdlCustomerType LoadCustomerTypebyID(String lCustTypeID) {
		model.mdlCustomerType mdlCustomerType = new model.mdlCustomerType();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT CustomerTypeName, Description FROM customer_type WHERE CustomerTypeID = ?");
			jrs.setString(1,  lCustTypeID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				mdlCustomerType.setCustomerTypeID(lCustTypeID);
				mdlCustomerType.setCustomerTypeName(jrs.getString("CustomerTypeName"));
				mdlCustomerType.setDescription(jrs.getString("Description"));
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomerTypebyID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCustomerTypebyID", "close opened connection protocol", Globals.user);
			}
		}

		return mdlCustomerType;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlCustomerType> LoadCustomerType(String lUser) {
		List<model.mdlCustomerType> listmdlCustomerType = new ArrayList<model.mdlCustomerType>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT CustomerTypeID, CustomerTypeName, Description FROM customer_type");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlCustomerType mdlCustomerType = new model.mdlCustomerType();
				mdlCustomerType.setCustomerTypeID(jrs.getString("CustomerTypeID"));
				mdlCustomerType.setCustomerTypeName(jrs.getString("CustomerTypeName"));
				mdlCustomerType.setDescription(jrs.getString("Description"));
					
				
				listmdlCustomerType.add(mdlCustomerType);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomerType", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadCustomerType", "close opened connection protocol", lUser);
			}
		}

		return listmdlCustomerType;
	}
	
	@SuppressWarnings("resource")
	public static String InsertCustomerType(model.mdlCustomerType lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlCustomerType CheckDuplicateCustomerType = LoadCustomerTypebyID(lParam.getCustomerTypeID());
			if(CheckDuplicateCustomerType.getCustomerTypeName() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();			
				
				jrs.setCommand("SELECT CustomerTypeID, CustomerTypeName, Description FROM customer_type LIMIT 1");
				jrs.execute();
				
				jrs.moveToInsertRow();
				jrs.updateString("CustomerTypeID",lParam.getCustomerTypeID());
				jrs.updateString("CustomerTypeName",lParam.getCustomerTypeName());
				jrs.updateString("Description",lParam.getDescription());
				
				jrs.insertRow();	
				Globals.gCommand = jrs.getCommand();
				
				Globals.gReturn_Status = "Success Insert Customer Type";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Tipe pelanggan sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertCustomerType", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Insert Customer Type";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertCustomerType", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String UpdateCustomerType(model.mdlCustomerType lParam,String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT CustomerTypeID, CustomerTypeName, Description FROM customer_type WHERE CustomerTypeID=? LIMIT 1");
			jrs.setString(1,  lParam.getCustomerTypeID());
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("CustomerTypeName",lParam.getCustomerTypeName());
			jrs.updateString("Description",lParam.getDescription());
			jrs.updateRow();
			Globals.gCommand = jrs.getCommand();
				
			Globals.gReturn_Status = "Success Update Customer Type";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateCustomerType", Globals.gCommand , lUser);
//			Globals.gReturn_Status = "Error Update Customer Type";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateCustomerType", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String DeleteCustomerType(String lCustTypeID, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT CustomerTypeID, CustomerTypeName, Description FROM customer_type WHERE CustomerTypeID = ? LIMIT 1");
			jrs.setString(1, lCustTypeID);
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			Globals.gCommand = jrs.getCommand();
			
			Globals.gReturn_Status = "Success Delete Customer Type";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteCustomerType", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Delete Customer Type";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteCustomerType", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}



}
