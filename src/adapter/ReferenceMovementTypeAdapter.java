package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlReferenceMovementType;
import model.mdlStorageSectionSearch;

public class ReferenceMovementTypeAdapter {

	public static List<model.mdlReferenceMovementType> LoadReferenceMovementType() {
		List<model.mdlReferenceMovementType> listRefMovementType = new ArrayList<model.mdlReferenceMovementType>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand="";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT MovementID,MovementType,MovementDescription "
					+ "FROM wms_ref_movementtype");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlReferenceMovementType RefMovementType = new model.mdlReferenceMovementType();
				
				RefMovementType.setMovementID(jrs.getString("MovementID"));
				RefMovementType.setMovementType(jrs.getString("MovementType"));
				RefMovementType.setMovementDescription(jrs.getString("MovementDescription"));
				
				listRefMovementType.add(RefMovementType);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadReferenceMovementType", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadReferenceMovementType", "close opened connection protocol", Globals.user);
			}
		}

		return listRefMovementType;
	}

	public static model.mdlReferenceMovementType LoadRefMovementTypeSearchByKey(String lMovementID) {
		model.mdlReferenceMovementType mdlReferenceMovementType = new model.mdlReferenceMovementType();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT MovementID, MovementType, MovementDescription FROM wms_ref_movementtype "
						+ "WHERE MovementID = ?");
			jrs.setString(1,  lMovementID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlReferenceMovementType.setMovementID(jrs.getString("MovementID"));
				mdlReferenceMovementType.setMovementType(jrs.getString("MovementType"));
				mdlReferenceMovementType.setMovementDescription(jrs.getString("MovementDescription"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadRefMovementTypeSearchByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadRefMovementTypeSearchByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlReferenceMovementType;
	}
	
	public static String DeleteReferenceMovementType(String lMovementID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT MovementID, MovementType, MovementDescription "
							+ "FROM wms_ref_movementtype WHERE MovementID = ? LIMIT 1");
			jrs.setString(1, lMovementID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Reference Movement Type";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteReferenceMovementType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteReferenceMovementType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	public static String InsertReferenceMovementType(model.mdlReferenceMovementType lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlReferenceMovementType Check = LoadRefMovementTypeSearchByKey(lParam.getMovementID());
			if(Check.getMovementID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT MovementID, MovementType, MovementDescription "
						+ "FROM wms_ref_movementtype LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("MovementID",lParam.getMovementID());
				jrs.updateString("MovementType",lParam.getMovementType());
				jrs.updateString("MovementDescription",lParam.getMovementDescription());
				
				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Reference Movement Type";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Reference movement type sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertReferenceMovementType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertReferenceMovementType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	public static String UpdateReferenceMovementType(model.mdlReferenceMovementType lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT MovementID, MovementType, MovementDescription "
						+ "FROM wms_ref_movementtype WHERE MovementID = ? LIMIT 1");
			jrs.setString(1,  lParam.getMovementID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("MovementType", lParam.getMovementType());
			jrs.updateString("MovementDescription", lParam.getMovementDescription());
			
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Reference Movement Type";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateReferenceMovementType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateReferenceMovementType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
}
