package adapter;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.google.gson.Gson;

import model.Globals;

/** Documentation
 * 
 */
public class TokenAdapter {
	
	public static model.mdlToken GetToken()
	{	
		model.mdlToken mdlToken = new model.mdlToken();
		try {
			Client client = Client.create();
			
			Globals.gCommand = "http://dev.webarq.info:8081/kenmaster-be/api/token";
			WebResource webResource = client.resource(Globals.gCommand);
			
			model.mdlGetToken mdlGetToken = new model.mdlGetToken();
			mdlGetToken.setUsername("kenmaster");
			mdlGetToken.setPassword("kenmaster");
			
			//String json = "{\"Username\":\"test\",\"Password\":\"123\",\"Token\":\"1234567890987654321\"}";
			
			Gson gson = new Gson();
			String json = gson.toJson(mdlGetToken);
			
			ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);		
			
			//close code karena status balikan dari ken master = 200
			//if (response.getStatus() != 201) {
			//throw new RuntimeException("Failed : HTTP error code : "
			//+ response.getStatus());
			//}
			
			Globals.gReturn_Status = response.getEntity(String.class);
			mdlToken = gson.fromJson(Globals.gReturn_Status, model.mdlToken.class);
		  
		} 
		catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetToken", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Error GetToken";
		  }
		
		return mdlToken;
	}
	
}
