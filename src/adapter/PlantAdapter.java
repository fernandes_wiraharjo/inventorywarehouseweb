package adapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import javax.swing.JOptionPane;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlPlant;

public class PlantAdapter {
	
	@SuppressWarnings("resource")
	public static List<model.mdlPlant> LoadPlant (String lUser) {
		List<model.mdlPlant> listmdlPlant = new ArrayList<model.mdlPlant>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand="";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID,PlantName,PlantDesc FROM Plant "
					+ "ORDER BY PlantID");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlPlant mdlPlant = new model.mdlPlant();
				mdlPlant.setPlantID(jrs.getString("PlantID"));
				mdlPlant.setPlantNm(jrs.getString("PlantName"));
				mdlPlant.setDesc(jrs.getString("PlantDesc"));
				
				listmdlPlant.add(mdlPlant);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadPlant", Globals.gCommand , lUser);
		}
		finally {
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "LoadPlant", "close opened connection protocol", lUser);
			}
		}
		
		return listmdlPlant;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlPlant> LoadPlantByRole() {
		List<model.mdlPlant> listmdlPlant = new ArrayList<model.mdlPlant>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand="";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID,PlantName,PlantDesc FROM Plant WHERE PlantID IN ("+Globals.user_area+") ORDER BY PlantID");
			//jrs.setString(1, Globals.user_area);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				model.mdlPlant mdlPlant = new model.mdlPlant();
				mdlPlant.setPlantID(jrs.getString("PlantID"));
				mdlPlant.setPlantNm(jrs.getString("PlantName"));
				mdlPlant.setDesc(jrs.getString("PlantDesc"));
				
				listmdlPlant.add(mdlPlant);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadPlantByRole", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "LoadPlantByRole", "close opened connection protocol", Globals.user);
			}
		}
		
		return listmdlPlant;
	}

	@SuppressWarnings("resource")
	public static model.mdlPlant LoadPlantByID (String lPlantID) {
		model.mdlPlant mdlPlant = new model.mdlPlant();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand="";
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID,PlantName,PlantDesc FROM Plant WHERE PlantID=?");
			jrs.setString(1, lPlantID);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				mdlPlant.setPlantID(jrs.getString("PlantID"));
				mdlPlant.setPlantNm(jrs.getString("PlantName"));
				mdlPlant.setDesc(jrs.getString("PlantDesc"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadPlantByID", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "LoadPlantByID", "close opened connection protocol", Globals.user);
			}
		}
		
		return mdlPlant;
	}
	
	@SuppressWarnings("resource")
	public static String InsertPlant(mdlPlant lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlPlant CheckDuplicatePlant = LoadPlantByID(lParam.getPlantID());
			if(CheckDuplicatePlant.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();			
				
				jrs.setCommand("SELECT PlantID,PlantName,PlantDesc FROM plant LIMIT 1");
				jrs.execute();
				
				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("PlantName", lParam.getPlantNm());
				jrs.updateString("PlantDesc", lParam.getDesc());
				
				jrs.insertRow();
				
				Globals.gCommand = jrs.getCommand();
				
				Globals.gReturn_Status = "Success Insert Plant";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Plant sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertPlant", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Insert Plant";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertPlant", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String UpdatePlant(mdlPlant lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT PlantID,PlantName,PlantDesc FROM plant WHERE PlantID = ? LIMIT 1");
			jrs.setString(1, lParam.getPlantID());
		
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("PlantName", lParam.getPlantNm());
			jrs.updateString("PlantDesc", lParam.getDesc());
			jrs.updateRow();
			
			Globals.gCommand = jrs.getCommand();
			
			Globals.gReturn_Status = "Success Update Plant";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdatePlant", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Update Plant";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdatePlant", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeletePlant(String id, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();	

			jrs.setCommand("SELECT PlantID,PlantName,PlantDesc FROM plant WHERE PlantID = ? LIMIT 1");
			jrs.setString(1, id);
			Globals.gCommand = jrs.getCommand();
		
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			Globals.gReturn_Status = "Success Delete Plant";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeletePlant", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Delete Plant";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeletePlant", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
}
