package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlStorageSection;;

public class StorageSectionAdapter {

	@SuppressWarnings("resource")
	public static model.mdlStorageSection LoadStorageSectionByKey(String lPlantID, String lWarehouseID, String lStorageTypeID, String lStorageSectionID) {
		model.mdlStorageSection mdlStorageSection = new model.mdlStorageSection();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeID, StorageSectionID, StorageSectionName FROM wms_storagesection "
							+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageTypeID=? AND StorageSectionID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lStorageTypeID);
			jrs.setString(4,  lStorageSectionID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlStorageSection.setPlantID(jrs.getString("PlantID"));
				mdlStorageSection.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageSection.setStorageTypeID(jrs.getString("StorageTypeID"));
				mdlStorageSection.setStorageSectionID(jrs.getString("StorageSectionID"));
				mdlStorageSection.setStorageSectionName(jrs.getString("StorageSectionName"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageSectionByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageSectionByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlStorageSection;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlStorageSection> LoadStorageSection() {
		List<model.mdlStorageSection> listmdlStorageSection = new ArrayList<model.mdlStorageSection>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT a.PlantID, c.PlantName, a.WarehouseID, d.WarehouseName , a.StorageTypeID, b.StorageTypeName , a.StorageSectionID, a.StorageSectionName "
					+ "FROM wms_storagesection a "
					+ "INNER JOIN wms_storage_type b ON b.StorageTypeID=a.StorageTypeID AND b.PlantID=a.PlantID AND b.WarehouseID=a.WarehouseID "
					+ "INNER JOIN plant c ON c.PlantID=a.PlantID "
					+ "INNER JOIN warehouse d ON d.PlantID=a.PlantID AND d.WarehouseID=a.WarehouseID "
					+ "WHERE a.PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next()){
				model.mdlStorageSection mdlStorageSection = new model.mdlStorageSection();
				mdlStorageSection.setPlantID(jrs.getString("PlantID"));
				mdlStorageSection.setPlantName(jrs.getString("PlantName"));
				mdlStorageSection.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageSection.setWarehouseName(jrs.getString("WarehouseName"));
				mdlStorageSection.setStorageTypeID(jrs.getString("StorageTypeID"));
				mdlStorageSection.setStorageTypeName(jrs.getString("StorageTypeName"));
				mdlStorageSection.setStorageSectionID(jrs.getString("StorageSectionID"));
				mdlStorageSection.setStorageSectionName(jrs.getString("StorageSectionName"));

				listmdlStorageSection.add(mdlStorageSection);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageSection", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageSection", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlStorageSection;
	}

	public static List<model.mdlStorageSection> LoadDynamicStorageSection(String lPlantID, String lWarehouseID, String lStorageTypeID) {
		List<model.mdlStorageSection> listmdlStorageSection = new ArrayList<model.mdlStorageSection>();

		Connection connection = null;
		PreparedStatement pstmLoadStorageSection = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();

			String sqlLoadStorageSection = "SELECT StorageSectionID, StorageSectionName FROM wms_storagesection "
					+ "WHERE PlantID=? AND WarehouseID=? AND StorageTypeID=?";

			pstmLoadStorageSection = connection.prepareStatement(sqlLoadStorageSection);
			pstmLoadStorageSection.setString(1,  lPlantID);
			pstmLoadStorageSection.setString(2,  lWarehouseID);
			pstmLoadStorageSection.setString(3,  lStorageTypeID);

			Globals.gCommand = pstmLoadStorageSection.toString();

			jrs = pstmLoadStorageSection.executeQuery();

			while(jrs.next()){
				model.mdlStorageSection mdlStorageSection = new model.mdlStorageSection();
				mdlStorageSection.setStorageSectionID(jrs.getString("StorageSectionID"));
				mdlStorageSection.setStorageSectionName(jrs.getString("StorageSectionName"));

				listmdlStorageSection.add(mdlStorageSection);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDynamicStorageSection", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstmLoadStorageSection != null) {
					 pstmLoadStorageSection.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDynamicStorageSection", "close opened connection protocol", Globals.user);
			}
		 }

		return listmdlStorageSection;
	}

	public static List<model.mdlStorageSection> LoadDynamicStorageSection2(String lPlantID, String lWarehouseID, String lStorageTypeID, String listSeq) {
		List<model.mdlStorageSection> listmdlStorageSection = new ArrayList<model.mdlStorageSection>();

		Connection connection = null;
		PreparedStatement pstmLoadStorageSection = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();

			String sqlLoadStorageSection = "SELECT StorageTypeID, StorageSectionID, StorageSectionName FROM wms_storagesection "
					+ "WHERE PlantID=? AND WarehouseID=? AND StorageTypeID=? AND StorageSectionID NOT IN ("+listSeq+")";

			pstmLoadStorageSection = connection.prepareStatement(sqlLoadStorageSection);
			pstmLoadStorageSection.setString(1,  lPlantID);
			pstmLoadStorageSection.setString(2,  lWarehouseID);
			pstmLoadStorageSection.setString(3,  lStorageTypeID);

			Globals.gCommand = pstmLoadStorageSection.toString();

			jrs = pstmLoadStorageSection.executeQuery();

			while(jrs.next()){
				model.mdlStorageSection mdlStorageSection = new model.mdlStorageSection();
				mdlStorageSection.setStorageTypeID(jrs.getString("StorageTypeID"));
				mdlStorageSection.setStorageSectionID(jrs.getString("StorageSectionID"));
				mdlStorageSection.setStorageSectionName(jrs.getString("StorageSectionName"));

				listmdlStorageSection.add(mdlStorageSection);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDynamicStorageSection2", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				 if (pstmLoadStorageSection != null) {
					 pstmLoadStorageSection.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDynamicStorageSection2", "close opened connection protocol", Globals.user);
			}
		 }

		return listmdlStorageSection;
	}
	
	@SuppressWarnings("resource")
	public static String InsertStorageSection(model.mdlStorageSection lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlStorageSection CheckStorageSection = LoadStorageSectionByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getStorageTypeID() ,lParam.getStorageSectionID());
			if(CheckStorageSection.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeID, StorageSectionID, StorageSectionName FROM wms_storagesection LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("StorageTypeID",lParam.getStorageTypeID());
				jrs.updateString("StorageSectionID",lParam.getStorageSectionID());
				jrs.updateString("StorageSectionName",lParam.getStorageSectionName());

				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Storage Section";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Storage section sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertStorageSection", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertStorageSection", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String UpdateStorageSection(model.mdlStorageSection lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeID, StorageSectionID, StorageSectionName FROM wms_storagesection WHERE PlantID = ? "
							+ "AND WarehouseID = ? AND StorageTypeID=? AND StorageSectionID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getStorageTypeID());
			jrs.setString(4,  lParam.getStorageSectionID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("StorageTypeID", lParam.getStorageTypeID());
			jrs.updateString("StorageSectionName", lParam.getStorageSectionName());
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Storage Section";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStorageSection", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateStorageSection", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeleteStorageSection(String lPlantID, String lWarehouseID, String lStorageSectionID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeID, StorageSectionID FROM wms_storagesection WHERE PlantID = ? "
							+ "AND WarehouseID = ? AND StorageSectionID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lStorageSectionID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Storage Section";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteStorageSection", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteStorageSection", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
}
