package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlStorageSectionSearch;
import model.mdlStorageTypeSearch;

public class StorageSectionSearchAdapter {

	public static List<model.mdlStorageSectionSearch> LoadStorageSectionSearch() {
		List<model.mdlStorageSectionSearch> listStorageSectionSearch = new ArrayList<model.mdlStorageSectionSearch>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand="";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT a.PlantID,b.PlantName,a.WarehouseID,c.WarehouseName,a.StorageTypeID,d.StorageTypeName, "
					+ "a.StorageSectionIndicatorID,e.StorageSectionIndicatorName, "
					+ "Sequence1,Sequence2,Sequence3,Sequence4,Sequence5,Sequence6,Sequence7,Sequence8,Sequence9,Sequence10, "
					+ "Sequence11,Sequence12,Sequence13,Sequence14,Sequence15,Sequence16,Sequence17,Sequence18,Sequence19,Sequence20, "
					+ "Sequence21,Sequence22,Sequence23,Sequence24,Sequence25 "
					+ "FROM wms_storagesection_search a "
					+ "INNER JOIN plant b ON b.PlantID=a.PlantID "
					+ "INNER JOIN warehouse c ON c.PlantID=a.PlantID AND c.WarehouseID=a.WarehouseID "
					+ "INNER JOIN wms_storage_type d ON d.PlantID=a.PlantID AND d.WarehouseID=a.WarehouseID AND d.StorageTypeID=a.StorageTypeID  "
					+ "INNER JOIN wms_storagesectionindicator e ON e.PlantID=a.PlantID AND e.WarehouseID=a.WarehouseID AND e.StorageSectionIndicatorID=a.StorageSectionIndicatorID "
					+ "WHERE a.PlantID IN (" + Globals.user_area + ")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlStorageSectionSearch StorageSectionSearch = new model.mdlStorageSectionSearch();
				
				StorageSectionSearch.setPlantID(jrs.getString("PlantID"));
				StorageSectionSearch.setPlantName(jrs.getString("PlantName"));
				StorageSectionSearch.setWarehouseID(jrs.getString("WarehouseID"));
				StorageSectionSearch.setWarehouseName(jrs.getString("WarehouseName"));
				StorageSectionSearch.setStorageTypeID(jrs.getString("StorageTypeID"));
				StorageSectionSearch.setStorageTypeName(jrs.getString("StorageTypeName"));
				StorageSectionSearch.setStorageSectionIndicatorID(jrs.getString("StorageSectionIndicatorID"));
				StorageSectionSearch.setStorageSectionIndicatorName(jrs.getString("StorageSectionIndicatorName"));
				StorageSectionSearch.setStorageSection1(jrs.getString("Sequence1"));
				StorageSectionSearch.setStorageSection2(jrs.getString("Sequence2"));
				StorageSectionSearch.setStorageSection3(jrs.getString("Sequence3"));
				StorageSectionSearch.setStorageSection4(jrs.getString("Sequence4"));
				StorageSectionSearch.setStorageSection5(jrs.getString("Sequence5"));
				StorageSectionSearch.setStorageSection6(jrs.getString("Sequence6"));
				StorageSectionSearch.setStorageSection7(jrs.getString("Sequence7"));
				StorageSectionSearch.setStorageSection8(jrs.getString("Sequence8"));
				StorageSectionSearch.setStorageSection9(jrs.getString("Sequence9"));
				StorageSectionSearch.setStorageSection10(jrs.getString("Sequence10"));
				StorageSectionSearch.setStorageSection11(jrs.getString("Sequence11"));
				StorageSectionSearch.setStorageSection12(jrs.getString("Sequence12"));
				StorageSectionSearch.setStorageSection13(jrs.getString("Sequence13"));
				StorageSectionSearch.setStorageSection14(jrs.getString("Sequence14"));
				StorageSectionSearch.setStorageSection15(jrs.getString("Sequence15"));
				StorageSectionSearch.setStorageSection16(jrs.getString("Sequence16"));
				StorageSectionSearch.setStorageSection17(jrs.getString("Sequence17"));
				StorageSectionSearch.setStorageSection18(jrs.getString("Sequence18"));
				StorageSectionSearch.setStorageSection19(jrs.getString("Sequence19"));
				StorageSectionSearch.setStorageSection20(jrs.getString("Sequence20"));
				StorageSectionSearch.setStorageSection21(jrs.getString("Sequence21"));
				StorageSectionSearch.setStorageSection22(jrs.getString("Sequence22"));
				StorageSectionSearch.setStorageSection23(jrs.getString("Sequence23"));
				StorageSectionSearch.setStorageSection24(jrs.getString("Sequence24"));
				StorageSectionSearch.setStorageSection25(jrs.getString("Sequence25"));
				
				listStorageSectionSearch.add(StorageSectionSearch);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageSectionSearch", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageSectionSearch", "close opened connection protocol", Globals.user);
			}
		}

		return listStorageSectionSearch;
	}

	public static model.mdlStorageSectionSearch LoadStorageSectionSearchByKey(String lPlantID, String lWarehouseID, String lStorageTypeID, String lStorageSectionIndicatorID) {
		model.mdlStorageSectionSearch mdlStorageSectionSearch = new model.mdlStorageSectionSearch();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeID, StorageSectionIndicatorID FROM wms_storagesection_search "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageTypeID = ? AND StorageSectionIndicatorID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lStorageTypeID);
			jrs.setString(4,  lStorageSectionIndicatorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlStorageSectionSearch.setPlantID(jrs.getString("PlantID"));
				mdlStorageSectionSearch.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageSectionSearch.setStorageTypeID(jrs.getString("StorageTypeID"));
				mdlStorageSectionSearch.setStorageSectionIndicatorID(jrs.getString("StorageSectionIndicatorID"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageSectionSearchByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageSectionSearchByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlStorageSectionSearch;
	}
	
	public static String DeleteStorageSectionSearch(String lPlantID, String lWarehouseID, String lStorageTypeID, String lStorageSectionIndicatorID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeID, StorageSectionIndicatorID "
							+ "FROM wms_storagesection_search WHERE PlantID = ? AND WarehouseID = ? AND StorageTypeID = ? AND StorageSectionIndicatorID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lStorageTypeID);
			jrs.setString(4, lStorageSectionIndicatorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Storage Section Search";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteStorageSectionSearch", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteStorageSectionSearch", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	public static String InsertStorageSectionSearch(model.mdlStorageSectionSearch lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlStorageSectionSearch Check = LoadStorageSectionSearchByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getStorageTypeID(), lParam.getStorageSectionIndicatorID());
			if(Check.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeID, StorageSectionIndicatorID, Sequence1, Sequence2, "
						+ "Sequence3, Sequence4, Sequence5, Sequence6, "
						+ "Sequence7, Sequence8, Sequence9, Sequence10, Sequence11, Sequence12, "
						+ "Sequence13, Sequence14, Sequence15, Sequence16, "
						+ "Sequence17, Sequence18, Sequence19,Sequence20, Sequence21, Sequence22, "
						+ "Sequence23, Sequence24, Sequence25 "
						+ "FROM wms_storagesection_search LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("StorageTypeID",lParam.getStorageTypeID());
				jrs.updateString("StorageSectionIndicatorID",lParam.getStorageSectionIndicatorID());
				jrs.updateString("Sequence1",lParam.getStorageSection1());
				jrs.updateString("Sequence2",lParam.getStorageSection2());
				jrs.updateString("Sequence3",lParam.getStorageSection3());
				jrs.updateString("Sequence4",lParam.getStorageSection4());
				jrs.updateString("Sequence5",lParam.getStorageSection5());
				jrs.updateString("Sequence6",lParam.getStorageSection6());
				jrs.updateString("Sequence7",lParam.getStorageSection7());
				jrs.updateString("Sequence8",lParam.getStorageSection8());
				jrs.updateString("Sequence9",lParam.getStorageSection9());
				jrs.updateString("Sequence10",lParam.getStorageSection10());
				jrs.updateString("Sequence11",lParam.getStorageSection11());
				jrs.updateString("Sequence12",lParam.getStorageSection12());
				jrs.updateString("Sequence13",lParam.getStorageSection13());
				jrs.updateString("Sequence14",lParam.getStorageSection14());
				jrs.updateString("Sequence15",lParam.getStorageSection15());
				jrs.updateString("Sequence16",lParam.getStorageSection16());
				jrs.updateString("Sequence17",lParam.getStorageSection17());
				jrs.updateString("Sequence18",lParam.getStorageSection18());
				jrs.updateString("Sequence19",lParam.getStorageSection19());
				jrs.updateString("Sequence20",lParam.getStorageSection20());
				jrs.updateString("Sequence21",lParam.getStorageSection21());
				jrs.updateString("Sequence22",lParam.getStorageSection22());
				jrs.updateString("Sequence23",lParam.getStorageSection23());
				jrs.updateString("Sequence24",lParam.getStorageSection24());
				jrs.updateString("Sequence25",lParam.getStorageSection25());
				
				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Storage Section Search";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Storage section search sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertStorageSectionSearch", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertStorageSectionSearch", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	public static String UpdateStorageSectionSearch(model.mdlStorageSectionSearch lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeID, StorageSectionIndicatorID, Sequence1, Sequence2, "
						+ "Sequence3, Sequence4, Sequence5, Sequence6, "
						+ "Sequence7, Sequence8, Sequence9, Sequence10, Sequence11, Sequence12, "
						+ "Sequence13, Sequence14, Sequence15, Sequence16, "
						+ "Sequence17, Sequence18, Sequence19,Sequence20, Sequence21, Sequence22, "
						+ "Sequence23, Sequence24, Sequence25 "
						+ "FROM wms_storagesection_search WHERE PlantID = ? AND WarehouseID = ? AND StorageTypeID = ? AND StorageSectionIndicatorID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getStorageTypeID());
			jrs.setString(4,  lParam.getStorageSectionIndicatorID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("Sequence1", lParam.getStorageSection1());
			jrs.updateString("Sequence2", lParam.getStorageSection2());
			jrs.updateString("Sequence3", lParam.getStorageSection3());
			jrs.updateString("Sequence4", lParam.getStorageSection4());
			jrs.updateString("Sequence5", lParam.getStorageSection5());
			jrs.updateString("Sequence6", lParam.getStorageSection6());
			jrs.updateString("Sequence7", lParam.getStorageSection7());
			jrs.updateString("Sequence8", lParam.getStorageSection8());
			jrs.updateString("Sequence9", lParam.getStorageSection9());
			jrs.updateString("Sequence10", lParam.getStorageSection10());
			jrs.updateString("Sequence11", lParam.getStorageSection11());
			jrs.updateString("Sequence12", lParam.getStorageSection12());
			jrs.updateString("Sequence13", lParam.getStorageSection13());
			jrs.updateString("Sequence14", lParam.getStorageSection14());
			jrs.updateString("Sequence15", lParam.getStorageSection15());
			jrs.updateString("Sequence16", lParam.getStorageSection16());
			jrs.updateString("Sequence17", lParam.getStorageSection17());
			jrs.updateString("Sequence18", lParam.getStorageSection18());
			jrs.updateString("Sequence19", lParam.getStorageSection19());
			jrs.updateString("Sequence20", lParam.getStorageSection20());
			jrs.updateString("Sequence21", lParam.getStorageSection21());
			jrs.updateString("Sequence22", lParam.getStorageSection22());
			jrs.updateString("Sequence23", lParam.getStorageSection23());
			jrs.updateString("Sequence24", lParam.getStorageSection24());
			jrs.updateString("Sequence25", lParam.getStorageSection25());
			
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Storage Section Search";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStorageSectionSearch", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateStorageSectionSearch", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
}
