package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlWarehouse;

public class WarehouseAdapter {

	@SuppressWarnings("resource")
	public static List<model.mdlWarehouse> LoadWarehousebyPlantID(String lPlantID) {
		List<model.mdlWarehouse> listmdlWarehouse = new ArrayList<model.mdlWarehouse>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT PlantID, WarehouseID, WarehouseName, WarehouseDesc FROM warehouse WHERE PlantID = ?");
			jrs.setString(1,  lPlantID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next())
			{
			model.mdlWarehouse mdlWarehouse = new model.mdlWarehouse();
			
			mdlWarehouse.setPlantID(jrs.getString("PlantID"));
			mdlWarehouse.setWarehouseID(jrs.getString("WarehouseID"));
			mdlWarehouse.setWarehouseName(jrs.getString("WarehouseName"));
			mdlWarehouse.setWarehouseDesc(jrs.getString("WarehouseDesc"));	
			
			listmdlWarehouse.add(mdlWarehouse);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadWarehousebyPlantID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadWarehousebyPlantID", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlWarehouse;
	}

	@SuppressWarnings("resource")
	public static model.mdlWarehouse LoadWarehouseByKey(String lPlantID, String lWarehouseID) {
		model.mdlWarehouse mdlWarehouse = new model.mdlWarehouse();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT PlantID, WarehouseID, WarehouseName, WarehouseDesc FROM warehouse WHERE PlantID = ? AND WarehouseID=?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next())
			{
				mdlWarehouse.setPlantID(jrs.getString("PlantID"));
				mdlWarehouse.setWarehouseID(jrs.getString("WarehouseID"));
				mdlWarehouse.setWarehouseName(jrs.getString("WarehouseName"));
				mdlWarehouse.setWarehouseDesc(jrs.getString("WarehouseDesc"));	
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadWarehouseByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadWarehouseByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlWarehouse;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlWarehouse> LoadWarehouse(String lUser) {
		List<model.mdlWarehouse> listmdlWarehouse = new ArrayList<model.mdlWarehouse>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT PlantID, WarehouseID, WarehouseName, WarehouseDesc FROM warehouse");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlWarehouse mdlWarehouse = new model.mdlWarehouse();
				mdlWarehouse.setPlantID(jrs.getString("PlantID"));
				mdlWarehouse.setWarehouseID(jrs.getString("WarehouseID"));
				mdlWarehouse.setWarehouseName(jrs.getString("WarehouseName"));
				mdlWarehouse.setWarehouseDesc(jrs.getString("WarehouseDesc"));			
				
				listmdlWarehouse.add(mdlWarehouse);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadWarehouse", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadWarehouse", "close opened connection protocol", lUser);
			}
		}

		return listmdlWarehouse;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlWarehouse> LoadWarehouseExceptECM(String lPlantID) {
		List<model.mdlWarehouse> listmdlWarehouse = new ArrayList<model.mdlWarehouse>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT PlantID, WarehouseID, WarehouseName, WarehouseDesc "
					+ "FROM warehouse "
					+ "WHERE WarehouseID != 'Website' AND PlantID=?");
			jrs.setString(1,  lPlantID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlWarehouse mdlWarehouse = new model.mdlWarehouse();
				mdlWarehouse.setPlantID(jrs.getString("PlantID"));
				mdlWarehouse.setWarehouseID(jrs.getString("WarehouseID"));
				mdlWarehouse.setWarehouseName(jrs.getString("WarehouseName"));
				mdlWarehouse.setWarehouseDesc(jrs.getString("WarehouseDesc"));			
				
				listmdlWarehouse.add(mdlWarehouse);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadWarehouse", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadWarehouse", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlWarehouse;
	}
	
	
	public static List<model.mdlWarehouse> LoadWarehouseJoin(String lUser) {
		List<model.mdlWarehouse> listmdlWarehouse = new ArrayList<model.mdlWarehouse>();
		
		Connection connection = null;
		PreparedStatement pstmLoadWarehouse = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadWarehouse = "SELECT a.PlantID, a.WarehouseID, a.WarehouseName, a.WarehouseDesc, a.WMS_Status, a.UpdateWMFirst, b.PlantName "
					+ "FROM warehouse a "
					+ "LEFT JOIN plant b ON b.PlantID = a.PlantID "
					+ "WHERE a.PlantID IN ("+Globals.user_area+")";
			
			pstmLoadWarehouse = connection.prepareStatement(sqlLoadWarehouse);
			//pstmLoadWarehouse.setString(1,  Globals.user_area);
			
			Globals.gCommand = pstmLoadWarehouse.toString();
			
			jrs = pstmLoadWarehouse.executeQuery();
									
			while(jrs.next()){
				model.mdlWarehouse mdlWarehouse = new model.mdlWarehouse();
				mdlWarehouse.setPlantID(jrs.getString("PlantID"));
				mdlWarehouse.setWarehouseID(jrs.getString("WarehouseID"));
				mdlWarehouse.setWarehouseName(jrs.getString("WarehouseName"));
				mdlWarehouse.setWarehouseDesc(jrs.getString("WarehouseDesc"));
				mdlWarehouse.setWMS_Status(jrs.getBoolean("WMS_Status"));
				mdlWarehouse.setUpdateWMFirst(jrs.getBoolean("UpdateWMFirst"));
				mdlWarehouse.setPlantName(jrs.getString("PlantName"));
				
				listmdlWarehouse.add(mdlWarehouse);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadWarehouseJoin", Globals.gCommand , lUser);
		}
		finally {
			try{
				 if (pstmLoadWarehouse != null) {
					 pstmLoadWarehouse.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadWarehouseJoin", "close opened connection protocol", lUser);
			}
		 }

		return listmdlWarehouse;
	}
	
	@SuppressWarnings("resource")
	public static String InsertWarehouse(model.mdlWarehouse lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlWarehouse CheckWarehouse = LoadWarehouseByKey(lParam.getPlantID(), lParam.getWarehouseID());
			if(CheckWarehouse.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();			
				
				jrs.setCommand("SELECT PlantID, WarehouseID, WarehouseName, WarehouseDesc, WMS_Status, UpdateWMFirst FROM warehouse LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
				
				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("WarehouseName",lParam.getWarehouseName());
				jrs.updateString("WarehouseDesc",lParam.getWarehouseDesc());
				jrs.updateBoolean("WMS_Status",lParam.getWMS_Status());
				jrs.updateBoolean("UpdateWMFirst",lParam.getUpdateWMFirst());
				
				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Warehouse";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Gudang sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertWarehouse", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertWarehouse", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String UpdateWarehouse(model.mdlWarehouse lParam, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT PlantID, WarehouseID, WarehouseName, WarehouseDesc, WMS_Status, UpdateWMFirst FROM warehouse WHERE PlantID = ? AND WarehouseID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("WarehouseName", lParam.getWarehouseName());
			jrs.updateString("WarehouseDesc", lParam.getWarehouseDesc());
			jrs.updateBoolean("WMS_Status", lParam.getWMS_Status());
			jrs.updateBoolean("UpdateWMFirst", lParam.getUpdateWMFirst());
			jrs.updateRow();
				
			Globals.gReturn_Status = "Success Update Warehouse";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateWarehouse", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateWarehouse", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String DeleteWarehouse(String lPlantID, String lWarehouseID, String lUser)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT PlantID, WarehouseID, WarehouseName, WarehouseDesc FROM warehouse WHERE PlantID = ? AND WarehouseID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			Globals.gReturn_Status = "Success Delete Warehouse";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteWarehouse", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteWarehouse", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}

	public static String GetWMStatus(String lPlantID, String lWarehouseID) {
		String Status = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT WMS_Status,UpdateWMFirst, PlantID, WarehouseID "
					+ "FROM warehouse WHERE PlantID = ? AND WarehouseID=?");
			jrs.setString(1,  ValidateNull.NulltoStringEmpty(lPlantID));
			jrs.setString(2,  ValidateNull.NulltoStringEmpty(lWarehouseID));
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next())
			{
				if(jrs.getBoolean(1))
					Status = "1";
				else
					Status = "0";
					
				if(jrs.getBoolean(2))
					Status += "--1";
				else
					Status += "--0";
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetWMStatus", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "GetWMStatus", "close opened connection protocol", Globals.user);
			}
		}

		return Status;
	}
	
}
