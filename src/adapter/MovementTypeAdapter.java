package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlMovementType;
import model.mdlStorageBin;

public class MovementTypeAdapter {

	public static List<model.mdlMovementType> LoadMovementType() {
		List<model.mdlMovementType> listData = new ArrayList<model.mdlMovementType>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT a.PlantID,b.PlantName,a.WarehouseID,c.WarehouseName,a.MovementID,d.MovementDescription, "
					+ "a.StorageTypeIDSrc,a.StorageTypeIDDest,a.StorageTypeIDRet, "
					+ "a.StorageBinIDSrc,a.StorageBinIDDest,a.StorageBinIDRet,a.FxdBnSrc,a.FxdBnDest,a.Scr_Src,a.Scr_Dest "
					+ "FROM wms_movement_type a "
					+ "INNER JOIN plant b ON b.PlantID=a.PlantID "
					+ "INNER JOIN warehouse c ON c.PlantID=a.PlantID AND c.WarehouseID=a.WarehouseID "
					+ "INNER JOIN wms_ref_movementtype d ON d.MovementID=a.MovementID "
					+ "WHERE a.PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next()){
				model.mdlMovementType Data = new model.mdlMovementType();
				Data.setPlantID(jrs.getString("PlantID"));
				Data.setPlantName(jrs.getString("PlantName"));
				Data.setWarehouseID(jrs.getString("WarehouseID"));
				Data.setWarehouseName(jrs.getString("WarehouseName"));
				Data.setMovementID(jrs.getString("MovementID"));
				Data.setMovementName(jrs.getString("MovementDescription"));
				Data.setStorageTypeSource(jrs.getString("StorageTypeIDSrc"));
				Data.setStorageTypeDestination(jrs.getString("StorageTypeIDDest"));
				Data.setStorageTypeReturn(jrs.getString("StorageTypeIDRet"));
				Data.setStorageBinSource(jrs.getString("StorageBinIDSrc"));
				Data.setStorageBinDestination(jrs.getString("StorageBinIDDest"));
				Data.setStorageBinReturn(jrs.getString("StorageBinIDRet"));
				Data.setFxdBinSource(jrs.getBoolean("FxdBnSrc"));
				Data.setFxdBinDestination(jrs.getBoolean("FxdBnDest"));
				Data.setScrSource(jrs.getBoolean("Scr_Src"));
				Data.setScrDestination(jrs.getBoolean("Scr_Dest"));
				
				listData.add(Data);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMovementType", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadMovementType", "close opened connection protocol", Globals.user);
			}
		}

		return listData;
	}
	
	public static model.mdlMovementType LoadMovementTypeByKey(String lPlantID, String lWarehouseID, String lMovementID) {
		model.mdlMovementType mdlMovementType = new model.mdlMovementType();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, MovementID FROM wms_movement_type "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND MovementID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lMovementID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlMovementType.setPlantID(jrs.getString("PlantID"));
				mdlMovementType.setWarehouseID(jrs.getString("WarehouseID"));
				mdlMovementType.setMovementID(jrs.getString("MovementID"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMovementTypeByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadMovementTypeByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlMovementType;
	}
	
	public static String DeleteMovementType(String lPlantID, String lWarehouseID, String lMovementID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, MovementID "
							+ "FROM wms_movement_type WHERE PlantID = ? AND WarehouseID = ? AND MovementID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lMovementID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Movement Type";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteMovementType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteMovementType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	public static String InsertMovementType(model.mdlMovementType lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlMovementType Check = LoadMovementTypeByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getMovementID());
			if(Check.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, MovementID,StorageTypeIDSrc,StorageTypeIDDest,StorageTypeIDRet, "
						+ "StorageBinIDSrc, StorageBinIDDest, StorageBinIDRet,FxdBnSrc,FxdBnDest,Scr_Src,Scr_Dest "
						+ "FROM wms_movement_type LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("MovementID",lParam.getMovementID());
				jrs.updateString("StorageTypeIDSrc",lParam.getStorageTypeSource());
				jrs.updateString("StorageTypeIDDest",lParam.getStorageTypeDestination());
				jrs.updateString("StorageTypeIDRet",lParam.getStorageTypeReturn());
				jrs.updateString("StorageBinIDSrc",lParam.getStorageBinSource());
				jrs.updateString("StorageBinIDDest",lParam.getStorageBinDestination());
				jrs.updateString("StorageBinIDRet",lParam.getStorageBinReturn());
				jrs.updateBoolean("FxdBnSrc",lParam.getFxdBinSource());
				jrs.updateBoolean("FxdBnDest",lParam.getFxdBinDestination());
				jrs.updateBoolean("Scr_Src",lParam.getScrSource());
				jrs.updateBoolean("Scr_Dest",lParam.getScrDestination());

				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Movement Type";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Movement Type sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertMovementType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertMovementType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
	public static String UpdateMovementType(model.mdlMovementType lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID,WarehouseID,MovementID,StorageTypeIDSrc, StorageTypeIDDest, StorageTypeIDRet, "
						+ "StorageBinIDSrc, StorageBinIDDest, StorageBinIDRet, "
						+ "FxdBnSrc, FxdBnDest, Scr_Src, Scr_Dest "
						+ "FROM wms_movement_type WHERE PlantID = ? AND WarehouseID = ? AND MovementID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getMovementID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("StorageTypeIDSrc", lParam.getStorageTypeSource());
			jrs.updateString("StorageTypeIDDest", lParam.getStorageTypeDestination());
			jrs.updateString("StorageTypeIDRet", lParam.getStorageTypeReturn());
			jrs.updateString("StorageBinIDSrc", lParam.getStorageBinSource());
			jrs.updateString("StorageBinIDDest", lParam.getStorageBinDestination());
			jrs.updateString("StorageBinIDRet", lParam.getStorageBinReturn());
			jrs.updateBoolean("FxdBnSrc", lParam.getFxdBinSource());
			jrs.updateBoolean("FxdBnDest", lParam.getFxdBinDestination());
			jrs.updateBoolean("Scr_Src", lParam.getScrSource());
			jrs.updateBoolean("Scr_Dest", lParam.getScrDestination());
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Movement Type";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateMovementType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateMovementType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlMovementType> LoadStorTypeByKey(String lPlantID, String lWarehouseID, String lMovementID) {
		List<model.mdlMovementType> listMovementType = new ArrayList<mdlMovementType>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT StorageTypeIDSrc, StorageTypeIDDest, StorageBinIDSrc, StorageBinIDDest FROM wms_movement_type "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND MovementID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lMovementID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				model.mdlMovementType mdlMovementType = new model.mdlMovementType();
				
				mdlMovementType.setStorageTypeSource(jrs.getString("StorageTypeIDSrc"));
				mdlMovementType.setStorageTypeDestination(jrs.getString("StorageTypeIDDest"));
				mdlMovementType.setStorageBinSource(jrs.getString("StorageBinIDSrc"));
				mdlMovementType.setStorageBinDestination(jrs.getString("StorageBinIDDest"));
				
				listMovementType.add(mdlMovementType);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorTypeByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorTypeByKey", "close opened connection protocol", Globals.user);
			}
		}

		return listMovementType;
	}
	
}
