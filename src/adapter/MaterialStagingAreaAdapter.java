package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlDoor;
import model.mdlMaterialStagingArea;

public class MaterialStagingAreaAdapter {

	@SuppressWarnings("resource")
	public static List<model.mdlMaterialStagingArea> LoadMaterialStagingArea() {
		List<model.mdlMaterialStagingArea> listData = new ArrayList<model.mdlMaterialStagingArea>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT a.PlantID, b.PlantName, a.WarehouseID, c.WarehouseName, a.DoorID, d.DoorName, "
					+ "a.MaterialStagingAreaID, a.MaterialStagingAreaName "
					+ "FROM wms_materialstagingarea a "
					+ "INNER JOIN plant b ON b.PlantID=a.PlantID "
					+ "INNER JOIN warehouse c ON c.PlantID=a.PlantID AND c.WarehouseID=a.WarehouseID "
					+ "INNER JOIN wms_door d ON d.DoorID=a.DoorID AND d.PlantID=a.PlantID AND d.WarehouseID=a.WarehouseID "
					+ "WHERE a.PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next()){
				model.mdlMaterialStagingArea Data = new model.mdlMaterialStagingArea();
				Data.setPlantID(jrs.getString("PlantID"));
				Data.setPlantName(jrs.getString("PlantName"));
				Data.setWarehouseID(jrs.getString("WarehouseID"));
				Data.setWarehouseName(jrs.getString("WarehouseName"));
				Data.setDoorID(jrs.getString("DoorID"));
				Data.setDoorName(jrs.getString("DoorName"));
				Data.setMaterialStagingAreaID(jrs.getString("MaterialStagingAreaID"));
				Data.setMaterialStagingAreaName(jrs.getString("MaterialStagingAreaName"));
				listData.add(Data);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMaterialStagingArea", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadMaterialStagingArea", "close opened connection protocol", Globals.user);
			}
		}

		return listData;
	}

	@SuppressWarnings("resource")
	public static model.mdlMaterialStagingArea LoadStagingAreaByKey(String lPlantID, String lWarehouseID, String lDoorID, String lMaterialStagingAreaID) {
		model.mdlMaterialStagingArea mdlMaterialStagingArea = new model.mdlMaterialStagingArea();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, DoorID, MaterialStagingAreaID, MaterialStagingAreaName FROM wms_materialstagingarea "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND DoorID = ? AND MaterialStagingAreaID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lDoorID);
			jrs.setString(4,  lMaterialStagingAreaID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlMaterialStagingArea.setPlantID(jrs.getString("PlantID"));
				mdlMaterialStagingArea.setWarehouseID(jrs.getString("WarehouseID"));
				mdlMaterialStagingArea.setDoorID(jrs.getString("DoorID"));
				mdlMaterialStagingArea.setMaterialStagingAreaID(jrs.getString("MaterialStagingAreaID"));
				mdlMaterialStagingArea.setMaterialStagingAreaName(jrs.getString("MaterialStagingAreaName"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStagingAreaByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStagingAreaByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlMaterialStagingArea;
	}
	
	@SuppressWarnings("resource")
	public static String InsertMaterialStagingArea(model.mdlMaterialStagingArea lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlMaterialStagingArea Check = LoadStagingAreaByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getDoorID(), lParam.getMaterialStagingAreaID());
			if(Check.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, DoorID, MaterialStagingAreaID, MaterialStagingAreaName "
								+ "FROM wms_materialstagingarea LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("DoorID",lParam.getDoorID());
				jrs.updateString("MaterialStagingAreaID",lParam.getMaterialStagingAreaID());
				jrs.updateString("MaterialStagingAreaName", lParam.getMaterialStagingAreaName());

				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Material Staging Area";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Material staging area sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertMaterialStagingArea", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertMaterialStagingArea", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String UpdateMaterialStagingArea(model.mdlMaterialStagingArea lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID, WarehouseID, DoorID, MaterialStagingAreaID, MaterialStagingAreaName "
							+ "FROM wms_materialstagingarea WHERE PlantID = ? AND WarehouseID = ? AND DoorID = ? AND MaterialStagingAreaID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getDoorID());
			jrs.setString(4,  lParam.getMaterialStagingAreaID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("MaterialStagingAreaName", lParam.getMaterialStagingAreaName());
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Material Staging Area";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateMaterialStagingArea", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateMaterialStagingArea", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeleteMaterialStagingArea(String lPlantID, String lWarehouseID, String lDoorID, String lMaterialStagingArea)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, DoorID, MaterialStagingAreaID, MaterialStagingAreaName "
							+ "FROM wms_materialstagingarea WHERE PlantID = ? AND WarehouseID = ? AND DoorID = ? AND MaterialStagingAreaID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lDoorID);
			jrs.setString(4, lMaterialStagingArea);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Material Staging Area";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteMaterialStagingArea", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteMaterialStagingArea", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlMaterialStagingArea> LoadStagingAreaForInbound(String lPlantID, String lWarehouseID, String lDoorID) {
		List<model.mdlMaterialStagingArea> listStagingArea = new ArrayList<model.mdlMaterialStagingArea>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, DoorID, MaterialStagingAreaID, MaterialStagingAreaName FROM wms_materialstagingarea "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND DoorID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lDoorID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				model.mdlMaterialStagingArea StagingArea = new model.mdlMaterialStagingArea();
				
				StagingArea.setPlantID(jrs.getString("PlantID"));
				StagingArea.setWarehouseID(jrs.getString("WarehouseID"));
				StagingArea.setDoorID(jrs.getString("DoorID"));
				StagingArea.setMaterialStagingAreaID(jrs.getString("MaterialStagingAreaID"));
				StagingArea.setMaterialStagingAreaName(jrs.getString("MaterialStagingAreaName"));
				
				listStagingArea.add(StagingArea);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStagingAreaForInbound", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStagingAreaForInbound", "close opened connection protocol", Globals.user);
			}
		}

		return listStagingArea;
	}
	
}
