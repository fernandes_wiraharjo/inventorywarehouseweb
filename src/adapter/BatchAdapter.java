package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.javafx.scene.layout.region.Margins.Converter;
import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlBatch;

public class BatchAdapter {
	@SuppressWarnings("resource")
	public static model.mdlBatch LoadBatchbyParam(model.mdlBatch lParamBatch,String lUser) {
		
		model.mdlBatch mdlBatch = new model.mdlBatch();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		String Command = "";
		try{
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `ProductID`,`Batch_No` FROM batch WHERE `Batch_No` = ? AND `ProductID`=? LIMIT 1");
			jrs.setString(1,  lParamBatch.getBatch_No());
			jrs.setString(2,  lParamBatch.getProductID());
			Command = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
			mdlBatch.setProductID(jrs.getString("ProductID"));
			mdlBatch.setBatch_No(jrs.getString("Batch_No"));
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBatchbyParameter", Command , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadBatchbyParameter", "close opened connection protocol", lUser);
			}
		}

		return mdlBatch;
	}
	
	public static List<model.mdlBatch> LoadBatchbyProductID(String lProductID) {
		
		List<model.mdlBatch> mdlBatchlist = new ArrayList<model.mdlBatch>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `ProductID`, `Batch_No`, `Packing_No`, `Expired_Date`, `Vendor_Batch_No`, `VendorID` FROM batch WHERE `ProductID` = ?");
			jrs.setString(1,  lProductID);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
			model.mdlBatch mdlBatch = new model.mdlBatch();
			
			mdlBatch.setProductID(jrs.getString("ProductID"));
			mdlBatch.setBatch_No(jrs.getString("Batch_No"));
			mdlBatch.setPacking_No(jrs.getString("Packing_No"));
			mdlBatch.setExpired_Date(jrs.getString("Expired_Date"));
			mdlBatch.setVendor_Batch_No(jrs.getString("Vendor_Batch_No"));
			mdlBatch.setVendorID(jrs.getString("VendorID"));
			
			mdlBatchlist.add(mdlBatch);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBatchbyProductID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadBatchbyProductID", "close opened connection protocol", Globals.user);
			}
		}

		return mdlBatchlist;
	}
	
@SuppressWarnings("resource")
public static List<model.mdlBatch> LoadBatchForInboundDetail(String lProductID,String lVendorID,String lVendorBatch) {
		
		List<model.mdlBatch> mdlBatchlist = new ArrayList<model.mdlBatch>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `ProductID`, `Batch_No`, `Packing_No`, `Expired_Date`, `Vendor_Batch_No`, `VendorID` FROM batch WHERE `ProductID` = ? AND `VendorID` = ? AND `Vendor_Batch_No` = ?");
			jrs.setString(1,  lProductID);
			jrs.setString(2,  lVendorID);
			jrs.setString(3,  lVendorBatch);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
			model.mdlBatch mdlBatch = new model.mdlBatch();
			
			mdlBatch.setProductID(jrs.getString("ProductID"));
			mdlBatch.setBatch_No(jrs.getString("Batch_No"));
			mdlBatch.setPacking_No(jrs.getString("Packing_No"));
			mdlBatch.setExpired_Date(jrs.getString("Expired_Date"));
			mdlBatch.setVendor_Batch_No(jrs.getString("Vendor_Batch_No"));
			mdlBatch.setVendorID(jrs.getString("VendorID"));
			
			mdlBatchlist.add(mdlBatch);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBatchbyProductID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadBatchbyProductID", "close opened connection protocol", Globals.user);
			}
		}

		return mdlBatchlist;
	}
	
	public static List<model.mdlBatch> LoadBatch(String lUser) {
		List<model.mdlBatch> listmdlBatch = new ArrayList<model.mdlBatch>();
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `ProductID`, `Batch_No`, `Packing_No`, `GRDate`, `Expired_Date`, `Vendor_Batch_No`, `VendorID` FROM batch");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlBatch mdlBatch = new model.mdlBatch();
				mdlBatch.setBatch_No(jrs.getString("Batch_No"));
				mdlBatch.setProductID(jrs.getString("ProductID"));
				mdlBatch.setPacking_No(jrs.getString("Packing_No"));
				
				String newGRDate=ConvertDateTimeHelper.formatDate(jrs.getString("GRDate"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlBatch.setGRDate(newGRDate);
				
				String newExpiredDate=ConvertDateTimeHelper.formatDate(jrs.getString("Expired_Date"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlBatch.setExpired_Date(newExpiredDate);
				
				mdlBatch.setVendor_Batch_No(jrs.getString("Vendor_Batch_No"));	
				mdlBatch.setVendorID(jrs.getString("VendorID"));			
				
				listmdlBatch.add(mdlBatch);
			}
			jrs.close();
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBatch", Globals.gCommand , lUser);
		}

		return listmdlBatch;
	}

	//load batch by mysql query left join with product and vendor
	public static List<model.mdlBatch> LoadBatchJoin(String lUser) {
		List<model.mdlBatch> listmdlBatch = new ArrayList<model.mdlBatch>();
		
		Connection connection = null;
		PreparedStatement pstmLoadBatch = null;
		ResultSet jrs = null;
		Globals.gCommand = "";
		
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadBatch = "SELECT a.ProductID, b.Title_ID , a.Batch_No, a.Packing_No, a.GRDate, a.Expired_Date, a.Vendor_Batch_No, a.VendorID, c.VendorName "
					+ "FROM batch a "
					+ "LEFT JOIN product b ON b.ID = a.ProductID "
					+ "LEFT JOIN vendor c ON c.VendorID = a.VendorID";
			
			pstmLoadBatch = connection.prepareStatement(sqlLoadBatch);
			Globals.gCommand = pstmLoadBatch.toString();
			
			jrs = pstmLoadBatch.executeQuery();
									
			while(jrs.next()){
				model.mdlBatch mdlBatch = new model.mdlBatch();
				mdlBatch.setBatch_No(jrs.getString("Batch_No"));
				mdlBatch.setProductID(jrs.getString("ProductID"));
				mdlBatch.setProductName(jrs.getString("Title_ID"));
				mdlBatch.setPacking_No(jrs.getString("Packing_No"));
				
				String newGRDate=ConvertDateTimeHelper.formatDate(jrs.getString("GRDate"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlBatch.setGRDate(newGRDate);
				
				String newExpiredDate=ConvertDateTimeHelper.formatDate(jrs.getString("Expired_Date"), "yyyy-MM-dd", "dd MMM yyyy");
				mdlBatch.setExpired_Date(newExpiredDate);
				
				mdlBatch.setVendor_Batch_No(jrs.getString("Vendor_Batch_No"));	
				mdlBatch.setVendorID(jrs.getString("VendorID"));
				mdlBatch.setVendorName(jrs.getString("VendorName"));
				
				listmdlBatch.add(mdlBatch);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBatchJoin", Globals.gCommand , lUser);
		}
		finally {
			try {
				if (pstmLoadBatch != null) {
					pstmLoadBatch.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception e)
			{
				LogAdapter.InsertLogExc(e.toString(), "LoadBatchJoin", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlBatch;
	}
	
	public static String InsertBatch(model.mdlBatch lParam, String lUser)
	{
		Globals.gCommand = "Insert Batch_No : " + lParam.getBatch_No() + " Product ID : " + lParam.getProductID() ;
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();			
			
			jrs.setCommand("SELECT `ProductID`, `Batch_No`, `Packing_No`, `GRDate`, `Expired_Date`, `Vendor_Batch_No`, `VendorID`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM batch LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("ProductID",lParam.getProductID());
			jrs.updateString("Batch_No",lParam.getBatch_No());
			jrs.updateString("Packing_No",lParam.getPacking_No());
			jrs.updateString("GRDate",lParam.getGRDate());
			jrs.updateString("Expired_Date",lParam.getExpired_Date());
			jrs.updateString("Vendor_Batch_No",lParam.getVendor_Batch_No());
			jrs.updateString("VendorID",lParam.getVendorID());
			jrs.updateString("Created_by", lUser);
			jrs.updateString("Created_at", LocalDateTime.now().toString());
			jrs.updateString("Updated_by", lUser);
			jrs.updateString("Updated_at", LocalDateTime.now().toString());
			
			jrs.insertRow();	
			jrs.close();
			Globals.gReturn_Status = "Success Insert Batch";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertBatch", Globals.gCommand , lUser);
			Globals.gReturn_Status = "Error Insert Batch";

		}
		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String UpdateBatch(model.mdlBatch lParam)
	{
		Globals.gCommand = "Update Batch_No & ProductID : " + lParam.getBatch_No() + " & " + lParam.getProductID();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `ProductID`, `Batch_No`, `Packing_No`, `GRDate`, `Expired_Date`, `Vendor_Batch_No`, `VendorID` FROM batch WHERE `ProductID` = ? AND `Batch_No` = ? LIMIT 1");
			jrs.setString(1,  lParam.getProductID());
			jrs.setString(2,  lParam.getBatch_No());
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("Packing_No",lParam.getPacking_No());
			jrs.updateString("GRDate",lParam.getGRDate());
			jrs.updateString("Expired_Date",lParam.getExpired_Date());
			jrs.updateString("Vendor_Batch_No",lParam.getVendor_Batch_No());
			jrs.updateString("VendorID",lParam.getVendorID());
			jrs.updateRow();
			
			Globals.gReturn_Status = "Success Update Batch";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateBatch", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Batch";
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try {
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "UpdateBatch", "close opened connection protocol", Globals.user);
			}
		}
		return Globals.gReturn_Status;
	}
	
//	public static String DeleteBatch(String lProductID, String lBatchNo)
//	{
//		Globals.gCommand = "Delete Batch_No & ProductID : " + lProductID + " & " + lBatchNo ;
//		try{
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();		
//			jrs.setCommand("SELECT `Packing_No`, `GRDate`, `Expired_Date`, `Vendor_Batch_No`, `VendorID` FROM batch WHERE `ProductID` = ? AND `Batch_No`= ? LIMIT 1");
//			jrs.setString(1, lProductID);
//			jrs.setString(1, lBatchNo);
//			jrs.execute();
//			
//			jrs.last();
//			jrs.deleteRow();
//			
//			jrs.close();
//			Globals.gReturn_Status = "Success Delete Batch ";
//		}
//		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "DeleteBatch", Globals.gCommand , "");
//			Globals.gReturn_Status = "Error Delete Batch" + ex.toString();
//		}
//		return Globals.gReturn_Status;
//	}
	
	public static model.mdlTraceCode LoadTraceCode(String lDate, String lUser) {
//		List<model.mdlTraceCode> listmdlTraceCode = new ArrayList<model.mdlTraceCode>();
		model.mdlTraceCode mdlTraceCode = new model.mdlTraceCode();
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `Trace_Code`, `Created_at` FROM TraceCodeGenerate WHERE DATE(`Created_at`) = CURDATE() ORDER BY `Created_at` desc Limit 1");
//			jrs.setString(1, lDate);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				
				mdlTraceCode.setTraceCode(jrs.getString("Trace_Code"));
				mdlTraceCode.setDate(jrs.getString("Created_at"));		
				
//				listmdlTraceCode.add(mdlTraceCode);
			}
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadTraceCode", Globals.gCommand , lUser);
		}

		return mdlTraceCode;
	}
	
	public static void InsertTraceCode(String ltracecode, String lDate, String lUser)
	{
		Globals.gCommand = "Insert Trace no : " + ltracecode + " Date : " + lDate ;
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();			
			
			jrs.setCommand("SELECT `Trace_Code`, `Created_at` FROM TraceCodeGenerate LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("Trace_Code",ltracecode);
			jrs.updateString("Created_at",lDate);
			
			jrs.insertRow();	
			jrs.close();
			Globals.gReturn_Status = "Success Insert Trace Code";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertTraceCode", Globals.gCommand , lUser);
			Globals.gReturn_Status = "Error Insert Trace Code";

		}
		return;
	}

	public static String GenerateBatch(String lUser)
	{
		String generateDate = LocalDateTime.now().toString();
		String txtBatchNo =  "";
		model.mdlTraceCode lGetTraceCode = TraceCodeAdapter.LoadBatchTraceCode(generateDate.replace("T"," "));
		
		if(lGetTraceCode.TraceCode == null)
		{
			txtBatchNo = generateDate.substring(2, 10).replace("-", "") + "0001";
			TraceCodeAdapter.InsertTraceCode(txtBatchNo, generateDate.replace("T"," ") ,"Batch");
		}
		else
		{
			int lRunningNumber = Integer.parseInt(lGetTraceCode.TraceCode.substring(6,10)) + 1;
			String lnewRunningNumber = String.format("%04d", lRunningNumber);
					
			txtBatchNo = generateDate.substring(2, 10).replace("-", "") + lnewRunningNumber;
			TraceCodeAdapter.InsertTraceCode(txtBatchNo, generateDate.replace("T"," ") ,"Batch");
		}
		
		return txtBatchNo;
	}
	
	@SuppressWarnings("resource")
	public static String DeleteTraceCode(String ltracecode, String lUser)
	{
//		Globals.gCommand = "Delete Trace Code : " + ltracecode;
		String Command = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT `Trace_Code`, `Created_at` FROM tracecodegenerate WHERE `Trace_Code` = ? LIMIT 1");
			jrs.setString(1, ltracecode);
			Command = jrs.getCommand();
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			Globals.gReturn_Status = "Success Delete Trace Code Batch No";
		}
		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "DeleteTraceCode", Globals.gCommand , lUser);
			LogAdapter.InsertLogExc(ex.toString(), "DeleteTraceCode", Command , lUser);
			Globals.gReturn_Status = "Error Delete Trace Code Batch No";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteTraceCode", "close opened connection protocol", lUser);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	//<<001 Nanda
	public static List<model.mdlBatch> LoadBatchforInbound(model.mdlBatch lParamBatch) throws Exception{
	List<model.mdlBatch> listmdlBatch = new ArrayList<model.mdlBatch>();
	
	Connection connection = database.RowSetAdapter.getConnection();
	PreparedStatement pstmLoadBatch = null;
	ResultSet jrs = null;
	
	try{

	String sqlLoadBatch = "SELECT `ProductID`, `Batch_No`, `Packing_No`, `GRDate`, `Expired_Date`, `Vendor_Batch_No`, `VendorID` FROM `batch` WHERE `ProductID`=? AND  `Vendor_Batch_No`=? AND `VendorID`=? ";	
	pstmLoadBatch = connection.prepareStatement(sqlLoadBatch);
	pstmLoadBatch.setString(1,  lParamBatch.getProductID());
	pstmLoadBatch.setString(2,  lParamBatch.getVendor_Batch_No());
	pstmLoadBatch.setString(3,  lParamBatch.getVendorID());
	jrs = pstmLoadBatch.executeQuery();

	while(jrs.next()){
	model.mdlBatch mdlBatch = new model.mdlBatch();
	mdlBatch.setBatch_No(jrs.getString("Batch_No"));
	mdlBatch.setProductID(jrs.getString("ProductID"));
	mdlBatch.setProductName(jrs.getString("Title_EN"));
	mdlBatch.setPacking_No(jrs.getString("Packing_No"));
	mdlBatch.setGRDate(jrs.getString("GRDate"));
	mdlBatch.setExpired_Date(jrs.getString("Expired_Date"));
	mdlBatch.setVendor_Batch_No(jrs.getString("Vendor_Batch_No"));	
	mdlBatch.setVendorID(jrs.getString("VendorID"));	

	listmdlBatch.add(mdlBatch);
	}
//	jrs.close();
//	    pstmLoadBatch.close();	
	}
	catch(Exception ex){
	LogAdapter.InsertLogExc(ex.toString(), "LoadBatch2", Globals.gCommand , Globals.user);
	}
	 finally {
		 if (pstmLoadBatch != null) {
			 pstmLoadBatch.close();
		 }
		 if (connection != null) {
			 connection.close();
		 }
		 if (jrs != null) {
			 jrs.close();
		 }
	 }

	return listmdlBatch;
	}
	//>>
}
