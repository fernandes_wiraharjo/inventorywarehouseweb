package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

public class ProductAdapter {
	
	public static List<model.mdlProduct> GetProductAPI(String lUser){
		List<model.mdlProduct> listmdlProduct = new ArrayList<model.mdlProduct>();
		model.mdlToken mdlToken = TokenAdapter.GetToken();
		try {
			
			Client client = Client.create();
			
			Globals.gCommand = "http://dev.webarq.info:8081/kenmaster-be/api/product/listing";
			WebResource webResource = client.resource(Globals.gCommand);
			
			Gson gson = new Gson();
			String json = gson.toJson(mdlToken);
			
			ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);		
			
			Globals.gReturn_Status = response.getEntity(String.class);
			
			//Type TypeDepartment = new TypeToken<ArrayList<model.mdlDepartment>>(){}.getClass();
			listmdlProduct = gson.fromJson(Globals.gReturn_Status, model.mdlProductlist.class);
			
			
		  } catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetToken", Globals.gCommand , lUser);
//			Globals.gReturn_Status = "Error GetToken";
		  }
		
		return listmdlProduct;
		}

		public static String InsertProductlist(List<model.mdlProduct> lParamlist,List<model.mdlProductDetail> lParamDetaillist, String lUser)
		{
			Connection connection = null;
			PreparedStatement pstm = null;
			Globals.gCommand = "";
			try{
				connection = database.RowSetAdapter.getConnection();
				
				Gson gson = new Gson();
				Globals.gCommand = gson.toJson(lParamlist);
				
				String sql = "INSERT INTO product(`ID`, `Category_ID`, `Brand_ID`, `Title_EN`, `Title_ID`, `Code`, "
						+ "`BaseUOM`, `Short_Description_ID`, `Short_Description_EN`, `Description_ID`, `Description_EN`, "
						+ "`Spesification_ID`, `Spesification_EN`, `Detail_ID`, `Detail_EN`, `Image`, "
						+ "`Minimum_Order`, `Is_Best_Seller`, `Is_Featured_Product`, `Is_Only_Jabodetabek`, "
						+ "`Can_Be_Sold_Online`, `Status`, `Slug`, `Is_Bom` , `Created_at`, `Updated_at`) "
						+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "
						+ "ON DUPLICATE KEY UPDATE Image=?,Updated_at=?";
				pstm = connection.prepareStatement(sql);
				
				for(model.mdlProductDetail lParamDetail : lParamDetaillist)
				{
					for(model.mdlProduct lParam : lParamlist) 
					{
						if(lParam.getId().contentEquals(lParamDetail.getProduct_id()))
						{
							pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getId()+"-"+lParamDetail.getId()));
							pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getCategory_id()));
							pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getBrand_id()));
							pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getTitle_en()));
							pstm.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getTitle_id()));
							pstm.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getCode()));
							pstm.setString(7,  ValidateNull.NulltoStringEmpty("")); 
							pstm.setString(8,  ValidateNull.NulltoStringEmpty(lParam.getShort_description_id()));
							pstm.setString(9,  ValidateNull.NulltoStringEmpty(lParam.getShort_description_en()));
							pstm.setString(10, ValidateNull.NulltoStringEmpty(lParam.getDescription_id()));
							pstm.setString(11, ValidateNull.NulltoStringEmpty(lParam.getDescription_en()));
							pstm.setString(12, ValidateNull.NulltoStringEmpty(lParam.getSpesification_id()));
							pstm.setString(13, ValidateNull.NulltoStringEmpty(lParam.getSpesification_en()));
							pstm.setString(14, ValidateNull.NulltoStringEmpty(lParam.getDetail_id()));
							pstm.setString(15, ValidateNull.NulltoStringEmpty(lParam.getDetail_en()));
							pstm.setString(16, ValidateNull.NulltoStringEmpty(lParam.getImage()));
							pstm.setString(17, ValidateNull.NulltoStringEmpty(lParam.getMinimum_order()));
							pstm.setString(18, ValidateNull.NulltoStringEmpty(lParam.getIs_best_seller()));
							pstm.setString(19, ValidateNull.NulltoStringEmpty(lParam.getIs_featured_product()));
							pstm.setString(20, ValidateNull.NulltoStringEmpty(lParam.getIs_only_jabdetabek()));
							pstm.setString(21, ValidateNull.NulltoStringEmpty(lParam.getCan_be_sold_online()));
							pstm.setString(22, ValidateNull.NulltoStringEmpty(lParam.getStatus()));
							pstm.setString(23, ValidateNull.NulltoStringEmpty(lParam.getSlug()));
							pstm.setBoolean(24, false);
							pstm.setString(25, ValidateNull.NulltoStringEmpty(lParam.getCreated_at()));
							pstm.setString(26, ValidateNull.NulltoStringEmpty(lParam.getUpdated_at()));
							pstm.setString(27, ValidateNull.NulltoStringEmpty(lParam.getImage()));
							pstm.setString(28, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
							
							pstm.addBatch();
							Globals.gCommand = pstm.toString();
							break;
						}
					}
				}
				
				pstm.executeBatch();
				Globals.gReturn_Status = "Success Insert list Product API";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertProductbyAPI", Globals.gCommand , lUser);
				Globals.gReturn_Status = "Error Insert list Product API";
			}
			finally {
				try{
					 if (pstm != null) {
						 pstm.close();
					 }
					 if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertProductbyAPI", "close opened connection protocol", lUser);
				}
			 }
			
			return Globals.gReturn_Status;
		}
		
		public static String InsertProduct(model.mdlProduct lParam)
		{
			Globals.gCommand = "";
			
			Connection connection = null;
			PreparedStatement pstm = null;
			try{
				connection = database.RowSetAdapter.getConnection();
				
				String sql = "INSERT INTO product(`ID`, `Title_EN`, `BaseUOM`, `Is_Bom`, `Description_EN`, `Created_at`, `Updated_at`) VALUES (?,?,?,?,?,?,?)";
				pstm = connection.prepareStatement(sql);
				
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getId()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getTitle_en()));
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				pstm.setBoolean(4,  lParam.getIs_bom());
				pstm.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getDescription_en()));
				pstm.setString(6,  LocalDateTime.now().toString());
				pstm.setString(7,  LocalDateTime.now().toString());
				
				pstm.addBatch();
				Globals.gCommand = pstm.toString();
					
				pstm.executeBatch();
				
				Globals.gReturn_Status = "Success Insert Product";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertProduct", Globals.gCommand , Globals.user);
				Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
					 if (pstm != null) {
						 pstm.close();
					 }
					 if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertProduct", "close opened connection protocol", Globals.user);
				}
			 }
			
			return Globals.gReturn_Status;
		}
		
		public static String UpdateProduct(model.mdlProduct lParam)
		{
			Globals.gCommand = "";
			
			Connection connection = null;
			PreparedStatement pstm = null;
			PreparedStatement pstmUpdateProductUOM = null;
			PreparedStatement pstmDeleteProductBaseUOM = null;
			PreparedStatement pstmInsertUpdateProductBaseUOM = null;
			try{
				connection = database.RowSetAdapter.getConnection();
				
				connection.setAutoCommit(false);
				
				String sql = "UPDATE product SET Title_EN=? , Description_EN=? ,BaseUOM=?, Is_Bom=?,Updated_at=?  "
						+ "WHERE `ID`=?";
				pstm = connection.prepareStatement(sql);
				
				String sqlUpdateProductUOM = "UPDATE product_uom SET BaseUOM=? "
						+ "WHERE ProductID=? AND Qty!=1";
				pstmUpdateProductUOM = connection.prepareStatement(sqlUpdateProductUOM);
				
				String sqlDeleteProductBaseUOM = "DELETE FROM product_uom "
						+ "WHERE ProductID=? AND Qty=1";
				pstmDeleteProductBaseUOM = connection.prepareStatement(sqlDeleteProductBaseUOM);
				
				String sqlInsertUpdateProductBaseUOM = "INSERT INTO product_uom(ProductID,UOM,BaseUOM,Qty) VALUES(?,?,?,?) ON DUPLICATE KEY UPDATE UOM=?,BaseUOM=?;";
				pstmInsertUpdateProductBaseUOM = connection.prepareStatement(sqlInsertUpdateProductBaseUOM);
				
				
				//update product process
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getTitle_en()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getDescription_en()));
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				pstm.setBoolean(4,  lParam.getIs_bom());
				pstm.setString(5,  LocalDateTime.now().toString());
				pstm.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getId()));
				Globals.gCommand = pstm.toString();
				pstm.executeUpdate();
				//pstm.addBatch();
				//pstm.executeBatch();
				
				//update product uom process
				pstmUpdateProductUOM.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				pstmUpdateProductUOM.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getId()));
				Globals.gCommand = pstmUpdateProductUOM.toString();
				pstmUpdateProductUOM.executeUpdate();
				
				//delete product base uom process
				pstmDeleteProductBaseUOM.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getId()));
				Globals.gCommand = pstmDeleteProductBaseUOM.toString();
				pstmDeleteProductBaseUOM.executeUpdate();
				
				//insert update product base uom process
				pstmInsertUpdateProductBaseUOM.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getId()));
				pstmInsertUpdateProductBaseUOM.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				pstmInsertUpdateProductBaseUOM.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				pstmInsertUpdateProductBaseUOM.setInt(4, 1);
				pstmInsertUpdateProductBaseUOM.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				pstmInsertUpdateProductBaseUOM.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getBaseUOM()));
				Globals.gCommand = pstmInsertUpdateProductBaseUOM.toString();
				pstmInsertUpdateProductBaseUOM.executeUpdate();
				
				connection.commit(); //commit transaction if all of the proccess is running well
				
				Globals.gReturn_Status = "Success Update Product";
			}
			catch (Exception ex){
				if (connection != null) {
				    try {
				        System.err.print("Transaction is being rolled back");
				        connection.rollback();
				    } catch(SQLException excep) {
				    	LogAdapter.InsertLogExc(excep.toString(), "TransactionUpdateProduct", Globals.gCommand , Globals.user);
						Globals.gReturn_Status = "Database Error";
				    }
				}
				    
				LogAdapter.InsertLogExc(ex.toString(), "UpdateProduct", Globals.gCommand , Globals.user);
				Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
					 if (pstm != null) {
						 pstm.close();
					 }
					 if (pstmUpdateProductUOM != null) {
						 pstmUpdateProductUOM.close();
					 }
					 if (pstmDeleteProductBaseUOM != null) {
						 pstmDeleteProductBaseUOM.close();
					 }
					 if (pstmInsertUpdateProductBaseUOM != null) {
						 pstmInsertUpdateProductBaseUOM.close();
					 }
					 if (connection != null) {
						 connection.setAutoCommit(true);
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "UpdateProduct", "close opened connection protocol", Globals.user);
				}
			 }
			
			return Globals.gReturn_Status;
		}
		
		@SuppressWarnings("resource")
		public static List<model.mdlProduct> LoadProduct() {
			List<model.mdlProduct> listmdlProduct = new ArrayList<model.mdlProduct>();
			JdbcRowSet jrs = new JdbcRowSetImpl();
			Globals.gCommand="";
			try{
				jrs = database.RowSetAdapter.getJDBCRowSet();
				
				jrs.setCommand("SELECT `ID`, `Category_ID`, `Brand_ID`, `Title_EN`, `Title_ID`, `Code`, `BaseUOM`, `Short_Description_ID`,"
						+ " `Short_Description_EN`, `Description_ID`, `Description_EN`, `Spesification_ID`, `Spesification_EN`, `Detail_ID`, `Detail_EN`, `Image`,"
						+ " `Minimum_Order`, `Is_Best_Seller`, `Is_Featured_Product`, `Is_Only_Jabodetabek`, `Can_Be_Sold_Online`, `Status`, `Slug`, `Is_Bom`, `Created_at`,"
						+ " `Updated_at` FROM product");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
										
				while(jrs.next()){
					model.mdlProduct mdlProduct = new model.mdlProduct();
					
					mdlProduct.setId(jrs.getString("ID"));
					mdlProduct.setCategory_id(jrs.getString("Category_ID"));
					mdlProduct.setBrand_id(jrs.getString("Brand_ID"));
					mdlProduct.setTitle_en(jrs.getString("Title_EN"));
					mdlProduct.setTitle_id(jrs.getString("Title_ID"));
					mdlProduct.setBaseUOM(jrs.getString("BaseUOM"));
					mdlProduct.setCode(jrs.getString("Code"));
					mdlProduct.setShort_description_id(jrs.getString("Short_Description_ID"));
					mdlProduct.setShort_description_en(jrs.getString("Short_Description_EN"));
					mdlProduct.setDescription_id(jrs.getString("Description_ID"));
					mdlProduct.setDescription_en(jrs.getString("Description_EN"));
					mdlProduct.setSpesification_id(jrs.getString("Spesification_ID"));				
					mdlProduct.setSpesification_en(jrs.getString("Spesification_EN"));
					mdlProduct.setDetail_id(jrs.getString("Detail_ID"));
					mdlProduct.setDetail_en(jrs.getString("Detail_EN"));
					mdlProduct.setImage(jrs.getString("Image"));
					mdlProduct.setMinimum_order(jrs.getString("Minimum_Order"));
					mdlProduct.setIs_best_seller(jrs.getString("Is_Best_Seller"));
					mdlProduct.setIs_featured_product(jrs.getString("Is_Featured_Product"));
					mdlProduct.setIs_only_jabdetabek(jrs.getString("Is_Only_Jabodetabek"));
					mdlProduct.setCan_be_sold_online(jrs.getString("Can_Be_Sold_Online"));
					mdlProduct.setStatus(jrs.getString("Status"));
					mdlProduct.setSlug(jrs.getString("Slug"));
					mdlProduct.setIs_bom(jrs.getBoolean("Is_Bom"));
					mdlProduct.setCreated_at(jrs.getString("Created_at"));
					mdlProduct.setUpdated_at(jrs.getString("Updated_at"));
					
					listmdlProduct.add(mdlProduct);
				}	
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadProduct", Globals.gCommand , Globals.user);
			}
			finally{
				try{
					if(jrs!=null)
						jrs.close();
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "LoadProduct", "close opened connection protocol", Globals.user);
				}
			}

			return listmdlProduct;
		}

		@SuppressWarnings("resource")
		public static List<model.mdlProduct> LoadBOMProduct() {
			List<model.mdlProduct> listmdlProduct = new ArrayList<model.mdlProduct>();
			JdbcRowSet jrs = new JdbcRowSetImpl();
			Globals.gCommand="";
			try{
				jrs = database.RowSetAdapter.getJDBCRowSet();
				
				jrs.setCommand("SELECT `ID`, `Title_EN`, `Short_Description_EN` "
						+ "FROM product "
						+ "WHERE Is_Bom=1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
										
				while(jrs.next()){
					model.mdlProduct mdlProduct = new model.mdlProduct();
					
					mdlProduct.setId(jrs.getString("ID"));
					mdlProduct.setTitle_en(jrs.getString("Title_EN"));
					mdlProduct.setShort_description_en(jrs.getString("Short_Description_EN"));
					
					listmdlProduct.add(mdlProduct);
				}		
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadBOMProduct", Globals.gCommand , Globals.user);
			}
			finally {
				try {
					if(jrs!=null)
						jrs.close();
				}
				catch(Exception e) {
					LogAdapter.InsertLogExc(e.toString(), "LoadBOMProduct", "close opened connection protocol", Globals.user);
				}
			}

			return listmdlProduct;
		}
		
		@SuppressWarnings("resource")
		public static List<model.mdlProduct> LoadComponentProduct() {
			List<model.mdlProduct> listmdlProduct = new ArrayList<model.mdlProduct>();
			JdbcRowSet jrs = new JdbcRowSetImpl();
			Globals.gCommand="";
			try{
				jrs = database.RowSetAdapter.getJDBCRowSet();
				
				jrs.setCommand("SELECT `ID`, `Title_EN`, `Short_Description_EN` "
						+ "FROM product "
						+ "WHERE Is_Bom=0");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
										
				while(jrs.next()){
					model.mdlProduct mdlProduct = new model.mdlProduct();
					
					mdlProduct.setId(jrs.getString("ID"));
					mdlProduct.setTitle_en(jrs.getString("Title_EN"));
					mdlProduct.setShort_description_en(jrs.getString("Short_Description_EN"));
					
					listmdlProduct.add(mdlProduct);
				}		
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadComponentProduct", Globals.gCommand , Globals.user);
			}
			finally{
				try{
					if(jrs!=null)
						jrs.close();
				}
				catch(Exception e) {
					LogAdapter.InsertLogExc(e.toString(), "LoadComponentProduct", "close opened connection protocol", Globals.user);
				}
			}

			return listmdlProduct;
		}
		
		public static String LoadProductBaseUOM(String lProductID) {
//			List<model.mdlUom> listmdlUom = new ArrayList<model.mdlUom>();
			String BaseUOM = "";
			JdbcRowSet jrs = new JdbcRowSetImpl();
			Globals.gCommand = "";
			try{
				jrs = database.RowSetAdapter.getJDBCRowSet();
				
				jrs.setCommand("SELECT BaseUOM FROM product WHERE ID = ? LIMIT 1");
				jrs.setString(1, lProductID);
				
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
										
				while(jrs.next()){
			
					BaseUOM = jrs.getString("BaseUOM");
				}		
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadProductBaseUOM", Globals.gCommand , Globals.user);
			}
			finally{
				try{
					if(jrs!=null)
						jrs.close();
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "LoadProductBaseUOM", "close opened connection protocol", Globals.user);
				}
			}

			return BaseUOM;
		}
		
		public static List<model.mdlProductBOMDetail> LoadBOMProductDetail(String BOMProductID) throws Exception {
			List<model.mdlProductBOMDetail> listmdlProductBOMDetail = new ArrayList<model.mdlProductBOMDetail>();
			
			Connection connection = database.RowSetAdapter.getConnection();
			PreparedStatement pstmLoadBomProductDetail = null;
			ResultSet jrs = null;
			try{
				
				String sqlLoadBomProductDetail = "SELECT a.ProductID, a.ComponentLine, a.ComponentID, b.Title_EN, a.Qty, a.UOM "
						+ "FROM product_bom_detail a "
						+ "INNER JOIN product b ON b.ID = a.ComponentID "
						+ "WHERE a.ProductID=? ORDER BY a.ComponentLine";
				
				Globals.gCommand = sqlLoadBomProductDetail;
				
				pstmLoadBomProductDetail = connection.prepareStatement(sqlLoadBomProductDetail);
				
				pstmLoadBomProductDetail.setString(1, BOMProductID);
				pstmLoadBomProductDetail.addBatch();
				
				jrs = pstmLoadBomProductDetail.executeQuery();
										
				while(jrs.next()){
					model.mdlProductBOMDetail mdlBomProductDetail = new model.mdlProductBOMDetail();
					
					mdlBomProductDetail.setProductID(jrs.getString("ProductID"));
					mdlBomProductDetail.setComponentLine(jrs.getString("ComponentLine"));
					mdlBomProductDetail.setComponentID(jrs.getString("ComponentID"));
					mdlBomProductDetail.setQty(jrs.getString("Qty"));
					mdlBomProductDetail.setUOM(jrs.getString("UOM"));
					mdlBomProductDetail.setComponentName(jrs.getString("Title_EN"));
					
					listmdlProductBOMDetail.add(mdlBomProductDetail);
				}
//				jrs.close();
//				pstmLoadBomProductDetail.close();	
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadBOMProductDetail", Globals.gCommand , Globals.user);
			}
			finally {
				 if (pstmLoadBomProductDetail != null) {
					 pstmLoadBomProductDetail.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			 }

			return listmdlProductBOMDetail;
		}

		public static String InsertBOMProductDetail(model.mdlProductBOMDetail lParam) throws Exception
		{
			Globals.gCommand = "Insert BOM Product : " + lParam.getProductID() + ", Component Line : " + lParam.getComponentLine();
			
			Connection connection = database.RowSetAdapter.getConnection();
			String sql = "INSERT INTO product_bom_detail(`ProductID`, `ComponentLine`, `ComponentID`, `Qty`, `UOM`, `Created_at`, `Updated_at`) VALUES (?,?,?,?,?,?,?)";
			PreparedStatement pstm = connection.prepareStatement(sql);
			try{
				
					pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getProductID()));
					pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getComponentLine()));
					pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getComponentID()));
					pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getQty()));
					pstm.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getUOM()));
					pstm.setString(6,  LocalDateTime.now().toString());
					pstm.setString(7,  LocalDateTime.now().toString());
					
					pstm.addBatch();
					
				pstm.executeBatch();
				
//				pstm.close();
//				connection.close();	
				Globals.gReturn_Status = "Success Insert Component";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertBOMProductDetail", Globals.gCommand , Globals.user);
				Globals.gReturn_Status = "Database Error";
			}
			finally {
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			 }
			
			return Globals.gReturn_Status;
		}
		
		public static String UpdateBOMProductDetail(model.mdlProductBOMDetail lParam) throws Exception
		{
			Globals.gCommand = "Update BOM Product : " + lParam.getProductID() + ",Component Line : " + lParam.getComponentLine();
			
			Connection connection = database.RowSetAdapter.getConnection();
			String sql = "UPDATE product_bom_detail SET Qty=?, Updated_at=? WHERE ProductID=? AND ComponentLine=?";
			PreparedStatement pstm = connection.prepareStatement(sql);
			try{
				
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getQty()));
				pstm.setString(2,  LocalDateTime.now().toString());
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getProductID()));
				pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getComponentLine()));
					
				pstm.addBatch();
					
				pstm.executeBatch();
				
//				pstm.close();
//				connection.close();	
				Globals.gReturn_Status = "Success Update Component";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "UpdateBOMProductDetail", Globals.gCommand , Globals.user);
				Globals.gReturn_Status = "Database Error";
			}
			finally {
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			 }
			
			return Globals.gReturn_Status;
		}

		public static String DeleteBOMProductDetail(String ProductID, String ComponentLine) throws Exception
		{
			Globals.gCommand = "Delete BOM Product : " + ProductID + ", Component Line : " + ComponentLine;
			
			Connection connection = database.RowSetAdapter.getConnection();
			String sql = "DELETE FROM product_bom_detail WHERE ProductID=? AND ComponentLine=?";
			PreparedStatement pstm = connection.prepareStatement(sql);
			try{
				
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(ProductID));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(ComponentLine));
					
				pstm.addBatch();
					
				pstm.executeBatch();
				
//				pstm.close();
//				connection.close();	
				Globals.gReturn_Status = "Success Delete Component";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteBOMProductDetail", Globals.gCommand , Globals.user);
				Globals.gReturn_Status = "Database Error";
			}
			finally {
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			 }
			
			return Globals.gReturn_Status;
		}

		//-- CLOSE CODE --
//		public static List<model.mdlProduct> InsertProduct(model.mdlProduct lParam)
//		{
//			model.mdlToken mdlToken = TokenAdapter.GetToken();
//			
//			lParam.setToken(mdlToken.getToken());
//
//			List<model.mdlProduct> listmdlProduct = new ArrayList<model.mdlProduct>();
//			try {
//				Client client = Client.create();
//				
//				Globals.gCommand = "http://dev.webarq.info:8081/kenmaster-be/api/product/create";
//				WebResource webResource = client.resource(Globals.gCommand);
//				
//				Gson gson = new Gson();
//				String json = gson.toJson(lParam);
//				
//				ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);		
//				
//				Globals.gReturn_Status = response.getEntity(String.class);
//				
//				//Type TypeDepartment = new TypeToken<ArrayList<model.mdlDepartment>>(){}.getClass();
//				listmdlProduct = gson.fromJson(Globals.gReturn_Status, model.mdlProductlist.class);
//			  } 
//			catch (Exception ex) {
//				LogAdapter.InsertLogExc(ex.toString(), "InsertProduct", Globals.gCommand , Globals.user);
//				//Globals.gReturn_Status = "Error GetToken";
//			  }
//			
//			return listmdlProduct;
//		}
}