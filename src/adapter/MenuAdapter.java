package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import helper.ConvertDateTimeHelper;
import model.Globals;

public class MenuAdapter {

	public static List<model.mdlMenu> LoadMenu() {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		Connection connection = null;
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		
		PreparedStatement pstmLoadMenuLv1 = null;
		ResultSet jrs2 = null;
		
		PreparedStatement pstmLoadMenuLv2 = null;
		ResultSet jrs3 = null;
		
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadMenu = "SELECT MenuID,MenuName,MenuUrl,Type,Level FROM menu WHERE Level=0 ORDER BY MenuID";
			pstmLoadMenu = connection.prepareStatement(sqlLoadMenu);
			jrs = pstmLoadMenu.executeQuery();
			Globals.gCommand = pstmLoadMenu.toString();
									
			while(jrs.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(jrs.getString("MenuID"));
				mdlMenu.setMenuName(jrs.getString("MenuName"));
				mdlMenu.setMenuUrl(jrs.getString("MenuUrl"));
				mdlMenu.setType(jrs.getString("Type"));
				mdlMenu.setLevel(jrs.getString("Level"));
				
				listmdlMenu.add(mdlMenu);
				
				String sqlLoadMenuLv1 = "SELECT MenuID,MenuName,MenuUrl,Type,Level FROM menu WHERE Type=? AND Level=1 ORDER BY MenuID";
				pstmLoadMenuLv1 = connection.prepareStatement(sqlLoadMenuLv1);
				pstmLoadMenuLv1.setString(1, mdlMenu.getType());
				
				jrs2 = pstmLoadMenuLv1.executeQuery();
				Globals.gCommand = pstmLoadMenuLv1.toString();
				
				while(jrs2.next()) {
					model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
					mdlMenuLv1.setMenuID(jrs2.getString("MenuID"));
					mdlMenuLv1.setMenuName("- "+jrs2.getString("MenuName"));
					mdlMenuLv1.setMenuUrl(jrs2.getString("MenuUrl"));
					mdlMenuLv1.setType(jrs2.getString("Type"));
					mdlMenuLv1.setLevel(jrs2.getString("Level"));
					
					listmdlMenu.add(mdlMenuLv1);
					
					String sqlLoadMenuLv2 = "SELECT MenuID,MenuName,MenuUrl,Type,Level FROM menu WHERE Type=? AND Level=2 AND MenuID LIKE ? ORDER BY MenuID";
					pstmLoadMenuLv2 = connection.prepareStatement(sqlLoadMenuLv2);
					pstmLoadMenuLv2.setString(1, mdlMenu.getType());
					pstmLoadMenuLv2.setString(2, "%" + mdlMenuLv1.getMenuID() + "%");
					pstmLoadMenuLv2.addBatch();
					jrs3 = pstmLoadMenuLv2.executeQuery();
					Globals.gCommand = pstmLoadMenuLv2.toString();
					
					while(jrs3.next()) {
						model.mdlMenu mdlMenuLv2 = new model.mdlMenu();
						mdlMenuLv2.setMenuID(jrs3.getString("MenuID"));
						mdlMenuLv2.setMenuName("- "+jrs3.getString("MenuName"));
						mdlMenuLv2.setMenuUrl(jrs3.getString("MenuUrl"));
						mdlMenuLv2.setType(jrs3.getString("Type"));
						mdlMenuLv2.setLevel(jrs3.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv2);
					}
				}
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMenu", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (pstmLoadMenuLv1 != null) {
					pstmLoadMenuLv1.close();
				}
				if (pstmLoadMenuLv2 != null) {
					pstmLoadMenuLv2.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
				if (jrs2 != null) {
					jrs2.close();
				}
				if (jrs3 != null) {
					jrs3.close();
				}
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadMenu", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlMenu;
	}
	
	public static List<model.mdlMenu> LoadMenuByID(String lMenuID,String lCommand) {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		Connection connection = null;
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		
		PreparedStatement pstmLoadMenuLv0 = null;
		ResultSet jrsMenuLv0 = null;
		
		PreparedStatement pstmLoadMenuLv1 = null;
		ResultSet jrsMenuLv1 = null;
		
		PreparedStatement pstmLoadMenuLv2 = null;
		ResultSet jrsMenuLv2 = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadMenu = "SELECT MenuID,MenuName,MenuUrl,Type,Level FROM menu WHERE MenuID = ? ORDER BY MenuID";
			pstmLoadMenu = connection.prepareStatement(sqlLoadMenu);
			pstmLoadMenu.setString(1, lMenuID);
			pstmLoadMenu.addBatch();
			jrs = pstmLoadMenu.executeQuery();
			Globals.gCommand = pstmLoadMenu.toString();
									
			while(jrs.next()){
				
				String menuLevel = jrs.getString("Level");
				if(menuLevel.contentEquals("0"))
				{
					String sqlLoadMenuLv0 = "SELECT MenuID,MenuName,MenuUrl,Type,Level FROM menu WHERE Type = ? ORDER BY MenuID";
					pstmLoadMenuLv0 = connection.prepareStatement(sqlLoadMenuLv0);
					pstmLoadMenuLv0.setString(1, jrs.getString("Type"));
					pstmLoadMenuLv0.addBatch();
					jrsMenuLv0 = pstmLoadMenuLv0.executeQuery();
					Globals.gCommand = pstmLoadMenuLv0.toString();
					
					while(jrsMenuLv0.next())
					{
						model.mdlMenu mdlMenuLv0 = new model.mdlMenu();
					
						mdlMenuLv0.setMenuID(jrsMenuLv0.getString("MenuID"));
						
						if(jrsMenuLv0.getString("Level").contentEquals("0"))
							mdlMenuLv0.setMenuName(jrsMenuLv0.getString("MenuName"));
						else
							mdlMenuLv0.setMenuName("- " + jrsMenuLv0.getString("MenuName"));
							
						mdlMenuLv0.setMenuUrl(jrsMenuLv0.getString("MenuUrl"));
						mdlMenuLv0.setType(jrsMenuLv0.getString("Type"));
						mdlMenuLv0.setLevel(jrsMenuLv0.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv0);
					}
				}
				else if(menuLevel.contentEquals("1"))
				{
					String sqlLoadMenuLv1 = "";
					if(lCommand.contentEquals("Add"))
					{
						sqlLoadMenuLv1 = "SELECT MenuID,MenuName,MenuUrl,Type,Level FROM menu "
								+ "WHERE (MenuID LIKE ?) OR (Type=? AND Level=0) ORDER BY MenuID";
						
						pstmLoadMenuLv1 = connection.prepareStatement(sqlLoadMenuLv1);
						pstmLoadMenuLv1.setString(1, "%" + jrs.getString("MenuID") + "%");
						pstmLoadMenuLv1.setString(2, jrs.getString("Type"));
						pstmLoadMenuLv1.addBatch();
					}
					else
					{
						sqlLoadMenuLv1 = "SELECT MenuID,MenuName,MenuUrl,Type,Level FROM menu "
								+ "WHERE (MenuID LIKE ?) ORDER BY MenuID";
						
						pstmLoadMenuLv1 = connection.prepareStatement(sqlLoadMenuLv1);
						pstmLoadMenuLv1.setString(1, "%" + jrs.getString("MenuID") + "%");
						pstmLoadMenuLv1.addBatch();
					}
					
					jrsMenuLv1 = pstmLoadMenuLv1.executeQuery();
					Globals.gCommand = pstmLoadMenuLv1.toString();
					
					while(jrsMenuLv1.next())
					{
						model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
					
						mdlMenuLv1.setMenuID(jrsMenuLv1.getString("MenuID"));
						
						if(jrsMenuLv1.getString("Level").contentEquals("0"))
							mdlMenuLv1.setMenuName(jrsMenuLv1.getString("MenuName"));
						else
							mdlMenuLv1.setMenuName("- " + jrsMenuLv1.getString("MenuName"));
							
						mdlMenuLv1.setMenuUrl(jrsMenuLv1.getString("MenuUrl"));
						mdlMenuLv1.setType(jrsMenuLv1.getString("Type"));
						mdlMenuLv1.setLevel(jrsMenuLv1.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv1);
					}
				}
				else
				{
					String sqlLoadMenuLv2 = "";
					if(lCommand.contentEquals("Add"))
					{
						sqlLoadMenuLv2 = "SELECT MenuID,MenuName,MenuUrl,Type,Level FROM menu "
								+ "WHERE (MenuID=?) OR (Type=? AND Level=0) OR (MenuID=? AND Level=1) ORDER BY MenuID";
						
						pstmLoadMenuLv2 = connection.prepareStatement(sqlLoadMenuLv2);
						pstmLoadMenuLv2.setString(1, jrs.getString("MenuID"));
						pstmLoadMenuLv2.setString(2, jrs.getString("Type"));
						pstmLoadMenuLv2.setString(3, jrs.getString("MenuID").substring(0, 4));
						pstmLoadMenuLv2.addBatch();
					}
					else
					{
						sqlLoadMenuLv2 = "SELECT MenuID,MenuName,MenuUrl,Type,Level FROM menu "
								+ "WHERE (MenuID=?)";
						
						pstmLoadMenuLv2 = connection.prepareStatement(sqlLoadMenuLv2);
						pstmLoadMenuLv2.setString(1, jrs.getString("MenuID"));
						pstmLoadMenuLv2.addBatch();
					}
					
					jrsMenuLv2 = pstmLoadMenuLv2.executeQuery();
					Globals.gCommand = pstmLoadMenuLv2.toString();
					
					while(jrsMenuLv2.next())
					{
						model.mdlMenu mdlMenuLv2 = new model.mdlMenu();
					
						mdlMenuLv2.setMenuID(jrsMenuLv2.getString("MenuID"));
						
						if(jrsMenuLv2.getString("Level").contentEquals("0"))
							mdlMenuLv2.setMenuName(jrsMenuLv2.getString("MenuName"));
						else
							mdlMenuLv2.setMenuName("- " + jrsMenuLv2.getString("MenuName"));
							
						mdlMenuLv2.setMenuUrl(jrsMenuLv2.getString("MenuUrl"));
						mdlMenuLv2.setType(jrsMenuLv2.getString("Type"));
						mdlMenuLv2.setLevel(jrsMenuLv2.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv2);
					}
				}
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMenuByID", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (pstmLoadMenuLv0 != null) {
					pstmLoadMenuLv0.close();
				}
				if (pstmLoadMenuLv1 != null) {
					pstmLoadMenuLv1.close();
				}
				if (pstmLoadMenuLv2 != null) {
					pstmLoadMenuLv2.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
				if (jrsMenuLv0 != null) {
					jrsMenuLv0.close();
				}
				if (jrsMenuLv1 != null) {
					jrsMenuLv1.close();
				}
				if (jrsMenuLv2 != null) {
					jrsMenuLv2.close();
				}
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadMenuByID", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlMenu;
	}
	
	public static String InsertUserRule( String lRoleID,String lRoleName, String[] llistAllMenuID, String[] llistAllowedMenuID, String[] llistEditableMenuID) 
	{	
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmInsertRole = null;
		PreparedStatement pstmInsertAccessRole = null;
		PreparedStatement pstmUpdateIsAccess = null;
		PreparedStatement pstmUpdateIsModify = null;
		PreparedStatement pstmCheck = null;
		ResultSet jrsCheck = null;
		Boolean check = false;
			
		try{
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//check duplicate role id
			String sqlCheck = "SELECT RoleID FROM role WHERE RoleID=?;";
			pstmCheck = connection.prepareStatement(sqlCheck);
			
			//insert sql parameter and execute for check
			pstmCheck.setString(1,  lRoleID);
			jrsCheck = pstmCheck.executeQuery();
			Globals.gCommand = pstmCheck.toString();
			
			while(jrsCheck.next()){
				check = true;
			}
				//if no duplicate
				if(check==false)
				{
					connection.setAutoCommit(false);
					//Execute insert Role Before
					pstmInsertRole = connection.prepareStatement("INSERT INTO role(`RoleID`, `RoleName`) "
																+ "VALUES (?,?)");
					pstmInsertRole.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
					pstmInsertRole.setString(2,  ValidateNull.NulltoStringEmpty(lRoleName));
					Globals.gCommand = pstmInsertRole.toString();
					pstmInsertRole.executeUpdate();

					pstmUpdateIsAccess = connection.prepareStatement("INSERT INTO access_role(`RoleID`, `MenuID`, `IsAccess`, `IsModify`) "
														+ "VALUES (?,?,?,?) "
														+ "ON DUPLICATE KEY UPDATE `IsAccess`=?");
					pstmUpdateIsModify = connection.prepareStatement("UPDATE access_role "
																+ "SET `IsModify`= ? "
																+ "WHERE `RoleID`=? AND `MenuID`=?");
						
					//insert access role process parameter
					pstmInsertAccessRole = connection.prepareStatement("INSERT INTO access_role(`RoleID`, `MenuID`, `IsAccess`, `IsModify`) "
														+ "VALUES (?,?,?,?) "
														+ "ON DUPLICATE KEY UPDATE `RoleID`=?");
					for(String lmenuid : llistAllMenuID)
					{
						pstmInsertAccessRole.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
						pstmInsertAccessRole.setString(2,  ValidateNull.NulltoStringEmpty(lmenuid));
						pstmInsertAccessRole.setBoolean(3, false);
						pstmInsertAccessRole.setBoolean(4, false);
						pstmInsertAccessRole.setString(5,  ValidateNull.NulltoStringEmpty(lRoleID));
							
						Globals.gCommand = pstmInsertAccessRole.toString();
						pstmInsertAccessRole.executeUpdate();
					}
					
					//update isaccess process parameter

					for(String lmenuid : llistAllowedMenuID)
					{
						//nanda						
						pstmUpdateIsAccess.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
						pstmUpdateIsAccess.setString(2,  ValidateNull.NulltoStringEmpty(lmenuid));
						pstmUpdateIsAccess.setBoolean(3, true);
						pstmUpdateIsAccess.setBoolean(4, false);
						pstmUpdateIsAccess.setBoolean(5, true);
								
						Globals.gCommand = pstmUpdateIsAccess.toString();
						pstmUpdateIsAccess.executeUpdate();
					}
					
						
					//update ismodify process parameter
					for(String lmenuid : llistEditableMenuID)
					{
						pstmUpdateIsModify.setBoolean(1, true);
						pstmUpdateIsModify.setString(2, ValidateNull.NulltoStringEmpty(lRoleID));
						pstmUpdateIsModify.setString(3, ValidateNull.NulltoStringEmpty(lmenuid));
							
						Globals.gCommand = pstmUpdateIsModify.toString();
						pstmUpdateIsModify.executeUpdate();
					}
					
						
					connection.commit(); //commit transaction if all of the proccess is running well
						
					Globals.gReturn_Status = "Success Insert User Role";
				}
				//if duplicate
				else {
					Globals.gReturn_Status = "Hak/level akses sudah ada";
				}
		}
		
		catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
		           	LogAdapter.InsertLogExc(excep.toString(), "TransactionInsertUserRule", Globals.gCommand , Globals.user);
		  			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertUserRule", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
					if (pstmInsertRole != null) {
						pstmInsertRole.close();
					}
					if (pstmInsertAccessRole != null) {
						pstmInsertAccessRole.close();
					}
					if (pstmUpdateIsAccess != null) {
						pstmUpdateIsAccess.close();
					}
					if (pstmUpdateIsModify != null) {
						pstmUpdateIsModify.close();
					}
					if (pstmCheck != null) {
						 pstmCheck.close();
					 }
		        
					connection.setAutoCommit(true);
		        
					if (connection != null) {
						connection.close();
					}
					if (jrsCheck != null) {
						 jrsCheck.close();
					 }
				}
				catch(Exception ex){
					LogAdapter.InsertLogExc(ex.toString(), "InsertUserRule", "close opened connection protocol", Globals.user);
				}
		    }
			
			return Globals.gReturn_Status;
	}

	public static String UpdateUserRule( String lRoleID,String lRoleName, String[] llistAllMenuID, String[] llistAllowedMenuID, String[] llistEditableMenuID) {	
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmUpdateIsAccess = null;
		PreparedStatement pstmUpdateIsModify = null;
		PreparedStatement pstmUpdateRoleName = null;
		PreparedStatement pstmUpdateIsModifynAccessAll = null;
		try{ 
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);
			
			pstmUpdateRoleName = connection.prepareStatement("UPDATE role SET `RoleName`= ? WHERE `RoleID`=?");
			pstmUpdateRoleName.setString(1, ValidateNull.NulltoStringEmpty(lRoleName));
			pstmUpdateRoleName.setString(2, ValidateNull.NulltoStringEmpty(lRoleID));
			Globals.gCommand = pstmUpdateRoleName.toString();
			pstmUpdateRoleName.executeUpdate();
			
			
			pstmUpdateIsModifynAccessAll = connection.prepareStatement("UPDATE access_role "
														+ "SET `IsAccess`=?,`IsModify`= ? WHERE `RoleID`=?");
			pstmUpdateIsModifynAccessAll.setBoolean(1, false);
			pstmUpdateIsModifynAccessAll.setBoolean(2, false);
			pstmUpdateIsModifynAccessAll.setString(3, ValidateNull.NulltoStringEmpty(lRoleID));
			Globals.gCommand = pstmUpdateIsModifynAccessAll.toString();
			pstmUpdateIsModifynAccessAll.executeUpdate();
			
			
			pstmUpdateIsAccess = connection.prepareStatement("INSERT INTO access_role(`RoleID`, `MenuID`, `IsAccess`, `IsModify`) "
												+ "VALUES (?,?,?,?) "
												+ "ON DUPLICATE KEY UPDATE `IsAccess`=?");
			//update isaccess process parameter	
			if (llistAllowedMenuID.length != 0)
			{
			
				//insert access role process parameter
				for(String lmenuid : llistAllowedMenuID)
				{	
					pstmUpdateIsAccess.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
					pstmUpdateIsAccess.setString(2,  ValidateNull.NulltoStringEmpty(lmenuid));
					pstmUpdateIsAccess.setBoolean(3, true);
					pstmUpdateIsAccess.setBoolean(4, false);
					pstmUpdateIsAccess.setBoolean(5, true);
					
					Globals.gCommand = pstmUpdateIsAccess.toString();
					pstmUpdateIsAccess.executeUpdate();
				}
			}
			
			
			pstmUpdateIsModify = connection.prepareStatement("UPDATE access_role "
					+ "SET `IsModify`= ? WHERE `RoleID`=?  AND `MenuID`=?");
			//update ismodify process parameter	
			if (llistEditableMenuID.length != 0){
				 for(String lmenuid : llistEditableMenuID)
				{
					pstmUpdateIsModify.setBoolean(1, true);
					pstmUpdateIsModify.setString(2, ValidateNull.NulltoStringEmpty(lRoleID));
					pstmUpdateIsModify.setString(3, ValidateNull.NulltoStringEmpty(lmenuid));
					
					Globals.gCommand = pstmUpdateIsModify.toString();
					pstmUpdateIsModify.executeUpdate();
				}
		}
		
		connection.commit(); //commit transaction if all of the proccess is running well
		
		Globals.gReturn_Status = "Success Update User Role";
		}
		
		catch (Exception e) {
		        if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
			           	LogAdapter.InsertLogExc(excep.toString(), "TransactionUpdateUserRule", Globals.gCommand , Globals.user);
			           	Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
	        LogAdapter.InsertLogExc(e.toString(), "UpdateUserRule", Globals.gCommand , Globals.user);
	    	Globals.gReturn_Status = "Database Error";
		}
		finally {
			try{
				if (pstmUpdateIsAccess != null) {
					pstmUpdateIsAccess.close();
				}
				if (pstmUpdateIsModify != null) {
					pstmUpdateIsModify.close();
				}
				if (pstmUpdateRoleName != null) {
					pstmUpdateRoleName.close();
				}
				if (pstmUpdateIsModifynAccessAll != null) {
					pstmUpdateIsModifynAccessAll.close();
				}
				        
				connection.setAutoCommit(true);
				        
				if (connection != null) {
					connection.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "UpdateUserRule", "close opened connection protocol", Globals.user);
			}
		}
		
		return Globals.gReturn_Status;
	}
	
	public static List<model.mdlMenu> LoadAllowedMenu(String lRoleID) {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		Connection connection = null;
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		
		PreparedStatement pstmLoadMenuLv1 = null;
		ResultSet jrs2 = null;
		
		PreparedStatement pstmLoadMenuLv2 = null;
		ResultSet jrs3 = null;
		
		Globals.gCommand = "";
		try{
			 connection = database.RowSetAdapter.getConnection();
			
			String sqlLoadMenu = "SELECT a.MenuID,a.MenuName,a.MenuUrl,a.Type,a.Level FROM menu a "
					+ "INNER JOIN access_role b ON b.MenuID=a.MenuID AND b.IsAccess=1 AND b.RoleID=? "
					+ "WHERE a.Level=0 ORDER BY a.MenuID";
			pstmLoadMenu = connection.prepareStatement(sqlLoadMenu);
			pstmLoadMenu.setString(1, lRoleID);
			pstmLoadMenu.addBatch();
			Globals.gCommand = pstmLoadMenu.toString();
			jrs = pstmLoadMenu.executeQuery();
									
			while(jrs.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(jrs.getString("MenuID"));
				mdlMenu.setMenuName(jrs.getString("MenuName"));
				mdlMenu.setMenuUrl(jrs.getString("MenuUrl"));
				mdlMenu.setType(jrs.getString("Type"));
				mdlMenu.setLevel(jrs.getString("Level"));
				
				listmdlMenu.add(mdlMenu);
				
				String sqlLoadMenuLv1 = "SELECT a.MenuID,a.MenuName,a.MenuUrl,a.Type,a.Level FROM menu a "
						+ "INNER JOIN access_role b ON b.MenuID=a.MenuID AND b.IsAccess=1 AND b.RoleID=? "
						+ "WHERE a.Type=? AND a.Level=1 ORDER BY a.MenuID";
				pstmLoadMenuLv1 = connection.prepareStatement(sqlLoadMenuLv1);
				pstmLoadMenuLv1.setString(1, lRoleID);
				pstmLoadMenuLv1.setString(2, mdlMenu.getType());
				pstmLoadMenuLv1.addBatch();
				Globals.gCommand = pstmLoadMenuLv1.toString();
				jrs2 = pstmLoadMenuLv1.executeQuery();
				
				while(jrs2.next()) {
					model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
					mdlMenuLv1.setMenuID(jrs2.getString("MenuID"));
					mdlMenuLv1.setMenuName("- "+jrs2.getString("MenuName"));
					mdlMenuLv1.setMenuUrl(jrs2.getString("MenuUrl"));
					mdlMenuLv1.setType(jrs2.getString("Type"));
					mdlMenuLv1.setLevel(jrs2.getString("Level"));
					
					listmdlMenu.add(mdlMenuLv1);
					
					String sqlLoadMenuLv2 = "SELECT a.MenuID,a.MenuName,a.MenuUrl,a.Type,a.Level FROM menu a "
							+ "INNER JOIN access_role b ON b.MenuID=a.MenuID AND b.IsAccess=1 AND b.RoleID=? "
							+ "WHERE a.Type=? AND a.Level=2 AND a.MenuID LIKE ? ORDER BY a.MenuID";
					pstmLoadMenuLv2 = connection.prepareStatement(sqlLoadMenuLv2);
					pstmLoadMenuLv2.setString(1, lRoleID);
					pstmLoadMenuLv2.setString(2, mdlMenu.getType());
					pstmLoadMenuLv2.setString(3, "%" + mdlMenuLv1.getMenuID() + "%");
					pstmLoadMenuLv2.addBatch();
					Globals.gCommand = pstmLoadMenuLv2.toString();
					jrs3 = pstmLoadMenuLv2.executeQuery();
					
					while(jrs3.next()) {
						model.mdlMenu mdlMenuLv2 = new model.mdlMenu();
						mdlMenuLv2.setMenuID(jrs3.getString("MenuID"));
						mdlMenuLv2.setMenuName("- "+jrs3.getString("MenuName"));
						mdlMenuLv2.setMenuUrl(jrs3.getString("MenuUrl"));
						mdlMenuLv2.setType(jrs3.getString("Type"));
						mdlMenuLv2.setLevel(jrs3.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv2);
					}
				}
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadAllowedMenu", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (pstmLoadMenuLv1 != null) {
					pstmLoadMenuLv1.close();
				}
				if (pstmLoadMenuLv2 != null) {
					pstmLoadMenuLv2.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
				if (jrs2 != null) {
					jrs2.close();
				}
				if (jrs3 != null) {
					jrs3.close();
				}
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadAllowedMenu", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlMenu;
	}

	public static List<model.mdlMenu> LoadCRUDMenu() throws Exception {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		Connection connection = database.RowSetAdapter.getConnection();
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		try{
			
			String sqlLoadMenu = "SELECT MenuID,MenuName,MenuUrl,Type,`Level`,CRUD FROM menu "
					+ "WHERE CRUD=1 ORDER BY MenuID";
			pstmLoadMenu = connection.prepareStatement(sqlLoadMenu);
			jrs = pstmLoadMenu.executeQuery();
			Globals.gCommand = sqlLoadMenu;
									
			while(jrs.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(jrs.getString("MenuID"));
				
				if(jrs.getString("MenuName").contentEquals("Master"))
					mdlMenu.setMenuName(jrs.getString("MenuName")+" "+jrs.getString("Type"));
				else
					mdlMenu.setMenuName(jrs.getString("MenuName"));
				
				mdlMenu.setMenuUrl(jrs.getString("MenuUrl"));
				mdlMenu.setType(jrs.getString("Type"));
				mdlMenu.setLevel(jrs.getString("Level"));
				mdlMenu.setCRUD(jrs.getBoolean("CRUD"));
				mdlMenu.setIsModify(false);
				
				listmdlMenu.add(mdlMenu);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCRUDMenu", Globals.gCommand , Globals.user);
		}
		finally {
			if (pstmLoadMenu != null) {
				pstmLoadMenu.close();
			}
			if (connection != null) {
				connection.close();
			}
			if (jrs != null) {
				jrs.close();
			}
		}

		return listmdlMenu;
	}

	public static List<model.mdlMenu> LoadEditableMenu(String lRoleID) {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		Connection connection = null;
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		
		try{
			connection = database.RowSetAdapter.getConnection();
			pstmLoadMenu = connection.prepareStatement("SELECT a.MenuID,a.MenuName,b.IsModify,b.RoleID FROM menu a "
										+ "INNER JOIN access_role b ON b.MenuID=a.MenuID "
										+ "WHERE a.CRUD=1 AND b.IsModify=1  AND b.RoleID = ? ORDER BY a.MenuID");
			pstmLoadMenu.setString(1, lRoleID);
																																										pstmLoadMenu.addBatch();
			jrs = pstmLoadMenu.executeQuery();
			Globals.gCommand = pstmLoadMenu.toString();
									
			while(jrs.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(jrs.getString("MenuID"));
				mdlMenu.setMenuName(jrs.getString("MenuName"));
				mdlMenu.setIsModify(jrs.getBoolean("IsModify"));
				
				listmdlMenu.add(mdlMenu);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadEditableMenuWithParam", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadEditableMenuWithParam", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlMenu;
	}

	public static List<model.mdlMenu> LoadEditableMenu() {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		Connection connection = null;
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		
		try{
			connection = database.RowSetAdapter.getConnection();
			pstmLoadMenu = connection.prepareStatement("SELECT a.MenuID,a.MenuName,a.Type,b.IsModify "
					+ "FROM menu a "
					+ "INNER JOIN access_role b ON b.MenuID=a.MenuID "
					+ "WHERE CRUD=1 "
					+ "ORDER BY a.MenuID");
			
			jrs = pstmLoadMenu.executeQuery();
			Globals.gCommand = pstmLoadMenu.toString();
									
			while(jrs.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(jrs.getString("MenuID"));
				
				if(jrs.getString("MenuName").contentEquals("Master"))
					mdlMenu.setMenuName(jrs.getString("MenuName")+" "+jrs.getString("Type"));
				else
					mdlMenu.setMenuName(jrs.getString("MenuName"));
				
				mdlMenu.setIsModify(jrs.getBoolean("IsModify"));
				
				listmdlMenu.add(mdlMenu);
			}
			if(!jrs.next())
			{
				listmdlMenu = LoadCRUDMenu();
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadEditableMenu", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadEditableMenu", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlMenu;
	}
	
	public static String DeleteUserRule(String lRoleID) 
	{	
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmRole = null;
		PreparedStatement pstmAccessRole = null;
			
		try{
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);
			
			//Execute insert Role Before
			pstmRole = connection.prepareStatement("DELETE FROM role WHERE RoleID = ?");
			pstmRole.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
			Globals.gCommand = pstmRole.toString();
			pstmRole.executeUpdate();
			
			pstmAccessRole = connection.prepareStatement("DELETE FROM access_role WHERE RoleID = ?");
			pstmAccessRole.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
			Globals.gCommand = pstmAccessRole.toString();
			pstmAccessRole.executeUpdate();
				
			connection.commit(); //commit transaction if all of the proccess is running well
				
			Globals.gReturn_Status = "Success Delete User Role";
		}
		
			
		catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
		           	LogAdapter.InsertLogExc(excep.toString(), "TransactionDeleteUserRule", Globals.gCommand , Globals.user);
		  			Globals.gReturn_Status = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "DeleteUserRule", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
			}
			finally {
				try{
					if (pstmRole != null) {
						pstmRole.close();
					}
					if (pstmAccessRole != null) {
						pstmAccessRole.close();
					}
		        
					connection.setAutoCommit(true);
		        
					if (connection != null) {
						connection.close();
					}
				}
				catch(Exception ex){
					LogAdapter.InsertLogExc(ex.toString(), "DeleteUserRule", "close opened connection protocol", Globals.user);
				}
		    }
			
		return Globals.gReturn_Status;
	}
	
	public static Boolean CheckMenu(String MenuURL, String UserID)  {
		Connection connection = null;
		PreparedStatement pstmCheckMenu = null;
		ResultSet jrs = null;
		Boolean CheckMenu = false;
		try{
			connection = database.RowSetAdapter.getConnection();
			pstmCheckMenu = connection.prepareStatement("SELECT a.IsAccess FROM access_role a "
					+ "INNER JOIN menu b ON b.MenuID = a.MenuID "
					+ "INNER JOIN user_login c ON c.RoleId = a.RoleID "
					+ "WHERE b.MenuUrl=? AND c.UserId=?");
			pstmCheckMenu.setString(1, MenuURL);
			pstmCheckMenu.setString(2, UserID);
			//pstmCheckMenu.addBatch();
			jrs = pstmCheckMenu.executeQuery();
			Globals.gCommand = pstmCheckMenu.toString();
									
			while(jrs.next()){
				if(jrs.getBoolean("IsAccess")==true)
				{
					CheckMenu = true;
				}
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CheckMenu", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				if (pstmCheckMenu != null) {
					pstmCheckMenu.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "CheckMenu", "close opened connection protocol", Globals.user);
			}
		}

		return CheckMenu;
	}

	public static String SetMenuButtonStatus(String MenuURL, String UserID)  {
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String ButtonStatus = "enabled";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("SELECT a.IsModify FROM access_role a "
					+ "INNER JOIN menu b ON b.MenuID = a.MenuID "
					+ "INNER JOIN user_login c ON c.RoleId = a.RoleID "
					+ "WHERE b.MenuUrl=?  AND c.UserId=?");
			pstm.setString(1, MenuURL);
			pstm.setString(2, UserID);
			pstm.addBatch();
			jrs = pstm.executeQuery();
			Globals.gCommand = pstm.toString();
									
			while(jrs.next()){
				if(jrs.getBoolean("IsModify")==false)
				{
					ButtonStatus = "disabled";
				}
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "SetMenuButtonStatus", Globals.gCommand , Globals.user);
		}
		finally {
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "SetMenuButtonStatus", "close opened connection protocol", Globals.user);
			}
		}

		return ButtonStatus;
	}
	
}
