package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.util.List;

import com.google.gson.Gson;

import model.Globals;
import model.mdlInbound;

public class TransactionAdapter {

	public static String TransactionInbound(model.mdlInbound lParamInbound, model.mdlBatch lParamBatch, model.mdlStockSummary lParamStockSummary, String LastProductQty, String lUser) throws Exception
	{	
		Globals.gCommand = "Insert Product ID : " + lParamInbound.getProductID() + " Batch No : " + lParamBatch.getBatch_No() ;
		
		Connection connection = database.RowSetAdapter.getConnection();
		String checkTransaction = "START TRANSACTION;"
				+ "INSERT INTO batch(ProductID, Batch_No, Packing_No, GRDate,Expired_Date,Vendor_Batch_No,VendorID,Created_by,Created_at,Updated_by,Updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?);"
				+ "INSERT INTO inbound_detail(DocNumber,DocLine,ProductID,Qty_UOM,UOM,Qty_BaseUOM,BaseUOM,Batch_No,Vendor_Batch_No,Packing_No,Expired_Date,Created_by,Created_at,Updated_by,Updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"
				+ "INSERT INTO stock_summary(Period,ProductID,PlantID,WarehouseID,Batch_No,Qty,UOM,Created_by,Created_at,Updated_by,Updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE Qty=?;"
				+ "COMMIT;";
		PreparedStatement pstmInsertBatch = connection.prepareStatement(checkTransaction);
		
		try{
			
			//insert batch process
			pstmInsertBatch.setString(1,  ValidateNull.NulltoStringEmpty(lParamBatch.getProductID()));
			pstmInsertBatch.setString(2,  ValidateNull.NulltoStringEmpty(lParamBatch.getBatch_No()));
			pstmInsertBatch.setString(3,  ValidateNull.NulltoStringEmpty(lParamBatch.getPacking_No()));
			pstmInsertBatch.setString(4,  ValidateNull.NulltoStringEmpty(lParamBatch.getGRDate()));
			pstmInsertBatch.setString(5,  ValidateNull.NulltoStringEmpty(lParamBatch.getExpired_Date()));
			pstmInsertBatch.setString(6,  ValidateNull.NulltoStringEmpty(lParamBatch.getVendor_Batch_No()));
			pstmInsertBatch.setString(7,  ValidateNull.NulltoStringEmpty(lParamBatch.getVendorID())); 
			pstmInsertBatch.setString(8,  ValidateNull.NulltoStringEmpty(lUser));
			pstmInsertBatch.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmInsertBatch.setString(10, ValidateNull.NulltoStringEmpty(lUser));
			pstmInsertBatch.setString(11, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			
			//insert inbound detail process
			pstmInsertBatch.setString(12, ValidateNull.NulltoStringEmpty(lParamInbound.getDocNumber()));
			pstmInsertBatch.setString(13, ValidateNull.NulltoStringEmpty(lParamInbound.getDocLine()));
			pstmInsertBatch.setString(14, ValidateNull.NulltoStringEmpty(lParamInbound.getProductID()));
			pstmInsertBatch.setString(15, ValidateNull.NulltoStringEmpty(lParamInbound.getQtyUom()));
			pstmInsertBatch.setString(16, ValidateNull.NulltoStringEmpty(lParamInbound.getUom()));
			pstmInsertBatch.setString(17, ValidateNull.NulltoStringEmpty(lParamInbound.getQtyBaseUom()));
			pstmInsertBatch.setString(18, ValidateNull.NulltoStringEmpty(lParamInbound.getBaseUom()));
			pstmInsertBatch.setString(19, ValidateNull.NulltoStringEmpty(lParamInbound.getBatchNo()));
			pstmInsertBatch.setString(20, ValidateNull.NulltoStringEmpty(lParamInbound.getVendorBatchNo()));
			pstmInsertBatch.setString(21, ValidateNull.NulltoStringEmpty(lParamInbound.getPackingNo()));
			pstmInsertBatch.setString(22, ValidateNull.NulltoStringEmpty(lParamInbound.getExpiredDate()));
			pstmInsertBatch.setString(23, ValidateNull.NulltoStringEmpty(lUser));
			pstmInsertBatch.setString(24, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmInsertBatch.setString(25, ValidateNull.NulltoStringEmpty(lUser));
			pstmInsertBatch.setString(26, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			
			//insert stock summary process
			int intLastProductQty = Integer.parseInt(LastProductQty);
			int intnewProductQty = Integer.parseInt(lParamInbound.getQtyBaseUom());
			int totalNewProductQty = intLastProductQty+intnewProductQty;
			
			pstmInsertBatch.setString(27, ValidateNull.NulltoStringEmpty(lParamStockSummary.getPeriod()));
			pstmInsertBatch.setString(28, ValidateNull.NulltoStringEmpty(lParamStockSummary.getProductID()));
			pstmInsertBatch.setString(29, ValidateNull.NulltoStringEmpty(lParamStockSummary.getPlantID()));
			pstmInsertBatch.setString(30, ValidateNull.NulltoStringEmpty(lParamStockSummary.getWarehouseID()));
			pstmInsertBatch.setString(31, ValidateNull.NulltoStringEmpty(lParamStockSummary.getBatch_No()));
			pstmInsertBatch.setString(32, ValidateNull.NulltoStringEmpty(String.valueOf(totalNewProductQty)));
			pstmInsertBatch.setString(33, ValidateNull.NulltoStringEmpty(lParamStockSummary.getUOM()));
			pstmInsertBatch.setString(34, ValidateNull.NulltoStringEmpty(lUser));
			pstmInsertBatch.setString(35, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmInsertBatch.setString(36, ValidateNull.NulltoStringEmpty(lUser));
			pstmInsertBatch.setString(37, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmInsertBatch.setString(38, ValidateNull.NulltoStringEmpty(String.valueOf(intLastProductQty)));
			
			pstmInsertBatch.addBatch();
				
			pstmInsertBatch.executeBatch();
			
//			pstmInsertBatch.close();
//			connection.close();
			
			Globals.gReturn_Status = "Success Insert Inbound Detail";
		}
		
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertInboundDetail", Globals.gCommand , lUser);
			Globals.gReturn_Status = "Error Insert Inbound Detail";
		}
		finally {
			 if (pstmInsertBatch != null) {
				 pstmInsertBatch.close();
			 }
			 if (connection != null) {
				 connection.close();
			 }
		 }
		
		return Globals.gReturn_Status;
	}
	
	//Insert Transaction For Outbound
		public static String InsertTransactionOutbound(model.mdlOutboundDetail lmdlOutboundDetail, model.mdlStockSummary lmdlStockSummary) throws Exception
		{
			Connection connection = database.RowSetAdapter.getConnection();
			String sql = "START TRANSACTION;"
					+ "INSERT INTO outbound_detail(`DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No`) VALUES (?,?,?,?,?,?,?,?,?);"
					+ "UPDATE `stock_summary` SET `Qty`=? WHERE `Period`=?, `ProductID`=?, `PlantID`=?, `WarehouseID`=?, `Batch_No`=?;"
					+ "COMMIT;";
			PreparedStatement pstm = connection.prepareStatement(sql);
			
			try{
				
				//parameter outbound_detail
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getDocNumber()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getDocLine()));
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getProductID()));
				pstm.setString(4,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getQty_UOM()));
				pstm.setString(5,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getUOM()));
				pstm.setString(6,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getQty_BaseUOM()));
				pstm.setString(7,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getBaseUOM()));
				pstm.setString(8,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getBatch_No()));
				pstm.setString(9,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getPacking_No()));
				
				//parameter stock_summary
				pstm.setString(10,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getQty()));
				pstm.setString(11,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getPeriod()));
				pstm.setString(12,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getProductID()));
				pstm.setString(13,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getPlantID()));
				pstm.setString(14,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getWarehouseID()));
				pstm.setString(15,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getBatch_No()));
				
				
				pstm.execute();
				
//				pstm.close();
//				connection.close();	
				Globals.gReturn_Status = "Success Insert Outbound Detail";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "TransactionOutbound", Globals.gCommand , "");
				Globals.gReturn_Status = "Error Insert Outbound Detail API";
			}
			finally {
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			 }
			
			return Globals.gReturn_Status;
		}
		
		//Update Transaction For Outbound
		public static String UpdateTransactionOutbound(model.mdlOutboundDetail lmdlOutboundDetail, model.mdlStockSummary lmdlStockSummary) throws Exception
		{
			Connection connection = database.RowSetAdapter.getConnection();
			String sql = "START TRANSACTION;"
					+ "UPDATE `outbound_detail` SET `ProductID`=?, `Qty_UOM`=?, `UOM`=?, `Qty_BaseUOM`=?, `BaseUOM`=?, `Batch_No`=?, `Packing_No`=? WHERE `DocNumber`=?, `DocLine`=?;" 
					+ "UPDATE `stock_summary` SET `Qty`=? WHERE `Period`=?, `ProductID`=?, `PlantID`=?, `WarehouseID`=?, `Batch_No`=?;"
					+ "COMMIT;";
			PreparedStatement pstm = connection.prepareStatement(sql);
			
			try{
				
				//parameter outbound_detail
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getProductID()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getQty_UOM()));
				pstm.setString(3,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getUOM()));
				pstm.setString(4,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getQty_BaseUOM()));
				pstm.setString(5,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getBaseUOM()));
				pstm.setString(6,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getBatch_No()));
				pstm.setString(7,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getPacking_No()));
				pstm.setString(8,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getDocNumber()));
				pstm.setString(9,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getDocLine()));
				
				//parameter stock_summary
				pstm.setString(10,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getQty()));
				pstm.setString(11,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getPeriod()));
				pstm.setString(12,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getProductID()));
				pstm.setString(13,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getPlantID()));
				pstm.setString(14,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getWarehouseID()));
				pstm.setString(15,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getBatch_No()));
				
				
				pstm.execute();
				
//				pstm.close();
//				connection.close();	
				Globals.gReturn_Status = "Success Insert Outbound Detail";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "TransactionOutbound", Globals.gCommand , "");
				Globals.gReturn_Status = "Error Insert Outbound Detail API";
				}
			finally {
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			 }
			
			return Globals.gReturn_Status;
		}
}
