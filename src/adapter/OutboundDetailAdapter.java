package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

/** Documentation
 * 001 nanda
 */
public class OutboundDetailAdapter {
	public static model.mdlOutboundDetail LoadOutboundDetailbyID(String lDocNumber, String lDocLine) {
		model.mdlOutboundDetail mdlOutboundDetail = new model.mdlOutboundDetail();
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No` FROM outbound_detail WHERE `DocNumber` = ? AND `DocLine` = ?");
			jrs.setString(1,  lDocNumber);
			jrs.setString(1,  lDocLine);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			mdlOutboundDetail.setDocNumber(lDocNumber);
			mdlOutboundDetail.setDocLine(lDocLine);
			mdlOutboundDetail.setProductID(jrs.getString("ProductID"));
			mdlOutboundDetail.setQty_UOM(jrs.getString("Qty_UOM"));
			mdlOutboundDetail.setUOM(jrs.getString("UOM"));
			mdlOutboundDetail.setQty_BaseUOM(jrs.getString("Qty_BaseUOM"));
			mdlOutboundDetail.setBaseUOM(jrs.getString("BaseUOM"));
			mdlOutboundDetail.setBatch_No(jrs.getString("Batch_No"));
			mdlOutboundDetail.setPacking_No(jrs.getString("Packing_No"));
			
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadOutboundDetailbyID", Globals.gCommand , "");
		}

		return mdlOutboundDetail;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlOutboundDetail> LoadOutboundDetail(String lDoc, String lUser) {
		List<model.mdlOutboundDetail> listmdlOutboundDetail = new ArrayList<model.mdlOutboundDetail>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No` FROM outbound_detail WHERE `DocNumber`=? ORDER BY DocLine");
			jrs.setString(1,  lDoc);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlOutboundDetail mdlOutboundDetail = new model.mdlOutboundDetail();
				mdlOutboundDetail.setDocNumber(jrs.getString("DocNumber"));
				mdlOutboundDetail.setDocLine(jrs.getString("DocLine"));
				mdlOutboundDetail.setProductID(jrs.getString("ProductID"));
				mdlOutboundDetail.setQty_UOM(jrs.getString("Qty_UOM"));
				mdlOutboundDetail.setUOM(jrs.getString("UOM"));
				mdlOutboundDetail.setQty_BaseUOM(jrs.getString("Qty_BaseUOM"));
				mdlOutboundDetail.setBaseUOM(jrs.getString("BaseUOM"));
				mdlOutboundDetail.setBatch_No(jrs.getString("Batch_No"));
				mdlOutboundDetail.setPacking_No(jrs.getString("Packing_No"));					
				
				listmdlOutboundDetail.add(mdlOutboundDetail);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadOutboundDetail", Globals.gCommand , lUser);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadOutboundDetail", "close opened connection protocol", lUser);
			}
		}

		return listmdlOutboundDetail;
	}
	
	public static String InsertOutboundDetail(model.mdlOutboundDetail lParam,String luser)
	{
		Globals.gCommand = "Insert DocNumber : " + lParam.getDocNumber() +" DocLine : "+lParam.getDocLine();
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();			
			
			jrs.setCommand("SELECT `DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM outbound_detail LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("DocNumber",lParam.getDocNumber());
			jrs.updateString("DocLine",lParam.getDocLine());
			jrs.updateString("ProductID",lParam.getProductID());
			jrs.updateString("Qty_UOM",lParam.getQty_UOM());
			jrs.updateString("UOM",lParam.getUOM());
			jrs.updateString("Qty_BaseUOM",lParam.getQty_BaseUOM());
			jrs.updateString("BaseUOM",lParam.getBaseUOM());
			jrs.updateString("Batch_No",lParam.getBatch_No());
			jrs.updateString("Packing_No",lParam.getPacking_No());
			jrs.updateString("Created_by",luser);
			jrs.updateString("Created_at",LocalDateTime.now().toString());
			jrs.updateString("Updated_by",luser);
			jrs.updateString("Updated_at",LocalDateTime.now().toString());
			
			
			jrs.insertRow();	
			jrs.close();
			Globals.gReturn_Status = "Success Insert OutboundDetail ";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertOutboundDetail", Globals.gCommand , "");
			Globals.gReturn_Status = "Error Insert OutboundDetail" + lParam.getDocNumber();

		}
		return Globals.gReturn_Status;
	}
	
	public static String UpdateOutboundDetail(model.mdlOutboundDetail lParam, String luser)
	{
		Globals.gCommand = "Update DocNumber : " + lParam.getDocNumber() +" DocLine : "+lParam.getDocLine();
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT `DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM outbound_detail WHERE `DocNumber` = ? AND `DocLine` = ? LIMIT 1");
			jrs.setString(1,  lParam.getDocNumber());
			jrs.setString(2,  lParam.getDocLine());
			jrs.execute();
			
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("DocNumber",lParam.getDocNumber());
			jrs.updateString("DocLine",lParam.getDocLine());
			jrs.updateString("Qty_UOM",lParam.getQty_UOM());
			jrs.updateString("UOM",lParam.getUOM());
			jrs.updateString("Qty_BaseUOM",lParam.getQty_BaseUOM());
			jrs.updateString("BaseUOM",lParam.getBaseUOM());
			jrs.updateString("Batch_No",lParam.getBatch_No());
			jrs.updateString("Packing_No",lParam.getPacking_No());
			jrs.updateString("Updated_by",luser);
			jrs.updateString("Updated_at",LocalDateTime.now().toString());
			jrs.updateRow();
				
			jrs.close();
			Globals.gReturn_Status = "Success Update OutboundDetail ";
		}	
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateOutboundDetail", Globals.gCommand , "");
			Globals.gReturn_Status = "Error Update OutboundDetail";
		}
		return Globals.gReturn_Status;
	}
	
	public static String DeleteOutboundDetail(String lDocNumber,String lDocLine)
	{
		Globals.gCommand = "Delete DocNumber : " + lDocNumber+" DocLine : "+lDocLine;
		try{
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();		
			jrs.setCommand("SELECT `DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at` FROM vendor WHERE `DocNumber` = ? AND `DocLine` = ? LIMIT 1");
			jrs.setString(1, lDocNumber);
			jrs.setString(2, lDocLine);
			jrs.execute();
			
			jrs.last();
			jrs.deleteRow();
			
			jrs.close();
			Globals.gReturn_Status = "Success Delete OutboundDetail ";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteOutboundDetail", Globals.gCommand , "");
			Globals.gReturn_Status = "Error Delete OutboundDetail" + ex.toString();
		}
		return Globals.gReturn_Status;
	}

	//<<001 Nanda
	//Insert Transaction For Outbound
	public static String InsertTransactionOutbound(model.mdlOutboundDetail lmdlOutboundDetail, model.mdlStockSummary lmdlStockSummary, String luser)
	{
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstmOutboundDetail = null;
		PreparedStatement pstmStockSummary = null;
			
		try{
			connection = database.RowSetAdapter.getConnection();
			
			connection.setAutoCommit(false);
			
			String sqlOutboundDetail = "INSERT INTO outbound_detail(`DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
			//String sqlStockSummary = "UPDATE `stock_summary` SET `Qty`=? , `Updated_by`=?, `Updated_at`=? WHERE `Period`=? AND `ProductID`=? AND `PlantID`=? AND `WarehouseID`=? AND `Batch_No`=?;";
			String sqlStockSummary = "INSERT INTO stock_summary(`Period`,`ProductID`,`PlantID`,`WarehouseID`,`Batch_No`,`Qty`,`UOM`,`Created_by`,`Created_at`,`Updated_by`,`Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
			pstmOutboundDetail = connection.prepareStatement(sqlOutboundDetail);
			pstmStockSummary = connection.prepareStatement(sqlStockSummary);
			
			//parameter outbound_detail
			pstmOutboundDetail.setString(1,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getDocNumber()));
			pstmOutboundDetail.setString(2,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getDocLine()));
			pstmOutboundDetail.setString(3,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getProductID()));
			pstmOutboundDetail.setString(4,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getQty_UOM()));
			pstmOutboundDetail.setString(5,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getUOM()));
			pstmOutboundDetail.setString(6,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getQty_BaseUOM()));
			pstmOutboundDetail.setString(7,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getBaseUOM()));
			pstmOutboundDetail.setString(8,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getBatch_No()));
			pstmOutboundDetail.setString(9,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getPacking_No()));
			
			pstmOutboundDetail.setString(10,  luser);
			pstmOutboundDetail.setString(11,  LocalDateTime.now().toString());
			pstmOutboundDetail.setString(12,  luser);
			pstmOutboundDetail.setString(13,  LocalDateTime.now().toString());
			Globals.gCommand = pstmOutboundDetail.toString();
			pstmOutboundDetail.executeUpdate();
				
			//parameter stock_summary
			pstmStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getPeriod()));
			pstmStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getProductID()));
			pstmStockSummary.setString(3,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getPlantID()));
			pstmStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getWarehouseID()));
			pstmStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getBatch_No()));
			pstmStockSummary.setString(6,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getQty()));
			pstmStockSummary.setString(7,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getUOM()));
			pstmStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(luser));
			pstmStockSummary.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmStockSummary.setString(10,  ValidateNull.NulltoStringEmpty(luser));
			pstmStockSummary.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			pstmStockSummary.setString(12,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getQty()));
			pstmStockSummary.setString(13,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getUOM()));
			pstmStockSummary.setString(14,  ValidateNull.NulltoStringEmpty(luser));
			pstmStockSummary.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
			Globals.gCommand = pstmStockSummary.toString();
			pstmStockSummary.executeUpdate();
			
			connection.commit();
			Globals.gReturn_Status = "Success Insert Outbound Detail";
		}catch (Exception e ) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "TransactionOutbound", Globals.gCommand , luser);
	    			Globals.gReturn_Status = "Database Error";
	            }
	            LogAdapter.InsertLogExc(e.toString(), "InsertOutboundDetail", Globals.gCommand , luser);
    			Globals.gReturn_Status = "Database Error";
	        }
		}finally {
			try{
		        if (pstmOutboundDetail != null) {
		        	pstmOutboundDetail.close();
		        }
		        if (pstmStockSummary != null) {
		        	pstmStockSummary.close();
		        }
		        connection.setAutoCommit(true);
		        if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertOutboundDetail", "close opened connection protocol", luser);
			}
	    }
		
		
		return Globals.gReturn_Status;
	}
	
	//Update Transaction For Outbound
	public static String UpdateTransactionOutbound(model.mdlOutboundDetail lmdlOutboundDetail, model.mdlStockSummary lmdlStockSummary,String luser) throws Exception
	{
		Connection connection = database.RowSetAdapter.getConnection();
		PreparedStatement pstmOutboundDetail = null;
		PreparedStatement pstmStockSummary = null;
		
		String sqlOutboundDetail = "UPDATE `outbound_detail` SET `ProductID`=?, `Qty_UOM`=?, `UOM`=?, `Qty_BaseUOM`=?, `BaseUOM`=?, `Batch_No`=?, `Packing_No`=?, `Updated_by`=?, `Updated_at`=? WHERE `DocNumber`=? AND `DocLine`=?;";
		String sqlStockSummary = "UPDATE `stock_summary` SET `Qty`=? WHERE `Period`=? AND `ProductID`=? AND `PlantID`=? AND `WarehouseID`=? AND `Batch_No`=?;";
		 
		
		try{
			connection.setAutoCommit(false);
			
			pstmOutboundDetail = connection.prepareStatement(sqlOutboundDetail);
			pstmStockSummary = connection.prepareStatement(sqlStockSummary);
			
			//parameter outbound_detail
			pstmOutboundDetail.setString(1,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getProductID()));
			pstmOutboundDetail.setString(2,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getQty_UOM()));
			pstmOutboundDetail.setString(3,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getUOM()));
			pstmOutboundDetail.setString(4,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getQty_BaseUOM()));
			pstmOutboundDetail.setString(5,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getBaseUOM()));
			pstmOutboundDetail.setString(6,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getBatch_No()));
			pstmOutboundDetail.setString(7,  ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getPacking_No()));
			pstmOutboundDetail.setString(8,  luser);
			pstmOutboundDetail.setString(9,  LocalDateTime.now().toString());
			pstmOutboundDetail.setString(10, ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getDocNumber()));
			pstmOutboundDetail.setString(11, ValidateNull.NulltoStringEmpty(lmdlOutboundDetail.getDocLine()));
			
			
			pstmOutboundDetail.executeUpdate();
			
			//parameter stock_summary
			pstmStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getQty()));
			pstmStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getPeriod()));
			pstmStockSummary.setString(3,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getProductID()));
			pstmStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getPlantID()));
			pstmStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getWarehouseID()));
			pstmStockSummary.setString(6,  ValidateNull.NulltoStringEmpty(lmdlStockSummary.getBatch_No()));
	
			pstmStockSummary.executeUpdate();	
			
			connection.commit();
			Globals.gReturn_Status = "Success Update Outbound Detail";
		
		}catch (SQLException e ) {
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
					connection.rollback();
				} catch(SQLException excep) {
					LogAdapter.InsertLogExc(excep.toString(), "TransactionOutbound", Globals.gCommand , luser);
					Globals.gReturn_Status = "Error Update Outbound Detail ";
				}
				
				LogAdapter.InsertLogExc(e.toString(), "TransactionOutbound", Globals.gCommand , luser);
				Globals.gReturn_Status = "Error Update Outbound Detail ";
			}
		}finally {
			if (pstmOutboundDetail != null) {
				pstmOutboundDetail.close();
			}
			if (pstmStockSummary != null) {
				pstmStockSummary.close();
		}
		connection.setAutoCommit(true);
		if (connection != null) {
			 connection.close();
		 }
    }
	
	return Globals.gReturn_Status;
	}
	
	public static String GetQtybyID(String lDocNumber, String lDocLine, String luser){
		String lqty_old = "";
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			jrs.setCommand("SELECT `Qty_BaseUOM` FROM outbound_detail WHERE `DocNumber` = ? AND `DocLine` = ?");
			jrs.setString(1,  lDocNumber);
			jrs.setString(2,  lDocLine);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){
				lqty_old = jrs.getString("Qty_BaseUOM");
			}
			
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateTransactionOutbound", Globals.gCommand , luser);
			lqty_old = "Error GetQtybyID";
		}
		
		return lqty_old;
		
	}
	//>>
	
	 //<<002 Nanda
	@SuppressWarnings("resource")
	public static String GenerateDocLineOutboundDet(String lDocNumber)
	{
		String txtDocLine ="";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			jrs = database.RowSetAdapter.getJDBCRowSet();
		
			jrs.setCommand("SELECT `DocLine` FROM `outbound_detail` WHERE `DocNumber` = ? ORDER BY `Created_at` DESC LIMIT 1");
			jrs.setString(1, lDocNumber);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			while(jrs.next()){
			txtDocLine = ValidateNull.NulltoStringEmpty(jrs.getString("DocLine"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GenerateDocLineOutboundDet", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "GenerateDocLineOutboundDet", "close opened connection protocol", Globals.user);
			}
		}

	return txtDocLine;
	}
	//>>
}
