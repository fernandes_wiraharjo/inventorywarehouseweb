package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlUom;

/** Documentation
 * 001 Nanda -- Beginning
 */
public class UomAdapter {
	
	public static String InsertUom(model.mdlUom lParam)
	{	
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			mdlUom CheckUOM = LoadUOMByKey(lParam.getCode());
			if(CheckUOM.getCode() == null){
				connection = database.RowSetAdapter.getConnection();
				
				String sql = "INSERT INTO `uom`(`Code`, `Description`,`Is_BaseUOM`) VALUES (?,?,?)";
				pstm = connection.prepareStatement(sql);
				
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getCode()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getDescription()));
				pstm.setBoolean(3,  lParam.getIs_BaseUOM());
				
				pstm.addBatch();
				Globals.gCommand = pstm.toString();
				pstm.executeBatch();
				
				Globals.gReturn_Status = "Success Insert UOM";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Satuan sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertUom", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertUom", "close opened connection protocol", Globals.user);
			}
		 }
		
		return Globals.gReturn_Status;
	}

	public static String UpdateUom(model.mdlUom lParam)
	{	
		Globals.gCommand = "";
		
		Connection connection = null;
		PreparedStatement pstm = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			
			String sql = "UPDATE `uom` SET `Description`=?, `Is_BaseUom`=? "
					+ "WHERE `Code`=?";
			pstm = connection.prepareStatement(sql);
			
			pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getDescription()));
			pstm.setBoolean(2,  lParam.getIs_BaseUOM());
			pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getCode()));
			
			pstm.addBatch();	
			Globals.gCommand = pstm.toString();
			pstm.executeBatch();
			
			Globals.gReturn_Status = "Success Update UOM";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateUom", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateUom", "close opened connection protocol", Globals.user);
			}
		 }
		
		return Globals.gReturn_Status;
	}
	
	public static String InsertUomlist(List<model.mdlUom> lParamlist)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		Globals.gCommand = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			
			Gson gson = new Gson();
			Globals.gCommand = gson.toJson(lParamlist);
			
			String sql = "INSERT INTO `uom`(`Code`, `Description`,`Is_BaseUOM`) VALUES (?,?,?) "
					+ "ON DUPLICATE KEY UPDATE `Description`=?,`Is_BaseUOM`=?";
			pstm = connection.prepareStatement(sql);
			
			for(model.mdlUom lParam : lParamlist)
			{	
				pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getCode()));
				pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getDescription()));
				pstm.setBoolean(3,  lParam.getIs_BaseUOM());
				pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getDescription()));
				pstm.setBoolean(5,  lParam.getIs_BaseUOM());
			
				pstm.addBatch();	
			}
			Globals.gCommand = pstm.toString();
			pstm.executeBatch();
			
			Globals.gReturn_Status = "Success Insert list Uom API";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertUomlist", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Error Insert list Uom API";
		}
		finally {
			try{
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }	
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertUomlist", "close opened connection protocol", Globals.user);
			}
		 }
		
		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlUom> LoadUOM(String lProductID) {
		List<model.mdlUom> listmdlUom = new ArrayList<model.mdlUom>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT UOM FROM product_uom WHERE ProductID = ?");
			jrs.setString(1, lProductID);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlUom mdlUom = new model.mdlUom();
				
				mdlUom.setCode(jrs.getString("UOM"));
				
				listmdlUom.add(mdlUom);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUom", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadUom", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlUom;
	}

	@SuppressWarnings("resource")
	public static model.mdlUom LoadUOMByKey(String lCode) {
		model.mdlUom mdlUom = new model.mdlUom();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Code,Description,Is_BaseUom FROM uom WHERE Code = ?");
			jrs.setString(1, lCode);
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				mdlUom.setCode(jrs.getString("Code"));
				mdlUom.setDescription(jrs.getString("Description"));
				mdlUom.setIs_BaseUOM(jrs.getBoolean("Is_BaseUom"));
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUOMByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadUOMByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlUom;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlUom> LoadMasterUOM() {
		List<model.mdlUom> listmdlUom = new ArrayList<model.mdlUom>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Code,Description,Is_BaseUom FROM uom ORDER BY Code");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlUom mdlUom = new model.mdlUom();
				
				mdlUom.setCode(jrs.getString("Code"));
				mdlUom.setDescription(jrs.getString("Description"));
				mdlUom.setIs_BaseUOM(jrs.getBoolean("Is_BaseUom"));
				
				listmdlUom.add(mdlUom);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMasterUom", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadMasterUom", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlUom;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlUom> LoadMasterBaseUOM() {
		List<model.mdlUom> listmdlUom = new ArrayList<model.mdlUom>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Code FROM uom where Is_BaseUom=1");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlUom mdlUom = new model.mdlUom();
				
				mdlUom.setCode(jrs.getString("Code"));
				
				listmdlUom.add(mdlUom);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMasterBaseUom", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadMasterBaseUom", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlUom;
	}
}
