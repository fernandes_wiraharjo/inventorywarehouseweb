package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

public class BrandAdapter {
	public static List<model.mdlBrand> GetBrandAPI(String lUser){
		List<model.mdlBrand> listmdlBrand = new ArrayList<model.mdlBrand>();
		model.mdlToken mdlToken = TokenAdapter.GetToken();
		try {
			
			Client client = Client.create();
			
			Globals.gCommand = "http://dev.webarq.info:8081/kenmaster-be/api/brand/listing";
			WebResource webResource = client.resource(Globals.gCommand);
			
			Gson gson = new Gson();
			String json = gson.toJson(mdlToken);
			
			ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);		
			
			Globals.gReturn_Status = response.getEntity(String.class);
			
			//Type TypeDepartment = new TypeToken<ArrayList<model.mdlDepartment>>(){}.getClass();
			listmdlBrand = gson.fromJson(Globals.gReturn_Status, model.mdlBrandlist.class);
			
			
		  } catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetToken", Globals.gCommand , lUser);
//			Globals.gReturn_Status = "Error GetToken";
		  }
		
		return listmdlBrand;
		}

		public static String InsertBrandlist(List<model.mdlBrand> lParamlist, String lUser)
		{
			Connection connection = null;
			PreparedStatement pstm = null;
			try{
				connection = database.RowSetAdapter.getConnection();

				Gson gson = new Gson();
				Globals.gCommand = gson.toJson(lParamlist);
				
				
				String sql = "INSERT INTO `brand`(ID, Name, Short_Description_EN, Description_EN, Short_Description_ID, Description_ID, `Order`, Status, Slug, Created_at, Updated_at, Image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE ID=ID";
				pstm = connection.prepareStatement(sql);
				
				for(model.mdlBrand lParam : lParamlist)
				{	
					pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getId()));
					pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getName()));
					pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getShort_description_en()));
					pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getDescription_en()));
					pstm.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getShort_description_id()));
					pstm.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getDescription_id()));
					pstm.setString(7,  ValidateNull.NulltoStringEmpty(lParam.getOrder()));
					pstm.setString(8,  ValidateNull.NulltoStringEmpty(lParam.getStatus()));
					pstm.setString(9, ValidateNull.NulltoStringEmpty(lParam.getSlug()));
					pstm.setString(10, ValidateNull.NulltoStringEmpty(lParam.getCreated_at()));
					pstm.setString(11, ValidateNull.NulltoStringEmpty(lParam.getUpdated_at()));
					pstm.setString(12, ValidateNull.NulltoStringEmpty(lParam.getImage()));
					
					//pstm.executeUpdate();
					pstm.addBatch();
				}
				pstm.executeBatch();
				
				Globals.gReturn_Status = "Success Insert list Brand API";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertBrandbyAPI", Globals.gCommand , lUser);
				Globals.gReturn_Status = "Error Insert list Brand API";
			}
			finally {
				try{
					 if (pstm != null) {
						 pstm.close();
					 }
					 if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertBrandbyAPI", "close opened connection protocol", lUser);
				}
			 }
			
			return Globals.gReturn_Status;
		}
		
		public static List<model.mdlBrand> LoadBrand() {
			List<model.mdlBrand> listmdlBrand = new ArrayList<model.mdlBrand>();
			try{
				
				JdbcRowSet jrs = new JdbcRowSetImpl();
				jrs = database.RowSetAdapter.getJDBCRowSet();
				
				jrs.setCommand("SELECT ID, Name, Short_Description_EN, Description_EN, Short_Description_ID, Description_ID, `Order`, Status, Slug, Created_at, Updated_at, Image FROM `brand`");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
										
				while(jrs.next()){
					model.mdlBrand mdlBrand = new model.mdlBrand();
					mdlBrand.setId(jrs.getString("ID"));
					mdlBrand.setName(jrs.getString("Name"));
					mdlBrand.setShort_description_en(jrs.getString("Short_Description_EN"));
					mdlBrand.setDescription_en(jrs.getString("Description_EN"));
					mdlBrand.setShort_description_id(jrs.getString("Short_Description_ID"));
					mdlBrand.setDescription_id(jrs.getString("Description_ID"));	
					mdlBrand.setOrder(jrs.getString("Order"));
					mdlBrand.setStatus(jrs.getString("Status"));
					mdlBrand.setSlug(jrs.getString("Slug"));
					mdlBrand.setCreated_at(jrs.getString("Created_at"));
					mdlBrand.setUpdated_at(jrs.getString("Updated_at"));
					mdlBrand.setImage(jrs.getString("Image"));
					
					listmdlBrand.add(mdlBrand);
				}
				jrs.close();		
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadBrand", Globals.gCommand , "");
			}

			return listmdlBrand;
		}


}
