package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlMaterialStagingArea;
import model.mdlStorageBin;
import model.mdlStorageBinType;
import model.mdlTransferOrder;

public class StorageBinAdapter {

	@SuppressWarnings("resource")
	public static List<model.mdlStorageBinType> LoadStorageBinType() {
		List<model.mdlStorageBinType> listData = new ArrayList<model.mdlStorageBinType>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT a.PlantID, b.PlantName, a.WarehouseID, c.WarehouseName, a.StorageBinTypeID, a.StorageBinTypeName "
					+ "FROM wms_storagebintype a "
					+ "INNER JOIN plant b ON b.PlantID=a.PlantID "
					+ "INNER JOIN warehouse c ON c.PlantID=a.PlantID AND c.WarehouseID=a.WarehouseID "
					+ "WHERE a.PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next()){
				model.mdlStorageBinType Data = new model.mdlStorageBinType();
				Data.setPlantID(jrs.getString("PlantID"));
				Data.setPlantName(jrs.getString("PlantName"));
				Data.setWarehouseID(jrs.getString("WarehouseID"));
				Data.setWarehouseName(jrs.getString("WarehouseName"));
				Data.setStorageBinTypeID(jrs.getString("StorageBinTypeID"));
				Data.setStorageBinTypeName(jrs.getString("StorageBinTypeName"));
				listData.add(Data);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageBinType", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageBinType", "close opened connection protocol", Globals.user);
			}
		}

		return listData;
	}

	@SuppressWarnings("resource")
	public static model.mdlStorageBinType LoadStorageBinTypeByKey(String lPlantID, String lWarehouseID, String lStorageBinTypeID) {
		model.mdlStorageBinType mdlStorageBinType = new model.mdlStorageBinType();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageBinTypeID, StorageBinTypeName FROM wms_storagebintype "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageBinTypeID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lStorageBinTypeID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlStorageBinType.setPlantID(jrs.getString("PlantID"));
				mdlStorageBinType.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageBinType.setStorageBinTypeID(jrs.getString("StorageBinTypeID"));
				mdlStorageBinType.setStorageBinTypeName(jrs.getString("StorageBinTypeName"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageBinTypeByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageBinTypeByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlStorageBinType;
	}

	@SuppressWarnings("resource")
	public static List<model.mdlStorageBinType> LoadDynamicStorageBinType(String lPlantID, String lWarehouseID) {
		List<model.mdlStorageBinType> listStorageBinType = new ArrayList<mdlStorageBinType>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT StorageBinTypeID, StorageBinTypeName FROM wms_storagebintype "
						+ "WHERE PlantID = ? AND WarehouseID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				model.mdlStorageBinType StorageBinType = new model.mdlStorageBinType();
				
				StorageBinType.setStorageBinTypeID(jrs.getString("StorageBinTypeID"));
				StorageBinType.setStorageBinTypeName(jrs.getString("StorageBinTypeName"));
				
				listStorageBinType.add(StorageBinType);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDynamicStorageBinType", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDynamicStorageBinType", "close opened connection protocol", Globals.user);
			}
		}

		return listStorageBinType;
	}
	
	@SuppressWarnings("resource")
	public static String InsertStorageBinType(model.mdlStorageBinType lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlStorageBinType Check = LoadStorageBinTypeByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getStorageBinTypeID());
			if(Check.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, StorageBinTypeID, StorageBinTypeName "
								+ "FROM wms_storagebintype LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("StorageBinTypeID",lParam.getStorageBinTypeID());
				jrs.updateString("StorageBinTypeName",lParam.getStorageBinTypeName());

				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Storage Bin Type";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Tipe storage bin sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertStorageBinType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertStorageBinType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String UpdateStorageBinType(model.mdlStorageBinType lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID, WarehouseID, StorageBinTypeID, StorageBinTypeName "
							+ "FROM wms_storagebintype WHERE PlantID = ? AND WarehouseID = ? AND StorageBinTypeID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getStorageBinTypeID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("StorageBinTypeName", lParam.getStorageBinTypeName());
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Storage Bin Type";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStorageBinType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateStorageBinType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeleteStorageBinType(String lPlantID, String lWarehouseID, String lStorageBinTypeID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageBinTypeID, StorageBinTypeName "
							+ "FROM wms_storagebintype WHERE PlantID = ? AND WarehouseID = ? AND StorageBinTypeID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lStorageBinTypeID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Storage Bin Type";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteStorageBinType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteStorageBinType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlStorageBin> LoadStorageBin() {
		List<model.mdlStorageBin> listData = new ArrayList<model.mdlStorageBin>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT a.PlantID, b.PlantName, a.WarehouseID, c.WarehouseName, a.StorageTypeID, "
					+ "d.StorageTypeName, a.StorageBinID, a.StorageSectionID, e.StorageSectionName, a.StorageBinTypeID, "
					+ "f.StorageBinTypeName, "
					+ "a.TotalCapacity, a.Utilization, a.CapacityUsed, a.NoOfQuantity, a.NoStorageUnits, a.PutawayBlock, "
					+ "a.StockRemovalBlock, a.BlockReasonID "
					+ "FROM wms_storagebin a "
					+ "INNER JOIN plant b ON b.PlantID=a.PlantID "
					+ "INNER JOIN warehouse c ON c.PlantID=a.PlantID AND c.WarehouseID=a.WarehouseID "
					+ "INNER JOIN wms_storage_type d ON d.PlantID=a.PlantID AND d.WarehouseID=a.WarehouseID AND d.StorageTypeID=a.StorageTypeID "
					+ "INNER JOIN wms_storagesection e ON e.PlantID=a.PlantID AND e.WarehouseID=a.WarehouseID AND e.StorageTypeID=a.StorageTypeID AND e.StorageSectionID=a.StorageSectionID "
					+ "INNER JOIN wms_storagebintype f ON f.PlantID=a.PlantID AND f.WarehouseID=a.WarehouseID AND f.StorageBinTypeID=a.StorageBinTypeID "
					+ "WHERE a.PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next()){
				model.mdlStorageBin Data = new model.mdlStorageBin();
				Data.setPlantID(jrs.getString("PlantID"));
				Data.setPlantName(jrs.getString("PlantName"));
				Data.setWarehouseID(jrs.getString("WarehouseID"));
				Data.setWarehouseName(jrs.getString("WarehouseName"));
				Data.setStorageTypeID(jrs.getString("StorageTypeID"));
				Data.setStorageTypeName(jrs.getString("StorageTypeName"));
				Data.setStorageBinID(jrs.getString("StorageBinID"));
				Data.setStorageSectionID(jrs.getString("StorageSectionID"));
				Data.setStorageSectionName(jrs.getString("StorageSectionName"));
				Data.setStorageBinTypeID(jrs.getString("StorageBinTypeID"));
				Data.setStorageBinTypeName(jrs.getString("StorageBinTypeName"));
				Data.setTotalCapacity(jrs.getDouble("TotalCapacity"));
				Data.setUtilization(jrs.getDouble("Utilization"));
				Data.setCapacityUsed(jrs.getDouble("CapacityUsed"));
				Data.setNoOfQuantity(jrs.getInt("NoOfQuantity"));
				Data.setNoStorageUnits(jrs.getInt("NoStorageUnits"));
				Data.setPutawayBlock(jrs.getBoolean("PutawayBlock"));
				Data.setStockRemovalBlock(jrs.getBoolean("StockRemovalBlock"));
				Data.setBlockReasonID(jrs.getString("BlockReasonID"));
				
				listData.add(Data);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageBin", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageBin", "close opened connection protocol", Globals.user);
			}
		}

		return listData;
	}
	
	public static List<model.mdlStorageBin> LoadDynamicStorageBin(String lPlantID, String lWarehouseID, String lStorageTypeID) {
		List<model.mdlStorageBin> listStorageBin = new ArrayList<mdlStorageBin>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT StorageBinID, StorageSectionID, StorageBinTypeID FROM wms_storagebin "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageTypeID=?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lStorageTypeID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				model.mdlStorageBin StorageBin = new model.mdlStorageBin();
				
				StorageBin.setStorageBinID(jrs.getString("StorageBinID"));
				StorageBin.setStorageSectionID(jrs.getString("StorageSectionID"));
				StorageBin.setStorageBinTypeID(jrs.getString("StorageBinTypeID"));
				
				listStorageBin.add(StorageBin);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDynamicStorageBin", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDynamicStorageBin", "close opened connection protocol", Globals.user);
			}
		}

		return listStorageBin;
	}
	
	public static List<model.mdlStorageBin> LoadStorageBinByPlantWarehouse(String lPlantID, String lWarehouseID) {
		List<model.mdlStorageBin> listStorageBin = new ArrayList<mdlStorageBin>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT StorageTypeID, StorageBinID, StorageSectionID FROM wms_storagebin "
						+ "WHERE PlantID = ? AND WarehouseID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				model.mdlStorageBin StorageBin = new model.mdlStorageBin();
				
				StorageBin.setStorageTypeID(jrs.getString("StorageTypeID"));
				StorageBin.setStorageBinID(jrs.getString("StorageBinID"));
				StorageBin.setStorageSectionID(jrs.getString("StorageSectionID"));
				
				listStorageBin.add(StorageBin);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageBinByPlantWarehouse", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageBinByPlantWarehouse", "close opened connection protocol", Globals.user);
			}
		}

		return listStorageBin;
	}
	
	public static List<model.mdlStorageBinDetail> LoadDynamicStorageBinDetail(String lPlantID, String lWarehouseID, String lStorageTypeID, String lStorageSectionID, String lStorageBinID) {
		List<model.mdlStorageBinDetail> listStorageBinDetail = new ArrayList<model.mdlStorageBinDetail>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT ProductID, BatchNo, Qty, UOM "
							+ "FROM wms_storagebin_detail "
							+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageType=? AND StorageSection=? AND StorageBin=?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lStorageTypeID);
			jrs.setString(4,  lStorageSectionID);
			jrs.setString(5,  lStorageBinID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				model.mdlStorageBinDetail StorageBinDetail = new model.mdlStorageBinDetail();
				
				StorageBinDetail.setProductID(jrs.getString("ProductID"));
				StorageBinDetail.setBatchNo(jrs.getString("BatchNo"));
				StorageBinDetail.setQty(jrs.getInt("Qty"));
				StorageBinDetail.setUOM(jrs.getString("UOM"));
				
				listStorageBinDetail.add(StorageBinDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDynamicStorageBinDetail", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDynamicStorageBinDetail", "close opened connection protocol", Globals.user);
			}
		}

		return listStorageBinDetail;
	}
	
	@SuppressWarnings("resource")
	public static model.mdlStorageBin LoadStorageBinByKey(String lPlantID, String lWarehouseID, String lStorageBinID, String lStorageTypeID) {
		model.mdlStorageBin mdlStorageBin = new model.mdlStorageBin();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageBinID, StorageTypeID, TotalCapacity, CapacityUsed FROM wms_storagebin "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageBinID = ? AND StorageTypeID=?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lStorageBinID);
			jrs.setString(4,  lStorageTypeID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlStorageBin.setPlantID(jrs.getString("PlantID"));
				mdlStorageBin.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageBin.setStorageBinID(jrs.getString("StorageBinID"));
				mdlStorageBin.setStorageTypeID(jrs.getString("StorageTypeID"));
				mdlStorageBin.setTotalCapacity(jrs.getDouble("TotalCapacity"));
				mdlStorageBin.setCapacityUsed(jrs.getDouble("CapacityUsed"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageBinByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageBinByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlStorageBin;
	}
	
	@SuppressWarnings("resource")
	public static String InsertStorageBin(model.mdlStorageBin lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlStorageBin Check = LoadStorageBinByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getStorageBinID(), lParam.getStorageTypeID());
			if(Check.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, StorageTypeID, StorageBinID, StorageSectionID, StorageBinTypeID, "
						+ "TotalCapacity, Utilization, CapacityUsed, NoOfQuantity, NoStorageUnits, PutawayBlock, "
						+ "StockRemovalBlock, BlockReasonID "
						+ "FROM wms_storagebin LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("StorageTypeID",lParam.getStorageTypeID());
				jrs.updateString("StorageBinID",lParam.getStorageBinID());
				jrs.updateString("StorageSectionID",lParam.getStorageSectionID());
				jrs.updateString("StorageBinTypeID",lParam.getStorageBinTypeID());
				jrs.updateDouble("TotalCapacity",lParam.getTotalCapacity());
				jrs.updateDouble("CapacityUsed",lParam.getCapacityUsed());
				jrs.updateDouble("Utilization",lParam.getUtilization());
				jrs.updateInt("NoOfQuantity",lParam.getNoOfQuantity());
				jrs.updateInt("NoStorageUnits",lParam.getNoStorageUnits());
				jrs.updateBoolean("PutawayBlock",lParam.getPutawayBlock());
				jrs.updateBoolean("StockRemovalBlock",lParam.getStockRemovalBlock());
				jrs.updateString("BlockReasonID",lParam.getBlockReasonID());

				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Storage Bin";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Storage Bin sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertStorageBin", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertStorageBin", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String UpdateStorageBin(model.mdlStorageBin lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID,WarehouseID,StorageBinID, StorageTypeID, StorageSectionID, StorageBinTypeID, "
						+ "TotalCapacity, Utilization, CapacityUsed, NoOfQuantity, NoStorageUnits, PutawayBlock, "
						+ "StockRemovalBlock, BlockReasonID "
						+ "FROM wms_storagebin WHERE PlantID = ? AND WarehouseID = ? AND StorageBinID = ? AND StorageTypeID=? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getStorageBinID());
			jrs.setString(4,  lParam.getStorageTypeID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("StorageSectionID", lParam.getStorageSectionID());
			jrs.updateString("StorageBinTypeID", lParam.getStorageBinTypeID());
			jrs.updateDouble("TotalCapacity", lParam.getTotalCapacity());
			jrs.updateDouble("CapacityUsed", lParam.getCapacityUsed());
			jrs.updateDouble("Utilization", lParam.getUtilization());
			jrs.updateInt("NoOfQuantity", lParam.getNoOfQuantity());
			jrs.updateInt("NoStorageUnits", lParam.getNoStorageUnits());
			jrs.updateBoolean("PutawayBlock", lParam.getPutawayBlock());
			jrs.updateBoolean("StockRemovalBlock", lParam.getStockRemovalBlock());
			jrs.updateString("BlockReasonID", lParam.getBlockReasonID());
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Storage Bin";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStorageBin", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateStorageBin", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static String DeleteStorageBin(String lPlantID, String lWarehouseID, String lStorageBinID, String lStorageTypeID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageBinID, StorageTypeID "
							+ "FROM wms_storagebin WHERE PlantID = ? AND WarehouseID = ? AND StorageBinID = ? AND StorageTypeID=? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lStorageBinID);
			jrs.setString(4, lStorageTypeID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Storage Bin";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteStorageBin", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Delete Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteStorageBin", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
	
	@SuppressWarnings("resource")
	public static model.mdlStorageBin LoadStorageBinDetailForPlacement(mdlTransferOrder lParam) {
		model.mdlStorageBin mdlStorageBin = new model.mdlStorageBin();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		JdbcRowSet jrs2 = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT COUNT(DestStorageUnit) AS NoStorageUnits FROM wms_storagebin_detail "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageType = ? AND StorageSection=? "
						+ "AND StorageBin=?");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getStorageTypeID());
			jrs.setString(4,  lParam.getStorageSectionID());
			jrs.setString(5,  lParam.getDestinationStorageBin());
			//jrs.setInt(6,  lParam.getDestinationStorageUnit());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			while(jrs.next())
			{
				mdlStorageBin.setNoStorageUnits(jrs.getInt("NoStorageUnits"));
			}
			
			
			jrs2 = database.RowSetAdapter.getJDBCRowSet();
			jrs2.setCommand("SELECT COUNT(DISTINCT(BatchNo)) AS NoOfQuantity "
					+ "FROM wms_storagebin_detail "
					+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageType = ? AND StorageSection=? "
					+ "AND StorageBin=?");
			jrs2.setString(1,  lParam.getPlantID());
			jrs2.setString(2,  lParam.getWarehouseID());
			jrs2.setString(3,  lParam.getStorageTypeID());
			jrs2.setString(4,  lParam.getStorageSectionID());
			jrs2.setString(5,  lParam.getDestinationStorageBin());
			//jrs2.setInt(6,  lParam.getDestinationStorageUnit());
			Globals.gCommand = jrs2.getCommand();
			jrs2.execute();
			while(jrs2.next())
			{
				mdlStorageBin.setNoOfQuantity(jrs2.getInt("NoOfQuantity"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageBinDetailForPlacement", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
				if(jrs2!=null)
					jrs2.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageBinDetailForPlacement", "close opened connection protocol", Globals.user);
			}
		}

		return mdlStorageBin;
	}
	
	public static model.mdlStorageBin LoadStorageBinByKey2(String lPlantID, String lWarehouseID, String lStorageBinID, String lStorageTypeID, String lStorageSectionID) {
		model.mdlStorageBin mdlStorageBin = new model.mdlStorageBin();
		Globals.gCommand = "";
		try{

			JdbcRowSet jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageBinID, StorageBinTypeID, StorageTypeID, TotalCapacity, CapacityUsed "
						+ "FROM wms_storagebin "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageBinID = ? AND StorageTypeID=? AND StorageSectionID=? AND PutawayBlock=0");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lStorageBinID);
			jrs.setString(4,  lStorageTypeID);
			jrs.setString(5,  lStorageSectionID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlStorageBin.setPlantID(jrs.getString("PlantID"));
				mdlStorageBin.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageBin.setStorageBinID(jrs.getString("StorageBinID"));
				mdlStorageBin.setStorageBinTypeID(jrs.getString("StorageBinTypeID"));
				mdlStorageBin.setStorageTypeID(jrs.getString("StorageTypeID"));
				mdlStorageBin.setTotalCapacity(jrs.getDouble("TotalCapacity"));
				mdlStorageBin.setCapacityUsed(jrs.getDouble("CapacityUsed"));
				mdlStorageBin.setCapacityLeft(mdlStorageBin.getTotalCapacity() - mdlStorageBin.getCapacityUsed());
			}
			
			jrs.close();
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageBinByKey2", Globals.gCommand , Globals.user);
		}

		return mdlStorageBin;
	}
	
}
