package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlStorageUnitType;

public class StorageUnitTypeAdapter {

	@SuppressWarnings("resource")
	public static List<model.mdlStorageUnitType> LoadStorageUnitType() {
		List<model.mdlStorageUnitType> listData = new ArrayList<model.mdlStorageUnitType>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT storageunittype.PlantID, plant.PlantName, storageunittype.WarehouseID, warehouse.WarehouseName, "
					+ "storageunittype.StorageUnitTypeID, storageunittype.StorageUnitTypeName, "
					+ "storageunittype.CapacityUsageLET, storageunittype.UOMType, storageunittype.UOMTypeName FROM wms_storageunittype storageunittype "
					+ "INNER JOIN plant ON plant.PlantID = storageunittype.PlantID "
					+ "INNER JOIN warehouse ON warehouse.PlantID = storageunittype.PlantID AND warehouse.WarehouseID = storageunittype.WarehouseID "
					+ "WHERE storageunittype.PlantID IN ("+Globals.user_area+")");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next()){
				model.mdlStorageUnitType Data = new model.mdlStorageUnitType();
				Data.setPlantID(jrs.getString("PlantID"));
				Data.setPlantName(jrs.getString("PlantName"));
				Data.setWarehouseID(jrs.getString("WarehouseID"));
				Data.setWarehouseName(jrs.getString("WarehouseName"));
				Data.setStorageUnitTypeID(jrs.getString("StorageUnitTypeID"));
				Data.setStorageUnitTypeName(jrs.getString("StorageUnitTypeName"));
				Data.setCapacityUsageLET(jrs.getDouble("CapacityUsageLET"));
				Data.setUOMType(jrs.getString("UOMType"));
				Data.setUOMTypeName(jrs.getString("UOMTypeName"));
				listData.add(Data);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageUnitType", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageUnitType", "close opened connection protocol", Globals.user);
			}
		}

		return listData;
	}

	@SuppressWarnings("resource")
	public static model.mdlStorageUnitType LoadStorageUnitTypeByKey(String lPlantID, String lWarehouseID, String lStorageUnitTypeID) {
		model.mdlStorageUnitType mdlStorageUnitType = new model.mdlStorageUnitType();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageUnitTypeID FROM wms_storageunittype "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageUnitTypeID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			jrs.setString(3,  lStorageUnitTypeID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				mdlStorageUnitType.setPlantID(jrs.getString("PlantID"));
				mdlStorageUnitType.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageUnitType.setStorageUnitTypeID(jrs.getString("StorageUnitTypeID"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageUnitTypeByKey", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageUnitTypeByKey", "close opened connection protocol", Globals.user);
			}
		}

		return mdlStorageUnitType;
	}
	
	@SuppressWarnings("resource")
	public static List<model.mdlStorageUnitType> LoadStorageUnitTypeByPlantWarehouse(String lPlantID, String lWarehouseID) {
		List<model.mdlStorageUnitType> listStorageUnitType = new ArrayList<model.mdlStorageUnitType>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{

			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageUnitTypeID, StorageUnitTypeName, CapacityUsageLET FROM wms_storageunittype "
						+ "WHERE PlantID = ? AND WarehouseID = ?");
			jrs.setString(1,  lPlantID);
			jrs.setString(2,  lWarehouseID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			while(jrs.next())
			{
				model.mdlStorageUnitType mdlStorageUnitType = new model.mdlStorageUnitType();
				
				mdlStorageUnitType.setPlantID(jrs.getString("PlantID"));
				mdlStorageUnitType.setWarehouseID(jrs.getString("WarehouseID"));
				mdlStorageUnitType.setStorageUnitTypeID(jrs.getString("StorageUnitTypeID"));
				mdlStorageUnitType.setStorageUnitTypeName(jrs.getString("StorageUnitTypeName"));
				mdlStorageUnitType.setCapacityUsageLET(jrs.getDouble("CapacityUsageLET"));
				
				listStorageUnitType.add(mdlStorageUnitType);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStorageUnitTypeByPlantWarehouse", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadStorageUnitTypeByPlantWarehouse", "close opened connection protocol", Globals.user);
			}
		}

		return listStorageUnitType;
	}

	@SuppressWarnings("resource")
	public static String InsertStorageUnitType(model.mdlStorageUnitType lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			mdlStorageUnitType Check = LoadStorageUnitTypeByKey(lParam.getPlantID(), lParam.getWarehouseID(), lParam.getStorageUnitTypeID());
			if(Check.getPlantID() == null){
				jrs = database.RowSetAdapter.getJDBCRowSet();

				jrs.setCommand("SELECT PlantID, WarehouseID, StorageUnitTypeID, StorageUnitTypeName, "
						+ "CapacityUsageLET, UOMType, UOMTypeName FROM wms_storageunittype LIMIT 1");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();

				jrs.moveToInsertRow();
				jrs.updateString("PlantID",lParam.getPlantID());
				jrs.updateString("WarehouseID",lParam.getWarehouseID());
				jrs.updateString("StorageUnitTypeID",lParam.getStorageUnitTypeID());
				jrs.updateString("StorageUnitTypeName",lParam.getStorageUnitTypeName());
				jrs.updateDouble("CapacityUsageLET",lParam.getCapacityUsageLET());
				jrs.updateString("UOMType",lParam.getUOMType());
				jrs.updateString("UOMTypeName",lParam.getUOMTypeName());

				jrs.insertRow();
				Globals.gReturn_Status = "Success Insert Storage Unit Type";
			}
			//if duplicate
			else{
				Globals.gReturn_Status = "Storage Unit Type sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertStorageUnitType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Insert Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "InsertStorageUnitType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String UpdateStorageUnitType(model.mdlStorageUnitType lParam)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();

			jrs.setCommand("SELECT PlantID, WarehouseID, StorageUnitTypeID, StorageUnitTypeName, CapacityUsageLET, UOMType, UOMTypeName "
						+ "FROM wms_storageunittype "
						+ "WHERE PlantID = ? AND WarehouseID = ? AND StorageUnitTypeID = ? LIMIT 1");
			jrs.setString(1,  lParam.getPlantID());
			jrs.setString(2,  lParam.getWarehouseID());
			jrs.setString(3,  lParam.getStorageUnitTypeID());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			int rowNum = jrs.getRow();

			jrs.absolute(rowNum);
			jrs.updateString("StorageUnitTypeName",lParam.getStorageUnitTypeName());
			jrs.updateDouble("CapacityUsageLET",lParam.getCapacityUsageLET());
			jrs.updateString("UOMType",lParam.getUOMType());
			jrs.updateString("UOMTypeName",lParam.getUOMTypeName());
			jrs.updateRow();

			Globals.gReturn_Status = "Success Update Storage Unit Type";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStorageUnitType", Globals.gCommand , Globals.user);
			//Globals.gReturn_Status = "Error Update Warehouse";
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "UpdateStorageUnitType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}

	@SuppressWarnings("resource")
	public static String DeleteStorageUnitType(String lPlantID, String lWarehouseID, String lStorageUnitTypeID)
	{
		Globals.gCommand = "";
		JdbcRowSet jrs = new JdbcRowSetImpl();
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			jrs.setCommand("SELECT PlantID, WarehouseID, StorageUnitTypeID "
							+ "FROM wms_storageunittype WHERE PlantID = ? AND WarehouseID = ? AND StorageUnitTypeID = ? LIMIT 1");
			jrs.setString(1, lPlantID);
			jrs.setString(2, lWarehouseID);
			jrs.setString(3, lStorageUnitTypeID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();

			jrs.last();
			jrs.deleteRow();

			Globals.gReturn_Status = "Success Delete Storage Unit Type";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteStorageUnitType", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Database Error";
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "DeleteStorageUnitType", "close opened connection protocol", Globals.user);
			}
		}

		return Globals.gReturn_Status;
	}
}
