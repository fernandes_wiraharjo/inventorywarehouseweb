package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

/** Documentation
 * 001 Nanda
 */

public class ProductDetailAdapter {
	
	public static List<model.mdlProductDetail> GetProductDetailAPI(String lUser){
		List<model.mdlProductDetail> listmdlProductDetail = new ArrayList<model.mdlProductDetail>();
		model.mdlToken mdlToken = TokenAdapter.GetToken();
		try {
			
			Client client = Client.create();
			
			Globals.gCommand = "http://dev.webarq.info:8081/kenmaster-be/api/product-detail/listing";
			WebResource webResource = client.resource(Globals.gCommand);
			
			Gson gson = new Gson();
			String json = gson.toJson(mdlToken);
			
			ClientResponse response  = webResource.type("application/json").post(ClientResponse.class,json);		
			
			Globals.gReturn_Status = response.getEntity(String.class);
			
			//Type TypeDepartment = new TypeToken<ArrayList<model.mdlDepartment>>(){}.getClass();
			listmdlProductDetail = gson.fromJson(Globals.gReturn_Status, model.mdlProductDetaillist.class);
			
			ProductDetailAdapter.InsertProductDetaillist(listmdlProductDetail,Globals.user);

		  } 
		catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetProductDetailAPI", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error GetToken";
		  }
		
		return listmdlProductDetail;
	}

	public static String InsertProductDetaillist(List<model.mdlProductDetail> lParamlist, String lUser)
		{
			Connection connection = null;
			PreparedStatement pstm = null;
			Globals.gCommand = "";
			try{
				connection = database.RowSetAdapter.getConnection();
				
				Gson gson = new Gson();
				Globals.gCommand = gson.toJson(lParamlist);
				
				String sql = "INSERT INTO `product_detail`(`ID`, `Code`, `Product_ID`, `Attribute_ID`, `Quantity`, `Sku`, `Status`, `Image`, `Price_Basic`, `Price_Selling`, `Price_Carton`, `Jumlah_Per_Carton`, `Weight`,"
						+ " `Width`, `Height`, `Volume`, `Wide`, `Wide_Carton`, `Weight_Carton`, `Width_Carton`, `Height_Carton`, `Volume_Carton`, `View`, `Barcode`, `Price_Selling_Tax`, `Price_Carton_Tax`, `Created_at`, `Updated_at`) "
						+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "
						+ "ON DUPLICATE KEY UPDATE Quantity=?,Image=?,Jumlah_Per_Carton=?,Updated_at=?";
				pstm = connection.prepareStatement(sql);
				
				for(model.mdlProductDetail lParam : lParamlist)
				{	
					pstm.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getId()));
					pstm.setString(2,  ValidateNull.NulltoStringEmpty(lParam.getCode()));
					pstm.setString(3,  ValidateNull.NulltoStringEmpty(lParam.getProduct_id()));
					pstm.setString(4,  ValidateNull.NulltoStringEmpty(lParam.getAttribute_id()));
					pstm.setString(5,  ValidateNull.NulltoStringEmpty(lParam.getQuantity()));
					pstm.setString(6,  ValidateNull.NulltoStringEmpty(lParam.getSku()));
					pstm.setString(7,  ValidateNull.NulltoStringEmpty(lParam.getStatus()));
					pstm.setString(8,  ValidateNull.NulltoStringEmpty(lParam.getImage()));
					pstm.setString(9,  ValidateNull.NulltoStringEmpty(lParam.getPrice_basic()));
					pstm.setString(10, ValidateNull.NulltoStringEmpty(lParam.getPrice_selling()));
					pstm.setString(11, ValidateNull.NulltoStringEmpty(lParam.getPrice_carton()));					
					pstm.setString(12,  ValidateNull.NulltoStringEmpty(lParam.getJumlah_per_carton()));
					pstm.setString(13,  ValidateNull.NulltoStringEmpty(lParam.getWeight()));
					pstm.setString(14,  ValidateNull.NulltoStringEmpty(lParam.getWidth()));
					pstm.setString(15,  ValidateNull.NulltoStringEmpty(lParam.getHeight()));
					pstm.setString(16,  ValidateNull.NulltoStringEmpty(lParam.getVolume()));
					pstm.setString(17,  ValidateNull.NulltoStringEmpty(lParam.getWide()));
					pstm.setString(18,  ValidateNull.NulltoStringEmpty(lParam.getWide_carton()));
					pstm.setString(19,  ValidateNull.NulltoStringEmpty(lParam.getWeight_carton()));
					pstm.setString(20, ValidateNull.NulltoStringEmpty(lParam.getWidth_carton()));
					pstm.setString(21, ValidateNull.NulltoStringEmpty(lParam.getHeight_carton()));					
					pstm.setString(22,  ValidateNull.NulltoStringEmpty(lParam.getVolume_carton()));
					pstm.setString(23,  ValidateNull.NulltoStringEmpty(lParam.getView()));
					pstm.setString(24,  ValidateNull.NulltoStringEmpty(lParam.getBarcode()));
					pstm.setString(25,  ValidateNull.NulltoStringEmpty(lParam.getPrice_selling_tax()));
					pstm.setString(26,  ValidateNull.NulltoStringEmpty(lParam.getPrice_carton_tax()));
					pstm.setString(27,  ValidateNull.NulltoDateTimeEmpty(lParam.getCreated_at()));
					pstm.setString(28,  ValidateNull.NulltoDateTimeEmpty(lParam.getUpdated_at()));
					pstm.setString(29,  ValidateNull.NulltoDateTimeEmpty(lParam.getQuantity()));
					pstm.setString(30,  ValidateNull.NulltoDateTimeEmpty(lParam.getImage()));
					pstm.setString(31,  ValidateNull.NulltoDateTimeEmpty(lParam.getJumlah_per_carton()));
					pstm.setString(32,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
					Globals.gCommand = pstm.toString();
					//pstm.executeUpdate();
					pstm.addBatch();
				}
				pstm.executeBatch();
				
				//<<001 Nanda
				List<model.mdlUom> mdlUomList = new ArrayList<model.mdlUom>();
				List<model.mdlProductUom> mdlProductUomList = new ArrayList<model.mdlProductUom>();
				List<model.mdlProductDetail> mdlProductDetail = GetProductID();
				for(model.mdlProductDetail lParam : mdlProductDetail)
				{
					//Generate and Insert Uom , Generate and Insert Product Uom
					model.mdlUom mdlUom = new model.mdlUom();
					model.mdlProductUom mdlProductUom = new model.mdlProductUom();
					
					String ProductBaseUOM = "";
					ProductBaseUOM = ProductAdapter.LoadProductBaseUOM(lParam.getProduct_id());
					
					if(!lParam.getJumlah_per_carton().contentEquals("0"))
					{
						if(lParam.getJumlah_per_carton().contentEquals("1"))
						{
							mdlUom.setCode("Pcs");
							mdlUom.setDescription("Pieces");
							mdlUom.setIs_BaseUOM(true);
						}
						else
						{
							mdlUom.setCode("Carton"+lParam.getJumlah_per_carton());
							mdlUom.setDescription("1 Carton isi "+lParam.getJumlah_per_carton());
							mdlUom.setIs_BaseUOM(false);
							
							mdlProductUom.setProductID(lParam.getProduct_id());
							mdlProductUom.setUOM("Carton"+lParam.getJumlah_per_carton());
							mdlProductUom.setBaseUOM(ProductBaseUOM);
							mdlProductUom.setQty(lParam.getJumlah_per_carton());
							mdlProductUomList.add(mdlProductUom);
						}
						mdlUomList.add(mdlUom);
						
						if(!ProductBaseUOM.contentEquals(""))
						{
							if (lParam.getProduct_id() != null){
								mdlProductUom = new model.mdlProductUom()	;
								mdlProductUom.setProductID(lParam.getProduct_id());
								mdlProductUom.setUOM(ProductBaseUOM);
								mdlProductUom.setBaseUOM(ProductBaseUOM);
								mdlProductUom.setQty("1");
								mdlProductUomList.add(mdlProductUom);
							}
						}
					}
				}
				UomAdapter.InsertUomlist(mdlUomList);
				
				ProductUomAdapter.InsertProductUomlist(mdlProductUomList);
				//>>
				
				Globals.gReturn_Status = "Success Insert list Product Detail API";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertProductDetaillist", "InsertProductDetailAPIlist" , lUser);
				Globals.gReturn_Status = "Error Insert list Product Detail API";
			}
			finally {
				try{
					 if (pstm != null) {
						 pstm.close();
					 }
					 if (connection != null) {
						 connection.close();
					 }
				}
				catch(Exception e){
					LogAdapter.InsertLogExc(e.toString(), "InsertProductDetaillist", "close opened connection protocol", lUser);
				}
			 }
			
			return Globals.gReturn_Status;
		}
		
	public static List<model.mdlProductDetail> LoadProductDetail() {
			List<model.mdlProductDetail> listmdlProductDetail = new ArrayList<model.mdlProductDetail>();
			try{
				
				JdbcRowSet jrs = new JdbcRowSetImpl();
				jrs = database.RowSetAdapter.getJDBCRowSet();
				
				jrs.setCommand("SELECT `ID`, `Code`, `Product_ID`, `Attribute_ID`, `Quantity`, `Sku`, `Status`, `Image`, `Price_Basic`, `Price_Selling`, `Price_Carton`, `Jumlah_Per_Carton`, `Weight`,"
						+ " `Width`, `Height`, `Volume`, `Wide`, `Wide_Carton`, `Weight_Carton`, `Width_Carton`, `Height_Carton`, `Volume_Carton`, `View`, `Barcode`, `Price_Selling_Tax`, `Price_Carton_Tax` FROM `product_detail`");
				Globals.gCommand = jrs.getCommand();
				jrs.execute();
										
				while(jrs.next()){
					model.mdlProductDetail mdlProductDetail = new model.mdlProductDetail();
					mdlProductDetail.setId(jrs.getString("ID"));
					mdlProductDetail.setCode(jrs.getString("Code"));
					mdlProductDetail.setProduct_id(jrs.getString("Product_ID"));
					mdlProductDetail.setAttribute_id(jrs.getString("Attribute_ID"));
					mdlProductDetail.setQuantity(jrs.getString("Quantity"));
					mdlProductDetail.setSku(jrs.getString("Sku"));	
					mdlProductDetail.setStatus(jrs.getString("Status"));
					mdlProductDetail.setImage(jrs.getString("Image"));
					mdlProductDetail.setPrice_basic(jrs.getString("Price_Basic"));
					mdlProductDetail.setPrice_selling(jrs.getString("Price_Selling"));			
					mdlProductDetail.setPrice_carton(jrs.getString("Price_Carton"));
					mdlProductDetail.setJumlah_per_carton(jrs.getString("Jumlah_Per_Carton"));
					mdlProductDetail.setWeight(jrs.getString("Weight"));
					mdlProductDetail.setWidth(jrs.getString("Width"));
					mdlProductDetail.setHeight(jrs.getString("Height"));
					mdlProductDetail.setVolume(jrs.getString("Volume"));	
					mdlProductDetail.setWide(jrs.getString("Wide"));
					mdlProductDetail.setWide_carton(jrs.getString("Wide_Carton"));
					mdlProductDetail.setWeight_carton(jrs.getString("Weight_Carton"));
					mdlProductDetail.setWidth_carton(jrs.getString("Width_Carton"));
					mdlProductDetail.setHeight_carton(jrs.getString("Height_Carton"));
					mdlProductDetail.setVolume_carton(jrs.getString("Volume_Carton"));
					mdlProductDetail.setView(jrs.getString("View"));
					mdlProductDetail.setBarcode(jrs.getString("Barcode"));
					mdlProductDetail.setPrice_selling_tax(jrs.getString("Price_Selling_Tax"));
					mdlProductDetail.setPrice_carton_tax(jrs.getString("Price_Carton_Tax"));
					//mdlProductDetail.setCreated_at(jrs.getString("Created_at"));
					//mdlProductDetail.setUpdated_at(jrs.getString("Updated_at"));
					
					
					listmdlProductDetail.add(mdlProductDetail);
				}
				jrs.close();		
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadProductDetail", Globals.gCommand , "");
			}

			return listmdlProductDetail;
		}
	
	//<<001 Nanda
	@SuppressWarnings("resource")
	public static List<model.mdlProductDetail> GetProductID() {
		List<model.mdlProductDetail> listmdlProductDetail = new ArrayList<model.mdlProductDetail>();
		JdbcRowSet jrs = new JdbcRowSetImpl();
		Globals.gCommand = "";
		try{
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT DISTINCT product.ID ,  product_detail.Jumlah_Per_Carton "
							+ "FROM  product_detail,product "
							+ "WHERE SUBSTRING_INDEX(product.ID,'-',1) = product_detail.Product_ID");
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				model.mdlProductDetail mdlProductDetail = new model.mdlProductDetail();
				mdlProductDetail.setProduct_id(jrs.getString("ID"));
				mdlProductDetail.setJumlah_per_carton(jrs.getString("Jumlah_Per_Carton"));								
				listmdlProductDetail.add(mdlProductDetail);
			}		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetProductID", Globals.gCommand , Globals.user);
		}
		finally{
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "GetProductID", "close opened connection protocol", Globals.user);
			}
		}

		return listmdlProductDetail;
	}
	//>>
	
}
