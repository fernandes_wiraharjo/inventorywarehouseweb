package model;

public class mdlStorageUnitType {
	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String StorageUnitTypeID;
	public String StorageUnitTypeName;
	public Double CapacityUsageLET;
	public String UOMType;
	public String UOMTypeName;

	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getStorageUnitTypeID() {
		return StorageUnitTypeID;
	}
	public void setStorageUnitTypeID(String storageUnitTypeID) {
		StorageUnitTypeID = storageUnitTypeID;
	}
	public String getStorageUnitTypeName() {
		return StorageUnitTypeName;
	}
	public void setStorageUnitTypeName(String storageUnitTypeName) {
		StorageUnitTypeName = storageUnitTypeName;
	}
	public Double getCapacityUsageLET() {
		return CapacityUsageLET;
	}
	public void setCapacityUsageLET(Double capacityUsageLET) {
		CapacityUsageLET = capacityUsageLET;
	}
	public String getUOMType() {
		return UOMType;
	}
	public void setUOMType(String uOMType) {
		UOMType = uOMType;
	}
	public String getUOMTypeName() {
		return UOMTypeName;
	}
	public void setUOMTypeName(String uOMTypeName) {
		UOMTypeName = uOMTypeName;
	}
	
}
