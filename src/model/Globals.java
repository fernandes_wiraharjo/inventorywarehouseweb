package model;

import java.util.ArrayList;
import java.util.List;

public class Globals {
	public static String gReturn_Status = "";
	public static String gParam1 = "";
	public static String gtemp_Param1 = "";
	public static String gtemp_Param2 = "";
	public static String gtemp_Param3 = "";
	public static String gtemp_Param4 = "";
	public static String gtemp_Param5 = "";
	public static String gtemp_Param6 = "";
	public static String gtemp_Param7 = "";
	public static String gtemp_Param8 = "";
	public static String gtemp_Param9 = "";
	public static String gtemp_Param10 = "";
	public static String gCommand = "";
	public static String gCondition = "";
	public static String gConditionDesc = "";
	public static String user = "";
	public static String gVendorId = "";
	public static String gRole = "";
	public static String gWarehouseId = "";
	public static String user_area = "";
	public static String gPlantID = "";
	public static String gPlantName = "";
	public static String gtemp_Inbound_WMStatus = "";
	public static String gtemp_Inbound_UpdateWMFirst = "";
	public static List<model.mdlStorageBin> gUsedStorBin = new ArrayList<model.mdlStorageBin>();
	public static List<model.mdlStorageBin> gBackupUsedStorBin = new ArrayList<model.mdlStorageBin>();
}
