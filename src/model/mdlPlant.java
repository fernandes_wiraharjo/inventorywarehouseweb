package model;

public class mdlPlant {

	public String PlantID;
	public String PlantNm;
	public String Desc;
	
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getPlantNm() {
		return PlantNm;
	}
	public void setPlantNm(String plantNm) {
		PlantNm = plantNm;
	}
	public String getDesc() {
		return Desc;
	}
	public void setDesc(String desc) {
		Desc = desc;
	}
	
}
