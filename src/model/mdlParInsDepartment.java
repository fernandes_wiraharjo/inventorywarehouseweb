package model;

public class mdlParInsDepartment {
	public String token;
	public String title_en;
	public String title_id;
	public String order;
	public String status;
	public String icon;
	public String banner;
	
	public String getToken() {
		return token;
	}
	public void setToken(String ltoken) {
		this.token = ltoken;
	}
	
	public String getTitle_en() {
		return title_en;
	}
	public void setTitle_en(String ltitle_en) {
		this.title_en = ltitle_en;
	}
	
	public String getTitle_id() {
		return title_id;
	}
	public void setTitle_id(String ltitle_id) {
		this.title_id = ltitle_id;
	}
	
	public String getIcon() {
		return icon;
	}
	public void setIcon(String licon) {
		this.icon = licon;
	}

	public String getBanner() {
		return banner;
	}
	public void setBanner(String lbanner) {
		this.banner = lbanner;
	}
	
	public String getOrder() {
		return order;
	}
	public void setOrder(String lorder) {
		this.order = lorder;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String lstatus) {
		this.status = lstatus;
	}

}
