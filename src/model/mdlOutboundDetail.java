package model;

public class mdlOutboundDetail {
	public String DocNumber;
	public String DocLine;
	public String ProductID;
	public String Qty_UOM;
	public String UOM;
	public String Qty_BaseUOM;
	public String BaseUOM;
	public String Batch_No;
	public String Packing_No;
	public String getDocNumber() {
		return DocNumber;
	}
	public void setDocNumber(String docNumber) {
		DocNumber = docNumber;
	}
	public String getDocLine() {
		return DocLine;
	}
	public void setDocLine(String docLine) {
		DocLine = docLine;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getQty_UOM() {
		return Qty_UOM;
	}
	public void setQty_UOM(String qty_UOM) {
		Qty_UOM = qty_UOM;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getQty_BaseUOM() {
		return Qty_BaseUOM;
	}
	public void setQty_BaseUOM(String qty_BaseUOM) {
		Qty_BaseUOM = qty_BaseUOM;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	public String getBatch_No() {
		return Batch_No;
	}
	public void setBatch_No(String batch_No) {
		Batch_No = batch_No;
	}
	public String getPacking_No() {
		return Packing_No;
	}
	public void setPacking_No(String packing_No) {
		Packing_No = packing_No;
	}
	
	

}
