package model;

import com.sun.org.apache.xpath.internal.operations.Bool;

/** Documentation
 * 
 */
public class mdlProduct {
	public String token;
	public String id;
	public String category_id;
	public String brand_id;
	public String title_en;
	public String short_description_en;
	public String description_en;
	public String spesification_en;
	public String detail_en;
	public String title_id;
	public String short_description_id;
	public String description_id;	
	public String spesification_id;
	public String detail_id;
	public String image;
	public String minimum_order;
	public String is_best_seller;
	public String is_featured_product;
	public String is_only_jabdetabek;	
	public String status;
	public String slug;
	public String created_at;
	public String updated_at;
	public String can_be_sold_online;
	public String code;
	public String BaseUOM;
	public Boolean is_bom;
	public String department_id;
	public String group_id;
	
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategory_id() {
		return category_id;
	}
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	public String getBrand_id() {
		return brand_id;
	}
	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}
	public String getTitle_en() {
		return title_en;
	}
	public void setTitle_en(String title_en) {
		this.title_en = title_en;
	}
	public String getShort_description_en() {
		return short_description_en;
	}
	public void setShort_description_en(String short_description_en) {
		this.short_description_en = short_description_en;
	}
	public String getDescription_en() {
		return description_en;
	}
	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}
	public String getSpesification_en() {
		return spesification_en;
	}
	public void setSpesification_en(String spesification_en) {
		this.spesification_en = spesification_en;
	}
	public String getDetail_en() {
		return detail_en;
	}
	public void setDetail_en(String detail_en) {
		this.detail_en = detail_en;
	}
	public String getTitle_id() {
		return title_id;
	}
	public void setTitle_id(String title_id) {
		this.title_id = title_id;
	}
	public String getShort_description_id() {
		return short_description_id;
	}
	public void setShort_description_id(String short_description_id) {
		this.short_description_id = short_description_id;
	}
	public String getDescription_id() {
		return description_id;
	}
	public void setDescription_id(String description_id) {
		this.description_id = description_id;
	}
	public String getSpesification_id() {
		return spesification_id;
	}
	public void setSpesification_id(String spesification_id) {
		this.spesification_id = spesification_id;
	}
	public String getDetail_id() {
		return detail_id;
	}
	public void setDetail_id(String detail_id) {
		this.detail_id = detail_id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getMinimum_order() {
		return minimum_order;
	}
	public void setMinimum_order(String minimum_order) {
		this.minimum_order = minimum_order;
	}
	public String getIs_best_seller() {
		return is_best_seller;
	}
	public void setIs_best_seller(String is_best_seller) {
		this.is_best_seller = is_best_seller;
	}
	public String getIs_featured_product() {
		return is_featured_product;
	}
	public void setIs_featured_product(String is_featured_product) {
		this.is_featured_product = is_featured_product;
	}
	public String getIs_only_jabdetabek() {
		return is_only_jabdetabek;
	}
	public void setIs_only_jabdetabek(String is_only_jabdetabek) {
		this.is_only_jabdetabek = is_only_jabdetabek;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getCan_be_sold_online() {
		return can_be_sold_online;
	}
	public void setCan_be_sold_online(String can_be_sold_online) {
		this.can_be_sold_online = can_be_sold_online;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	public Boolean getIs_bom() {
		return is_bom;
	}
	public void setIs_bom(Boolean is_bom) {
		this.is_bom = is_bom;
	}
	public String getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(String department_id) {
		this.department_id = department_id;
	}
	public String getGroup_id() {
		return group_id;
	}
	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}
	
	
}
