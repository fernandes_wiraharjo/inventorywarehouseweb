package model;

public class mdlProductWM {

	public String ProductID;
	public String ProductName;
	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String BaseUOM;
	public Double CapacityUsage;
	public String CapacityUsageUOM;
	public Boolean ApprBatchRecReq;
	public String StockRemoval;
	public String StockPlacement;
	public String StorageSectionIndicator;
	public String StorageBin;
	public Double LEQty1;
	public Double LEQty2;
	public Double LEQty3;
	public String UOMLEQty1;
	public String UOMLEQty2;
	public String UOMLEQty3;
	public String SUTLEQty1;
	public String SUTLEQty2;
	public String SUTLEQty3;
	public String StorageUnitType;
	public Integer QtyPerStorageUnit;
	public String BatchNo;
	
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	public Double getCapacityUsage() {
		return CapacityUsage;
	}
	public void setCapacityUsage(Double capacityUsage) {
		CapacityUsage = capacityUsage;
	}
	public String getCapacityUsageUOM() {
		return CapacityUsageUOM;
	}
	public void setCapacityUsageUOM(String capacityUsageUOM) {
		CapacityUsageUOM = capacityUsageUOM;
	}
	public Boolean getApprBatchRecReq() {
		return ApprBatchRecReq;
	}
	public void setApprBatchRecReq(Boolean apprBatchRecReq) {
		ApprBatchRecReq = apprBatchRecReq;
	}
	public String getStockRemoval() {
		return StockRemoval;
	}
	public void setStockRemoval(String stockRemoval) {
		StockRemoval = stockRemoval;
	}
	public String getStockPlacement() {
		return StockPlacement;
	}
	public void setStockPlacement(String stockPlacement) {
		StockPlacement = stockPlacement;
	}
	public String getStorageSectionIndicator() {
		return StorageSectionIndicator;
	}
	public void setStorageSectionIndicator(String storageSectionIndicator) {
		StorageSectionIndicator = storageSectionIndicator;
	}
	public String getStorageBin() {
		return StorageBin;
	}
	public void setStorageBin(String storageBin) {
		StorageBin = storageBin;
	}
	public Double getLEQty1() {
		return LEQty1;
	}
	public void setLEQty1(Double lEQty1) {
		LEQty1 = lEQty1;
	}
	public Double getLEQty2() {
		return LEQty2;
	}
	public void setLEQty2(Double lEQty2) {
		LEQty2 = lEQty2;
	}
	public Double getLEQty3() {
		return LEQty3;
	}
	public void setLEQty3(Double lEQty3) {
		LEQty3 = lEQty3;
	}
	public String getUOMLEQty1() {
		return UOMLEQty1;
	}
	public void setUOMLEQty1(String uOMLEQty1) {
		UOMLEQty1 = uOMLEQty1;
	}
	public String getUOMLEQty2() {
		return UOMLEQty2;
	}
	public void setUOMLEQty2(String uOMLEQty2) {
		UOMLEQty2 = uOMLEQty2;
	}
	public String getUOMLEQty3() {
		return UOMLEQty3;
	}
	public void setUOMLEQty3(String uOMLEQty3) {
		UOMLEQty3 = uOMLEQty3;
	}
	public String getSUTLEQty1() {
		return SUTLEQty1;
	}
	public void setSUTLEQty1(String sUTLEQty1) {
		SUTLEQty1 = sUTLEQty1;
	}
	public String getSUTLEQty2() {
		return SUTLEQty2;
	}
	public void setSUTLEQty2(String sUTLEQty2) {
		SUTLEQty2 = sUTLEQty2;
	}
	public String getSUTLEQty3() {
		return SUTLEQty3;
	}
	public void setSUTLEQty3(String sUTLEQty3) {
		SUTLEQty3 = sUTLEQty3;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getStorageUnitType() {
		return StorageUnitType;
	}
	public void setStorageUnitType(String storageUnitType) {
		StorageUnitType = storageUnitType;
	}
	public Integer getQtyPerStorageUnit() {
		return QtyPerStorageUnit;
	}
	public void setQtyPerStorageUnit(Integer qtyPerStorageUnit) {
		QtyPerStorageUnit = qtyPerStorageUnit;
	}
	public String getBatchNo() {
		return BatchNo;
	}
	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}
	
	
	
}
