package model;

/** Documentation
 * 
 */
public class mdlDepartment {
	public String id;
	public String code;
	public String title_en;
	public String title_id;
	public String slug;
	public String icon;
	public String banner;
	public String order;
	public String status;
	public String created_at;
	public String updated_at;
	
	public String getId() {
		return id;
	}
	public void setId(String lid) {
		this.id = lid;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String lcode) {
		this.code = lcode;
	}
	
	public String getTitle_en() {
		return title_en;
	}
	public void setTitle_en(String ltitle_en) {
		this.title_en = ltitle_en;
	}
	
	public String getTitle_id() {
		return title_id;
	}
	public void setTitle_id(String ltitle_id) {
		this.title_id = ltitle_id;
	}
	
	public String getSlug() {
		return slug;
	}
	public void setSlug(String lslug) {
		this.slug = lslug;
	}
	
	public String getIcon() {
		return icon;
	}
	public void setIcon(String licon) {
		this.icon = licon;
	}

	public String getBanner() {
		return banner;
	}
	public void setBanner(String lbanner) {
		this.banner = lbanner;
	}
	
	public String getOrder() {
		return order;
	}
	public void setOrder(String lorder) {
		this.order = lorder;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String lstatus) {
		this.status = lstatus;
	}
	
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String lcreated_at) {
		this.created_at = lcreated_at;
	}
	
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String lupdated_at) {
		this.updated_at = lupdated_at;
	}
}

