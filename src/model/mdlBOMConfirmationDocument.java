package model;

public class mdlBOMConfirmationDocument {

	public String ConfirmationID;
	public String RangeFrom;
	public String RangeTo;
	public String ConfirmationDesc;
	public String ConfirmationType;
	
	public String getConfirmationID() {
		return ConfirmationID;
	}
	public void setConfirmationID(String confirmationID) {
		ConfirmationID = confirmationID;
	}
	public String getRangeFrom() {
		return RangeFrom;
	}
	public void setRangeFrom(String rangeFrom) {
		RangeFrom = rangeFrom;
	}
	public String getRangeTo() {
		return RangeTo;
	}
	public void setRangeTo(String rangeTo) {
		RangeTo = rangeTo;
	}
	public String getConfirmationDesc() {
		return ConfirmationDesc;
	}
	public void setConfirmationDesc(String confirmationDesc) {
		ConfirmationDesc = confirmationDesc;
	}
	public String getConfirmationType() {
		return ConfirmationType;
	}
	public void setConfirmationType(String confirmationType) {
		ConfirmationType = confirmationType;
	}
	
	
}
