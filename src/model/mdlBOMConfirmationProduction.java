package model;

public class mdlBOMConfirmationProduction {

	public String ConfirmationID;
	public String ConfirmationNo;
	public String ConfirmationDate;
	public String OrderTypeID;
	public String OrderNo;
	public String ProductID;
	public String PackingNo;
	public String ExpiredDate;
	public String ProductName;
	public String BatchNo;
	public String TargetQty;
	public String Qty;
	public String UOM;
	public String QtyBaseUOM;
	public String BaseUOM;
	
	public String getConfirmationID() {
		return ConfirmationID;
	}
	public void setConfirmationID(String confirmationID) {
		ConfirmationID = confirmationID;
	}
	public String getConfirmationNo() {
		return ConfirmationNo;
	}
	public void setConfirmationNo(String confirmationNo) {
		ConfirmationNo = confirmationNo;
	}
	public String getConfirmationDate() {
		return ConfirmationDate;
	}
	public void setConfirmationDate(String confirmationDate) {
		ConfirmationDate = confirmationDate;
	}
	public String getOrderTypeID() {
		return OrderTypeID;
	}
	public void setOrderTypeID(String orderTypeID) {
		OrderTypeID = orderTypeID;
	}
	public String getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(String orderNo) {
		OrderNo = orderNo;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getBatchNo() {
		return BatchNo;
	}
	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getQtyBaseUOM() {
		return QtyBaseUOM;
	}
	public void setQtyBaseUOM(String qtyBaseUOM) {
		QtyBaseUOM = qtyBaseUOM;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	public String getPackingNo() {
		return PackingNo;
	}
	public void setPackingNo(String packingNo) {
		PackingNo = packingNo;
	}
	public String getExpiredDate() {
		return ExpiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		ExpiredDate = expiredDate;
	}
	public String getTargetQty() {
		return TargetQty;
	}
	public void setTargetQty(String targetQty) {
		TargetQty = targetQty;
	}
	
	
	
}
