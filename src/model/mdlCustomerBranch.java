package model;

public class mdlCustomerBranch {
	public String CustBranchID;
	public String CustBranchName;
	public String CustBranchAddress;
	public String Phone;
	public String Email;
	public String Pic;
	public String CustomerID;
	public String CustomerName;
	
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getCustBranchID() {
		return CustBranchID;
	}
	public void setCustBranchID(String custBranchID) {
		CustBranchID = custBranchID;
	}
	
	public String getCustBranchName() {
		return CustBranchName;
	}
	public void setCustBranchName(String custBranchName) {
		CustBranchName = custBranchName;
	}
	
	public String getCustBranchAddress() {
		return CustBranchAddress;
	}
	public void setCustBranchAddress(String custBranchAddress) {
		CustBranchAddress = custBranchAddress;
	}
	
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}

	public String getPic() {
		return Pic;
	}
	public void setPic(String pic) {
		Pic = pic;
	}
	
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}

}
