package model;

public class mdlWMNumberRanges {

	public String PlantID;
	public String WarehouseID;
	public String FromNumber;
	public String ToNumber;
	public String NumberRangesType;
	
	public String getFromNumber() {
		return FromNumber;
	}
	public void setFromNumber(String fromNumber) {
		FromNumber = fromNumber;
	}
	public String getToNumber() {
		return ToNumber;
	}
	public void setToNumber(String toNumber) {
		ToNumber = toNumber;
	}
	public String getNumberRangesType() {
		return NumberRangesType;
	}
	public void setNumberRangesType(String numberRangesType) {
		NumberRangesType = numberRangesType;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	
	
}
