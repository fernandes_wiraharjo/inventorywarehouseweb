package model;

public class mdlBOMProductionOrderDetail {

	public String OrderTypeID;
	public String OrderNo;
	public String ComponentLine;
	public String ComponentID;
	public String Qty;
	public String UOM;
	public String QtyBaseUOM;
	public String BaseUOM;
	public String QtyConfirmation;
	
	public String getOrderTypeID() {
		return OrderTypeID;
	}
	public void setOrderTypeID(String orderTypeID) {
		OrderTypeID = orderTypeID;
	}
	public String getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(String orderNo) {
		OrderNo = orderNo;
	}
	public String getComponentLine() {
		return ComponentLine;
	}
	public void setComponentLine(String componentLine) {
		ComponentLine = componentLine;
	}
	public String getComponentID() {
		return ComponentID;
	}
	public void setComponentID(String componentID) {
		ComponentID = componentID;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getQtyBaseUOM() {
		return QtyBaseUOM;
	}
	public void setQtyBaseUOM(String qtyBaseUOM) {
		QtyBaseUOM = qtyBaseUOM;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	public String getQtyConfirmation() {
		return QtyConfirmation;
	}
	public void setQtyConfirmation(String qtyConfirmation) {
		QtyConfirmation = qtyConfirmation;
	}
	
}
