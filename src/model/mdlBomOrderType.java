package model;

public class mdlBomOrderType {

	public String OrderTypeID;
	public String From;
	public String To;
	public String OrderTypeDesc;
	
	public String getOrderTypeID() {
		return OrderTypeID;
	}
	public void setOrderTypeID(String orderTypeID) {
		OrderTypeID = orderTypeID;
	}
	public String getFrom() {
		return From;
	}
	public void setFrom(String from) {
		From = from;
	}
	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}
	public String getOrderTypeDesc() {
		return OrderTypeDesc;
	}
	public void setOrderTypeDesc(String orderTypeDesc) {
		OrderTypeDesc = orderTypeDesc;
	}
	

}
