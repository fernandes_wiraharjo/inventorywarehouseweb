package model;

public class mdlStockTransfer {
	public String TransferID;
	public String PlantID;
	public String From;
	public String To;
	public String Date;
	
	public String getTransferID() {
		return TransferID;
	}
	public void setTransferID(String transferID) {
		TransferID = transferID;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getFrom() {
		return From;
	}
	public void setFrom(String from) {
		From = from;
	}
	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
}
