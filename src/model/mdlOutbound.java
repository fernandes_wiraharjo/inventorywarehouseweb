package model;

public class mdlOutbound {
	public String DocNumber;
	public String Date;
	public String CustomerID;
	public String PlantID;
	public String WarehouseID;
	
	public String getDocNumber() {
		return DocNumber;
	}
	public void setDocNumber(String docNumber) {
		DocNumber = docNumber;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	
	

}
