package model;

public class mdlStorageSection {
	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String StorageTypeID;
	public String StorageTypeName;
	public String StorageSectionID;
	public String StorageSectionName;

	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getStorageTypeID() {
		return StorageTypeID;
	}
	public void setStorageTypeID(String storageTypeID) {
		StorageTypeID = storageTypeID;
	}
	public String getStorageTypeName() {
		return StorageTypeName;
	}
	public void setStorageTypeName(String storageTypeName) {
		StorageTypeName = storageTypeName;
	}
	public String getStorageSectionID() {
		return StorageSectionID;
	}
	public void setStorageSectionID(String storageSectionID) {
		StorageSectionID = storageSectionID;
	}
	public String getStorageSectionName() {
		return StorageSectionName;
	}
	public void setStorageSectionName(String storageSectionName) {
		StorageSectionName = storageSectionName;
	}
}
