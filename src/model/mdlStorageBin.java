package model;

public class mdlStorageBin {
	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String StorageTypeID;
	public String StorageTypeName;
	public String StorageBinID;
	public String StorageSectionID;
	public String StorageSectionName;
	public String StorageBinTypeID;
	public String StorageBinTypeName;
	public Double TotalCapacity;
	public Double Utilization;
	public Double CapacityUsed;
	public Integer NoOfQuantity;
	public Integer NoStorageUnits;
	public Boolean PutawayBlock;
	public Boolean StockRemovalBlock;
	public String BlockReasonID;
	public Double CapacityLeft;
	
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getStorageTypeID() {
		return StorageTypeID;
	}
	public void setStorageTypeID(String storageTypeID) {
		StorageTypeID = storageTypeID;
	}
	public String getStorageTypeName() {
		return StorageTypeName;
	}
	public void setStorageTypeName(String storageTypeName) {
		StorageTypeName = storageTypeName;
	}
	public String getStorageBinID() {
		return StorageBinID;
	}
	public void setStorageBinID(String storageBinID) {
		StorageBinID = storageBinID;
	}
	public String getStorageSectionID() {
		return StorageSectionID;
	}
	public void setStorageSectionID(String storageSectionID) {
		StorageSectionID = storageSectionID;
	}
	public String getStorageSectionName() {
		return StorageSectionName;
	}
	public void setStorageSectionName(String storageSectionName) {
		StorageSectionName = storageSectionName;
	}
	public String getStorageBinTypeID() {
		return StorageBinTypeID;
	}
	public void setStorageBinTypeID(String storageBinTypeID) {
		StorageBinTypeID = storageBinTypeID;
	}
	public String getStorageBinTypeName() {
		return StorageBinTypeName;
	}
	public void setStorageBinTypeName(String storageBinTypeName) {
		StorageBinTypeName = storageBinTypeName;
	}
	public Double getTotalCapacity() {
		return TotalCapacity;
	}
	public void setTotalCapacity(Double totalCapacity) {
		TotalCapacity = totalCapacity;
	}
	public Double getUtilization() {
		return Utilization;
	}
	public void setUtilization(Double utilization) {
		Utilization = utilization;
	}
	public Double getCapacityUsed() {
		return CapacityUsed;
	}
	public void setCapacityUsed(Double capacityUsed) {
		CapacityUsed = capacityUsed;
	}
	public Integer getNoOfQuantity() {
		return NoOfQuantity;
	}
	public void setNoOfQuantity(Integer noOfQuantity) {
		NoOfQuantity = noOfQuantity;
	}
	public Integer getNoStorageUnits() {
		return NoStorageUnits;
	}
	public void setNoStorageUnits(Integer noStorageUnits) {
		NoStorageUnits = noStorageUnits;
	}
	public Boolean getPutawayBlock() {
		return PutawayBlock;
	}
	public void setPutawayBlock(Boolean putawayBlock) {
		PutawayBlock = putawayBlock;
	}
	public Boolean getStockRemovalBlock() {
		return StockRemovalBlock;
	}
	public void setStockRemovalBlock(Boolean stockRemovalBlock) {
		StockRemovalBlock = stockRemovalBlock;
	}
	public String getBlockReasonID() {
		return BlockReasonID;
	}
	public void setBlockReasonID(String blockReasonID) {
		BlockReasonID = blockReasonID;
	}
	public Double getCapacityLeft() {
		return CapacityLeft;
	}
	public void setCapacityLeft(Double capacityLeft) {
		CapacityLeft = capacityLeft;
	}
	
	
	
}
