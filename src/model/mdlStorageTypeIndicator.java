package model;

public class mdlStorageTypeIndicator {
	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String StorageTypeIndicatorID;
	public String StorageTypeIndicatorName;

	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getStorageTypeIndicatorID() {
		return StorageTypeIndicatorID;
	}
	public void setStorageTypeIndicatorID(String storageTypeIndicatorID) {
		StorageTypeIndicatorID = storageTypeIndicatorID;
	}
	public String getStorageTypeIndicatorName() {
		return StorageTypeIndicatorName;
	}
	public void setStorageTypeIndicatorName(String storageTypeIndicatorName) {
		StorageTypeIndicatorName = storageTypeIndicatorName;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
}
