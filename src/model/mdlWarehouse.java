package model;

import com.sun.org.apache.xpath.internal.operations.Bool;

public class mdlWarehouse {

	public String PlantID;
	public String WarehouseID;
	public String WarehouseName;
	public String WarehouseDesc;
	public Boolean WMS_Status;
	public Boolean UpdateWMFirst;
	public String PlantName;
	
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getWarehouseDesc() {
		return WarehouseDesc;
	}
	public void setWarehouseDesc(String warehouseDesc) {
		WarehouseDesc = warehouseDesc;
	}
	public Boolean getWMS_Status() {
		return WMS_Status;
	}
	public void setWMS_Status(Boolean wMS_Status) {
		WMS_Status = wMS_Status;
	}
	public Boolean getUpdateWMFirst() {
		return UpdateWMFirst;
	}
	public void setUpdateWMFirst(Boolean updateWMFirst) {
		UpdateWMFirst = updateWMFirst;
	}
	
	
	
}
