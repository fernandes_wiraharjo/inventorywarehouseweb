package model;

public class mdlMovementType {

	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String MovementID;
	public String MovementName;
	public String StorageTypeSource;
	public String StorageTypeDestination;
	public String StorageTypeReturn;
	public String StorageBinSource;
	public String StorageBinDestination;
	public String StorageBinReturn;
	public Boolean FxdBinSource;
	public Boolean FxdBinDestination;
	public Boolean ScrSource;
	public Boolean ScrDestination;
	
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getMovementID() {
		return MovementID;
	}
	public void setMovementID(String movementID) {
		MovementID = movementID;
	}
	public String getMovementName() {
		return MovementName;
	}
	public void setMovementName(String movementName) {
		MovementName = movementName;
	}
	public String getStorageTypeSource() {
		return StorageTypeSource;
	}
	public void setStorageTypeSource(String storageTypeSource) {
		StorageTypeSource = storageTypeSource;
	}
	public String getStorageTypeDestination() {
		return StorageTypeDestination;
	}
	public void setStorageTypeDestination(String storageTypeDestination) {
		StorageTypeDestination = storageTypeDestination;
	}
	public String getStorageTypeReturn() {
		return StorageTypeReturn;
	}
	public void setStorageTypeReturn(String storageTypeReturn) {
		StorageTypeReturn = storageTypeReturn;
	}
	public String getStorageBinSource() {
		return StorageBinSource;
	}
	public void setStorageBinSource(String storageBinSource) {
		StorageBinSource = storageBinSource;
	}
	public String getStorageBinDestination() {
		return StorageBinDestination;
	}
	public void setStorageBinDestination(String storageBinDestination) {
		StorageBinDestination = storageBinDestination;
	}
	public String getStorageBinReturn() {
		return StorageBinReturn;
	}
	public void setStorageBinReturn(String storageBinReturn) {
		StorageBinReturn = storageBinReturn;
	}
	public Boolean getFxdBinSource() {
		return FxdBinSource;
	}
	public void setFxdBinSource(Boolean fxdBinSource) {
		FxdBinSource = fxdBinSource;
	}
	public Boolean getFxdBinDestination() {
		return FxdBinDestination;
	}
	public void setFxdBinDestination(Boolean fxdBinDestination) {
		FxdBinDestination = fxdBinDestination;
	}
	public Boolean getScrSource() {
		return ScrSource;
	}
	public void setScrSource(Boolean scrSource) {
		ScrSource = scrSource;
	}
	public Boolean getScrDestination() {
		return ScrDestination;
	}
	public void setScrDestination(Boolean scrDestination) {
		ScrDestination = scrDestination;
	}
	
	
}
