package model;

public class mdlBatch {
	public String ProductID;
	public String ProductName;
	public String Batch_No;
	public String Packing_No;
	public String GRDate;
	public String Expired_Date;
	public String Vendor_Batch_No;
	public String VendorID;
	public String VendorName;
	
	
	public String getVendorName() {
		return VendorName;
	}
	public void setVendorName(String vendorName) {
		VendorName = vendorName;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getBatch_No() {
		return Batch_No;
	}
	public void setBatch_No(String batch_No) {
		Batch_No = batch_No;
	}
	public String getPacking_No() {
		return Packing_No;
	}
	public void setPacking_No(String packing_No) {
		Packing_No = packing_No;
	}
	public String getGRDate() {
		return GRDate;
	}
	public void setGRDate(String gRDate) {
		GRDate = gRDate;
	}
	public String getExpired_Date() {
		return Expired_Date;
	}
	public void setExpired_Date(String expired_Date) {
		Expired_Date = expired_Date;
	}
	public String getVendor_Batch_No() {
		return Vendor_Batch_No;
	}
	public void setVendor_Batch_No(String vendor_Batch_No) {
		Vendor_Batch_No = vendor_Batch_No;
	}
	public String getVendorID() {
		return VendorID;
	}
	public void setVendorID(String vendorID) {
		VendorID = vendorID;
	}
	
	

}
