package model;

public class mdlLogin {

	public String userId;
	public String password;
	
	public String getUserid() {
		return userId;
	}
	public void setUserid(String lUserId) {
		this.userId = lUserId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String lPassword) {
		this.password = lPassword;
	}
}
