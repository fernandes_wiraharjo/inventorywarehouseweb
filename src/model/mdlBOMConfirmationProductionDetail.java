package model;

public class mdlBOMConfirmationProductionDetail {

	public String ConfirmationID;
	public String ConfirmationNo;
	public String ComponentLine;
	public String ComponentID;
	public String BatchNo;
	public String Qty;
	public String UOM;
	public String QtyBaseUOM;
	public String BaseUOM;
	public String getConfirmationID() {
		return ConfirmationID;
	}
	public void setConfirmationID(String confirmationID) {
		ConfirmationID = confirmationID;
	}
	public String getConfirmationNo() {
		return ConfirmationNo;
	}
	public void setConfirmationNo(String confirmationNo) {
		ConfirmationNo = confirmationNo;
	}
	public String getComponentLine() {
		return ComponentLine;
	}
	public void setComponentLine(String componentLine) {
		ComponentLine = componentLine;
	}
	public String getComponentID() {
		return ComponentID;
	}
	public void setComponentID(String componentID) {
		ComponentID = componentID;
	}
	public String getBatchNo() {
		return BatchNo;
	}
	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getQtyBaseUOM() {
		return QtyBaseUOM;
	}
	public void setQtyBaseUOM(String qtyBaseUOM) {
		QtyBaseUOM = qtyBaseUOM;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	
}
