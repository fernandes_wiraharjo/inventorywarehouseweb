package model;

public class mdlDoor {
	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String DoorID;
	public String DoorName;
	public Boolean GI_Indicator;
	public Boolean GR_Indicator;
	public Boolean CD_Indicator;

	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getDoorID() {
		return DoorID;
	}
	public void setDoorID(String doorID) {
		DoorID = doorID;
	}
	public String getDoorName() {
		return DoorName;
	}
	public void setDoorName(String doorName) {
		DoorName = doorName;
	}
	public Boolean getGI_Indicator() {
		return GI_Indicator;
	}
	public void setGI_Indicator(Boolean gI_Indicator) {
		GI_Indicator = gI_Indicator;
	}
	public Boolean getGR_Indicator() {
		return GR_Indicator;
	}
	public void setGR_Indicator(Boolean gR_Indicator) {
		GR_Indicator = gR_Indicator;
	}
	public Boolean getCD_Indicator() {
		return CD_Indicator;
	}
	public void setCD_Indicator(Boolean cD_Indicator) {
		CD_Indicator = cD_Indicator;
	}
}
