package model;

public class mdlTempComponentBatchSummary {

	public String OrderTypeID;
	public String OrderNo;
	public String ComponentLine;
	public String ComponentID;
	public String BatchNo;
	public String GRDate;
	public String ExpiredDate;
	public String PlantID;
	public String WarehouseID;
	public Integer Qty;
	public String UOM;
	public String Status;
	
	
	public String getOrderTypeID() {
		return OrderTypeID;
	}
	public void setOrderTypeID(String orderTypeID) {
		OrderTypeID = orderTypeID;
	}
	public String getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(String orderNo) {
		OrderNo = orderNo;
	}
	public String getComponentLine() {
		return ComponentLine;
	}
	public void setComponentLine(String componentLine) {
		ComponentLine = componentLine;
	}
	public String getComponentID() {
		return ComponentID;
	}
	public void setComponentID(String componentID) {
		ComponentID = componentID;
	}
	public String getBatchNo() {
		return BatchNo;
	}
	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}
	public String getGRDate() {
		return GRDate;
	}
	public void setGRDate(String gRDate) {
		GRDate = gRDate;
	}
	public String getExpiredDate() {
		return ExpiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		ExpiredDate = expiredDate;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public Integer getQty() {
		return Qty;
	}
	public void setQty(Integer qty) {
		Qty = qty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	
}
