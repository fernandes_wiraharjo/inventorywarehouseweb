package model;

public class mdlCustomerType {
	public String CustomerTypeID;
	public String CustomerTypeName;
	public String Description;
	
	public String getCustomerTypeID() {
		return CustomerTypeID;
	}
	public void setCustomerTypeID(String customerTypeID) {
		CustomerTypeID = customerTypeID;
	}
	
	public String getCustomerTypeName() {
		return CustomerTypeName;
	}
	public void setCustomerTypeName(String customerTypeName) {
		CustomerTypeName = customerTypeName;
	}
	
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
}
