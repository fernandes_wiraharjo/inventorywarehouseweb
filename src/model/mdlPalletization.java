package model;

public class mdlPalletization {

	public Integer StorageUnit1;
	public Integer StorageUnit2;
	public Integer StorageUnit3;
	public Integer QtyPerStorageUnit1;
	public Integer QtyPerStorageUnit2;
	public Integer QtyPerStorageUnit3;
	public String StorageUnitType1;
	public String StorageUnitType2;
	public String StorageUnitType3;
	public Integer StockPlacementQty;
	public String StockPlacementUOM;
	public Integer OpenQty;
	public Integer TotalTransferOrder;
	public String ProductID;
	public String PlantID;
	public String WarehouseID;
	public Integer ProductQty;
	public String ProductUOM;
	public String InboundDoc;
	public String SrcStorType;
	public String SrcStorBin;
	
	public Integer getStorageUnit1() {
		return StorageUnit1;
	}
	public void setStorageUnit1(Integer storageUnit1) {
		StorageUnit1 = storageUnit1;
	}
	public Integer getStorageUnit2() {
		return StorageUnit2;
	}
	public void setStorageUnit2(Integer storageUnit2) {
		StorageUnit2 = storageUnit2;
	}
	public Integer getStorageUnit3() {
		return StorageUnit3;
	}
	public void setStorageUnit3(Integer storageUnit3) {
		StorageUnit3 = storageUnit3;
	}
	public Integer getQtyPerStorageUnit1() {
		return QtyPerStorageUnit1;
	}
	public void setQtyPerStorageUnit1(Integer qtyPerStorageUnit1) {
		QtyPerStorageUnit1 = qtyPerStorageUnit1;
	}
	public Integer getQtyPerStorageUnit2() {
		return QtyPerStorageUnit2;
	}
	public void setQtyPerStorageUnit2(Integer qtyPerStorageUnit2) {
		QtyPerStorageUnit2 = qtyPerStorageUnit2;
	}
	public Integer getQtyPerStorageUnit3() {
		return QtyPerStorageUnit3;
	}
	public void setQtyPerStorageUnit3(Integer qtyPerStorageUnit3) {
		QtyPerStorageUnit3 = qtyPerStorageUnit3;
	}
	public String getStorageUnitType1() {
		return StorageUnitType1;
	}
	public void setStorageUnitType1(String storageUnitType1) {
		StorageUnitType1 = storageUnitType1;
	}
	public String getStorageUnitType2() {
		return StorageUnitType2;
	}
	public void setStorageUnitType2(String storageUnitType2) {
		StorageUnitType2 = storageUnitType2;
	}
	public String getStorageUnitType3() {
		return StorageUnitType3;
	}
	public void setStorageUnitType3(String storageUnitType3) {
		StorageUnitType3 = storageUnitType3;
	}
	public Integer getStockPlacementQty() {
		return StockPlacementQty;
	}
	public void setStockPlacementQty(Integer stockPlacementQty) {
		StockPlacementQty = stockPlacementQty;
	}
	public String getStockPlacementUOM() {
		return StockPlacementUOM;
	}
	public void setStockPlacementUOM(String stockPlacementUOM) {
		StockPlacementUOM = stockPlacementUOM;
	}
	public Integer getOpenQty() {
		return OpenQty;
	}
	public void setOpenQty(Integer openQty) {
		OpenQty = openQty;
	}
	public Integer getTotalTransferOrder() {
		return TotalTransferOrder;
	}
	public void setTotalTransferOrder(Integer totalTransferOrder) {
		TotalTransferOrder = totalTransferOrder;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public Integer getProductQty() {
		return ProductQty;
	}
	public void setProductQty(Integer productQty) {
		ProductQty = productQty;
	}
	public String getProductUOM() {
		return ProductUOM;
	}
	public void setProductUOM(String productUOM) {
		ProductUOM = productUOM;
	}
	public String getInboundDoc() {
		return InboundDoc;
	}
	public void setInboundDoc(String inboundDoc) {
		InboundDoc = inboundDoc;
	}
	public String getSrcStorType() {
		return SrcStorType;
	}
	public void setSrcStorType(String srcStorType) {
		SrcStorType = srcStorType;
	}
	public String getSrcStorBin() {
		return SrcStorBin;
	}
	public void setSrcStorBin(String srcStorBin) {
		SrcStorBin = srcStorBin;
	}
	
	
	
}
