package model;

public class mdlStorageSectionSearch {

	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String StorageTypeID;
	public String StorageTypeName;
	public String StorageSectionIndicatorID;
	public String StorageSectionIndicatorName;
	public String StorageSection1;
	public String StorageSection2;
	public String StorageSection3;
	public String StorageSection4;
	public String StorageSection5;
	public String StorageSection6;
	public String StorageSection7;
	public String StorageSection8;
	public String StorageSection9;
	public String StorageSection10;
	public String StorageSection11;
	public String StorageSection12;
	public String StorageSection13;
	public String StorageSection14;
	public String StorageSection15;
	public String StorageSection16;
	public String StorageSection17;
	public String StorageSection18;
	public String StorageSection19;
	public String StorageSection20;
	public String StorageSection21;
	public String StorageSection22;
	public String StorageSection23;
	public String StorageSection24;
	public String StorageSection25;
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getStorageTypeID() {
		return StorageTypeID;
	}
	public void setStorageTypeID(String storageTypeID) {
		StorageTypeID = storageTypeID;
	}
	public String getStorageTypeName() {
		return StorageTypeName;
	}
	public void setStorageTypeName(String storageTypeName) {
		StorageTypeName = storageTypeName;
	}
	public String getStorageSectionIndicatorID() {
		return StorageSectionIndicatorID;
	}
	public void setStorageSectionIndicatorID(String storageSectionIndicatorID) {
		StorageSectionIndicatorID = storageSectionIndicatorID;
	}
	public String getStorageSectionIndicatorName() {
		return StorageSectionIndicatorName;
	}
	public void setStorageSectionIndicatorName(String storageSectionIndicatorName) {
		StorageSectionIndicatorName = storageSectionIndicatorName;
	}
	public String getStorageSection1() {
		return StorageSection1;
	}
	public void setStorageSection1(String storageSection1) {
		StorageSection1 = storageSection1;
	}
	public String getStorageSection2() {
		return StorageSection2;
	}
	public void setStorageSection2(String storageSection2) {
		StorageSection2 = storageSection2;
	}
	public String getStorageSection3() {
		return StorageSection3;
	}
	public void setStorageSection3(String storageSection3) {
		StorageSection3 = storageSection3;
	}
	public String getStorageSection4() {
		return StorageSection4;
	}
	public void setStorageSection4(String storageSection4) {
		StorageSection4 = storageSection4;
	}
	public String getStorageSection5() {
		return StorageSection5;
	}
	public void setStorageSection5(String storageSection5) {
		StorageSection5 = storageSection5;
	}
	public String getStorageSection6() {
		return StorageSection6;
	}
	public void setStorageSection6(String storageSection6) {
		StorageSection6 = storageSection6;
	}
	public String getStorageSection7() {
		return StorageSection7;
	}
	public void setStorageSection7(String storageSection7) {
		StorageSection7 = storageSection7;
	}
	public String getStorageSection8() {
		return StorageSection8;
	}
	public void setStorageSection8(String storageSection8) {
		StorageSection8 = storageSection8;
	}
	public String getStorageSection9() {
		return StorageSection9;
	}
	public void setStorageSection9(String storageSection9) {
		StorageSection9 = storageSection9;
	}
	public String getStorageSection10() {
		return StorageSection10;
	}
	public void setStorageSection10(String storageSection10) {
		StorageSection10 = storageSection10;
	}
	public String getStorageSection11() {
		return StorageSection11;
	}
	public void setStorageSection11(String storageSection11) {
		StorageSection11 = storageSection11;
	}
	public String getStorageSection12() {
		return StorageSection12;
	}
	public void setStorageSection12(String storageSection12) {
		StorageSection12 = storageSection12;
	}
	public String getStorageSection13() {
		return StorageSection13;
	}
	public void setStorageSection13(String storageSection13) {
		StorageSection13 = storageSection13;
	}
	public String getStorageSection14() {
		return StorageSection14;
	}
	public void setStorageSection14(String storageSection14) {
		StorageSection14 = storageSection14;
	}
	public String getStorageSection15() {
		return StorageSection15;
	}
	public void setStorageSection15(String storageSection15) {
		StorageSection15 = storageSection15;
	}
	public String getStorageSection16() {
		return StorageSection16;
	}
	public void setStorageSection16(String storageSection16) {
		StorageSection16 = storageSection16;
	}
	public String getStorageSection17() {
		return StorageSection17;
	}
	public void setStorageSection17(String storageSection17) {
		StorageSection17 = storageSection17;
	}
	public String getStorageSection18() {
		return StorageSection18;
	}
	public void setStorageSection18(String storageSection18) {
		StorageSection18 = storageSection18;
	}
	public String getStorageSection19() {
		return StorageSection19;
	}
	public void setStorageSection19(String storageSection19) {
		StorageSection19 = storageSection19;
	}
	public String getStorageSection20() {
		return StorageSection20;
	}
	public void setStorageSection20(String storageSection20) {
		StorageSection20 = storageSection20;
	}
	public String getStorageSection21() {
		return StorageSection21;
	}
	public void setStorageSection21(String storageSection21) {
		StorageSection21 = storageSection21;
	}
	public String getStorageSection22() {
		return StorageSection22;
	}
	public void setStorageSection22(String storageSection22) {
		StorageSection22 = storageSection22;
	}
	public String getStorageSection23() {
		return StorageSection23;
	}
	public void setStorageSection23(String storageSection23) {
		StorageSection23 = storageSection23;
	}
	public String getStorageSection24() {
		return StorageSection24;
	}
	public void setStorageSection24(String storageSection24) {
		StorageSection24 = storageSection24;
	}
	public String getStorageSection25() {
		return StorageSection25;
	}
	public void setStorageSection25(String storageSection25) {
		StorageSection25 = storageSection25;
	}
	
	
}
