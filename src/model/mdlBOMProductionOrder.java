package model;

public class mdlBOMProductionOrder {

	public String OrderTypeID;
	public String OrderTypeDesc;
	public String OrderNo;
	public String ProductID;
	public String ProductName;
	public String PlantID;
	public String PlantName;
	public String Qty;
	public String UOM;
	public String QtyBaseUOM;
	public String BaseUOM;
	public String StartDate;
	public String WarehouseID;
	public String WarehouseName;
	public String Status;
	public String ButtonStatus;
	public String ButtonCancelStatus;
	
	public String getOrderTypeID() {
		return OrderTypeID;
	}
	public void setOrderTypeID(String orderTypeID) {
		OrderTypeID = orderTypeID;
	}
	public String getOrderTypeDesc() {
		return OrderTypeDesc;
	}
	public void setOrderTypeDesc(String orderTypeDesc) {
		OrderTypeDesc = orderTypeDesc;
	}
	public String getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(String orderNo) {
		OrderNo = orderNo;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getQtyBaseUOM() {
		return QtyBaseUOM;
	}
	public void setQtyBaseUOM(String qtyBaseUOM) {
		QtyBaseUOM = qtyBaseUOM;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	public String getStartDate() {
		return StartDate;
	}
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getButtonStatus() {
		return ButtonStatus;
	}
	public void setButtonStatus(String buttonStatus) {
		ButtonStatus = buttonStatus;
	}
	public String getButtonCancelStatus() {
		return ButtonCancelStatus;
	}
	public void setButtonCancelStatus(String buttonCancelStatus) {
		ButtonCancelStatus = buttonCancelStatus;
	}
	
	
}
