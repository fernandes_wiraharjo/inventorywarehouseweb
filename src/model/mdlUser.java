package model;

public class mdlUser {
	public String UserId;
	public String Password;
	public String Plant;
	public String Role;
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getPlant() {
		return Plant;
	}
	public void setPlant(String plant) {
		Plant = plant;
	}
	public String getRole() {
		return Role;
	}
	public void setRole(String role) {
		Role = role;
	}
	
	
}
