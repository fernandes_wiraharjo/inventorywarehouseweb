package model;

public class mdlReferenceMovementType {

	public String MovementID;
	public String MovementType;
	public String MovementDescription;
	
	public String getMovementID() {
		return MovementID;
	}
	public void setMovementID(String movementID) {
		MovementID = movementID;
	}
	public String getMovementType() {
		return MovementType;
	}
	public void setMovementType(String movementType) {
		MovementType = movementType;
	}
	public String getMovementDescription() {
		return MovementDescription;
	}
	public void setMovementDescription(String movementDescription) {
		MovementDescription = movementDescription;
	}
	
	
}
