package model;

public class mdlProductDetail {
	public String id;
	public String product_id;
	public String attribute_id;
	public String quantity;
	public String sku;
	public String status;
	public String created_at;
	public String updated_at;
	public String image;
	public String price_basic;
	public String price_selling;	
	public String price_carton;
	public String jumlah_per_carton;
	public String weight;
	public String width;
	public String height;
	public String volume;
	public String wide;
	public String wide_carton;
	public String weight_carton;
	public String width_carton;
	public String height_carton;
	public String volume_carton;
	public String view;
	public String code;
	public String barcode;
	public String price_selling_tax;
	public String price_carton_tax;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getAttribute_id() {
		return attribute_id;
	}
	public void setAttribute_id(String attribute_id) {
		this.attribute_id = attribute_id;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getPrice_basic() {
		return price_basic;
	}
	public void setPrice_basic(String price_basic) {
		this.price_basic = price_basic;
	}
	public String getPrice_selling() {
		return price_selling;
	}
	public void setPrice_selling(String price_selling) {
		this.price_selling = price_selling;
	}
	public String getPrice_carton() {
		return price_carton;
	}
	public void setPrice_carton(String price_carton) {
		this.price_carton = price_carton;
	}
	public String getJumlah_per_carton() {
		return jumlah_per_carton;
	}
	public void setJumlah_per_carton(String jumlah_per_carton) {
		this.jumlah_per_carton = jumlah_per_carton;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getWide() {
		return wide;
	}
	public void setWide(String wide) {
		this.wide = wide;
	}
	public String getWide_carton() {
		return wide_carton;
	}
	public void setWide_carton(String wide_carton) {
		this.wide_carton = wide_carton;
	}
	public String getWeight_carton() {
		return weight_carton;
	}
	public void setWeight_carton(String weight_carton) {
		this.weight_carton = weight_carton;
	}
	public String getWidth_carton() {
		return width_carton;
	}
	public void setWidth_carton(String width_carton) {
		this.width_carton = width_carton;
	}
	public String getHeight_carton() {
		return height_carton;
	}
	public void setHeight_carton(String height_carton) {
		this.height_carton = height_carton;
	}
	public String getVolume_carton() {
		return volume_carton;
	}
	public void setVolume_carton(String volume_carton) {
		this.volume_carton = volume_carton;
	}
	public String getView() {
		return view;
	}
	public void setView(String view) {
		this.view = view;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getPrice_selling_tax() {
		return price_selling_tax;
	}
	public void setPrice_selling_tax(String price_selling_tax) {
		this.price_selling_tax = price_selling_tax;
	}
	public String getPrice_carton_tax() {
		return price_carton_tax;
	}
	public void setPrice_carton_tax(String price_carton_tax) {
		this.price_carton_tax = price_carton_tax;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	

}
