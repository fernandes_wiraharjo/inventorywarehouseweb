package model;

public class mdlStorageType {

	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String StorageTypeID;
	public String StorageTypeName;
	public Boolean SU_mgmt_active;
	public Boolean Stor_type_is_ID_pnt;
	public Boolean Stor_type_is_pck_pnt;
	public String PutawayStrategy;
	public Boolean Stock_plcmt_req_confirmation;
	public Boolean Dst_bin_ch_during_confirm;
	public Boolean MixedStorage;
	public Boolean Addn_to_stock;
	public Boolean Retain_overdeliveries;
	public Boolean SUT_check_active;
	public Boolean Storage_sec_check_active;
	public Boolean Block_upon_stock_plcmt;
	public String Assigned_ID_point_stor_type;
	public String StockRemovalStrategy;
	public Boolean Stock_rmvl_req_confirmation;
	public Boolean Allow_negative_stock;
	public Boolean Full_stk_rmvl_reqmt_act;
	public Boolean Return_stock_to_same_storage_bin;
	public Boolean Execute_zero_stock_check;
	public Boolean Round_off_qty;
	public Boolean Block_upon_stock_rmvl;
	public String Assigned_pick_point_stor_ty;
	public String Return_storage_type;
	
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getStorageTypeID() {
		return StorageTypeID;
	}
	public void setStorageTypeID(String storageTypeID) {
		StorageTypeID = storageTypeID;
	}
	public String getStorageTypeName() {
		return StorageTypeName;
	}
	public void setStorageTypeName(String storageTypeName) {
		StorageTypeName = storageTypeName;
	}
	public Boolean getSU_mgmt_active() {
		return SU_mgmt_active;
	}
	public void setSU_mgmt_active(Boolean sU_mgmt_active) {
		SU_mgmt_active = sU_mgmt_active;
	}
	public Boolean getStor_type_is_ID_pnt() {
		return Stor_type_is_ID_pnt;
	}
	public void setStor_type_is_ID_pnt(Boolean stor_type_is_ID_pnt) {
		Stor_type_is_ID_pnt = stor_type_is_ID_pnt;
	}
	public Boolean getStor_type_is_pck_pnt() {
		return Stor_type_is_pck_pnt;
	}
	public void setStor_type_is_pck_pnt(Boolean stor_type_is_pck_pnt) {
		Stor_type_is_pck_pnt = stor_type_is_pck_pnt;
	}
	public String getPutawayStrategy() {
		return PutawayStrategy;
	}
	public void setPutawayStrategy(String putawayStrategy) {
		PutawayStrategy = putawayStrategy;
	}
	public Boolean getStock_plcmt_req_confirmation() {
		return Stock_plcmt_req_confirmation;
	}
	public void setStock_plcmt_req_confirmation(Boolean stock_plcmt_req_confirmation) {
		Stock_plcmt_req_confirmation = stock_plcmt_req_confirmation;
	}
	public Boolean getDst_bin_ch_during_confirm() {
		return Dst_bin_ch_during_confirm;
	}
	public void setDst_bin_ch_during_confirm(Boolean dst_bin_ch_during_confirm) {
		Dst_bin_ch_during_confirm = dst_bin_ch_during_confirm;
	}
	public Boolean getMixedStorage() {
		return MixedStorage;
	}
	public void setMixedStorage(Boolean mixedStorage) {
		MixedStorage = mixedStorage;
	}
	public Boolean getAddn_to_stock() {
		return Addn_to_stock;
	}
	public void setAddn_to_stock(Boolean addn_to_stock) {
		Addn_to_stock = addn_to_stock;
	}
	public Boolean getRetain_overdeliveries() {
		return Retain_overdeliveries;
	}
	public void setRetain_overdeliveries(Boolean retain_overdeliveries) {
		Retain_overdeliveries = retain_overdeliveries;
	}
	public Boolean getSUT_check_active() {
		return SUT_check_active;
	}
	public void setSUT_check_active(Boolean sUT_check_active) {
		SUT_check_active = sUT_check_active;
	}
	public Boolean getStorage_sec_check_active() {
		return Storage_sec_check_active;
	}
	public void setStorage_sec_check_active(Boolean storage_sec_check_active) {
		Storage_sec_check_active = storage_sec_check_active;
	}
	public String getAssigned_ID_point_stor_type() {
		return Assigned_ID_point_stor_type;
	}
	public void setAssigned_ID_point_stor_type(String assigned_ID_point_stor_type) {
		Assigned_ID_point_stor_type = assigned_ID_point_stor_type;
	}
	public String getStockRemovalStrategy() {
		return StockRemovalStrategy;
	}
	public void setStockRemovalStrategy(String stockRemovalStrategy) {
		StockRemovalStrategy = stockRemovalStrategy;
	}
	public Boolean getStock_rmvl_req_confirmation() {
		return Stock_rmvl_req_confirmation;
	}
	public void setStock_rmvl_req_confirmation(Boolean stock_rmvl_req_confirmation) {
		Stock_rmvl_req_confirmation = stock_rmvl_req_confirmation;
	}
	public Boolean getAllow_negative_stock() {
		return Allow_negative_stock;
	}
	public void setAllow_negative_stock(Boolean allow_negative_stock) {
		Allow_negative_stock = allow_negative_stock;
	}
	public Boolean getFull_stk_rmvl_reqmt_act() {
		return Full_stk_rmvl_reqmt_act;
	}
	public void setFull_stk_rmvl_reqmt_act(Boolean full_stk_rmvl_reqmt_act) {
		Full_stk_rmvl_reqmt_act = full_stk_rmvl_reqmt_act;
	}
	public Boolean getReturn_stock_to_same_storage_bin() {
		return Return_stock_to_same_storage_bin;
	}
	public void setReturn_stock_to_same_storage_bin(Boolean return_stock_to_same_storage_bin) {
		Return_stock_to_same_storage_bin = return_stock_to_same_storage_bin;
	}
	public Boolean getExecute_zero_stock_check() {
		return Execute_zero_stock_check;
	}
	public void setExecute_zero_stock_check(Boolean execute_zero_stock_check) {
		Execute_zero_stock_check = execute_zero_stock_check;
	}
	public Boolean getRound_off_qty() {
		return Round_off_qty;
	}
	public void setRound_off_qty(Boolean round_off_qty) {
		Round_off_qty = round_off_qty;
	}
	public Boolean getBlock_upon_stock_rmvl() {
		return Block_upon_stock_rmvl;
	}
	public void setBlock_upon_stock_rmvl(Boolean block_upon_stock_rmvl) {
		Block_upon_stock_rmvl = block_upon_stock_rmvl;
	}
	public String getAssigned_pick_point_stor_ty() {
		return Assigned_pick_point_stor_ty;
	}
	public void setAssigned_pick_point_stor_ty(String assigned_pick_point_stor_ty) {
		Assigned_pick_point_stor_ty = assigned_pick_point_stor_ty;
	}
	public String getReturn_storage_type() {
		return Return_storage_type;
	}
	public void setReturn_storage_type(String return_storage_type) {
		Return_storage_type = return_storage_type;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public Boolean getBlock_upon_stock_plcmt() {
		return Block_upon_stock_plcmt;
	}
	public void setBlock_upon_stock_plcmt(Boolean block_upon_stock_plcmt) {
		Block_upon_stock_plcmt = block_upon_stock_plcmt;
	}

	
}
