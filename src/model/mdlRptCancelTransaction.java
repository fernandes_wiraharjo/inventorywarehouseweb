package model;

public class mdlRptCancelTransaction {
	public String DocNumber;
	public String Date;
	public String PlantID;
	public String WarehouseID;
	public String RefDocNumber;
	public String DocLine;
	public String ProductID;
	public String QtyUOM;
	public String UOM;
	public String QtyBaseUOM;	
	public String BaseUOM;
	public String BatchNo;
	public String PackingNo;
	
	public String getDocNumber() {
		return DocNumber;
	}
	public void setDocNumber(String docNumber) {
		DocNumber = docNumber;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getRefDocNumber() {
		return RefDocNumber;
	}
	public void setRefDocNumber(String refDocNumber) {
		RefDocNumber = refDocNumber;
	}
	public String getDocLine() {
		return DocLine;
	}
	public void setDocLine(String docLine) {
		DocLine = docLine;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getQtyUOM() {
		return QtyUOM;
	}
	public void setQtyUOM(String qtyUOM) {
		QtyUOM = qtyUOM;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getQtyBaseUOM() {
		return QtyBaseUOM;
	}
	public void setQtyBaseUOM(String qtyBaseUOM) {
		QtyBaseUOM = qtyBaseUOM;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	public String getBatchNo() {
		return BatchNo;
	}
	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}
	public String getPackingNo() {
		return PackingNo;
	}
	public void setPackingNo(String packingNo) {
		PackingNo = packingNo;
	}

}
