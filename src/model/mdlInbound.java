package model;

public class mdlInbound {
	
	public String DocNumber;
	public String Date;
	public String VendorID;
	public String PlantID;
	public String WarehouseID;
	public String DocLine;
	public String ProductID;
	public String ProductName;
	public String QtyUom;
	public String Uom;
	public String QtyBaseUom;
	public String BaseUom;
	public String BatchNo;
	public String VendorBatchNo;
	public String PackingNo;
	public String ExpiredDate;
	public String DoorID;
	public String StagingAreaID;
	public String WMStatus;
	public String UpdateWMFirst;
	public String InboundStatus;
	
	public String getDocNumber() {
		return DocNumber;
	}
	public void setDocNumber(String docNumber) {
		DocNumber = docNumber;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getVendorID() {
		return VendorID;
	}
	public void setVendorID(String vendorID) {
		VendorID = vendorID;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getDocLine() {
		return DocLine;
	}
	public void setDocLine(String docLine) {
		DocLine = docLine;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getQtyUom() {
		return QtyUom;
	}
	public void setQtyUom(String qtyUom) {
		QtyUom = qtyUom;
	}
	public String getUom() {
		return Uom;
	}
	public void setUom(String uom) {
		Uom = uom;
	}
	public String getQtyBaseUom() {
		return QtyBaseUom;
	}
	public void setQtyBaseUom(String qtyBaseUom) {
		QtyBaseUom = qtyBaseUom;
	}
	public String getBaseUom() {
		return BaseUom;
	}
	public void setBaseUom(String baseUom) {
		BaseUom = baseUom;
	}
	public String getBatchNo() {
		return BatchNo;
	}
	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}
	public String getVendorBatchNo() {
		return VendorBatchNo;
	}
	public void setVendorBatchNo(String vendorBatchNo) {
		VendorBatchNo = vendorBatchNo;
	}
	public String getPackingNo() {
		return PackingNo;
	}
	public void setPackingNo(String packingNo) {
		PackingNo = packingNo;
	}
	public String getExpiredDate() {
		return ExpiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		ExpiredDate = expiredDate;
	}
	public String getDoorID() {
		return DoorID;
	}
	public void setDoorID(String doorID) {
		DoorID = doorID;
	}
	public String getStagingAreaID() {
		return StagingAreaID;
	}
	public void setStagingAreaID(String stagingAreaID) {
		StagingAreaID = stagingAreaID;
	}
	public String getWMStatus() {
		return WMStatus;
	}
	public void setWMStatus(String wMStatus) {
		WMStatus = wMStatus;
	}
	public String getUpdateWMFirst() {
		return UpdateWMFirst;
	}
	public void setUpdateWMFirst(String updateWMFirst) {
		UpdateWMFirst = updateWMFirst;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getInboundStatus() {
		return InboundStatus;
	}
	public void setInboundStatus(String inboundStatus) {
		InboundStatus = inboundStatus;
	}
	
	
}
