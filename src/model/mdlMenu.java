package model;

public class mdlMenu {

	public String MenuID;
	public String MenuName;
	public String MenuUrl;
	public String Type;
	public String Level;
	public Boolean CRUD;
	public Boolean IsModify;
	
	public String getMenuID() {
		return MenuID;
	}
	public void setMenuID(String menuID) {
		MenuID = menuID;
	}
	public String getMenuName() {
		return MenuName;
	}
	public void setMenuName(String menuName) {
		MenuName = menuName;
	}
	public String getMenuUrl() {
		return MenuUrl;
	}
	public void setMenuUrl(String menuUrl) {
		MenuUrl = menuUrl;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getLevel() {
		return Level;
	}
	public void setLevel(String level) {
		Level = level;
	}
	public Boolean getCRUD() {
		return CRUD;
	}
	public void setCRUD(Boolean cRUD) {
		CRUD = cRUD;
	}
	public Boolean getIsModify() {
		return IsModify;
	}
	public void setIsModify(Boolean isModify) {
		IsModify = isModify;
	}
	
	
}
