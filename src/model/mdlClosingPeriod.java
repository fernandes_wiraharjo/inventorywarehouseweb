package model;

public class mdlClosingPeriod {

	public String Period;
	public String Fiscal_Year;
	public String IsActive;
	public String Created_by;
	public String Created_at;
	
	public String getPeriod() {
		return Period;
	}
	public void setPeriod(String period) {
		Period = period;
	}
	public String getFiscal_Year() {
		return Fiscal_Year;
	}
	public void setFiscal_Year(String fiscal_Year) {
		Fiscal_Year = fiscal_Year;
	}
	public String getIsActive() {
		return IsActive;
	}
	public void setIsActive(String isActive) {
		IsActive = isActive;
	}
	public String getCreated_by() {
		return Created_by;
	}
	public void setCreated_by(String created_by) {
		Created_by = created_by;
	}
	public String getCreated_at() {
		return Created_at;
	}
	public void setCreated_at(String created_at) {
		Created_at = created_at;
	}
	
}
