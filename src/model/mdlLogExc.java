package model;

public class mdlLogExc {
	public String Exception;
	public String Created_on;
	public String Created_by;
	
	public String getException() {
		return Exception;
	}
	public void setException(String lException) {
		Exception = lException;
	}
	
	public String getCreated_on() {
		return Created_on;
	}
	public void setCreated_on(String lCreated_on) {
		Created_on = lCreated_on;
	}
	
	public String getCreated_by() {
		return Created_by;
	}
	public void setCreated_by(String lCreated_by) {
		Created_by = lCreated_by;
	}
}
