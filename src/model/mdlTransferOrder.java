package model;

public class mdlTransferOrder {

	public Integer No;
	public String TransferOrderID;
	public String PlantID;
	public String WarehouseID;
	public Boolean WMS_Status;
	public Boolean UpdateWMFirst;
	public String ProductID;
	public Integer DestinationQty;
	public String StorageUnitType;
	public String StorageTypeID;
	public String StorageSectionID;
	public String DestinationStorageBin;
	public Integer DestinationStorageUnit;
	public String BatchNo;
	public String InboundDocID;
	public String Status;
	public String UOM;
	public Double CapacityUsed;
	public String SrcStorageType;
	public String SrcStorageBin;
	public String CreationDate;
	public String ButtonStatus;
	public String Result;
	
	public Integer getNo() {
		return No;
	}
	public void setNo(Integer no) {
		No = no;
	}
	public String getTransferOrderID() {
		return TransferOrderID;
	}
	public void setTransferOrderID(String transferOrderID) {
		TransferOrderID = transferOrderID;
	}
	public Integer getDestinationQty() {
		return DestinationQty;
	}
	public void setDestinationQty(Integer destinationQty) {
		DestinationQty = destinationQty;
	}
	public String getStorageUnitType() {
		return StorageUnitType;
	}
	public void setStorageUnitType(String storageUnitType) {
		StorageUnitType = storageUnitType;
	}
	public String getStorageTypeID() {
		return StorageTypeID;
	}
	public void setStorageTypeID(String storageTypeID) {
		StorageTypeID = storageTypeID;
	}
	public String getStorageSectionID() {
		return StorageSectionID;
	}
	public void setStorageSectionID(String storageSectionID) {
		StorageSectionID = storageSectionID;
	}
	public String getDestinationStorageBin() {
		return DestinationStorageBin;
	}
	public void setDestinationStorageBin(String destinationStorageBin) {
		DestinationStorageBin = destinationStorageBin;
	}
	public Integer getDestinationStorageUnit() {
		return DestinationStorageUnit;
	}
	public void setDestinationStorageUnit(Integer destinationStorageUnit) {
		DestinationStorageUnit = destinationStorageUnit;
	}
	public String getBatchNo() {
		return BatchNo;
	}
	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}
	public String getInboundDocID() {
		return InboundDocID;
	}
	public void setInboundDocID(String inboundDocID) {
		InboundDocID = inboundDocID;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public Double getCapacityUsed() {
		return CapacityUsed;
	}
	public void setCapacityUsed(Double capacityUsed) {
		CapacityUsed = capacityUsed;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getSrcStorageType() {
		return SrcStorageType;
	}
	public void setSrcStorageType(String srcStorageType) {
		SrcStorageType = srcStorageType;
	}
	public String getSrcStorageBin() {
		return SrcStorageBin;
	}
	public void setSrcStorageBin(String srcStorageBin) {
		SrcStorageBin = srcStorageBin;
	}
	public String getCreationDate() {
		return CreationDate;
	}
	public void setCreationDate(String creationDate) {
		CreationDate = creationDate;
	}
	public String getButtonStatus() {
		return ButtonStatus;
	}
	public void setButtonStatus(String buttonStatus) {
		ButtonStatus = buttonStatus;
	}
	public Boolean getWMS_Status() {
		return WMS_Status;
	}
	public void setWMS_Status(Boolean wMS_Status) {
		WMS_Status = wMS_Status;
	}
	public Boolean getUpdateWMFirst() {
		return UpdateWMFirst;
	}
	public void setUpdateWMFirst(Boolean updateWMFirst) {
		UpdateWMFirst = updateWMFirst;
	}
	public String getResult() {
		return Result;
	}
	public void setResult(String result) {
		Result = result;
	}
	
	
	
}
