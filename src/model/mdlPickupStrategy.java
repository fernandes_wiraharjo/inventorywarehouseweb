package model;

public class mdlPickupStrategy {
	public String StockRemovalStrategy;
	public String Description;
	
	public String getStockRemovalStrategy() {
		return StockRemovalStrategy;
	}
	public void setStockRemovalStrategy(String stockRemovalStrategy) {
		StockRemovalStrategy = stockRemovalStrategy;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	
	
}
