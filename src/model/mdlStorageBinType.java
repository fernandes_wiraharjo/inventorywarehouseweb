package model;

public class mdlStorageBinType {
	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String StorageBinTypeID;
	public String StorageBinTypeName;
	
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getStorageBinTypeID() {
		return StorageBinTypeID;
	}
	public void setStorageBinTypeID(String storageBinTypeID) {
		StorageBinTypeID = storageBinTypeID;
	}
	public String getStorageBinTypeName() {
		return StorageBinTypeName;
	}
	public void setStorageBinTypeName(String storageBinTypeName) {
		StorageBinTypeName = storageBinTypeName;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	
	
}
