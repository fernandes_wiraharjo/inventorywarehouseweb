package model;

public class mdlBOMCancelConfirmationDetail {

	public String CancelConfirmationID;
	public String CancelConfirmationNo;
	public String ComponentLine;
	public String ComponentID;
	public String BatchNo;
	public String Qty;
	public String UOM;
	public String QtyBaseUOM;
	public String BaseUOM;
	
	public String getCancelConfirmationID() {
		return CancelConfirmationID;
	}
	public void setCancelConfirmationID(String cancelConfirmationID) {
		CancelConfirmationID = cancelConfirmationID;
	}
	public String getCancelConfirmationNo() {
		return CancelConfirmationNo;
	}
	public void setCancelConfirmationNo(String cancelConfirmationNo) {
		CancelConfirmationNo = cancelConfirmationNo;
	}
	public String getComponentLine() {
		return ComponentLine;
	}
	public void setComponentLine(String componentLine) {
		ComponentLine = componentLine;
	}
	public String getComponentID() {
		return ComponentID;
	}
	public void setComponentID(String componentID) {
		ComponentID = componentID;
	}
	public String getBatchNo() {
		return BatchNo;
	}
	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getQtyBaseUOM() {
		return QtyBaseUOM;
	}
	public void setQtyBaseUOM(String qtyBaseUOM) {
		QtyBaseUOM = qtyBaseUOM;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	
}
