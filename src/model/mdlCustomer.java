package model;
/** Documentation
 * 
 */
public class mdlCustomer {
	public String CustomerID;
	public String CustomerName;
	public String CustomerAddress;
	public String Phone;
	public String Email;
	public String Pic;
	public String CustomerTypeID;
	public String CustomerTypeName;
	
	public String getCustomerTypeName() {
		return CustomerTypeName;
	}
	public void setCustomerTypeName(String customerTypeName) {
		CustomerTypeName = customerTypeName;
	}
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	
	public String getCustomerAddress() {
		return CustomerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		CustomerAddress = customerAddress;
	}
	
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}

	public String getPic() {
		return Pic;
	}
	public void setPic(String pic) {
		Pic = pic;
	}
	
	public String getCustomerTypeID() {
		return CustomerTypeID;
	}
	public void setCustomerTypeID(String customerTypeID) {
		CustomerTypeID = customerTypeID;
	}
}
