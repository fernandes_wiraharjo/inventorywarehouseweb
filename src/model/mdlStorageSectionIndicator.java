package model;

public class mdlStorageSectionIndicator {
	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String StorageSectionIndicatorID;
	public String StorageSectionIndicatorName;

	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getStorageSectionIndicatorID() {
		return StorageSectionIndicatorID;
	}
	public void setStorageSectionIndicatorID(String storageSectionIndicatorID) {
		StorageSectionIndicatorID = storageSectionIndicatorID;
	}
	public String getStorageSectionIndicatorName() {
		return StorageSectionIndicatorName;
	}
	public void setStorageSectionIndicatorName(String storageSectionIndicatorName) {
		StorageSectionIndicatorName = storageSectionIndicatorName;
	}
}
