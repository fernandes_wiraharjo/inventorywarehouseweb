package model;

/** Documentation
 * 001 Nanda -- beginning
 */

public class mdlUom {
	public String Code;
	public String Description;
	public Boolean Is_BaseUOM;
	
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Boolean getIs_BaseUOM() {
		return Is_BaseUOM;
	}
	public void setIs_BaseUOM(Boolean is_BaseUOM) {
		Is_BaseUOM = is_BaseUOM;
	}
	
	
}
