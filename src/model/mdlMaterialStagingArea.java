package model;

public class mdlMaterialStagingArea {
	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String MaterialStagingAreaID;
	public String MaterialStagingAreaName;
	public String DoorID;
	public String DoorName;
	
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getMaterialStagingAreaID() {
		return MaterialStagingAreaID;
	}
	public void setMaterialStagingAreaID(String materialStagingAreaID) {
		MaterialStagingAreaID = materialStagingAreaID;
	}
	public String getMaterialStagingAreaName() {
		return MaterialStagingAreaName;
	}
	public void setMaterialStagingAreaName(String materialStagingAreaName) {
		MaterialStagingAreaName = materialStagingAreaName;
	}
	public String getDoorID() {
		return DoorID;
	}
	public void setDoorID(String doorID) {
		DoorID = doorID;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getDoorName() {
		return DoorName;
	}
	public void setDoorName(String doorName) {
		DoorName = doorName;
	}
	
	
}
