package model;

public class mdlProductBOMDetail {

	public String ProductID;
	public String ComponentLine;
	public String ComponentID;
	public String ComponentName;
	public String Qty;
	public String UOM;
	
	public String getComponentLine() {
		return ComponentLine;
	}
	public void setComponentLine(String componentLine) {
		ComponentLine = componentLine;
	}
	public String getComponentName() {
		return ComponentName;
	}
	public void setComponentName(String componentName) {
		ComponentName = componentName;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getComponentID() {
		return ComponentID;
	}
	public void setComponentID(String componentID) {
		ComponentID = componentID;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	
}
