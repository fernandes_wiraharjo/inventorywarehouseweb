package model;

public class mdlStorageBinDetail {

	public String PlantID;
	public String WarehouseID;
	public String StorageType;
	public String StorageSection;
	public String StorageBin;
	public String DestStorageUnit;
	public String StorageUnitType;
	public String ProductID;
	public String BatchNo;
	public Integer Qty;
	public String UOM;
	
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getStorageType() {
		return StorageType;
	}
	public void setStorageType(String storageType) {
		StorageType = storageType;
	}
	public String getStorageSection() {
		return StorageSection;
	}
	public void setStorageSection(String storageSection) {
		StorageSection = storageSection;
	}
	public String getStorageBin() {
		return StorageBin;
	}
	public void setStorageBin(String storageBin) {
		StorageBin = storageBin;
	}
	public String getDestStorageUnit() {
		return DestStorageUnit;
	}
	public void setDestStorageUnit(String destStorageUnit) {
		DestStorageUnit = destStorageUnit;
	}
	public String getStorageUnitType() {
		return StorageUnitType;
	}
	public void setStorageUnitType(String storageUnitType) {
		StorageUnitType = storageUnitType;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getBatchNo() {
		return BatchNo;
	}
	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}
	public Integer getQty() {
		return Qty;
	}
	public void setQty(Integer qty) {
		Qty = qty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	
	
}
