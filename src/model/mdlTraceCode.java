package model;

/** Documentation
 * 001 Nanda
 */
public class mdlTraceCode {
	
	public String TraceCode;
	public String Date;
	public String Flag_Table;  //001 Nanda
	
	public String getTraceCode() {
		return TraceCode;
	}
	public void setTraceCode(String traceCode) {
		TraceCode = traceCode;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getFlag_Table() {
		return Flag_Table;
	}
	public void setFlag_Table(String flag_Table) {
		Flag_Table = flag_Table;
	}
}
