package model;

public class mdlStorageTypeSearch {

	public String PlantID;
	public String PlantName;
	public String WarehouseID;
	public String WarehouseName;
	public String Operation;
	public String StorageTypeIndicatorID;
	public String StorageTypeIndicatorName;
	public String StorageType1;
	public String StorageType2;
	public String StorageType3;
	public String StorageType4;
	public String StorageType5;
	public String StorageType6;
	public String StorageType7;
	public String StorageType8;
	public String StorageType9;
	public String StorageType10;
	public String StorageType11;
	public String StorageType12;
	public String StorageType13;
	public String StorageType14;
	public String StorageType15;
	public String StorageType16;
	public String StorageType17;
	public String StorageType18;
	public String StorageType19;
	public String StorageType20;
	public String StorageType21;
	public String StorageType22;
	public String StorageType23;
	public String StorageType24;
	public String StorageType25;
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getOperation() {
		return Operation;
	}
	public void setOperation(String operation) {
		Operation = operation;
	}
	public String getStorageType1() {
		return StorageType1;
	}
	public void setStorageType1(String storageType1) {
		StorageType1 = storageType1;
	}
	public String getStorageType2() {
		return StorageType2;
	}
	public void setStorageType2(String storageType2) {
		StorageType2 = storageType2;
	}
	public String getStorageType3() {
		return StorageType3;
	}
	public void setStorageType3(String storageType3) {
		StorageType3 = storageType3;
	}
	public String getStorageType4() {
		return StorageType4;
	}
	public void setStorageType4(String storageType4) {
		StorageType4 = storageType4;
	}
	public String getStorageType5() {
		return StorageType5;
	}
	public void setStorageType5(String storageType5) {
		StorageType5 = storageType5;
	}
	public String getStorageType6() {
		return StorageType6;
	}
	public void setStorageType6(String storageType6) {
		StorageType6 = storageType6;
	}
	public String getStorageType7() {
		return StorageType7;
	}
	public void setStorageType7(String storageType7) {
		StorageType7 = storageType7;
	}
	public String getStorageType8() {
		return StorageType8;
	}
	public void setStorageType8(String storageType8) {
		StorageType8 = storageType8;
	}
	public String getStorageType9() {
		return StorageType9;
	}
	public void setStorageType9(String storageType9) {
		StorageType9 = storageType9;
	}
	public String getStorageType10() {
		return StorageType10;
	}
	public void setStorageType10(String storageType10) {
		StorageType10 = storageType10;
	}
	public String getStorageType11() {
		return StorageType11;
	}
	public void setStorageType11(String storageType11) {
		StorageType11 = storageType11;
	}
	public String getStorageType12() {
		return StorageType12;
	}
	public void setStorageType12(String storageType12) {
		StorageType12 = storageType12;
	}
	public String getStorageType13() {
		return StorageType13;
	}
	public void setStorageType13(String storageType13) {
		StorageType13 = storageType13;
	}
	public String getStorageType14() {
		return StorageType14;
	}
	public void setStorageType14(String storageType14) {
		StorageType14 = storageType14;
	}
	public String getStorageType15() {
		return StorageType15;
	}
	public void setStorageType15(String storageType15) {
		StorageType15 = storageType15;
	}
	public String getStorageType16() {
		return StorageType16;
	}
	public void setStorageType16(String storageType16) {
		StorageType16 = storageType16;
	}
	public String getStorageType17() {
		return StorageType17;
	}
	public void setStorageType17(String storageType17) {
		StorageType17 = storageType17;
	}
	public String getStorageType18() {
		return StorageType18;
	}
	public void setStorageType18(String storageType18) {
		StorageType18 = storageType18;
	}
	public String getStorageType19() {
		return StorageType19;
	}
	public void setStorageType19(String storageType19) {
		StorageType19 = storageType19;
	}
	public String getStorageType20() {
		return StorageType20;
	}
	public void setStorageType20(String storageType20) {
		StorageType20 = storageType20;
	}
	public String getStorageType21() {
		return StorageType21;
	}
	public void setStorageType21(String storageType21) {
		StorageType21 = storageType21;
	}
	public String getStorageType22() {
		return StorageType22;
	}
	public void setStorageType22(String storageType22) {
		StorageType22 = storageType22;
	}
	public String getStorageType23() {
		return StorageType23;
	}
	public void setStorageType23(String storageType23) {
		StorageType23 = storageType23;
	}
	public String getStorageType24() {
		return StorageType24;
	}
	public void setStorageType24(String storageType24) {
		StorageType24 = storageType24;
	}
	public String getStorageType25() {
		return StorageType25;
	}
	public void setStorageType25(String storageType25) {
		StorageType25 = storageType25;
	}
	public String getPlantName() {
		return PlantName;
	}
	public void setPlantName(String plantName) {
		PlantName = plantName;
	}
	public String getWarehouseName() {
		return WarehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		WarehouseName = warehouseName;
	}
	public String getStorageTypeIndicatorID() {
		return StorageTypeIndicatorID;
	}
	public void setStorageTypeIndicatorID(String storageTypeIndicatorID) {
		StorageTypeIndicatorID = storageTypeIndicatorID;
	}
	public String getStorageTypeIndicatorName() {
		return StorageTypeIndicatorName;
	}
	public void setStorageTypeIndicatorName(String storageTypeIndicatorName) {
		StorageTypeIndicatorName = storageTypeIndicatorName;
	}
	
	
}
