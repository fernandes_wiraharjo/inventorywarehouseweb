package model;

public class mdlBrand {
	public String id;
	public String name;
	public String short_description_en;
	public String description_en;
	public String short_description_id;
	public String description_id;
	public String order;
	public String status;
	public String slug;
	public String created_at;
	public String updated_at;
	public String image;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getShort_description_en() {
		return short_description_en;
	}
	public void setShort_description_en(String short_description_en) {
		this.short_description_en = short_description_en;
	}
	public String getDescription_en() {
		return description_en;
	}
	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}
	public String getShort_description_id() {
		return short_description_id;
	}
	public void setShort_description_id(String short_description_id) {
		this.short_description_id = short_description_id;
	}
	public String getDescription_id() {
		return description_id;
	}
	public void setDescription_id(String description_id) {
		this.description_id = description_id;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	

}
