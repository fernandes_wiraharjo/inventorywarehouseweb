package model;

public class mdlBOMCancelConfirmation {

	public String CancelConfirmationID;
	public String CancelConfirmationNo;
	public String ConfirmationID;
	public String ConfirmationNo;
	public String Date;
	public String ProductID;
	public String BatchNo;
	public String Qty;
	public String UOM;
	public String QtyBaseUOM;
	public String BaseUOM;
	public String PlantID;
	public String WarehouseID;
	public String OrderTypeID;
	public String OrderNo;
	
	public String getCancelConfirmationID() {
		return CancelConfirmationID;
	}
	public void setCancelConfirmationID(String cancelConfirmationID) {
		CancelConfirmationID = cancelConfirmationID;
	}
	public String getCancelConfirmationNo() {
		return CancelConfirmationNo;
	}
	public void setCancelConfirmationNo(String cancelConfirmationNo) {
		CancelConfirmationNo = cancelConfirmationNo;
	}
	public String getConfirmationID() {
		return ConfirmationID;
	}
	public void setConfirmationID(String confirmationID) {
		ConfirmationID = confirmationID;
	}
	public String getConfirmationNo() {
		return ConfirmationNo;
	}
	public void setConfirmationNo(String confirmationNo) {
		ConfirmationNo = confirmationNo;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getBatchNo() {
		return BatchNo;
	}
	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getQtyBaseUOM() {
		return QtyBaseUOM;
	}
	public void setQtyBaseUOM(String qtyBaseUOM) {
		QtyBaseUOM = qtyBaseUOM;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getWarehouseID() {
		return WarehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		WarehouseID = warehouseID;
	}
	public String getOrderTypeID() {
		return OrderTypeID;
	}
	public void setOrderTypeID(String orderTypeID) {
		OrderTypeID = orderTypeID;
	}
	public String getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(String orderNo) {
		OrderNo = orderNo;
	}
	
	
}
