package model;

public class mdlBOMDetail {

	public String ProductID;
	public String ValidDate;
	public String PlantID;
	public String ComponentLine;
	public String ComponentID;
	public String ComponentName;
	public String Qty;
	public String UOM;
	public String QtyBaseUOM;
	public String BaseUOM;
	public Integer QtyBomHeader;
	public String UOMBomHeader;
	public Integer QtyBaseUOMBomHeader;
	public String BaseUOMBomHeader;
	
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getValidDate() {
		return ValidDate;
	}
	public void setValidDate(String validDate) {
		ValidDate = validDate;
	}
	public String getPlantID() {
		return PlantID;
	}
	public void setPlantID(String plantID) {
		PlantID = plantID;
	}
	public String getComponentLine() {
		return ComponentLine;
	}
	public void setComponentLine(String componentLine) {
		ComponentLine = componentLine;
	}
	public String getComponentID() {
		return ComponentID;
	}
	public void setComponentID(String componentID) {
		ComponentID = componentID;
	}
	public String getComponentName() {
		return ComponentName;
	}
	public void setComponentName(String componentName) {
		ComponentName = componentName;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getQtyBaseUOM() {
		return QtyBaseUOM;
	}
	public void setQtyBaseUOM(String qtyBaseUOM) {
		QtyBaseUOM = qtyBaseUOM;
	}
	public String getBaseUOM() {
		return BaseUOM;
	}
	public void setBaseUOM(String baseUOM) {
		BaseUOM = baseUOM;
	}
	public Integer getQtyBomHeader() {
		return QtyBomHeader;
	}
	public void setQtyBomHeader(Integer qtyBomHeader) {
		QtyBomHeader = qtyBomHeader;
	}
	public String getUOMBomHeader() {
		return UOMBomHeader;
	}
	public void setUOMBomHeader(String uOMBomHeader) {
		UOMBomHeader = uOMBomHeader;
	}
	public Integer getQtyBaseUOMBomHeader() {
		return QtyBaseUOMBomHeader;
	}
	public void setQtyBaseUOMBomHeader(Integer qtyBaseUOMBomHeader) {
		QtyBaseUOMBomHeader = qtyBaseUOMBomHeader;
	}
	public String getBaseUOMBomHeader() {
		return BaseUOMBomHeader;
	}
	public void setBaseUOMBomHeader(String baseUOMBomHeader) {
		BaseUOMBomHeader = baseUOMBomHeader;
	}
	
	
}
