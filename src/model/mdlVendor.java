package model;

/** Documentation
 * 
 */
public class mdlVendor {
	public String VendorID;
	public String VendorType;
	public String VendorName;
	public String VendorAddress;
	public String Phone;
	public String PIC;
	
	public String getPIC() {
		return PIC;
	}
	public void setPIC(String pIC) {
		PIC = pIC;
	}
	public String getVendorID() {
		return VendorID;
	}
	public void setVendorID(String vendorID) {
		VendorID = vendorID;
	}
	
	
	public String getVendorType() {
		return VendorType;
	}
	public void setVendorType(String vendorType) {
		VendorType = vendorType;
	}
	
	public String getVendorName() {
		return VendorName;
	}
	public void setVendorName(String vendorName) {
		VendorName = vendorName;
	}
	
	public String getVendorAddress() {
		return VendorAddress;
	}	
	public void setVendorAddress(String vendorAddress) {
		VendorAddress = vendorAddress;
	}
	
	public String getPhone() {
		return Phone;
	}	
	public void setPhone(String phone) {
		Phone = phone;
	}
}
